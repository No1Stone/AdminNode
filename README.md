# **Development environment configuration**

```
AdminNode
```

# **Amdin Structure**
```
com.sparwk.adminnode.admin
- biz(비즈니스 서비스)
    - version
        - account (계정 프로필 관련 /인증 및 조회)
        - anr (A&R 관련 코드)
        - board (Q&A, FAQ, News 등)
        - commonCode(공통코드)
        - continent (대륙코드)
        - country (나라코드)
        - currency (통화코드)
        - language (언어코드)
        - passwordPolicy (패스워드 정책)
        - permissionSet (관리자 권한관리)
        - permissionSetGroups (사용안함)
        - popup (팝업관리)
        - profile (프로필 조회, account와 통합)
        - projects (프로젝트 관리)
        - role(DDEX, SPARWK 등 role 관련 코드)
        - sns (SNS 코드)
        - song (Song 조회)
        - songCode(장르, 서브장르 등 song 관련 코드)
        - timezone (Timezone 코드)
- config (설정)
    - common****
    - filter
        - exception
- jpa
    - entity
        - account
        - anr
        - board
        - codeSeq
        - commonCode
        - continent
        - country
        - currency
        - language
        - passwordPolicy
        - permissionSet
        - permissionSetGroups
        - popup
        - pofile
        - projects
        - role
        - sns
        - song
        - songCode
        - timezone
    - repository
        - account
            - dsl
        - anr
            - dsl
        - board
            - dsl
        - commonCode
            - dsl
        - continent
            - dsl
        - country
            - dsl
        - currency
            - dsl
        - language
            - dsl
        - permissionSet
            - dsl
        - permissionSetGroups
            - dsl
        - passwordPolicy
            - dsl
        - popup
            - dsl
        - profile
            - dsl
        - projects
            - dsl
        - role
            - dsl
        - sns
            - dsl
        - song
            - dsl
        - songCode
            - dsl
        - timezone
            - dsl
        
```


# **Server Architecture**
```
project port: 8090
```


# **Database Table List**
```
-- code infomation
tb_admin_anr_code
tb_admin_common_code
tb_admin_common_detail_code
tb_admin_continent_code
tb_admin_country_code
tb_admin_currency_code
tb_admin_language_code
tb_admin_menu
tb_admin_organization
tb_admin_password_policy
tb_admin_permission
tb_admin_permission_group
tb_admin_permission_menu
tb_admin_role_detail_code
tb_admin_sns_code
tb_admin_song_detail_code
tb_admin_timezone_code
tb_board_attach
tb_board_faq
tb_board_news
tb_board_policy
tb_board_popup
tb_board_qna
tb_board_success
view_admin_associate_mamber_list
view_admin_company_account_list
view_admin_individual_account_list
view_admin_project_list
view_admin_song_list
```

