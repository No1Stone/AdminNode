package com.sparwk.adminnode.admin.jpa.entity.commonCode;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QCommonOrganizationCodeEntity is a Querydsl query type for CommonOrganizationCodeEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QCommonOrganizationCodeEntity extends EntityPathBase<CommonOrganizationCodeEntity> {

    private static final long serialVersionUID = -1818803737L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QCommonOrganizationCodeEntity commonOrganizationCodeEntity = new QCommonOrganizationCodeEntity("commonOrganizationCodeEntity");

    public final com.sparwk.adminnode.admin.jpa.entity.QBaseEntity _super = new com.sparwk.adminnode.admin.jpa.entity.QBaseEntity(this);

    public final QCommonCodeEntity commonCodeEntity;

    public final NumberPath<Long> commonOrganizationCodeSeq = createNumber("commonOrganizationCodeSeq", Long.class);

    public final StringPath countryCd = createString("countryCd");

    public final StringPath dcode = createString("dcode");

    public final StringPath description = createString("description");

    public final NumberPath<Integer> hit = createNumber("hit", Integer.class);

    //inherited
    public final DateTimePath<java.time.LocalDateTime> modDt = _super.modDt;

    //inherited
    public final NumberPath<Long> modUsr = _super.modUsr;

    public final StringPath pcode = createString("pcode");

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> popularType = createEnum("popularType", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    //inherited
    public final DateTimePath<java.time.LocalDateTime> regDt = _super.regDt;

    //inherited
    public final NumberPath<Long> regUsr = _super.regUsr;

    public final NumberPath<Integer> sortIndex = createNumber("sortIndex", Integer.class);

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> useType = createEnum("useType", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public final StringPath val = createString("val");

    public QCommonOrganizationCodeEntity(String variable) {
        this(CommonOrganizationCodeEntity.class, forVariable(variable), INITS);
    }

    public QCommonOrganizationCodeEntity(Path<? extends CommonOrganizationCodeEntity> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QCommonOrganizationCodeEntity(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QCommonOrganizationCodeEntity(PathMetadata metadata, PathInits inits) {
        this(CommonOrganizationCodeEntity.class, metadata, inits);
    }

    public QCommonOrganizationCodeEntity(Class<? extends CommonOrganizationCodeEntity> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.commonCodeEntity = inits.isInitialized("commonCodeEntity") ? new QCommonCodeEntity(forProperty("commonCodeEntity")) : null;
    }

}

