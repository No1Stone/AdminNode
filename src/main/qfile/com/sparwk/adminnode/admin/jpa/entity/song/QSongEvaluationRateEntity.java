package com.sparwk.adminnode.admin.jpa.entity.song;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QSongEvaluationRateEntity is a Querydsl query type for SongEvaluationRateEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QSongEvaluationRateEntity extends EntityPathBase<SongEvaluationRateEntity> {

    private static final long serialVersionUID = 1950499088L;

    public static final QSongEvaluationRateEntity songEvaluationRateEntity = new QSongEvaluationRateEntity("songEvaluationRateEntity");

    public final com.sparwk.adminnode.admin.jpa.entity.QBaseEntity _super = new com.sparwk.adminnode.admin.jpa.entity.QBaseEntity(this);

    public final NumberPath<Long> evalAnrSeq = createNumber("evalAnrSeq", Long.class);

    public final NumberPath<Integer> evalRate = createNumber("evalRate", Integer.class);

    public final NumberPath<Long> evalSeq = createNumber("evalSeq", Long.class);

    public final StringPath evalTemplDtlCd = createString("evalTemplDtlCd");

    //inherited
    public final DateTimePath<java.time.LocalDateTime> modDt = _super.modDt;

    //inherited
    public final NumberPath<Long> modUsr = _super.modUsr;

    //inherited
    public final DateTimePath<java.time.LocalDateTime> regDt = _super.regDt;

    //inherited
    public final NumberPath<Long> regUsr = _super.regUsr;

    public QSongEvaluationRateEntity(String variable) {
        super(SongEvaluationRateEntity.class, forVariable(variable));
    }

    public QSongEvaluationRateEntity(Path<? extends SongEvaluationRateEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QSongEvaluationRateEntity(PathMetadata metadata) {
        super(SongEvaluationRateEntity.class, metadata);
    }

}

