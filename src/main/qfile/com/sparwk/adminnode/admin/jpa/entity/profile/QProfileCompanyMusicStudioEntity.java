package com.sparwk.adminnode.admin.jpa.entity.profile;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QProfileCompanyMusicStudioEntity is a Querydsl query type for ProfileCompanyMusicStudioEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QProfileCompanyMusicStudioEntity extends EntityPathBase<ProfileCompanyMusicStudioEntity> {

    private static final long serialVersionUID = 1981465608L;

    public static final QProfileCompanyMusicStudioEntity profileCompanyMusicStudioEntity = new QProfileCompanyMusicStudioEntity("profileCompanyMusicStudioEntity");

    public final com.sparwk.adminnode.admin.jpa.entity.QBaseEntity _super = new com.sparwk.adminnode.admin.jpa.entity.QBaseEntity(this);

    public final StringPath addr1 = createString("addr1");

    public final StringPath addr2 = createString("addr2");

    public final StringPath businessLocationCd = createString("businessLocationCd");

    public final StringPath city = createString("city");

    //inherited
    public final DateTimePath<java.time.LocalDateTime> modDt = _super.modDt;

    //inherited
    public final NumberPath<Long> modUsr = _super.modUsr;

    public final NumberPath<Long> ownerId = createNumber("ownerId", Long.class);

    public final StringPath postCd = createString("postCd");

    public final NumberPath<Long> profileId = createNumber("profileId", Long.class);

    //inherited
    public final DateTimePath<java.time.LocalDateTime> regDt = _super.regDt;

    public final StringPath region = createString("region");

    //inherited
    public final NumberPath<Long> regUsr = _super.regUsr;

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> skipYn = createEnum("skipYn", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public final NumberPath<Long> studioId = createNumber("studioId", Long.class);

    public final NumberPath<Long> studioName = createNumber("studioName", Long.class);

    public QProfileCompanyMusicStudioEntity(String variable) {
        super(ProfileCompanyMusicStudioEntity.class, forVariable(variable));
    }

    public QProfileCompanyMusicStudioEntity(Path<? extends ProfileCompanyMusicStudioEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QProfileCompanyMusicStudioEntity(PathMetadata metadata) {
        super(ProfileCompanyMusicStudioEntity.class, metadata);
    }

}

