package com.sparwk.adminnode.admin.jpa.entity.permissionSet;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QPermissionMenuSetEntity is a Querydsl query type for PermissionMenuSetEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QPermissionMenuSetEntity extends EntityPathBase<PermissionMenuSetEntity> {

    private static final long serialVersionUID = -2072104193L;

    public static final QPermissionMenuSetEntity permissionMenuSetEntity = new QPermissionMenuSetEntity("permissionMenuSetEntity");

    public final com.sparwk.adminnode.admin.jpa.entity.QBaseEntity _super = new com.sparwk.adminnode.admin.jpa.entity.QBaseEntity(this);

    public final NumberPath<Long> adminMenuId = createNumber("adminMenuId", Long.class);

    public final NumberPath<Long> adminPermissionId = createNumber("adminPermissionId", Long.class);

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> createYn = createEnum("createYn", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> deleteYn = createEnum("deleteYn", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> editYn = createEnum("editYn", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    //inherited
    public final DateTimePath<java.time.LocalDateTime> modDt = _super.modDt;

    //inherited
    public final NumberPath<Long> modUsr = _super.modUsr;

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> readYn = createEnum("readYn", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    //inherited
    public final DateTimePath<java.time.LocalDateTime> regDt = _super.regDt;

    //inherited
    public final NumberPath<Long> regUsr = _super.regUsr;

    public QPermissionMenuSetEntity(String variable) {
        super(PermissionMenuSetEntity.class, forVariable(variable));
    }

    public QPermissionMenuSetEntity(Path<? extends PermissionMenuSetEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QPermissionMenuSetEntity(PathMetadata metadata) {
        super(PermissionMenuSetEntity.class, metadata);
    }

}

