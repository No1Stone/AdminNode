package com.sparwk.adminnode.admin.jpa.entity.admin;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QAdminLoginLogEntity is a Querydsl query type for AdminLoginLogEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QAdminLoginLogEntity extends EntityPathBase<AdminLoginLogEntity> {

    private static final long serialVersionUID = 1437396113L;

    public static final QAdminLoginLogEntity adminLoginLogEntity = new QAdminLoginLogEntity("adminLoginLogEntity");

    public final NumberPath<Long> adminId = createNumber("adminId", Long.class);

    public final NumberPath<Long> adminLoginLogSeq = createNumber("adminLoginLogSeq", Long.class);

    public final StringPath connectBrowser = createString("connectBrowser");

    public final StringPath connectDevice = createString("connectDevice");

    public final StringPath connectIp = createString("connectIp");

    public final StringPath connectOs = createString("connectOs");

    public final StringPath connectResult = createString("connectResult");

    public final DateTimePath<java.time.LocalDateTime> connectTime = createDateTime("connectTime", java.time.LocalDateTime.class);

    public QAdminLoginLogEntity(String variable) {
        super(AdminLoginLogEntity.class, forVariable(variable));
    }

    public QAdminLoginLogEntity(Path<? extends AdminLoginLogEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QAdminLoginLogEntity(PathMetadata metadata) {
        super(AdminLoginLogEntity.class, metadata);
    }

}

