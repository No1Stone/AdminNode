package com.sparwk.adminnode.admin.jpa.entity.anr;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QAnrEntity is a Querydsl query type for AnrEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QAnrEntity extends EntityPathBase<AnrEntity> {

    private static final long serialVersionUID = 214434690L;

    public static final QAnrEntity anrEntity = new QAnrEntity("anrEntity");

    public final com.sparwk.adminnode.admin.jpa.entity.QBaseEntity _super = new com.sparwk.adminnode.admin.jpa.entity.QBaseEntity(this);

    public final StringPath anrCd = createString("anrCd");

    public final StringPath anrGroupCd = createString("anrGroupCd");

    public final NumberPath<Long> anrSeq = createNumber("anrSeq", Long.class);

    public final StringPath anrServiceName = createString("anrServiceName");

    public final StringPath description = createString("description");

    //inherited
    public final DateTimePath<java.time.LocalDateTime> modDt = _super.modDt;

    //inherited
    public final NumberPath<Long> modUsr = _super.modUsr;

    //inherited
    public final DateTimePath<java.time.LocalDateTime> regDt = _super.regDt;

    //inherited
    public final NumberPath<Long> regUsr = _super.regUsr;

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> useType = createEnum("useType", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public QAnrEntity(String variable) {
        super(AnrEntity.class, forVariable(variable));
    }

    public QAnrEntity(Path<? extends AnrEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QAnrEntity(PathMetadata metadata) {
        super(AnrEntity.class, metadata);
    }

}

