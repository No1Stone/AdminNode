package com.sparwk.adminnode.admin.jpa.entity.song;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QSongPitchListAnrEntity is a Querydsl query type for SongPitchListAnrEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QSongPitchListAnrEntity extends EntityPathBase<SongPitchListAnrEntity> {

    private static final long serialVersionUID = 997295067L;

    public static final QSongPitchListAnrEntity songPitchListAnrEntity = new QSongPitchListAnrEntity("songPitchListAnrEntity");

    public final com.sparwk.adminnode.admin.jpa.entity.QBaseEntity _super = new com.sparwk.adminnode.admin.jpa.entity.QBaseEntity(this);

    public final NumberPath<Long> anrProfileCompanyId = createNumber("anrProfileCompanyId", Long.class);

    //inherited
    public final DateTimePath<java.time.LocalDateTime> modDt = _super.modDt;

    //inherited
    public final NumberPath<Long> modUsr = _super.modUsr;

    public final NumberPath<Long> pitchlistAnrSeq = createNumber("pitchlistAnrSeq", Long.class);

    public final NumberPath<Long> pitchlistId = createNumber("pitchlistId", Long.class);

    public final StringPath pitchStatus = createString("pitchStatus");

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> pitchYn = createEnum("pitchYn", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public final NumberPath<Long> profileId = createNumber("profileId", Long.class);

    //inherited
    public final DateTimePath<java.time.LocalDateTime> regDt = _super.regDt;

    //inherited
    public final NumberPath<Long> regUsr = _super.regUsr;

    public QSongPitchListAnrEntity(String variable) {
        super(SongPitchListAnrEntity.class, forVariable(variable));
    }

    public QSongPitchListAnrEntity(Path<? extends SongPitchListAnrEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QSongPitchListAnrEntity(PathMetadata metadata) {
        super(SongPitchListAnrEntity.class, metadata);
    }

}

