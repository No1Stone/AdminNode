package com.sparwk.adminnode.admin.jpa.entity.account;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QAccountGroupTypeEntity is a Querydsl query type for AccountGroupTypeEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QAccountGroupTypeEntity extends EntityPathBase<AccountGroupTypeEntity> {

    private static final long serialVersionUID = 2030897869L;

    public static final QAccountGroupTypeEntity accountGroupTypeEntity = new QAccountGroupTypeEntity("accountGroupTypeEntity");

    public final com.sparwk.adminnode.admin.jpa.entity.QBaseEntity _super = new com.sparwk.adminnode.admin.jpa.entity.QBaseEntity(this);

    public final NumberPath<Long> accntId = createNumber("accntId", Long.class);

    public final StringPath groupCd = createString("groupCd");

    public final StringPath groupLicenseFileName = createString("groupLicenseFileName");

    public final StringPath groupLicenseFileUrl = createString("groupLicenseFileUrl");

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> groupLicenseVerifyYn = createEnum("groupLicenseVerifyYn", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    //inherited
    public final DateTimePath<java.time.LocalDateTime> modDt = _super.modDt;

    //inherited
    public final NumberPath<Long> modUsr = _super.modUsr;

    //inherited
    public final DateTimePath<java.time.LocalDateTime> regDt = _super.regDt;

    //inherited
    public final NumberPath<Long> regUsr = _super.regUsr;

    public final StringPath songDataUrl = createString("songDataUrl");

    public QAccountGroupTypeEntity(String variable) {
        super(AccountGroupTypeEntity.class, forVariable(variable));
    }

    public QAccountGroupTypeEntity(Path<? extends AccountGroupTypeEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QAccountGroupTypeEntity(PathMetadata metadata) {
        super(AccountGroupTypeEntity.class, metadata);
    }

}

