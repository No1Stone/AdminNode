package com.sparwk.adminnode.admin.jpa.entity.account;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QAccountPassportEntity is a Querydsl query type for AccountPassportEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QAccountPassportEntity extends EntityPathBase<AccountPassportEntity> {

    private static final long serialVersionUID = -651829948L;

    public static final QAccountPassportEntity accountPassportEntity = new QAccountPassportEntity("accountPassportEntity");

    public final NumberPath<Long> accntId = createNumber("accntId", Long.class);

    public final StringPath countryCd = createString("countryCd");

    public final StringPath passportFirstName = createString("passportFirstName");

    public final StringPath passportImgFileUrl = createString("passportImgFileUrl");

    public final StringPath passportLastName = createString("passportLastName");

    public final StringPath passportMiddleName = createString("passportMiddleName");

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> passportVerifyYn = createEnum("passportVerifyYn", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> personalInfoCollectionYn = createEnum("personalInfoCollectionYn", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public QAccountPassportEntity(String variable) {
        super(AccountPassportEntity.class, forVariable(variable));
    }

    public QAccountPassportEntity(Path<? extends AccountPassportEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QAccountPassportEntity(PathMetadata metadata) {
        super(AccountPassportEntity.class, metadata);
    }

}

