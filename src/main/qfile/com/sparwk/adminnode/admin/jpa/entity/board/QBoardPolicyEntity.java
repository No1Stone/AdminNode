package com.sparwk.adminnode.admin.jpa.entity.board;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QBoardPolicyEntity is a Querydsl query type for BoardPolicyEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QBoardPolicyEntity extends EntityPathBase<BoardPolicyEntity> {

    private static final long serialVersionUID = 1291030L;

    public static final QBoardPolicyEntity boardPolicyEntity = new QBoardPolicyEntity("boardPolicyEntity");

    public final com.sparwk.adminnode.admin.jpa.entity.QBaseEntity _super = new com.sparwk.adminnode.admin.jpa.entity.QBaseEntity(this);

    public final StringPath cateCd = createString("cateCd");

    public final StringPath content = createString("content");

    public final NumberPath<Long> hit = createNumber("hit", Long.class);

    //inherited
    public final DateTimePath<java.time.LocalDateTime> modDt = _super.modDt;

    //inherited
    public final NumberPath<Long> modUsr = _super.modUsr;

    public final NumberPath<Long> policyId = createNumber("policyId", Long.class);

    //inherited
    public final DateTimePath<java.time.LocalDateTime> regDt = _super.regDt;

    //inherited
    public final NumberPath<Long> regUsr = _super.regUsr;

    public final StringPath title = createString("title");

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> useYn = createEnum("useYn", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public QBoardPolicyEntity(String variable) {
        super(BoardPolicyEntity.class, forVariable(variable));
    }

    public QBoardPolicyEntity(Path<? extends BoardPolicyEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QBoardPolicyEntity(PathMetadata metadata) {
        super(BoardPolicyEntity.class, metadata);
    }

}

