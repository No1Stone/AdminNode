package com.sparwk.adminnode.admin.jpa.entity.projects;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QProjectViewEntity is a Querydsl query type for ProjectViewEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QProjectViewEntity extends EntityPathBase<ProjectViewEntity> {

    private static final long serialVersionUID = -209074966L;

    public static final QProjectViewEntity projectViewEntity = new QProjectViewEntity("projectViewEntity");

    public final StringPath avatarFileUrl = createString("avatarFileUrl");

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> completeYn = createEnum("completeYn", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public final NumberPath<Long> compProfileId = createNumber("compProfileId", Long.class);

    public final StringPath compProfileName = createString("compProfileName");

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modUsr = createNumber("modUsr", Long.class);

    public final StringPath pitchProjTypeCd = createString("pitchProjTypeCd");

    public final StringPath pitchProjTypeCdName = createString("pitchProjTypeCdName");

    public final StringPath projCondCd = createString("projCondCd");

    public final StringPath projCondCdName = createString("projCondCdName");

    public final StringPath projGenreCd = createString("projGenreCd");

    public final StringPath projGenreName = createString("projGenreName");

    public final NumberPath<Long> projId = createNumber("projId", Long.class);

    public final NumberPath<Long> projOwner = createNumber("projOwner", Long.class);

    public final StringPath projOwnerName = createString("projOwnerName");

    public final StringPath projTitle = createString("projTitle");

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> regUsr = createNumber("regUsr", Long.class);

    public QProjectViewEntity(String variable) {
        super(ProjectViewEntity.class, forVariable(variable));
    }

    public QProjectViewEntity(Path<? extends ProjectViewEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QProjectViewEntity(PathMetadata metadata) {
        super(ProjectViewEntity.class, metadata);
    }

}

