package com.sparwk.adminnode.admin.jpa.entity.permissionSetGroups;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QPermissionSetGroupsEntity is a Querydsl query type for PermissionSetGroupsEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QPermissionSetGroupsEntity extends EntityPathBase<PermissionSetGroupsEntity> {

    private static final long serialVersionUID = 78288198L;

    public static final QPermissionSetGroupsEntity permissionSetGroupsEntity = new QPermissionSetGroupsEntity("permissionSetGroupsEntity");

    public final com.sparwk.adminnode.admin.jpa.entity.QBaseEntity _super = new com.sparwk.adminnode.admin.jpa.entity.QBaseEntity(this);

    public final NumberPath<Long> adminPermissionGroupId = createNumber("adminPermissionGroupId", Long.class);

    public final StringPath cdDesc = createString("cdDesc");

    public final NumberPath<Integer> cdOrd = createNumber("cdOrd", Integer.class);

    public final StringPath labelVal = createString("labelVal");

    //inherited
    public final DateTimePath<java.time.LocalDateTime> modDt = _super.modDt;

    //inherited
    public final NumberPath<Long> modUsr = _super.modUsr;

    //inherited
    public final DateTimePath<java.time.LocalDateTime> regDt = _super.regDt;

    //inherited
    public final NumberPath<Long> regUsr = _super.regUsr;

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> useType = createEnum("useType", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public QPermissionSetGroupsEntity(String variable) {
        super(PermissionSetGroupsEntity.class, forVariable(variable));
    }

    public QPermissionSetGroupsEntity(Path<? extends PermissionSetGroupsEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QPermissionSetGroupsEntity(PathMetadata metadata) {
        super(PermissionSetGroupsEntity.class, metadata);
    }

}

