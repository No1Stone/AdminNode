package com.sparwk.adminnode.admin.jpa.entity.profile;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QProfileLanguageEntity is a Querydsl query type for ProfileLanguageEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QProfileLanguageEntity extends EntityPathBase<ProfileLanguageEntity> {

    private static final long serialVersionUID = 692390146L;

    public static final QProfileLanguageEntity profileLanguageEntity = new QProfileLanguageEntity("profileLanguageEntity");

    public final StringPath languageCd = createString("languageCd");

    public final NumberPath<Long> profileId = createNumber("profileId", Long.class);

    public QProfileLanguageEntity(String variable) {
        super(ProfileLanguageEntity.class, forVariable(variable));
    }

    public QProfileLanguageEntity(Path<? extends ProfileLanguageEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QProfileLanguageEntity(PathMetadata metadata) {
        super(ProfileLanguageEntity.class, metadata);
    }

}

