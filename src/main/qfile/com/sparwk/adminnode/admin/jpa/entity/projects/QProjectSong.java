package com.sparwk.adminnode.admin.jpa.entity.projects;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QProjectSong is a Querydsl query type for ProjectSong
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QProjectSong extends EntityPathBase<ProjectSong> {

    private static final long serialVersionUID = 293415063L;

    public static final QProjectSong projectSong = new QProjectSong("projectSong");

    public final com.sparwk.adminnode.admin.jpa.entity.QBaseEntity _super = new com.sparwk.adminnode.admin.jpa.entity.QBaseEntity(this);

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modUsr = createNumber("modUsr", Long.class);

    public final NumberPath<Long> projId = createNumber("projId", Long.class);

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> regUsr = createNumber("regUsr", Long.class);

    public final NumberPath<Long> songId = createNumber("songId", Long.class);

    public QProjectSong(String variable) {
        super(ProjectSong.class, forVariable(variable));
    }

    public QProjectSong(Path<? extends ProjectSong> path) {
        super(path.getType(), path.getMetadata());
    }

    public QProjectSong(PathMetadata metadata) {
        super(ProjectSong.class, metadata);
    }

}

