package com.sparwk.adminnode.admin.jpa.entity.board;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QBoardNewsEntity is a Querydsl query type for BoardNewsEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QBoardNewsEntity extends EntityPathBase<BoardNewsEntity> {

    private static final long serialVersionUID = -740655049L;

    public static final QBoardNewsEntity boardNewsEntity = new QBoardNewsEntity("boardNewsEntity");

    public final com.sparwk.adminnode.admin.jpa.entity.QBaseEntity _super = new com.sparwk.adminnode.admin.jpa.entity.QBaseEntity(this);

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> attachYn = createEnum("attachYn", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public final StringPath cateCd = createString("cateCd");

    public final StringPath content = createString("content");

    public final NumberPath<Long> hit = createNumber("hit", Long.class);

    //inherited
    public final DateTimePath<java.time.LocalDateTime> modDt = _super.modDt;

    //inherited
    public final NumberPath<Long> modUsr = _super.modUsr;

    public final NumberPath<Long> newsId = createNumber("newsId", Long.class);

    //inherited
    public final DateTimePath<java.time.LocalDateTime> regDt = _super.regDt;

    //inherited
    public final NumberPath<Long> regUsr = _super.regUsr;

    public final StringPath title = createString("title");

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> useYn = createEnum("useYn", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public QBoardNewsEntity(String variable) {
        super(BoardNewsEntity.class, forVariable(variable));
    }

    public QBoardNewsEntity(Path<? extends BoardNewsEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QBoardNewsEntity(PathMetadata metadata) {
        super(BoardNewsEntity.class, metadata);
    }

}

