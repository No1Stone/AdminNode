package com.sparwk.adminnode.admin.jpa.entity.commonCode;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QCommonCodeEntity is a Querydsl query type for CommonCodeEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QCommonCodeEntity extends EntityPathBase<CommonCodeEntity> {

    private static final long serialVersionUID = -1994444204L;

    public static final QCommonCodeEntity commonCodeEntity = new QCommonCodeEntity("commonCodeEntity");

    public final com.sparwk.adminnode.admin.jpa.entity.QBaseEntity _super = new com.sparwk.adminnode.admin.jpa.entity.QBaseEntity(this);

    public final StringPath code = createString("code");

    public final NumberPath<Long> commonCodeSeq = createNumber("commonCodeSeq", Long.class);

    public final ListPath<CommonDetailCodeEntity, QCommonDetailCodeEntity> commonDetailCodeEntity = this.<CommonDetailCodeEntity, QCommonDetailCodeEntity>createList("commonDetailCodeEntity", CommonDetailCodeEntity.class, QCommonDetailCodeEntity.class, PathInits.DIRECT2);

    public final StringPath format = createString("format");

    public final NumberPath<Integer> maxNum = createNumber("maxNum", Integer.class);

    //inherited
    public final DateTimePath<java.time.LocalDateTime> modDt = _super.modDt;

    //inherited
    public final NumberPath<Long> modUsr = _super.modUsr;

    //inherited
    public final DateTimePath<java.time.LocalDateTime> regDt = _super.regDt;

    //inherited
    public final NumberPath<Long> regUsr = _super.regUsr;

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> useType = createEnum("useType", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public final StringPath val = createString("val");

    public QCommonCodeEntity(String variable) {
        super(CommonCodeEntity.class, forVariable(variable));
    }

    public QCommonCodeEntity(Path<? extends CommonCodeEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QCommonCodeEntity(PathMetadata metadata) {
        super(CommonCodeEntity.class, metadata);
    }

}

