package com.sparwk.adminnode.admin.jpa.entity.board;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QBoardAttachEntity is a Querydsl query type for BoardAttachEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QBoardAttachEntity extends EntityPathBase<BoardAttachEntity> {

    private static final long serialVersionUID = -234839575L;

    public static final QBoardAttachEntity boardAttachEntity = new QBoardAttachEntity("boardAttachEntity");

    public final com.sparwk.adminnode.admin.jpa.entity.QBaseEntity _super = new com.sparwk.adminnode.admin.jpa.entity.QBaseEntity(this);

    public final NumberPath<Long> attachId = createNumber("attachId", Long.class);

    public final NumberPath<Long> boardId = createNumber("boardId", Long.class);

    public final StringPath boardType = createString("boardType");

    public final StringPath fileName = createString("fileName");

    public final NumberPath<Long> fileNum = createNumber("fileNum", Long.class);

    public final NumberPath<Long> fileSize = createNumber("fileSize", Long.class);

    public final StringPath fileUrl = createString("fileUrl");

    //inherited
    public final DateTimePath<java.time.LocalDateTime> modDt = _super.modDt;

    //inherited
    public final NumberPath<Long> modUsr = _super.modUsr;

    //inherited
    public final DateTimePath<java.time.LocalDateTime> regDt = _super.regDt;

    //inherited
    public final NumberPath<Long> regUsr = _super.regUsr;

    public QBoardAttachEntity(String variable) {
        super(BoardAttachEntity.class, forVariable(variable));
    }

    public QBoardAttachEntity(Path<? extends BoardAttachEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QBoardAttachEntity(PathMetadata metadata) {
        super(BoardAttachEntity.class, metadata);
    }

}

