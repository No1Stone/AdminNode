package com.sparwk.adminnode.admin.jpa.entity.profile;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QProfileEducation is a Querydsl query type for ProfileEducation
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QProfileEducation extends EntityPathBase<ProfileEducation> {

    private static final long serialVersionUID = 1394308001L;

    public static final QProfileEducation profileEducation = new QProfileEducation("profileEducation");

    public final com.sparwk.adminnode.admin.jpa.entity.QBaseEntity _super = new com.sparwk.adminnode.admin.jpa.entity.QBaseEntity(this);

    public final StringPath eduCourseNm = createString("eduCourseNm");

    public final DateTimePath<java.time.LocalDateTime> eduEndDt = createDateTime("eduEndDt", java.time.LocalDateTime.class);

    public final StringPath eduOrganizationNm = createString("eduOrganizationNm");

    public final NumberPath<Long> eduSeq = createNumber("eduSeq", Long.class);

    public final DateTimePath<java.time.LocalDateTime> eduStartDt = createDateTime("eduStartDt", java.time.LocalDateTime.class);

    public final StringPath locationCityCountryCd = createString("locationCityCountryCd");

    public final StringPath locationCityNm = createString("locationCityNm");

    //inherited
    public final DateTimePath<java.time.LocalDateTime> modDt = _super.modDt;

    //inherited
    public final NumberPath<Long> modUsr = _super.modUsr;

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> presentYn = createEnum("presentYn", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public final NumberPath<Long> profileId = createNumber("profileId", Long.class);

    //inherited
    public final DateTimePath<java.time.LocalDateTime> regDt = _super.regDt;

    //inherited
    public final NumberPath<Long> regUsr = _super.regUsr;

    public QProfileEducation(String variable) {
        super(ProfileEducation.class, forVariable(variable));
    }

    public QProfileEducation(Path<? extends ProfileEducation> path) {
        super(path.getType(), path.getMetadata());
    }

    public QProfileEducation(PathMetadata metadata) {
        super(ProfileEducation.class, metadata);
    }

}

