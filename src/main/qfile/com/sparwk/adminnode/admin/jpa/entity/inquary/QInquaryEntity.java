package com.sparwk.adminnode.admin.jpa.entity.inquary;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QInquaryEntity is a Querydsl query type for InquaryEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QInquaryEntity extends EntityPathBase<InquaryEntity> {

    private static final long serialVersionUID = -1962672202L;

    public static final QInquaryEntity inquaryEntity = new QInquaryEntity("inquaryEntity");

    public final NumberPath<Long> adminId = createNumber("adminId", Long.class);

    public final NumberPath<Long> adminLoginLogSeq = createNumber("adminLoginLogSeq", Long.class);

    public final StringPath connectBrowser = createString("connectBrowser");

    public final StringPath connectDevice = createString("connectDevice");

    public final StringPath connectIp = createString("connectIp");

    public final StringPath connectOs = createString("connectOs");

    public final StringPath connectResult = createString("connectResult");

    public final DateTimePath<java.time.LocalDateTime> connectTime = createDateTime("connectTime", java.time.LocalDateTime.class);

    public QInquaryEntity(String variable) {
        super(InquaryEntity.class, forVariable(variable));
    }

    public QInquaryEntity(Path<? extends InquaryEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QInquaryEntity(PathMetadata metadata) {
        super(InquaryEntity.class, metadata);
    }

}

