package com.sparwk.adminnode.admin.jpa.entity.profile;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QProfileCompanyOnthewebEntity is a Querydsl query type for ProfileCompanyOnthewebEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QProfileCompanyOnthewebEntity extends EntityPathBase<ProfileCompanyOnthewebEntity> {

    private static final long serialVersionUID = -561710405L;

    public static final QProfileCompanyOnthewebEntity profileCompanyOnthewebEntity = new QProfileCompanyOnthewebEntity("profileCompanyOnthewebEntity");

    public final com.sparwk.adminnode.admin.jpa.entity.QBaseEntity _super = new com.sparwk.adminnode.admin.jpa.entity.QBaseEntity(this);

    //inherited
    public final DateTimePath<java.time.LocalDateTime> modDt = _super.modDt;

    //inherited
    public final NumberPath<Long> modUsr = _super.modUsr;

    public final NumberPath<Long> profileId = createNumber("profileId", Long.class);

    //inherited
    public final DateTimePath<java.time.LocalDateTime> regDt = _super.regDt;

    //inherited
    public final NumberPath<Long> regUsr = _super.regUsr;

    public final StringPath snsTypeCd = createString("snsTypeCd");

    public final StringPath snsUrl = createString("snsUrl");

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> useYn = createEnum("useYn", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public QProfileCompanyOnthewebEntity(String variable) {
        super(ProfileCompanyOnthewebEntity.class, forVariable(variable));
    }

    public QProfileCompanyOnthewebEntity(Path<? extends ProfileCompanyOnthewebEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QProfileCompanyOnthewebEntity(PathMetadata metadata) {
        super(ProfileCompanyOnthewebEntity.class, metadata);
    }

}

