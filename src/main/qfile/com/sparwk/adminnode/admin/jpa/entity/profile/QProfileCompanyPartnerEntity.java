package com.sparwk.adminnode.admin.jpa.entity.profile;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QProfileCompanyPartnerEntity is a Querydsl query type for ProfileCompanyPartnerEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QProfileCompanyPartnerEntity extends EntityPathBase<ProfileCompanyPartnerEntity> {

    private static final long serialVersionUID = 1424149429L;

    public static final QProfileCompanyPartnerEntity profileCompanyPartnerEntity = new QProfileCompanyPartnerEntity("profileCompanyPartnerEntity");

    public final com.sparwk.adminnode.admin.jpa.entity.QBaseEntity _super = new com.sparwk.adminnode.admin.jpa.entity.QBaseEntity(this);

    //inherited
    public final DateTimePath<java.time.LocalDateTime> modDt = _super.modDt;

    //inherited
    public final NumberPath<Long> modUsr = _super.modUsr;

    public final NumberPath<Long> partnerId = createNumber("partnerId", Long.class);

    public final NumberPath<Long> profileId = createNumber("profileId", Long.class);

    //inherited
    public final DateTimePath<java.time.LocalDateTime> regDt = _super.regDt;

    //inherited
    public final NumberPath<Long> regUsr = _super.regUsr;

    public final StringPath relationStatus = createString("relationStatus");

    public final StringPath relationType = createString("relationType");

    public QProfileCompanyPartnerEntity(String variable) {
        super(ProfileCompanyPartnerEntity.class, forVariable(variable));
    }

    public QProfileCompanyPartnerEntity(Path<? extends ProfileCompanyPartnerEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QProfileCompanyPartnerEntity(PathMetadata metadata) {
        super(ProfileCompanyPartnerEntity.class, metadata);
    }

}

