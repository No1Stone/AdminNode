package com.sparwk.adminnode.admin.jpa.entity.projects;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QProjectMetadata is a Querydsl query type for ProjectMetadata
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QProjectMetadata extends EntityPathBase<ProjectMetadata> {

    private static final long serialVersionUID = -791994959L;

    public static final QProjectMetadata projectMetadata = new QProjectMetadata("projectMetadata");

    public final com.sparwk.adminnode.admin.jpa.entity.QBaseEntity _super = new com.sparwk.adminnode.admin.jpa.entity.QBaseEntity(this);

    public final StringPath attrDtlCd = createString("attrDtlCd");

    public final StringPath attrTypeCd = createString("attrTypeCd");

    //inherited
    public final DateTimePath<java.time.LocalDateTime> modDt = _super.modDt;

    //inherited
    public final NumberPath<Long> modUsr = _super.modUsr;

    public final NumberPath<Long> projId = createNumber("projId", Long.class);

    //inherited
    public final DateTimePath<java.time.LocalDateTime> regDt = _super.regDt;

    //inherited
    public final NumberPath<Long> regUsr = _super.regUsr;

    public QProjectMetadata(String variable) {
        super(ProjectMetadata.class, forVariable(variable));
    }

    public QProjectMetadata(Path<? extends ProjectMetadata> path) {
        super(path.getType(), path.getMetadata());
    }

    public QProjectMetadata(PathMetadata metadata) {
        super(ProjectMetadata.class, metadata);
    }

}

