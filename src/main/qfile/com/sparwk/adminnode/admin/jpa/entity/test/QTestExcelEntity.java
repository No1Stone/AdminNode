package com.sparwk.adminnode.admin.jpa.entity.test;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QTestExcelEntity is a Querydsl query type for TestExcelEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QTestExcelEntity extends EntityPathBase<TestExcelEntity> {

    private static final long serialVersionUID = 868794473L;

    public static final QTestExcelEntity testExcelEntity = new QTestExcelEntity("testExcelEntity");

    public final StringPath cdesc = createString("cdesc");

    public final StringPath ckey = createString("ckey");

    public final StringPath code = createString("code");

    public final StringPath cval = createString("cval");

    public final NumberPath<Long> testExcelId = createNumber("testExcelId", Long.class);

    public QTestExcelEntity(String variable) {
        super(TestExcelEntity.class, forVariable(variable));
    }

    public QTestExcelEntity(Path<? extends TestExcelEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTestExcelEntity(PathMetadata metadata) {
        super(TestExcelEntity.class, metadata);
    }

}

