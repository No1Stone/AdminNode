package com.sparwk.adminnode.admin.jpa.entity.board;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QBoardQnaEntity is a Querydsl query type for BoardQnaEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QBoardQnaEntity extends EntityPathBase<BoardQnaEntity> {

    private static final long serialVersionUID = -1697922266L;

    public static final QBoardQnaEntity boardQnaEntity = new QBoardQnaEntity("boardQnaEntity");

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> answerAttachYn = createEnum("answerAttachYn", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public final StringPath answerContent = createString("answerContent");

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> answerEmailYn = createEnum("answerEmailYn", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public final DateTimePath<java.time.LocalDateTime> answerModDt = createDateTime("answerModDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> answerModUsr = createNumber("answerModUsr", Long.class);

    public final DateTimePath<java.time.LocalDateTime> answerRegDt = createDateTime("answerRegDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> answerRegUsr = createNumber("answerRegUsr", Long.class);

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> answerYn = createEnum("answerYn", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public final StringPath cateCd = createString("cateCd");

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modUsr = createNumber("modUsr", Long.class);

    public final NumberPath<Long> qnaId = createNumber("qnaId", Long.class);

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> questionAttachYn = createEnum("questionAttachYn", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public final StringPath questionContent = createString("questionContent");

    public final StringPath reciveEmail = createString("reciveEmail");

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> reciveEmailYn = createEnum("reciveEmailYn", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> regUsr = createNumber("regUsr", Long.class);

    public final StringPath title = createString("title");

    public QBoardQnaEntity(String variable) {
        super(BoardQnaEntity.class, forVariable(variable));
    }

    public QBoardQnaEntity(Path<? extends BoardQnaEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QBoardQnaEntity(PathMetadata metadata) {
        super(BoardQnaEntity.class, metadata);
    }

}

