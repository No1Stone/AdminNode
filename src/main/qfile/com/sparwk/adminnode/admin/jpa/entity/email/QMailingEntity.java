package com.sparwk.adminnode.admin.jpa.entity.email;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QMailingEntity is a Querydsl query type for MailingEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QMailingEntity extends EntityPathBase<MailingEntity> {

    private static final long serialVersionUID = -1031730273L;

    public static final QMailingEntity mailingEntity = new QMailingEntity("mailingEntity");

    public final com.sparwk.adminnode.admin.jpa.entity.QBaseEntity _super = new com.sparwk.adminnode.admin.jpa.entity.QBaseEntity(this);

    public final StringPath content = createString("content");

    public final StringPath fromEmail = createString("fromEmail");

    //inherited
    public final DateTimePath<java.time.LocalDateTime> modDt = _super.modDt;

    //inherited
    public final NumberPath<Long> modUsr = _super.modUsr;

    //inherited
    public final DateTimePath<java.time.LocalDateTime> regDt = _super.regDt;

    //inherited
    public final NumberPath<Long> regUsr = _super.regUsr;

    public final StringPath sendYn = createString("sendYn");

    public final StringPath status = createString("status");

    public final StringPath subject = createString("subject");

    public final StringPath toEmail = createString("toEmail");

    public QMailingEntity(String variable) {
        super(MailingEntity.class, forVariable(variable));
    }

    public QMailingEntity(Path<? extends MailingEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QMailingEntity(PathMetadata metadata) {
        super(MailingEntity.class, metadata);
    }

}

