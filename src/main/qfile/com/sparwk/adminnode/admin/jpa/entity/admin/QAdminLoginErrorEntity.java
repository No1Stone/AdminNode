package com.sparwk.adminnode.admin.jpa.entity.admin;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QAdminLoginErrorEntity is a Querydsl query type for AdminLoginErrorEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QAdminLoginErrorEntity extends EntityPathBase<AdminLoginErrorEntity> {

    private static final long serialVersionUID = 1447890389L;

    public static final QAdminLoginErrorEntity adminLoginErrorEntity = new QAdminLoginErrorEntity("adminLoginErrorEntity");

    public final NumberPath<Long> adminId = createNumber("adminId", Long.class);

    public final NumberPath<Long> adminLoginErrorSeq = createNumber("adminLoginErrorSeq", Long.class);

    public final NumberPath<Integer> errCnt = createNumber("errCnt", Integer.class);

    public final DateTimePath<java.time.LocalDateTime> errorLastConnect = createDateTime("errorLastConnect", java.time.LocalDateTime.class);

    public final StringPath errorLastIp = createString("errorLastIp");

    public QAdminLoginErrorEntity(String variable) {
        super(AdminLoginErrorEntity.class, forVariable(variable));
    }

    public QAdminLoginErrorEntity(Path<? extends AdminLoginErrorEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QAdminLoginErrorEntity(PathMetadata metadata) {
        super(AdminLoginErrorEntity.class, metadata);
    }

}

