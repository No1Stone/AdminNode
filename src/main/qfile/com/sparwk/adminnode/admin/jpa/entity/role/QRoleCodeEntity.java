package com.sparwk.adminnode.admin.jpa.entity.role;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QRoleCodeEntity is a Querydsl query type for RoleCodeEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QRoleCodeEntity extends EntityPathBase<RoleCodeEntity> {

    private static final long serialVersionUID = 1041426913L;

    public static final QRoleCodeEntity roleCodeEntity = new QRoleCodeEntity("roleCodeEntity");

    public final com.sparwk.adminnode.admin.jpa.entity.QBaseEntity _super = new com.sparwk.adminnode.admin.jpa.entity.QBaseEntity(this);

    public final StringPath code = createString("code");

    //inherited
    public final DateTimePath<java.time.LocalDateTime> modDt = _super.modDt;

    //inherited
    public final NumberPath<Long> modUsr = _super.modUsr;

    //inherited
    public final DateTimePath<java.time.LocalDateTime> regDt = _super.regDt;

    //inherited
    public final NumberPath<Long> regUsr = _super.regUsr;

    public final NumberPath<Long> roleCodeSeq = createNumber("roleCodeSeq", Long.class);

    public final ListPath<RoleDetailCodeEntity, QRoleDetailCodeEntity> roleDetailCodeEntity = this.<RoleDetailCodeEntity, QRoleDetailCodeEntity>createList("roleDetailCodeEntity", RoleDetailCodeEntity.class, QRoleDetailCodeEntity.class, PathInits.DIRECT2);

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> useType = createEnum("useType", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public final StringPath val = createString("val");

    public QRoleCodeEntity(String variable) {
        super(RoleCodeEntity.class, forVariable(variable));
    }

    public QRoleCodeEntity(Path<? extends RoleCodeEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QRoleCodeEntity(PathMetadata metadata) {
        super(RoleCodeEntity.class, metadata);
    }

}

