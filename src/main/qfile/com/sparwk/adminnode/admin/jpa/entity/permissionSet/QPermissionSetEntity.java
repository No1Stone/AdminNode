package com.sparwk.adminnode.admin.jpa.entity.permissionSet;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QPermissionSetEntity is a Querydsl query type for PermissionSetEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QPermissionSetEntity extends EntityPathBase<PermissionSetEntity> {

    private static final long serialVersionUID = -1722363362L;

    public static final QPermissionSetEntity permissionSetEntity = new QPermissionSetEntity("permissionSetEntity");

    public final com.sparwk.adminnode.admin.jpa.entity.QBaseEntity _super = new com.sparwk.adminnode.admin.jpa.entity.QBaseEntity(this);

    public final NumberPath<Long> adminPermissionId = createNumber("adminPermissionId", Long.class);

    public final StringPath cdDesc = createString("cdDesc");

    public final NumberPath<Integer> cdOrd = createNumber("cdOrd", Integer.class);

    public final StringPath labelVal = createString("labelVal");

    //inherited
    public final DateTimePath<java.time.LocalDateTime> modDt = _super.modDt;

    //inherited
    public final NumberPath<Long> modUsr = _super.modUsr;

    //inherited
    public final DateTimePath<java.time.LocalDateTime> regDt = _super.regDt;

    //inherited
    public final NumberPath<Long> regUsr = _super.regUsr;

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> useType = createEnum("useType", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public QPermissionSetEntity(String variable) {
        super(PermissionSetEntity.class, forVariable(variable));
    }

    public QPermissionSetEntity(Path<? extends PermissionSetEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QPermissionSetEntity(PathMetadata metadata) {
        super(PermissionSetEntity.class, metadata);
    }

}

