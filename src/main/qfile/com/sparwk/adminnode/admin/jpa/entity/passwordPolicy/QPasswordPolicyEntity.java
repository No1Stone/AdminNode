package com.sparwk.adminnode.admin.jpa.entity.passwordPolicy;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QPasswordPolicyEntity is a Querydsl query type for PasswordPolicyEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QPasswordPolicyEntity extends EntityPathBase<PasswordPolicyEntity> {

    private static final long serialVersionUID = 343501748L;

    public static final QPasswordPolicyEntity passwordPolicyEntity = new QPasswordPolicyEntity("passwordPolicyEntity");

    public final StringPath lockoutEffectivePeriod = createString("lockoutEffectivePeriod");

    public final StringPath maximumInvalidLogin = createString("maximumInvalidLogin");

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modUsr = createNumber("modUsr", Long.class);

    public final StringPath passwordComplexityRequirement = createString("passwordComplexityRequirement");

    public final NumberPath<Integer> passwordExpire = createNumber("passwordExpire", Integer.class);

    public final NumberPath<Integer> passwordHistory = createNumber("passwordHistory", Integer.class);

    public final NumberPath<Integer> passwordMiNinimumLength = createNumber("passwordMiNinimumLength", Integer.class);

    public final NumberPath<Long> passwordPolicySeq = createNumber("passwordPolicySeq", Long.class);

    public QPasswordPolicyEntity(String variable) {
        super(PasswordPolicyEntity.class, forVariable(variable));
    }

    public QPasswordPolicyEntity(Path<? extends PasswordPolicyEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QPasswordPolicyEntity(PathMetadata metadata) {
        super(PasswordPolicyEntity.class, metadata);
    }

}

