package com.sparwk.adminnode.admin.jpa.entity.sns;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QSnsEntity is a Querydsl query type for SnsEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QSnsEntity extends EntityPathBase<SnsEntity> {

    private static final long serialVersionUID = 144258216L;

    public static final QSnsEntity snsEntity = new QSnsEntity("snsEntity");

    public final com.sparwk.adminnode.admin.jpa.entity.QBaseEntity _super = new com.sparwk.adminnode.admin.jpa.entity.QBaseEntity(this);

    //inherited
    public final DateTimePath<java.time.LocalDateTime> modDt = _super.modDt;

    //inherited
    public final NumberPath<Long> modUsr = _super.modUsr;

    //inherited
    public final DateTimePath<java.time.LocalDateTime> regDt = _super.regDt;

    //inherited
    public final NumberPath<Long> regUsr = _super.regUsr;

    public final StringPath snsCd = createString("snsCd");

    public final StringPath snsIconUrl = createString("snsIconUrl");

    public final StringPath snsName = createString("snsName");

    public final NumberPath<Long> snsSeq = createNumber("snsSeq", Long.class);

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> useType = createEnum("useType", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public QSnsEntity(String variable) {
        super(SnsEntity.class, forVariable(variable));
    }

    public QSnsEntity(Path<? extends SnsEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QSnsEntity(PathMetadata metadata) {
        super(SnsEntity.class, metadata);
    }

}

