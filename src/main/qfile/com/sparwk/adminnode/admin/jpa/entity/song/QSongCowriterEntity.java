package com.sparwk.adminnode.admin.jpa.entity.song;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QSongCowriterEntity is a Querydsl query type for SongCowriterEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QSongCowriterEntity extends EntityPathBase<SongCowriterEntity> {

    private static final long serialVersionUID = -1644850893L;

    public static final QSongCowriterEntity songCowriterEntity = new QSongCowriterEntity("songCowriterEntity");

    public final com.sparwk.adminnode.admin.jpa.entity.QBaseEntity _super = new com.sparwk.adminnode.admin.jpa.entity.QBaseEntity(this);

    public final DateTimePath<java.time.LocalDateTime> acceptDt = createDateTime("acceptDt", java.time.LocalDateTime.class);

    public final StringPath acceptYn = createString("acceptYn");

    public final StringPath copyrightControlYn = createString("copyrightControlYn");

    //inherited
    public final DateTimePath<java.time.LocalDateTime> modDt = _super.modDt;

    //inherited
    public final NumberPath<Long> modUsr = _super.modUsr;

    public final NumberPath<Long> opProfileId = createNumber("opProfileId", Long.class);

    public final StringPath personIdNumber = createString("personIdNumber");

    public final StringPath personIdYype = createString("personIdYype");

    public final NumberPath<Long> profileId = createNumber("profileId", Long.class);

    public final NumberPath<Long> proProfileId = createNumber("proProfileId", Long.class);

    public final NumberPath<Float> rateShare = createNumber("rateShare", Float.class);

    public final StringPath rateShareComt = createString("rateShareComt");

    //inherited
    public final DateTimePath<java.time.LocalDateTime> regDt = _super.regDt;

    //inherited
    public final NumberPath<Long> regUsr = _super.regUsr;

    public final NumberPath<Long> songCowriterSeq = createNumber("songCowriterSeq", Long.class);

    public final NumberPath<Long> songId = createNumber("songId", Long.class);

    public QSongCowriterEntity(String variable) {
        super(SongCowriterEntity.class, forVariable(variable));
    }

    public QSongCowriterEntity(Path<? extends SongCowriterEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QSongCowriterEntity(PathMetadata metadata) {
        super(SongCowriterEntity.class, metadata);
    }

}

