package com.sparwk.adminnode.admin.jpa.entity.song;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QSongFileEntity is a Querydsl query type for SongFileEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QSongFileEntity extends EntityPathBase<SongFileEntity> {

    private static final long serialVersionUID = 764042640L;

    public static final QSongFileEntity songFileEntity = new QSongFileEntity("songFileEntity");

    public final com.sparwk.adminnode.admin.jpa.entity.QBaseEntity _super = new com.sparwk.adminnode.admin.jpa.entity.QBaseEntity(this);

    public final StringPath bitDepth = createString("bitDepth");

    public final NumberPath<Integer> bpm = createNumber("bpm", Integer.class);

    public final StringPath containSampleYn = createString("containSampleYn");

    //inherited
    public final DateTimePath<java.time.LocalDateTime> modDt = _super.modDt;

    //inherited
    public final NumberPath<Long> modUsr = _super.modUsr;

    //inherited
    public final DateTimePath<java.time.LocalDateTime> regDt = _super.regDt;

    //inherited
    public final NumberPath<Long> regUsr = _super.regUsr;

    public final StringPath samplingRate = createString("samplingRate");

    public final StringPath songFileComt = createString("songFileComt");

    public final StringPath songFileName = createString("songFileName");

    public final StringPath songFilePath = createString("songFilePath");

    public final NumberPath<Long> songFileSeq = createNumber("songFileSeq", Long.class);

    public final NumberPath<Integer> songFileSize = createNumber("songFileSize", Integer.class);

    public final NumberPath<Long> songId = createNumber("songId", Long.class);

    public final StringPath stereoType = createString("stereoType");

    public QSongFileEntity(String variable) {
        super(SongFileEntity.class, forVariable(variable));
    }

    public QSongFileEntity(Path<? extends SongFileEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QSongFileEntity(PathMetadata metadata) {
        super(SongFileEntity.class, metadata);
    }

}

