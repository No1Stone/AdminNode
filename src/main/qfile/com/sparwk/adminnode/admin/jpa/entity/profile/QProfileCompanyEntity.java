package com.sparwk.adminnode.admin.jpa.entity.profile;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QProfileCompanyEntity is a Querydsl query type for ProfileCompanyEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QProfileCompanyEntity extends EntityPathBase<ProfileCompanyEntity> {

    private static final long serialVersionUID = 2061460025L;

    public static final QProfileCompanyEntity profileCompanyEntity = new QProfileCompanyEntity("profileCompanyEntity");

    public final com.sparwk.adminnode.admin.jpa.entity.QBaseEntity _super = new com.sparwk.adminnode.admin.jpa.entity.QBaseEntity(this);

    public final NumberPath<Long> accntId = createNumber("accntId", Long.class);

    public final StringPath bio = createString("bio");

    public final StringPath comInfoAddress = createString("comInfoAddress");

    public final StringPath comInfoCountryCd = createString("comInfoCountryCd");

    public final StringPath comInfoEmail = createString("comInfoEmail");

    public final StringPath comInfoFound = createString("comInfoFound");

    public final StringPath comInfoLat = createString("comInfoLat");

    public final StringPath comInfoLon = createString("comInfoLon");

    public final StringPath comInfoOverview = createString("comInfoOverview");

    public final StringPath comInfoPhone = createString("comInfoPhone");

    public final StringPath comInfoWebsite = createString("comInfoWebsite");

    public final StringPath headline = createString("headline");

    public final StringPath ipiNumber = createString("ipiNumber");

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> ipiNumberVarifyYn = createEnum("ipiNumberVarifyYn", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public final StringPath mattermostId = createString("mattermostId");

    //inherited
    public final DateTimePath<java.time.LocalDateTime> modDt = _super.modDt;

    //inherited
    public final NumberPath<Long> modUsr = _super.modUsr;

    public final StringPath profileBgdImgUrl = createString("profileBgdImgUrl");

    public final StringPath profileCompanyName = createString("profileCompanyName");

    public final NumberPath<Long> profileId = createNumber("profileId", Long.class);

    public final StringPath profileImgUrl = createString("profileImgUrl");

    //inherited
    public final DateTimePath<java.time.LocalDateTime> regDt = _super.regDt;

    //inherited
    public final NumberPath<Long> regUsr = _super.regUsr;

    public final StringPath vatNumber = createString("vatNumber");

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> vatNumberVarifyYn = createEnum("vatNumberVarifyYn", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public QProfileCompanyEntity(String variable) {
        super(ProfileCompanyEntity.class, forVariable(variable));
    }

    public QProfileCompanyEntity(Path<? extends ProfileCompanyEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QProfileCompanyEntity(PathMetadata metadata) {
        super(ProfileCompanyEntity.class, metadata);
    }

}

