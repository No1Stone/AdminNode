package com.sparwk.adminnode.admin.jpa.entity.projects;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QProjectReferenceFile is a Querydsl query type for ProjectReferenceFile
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QProjectReferenceFile extends EntityPathBase<ProjectReferenceFile> {

    private static final long serialVersionUID = 2094246437L;

    public static final QProjectReferenceFile projectReferenceFile = new QProjectReferenceFile("projectReferenceFile");

    public final com.sparwk.adminnode.admin.jpa.entity.QBaseEntity _super = new com.sparwk.adminnode.admin.jpa.entity.QBaseEntity(this);

    public final NumberPath<Long> fileFefSeq = createNumber("fileFefSeq", Long.class);

    public final StringPath fileName = createString("fileName");

    public final StringPath filePath = createString("filePath");

    public final NumberPath<Integer> fileSize = createNumber("fileSize", Integer.class);

    //inherited
    public final DateTimePath<java.time.LocalDateTime> modDt = _super.modDt;

    //inherited
    public final NumberPath<Long> modUsr = _super.modUsr;

    public final NumberPath<Long> projId = createNumber("projId", Long.class);

    public final StringPath refUrl = createString("refUrl");

    //inherited
    public final DateTimePath<java.time.LocalDateTime> regDt = _super.regDt;

    //inherited
    public final NumberPath<Long> regUsr = _super.regUsr;

    public final StringPath uploadTypeCd = createString("uploadTypeCd");

    public QProjectReferenceFile(String variable) {
        super(ProjectReferenceFile.class, forVariable(variable));
    }

    public QProjectReferenceFile(Path<? extends ProjectReferenceFile> path) {
        super(path.getType(), path.getMetadata());
    }

    public QProjectReferenceFile(PathMetadata metadata) {
        super(ProjectReferenceFile.class, metadata);
    }

}

