package com.sparwk.adminnode.admin.jpa.entity.song;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QSongSplitsheetViewEntity is a Querydsl query type for SongSplitsheetViewEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QSongSplitsheetViewEntity extends EntityPathBase<SongSplitsheetViewEntity> {

    private static final long serialVersionUID = 847403806L;

    public static final QSongSplitsheetViewEntity songSplitsheetViewEntity = new QSongSplitsheetViewEntity("songSplitsheetViewEntity");

    public final StringPath caeInfo = createString("caeInfo");

    public final StringPath fullName = createString("fullName");

    public final StringPath ipiInfo = createString("ipiInfo");

    public final StringPath ipnInfo = createString("ipnInfo");

    public final StringPath isniInfo = createString("isniInfo");

    public final StringPath originalPublisher = createString("originalPublisher");

    public final StringPath pro = createString("pro");

    public final NumberPath<Long> profileId = createNumber("profileId", Long.class);

    public final StringPath rateShare = createString("rateShare");

    public final StringPath roleName = createString("roleName");

    public final NumberPath<Long> songId = createNumber("songId", Long.class);

    public final StringPath subPublisher = createString("subPublisher");

    public QSongSplitsheetViewEntity(String variable) {
        super(SongSplitsheetViewEntity.class, forVariable(variable));
    }

    public QSongSplitsheetViewEntity(Path<? extends SongSplitsheetViewEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QSongSplitsheetViewEntity(PathMetadata metadata) {
        super(SongSplitsheetViewEntity.class, metadata);
    }

}

