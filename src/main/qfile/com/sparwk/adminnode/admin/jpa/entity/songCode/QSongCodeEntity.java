package com.sparwk.adminnode.admin.jpa.entity.songCode;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QSongCodeEntity is a Querydsl query type for SongCodeEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QSongCodeEntity extends EntityPathBase<SongCodeEntity> {

    private static final long serialVersionUID = -929609580L;

    public static final QSongCodeEntity songCodeEntity = new QSongCodeEntity("songCodeEntity");

    public final com.sparwk.adminnode.admin.jpa.entity.QBaseEntity _super = new com.sparwk.adminnode.admin.jpa.entity.QBaseEntity(this);

    public final StringPath code = createString("code");

    //inherited
    public final DateTimePath<java.time.LocalDateTime> modDt = _super.modDt;

    //inherited
    public final NumberPath<Long> modUsr = _super.modUsr;

    //inherited
    public final DateTimePath<java.time.LocalDateTime> regDt = _super.regDt;

    //inherited
    public final NumberPath<Long> regUsr = _super.regUsr;

    public final NumberPath<Long> songCodeSeq = createNumber("songCodeSeq", Long.class);

    public final ListPath<SongDetailCodeEntity, QSongDetailCodeEntity> songDetailCodeEntity = this.<SongDetailCodeEntity, QSongDetailCodeEntity>createList("songDetailCodeEntity", SongDetailCodeEntity.class, QSongDetailCodeEntity.class, PathInits.DIRECT2);

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> useType = createEnum("useType", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public final StringPath val = createString("val");

    public QSongCodeEntity(String variable) {
        super(SongCodeEntity.class, forVariable(variable));
    }

    public QSongCodeEntity(Path<? extends SongCodeEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QSongCodeEntity(PathMetadata metadata) {
        super(SongCodeEntity.class, metadata);
    }

}

