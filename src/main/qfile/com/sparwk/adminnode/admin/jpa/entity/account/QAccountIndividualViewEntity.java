package com.sparwk.adminnode.admin.jpa.entity.account;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QAccountIndividualViewEntity is a Querydsl query type for AccountIndividualViewEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QAccountIndividualViewEntity extends EntityPathBase<AccountIndividualViewEntity> {

    private static final long serialVersionUID = -1267341872L;

    public static final QAccountIndividualViewEntity accountIndividualViewEntity = new QAccountIndividualViewEntity("accountIndividualViewEntity");

    public final StringPath accntEmail = createString("accntEmail");

    public final NumberPath<Long> accntId = createNumber("accntId", Long.class);

    public final StringPath companyName = createString("companyName");

    public final StringPath fullName = createString("fullName");

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> ipiVerifyYn = createEnum("ipiVerifyYn", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public final StringPath passportFirstName = createString("passportFirstName");

    public final StringPath passportLastName = createString("passportLastName");

    public final StringPath passportMiddleName = createString("passportMiddleName");

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> passportVerifyYn = createEnum("passportVerifyYn", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public final NumberPath<Long> profileId = createNumber("profileId", Long.class);

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final StringPath rolesName = createString("rolesName");

    public QAccountIndividualViewEntity(String variable) {
        super(AccountIndividualViewEntity.class, forVariable(variable));
    }

    public QAccountIndividualViewEntity(Path<? extends AccountIndividualViewEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QAccountIndividualViewEntity(PathMetadata metadata) {
        super(AccountIndividualViewEntity.class, metadata);
    }

}

