package com.sparwk.adminnode.admin.jpa.entity.role;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QRoleDetailCodeEntity is a Querydsl query type for RoleDetailCodeEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QRoleDetailCodeEntity extends EntityPathBase<RoleDetailCodeEntity> {

    private static final long serialVersionUID = 878421074L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QRoleDetailCodeEntity roleDetailCodeEntity = new QRoleDetailCodeEntity("roleDetailCodeEntity");

    public final com.sparwk.adminnode.admin.jpa.entity.QBaseEntity _super = new com.sparwk.adminnode.admin.jpa.entity.QBaseEntity(this);

    public final StringPath abbeviation = createString("abbeviation");

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> creditRoleYn = createEnum("creditRoleYn", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public final StringPath customCode = createString("customCode");

    public final StringPath dcode = createString("dcode");

    public final StringPath description = createString("description");

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.FormatTypeEnum> formatType = createEnum("formatType", com.sparwk.adminnode.admin.jpa.entity.FormatTypeEnum.class);

    public final NumberPath<Long> hit = createNumber("hit", Long.class);

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> matchingRoleYn = createEnum("matchingRoleYn", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    //inherited
    public final DateTimePath<java.time.LocalDateTime> modDt = _super.modDt;

    //inherited
    public final NumberPath<Long> modUsr = _super.modUsr;

    public final StringPath pcode = createString("pcode");

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> popularType = createEnum("popularType", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    //inherited
    public final DateTimePath<java.time.LocalDateTime> regDt = _super.regDt;

    //inherited
    public final NumberPath<Long> regUsr = _super.regUsr;

    public final StringPath role = createString("role");

    public final QRoleCodeEntity roleCodeEntity;

    public final NumberPath<Long> roleDetailCodeSeq = createNumber("roleDetailCodeSeq", Long.class);

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> splitSheetRoleYn = createEnum("splitSheetRoleYn", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> useType = createEnum("useType", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public QRoleDetailCodeEntity(String variable) {
        this(RoleDetailCodeEntity.class, forVariable(variable), INITS);
    }

    public QRoleDetailCodeEntity(Path<? extends RoleDetailCodeEntity> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QRoleDetailCodeEntity(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QRoleDetailCodeEntity(PathMetadata metadata, PathInits inits) {
        this(RoleDetailCodeEntity.class, metadata, inits);
    }

    public QRoleDetailCodeEntity(Class<? extends RoleDetailCodeEntity> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.roleCodeEntity = inits.isInitialized("roleCodeEntity") ? new QRoleCodeEntity(forProperty("roleCodeEntity")) : null;
    }

}

