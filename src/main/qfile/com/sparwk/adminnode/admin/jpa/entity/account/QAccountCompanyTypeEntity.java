package com.sparwk.adminnode.admin.jpa.entity.account;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QAccountCompanyTypeEntity is a Querydsl query type for AccountCompanyTypeEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QAccountCompanyTypeEntity extends EntityPathBase<AccountCompanyTypeEntity> {

    private static final long serialVersionUID = 1476879115L;

    public static final QAccountCompanyTypeEntity accountCompanyTypeEntity = new QAccountCompanyTypeEntity("accountCompanyTypeEntity");

    public final NumberPath<Long> accntId = createNumber("accntId", Long.class);

    public final StringPath companyCd = createString("companyCd");

    public final StringPath companyLicenseFileName = createString("companyLicenseFileName");

    public final StringPath companyLicenseFileUrl = createString("companyLicenseFileUrl");

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> companyLicenseVerifyYn = createEnum("companyLicenseVerifyYn", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public QAccountCompanyTypeEntity(String variable) {
        super(AccountCompanyTypeEntity.class, forVariable(variable));
    }

    public QAccountCompanyTypeEntity(Path<? extends AccountCompanyTypeEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QAccountCompanyTypeEntity(PathMetadata metadata) {
        super(AccountCompanyTypeEntity.class, metadata);
    }

}

