package com.sparwk.adminnode.admin.jpa.entity.projects;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QProjectMemb is a Querydsl query type for ProjectMemb
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QProjectMemb extends EntityPathBase<ProjectMemb> {

    private static final long serialVersionUID = 293226671L;

    public static final QProjectMemb projectMemb = new QProjectMemb("projectMemb");

    public final com.sparwk.adminnode.admin.jpa.entity.QBaseEntity _super = new com.sparwk.adminnode.admin.jpa.entity.QBaseEntity(this);

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> activeYn = createEnum("activeYn", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public final StringPath applyStat = createString("applyStat");

    public final DateTimePath<java.time.LocalDateTime> banDt = createDateTime("banDt", java.time.LocalDateTime.class);

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> banYn = createEnum("banYn", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> confirmYn = createEnum("confirmYn", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public final DateTimePath<java.time.LocalDateTime> invtDt = createDateTime("invtDt", java.time.LocalDateTime.class);

    public final StringPath invtStat = createString("invtStat");

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modUsr = createNumber("modUsr", Long.class);

    public final DateTimePath<java.time.LocalDateTime> patpEdt = createDateTime("patpEdt", java.time.LocalDateTime.class);

    public final DateTimePath<java.time.LocalDateTime> patpSdt = createDateTime("patpSdt", java.time.LocalDateTime.class);

    public final NumberPath<Long> profileId = createNumber("profileId", Long.class);

    public final NumberPath<Long> projId = createNumber("projId", Long.class);

    public final DateTimePath<java.time.LocalDateTime> quitDt = createDateTime("quitDt", java.time.LocalDateTime.class);

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> quitYn = createEnum("quitYn", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> regUsr = createNumber("regUsr", Long.class);

    public QProjectMemb(String variable) {
        super(ProjectMemb.class, forVariable(variable));
    }

    public QProjectMemb(Path<? extends ProjectMemb> path) {
        super(path.getType(), path.getMetadata());
    }

    public QProjectMemb(PathMetadata metadata) {
        super(ProjectMemb.class, metadata);
    }

}

