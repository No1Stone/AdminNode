package com.sparwk.adminnode.admin.jpa.entity.profile;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QProfilePositionEntity is a Querydsl query type for ProfilePositionEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QProfilePositionEntity extends EntityPathBase<ProfilePositionEntity> {

    private static final long serialVersionUID = -719081965L;

    public static final QProfilePositionEntity profilePositionEntity = new QProfilePositionEntity("profilePositionEntity");

    public final com.sparwk.adminnode.admin.jpa.entity.QBaseEntity _super = new com.sparwk.adminnode.admin.jpa.entity.QBaseEntity(this);

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> anrYn = createEnum("anrYn", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> artistYn = createEnum("artistYn", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public final NumberPath<Long> companyProfileId = createNumber("companyProfileId", Long.class);

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> companyVerifyYn = createEnum("companyVerifyYn", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> creatorYn = createEnum("creatorYn", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public final StringPath deptRoleInfo = createString("deptRoleInfo");

    //inherited
    public final DateTimePath<java.time.LocalDateTime> modDt = _super.modDt;

    //inherited
    public final NumberPath<Long> modUsr = _super.modUsr;

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> primaryYn = createEnum("primaryYn", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public final NumberPath<Long> profileId = createNumber("profileId", Long.class);

    public final NumberPath<Long> profilePositionSeq = createNumber("profilePositionSeq", Long.class);

    //inherited
    public final DateTimePath<java.time.LocalDateTime> regDt = _super.regDt;

    //inherited
    public final NumberPath<Long> regUsr = _super.regUsr;

    public QProfilePositionEntity(String variable) {
        super(ProfilePositionEntity.class, forVariable(variable));
    }

    public QProfilePositionEntity(Path<? extends ProfilePositionEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QProfilePositionEntity(PathMetadata metadata) {
        super(ProfilePositionEntity.class, metadata);
    }

}

