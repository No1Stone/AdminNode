package com.sparwk.adminnode.admin.jpa.entity.admin;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QAdminActiveLogEntity is a Querydsl query type for AdminActiveLogEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QAdminActiveLogEntity extends EntityPathBase<AdminActiveLogEntity> {

    private static final long serialVersionUID = 1175603342L;

    public static final QAdminActiveLogEntity adminActiveLogEntity = new QAdminActiveLogEntity("adminActiveLogEntity");

    public final StringPath activeMsg = createString("activeMsg");

    public final DateTimePath<java.time.LocalDateTime> activeTime = createDateTime("activeTime", java.time.LocalDateTime.class);

    public final StringPath activeType = createString("activeType");

    public final NumberPath<Long> adminActiveLogSeq = createNumber("adminActiveLogSeq", Long.class);

    public final NumberPath<Long> adminId = createNumber("adminId", Long.class);

    public final NumberPath<Long> contentsId = createNumber("contentsId", Long.class);

    public final StringPath menuName = createString("menuName");

    public QAdminActiveLogEntity(String variable) {
        super(AdminActiveLogEntity.class, forVariable(variable));
    }

    public QAdminActiveLogEntity(Path<? extends AdminActiveLogEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QAdminActiveLogEntity(PathMetadata metadata) {
        super(AdminActiveLogEntity.class, metadata);
    }

}

