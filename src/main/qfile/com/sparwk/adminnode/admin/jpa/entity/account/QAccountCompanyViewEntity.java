package com.sparwk.adminnode.admin.jpa.entity.account;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QAccountCompanyViewEntity is a Querydsl query type for AccountCompanyViewEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QAccountCompanyViewEntity extends EntityPathBase<AccountCompanyViewEntity> {

    private static final long serialVersionUID = 1037472310L;

    public static final QAccountCompanyViewEntity accountCompanyViewEntity = new QAccountCompanyViewEntity("accountCompanyViewEntity");

    public final StringPath accntEmail = createString("accntEmail");

    public final NumberPath<Long> accntId = createNumber("accntId", Long.class);

    public final StringPath bussinessLocation = createString("bussinessLocation");

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> companyLicenseVerifyYn = createEnum("companyLicenseVerifyYn", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public final StringPath companyName = createString("companyName");

    public final StringPath companyType = createString("companyType");

    public final StringPath ipiNumber = createString("ipiNumber");

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> ipiNumberVarifyYn = createEnum("ipiNumberVarifyYn", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public final NumberPath<Long> profileId = createNumber("profileId", Long.class);

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final StringPath vatNumber = createString("vatNumber");

    public QAccountCompanyViewEntity(String variable) {
        super(AccountCompanyViewEntity.class, forVariable(variable));
    }

    public QAccountCompanyViewEntity(Path<? extends AccountCompanyViewEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QAccountCompanyViewEntity(PathMetadata metadata) {
        super(AccountCompanyViewEntity.class, metadata);
    }

}

