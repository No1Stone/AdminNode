package com.sparwk.adminnode.admin.jpa.entity.board;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QBoardFaqEntity is a Querydsl query type for BoardFaqEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QBoardFaqEntity extends EntityPathBase<BoardFaqEntity> {

    private static final long serialVersionUID = 1137666776L;

    public static final QBoardFaqEntity boardFaqEntity = new QBoardFaqEntity("boardFaqEntity");

    public final com.sparwk.adminnode.admin.jpa.entity.QBaseEntity _super = new com.sparwk.adminnode.admin.jpa.entity.QBaseEntity(this);

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> attachYn = createEnum("attachYn", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public final StringPath cateCd = createString("cateCd");

    public final StringPath content = createString("content");

    public final NumberPath<Long> faqId = createNumber("faqId", Long.class);

    public final NumberPath<Long> hit = createNumber("hit", Long.class);

    //inherited
    public final DateTimePath<java.time.LocalDateTime> modDt = _super.modDt;

    //inherited
    public final NumberPath<Long> modUsr = _super.modUsr;

    //inherited
    public final DateTimePath<java.time.LocalDateTime> regDt = _super.regDt;

    //inherited
    public final NumberPath<Long> regUsr = _super.regUsr;

    public final StringPath title = createString("title");

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> useYn = createEnum("useYn", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public QBoardFaqEntity(String variable) {
        super(BoardFaqEntity.class, forVariable(variable));
    }

    public QBoardFaqEntity(Path<? extends BoardFaqEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QBoardFaqEntity(PathMetadata metadata) {
        super(BoardFaqEntity.class, metadata);
    }

}

