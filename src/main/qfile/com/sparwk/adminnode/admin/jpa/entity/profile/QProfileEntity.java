package com.sparwk.adminnode.admin.jpa.entity.profile;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QProfileEntity is a Querydsl query type for ProfileEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QProfileEntity extends EntityPathBase<ProfileEntity> {

    private static final long serialVersionUID = -742587766L;

    public static final QProfileEntity profileEntity = new QProfileEntity("profileEntity");

    public final com.sparwk.adminnode.admin.jpa.entity.QBaseEntity _super = new com.sparwk.adminnode.admin.jpa.entity.QBaseEntity(this);

    public final NumberPath<Long> accntId = createNumber("accntId", Long.class);

    public final StringPath accomplishmentsInfo = createString("accomplishmentsInfo");

    public final StringPath bio = createString("bio");

    public final StringPath bthDay = createString("bthDay");

    public final StringPath bthMonth = createString("bthMonth");

    public final StringPath bthYear = createString("bthYear");

    public final StringPath caeInfo = createString("caeInfo");

    public final StringPath currentCityCountryCd = createString("currentCityCountryCd");

    public final StringPath currentCityNm = createString("currentCityNm");

    public final StringPath fullName = createString("fullName");

    public final StringPath headline = createString("headline");

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> hireMeYn = createEnum("hireMeYn", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public final StringPath homeTownCountryCd = createString("homeTownCountryCd");

    public final StringPath homeTownNm = createString("homeTownNm");

    public final StringPath ipiInfo = createString("ipiInfo");

    public final StringPath ipnInfo = createString("ipnInfo");

    public final StringPath isniInfo = createString("isniInfo");

    public final StringPath mattermostId = createString("mattermostId");

    //inherited
    public final DateTimePath<java.time.LocalDateTime> modDt = _super.modDt;

    //inherited
    public final NumberPath<Long> modUsr = _super.modUsr;

    public final StringPath nroInfo = createString("nroInfo");

    public final StringPath nroTypeCd = createString("nroTypeCd");

    public final StringPath profileBgdImgUrl = createString("profileBgdImgUrl");

    public final ListPath<ProfileCareer, QProfileCareer> profileCareer = this.<ProfileCareer, QProfileCareer>createList("profileCareer", ProfileCareer.class, QProfileCareer.class, PathInits.DIRECT2);

    public final ListPath<ProfileEducation, QProfileEducation> profileEducation = this.<ProfileEducation, QProfileEducation>createList("profileEducation", ProfileEducation.class, QProfileEducation.class, PathInits.DIRECT2);

    public final ListPath<ProfileGenderEntity, QProfileGenderEntity> profileGender = this.<ProfileGenderEntity, QProfileGenderEntity>createList("profileGender", ProfileGenderEntity.class, QProfileGenderEntity.class, PathInits.DIRECT2);

    public final NumberPath<Long> profileId = createNumber("profileId", Long.class);

    public final StringPath profileImgUrl = createString("profileImgUrl");

    public final ListPath<ProfileInterestMeta, QProfileInterestMeta> profileInterestMeta = this.<ProfileInterestMeta, QProfileInterestMeta>createList("profileInterestMeta", ProfileInterestMeta.class, QProfileInterestMeta.class, PathInits.DIRECT2);

    public final ListPath<ProfileLanguageEntity, QProfileLanguageEntity> profileLanguageEntity = this.<ProfileLanguageEntity, QProfileLanguageEntity>createList("profileLanguageEntity", ProfileLanguageEntity.class, QProfileLanguageEntity.class, PathInits.DIRECT2);

    public final ListPath<ProfilePositionEntity, QProfilePositionEntity> profilePositionEntity = this.<ProfilePositionEntity, QProfilePositionEntity>createList("profilePositionEntity", ProfilePositionEntity.class, QProfilePositionEntity.class, PathInits.DIRECT2);

    public final ListPath<ProfileSkillEntity, QProfileSkillEntity> profileSkillEntity = this.<ProfileSkillEntity, QProfileSkillEntity>createList("profileSkillEntity", ProfileSkillEntity.class, QProfileSkillEntity.class, PathInits.DIRECT2);

    //inherited
    public final DateTimePath<java.time.LocalDateTime> regDt = _super.regDt;

    //inherited
    public final NumberPath<Long> regUsr = _super.regUsr;

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> stageNameYn = createEnum("stageNameYn", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> useYn = createEnum("useYn", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> verifyYn = createEnum("verifyYn", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public QProfileEntity(String variable) {
        super(ProfileEntity.class, forVariable(variable));
    }

    public QProfileEntity(Path<? extends ProfileEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QProfileEntity(PathMetadata metadata) {
        super(ProfileEntity.class, metadata);
    }

}

