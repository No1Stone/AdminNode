package com.sparwk.adminnode.admin.jpa.entity.board;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QBoardSuccessEntity is a Querydsl query type for BoardSuccessEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QBoardSuccessEntity extends EntityPathBase<BoardSuccessEntity> {

    private static final long serialVersionUID = -89341307L;

    public static final QBoardSuccessEntity boardSuccessEntity = new QBoardSuccessEntity("boardSuccessEntity");

    public final com.sparwk.adminnode.admin.jpa.entity.QBaseEntity _super = new com.sparwk.adminnode.admin.jpa.entity.QBaseEntity(this);

    public final StringPath content = createString("content");

    public final StringPath genre = createString("genre");

    public final StringPath imageUrl = createString("imageUrl");

    //inherited
    public final DateTimePath<java.time.LocalDateTime> modDt = _super.modDt;

    //inherited
    public final NumberPath<Long> modUsr = _super.modUsr;

    public final StringPath name = createString("name");

    //inherited
    public final DateTimePath<java.time.LocalDateTime> regDt = _super.regDt;

    //inherited
    public final NumberPath<Long> regUsr = _super.regUsr;

    public final StringPath role = createString("role");

    public final NumberPath<Long> successId = createNumber("successId", Long.class);

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> useType = createEnum("useType", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public QBoardSuccessEntity(String variable) {
        super(BoardSuccessEntity.class, forVariable(variable));
    }

    public QBoardSuccessEntity(Path<? extends BoardSuccessEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QBoardSuccessEntity(PathMetadata metadata) {
        super(BoardSuccessEntity.class, metadata);
    }

}

