package com.sparwk.adminnode.admin.jpa.entity.song;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QSongEvaluationEntity is a Querydsl query type for SongEvaluationEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QSongEvaluationEntity extends EntityPathBase<SongEvaluationEntity> {

    private static final long serialVersionUID = 1394364048L;

    public static final QSongEvaluationEntity songEvaluationEntity = new QSongEvaluationEntity("songEvaluationEntity");

    public final com.sparwk.adminnode.admin.jpa.entity.QBaseEntity _super = new com.sparwk.adminnode.admin.jpa.entity.QBaseEntity(this);

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> confirmYn = createEnum("confirmYn", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> delYn = createEnum("delYn", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public final DateTimePath<java.time.LocalDateTime> evalEndDt = createDateTime("evalEndDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> evalOwnerId = createNumber("evalOwnerId", Long.class);

    public final NumberPath<Long> evalSeq = createNumber("evalSeq", Long.class);

    public final DateTimePath<java.time.LocalDateTime> evalStartDt = createDateTime("evalStartDt", java.time.LocalDateTime.class);

    public final StringPath evalStatus = createString("evalStatus");

    public final StringPath evalTemplCd = createString("evalTemplCd");

    public final DateTimePath<java.time.LocalDateTime> holdExpiredDt = createDateTime("holdExpiredDt", java.time.LocalDateTime.class);

    public final DateTimePath<java.time.LocalDateTime> holdStartDt = createDateTime("holdStartDt", java.time.LocalDateTime.class);

    //inherited
    public final DateTimePath<java.time.LocalDateTime> modDt = _super.modDt;

    //inherited
    public final NumberPath<Long> modUsr = _super.modUsr;

    public final NumberPath<Long> pitchlistAnrSeq = createNumber("pitchlistAnrSeq", Long.class);

    public final NumberPath<Long> pitchlistId = createNumber("pitchlistId", Long.class);

    //inherited
    public final DateTimePath<java.time.LocalDateTime> regDt = _super.regDt;

    //inherited
    public final NumberPath<Long> regUsr = _super.regUsr;

    public final NumberPath<Integer> shareUrl = createNumber("shareUrl", Integer.class);

    public final NumberPath<Long> songId = createNumber("songId", Long.class);

    public QSongEvaluationEntity(String variable) {
        super(SongEvaluationEntity.class, forVariable(variable));
    }

    public QSongEvaluationEntity(Path<? extends SongEvaluationEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QSongEvaluationEntity(PathMetadata metadata) {
        super(SongEvaluationEntity.class, metadata);
    }

}

