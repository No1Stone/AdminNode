package com.sparwk.adminnode.admin.jpa.entity.profile;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QProfileGroupConcatEntity is a Querydsl query type for ProfileGroupConcatEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QProfileGroupConcatEntity extends EntityPathBase<ProfileGroupConcatEntity> {

    private static final long serialVersionUID = 1044000559L;

    public static final QProfileGroupConcatEntity profileGroupConcatEntity = new QProfileGroupConcatEntity("profileGroupConcatEntity");

    public final com.sparwk.adminnode.admin.jpa.entity.QBaseEntity _super = new com.sparwk.adminnode.admin.jpa.entity.QBaseEntity(this);

    public final StringPath contctEmail = createString("contctEmail");

    public final StringPath contctFirstName = createString("contctFirstName");

    public final StringPath contctLastName = createString("contctLastName");

    public final StringPath contctMidleName = createString("contctMidleName");

    public final StringPath contctPhoneNumber = createString("contctPhoneNumber");

    public final StringPath countryCd = createString("countryCd");

    //inherited
    public final DateTimePath<java.time.LocalDateTime> modDt = _super.modDt;

    //inherited
    public final NumberPath<Long> modUsr = _super.modUsr;

    public final StringPath profileContactDescription = createString("profileContactDescription");

    public final StringPath profileContactImgUrl = createString("profileContactImgUrl");

    public final NumberPath<Long> profileId = createNumber("profileId", Long.class);

    //inherited
    public final DateTimePath<java.time.LocalDateTime> regDt = _super.regDt;

    //inherited
    public final NumberPath<Long> regUsr = _super.regUsr;

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> verifyPhoneYn = createEnum("verifyPhoneYn", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public QProfileGroupConcatEntity(String variable) {
        super(ProfileGroupConcatEntity.class, forVariable(variable));
    }

    public QProfileGroupConcatEntity(Path<? extends ProfileGroupConcatEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QProfileGroupConcatEntity(PathMetadata metadata) {
        super(ProfileGroupConcatEntity.class, metadata);
    }

}

