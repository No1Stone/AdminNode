package com.sparwk.adminnode.admin.jpa.entity.song;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QSongCreditsViewEntity is a Querydsl query type for SongCreditsViewEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QSongCreditsViewEntity extends EntityPathBase<SongCreditsViewEntity> {

    private static final long serialVersionUID = -584021775L;

    public static final QSongCreditsViewEntity songCreditsViewEntity = new QSongCreditsViewEntity("songCreditsViewEntity");

    public final StringPath caeInfo = createString("caeInfo");

    public final StringPath fullName = createString("fullName");

    public final StringPath ipiInfo = createString("ipiInfo");

    public final StringPath ipnInfo = createString("ipnInfo");

    public final StringPath isniInfo = createString("isniInfo");

    public final StringPath nroInfo = createString("nroInfo");

    public final StringPath nroTypeName = createString("nroTypeName");

    public final NumberPath<Long> profileId = createNumber("profileId", Long.class);

    public final StringPath roleName = createString("roleName");

    public final NumberPath<Long> songId = createNumber("songId", Long.class);

    public QSongCreditsViewEntity(String variable) {
        super(SongCreditsViewEntity.class, forVariable(variable));
    }

    public QSongCreditsViewEntity(Path<? extends SongCreditsViewEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QSongCreditsViewEntity(PathMetadata metadata) {
        super(SongCreditsViewEntity.class, metadata);
    }

}

