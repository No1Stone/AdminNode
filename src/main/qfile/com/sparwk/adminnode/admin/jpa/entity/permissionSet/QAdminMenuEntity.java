package com.sparwk.adminnode.admin.jpa.entity.permissionSet;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QAdminMenuEntity is a Querydsl query type for AdminMenuEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QAdminMenuEntity extends EntityPathBase<AdminMenuEntity> {

    private static final long serialVersionUID = 81982233L;

    public static final QAdminMenuEntity adminMenuEntity = new QAdminMenuEntity("adminMenuEntity");

    public final com.sparwk.adminnode.admin.jpa.entity.QBaseEntity _super = new com.sparwk.adminnode.admin.jpa.entity.QBaseEntity(this);

    public final StringPath description = createString("description");

    public final NumberPath<Integer> menuDepth = createNumber("menuDepth", Integer.class);

    public final StringPath menuLabel = createString("menuLabel");

    public final NumberPath<Long> menuSeq = createNumber("menuSeq", Long.class);

    //inherited
    public final DateTimePath<java.time.LocalDateTime> modDt = _super.modDt;

    //inherited
    public final NumberPath<Long> modUsr = _super.modUsr;

    //inherited
    public final DateTimePath<java.time.LocalDateTime> regDt = _super.regDt;

    //inherited
    public final NumberPath<Long> regUsr = _super.regUsr;

    public final NumberPath<Integer> sortIndex = createNumber("sortIndex", Integer.class);

    public final NumberPath<Long> uppMenuSeq = createNumber("uppMenuSeq", Long.class);

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> useType = createEnum("useType", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public QAdminMenuEntity(String variable) {
        super(AdminMenuEntity.class, forVariable(variable));
    }

    public QAdminMenuEntity(Path<? extends AdminMenuEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QAdminMenuEntity(PathMetadata metadata) {
        super(AdminMenuEntity.class, metadata);
    }

}

