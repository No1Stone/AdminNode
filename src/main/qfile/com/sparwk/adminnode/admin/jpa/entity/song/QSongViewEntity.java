package com.sparwk.adminnode.admin.jpa.entity.song;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QSongViewEntity is a Querydsl query type for SongViewEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QSongViewEntity extends EntityPathBase<SongViewEntity> {

    private static final long serialVersionUID = 1395220473L;

    public static final QSongViewEntity songViewEntity = new QSongViewEntity("songViewEntity");

    public final StringPath avatarFileUrl = createString("avatarFileUrl");

    public final NumberPath<Long> bpm = createNumber("bpm", Long.class);

    public final StringPath duration = createString("duration");

    public final StringPath isrc = createString("isrc");

    public final StringPath keySignature = createString("keySignature");

    public final StringPath lyrics = createString("lyrics");

    public final StringPath lyricsComt = createString("lyricsComt");

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modUsr = createNumber("modUsr", Long.class);

    public final StringPath modUsrName = createString("modUsrName");

    public final StringPath projTitle = createString("projTitle");

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> regUsr = createNumber("regUsr", Long.class);

    public final StringPath regUsrName = createString("regUsrName");

    public final StringPath songCowritersName = createString("songCowritersName");

    public final StringPath songFilePath = createString("songFilePath");

    public final StringPath songGenreCd = createString("songGenreCd");

    public final StringPath songGenreName = createString("songGenreName");

    public final NumberPath<Long> songId = createNumber("songId", Long.class);

    public final StringPath songLangCd = createString("songLangCd");

    public final StringPath songLangCdName = createString("songLangCdName");

    public final StringPath songMoodCd = createString("songMoodCd");

    public final StringPath songMoodName = createString("songMoodName");

    public final NumberPath<Long> songOwner = createNumber("songOwner", Long.class);

    public final StringPath songOwnerName = createString("songOwnerName");

    public final StringPath songStatusCd = createString("songStatusCd");

    public final StringPath songStatusCdName = createString("songStatusCdName");

    public final StringPath songSubgenreCd = createString("songSubgenreCd");

    public final StringPath songSubgenreName = createString("songSubgenreName");

    public final StringPath songThemeCd = createString("songThemeCd");

    public final StringPath songThemeName = createString("songThemeName");

    public final StringPath songTitle = createString("songTitle");

    public final StringPath songVersionCd = createString("songVersionCd");

    public final StringPath songVersionCdName = createString("songVersionCdName");

    public final StringPath tempo = createString("tempo");

    public final StringPath timeSignature = createString("timeSignature");

    public QSongViewEntity(String variable) {
        super(SongViewEntity.class, forVariable(variable));
    }

    public QSongViewEntity(Path<? extends SongViewEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QSongViewEntity(PathMetadata metadata) {
        super(SongViewEntity.class, metadata);
    }

}

