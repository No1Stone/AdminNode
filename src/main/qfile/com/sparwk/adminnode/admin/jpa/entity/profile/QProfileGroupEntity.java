package com.sparwk.adminnode.admin.jpa.entity.profile;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QProfileGroupEntity is a Querydsl query type for ProfileGroupEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QProfileGroupEntity extends EntityPathBase<ProfileGroupEntity> {

    private static final long serialVersionUID = 497523963L;

    public static final QProfileGroupEntity profileGroupEntity = new QProfileGroupEntity("profileGroupEntity");

    public final com.sparwk.adminnode.admin.jpa.entity.QBaseEntity _super = new com.sparwk.adminnode.admin.jpa.entity.QBaseEntity(this);

    public final NumberPath<Long> accntId = createNumber("accntId", Long.class);

    public final StringPath headline = createString("headline");

    public final StringPath mattermostId = createString("mattermostId");

    //inherited
    public final DateTimePath<java.time.LocalDateTime> modDt = _super.modDt;

    //inherited
    public final NumberPath<Long> modUsr = _super.modUsr;

    public final StringPath overview = createString("overview");

    public final StringPath profileBgdImgUrl = createString("profileBgdImgUrl");

    public final StringPath profileGroupName = createString("profileGroupName");

    public final NumberPath<Long> profileId = createNumber("profileId", Long.class);

    public final StringPath profileImgUrl = createString("profileImgUrl");

    //inherited
    public final DateTimePath<java.time.LocalDateTime> regDt = _super.regDt;

    //inherited
    public final NumberPath<Long> regUsr = _super.regUsr;

    public final StringPath website = createString("website");

    public QProfileGroupEntity(String variable) {
        super(ProfileGroupEntity.class, forVariable(variable));
    }

    public QProfileGroupEntity(Path<? extends ProfileGroupEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QProfileGroupEntity(PathMetadata metadata) {
        super(ProfileGroupEntity.class, metadata);
    }

}

