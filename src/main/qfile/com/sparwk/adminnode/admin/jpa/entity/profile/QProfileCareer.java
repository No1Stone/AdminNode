package com.sparwk.adminnode.admin.jpa.entity.profile;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QProfileCareer is a Querydsl query type for ProfileCareer
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QProfileCareer extends EntityPathBase<ProfileCareer> {

    private static final long serialVersionUID = -811915739L;

    public static final QProfileCareer profileCareer = new QProfileCareer("profileCareer");

    public final com.sparwk.adminnode.admin.jpa.entity.QBaseEntity _super = new com.sparwk.adminnode.admin.jpa.entity.QBaseEntity(this);

    public final DateTimePath<java.time.LocalDateTime> careerEndDt = createDateTime("careerEndDt", java.time.LocalDateTime.class);

    public final StringPath careerOrganizationNm = createString("careerOrganizationNm");

    public final NumberPath<Long> careerSeq = createNumber("careerSeq", Long.class);

    public final DateTimePath<java.time.LocalDateTime> careerStartDt = createDateTime("careerStartDt", java.time.LocalDateTime.class);

    public final StringPath deptRoleNm = createString("deptRoleNm");

    public final StringPath locationCityCountryCd = createString("locationCityCountryCd");

    public final StringPath locationCityNm = createString("locationCityNm");

    //inherited
    public final DateTimePath<java.time.LocalDateTime> modDt = _super.modDt;

    //inherited
    public final NumberPath<Long> modUsr = _super.modUsr;

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> presentYn = createEnum("presentYn", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public final NumberPath<Long> profileId = createNumber("profileId", Long.class);

    //inherited
    public final DateTimePath<java.time.LocalDateTime> regDt = _super.regDt;

    //inherited
    public final NumberPath<Long> regUsr = _super.regUsr;

    public QProfileCareer(String variable) {
        super(ProfileCareer.class, forVariable(variable));
    }

    public QProfileCareer(Path<? extends ProfileCareer> path) {
        super(path.getType(), path.getMetadata());
    }

    public QProfileCareer(PathMetadata metadata) {
        super(ProfileCareer.class, metadata);
    }

}

