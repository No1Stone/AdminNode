package com.sparwk.adminnode.admin.jpa.entity.profile;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QProfileCompanyConcatEntity is a Querydsl query type for ProfileCompanyConcatEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QProfileCompanyConcatEntity extends EntityPathBase<ProfileCompanyConcatEntity> {

    private static final long serialVersionUID = -1853059603L;

    public static final QProfileCompanyConcatEntity profileCompanyConcatEntity = new QProfileCompanyConcatEntity("profileCompanyConcatEntity");

    public final com.sparwk.adminnode.admin.jpa.entity.QBaseEntity _super = new com.sparwk.adminnode.admin.jpa.entity.QBaseEntity(this);

    public final StringPath contctEmail = createString("contctEmail");

    public final StringPath contctFirstName = createString("contctFirstName");

    public final StringPath contctLastName = createString("contctLastName");

    public final StringPath contctMidleName = createString("contctMidleName");

    public final StringPath contctPhoneNumber = createString("contctPhoneNumber");

    public final StringPath countryCd = createString("countryCd");

    //inherited
    public final DateTimePath<java.time.LocalDateTime> modDt = _super.modDt;

    //inherited
    public final NumberPath<Long> modUsr = _super.modUsr;

    public final StringPath profileContactDescription = createString("profileContactDescription");

    public final StringPath profileContactImgUrl = createString("profileContactImgUrl");

    public final NumberPath<Long> profileId = createNumber("profileId", Long.class);

    //inherited
    public final DateTimePath<java.time.LocalDateTime> regDt = _super.regDt;

    //inherited
    public final NumberPath<Long> regUsr = _super.regUsr;

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> verifyPhoneYn = createEnum("verifyPhoneYn", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public QProfileCompanyConcatEntity(String variable) {
        super(ProfileCompanyConcatEntity.class, forVariable(variable));
    }

    public QProfileCompanyConcatEntity(Path<? extends ProfileCompanyConcatEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QProfileCompanyConcatEntity(PathMetadata metadata) {
        super(ProfileCompanyConcatEntity.class, metadata);
    }

}

