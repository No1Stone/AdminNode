package com.sparwk.adminnode.admin.jpa.entity.song;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QSongLyricsEntity is a Querydsl query type for SongLyricsEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QSongLyricsEntity extends EntityPathBase<SongLyricsEntity> {

    private static final long serialVersionUID = 1620940232L;

    public static final QSongLyricsEntity songLyricsEntity = new QSongLyricsEntity("songLyricsEntity");

    public final com.sparwk.adminnode.admin.jpa.entity.QBaseEntity _super = new com.sparwk.adminnode.admin.jpa.entity.QBaseEntity(this);

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> explicitContentYn = createEnum("explicitContentYn", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public final StringPath lyrics = createString("lyrics");

    public final StringPath lyricsComt = createString("lyricsComt");

    //inherited
    public final DateTimePath<java.time.LocalDateTime> modDt = _super.modDt;

    //inherited
    public final NumberPath<Long> modUsr = _super.modUsr;

    //inherited
    public final DateTimePath<java.time.LocalDateTime> regDt = _super.regDt;

    //inherited
    public final NumberPath<Long> regUsr = _super.regUsr;

    public final NumberPath<Long> songFileSeq = createNumber("songFileSeq", Long.class);

    public final NumberPath<Long> songId = createNumber("songId", Long.class);

    public QSongLyricsEntity(String variable) {
        super(SongLyricsEntity.class, forVariable(variable));
    }

    public QSongLyricsEntity(Path<? extends SongLyricsEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QSongLyricsEntity(PathMetadata metadata) {
        super(SongLyricsEntity.class, metadata);
    }

}

