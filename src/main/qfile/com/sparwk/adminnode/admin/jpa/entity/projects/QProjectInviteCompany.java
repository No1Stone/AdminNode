package com.sparwk.adminnode.admin.jpa.entity.projects;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QProjectInviteCompany is a Querydsl query type for ProjectInviteCompany
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QProjectInviteCompany extends EntityPathBase<ProjectInviteCompany> {

    private static final long serialVersionUID = -641533966L;

    public static final QProjectInviteCompany projectInviteCompany = new QProjectInviteCompany("projectInviteCompany");

    public final com.sparwk.adminnode.admin.jpa.entity.QBaseEntity _super = new com.sparwk.adminnode.admin.jpa.entity.QBaseEntity(this);

    public final NumberPath<Long> companyProfileId = createNumber("companyProfileId", Long.class);

    //inherited
    public final DateTimePath<java.time.LocalDateTime> modDt = _super.modDt;

    //inherited
    public final NumberPath<Long> modUsr = _super.modUsr;

    public final NumberPath<Long> projId = createNumber("projId", Long.class);

    //inherited
    public final DateTimePath<java.time.LocalDateTime> regDt = _super.regDt;

    //inherited
    public final NumberPath<Long> regUsr = _super.regUsr;

    public QProjectInviteCompany(String variable) {
        super(ProjectInviteCompany.class, forVariable(variable));
    }

    public QProjectInviteCompany(Path<? extends ProjectInviteCompany> path) {
        super(path.getType(), path.getMetadata());
    }

    public QProjectInviteCompany(PathMetadata metadata) {
        super(ProjectInviteCompany.class, metadata);
    }

}

