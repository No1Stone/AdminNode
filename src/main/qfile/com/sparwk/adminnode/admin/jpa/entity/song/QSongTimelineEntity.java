package com.sparwk.adminnode.admin.jpa.entity.song;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QSongTimelineEntity is a Querydsl query type for SongTimelineEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QSongTimelineEntity extends EntityPathBase<SongTimelineEntity> {

    private static final long serialVersionUID = -1623890923L;

    public static final QSongTimelineEntity songTimelineEntity = new QSongTimelineEntity("songTimelineEntity");

    public final com.sparwk.adminnode.admin.jpa.entity.QBaseEntity _super = new com.sparwk.adminnode.admin.jpa.entity.QBaseEntity(this);

    public final NumberPath<Long> componentUseId = createNumber("componentUseId", Long.class);

    public final StringPath componetCd = createString("componetCd");

    //inherited
    public final DateTimePath<java.time.LocalDateTime> modDt = _super.modDt;

    //inherited
    public final NumberPath<Long> modUsr = _super.modUsr;

    public final StringPath params = createString("params");

    //inherited
    public final DateTimePath<java.time.LocalDateTime> regDt = _super.regDt;

    //inherited
    public final NumberPath<Long> regUsr = _super.regUsr;

    public final StringPath resultMsg = createString("resultMsg");

    public final NumberPath<Long> songId = createNumber("songId", Long.class);

    public final NumberPath<Long> timelineSeq = createNumber("timelineSeq", Long.class);

    public final StringPath tlmCd = createString("tlmCd");

    public QSongTimelineEntity(String variable) {
        super(SongTimelineEntity.class, forVariable(variable));
    }

    public QSongTimelineEntity(Path<? extends SongTimelineEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QSongTimelineEntity(PathMetadata metadata) {
        super(SongTimelineEntity.class, metadata);
    }

}

