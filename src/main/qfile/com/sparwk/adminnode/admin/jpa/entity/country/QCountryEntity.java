package com.sparwk.adminnode.admin.jpa.entity.country;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QCountryEntity is a Querydsl query type for CountryEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QCountryEntity extends EntityPathBase<CountryEntity> {

    private static final long serialVersionUID = 295458276L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QCountryEntity countryEntity = new QCountryEntity("countryEntity");

    public final com.sparwk.adminnode.admin.jpa.entity.QBaseEntity _super = new com.sparwk.adminnode.admin.jpa.entity.QBaseEntity(this);

    public final StringPath continentCode = createString("continentCode");

    public final com.sparwk.adminnode.admin.jpa.entity.continent.QContinentEntity continentEntity;

    public final StringPath country = createString("country");

    public final StringPath countryCd = createString("countryCd");

    public final NumberPath<Long> countrySeq = createNumber("countrySeq", Long.class);

    public final StringPath description = createString("description");

    public final StringPath dial = createString("dial");

    public final StringPath iso2 = createString("iso2");

    public final StringPath iso3 = createString("iso3");

    //inherited
    public final DateTimePath<java.time.LocalDateTime> modDt = _super.modDt;

    //inherited
    public final NumberPath<Long> modUsr = _super.modUsr;

    public final StringPath nmr = createString("nmr");

    //inherited
    public final DateTimePath<java.time.LocalDateTime> regDt = _super.regDt;

    //inherited
    public final NumberPath<Long> regUsr = _super.regUsr;

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> useType = createEnum("useType", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public QCountryEntity(String variable) {
        this(CountryEntity.class, forVariable(variable), INITS);
    }

    public QCountryEntity(Path<? extends CountryEntity> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QCountryEntity(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QCountryEntity(PathMetadata metadata, PathInits inits) {
        this(CountryEntity.class, metadata, inits);
    }

    public QCountryEntity(Class<? extends CountryEntity> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.continentEntity = inits.isInitialized("continentEntity") ? new com.sparwk.adminnode.admin.jpa.entity.continent.QContinentEntity(forProperty("continentEntity")) : null;
    }

}

