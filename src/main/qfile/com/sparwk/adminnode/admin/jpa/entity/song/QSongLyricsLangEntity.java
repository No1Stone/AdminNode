package com.sparwk.adminnode.admin.jpa.entity.song;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QSongLyricsLangEntity is a Querydsl query type for SongLyricsLangEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QSongLyricsLangEntity extends EntityPathBase<SongLyricsLangEntity> {

    private static final long serialVersionUID = 95022806L;

    public static final QSongLyricsLangEntity songLyricsLangEntity = new QSongLyricsLangEntity("songLyricsLangEntity");

    public final com.sparwk.adminnode.admin.jpa.entity.QBaseEntity _super = new com.sparwk.adminnode.admin.jpa.entity.QBaseEntity(this);

    public final StringPath langCd = createString("langCd");

    //inherited
    public final DateTimePath<java.time.LocalDateTime> modDt = _super.modDt;

    //inherited
    public final NumberPath<Long> modUsr = _super.modUsr;

    //inherited
    public final DateTimePath<java.time.LocalDateTime> regDt = _super.regDt;

    //inherited
    public final NumberPath<Long> regUsr = _super.regUsr;

    public final NumberPath<Long> songId = createNumber("songId", Long.class);

    public QSongLyricsLangEntity(String variable) {
        super(SongLyricsLangEntity.class, forVariable(variable));
    }

    public QSongLyricsLangEntity(Path<? extends SongLyricsLangEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QSongLyricsLangEntity(PathMetadata metadata) {
        super(SongLyricsLangEntity.class, metadata);
    }

}

