package com.sparwk.adminnode.admin.jpa.entity.languagePack;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QLanguagePackViewEntity is a Querydsl query type for LanguagePackViewEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QLanguagePackViewEntity extends EntityPathBase<LanguagePackViewEntity> {

    private static final long serialVersionUID = 1704499833L;

    public static final QLanguagePackViewEntity languagePackViewEntity = new QLanguagePackViewEntity("languagePackViewEntity");

    public final com.sparwk.adminnode.admin.jpa.entity.QBaseEntity _super = new com.sparwk.adminnode.admin.jpa.entity.QBaseEntity(this);

    public final StringPath jsonUploadUrl = createString("jsonUploadUrl");

    public final StringPath languageCd = createString("languageCd");

    public final StringPath languageName = createString("languageName");

    //inherited
    public final DateTimePath<java.time.LocalDateTime> modDt = _super.modDt;

    //inherited
    public final NumberPath<Long> modUsr = _super.modUsr;

    public final StringPath nationalFlagUrl = createString("nationalFlagUrl");

    public final StringPath nativeLanguage = createString("nativeLanguage");

    public final NumberPath<Long> packId = createNumber("packId", Long.class);

    public final NumberPath<Integer> packVersion = createNumber("packVersion", Integer.class);

    //inherited
    public final DateTimePath<java.time.LocalDateTime> regDt = _super.regDt;

    //inherited
    public final NumberPath<Long> regUsr = _super.regUsr;

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> useType = createEnum("useType", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public QLanguagePackViewEntity(String variable) {
        super(LanguagePackViewEntity.class, forVariable(variable));
    }

    public QLanguagePackViewEntity(Path<? extends LanguagePackViewEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QLanguagePackViewEntity(PathMetadata metadata) {
        super(LanguagePackViewEntity.class, metadata);
    }

}

