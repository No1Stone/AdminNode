package com.sparwk.adminnode.admin.jpa.entity.song;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QSongEntity is a Querydsl query type for SongEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QSongEntity extends EntityPathBase<SongEntity> {

    private static final long serialVersionUID = 2113124724L;

    public static final QSongEntity songEntity = new QSongEntity("songEntity");

    public final com.sparwk.adminnode.admin.jpa.entity.QBaseEntity _super = new com.sparwk.adminnode.admin.jpa.entity.QBaseEntity(this);

    public final StringPath avatarFileUrl = createString("avatarFileUrl");

    public final StringPath avatarImgUseYn = createString("avatarImgUseYn");

    public final StringPath description = createString("description");

    public final StringPath isrc = createString("isrc");

    public final StringPath iswc = createString("iswc");

    public final StringPath langCd = createString("langCd");

    //inherited
    public final DateTimePath<java.time.LocalDateTime> modDt = _super.modDt;

    //inherited
    public final NumberPath<Long> modUsr = _super.modUsr;

    public final NumberPath<Long> originalSongId = createNumber("originalSongId", Long.class);

    //inherited
    public final DateTimePath<java.time.LocalDateTime> regDt = _super.regDt;

    //inherited
    public final NumberPath<Long> regUsr = _super.regUsr;

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> songAvailYn = createEnum("songAvailYn", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public final NumberPath<Long> songId = createNumber("songId", Long.class);

    public final NumberPath<Long> songOwner = createNumber("songOwner", Long.class);

    public final StringPath songStatusCd = createString("songStatusCd");

    public final DateTimePath<java.time.LocalDateTime> songStatusModDt = createDateTime("songStatusModDt", java.time.LocalDateTime.class);

    public final StringPath songSubTitle = createString("songSubTitle");

    public final StringPath songTitle = createString("songTitle");

    public final StringPath songVersionCd = createString("songVersionCd");

    public final StringPath userDefineVersion = createString("userDefineVersion");

    public QSongEntity(String variable) {
        super(SongEntity.class, forVariable(variable));
    }

    public QSongEntity(Path<? extends SongEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QSongEntity(PathMetadata metadata) {
        super(SongEntity.class, metadata);
    }

}

