package com.sparwk.adminnode.admin.jpa.entity.profile;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QProfileGroupOnthewebEntity is a Querydsl query type for ProfileGroupOnthewebEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QProfileGroupOnthewebEntity extends EntityPathBase<ProfileGroupOnthewebEntity> {

    private static final long serialVersionUID = 374297469L;

    public static final QProfileGroupOnthewebEntity profileGroupOnthewebEntity = new QProfileGroupOnthewebEntity("profileGroupOnthewebEntity");

    public final com.sparwk.adminnode.admin.jpa.entity.QBaseEntity _super = new com.sparwk.adminnode.admin.jpa.entity.QBaseEntity(this);

    //inherited
    public final DateTimePath<java.time.LocalDateTime> modDt = _super.modDt;

    //inherited
    public final NumberPath<Long> modUsr = _super.modUsr;

    public final NumberPath<Long> profileId = createNumber("profileId", Long.class);

    //inherited
    public final DateTimePath<java.time.LocalDateTime> regDt = _super.regDt;

    //inherited
    public final NumberPath<Long> regUsr = _super.regUsr;

    public final StringPath snsTypeCd = createString("snsTypeCd");

    public final StringPath snsUrl = createString("snsUrl");

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> useYn = createEnum("useYn", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public QProfileGroupOnthewebEntity(String variable) {
        super(ProfileGroupOnthewebEntity.class, forVariable(variable));
    }

    public QProfileGroupOnthewebEntity(Path<? extends ProfileGroupOnthewebEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QProfileGroupOnthewebEntity(PathMetadata metadata) {
        super(ProfileGroupOnthewebEntity.class, metadata);
    }

}

