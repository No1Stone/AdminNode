package com.sparwk.adminnode.admin.jpa.entity.admin;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QAdminPasswordEntity is a Querydsl query type for AdminPasswordEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QAdminPasswordEntity extends EntityPathBase<AdminPasswordEntity> {

    private static final long serialVersionUID = 1948086289L;

    public static final QAdminPasswordEntity adminPasswordEntity = new QAdminPasswordEntity("adminPasswordEntity");

    public final NumberPath<Long> adminId = createNumber("adminId", Long.class);

    public final StringPath adminPassword = createString("adminPassword");

    public final StringPath adminPrevPassword = createString("adminPrevPassword");

    public final DateTimePath<java.time.LocalDateTime> lastConnect = createDateTime("lastConnect", java.time.LocalDateTime.class);

    public QAdminPasswordEntity(String variable) {
        super(AdminPasswordEntity.class, forVariable(variable));
    }

    public QAdminPasswordEntity(Path<? extends AdminPasswordEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QAdminPasswordEntity(PathMetadata metadata) {
        super(AdminPasswordEntity.class, metadata);
    }

}

