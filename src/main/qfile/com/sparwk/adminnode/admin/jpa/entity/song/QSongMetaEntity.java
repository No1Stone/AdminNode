package com.sparwk.adminnode.admin.jpa.entity.song;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QSongMetaEntity is a Querydsl query type for SongMetaEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QSongMetaEntity extends EntityPathBase<SongMetaEntity> {

    private static final long serialVersionUID = -169135271L;

    public static final QSongMetaEntity songMetaEntity = new QSongMetaEntity("songMetaEntity");

    public final com.sparwk.adminnode.admin.jpa.entity.QBaseEntity _super = new com.sparwk.adminnode.admin.jpa.entity.QBaseEntity(this);

    public final StringPath attrDtlCd = createString("attrDtlCd");

    public final StringPath attrTypeCd = createString("attrTypeCd");

    //inherited
    public final DateTimePath<java.time.LocalDateTime> modDt = _super.modDt;

    //inherited
    public final NumberPath<Long> modUsr = _super.modUsr;

    //inherited
    public final DateTimePath<java.time.LocalDateTime> regDt = _super.regDt;

    //inherited
    public final NumberPath<Long> regUsr = _super.regUsr;

    public final NumberPath<Long> songId = createNumber("songId", Long.class);

    public QSongMetaEntity(String variable) {
        super(SongMetaEntity.class, forVariable(variable));
    }

    public QSongMetaEntity(Path<? extends SongMetaEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QSongMetaEntity(PathMetadata metadata) {
        super(SongMetaEntity.class, metadata);
    }

}

