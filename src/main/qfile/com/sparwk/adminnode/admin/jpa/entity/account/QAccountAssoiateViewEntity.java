package com.sparwk.adminnode.admin.jpa.entity.account;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QAccountAssoiateViewEntity is a Querydsl query type for AccountAssoiateViewEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QAccountAssoiateViewEntity extends EntityPathBase<AccountAssoiateViewEntity> {

    private static final long serialVersionUID = -1681997298L;

    public static final QAccountAssoiateViewEntity accountAssoiateViewEntity = new QAccountAssoiateViewEntity("accountAssoiateViewEntity");

    public final StringPath accntEmail = createString("accntEmail");

    public final NumberPath<Long> accntId = createNumber("accntId", Long.class);

    public final StringPath bthDay = createString("bthDay");

    public final StringPath bthMonth = createString("bthMonth");

    public final StringPath bthYear = createString("bthYear");

    public final StringPath fullName = createString("fullName");

    public final StringPath gender = createString("gender");

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final StringPath passportFirstName = createString("passportFirstName");

    public final StringPath passportLastName = createString("passportLastName");

    public final StringPath passportMiddleName = createString("passportMiddleName");

    public final NumberPath<Long> profileId = createNumber("profileId", Long.class);

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> stageNameYn = createEnum("stageNameYn", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public QAccountAssoiateViewEntity(String variable) {
        super(AccountAssoiateViewEntity.class, forVariable(variable));
    }

    public QAccountAssoiateViewEntity(Path<? extends AccountAssoiateViewEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QAccountAssoiateViewEntity(PathMetadata metadata) {
        super(AccountAssoiateViewEntity.class, metadata);
    }

}

