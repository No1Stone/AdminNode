package com.sparwk.adminnode.admin.jpa.entity.account;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QAccountGroupDetailEntity is a Querydsl query type for AccountGroupDetailEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QAccountGroupDetailEntity extends EntityPathBase<AccountGroupDetailEntity> {

    private static final long serialVersionUID = -1358847324L;

    public static final QAccountGroupDetailEntity accountGroupDetailEntity = new QAccountGroupDetailEntity("accountGroupDetailEntity");

    public final com.sparwk.adminnode.admin.jpa.entity.QBaseEntity _super = new com.sparwk.adminnode.admin.jpa.entity.QBaseEntity(this);

    public final NumberPath<Long> accntId = createNumber("accntId", Long.class);

    public final StringPath addr1 = createString("addr1");

    public final StringPath addr2 = createString("addr2");

    public final StringPath city = createString("city");

    public final StringPath groupName = createString("groupName");

    public final StringPath locationCd = createString("locationCd");

    //inherited
    public final DateTimePath<java.time.LocalDateTime> modDt = _super.modDt;

    //inherited
    public final NumberPath<Long> modUsr = _super.modUsr;

    public final StringPath postCd = createString("postCd");

    //inherited
    public final DateTimePath<java.time.LocalDateTime> regDt = _super.regDt;

    public final StringPath region = createString("region");

    //inherited
    public final NumberPath<Long> regUsr = _super.regUsr;

    public QAccountGroupDetailEntity(String variable) {
        super(AccountGroupDetailEntity.class, forVariable(variable));
    }

    public QAccountGroupDetailEntity(Path<? extends AccountGroupDetailEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QAccountGroupDetailEntity(PathMetadata metadata) {
        super(AccountGroupDetailEntity.class, metadata);
    }

}

