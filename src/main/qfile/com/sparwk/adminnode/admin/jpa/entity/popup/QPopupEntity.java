package com.sparwk.adminnode.admin.jpa.entity.popup;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QPopupEntity is a Querydsl query type for PopupEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QPopupEntity extends EntityPathBase<PopupEntity> {

    private static final long serialVersionUID = 66538000L;

    public static final QPopupEntity popupEntity = new QPopupEntity("popupEntity");

    public final com.sparwk.adminnode.admin.jpa.entity.QBaseEntity _super = new com.sparwk.adminnode.admin.jpa.entity.QBaseEntity(this);

    public final StringPath content = createString("content");

    public final StringPath edate = createString("edate");

    //inherited
    public final DateTimePath<java.time.LocalDateTime> modDt = _super.modDt;

    //inherited
    public final NumberPath<Long> modUsr = _super.modUsr;

    public final NumberPath<Integer> popupLocationHeight = createNumber("popupLocationHeight", Integer.class);

    public final NumberPath<Integer> popupLocationWidth = createNumber("popupLocationWidth", Integer.class);

    public final NumberPath<Long> popupSeq = createNumber("popupSeq", Long.class);

    public final NumberPath<Integer> popupSizeHeight = createNumber("popupSizeHeight", Integer.class);

    public final NumberPath<Integer> popupSizeWidth = createNumber("popupSizeWidth", Integer.class);

    public final StringPath popupStatusCd = createString("popupStatusCd");

    public final StringPath popupTitle = createString("popupTitle");

    //inherited
    public final DateTimePath<java.time.LocalDateTime> regDt = _super.regDt;

    //inherited
    public final NumberPath<Long> regUsr = _super.regUsr;

    public final StringPath sdate = createString("sdate");

    public QPopupEntity(String variable) {
        super(PopupEntity.class, forVariable(variable));
    }

    public QPopupEntity(Path<? extends PopupEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QPopupEntity(PathMetadata metadata) {
        super(PopupEntity.class, metadata);
    }

}

