package com.sparwk.adminnode.admin.jpa.entity.profile;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QProfileGenderEntity is a Querydsl query type for ProfileGenderEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QProfileGenderEntity extends EntityPathBase<ProfileGenderEntity> {

    private static final long serialVersionUID = -899730581L;

    public static final QProfileGenderEntity profileGenderEntity = new QProfileGenderEntity("profileGenderEntity");

    public final StringPath genderCd = createString("genderCd");

    public final NumberPath<Long> profileId = createNumber("profileId", Long.class);

    public QProfileGenderEntity(String variable) {
        super(ProfileGenderEntity.class, forVariable(variable));
    }

    public QProfileGenderEntity(Path<? extends ProfileGenderEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QProfileGenderEntity(PathMetadata metadata) {
        super(ProfileGenderEntity.class, metadata);
    }

}

