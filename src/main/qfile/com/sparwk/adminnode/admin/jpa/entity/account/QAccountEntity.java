package com.sparwk.adminnode.admin.jpa.entity.account;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QAccountEntity is a Querydsl query type for AccountEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QAccountEntity extends EntityPathBase<AccountEntity> {

    private static final long serialVersionUID = -1549772398L;

    public static final QAccountEntity accountEntity = new QAccountEntity("accountEntity");

    public final com.sparwk.adminnode.admin.jpa.entity.QBaseEntity _super = new com.sparwk.adminnode.admin.jpa.entity.QBaseEntity(this);

    public final StringPath accntEmail = createString("accntEmail");

    public final NumberPath<Long> accntId = createNumber("accntId", Long.class);

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> accntLoginActiveYn = createEnum("accntLoginActiveYn", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public final StringPath accntPass = createString("accntPass");

    public final NumberPath<Long> accntRepositoryId = createNumber("accntRepositoryId", Long.class);

    public final StringPath accntTypeCd = createString("accntTypeCd");

    public final StringPath countryCd = createString("countryCd");

    public final NumberPath<Long> lastUseProfileId = createNumber("lastUseProfileId", Long.class);

    //inherited
    public final DateTimePath<java.time.LocalDateTime> modDt = _super.modDt;

    //inherited
    public final NumberPath<Long> modUsr = _super.modUsr;

    public final StringPath phoneNumber = createString("phoneNumber");

    //inherited
    public final DateTimePath<java.time.LocalDateTime> regDt = _super.regDt;

    //inherited
    public final NumberPath<Long> regUsr = _super.regUsr;

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> useYn = createEnum("useYn", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> verifyPhoneYn = createEnum("verifyPhoneYn", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> verifyYn = createEnum("verifyYn", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public QAccountEntity(String variable) {
        super(AccountEntity.class, forVariable(variable));
    }

    public QAccountEntity(Path<? extends AccountEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QAccountEntity(PathMetadata metadata) {
        super(AccountEntity.class, metadata);
    }

}

