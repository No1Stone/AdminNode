package com.sparwk.adminnode.admin.jpa.entity.languagePack;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QLanguagePackEntity is a Querydsl query type for LanguagePackEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QLanguagePackEntity extends EntityPathBase<LanguagePackEntity> {

    private static final long serialVersionUID = 1780757492L;

    public static final QLanguagePackEntity languagePackEntity = new QLanguagePackEntity("languagePackEntity");

    public final com.sparwk.adminnode.admin.jpa.entity.QBaseEntity _super = new com.sparwk.adminnode.admin.jpa.entity.QBaseEntity(this);

    public final StringPath jsonUploadUrl = createString("jsonUploadUrl");

    public final StringPath languageCd = createString("languageCd");

    //inherited
    public final DateTimePath<java.time.LocalDateTime> modDt = _super.modDt;

    //inherited
    public final NumberPath<Long> modUsr = _super.modUsr;

    public final StringPath nationalFlagUrl = createString("nationalFlagUrl");

    public final NumberPath<Long> packId = createNumber("packId", Long.class);

    public final NumberPath<Integer> packVersion = createNumber("packVersion", Integer.class);

    //inherited
    public final DateTimePath<java.time.LocalDateTime> regDt = _super.regDt;

    //inherited
    public final NumberPath<Long> regUsr = _super.regUsr;

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> useType = createEnum("useType", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public QLanguagePackEntity(String variable) {
        super(LanguagePackEntity.class, forVariable(variable));
    }

    public QLanguagePackEntity(Path<? extends LanguagePackEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QLanguagePackEntity(PathMetadata metadata) {
        super(LanguagePackEntity.class, metadata);
    }

}

