package com.sparwk.adminnode.admin.jpa.entity.projects;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QProjectCowritersViewEntity is a Querydsl query type for ProjectCowritersViewEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QProjectCowritersViewEntity extends EntityPathBase<ProjectCowritersViewEntity> {

    private static final long serialVersionUID = 903019194L;

    public static final QProjectCowritersViewEntity projectCowritersViewEntity = new QProjectCowritersViewEntity("projectCowritersViewEntity");

    public final StringPath accntEmail = createString("accntEmail");

    public final NumberPath<Integer> cnt = createNumber("cnt", Integer.class);

    public final StringPath companyName = createString("companyName");

    public final StringPath fullName = createString("fullName");

    public final StringPath headline = createString("headline");

    public final StringPath phoneNumber = createString("phoneNumber");

    public final NumberPath<Long> profileId = createNumber("profileId", Long.class);

    public final NumberPath<Long> projId = createNumber("projId", Long.class);

    public QProjectCowritersViewEntity(String variable) {
        super(ProjectCowritersViewEntity.class, forVariable(variable));
    }

    public QProjectCowritersViewEntity(Path<? extends ProjectCowritersViewEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QProjectCowritersViewEntity(PathMetadata metadata) {
        super(ProjectCowritersViewEntity.class, metadata);
    }

}

