package com.sparwk.adminnode.admin.jpa.entity.timezone;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QTimezoneEntity is a Querydsl query type for TimezoneEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QTimezoneEntity extends EntityPathBase<TimezoneEntity> {

    private static final long serialVersionUID = 166408820L;

    public static final QTimezoneEntity timezoneEntity = new QTimezoneEntity("timezoneEntity");

    public final com.sparwk.adminnode.admin.jpa.entity.QBaseEntity _super = new com.sparwk.adminnode.admin.jpa.entity.QBaseEntity(this);

    public final StringPath city = createString("city");

    public final StringPath continent = createString("continent");

    public final NumberPath<Integer> diffTime = createNumber("diffTime", Integer.class);

    //inherited
    public final DateTimePath<java.time.LocalDateTime> modDt = _super.modDt;

    //inherited
    public final NumberPath<Long> modUsr = _super.modUsr;

    //inherited
    public final DateTimePath<java.time.LocalDateTime> regDt = _super.regDt;

    //inherited
    public final NumberPath<Long> regUsr = _super.regUsr;

    public final StringPath timezoneName = createString("timezoneName");

    public final NumberPath<Long> timezoneSeq = createNumber("timezoneSeq", Long.class);

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> useType = createEnum("useType", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public final StringPath utcHour = createString("utcHour");

    public QTimezoneEntity(String variable) {
        super(TimezoneEntity.class, forVariable(variable));
    }

    public QTimezoneEntity(Path<? extends TimezoneEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTimezoneEntity(PathMetadata metadata) {
        super(TimezoneEntity.class, metadata);
    }

}

