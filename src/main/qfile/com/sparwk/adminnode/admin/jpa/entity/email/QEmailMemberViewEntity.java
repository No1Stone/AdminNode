package com.sparwk.adminnode.admin.jpa.entity.email;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QEmailMemberViewEntity is a Querydsl query type for EmailMemberViewEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QEmailMemberViewEntity extends EntityPathBase<EmailMemberViewEntity> {

    private static final long serialVersionUID = -1373143825L;

    public static final QEmailMemberViewEntity emailMemberViewEntity = new QEmailMemberViewEntity("emailMemberViewEntity");

    public final StringPath accntEmail = createString("accntEmail");

    public final StringPath fullName = createString("fullName");

    public final NumberPath<Long> profileId = createNumber("profileId", Long.class);

    public final StringPath profileType = createString("profileType");

    public QEmailMemberViewEntity(String variable) {
        super(EmailMemberViewEntity.class, forVariable(variable));
    }

    public QEmailMemberViewEntity(Path<? extends EmailMemberViewEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QEmailMemberViewEntity(PathMetadata metadata) {
        super(EmailMemberViewEntity.class, metadata);
    }

}

