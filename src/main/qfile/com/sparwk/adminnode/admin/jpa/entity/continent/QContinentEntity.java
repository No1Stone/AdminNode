package com.sparwk.adminnode.admin.jpa.entity.continent;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QContinentEntity is a Querydsl query type for ContinentEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QContinentEntity extends EntityPathBase<ContinentEntity> {

    private static final long serialVersionUID = -1062183904L;

    public static final QContinentEntity continentEntity = new QContinentEntity("continentEntity");

    public final com.sparwk.adminnode.admin.jpa.entity.QBaseEntity _super = new com.sparwk.adminnode.admin.jpa.entity.QBaseEntity(this);

    public final StringPath code = createString("code");

    public final StringPath continent = createString("continent");

    public final StringPath continentCd = createString("continentCd");

    public final NumberPath<Long> continentSeq = createNumber("continentSeq", Long.class);

    public final StringPath description = createString("description");

    //inherited
    public final DateTimePath<java.time.LocalDateTime> modDt = _super.modDt;

    //inherited
    public final NumberPath<Long> modUsr = _super.modUsr;

    //inherited
    public final DateTimePath<java.time.LocalDateTime> regDt = _super.regDt;

    //inherited
    public final NumberPath<Long> regUsr = _super.regUsr;

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> useType = createEnum("useType", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public QContinentEntity(String variable) {
        super(ContinentEntity.class, forVariable(variable));
    }

    public QContinentEntity(Path<? extends ContinentEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QContinentEntity(PathMetadata metadata) {
        super(ContinentEntity.class, metadata);
    }

}

