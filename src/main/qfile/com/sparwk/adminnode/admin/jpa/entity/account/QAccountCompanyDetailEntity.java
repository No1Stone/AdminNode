package com.sparwk.adminnode.admin.jpa.entity.account;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QAccountCompanyDetailEntity is a Querydsl query type for AccountCompanyDetailEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QAccountCompanyDetailEntity extends EntityPathBase<AccountCompanyDetailEntity> {

    private static final long serialVersionUID = -1194925214L;

    public static final QAccountCompanyDetailEntity accountCompanyDetailEntity = new QAccountCompanyDetailEntity("accountCompanyDetailEntity");

    public final com.sparwk.adminnode.admin.jpa.entity.QBaseEntity _super = new com.sparwk.adminnode.admin.jpa.entity.QBaseEntity(this);

    public final NumberPath<Long> accntId = createNumber("accntId", Long.class);

    public final StringPath addr1 = createString("addr1");

    public final StringPath addr2 = createString("addr2");

    public final StringPath city = createString("city");

    public final StringPath companyName = createString("companyName");

    public final StringPath contctEmail = createString("contctEmail");

    public final StringPath contctFirstName = createString("contctFirstName");

    public final StringPath contctLastName = createString("contctLastName");

    public final StringPath contctMidleName = createString("contctMidleName");

    //inherited
    public final DateTimePath<java.time.LocalDateTime> modDt = _super.modDt;

    //inherited
    public final NumberPath<Long> modUsr = _super.modUsr;

    public final StringPath postCd = createString("postCd");

    //inherited
    public final DateTimePath<java.time.LocalDateTime> regDt = _super.regDt;

    public final StringPath region = createString("region");

    //inherited
    public final NumberPath<Long> regUsr = _super.regUsr;

    public QAccountCompanyDetailEntity(String variable) {
        super(AccountCompanyDetailEntity.class, forVariable(variable));
    }

    public QAccountCompanyDetailEntity(Path<? extends AccountCompanyDetailEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QAccountCompanyDetailEntity(PathMetadata metadata) {
        super(AccountCompanyDetailEntity.class, metadata);
    }

}

