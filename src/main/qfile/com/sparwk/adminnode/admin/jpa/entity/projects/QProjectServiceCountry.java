package com.sparwk.adminnode.admin.jpa.entity.projects;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QProjectServiceCountry is a Querydsl query type for ProjectServiceCountry
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QProjectServiceCountry extends EntityPathBase<ProjectServiceCountry> {

    private static final long serialVersionUID = 1339841571L;

    public static final QProjectServiceCountry projectServiceCountry = new QProjectServiceCountry("projectServiceCountry");

    public final com.sparwk.adminnode.admin.jpa.entity.QBaseEntity _super = new com.sparwk.adminnode.admin.jpa.entity.QBaseEntity(this);

    //inherited
    public final DateTimePath<java.time.LocalDateTime> modDt = _super.modDt;

    //inherited
    public final NumberPath<Long> modUsr = _super.modUsr;

    public final NumberPath<Long> projId = createNumber("projId", Long.class);

    //inherited
    public final DateTimePath<java.time.LocalDateTime> regDt = _super.regDt;

    //inherited
    public final NumberPath<Long> regUsr = _super.regUsr;

    public final StringPath serviceCntrCd = createString("serviceCntrCd");

    public QProjectServiceCountry(String variable) {
        super(ProjectServiceCountry.class, forVariable(variable));
    }

    public QProjectServiceCountry(Path<? extends ProjectServiceCountry> path) {
        super(path.getType(), path.getMetadata());
    }

    public QProjectServiceCountry(PathMetadata metadata) {
        super(ProjectServiceCountry.class, metadata);
    }

}

