package com.sparwk.adminnode.admin.jpa.entity.account;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QAccountSnsTokenEntity is a Querydsl query type for AccountSnsTokenEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QAccountSnsTokenEntity extends EntityPathBase<AccountSnsTokenEntity> {

    private static final long serialVersionUID = -897248941L;

    public static final QAccountSnsTokenEntity accountSnsTokenEntity = new QAccountSnsTokenEntity("accountSnsTokenEntity");

    public final NumberPath<Long> accntId = createNumber("accntId", Long.class);

    public final StringPath snsToken = createString("snsToken");

    public final StringPath snsTypeCd = createString("snsTypeCd");

    public QAccountSnsTokenEntity(String variable) {
        super(AccountSnsTokenEntity.class, forVariable(variable));
    }

    public QAccountSnsTokenEntity(Path<? extends AccountSnsTokenEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QAccountSnsTokenEntity(PathMetadata metadata) {
        super(AccountSnsTokenEntity.class, metadata);
    }

}

