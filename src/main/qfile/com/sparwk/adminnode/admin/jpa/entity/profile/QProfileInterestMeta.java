package com.sparwk.adminnode.admin.jpa.entity.profile;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QProfileInterestMeta is a Querydsl query type for ProfileInterestMeta
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QProfileInterestMeta extends EntityPathBase<ProfileInterestMeta> {

    private static final long serialVersionUID = 1229574582L;

    public static final QProfileInterestMeta profileInterestMeta = new QProfileInterestMeta("profileInterestMeta");

    public final com.sparwk.adminnode.admin.jpa.entity.QBaseEntity _super = new com.sparwk.adminnode.admin.jpa.entity.QBaseEntity(this);

    public final StringPath detailTypeCd = createString("detailTypeCd");

    public final StringPath kindTypeCd = createString("kindTypeCd");

    //inherited
    public final DateTimePath<java.time.LocalDateTime> modDt = _super.modDt;

    //inherited
    public final NumberPath<Long> modUsr = _super.modUsr;

    public final NumberPath<Long> profileId = createNumber("profileId", Long.class);

    //inherited
    public final DateTimePath<java.time.LocalDateTime> regDt = _super.regDt;

    //inherited
    public final NumberPath<Long> regUsr = _super.regUsr;

    public QProfileInterestMeta(String variable) {
        super(ProfileInterestMeta.class, forVariable(variable));
    }

    public QProfileInterestMeta(Path<? extends ProfileInterestMeta> path) {
        super(path.getType(), path.getMetadata());
    }

    public QProfileInterestMeta(PathMetadata metadata) {
        super(ProfileInterestMeta.class, metadata);
    }

}

