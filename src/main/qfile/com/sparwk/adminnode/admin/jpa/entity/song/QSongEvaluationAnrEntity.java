package com.sparwk.adminnode.admin.jpa.entity.song;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QSongEvaluationAnrEntity is a Querydsl query type for SongEvaluationAnrEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QSongEvaluationAnrEntity extends EntityPathBase<SongEvaluationAnrEntity> {

    private static final long serialVersionUID = -1300724645L;

    public static final QSongEvaluationAnrEntity songEvaluationAnrEntity = new QSongEvaluationAnrEntity("songEvaluationAnrEntity");

    public final com.sparwk.adminnode.admin.jpa.entity.QBaseEntity _super = new com.sparwk.adminnode.admin.jpa.entity.QBaseEntity(this);

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> activeYn = createEnum("activeYn", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public final DateTimePath<java.time.LocalDateTime> banDt = createDateTime("banDt", java.time.LocalDateTime.class);

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> banYn = createEnum("banYn", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> confirmYn = createEnum("confirmYn", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public final NumberPath<Long> evalAnrId = createNumber("evalAnrId", Long.class);

    public final NumberPath<Long> evalAnrSeq = createNumber("evalAnrSeq", Long.class);

    public final NumberPath<Long> evalSeq = createNumber("evalSeq", Long.class);

    public final DateTimePath<java.time.LocalDateTime> invtDt = createDateTime("invtDt", java.time.LocalDateTime.class);

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> invtStat = createEnum("invtStat", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    //inherited
    public final DateTimePath<java.time.LocalDateTime> modDt = _super.modDt;

    //inherited
    public final NumberPath<Long> modUsr = _super.modUsr;

    public final DateTimePath<java.time.LocalDateTime> patpEdt = createDateTime("patpEdt", java.time.LocalDateTime.class);

    public final DateTimePath<java.time.LocalDateTime> patpSdt = createDateTime("patpSdt", java.time.LocalDateTime.class);

    public final DateTimePath<java.time.LocalDateTime> quitDt = createDateTime("quitDt", java.time.LocalDateTime.class);

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> quitYn = createEnum("quitYn", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    //inherited
    public final DateTimePath<java.time.LocalDateTime> regDt = _super.regDt;

    //inherited
    public final NumberPath<Long> regUsr = _super.regUsr;

    public QSongEvaluationAnrEntity(String variable) {
        super(SongEvaluationAnrEntity.class, forVariable(variable));
    }

    public QSongEvaluationAnrEntity(Path<? extends SongEvaluationAnrEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QSongEvaluationAnrEntity(PathMetadata metadata) {
        super(SongEvaluationAnrEntity.class, metadata);
    }

}

