package com.sparwk.adminnode.admin.jpa.entity.song;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QSongMetaCustomEntity is a Querydsl query type for SongMetaCustomEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QSongMetaCustomEntity extends EntityPathBase<SongMetaCustomEntity> {

    private static final long serialVersionUID = 1121624490L;

    public static final QSongMetaCustomEntity songMetaCustomEntity = new QSongMetaCustomEntity("songMetaCustomEntity");

    public final com.sparwk.adminnode.admin.jpa.entity.QBaseEntity _super = new com.sparwk.adminnode.admin.jpa.entity.QBaseEntity(this);

    public final StringPath metadataName = createString("metadataName");

    public final NumberPath<Long> metadataSeq = createNumber("metadataSeq", Long.class);

    public final StringPath metadataValue = createString("metadataValue");

    //inherited
    public final DateTimePath<java.time.LocalDateTime> modDt = _super.modDt;

    //inherited
    public final NumberPath<Long> modUsr = _super.modUsr;

    //inherited
    public final DateTimePath<java.time.LocalDateTime> regDt = _super.regDt;

    //inherited
    public final NumberPath<Long> regUsr = _super.regUsr;

    public final NumberPath<Long> songId = createNumber("songId", Long.class);

    public QSongMetaCustomEntity(String variable) {
        super(SongMetaCustomEntity.class, forVariable(variable));
    }

    public QSongMetaCustomEntity(Path<? extends SongMetaCustomEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QSongMetaCustomEntity(PathMetadata metadata) {
        super(SongMetaCustomEntity.class, metadata);
    }

}

