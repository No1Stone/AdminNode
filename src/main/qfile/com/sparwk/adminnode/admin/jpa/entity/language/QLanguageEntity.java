package com.sparwk.adminnode.admin.jpa.entity.language;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QLanguageEntity is a Querydsl query type for LanguageEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QLanguageEntity extends EntityPathBase<LanguageEntity> {

    private static final long serialVersionUID = 1564009812L;

    public static final QLanguageEntity languageEntity = new QLanguageEntity("languageEntity");

    public final com.sparwk.adminnode.admin.jpa.entity.QBaseEntity _super = new com.sparwk.adminnode.admin.jpa.entity.QBaseEntity(this);

    public final StringPath iso639_1 = createString("iso639_1");

    public final StringPath iso639_2b = createString("iso639_2b");

    public final StringPath iso639_2t = createString("iso639_2t");

    public final StringPath language = createString("language");

    public final StringPath languageCd = createString("languageCd");

    public final StringPath languageFamily = createString("languageFamily");

    public final NumberPath<Long> languageSeq = createNumber("languageSeq", Long.class);

    //inherited
    public final DateTimePath<java.time.LocalDateTime> modDt = _super.modDt;

    //inherited
    public final NumberPath<Long> modUsr = _super.modUsr;

    public final StringPath nativeLanguage = createString("nativeLanguage");

    //inherited
    public final DateTimePath<java.time.LocalDateTime> regDt = _super.regDt;

    //inherited
    public final NumberPath<Long> regUsr = _super.regUsr;

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> useType = createEnum("useType", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public QLanguageEntity(String variable) {
        super(LanguageEntity.class, forVariable(variable));
    }

    public QLanguageEntity(Path<? extends LanguageEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QLanguageEntity(PathMetadata metadata) {
        super(LanguageEntity.class, metadata);
    }

}

