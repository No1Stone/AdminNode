package com.sparwk.adminnode.admin.jpa.entity.email;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QEmailEntity is a Querydsl query type for EmailEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QEmailEntity extends EntityPathBase<EmailEntity> {

    private static final long serialVersionUID = -2069224464L;

    public static final QEmailEntity emailEntity = new QEmailEntity("emailEntity");

    public final com.sparwk.adminnode.admin.jpa.entity.QBaseEntity _super = new com.sparwk.adminnode.admin.jpa.entity.QBaseEntity(this);

    public final StringPath content = createString("content");

    public final NumberPath<Long> emailSeq = createNumber("emailSeq", Long.class);

    public final StringPath fromEmail = createString("fromEmail");

    public final DateTimePath<java.time.LocalDateTime> lastSendDt = createDateTime("lastSendDt", java.time.LocalDateTime.class);

    public final NumberPath<Integer> maxAttachCnt = createNumber("maxAttachCnt", Integer.class);

    public final NumberPath<Integer> memberCnt = createNumber("memberCnt", Integer.class);

    //inherited
    public final DateTimePath<java.time.LocalDateTime> modDt = _super.modDt;

    //inherited
    public final NumberPath<Long> modUsr = _super.modUsr;

    //inherited
    public final DateTimePath<java.time.LocalDateTime> regDt = _super.regDt;

    //inherited
    public final NumberPath<Long> regUsr = _super.regUsr;

    public final StringPath subject = createString("subject");

    public final StringPath toEmail = createString("toEmail");

    public QEmailEntity(String variable) {
        super(EmailEntity.class, forVariable(variable));
    }

    public QEmailEntity(Path<? extends EmailEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QEmailEntity(PathMetadata metadata) {
        super(EmailEntity.class, metadata);
    }

}

