package com.sparwk.adminnode.admin.jpa.entity.songCode;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QSongDetailCodeEntity is a Querydsl query type for SongDetailCodeEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QSongDetailCodeEntity extends EntityPathBase<SongDetailCodeEntity> {

    private static final long serialVersionUID = 1386016965L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QSongDetailCodeEntity songDetailCodeEntity = new QSongDetailCodeEntity("songDetailCodeEntity");

    public final com.sparwk.adminnode.admin.jpa.entity.QBaseEntity _super = new com.sparwk.adminnode.admin.jpa.entity.QBaseEntity(this);

    public final StringPath dcode = createString("dcode");

    public final StringPath description = createString("description");

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.FormatTypeEnum> formatType = createEnum("formatType", com.sparwk.adminnode.admin.jpa.entity.FormatTypeEnum.class);

    public final NumberPath<Integer> hit = createNumber("hit", Integer.class);

    //inherited
    public final DateTimePath<java.time.LocalDateTime> modDt = _super.modDt;

    //inherited
    public final NumberPath<Long> modUsr = _super.modUsr;

    public final StringPath pcode = createString("pcode");

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> popularType = createEnum("popularType", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    //inherited
    public final DateTimePath<java.time.LocalDateTime> regDt = _super.regDt;

    //inherited
    public final NumberPath<Long> regUsr = _super.regUsr;

    public final QSongCodeEntity songCodeEntity;

    public final NumberPath<Long> songDetailCodeSeq = createNumber("songDetailCodeSeq", Long.class);

    public final NumberPath<Integer> sortIndex = createNumber("sortIndex", Integer.class);

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> useType = createEnum("useType", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public final StringPath val = createString("val");

    public QSongDetailCodeEntity(String variable) {
        this(SongDetailCodeEntity.class, forVariable(variable), INITS);
    }

    public QSongDetailCodeEntity(Path<? extends SongDetailCodeEntity> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QSongDetailCodeEntity(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QSongDetailCodeEntity(PathMetadata metadata, PathInits inits) {
        this(SongDetailCodeEntity.class, metadata, inits);
    }

    public QSongDetailCodeEntity(Class<? extends SongDetailCodeEntity> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.songCodeEntity = inits.isInitialized("songCodeEntity") ? new QSongCodeEntity(forProperty("songCodeEntity")) : null;
    }

}

