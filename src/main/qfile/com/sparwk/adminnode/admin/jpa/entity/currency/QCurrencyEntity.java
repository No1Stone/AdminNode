package com.sparwk.adminnode.admin.jpa.entity.currency;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QCurrencyEntity is a Querydsl query type for CurrencyEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QCurrencyEntity extends EntityPathBase<CurrencyEntity> {

    private static final long serialVersionUID = -483750540L;

    public static final QCurrencyEntity currencyEntity = new QCurrencyEntity("currencyEntity");

    public final com.sparwk.adminnode.admin.jpa.entity.QBaseEntity _super = new com.sparwk.adminnode.admin.jpa.entity.QBaseEntity(this);

    public final StringPath country = createString("country");

    public final StringPath currency = createString("currency");

    public final StringPath currencyCd = createString("currencyCd");

    public final NumberPath<Long> currencySeq = createNumber("currencySeq", Long.class);

    public final StringPath isoCode = createString("isoCode");

    //inherited
    public final DateTimePath<java.time.LocalDateTime> modDt = _super.modDt;

    //inherited
    public final NumberPath<Long> modUsr = _super.modUsr;

    //inherited
    public final DateTimePath<java.time.LocalDateTime> regDt = _super.regDt;

    //inherited
    public final NumberPath<Long> regUsr = _super.regUsr;

    public final StringPath symbol = createString("symbol");

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> useType = createEnum("useType", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public QCurrencyEntity(String variable) {
        super(CurrencyEntity.class, forVariable(variable));
    }

    public QCurrencyEntity(Path<? extends CurrencyEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QCurrencyEntity(PathMetadata metadata) {
        super(CurrencyEntity.class, metadata);
    }

}

