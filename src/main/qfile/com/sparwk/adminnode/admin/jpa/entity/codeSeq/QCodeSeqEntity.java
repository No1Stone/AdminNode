package com.sparwk.adminnode.admin.jpa.entity.codeSeq;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QCodeSeqEntity is a Querydsl query type for CodeSeqEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QCodeSeqEntity extends EntityPathBase<CodeSeqEntity> {

    private static final long serialVersionUID = 1388930844L;

    public static final QCodeSeqEntity codeSeqEntity = new QCodeSeqEntity("codeSeqEntity");

    public final StringPath format = createString("format");

    public final StringPath pcode = createString("pcode");

    public final NumberPath<Integer> seq = createNumber("seq", Integer.class);

    public QCodeSeqEntity(String variable) {
        super(CodeSeqEntity.class, forVariable(variable));
    }

    public QCodeSeqEntity(Path<? extends CodeSeqEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QCodeSeqEntity(PathMetadata metadata) {
        super(CodeSeqEntity.class, metadata);
    }

}

