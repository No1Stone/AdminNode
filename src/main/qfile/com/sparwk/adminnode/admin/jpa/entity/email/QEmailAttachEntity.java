package com.sparwk.adminnode.admin.jpa.entity.email;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QEmailAttachEntity is a Querydsl query type for EmailAttachEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QEmailAttachEntity extends EntityPathBase<EmailAttachEntity> {

    private static final long serialVersionUID = -1101891371L;

    public static final QEmailAttachEntity emailAttachEntity = new QEmailAttachEntity("emailAttachEntity");

    public final StringPath attachFileName = createString("attachFileName");

    public final NumberPath<Integer> attachFileSize = createNumber("attachFileSize", Integer.class);

    public final NumberPath<Integer> attachOrder = createNumber("attachOrder", Integer.class);

    public final NumberPath<Long> emailSeq = createNumber("emailSeq", Long.class);

    public QEmailAttachEntity(String variable) {
        super(EmailAttachEntity.class, forVariable(variable));
    }

    public QEmailAttachEntity(Path<? extends EmailAttachEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QEmailAttachEntity(PathMetadata metadata) {
        super(EmailAttachEntity.class, metadata);
    }

}

