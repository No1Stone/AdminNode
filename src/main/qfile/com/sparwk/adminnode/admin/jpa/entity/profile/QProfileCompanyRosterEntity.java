package com.sparwk.adminnode.admin.jpa.entity.profile;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QProfileCompanyRosterEntity is a Querydsl query type for ProfileCompanyRosterEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QProfileCompanyRosterEntity extends EntityPathBase<ProfileCompanyRosterEntity> {

    private static final long serialVersionUID = -660074460L;

    public static final QProfileCompanyRosterEntity profileCompanyRosterEntity = new QProfileCompanyRosterEntity("profileCompanyRosterEntity");

    public final com.sparwk.adminnode.admin.jpa.entity.QBaseEntity _super = new com.sparwk.adminnode.admin.jpa.entity.QBaseEntity(this);

    public final StringPath joiningEndDt = createString("joiningEndDt");

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> joiningEndYn = createEnum("joiningEndYn", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public final StringPath joiningStartDt = createString("joiningStartDt");

    //inherited
    public final DateTimePath<java.time.LocalDateTime> modDt = _super.modDt;

    //inherited
    public final NumberPath<Long> modUsr = _super.modUsr;

    public final NumberPath<Long> profileId = createNumber("profileId", Long.class);

    public final NumberPath<Long> profilePositionSeq = createNumber("profilePositionSeq", Long.class);

    //inherited
    public final DateTimePath<java.time.LocalDateTime> regDt = _super.regDt;

    //inherited
    public final NumberPath<Long> regUsr = _super.regUsr;

    public final NumberPath<Long> rosterSeq = createNumber("rosterSeq", Long.class);

    public final StringPath rosterStatus = createString("rosterStatus");

    public QProfileCompanyRosterEntity(String variable) {
        super(ProfileCompanyRosterEntity.class, forVariable(variable));
    }

    public QProfileCompanyRosterEntity(Path<? extends ProfileCompanyRosterEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QProfileCompanyRosterEntity(PathMetadata metadata) {
        super(ProfileCompanyRosterEntity.class, metadata);
    }

}

