package com.sparwk.adminnode.admin.jpa.entity.projects;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QProjectSongCowriter is a Querydsl query type for ProjectSongCowriter
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QProjectSongCowriter extends EntityPathBase<ProjectSongCowriter> {

    private static final long serialVersionUID = 1534965142L;

    public static final QProjectSongCowriter projectSongCowriter = new QProjectSongCowriter("projectSongCowriter");

    public final com.sparwk.adminnode.admin.jpa.entity.QBaseEntity _super = new com.sparwk.adminnode.admin.jpa.entity.QBaseEntity(this);

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> acceptYn = createEnum("acceptYn", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modUsr = createNumber("modUsr", Long.class);

    public final NumberPath<Long> profileId = createNumber("profileId", Long.class);

    public final NumberPath<Long> projId = createNumber("projId", Long.class);

    public final NumberPath<Double> rateShare = createNumber("rateShare", Double.class);

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> regUsr = createNumber("regUsr", Long.class);

    public final NumberPath<Long> songId = createNumber("songId", Long.class);

    public QProjectSongCowriter(String variable) {
        super(ProjectSongCowriter.class, forVariable(variable));
    }

    public QProjectSongCowriter(Path<? extends ProjectSongCowriter> path) {
        super(path.getType(), path.getMetadata());
    }

    public QProjectSongCowriter(PathMetadata metadata) {
        super(ProjectSongCowriter.class, metadata);
    }

}

