package com.sparwk.adminnode.admin.jpa.entity.profile;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QProfileSkillEntity is a Querydsl query type for ProfileSkillEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QProfileSkillEntity extends EntityPathBase<ProfileSkillEntity> {

    private static final long serialVersionUID = 1983892301L;

    public static final QProfileSkillEntity profileSkillEntity = new QProfileSkillEntity("profileSkillEntity");

    public final com.sparwk.adminnode.admin.jpa.entity.QBaseEntity _super = new com.sparwk.adminnode.admin.jpa.entity.QBaseEntity(this);

    //inherited
    public final DateTimePath<java.time.LocalDateTime> modDt = _super.modDt;

    //inherited
    public final NumberPath<Long> modUsr = _super.modUsr;

    public final NumberPath<Long> profileId = createNumber("profileId", Long.class);

    //inherited
    public final DateTimePath<java.time.LocalDateTime> regDt = _super.regDt;

    //inherited
    public final NumberPath<Long> regUsr = _super.regUsr;

    public final StringPath skillDetailTypeCd = createString("skillDetailTypeCd");

    public final StringPath skillTypeCd = createString("skillTypeCd");

    public QProfileSkillEntity(String variable) {
        super(ProfileSkillEntity.class, forVariable(variable));
    }

    public QProfileSkillEntity(Path<? extends ProfileSkillEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QProfileSkillEntity(PathMetadata metadata) {
        super(ProfileSkillEntity.class, metadata);
    }

}

