package com.sparwk.adminnode.admin.jpa.entity.account;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QAccountCompanyLocationEntity is a Querydsl query type for AccountCompanyLocationEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QAccountCompanyLocationEntity extends EntityPathBase<AccountCompanyLocationEntity> {

    private static final long serialVersionUID = -46492602L;

    public static final QAccountCompanyLocationEntity accountCompanyLocationEntity = new QAccountCompanyLocationEntity("accountCompanyLocationEntity");

    public final NumberPath<Long> accntId = createNumber("accntId", Long.class);

    public final StringPath locationCd = createString("locationCd");

    public QAccountCompanyLocationEntity(String variable) {
        super(AccountCompanyLocationEntity.class, forVariable(variable));
    }

    public QAccountCompanyLocationEntity(Path<? extends AccountCompanyLocationEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QAccountCompanyLocationEntity(PathMetadata metadata) {
        super(AccountCompanyLocationEntity.class, metadata);
    }

}

