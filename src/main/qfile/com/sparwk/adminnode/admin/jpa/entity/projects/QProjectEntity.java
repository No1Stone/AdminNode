package com.sparwk.adminnode.admin.jpa.entity.projects;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QProjectEntity is a Querydsl query type for ProjectEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QProjectEntity extends EntityPathBase<ProjectEntity> {

    private static final long serialVersionUID = -1897513243L;

    public static final QProjectEntity projectEntity = new QProjectEntity("projectEntity");

    public final com.sparwk.adminnode.admin.jpa.entity.QBaseEntity _super = new com.sparwk.adminnode.admin.jpa.entity.QBaseEntity(this);

    public final StringPath avatarFileUrl = createString("avatarFileUrl");

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> completeYn = createEnum("completeYn", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public final NumberPath<Long> compProfileId = createNumber("compProfileId", Long.class);

    public final StringPath genderCd = createString("genderCd");

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> leaveYn = createEnum("leaveYn", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public final NumberPath<Long> maxVal = createNumber("maxVal", Long.class);

    public final NumberPath<Long> minVal = createNumber("minVal", Long.class);

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modUsr = createNumber("modUsr", Long.class);

    public final StringPath pitchProjTypeCd = createString("pitchProjTypeCd");

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> projAvalYn = createEnum("projAvalYn", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public final StringPath projCondCd = createString("projCondCd");

    public final DatePath<java.time.LocalDate> projDdlDt = createDate("projDdlDt", java.time.LocalDate.class);

    public final StringPath projDesc = createString("projDesc");

    public final NumberPath<Long> projId = createNumber("projId", Long.class);

    public final StringPath projIndivCompType = createString("projIndivCompType");

    public final StringPath projInvtDesc = createString("projInvtDesc");

    public final StringPath projInvtTitle = createString("projInvtTitle");

    public final NumberPath<Long> projOwner = createNumber("projOwner", Long.class);

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> projPublYn = createEnum("projPublYn", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public final StringPath projPwd = createString("projPwd");

    public final StringPath projTitle = createString("projTitle");

    public final StringPath projWhatFor = createString("projWhatFor");

    public final StringPath projWorkLocat = createString("projWorkLocat");

    public final EnumPath<com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum> recruitYn = createEnum("recruitYn", com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum.class);

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> regUsr = createNumber("regUsr", Long.class);

    public QProjectEntity(String variable) {
        super(ProjectEntity.class, forVariable(variable));
    }

    public QProjectEntity(Path<? extends ProjectEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QProjectEntity(PathMetadata metadata) {
        super(ProjectEntity.class, metadata);
    }

}

