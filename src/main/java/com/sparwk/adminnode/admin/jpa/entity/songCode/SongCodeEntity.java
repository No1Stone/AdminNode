package com.sparwk.adminnode.admin.jpa.entity.songCode;

import com.sparwk.adminnode.admin.jpa.entity.BaseEntity;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name="tb_admin_song_code")
@IdClass(SongIdClass.class)
public class SongCodeEntity extends BaseEntity {

    @Id
    @GeneratedValue(
            generator = "tb_admin_song_code_seq"
    )
    @Column(name = "song_code_seq")
    private Long songCodeSeq;

    @Column(name = "code", length = 3, unique = true)
    private String code;

    @Column(name = "val", length = 30)
    private String val;

    @Enumerated(EnumType.STRING)
    @Column(name = "use_yn", length = 1)
    private YnTypeEnum useType;

    @OneToMany(mappedBy = "songCodeEntity", cascade = CascadeType.ALL)
    List<SongDetailCodeEntity> songDetailCodeEntity = new ArrayList<>();

    @Builder
    public SongCodeEntity(
                            Long songCodeSeq,
                            String code,
                            String val,
                            YnTypeEnum useType
                        ) {
        this.code = code;
        this.val = val;
        this.useType = useType;
    }


    public void updateYn(YnTypeEnum useType) {
        this.useType = useType;
    }

    public void update(
                            String code,
                            String val,
                            YnTypeEnum useType
                        ) {
        this.code = code;
        this.val = val;
        this.useType = useType;
    }

}
