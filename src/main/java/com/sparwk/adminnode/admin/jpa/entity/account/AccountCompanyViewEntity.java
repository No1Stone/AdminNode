package com.sparwk.adminnode.admin.jpa.entity.account;

import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "view_admin_company_account_list")
@DynamicUpdate
public class AccountCompanyViewEntity {

    @Id
    @Column(name = "profile_id")
    private Long profileId;
    @Column(name = "accnt_id")
    private Long accntId;
    @Column(name = "accnt_email")
    private String accntEmail;
    @Column(name = "company_name")
    private String companyName;
    @Column(name = "company_type")
    private String companyType;
    @Column(name = "bussiness_location")
    private String bussinessLocation;
    @Enumerated(EnumType.STRING)
    @Column(name = "company_license_verify_yn")
    private YnTypeEnum companyLicenseVerifyYn;
    @Enumerated(EnumType.STRING)
    @Column(name = "ipi_number_varify_yn")
    private YnTypeEnum ipiNumberVarifyYn;
    @Column(name = "ipi_number")
    private String ipiNumber;
    @Column(name = "vat_number")
    private String vatNumber;
    @Column(name = "reg_dt")
    private LocalDateTime regDt;



    @Builder
    AccountCompanyViewEntity(
            Long accntId,
            Long profileId,
            String accntEmail,
            String companyName,
            String companyType,
            String bussinessLocation,
            YnTypeEnum companyLicenseVerifyYn,
            YnTypeEnum ipiNumberVarifyYn,
            String ipiNumber,
            String vatNumber,
            LocalDateTime regDt
    ) {
        this.accntId = accntId;
        this.profileId = profileId;
        this.accntEmail = accntEmail;
        this.companyName = companyName;
        this.companyType = companyType;
        this.bussinessLocation = bussinessLocation;
        this.companyLicenseVerifyYn = companyLicenseVerifyYn;
        this.ipiNumberVarifyYn = ipiNumberVarifyYn;
        this.ipiNumber = ipiNumber;
        this.vatNumber = vatNumber;
        this.regDt = regDt;
    }
}
