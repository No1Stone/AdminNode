package com.sparwk.adminnode.admin.jpa.repository.account.dsl;

import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.sparwk.adminnode.admin.biz.v1.account.dto.AssoiateMemberViewListDto;
import com.sparwk.adminnode.admin.jpa.entity.PeriodTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.account.MaintenanceAssociateMemberCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.account.MaintenanceAssociateMemberSorterEnum;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import static com.sparwk.adminnode.admin.jpa.entity.account.QAccountAssoiateViewEntity.accountAssoiateViewEntity;


@Repository
public class AccountAssoiateViewCustomRepositoryImpl implements AccountAssoiateViewCustomRepository {

    private final JPAQueryFactory jpaQueryFactory;

    public AccountAssoiateViewCustomRepositoryImpl(JPAQueryFactory jpaQueryFactory) {
        this.jpaQueryFactory = jpaQueryFactory;
    }

    //인증하기 전 User List
    @Override
    public List<AssoiateMemberViewListDto> findQryAssociateMember(MaintenanceAssociateMemberCateEnum cate,
                                                                   String val,
                                                                   PeriodTypeEnum periodType,
                                                                   String sdate,
                                                                   String edate,
                                                                   MaintenanceAssociateMemberSorterEnum sorter,
                                                                   PageRequest pageRequest) {
        return jpaQueryFactory.select(Projections.bean(AssoiateMemberViewListDto.class,


                        accountAssoiateViewEntity.accntId,
                        accountAssoiateViewEntity.profileId,
                        accountAssoiateViewEntity.accntEmail,
                        accountAssoiateViewEntity.passportFirstName,
                        accountAssoiateViewEntity.passportMiddleName,
                        accountAssoiateViewEntity.passportLastName,
                        accountAssoiateViewEntity.fullName.as("profileFullName"),
                        accountAssoiateViewEntity.stageNameYn,
                        accountAssoiateViewEntity.bthYear,
                        accountAssoiateViewEntity.bthMonth,
                        accountAssoiateViewEntity.bthDay,
                        accountAssoiateViewEntity.gender,
                        accountAssoiateViewEntity.regDt.as("regDt")
                        )
                )
                .from(accountAssoiateViewEntity)
                .where(valLike(cate, val),  termBetween(periodType, sdate, edate))
                .limit(pageRequest.getPageSize())
                .offset(pageRequest.getPageSize() * (pageRequest.getPageNumber() - 1))
                .orderBy(sorterOrder(sorter).stream().toArray(OrderSpecifier[]::new))
                .fetch();
    }

//    인증하기 전 User List
    @Override
    public long countQryAssociateMember(MaintenanceAssociateMemberCateEnum cate,
                                                                  String val,
                                                                  PeriodTypeEnum periodType,
                                                                  String sdate,
                                                                  String edate) {
        return jpaQueryFactory.select(
                        accountAssoiateViewEntity.accntId.count()
                )
                .from(accountAssoiateViewEntity)
                .where(valLike(cate, val), termBetween(periodType, sdate, edate))
                .fetchOne();
    }



    private BooleanExpression valLike(MaintenanceAssociateMemberCateEnum cate, String val) {
        if (cate != null && val != null && val.length() > 0) {
            switch (cate) {
                case All:
                    return accountAssoiateViewEntity.accntEmail.toLowerCase().contains(val.toLowerCase())
                            .or(accountAssoiateViewEntity.fullName.toLowerCase().contains(val.toLowerCase()));
                case Email:
                    return accountAssoiateViewEntity.accntEmail.toLowerCase().contains(val.toLowerCase());
                case FullName:
                    return accountAssoiateViewEntity.fullName.toLowerCase().contains(val.toLowerCase())
                            .and(accountAssoiateViewEntity.stageNameYn.eq(YnTypeEnum.N)
                                    .or(accountAssoiateViewEntity.stageNameYn.isNull()));
                case StageName:
                    return accountAssoiateViewEntity.fullName.toLowerCase().contains(val.toLowerCase())
                            .and(accountAssoiateViewEntity.stageNameYn.eq(YnTypeEnum.Y));
                case Gender:
                    return accountAssoiateViewEntity.gender.toLowerCase().contains(val.toLowerCase());
                default:
                    return null;
            }
        }
        return null;
    }

    private BooleanExpression termBetween(PeriodTypeEnum periodType, String  sdate, String edate) {

        if (sdate != null && edate != null && periodType != null) {

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
            LocalDateTime searchStartDate = LocalDateTime.parse(sdate + " 00:00:00.000", formatter);
            LocalDateTime searchEndDate = LocalDateTime.parse(edate + " 23:59:59.999", formatter);

            switch (periodType) {
                case Created:
                    return accountAssoiateViewEntity.regDt.between(searchStartDate, searchEndDate);
                default:
                    return null;
            }
        }
        return null;
    }

    //이중으로 순차정렬 가능
    private List<OrderSpecifier> sorterOrder(MaintenanceAssociateMemberSorterEnum sorter) {

        List<OrderSpecifier> orders = new ArrayList<>();

        if (sorter == null) {
            orders.add(accountAssoiateViewEntity.regDt.desc());
            return orders;
        }

        if (sorter == MaintenanceAssociateMemberSorterEnum.FullNameAsc) {
            orders.add(accountAssoiateViewEntity.passportFirstName.asc());
            return orders;
        }

        if (sorter == MaintenanceAssociateMemberSorterEnum.FullNameDesc) {
            orders.add(accountAssoiateViewEntity.passportFirstName.desc());
            return orders;
        }

        if (sorter == MaintenanceAssociateMemberSorterEnum.CreatedAsc) {
            orders.add(accountAssoiateViewEntity.regDt.asc());
            return orders;
        }

        if (sorter == MaintenanceAssociateMemberSorterEnum.CreatedDesc) {
            orders.add(accountAssoiateViewEntity.regDt.desc());
            return orders;
        }

        if (sorter == MaintenanceAssociateMemberSorterEnum.EmailAsc) {
            orders.add(accountAssoiateViewEntity.accntEmail.asc());
            return orders;
        }

        if (sorter == MaintenanceAssociateMemberSorterEnum.EmailDesc) {
            orders.add(accountAssoiateViewEntity.accntEmail.desc());
            return orders;
        }

        if (sorter == MaintenanceAssociateMemberSorterEnum.BirthAsc) {
            orders.add(accountAssoiateViewEntity.bthYear.asc());
            orders.add(accountAssoiateViewEntity.bthMonth.asc());
            orders.add(accountAssoiateViewEntity.bthDay.asc());
            return orders;
        }

        if (sorter == MaintenanceAssociateMemberSorterEnum.BirthDesc) {
            orders.add(accountAssoiateViewEntity.bthYear.desc());
            orders.add(accountAssoiateViewEntity.bthMonth.desc());
            orders.add(accountAssoiateViewEntity.bthDay.desc());
            return orders;
        }

        orders.add(accountAssoiateViewEntity.regDt.desc());
        return orders;
    }

}
