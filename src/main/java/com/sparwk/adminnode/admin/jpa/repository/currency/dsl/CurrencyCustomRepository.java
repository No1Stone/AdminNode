package com.sparwk.adminnode.admin.jpa.repository.currency.dsl;

import com.sparwk.adminnode.admin.biz.v1.currency.dto.CurrencyDto;
import com.sparwk.adminnode.admin.biz.v1.currency.dto.CurrencyExcelDto;
import com.sparwk.adminnode.admin.jpa.entity.PeriodTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.currency.CurrencyCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.currency.CurrencySorterEnum;
import org.springframework.data.domain.PageRequest;

import java.util.List;

public interface CurrencyCustomRepository {

    List<CurrencyDto> findQryAll(CurrencyCateEnum cate,
                                 String val,
                                 YnTypeEnum useType,
                                 PeriodTypeEnum periodType,
                                 String sdate,
                                 String edate,
                                 CurrencySorterEnum sorter,
                                 PageRequest pageRequest);

    long countQryAll(CurrencyCateEnum cate,
                                 String val,
                                 YnTypeEnum useType,
                                 PeriodTypeEnum periodType,
                                 String sdate,
                                 String edate);

    List<CurrencyDto> findQryUseY(CurrencyCateEnum cate, String val);
    long countQryUseY(CurrencyCateEnum cate, String val);
    List<CurrencyExcelDto> findExcelList();

}
