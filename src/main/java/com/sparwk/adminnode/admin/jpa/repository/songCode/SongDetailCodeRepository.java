package com.sparwk.adminnode.admin.jpa.repository.songCode;

import com.sparwk.adminnode.admin.jpa.entity.songCode.SongDetailCodeEntity;
import com.sparwk.adminnode.admin.jpa.repository.codeSeq.CodeSeqRepository;
import com.sparwk.adminnode.admin.jpa.repository.songCode.dsl.SongDetailCodeCustomRepository;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface SongDetailCodeRepository extends JpaRepository<SongDetailCodeEntity, Long>, SongDetailCodeCustomRepository, CodeSeqRepository {
    Optional<SongDetailCodeEntity> findByDcode(String code);
    Long countByVal(String val);
}
