package com.sparwk.adminnode.admin.jpa.repository.languagePack.dsl;

import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.sparwk.adminnode.admin.biz.v1.languagePack.dto.LanguagePackDto;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.admin.QAdminEntity;
import com.sparwk.adminnode.admin.jpa.entity.commonCode.CommonSorterEnum2;
import com.sparwk.adminnode.admin.jpa.entity.languagePack.LanguagePackCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.languagePack.LanguagePackSorterEnum;
import com.sparwk.adminnode.admin.jpa.entity.languagePack.QLanguagePackEntity;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.sparwk.adminnode.admin.jpa.entity.admin.QAdminEntity.adminEntity;
import static com.sparwk.adminnode.admin.jpa.entity.commonCode.QCommonOrganizationCodeEntity.commonOrganizationCodeEntity;
import static com.sparwk.adminnode.admin.jpa.entity.languagePack.QLanguagePackViewEntity.languagePackViewEntity;

@Repository
public class LanguagePackCustomRepositoryImpl implements LanguagePackCustomRepository {

    private final JPAQueryFactory jpaQueryFactory;

    public LanguagePackCustomRepositoryImpl(JPAQueryFactory jpaQueryFactory) {
        this.jpaQueryFactory = jpaQueryFactory;
    }

    @Override
    public List<LanguagePackDto> findQryAll(LanguagePackCateEnum cate,
                                            String val,
                                            YnTypeEnum useType,
                                            LanguagePackSorterEnum sorter,
                                            PageRequest pageRequest) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(Projections.bean(LanguagePackDto.class,
                                languagePackViewEntity.packId,
                                languagePackViewEntity.languageCd,
                                languagePackViewEntity.languageName,
                                languagePackViewEntity.nativeLanguage,
                                languagePackViewEntity.nationalFlagUrl,
                                languagePackViewEntity.jsonUploadUrl,
                                languagePackViewEntity.packVersion,
                                languagePackViewEntity.useType,
                                languagePackViewEntity.regUsr,
                                adminEntity.fullName.as("regUsrName"),
                                languagePackViewEntity.regDt,
                                languagePackViewEntity.modUsr,
                                adminEntity2.fullName.as("modUsrName"),
                                languagePackViewEntity.modDt
                        )
                )
                .from(languagePackViewEntity)
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(languagePackViewEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(languagePackViewEntity.modUsr))
                .where(valLike(cate, val), useTypeEq(useType))
                .limit(pageRequest.getPageSize())
                .offset(pageRequest.getPageSize() * (pageRequest.getPageNumber() - 1))
                .orderBy(sorterOrder(sorter))
                .fetch();
    }

    @Override
    public long countQryAll(LanguagePackCateEnum cate,
                                        String val,
                                        YnTypeEnum useType) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(
                                languagePackViewEntity.packId.count()
                )
                .from(languagePackViewEntity)
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(languagePackViewEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(languagePackViewEntity.modUsr))
                .where(valLike(cate, val), useTypeEq(useType))
                .fetchOne();
    }

    @Override
    public void updateQryUseYn(String val, YnTypeEnum useType){
        jpaQueryFactory.update(languagePackViewEntity)
                .set(languagePackViewEntity.useType, useType)
                .where(languagePackViewEntity.languageCd.eq(val))
                .execute()
        ;
    }

    @Override
    public int findQryMaxVersion(String val) {
        return jpaQueryFactory.select(
                        languagePackViewEntity.packVersion.max()
                )
                .from(languagePackViewEntity)
                .where(languagePackViewEntity.languageCd.eq(val))
                .fetchOne();
    }


    private BooleanExpression valLike(LanguagePackCateEnum cate, String val) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        if (cate !=null && val != null && val.length() > 0) {
            switch (cate) {
                case All:
                    return languagePackViewEntity.languageName.toLowerCase().contains(val.toLowerCase())
                            .or(languagePackViewEntity.nativeLanguage.toLowerCase().contains(val.toLowerCase()));
                case LanguagePack:
                    return languagePackViewEntity.languageName.toLowerCase().contains(val.toLowerCase());
                case NativeLanguage:
                    return languagePackViewEntity.nativeLanguage.toLowerCase().contains(val.toLowerCase());
                case RegUsrName:
                    return adminEntity.fullName.toLowerCase().contains(val.toLowerCase());
                case ModUsrName:
                    return adminEntity2.fullName.toLowerCase().contains(val.toLowerCase());
            }
        }
        return null;
    }

    private BooleanExpression useTypeEq(YnTypeEnum useType) {
        if (useType != null)
            return languagePackViewEntity.useType.eq(useType);
        return null;
    }

    private OrderSpecifier sorterOrder(LanguagePackSorterEnum sorter) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        if (sorter == null)
            return languagePackViewEntity.regDt.desc();

        if (sorter == LanguagePackSorterEnum.LanguagePackAsc)
            return languagePackViewEntity.languageName.asc();

        if (sorter == LanguagePackSorterEnum.LanguagePackDesc)
            return languagePackViewEntity.languageName.desc();

        if (sorter == LanguagePackSorterEnum.NativeLanguageAsc)
            return languagePackViewEntity.nativeLanguage.asc();

        if (sorter == LanguagePackSorterEnum.NativeLanguageDesc)
            return languagePackViewEntity.nativeLanguage.desc();

        if (sorter == LanguagePackSorterEnum.UseAsc)
            return languagePackViewEntity.useType.desc();

        if (sorter == LanguagePackSorterEnum.UseDesc)
            return languagePackViewEntity.useType.asc();

        if (sorter == LanguagePackSorterEnum.CreatedAsc)
            return languagePackViewEntity.regDt.asc();

        if (sorter == LanguagePackSorterEnum.CreatedDesc)
            return languagePackViewEntity.regDt.desc();

        if (sorter == LanguagePackSorterEnum.CreaterNameAsc)
            return adminEntity.fullName.asc();

        if (sorter == LanguagePackSorterEnum.CreaterNameDesc)
            return adminEntity.fullName.desc();

        if (sorter == LanguagePackSorterEnum.ModifierNameAsc)
            return adminEntity2.fullName.asc();

        if (sorter == LanguagePackSorterEnum.ModifierNameDesc)
            return adminEntity2.fullName.desc();

        return languagePackViewEntity.regDt.desc();
    }

}
