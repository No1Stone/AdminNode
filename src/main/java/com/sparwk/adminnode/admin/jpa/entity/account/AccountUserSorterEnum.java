package com.sparwk.adminnode.admin.jpa.entity.account;

import lombok.Getter;

@Getter
public enum AccountUserSorterEnum {

    StageNameAsc("StageNameAsc"), StageNameDesc("StageNameDesc"),
    FullNameAsc("FullNameAsc"), FullNameDesc("FullNameDesc"),
    CreatedAsc("CreatedAsc"), CreatedDesc("CreatedDesc"),
    EmailAsc("EmailAsc"), EmailDesc("EmailDesc");

    private String sorter;

    AccountUserSorterEnum(String sorter) {
        this.sorter = sorter;
    }

}
