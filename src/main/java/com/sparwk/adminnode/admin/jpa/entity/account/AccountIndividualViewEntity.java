package com.sparwk.adminnode.admin.jpa.entity.account;


import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "view_admin_individual_account_list")
@DynamicUpdate
public class AccountIndividualViewEntity {
    @Id
    @Column(name = "profile_id")
    private Long profileId;

    @Column(name = "accnt_id")
    private Long accntId;

    @Column(name = "accnt_email")
    private String accntEmail;

    @Column(name = "passport_first_name")
    private String passportFirstName;

    @Column(name = "passport_middle_name")
    private String passportMiddleName;

    @Column(name = "passport_last_name")
    private String passportLastName;

    @Column(name = "full_name")
    private String fullName;

    @Column(name = "company_name")
    private String companyName;

    @Column(name = "roles_name")
    private String rolesName;

    @Enumerated(EnumType.STRING)
    @Column(name = "passport_verify_yn", length = 1)
    private YnTypeEnum passportVerifyYn;

    @Enumerated(EnumType.STRING)
    @Column(name = "ipi_verify_yn", length = 1)
    private YnTypeEnum ipiVerifyYn;

    @Column(name = "reg_dt")
    private LocalDateTime regDt;

    @Builder
    AccountIndividualViewEntity(
            Long profileId,
            Long accntId,
            String accntEmail,
            String passportFirstName,
            String passportMiddleName,
            String passportLastName,
            String fullName,
            String companyName,
            String rolesName,
            YnTypeEnum passportVerifyYn,
            YnTypeEnum ipiVerifyYn,
            LocalDateTime regDt
    ) {
        this.profileId = profileId;
        this.accntId = accntId;
        this.accntEmail = accntEmail;
        this.passportFirstName = passportFirstName;
        this.passportMiddleName = passportMiddleName;
        this.passportLastName = passportLastName;
        this.fullName = fullName;
        this.companyName = companyName;
        this.rolesName = rolesName;
        this.passportVerifyYn = passportVerifyYn;
        this.ipiVerifyYn = ipiVerifyYn;
        this.regDt = regDt;
    }

}
