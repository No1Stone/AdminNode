package com.sparwk.adminnode.admin.jpa.repository.songCode;

import com.sparwk.adminnode.admin.jpa.entity.songCode.SongCodeEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SongCodeRepository extends JpaRepository<SongCodeEntity, Long> {

    SongCodeEntity findByCode(String val);
}
