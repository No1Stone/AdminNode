package com.sparwk.adminnode.admin.jpa.repository.inquary.dsl;

import com.sparwk.adminnode.admin.biz.v1.email.dto.EmailListDto;
import com.sparwk.adminnode.admin.biz.v1.email.dto.EmailMemberListDto;
import com.sparwk.adminnode.admin.biz.v1.email.dto.EmailMemberMailListDto;
import com.sparwk.adminnode.admin.biz.v1.inquary.dto.InquaryDto;
import com.sparwk.adminnode.admin.jpa.entity.email.EmailCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.email.EmailSorterEnum;
import com.sparwk.adminnode.admin.jpa.entity.inquary.InquaryCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.inquary.InquarySorterEnum;
import org.springframework.data.domain.PageRequest;

import java.util.List;

public interface InquaryCustomRepository {

    List<InquaryDto> findQryAll(InquaryCateEnum cate,
                                String val,
                                InquarySorterEnum sorter,
                                PageRequest pageRequest);
    long countQryAll(InquaryCateEnum cate,
                     String val,
                     InquarySorterEnum sorter);

}
