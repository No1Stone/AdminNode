package com.sparwk.adminnode.admin.jpa.repository.song.dsl;

import com.querydsl.core.types.ExpressionUtils;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.CaseBuilder;
import com.querydsl.jpa.JPAExpressions;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.sparwk.adminnode.admin.biz.v1.song.dto.SongEvaluationDtailListDto;
import com.sparwk.adminnode.admin.biz.v1.song.dto.SongEvaluationDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.sparwk.adminnode.admin.jpa.entity.anr.QAnrEntity.anrEntity;
import static com.sparwk.adminnode.admin.jpa.entity.commonCode.QCommonDetailCodeEntity.commonDetailCodeEntity;
import static com.sparwk.adminnode.admin.jpa.entity.profile.QProfileCompanyEntity.profileCompanyEntity;
import static com.sparwk.adminnode.admin.jpa.entity.profile.QProfileEntity.profileEntity;
import static com.sparwk.adminnode.admin.jpa.entity.song.QSongEvaluationAnrEntity.songEvaluationAnrEntity;
import static com.sparwk.adminnode.admin.jpa.entity.song.QSongEvaluationEntity.songEvaluationEntity;
import static com.sparwk.adminnode.admin.jpa.entity.song.QSongEvaluationRateEntity.songEvaluationRateEntity;
import static com.sparwk.adminnode.admin.jpa.entity.song.QSongPitchListAnrEntity.songPitchListAnrEntity;


@Repository
public class SongEvaluationCustomRepositoryImpl implements SongEvaluationCustomRepository {

    private final JPAQueryFactory jpaQueryFactory;

    private final Logger logger = LoggerFactory.getLogger(SongEvaluationCustomRepositoryImpl.class);

    public SongEvaluationCustomRepositoryImpl(JPAQueryFactory jpaQueryFactory) {
        this.jpaQueryFactory = jpaQueryFactory;
    }

    @Override
    public List<SongEvaluationDto> findQryEvaluation(Long id) {

        //서브쿼리 subquery 사용
        return jpaQueryFactory.select(Projections.bean(SongEvaluationDto.class,
                        songEvaluationEntity.evalSeq,
                        songEvaluationEntity.songId,
                        new CaseBuilder()
                                .when(songEvaluationEntity.evalOwnerId.eq(songEvaluationAnrEntity.evalAnrId))
                                .then("Owner")
                                .otherwise("Member")
                                .as("memberType"),
                        songEvaluationAnrEntity.evalAnrId,
                        songEvaluationAnrEntity.evalAnrSeq,
                        profileEntity.fullName,
                        songPitchListAnrEntity.anrProfileCompanyId,
                        profileCompanyEntity.profileCompanyName,
                        ExpressionUtils.as(
                                JPAExpressions.select( anrEntity.anrServiceName)
                                        .from(anrEntity)
                                        .where(anrEntity.anrCd.eq(songEvaluationEntity.evalStatus)),
                                "evalStatusName"),

                        ExpressionUtils.as(
                                JPAExpressions.select( songEvaluationRateEntity.evalRate.sum())
                                        .from(songEvaluationRateEntity)
                                        .where(songEvaluationRateEntity.evalAnrSeq.eq(songEvaluationAnrEntity.evalAnrSeq)),
                                "totalRate")
                        )
                )
                .from(songEvaluationEntity)
                .leftJoin(songEvaluationAnrEntity).on(songEvaluationAnrEntity.evalSeq.eq(songEvaluationEntity.evalSeq))
                .leftJoin(profileEntity).on(profileEntity.profileId.eq(songEvaluationAnrEntity.evalAnrId))
                .leftJoin(songPitchListAnrEntity).on(songPitchListAnrEntity.pitchlistId.eq(songEvaluationEntity.pitchlistId))
                .leftJoin(profileCompanyEntity).on(profileCompanyEntity.profileId.eq(songPitchListAnrEntity.anrProfileCompanyId))
                .where(songEvaluationEntity.songId.eq(id))
                .fetch();
    }

    @Override
    public List<SongEvaluationDtailListDto> findQryEvaluationDetailList(Long id, Long seq) {

        //서브쿼리 subquery 사용
        return jpaQueryFactory.select(Projections.bean(SongEvaluationDtailListDto.class,
                                songEvaluationEntity.evalSeq,
                                songEvaluationEntity.songId,
                                songEvaluationAnrEntity.evalAnrSeq,
                                songEvaluationRateEntity.evalTemplDtlCd,
                                ExpressionUtils.as(
                                        JPAExpressions.select(commonDetailCodeEntity.val)
                                                .from(commonDetailCodeEntity)
                                                .where(commonDetailCodeEntity.dcode.eq(songEvaluationRateEntity.evalTemplDtlCd)),
                                        "evalTemplDtlCdName"),
                                songEvaluationRateEntity.evalRate
                        )
                )
                .from(songEvaluationEntity)
                .leftJoin(songEvaluationAnrEntity).on(songEvaluationAnrEntity.evalSeq.eq(songEvaluationEntity.evalSeq))
                .leftJoin(songEvaluationRateEntity).on(songEvaluationRateEntity.evalAnrSeq.eq(songEvaluationAnrEntity.evalAnrSeq))
                .where(songEvaluationEntity.songId.eq(id), songEvaluationAnrEntity.evalAnrSeq.eq(seq))
                .fetch();
    }
}
