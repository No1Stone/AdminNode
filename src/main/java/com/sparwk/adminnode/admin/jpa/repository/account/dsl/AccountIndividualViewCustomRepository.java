package com.sparwk.adminnode.admin.jpa.repository.account.dsl;

import com.sparwk.adminnode.admin.biz.v1.account.dto.IndividualMemberViewListDto;
import com.sparwk.adminnode.admin.jpa.entity.PeriodTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.account.VerificationIndividualAccountCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.account.VerificationIndividualAccountFilterEnum;
import com.sparwk.adminnode.admin.jpa.entity.account.VerificationIndividualAccountSorterEnum;
import org.springframework.data.domain.PageRequest;

import java.util.List;

public interface AccountIndividualViewCustomRepository {
    //인증하기 전 User List
    List<IndividualMemberViewListDto> findQryVerificationMember(VerificationIndividualAccountCateEnum cate,
                                                              String val,
                                                              PeriodTypeEnum periodType,
                                                              String sdate,
                                                              String edate,
                                                              VerificationIndividualAccountFilterEnum filter,
                                                              VerificationIndividualAccountSorterEnum sorter,
                                                              PageRequest pageRequest);

    long countQryVerificationMember(VerificationIndividualAccountCateEnum cate,
                                  String val,
                                  PeriodTypeEnum periodType,
                                  String sdate,
                                  String edate,
                                  VerificationIndividualAccountFilterEnum filter);
}
