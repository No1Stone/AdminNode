package com.sparwk.adminnode.admin.jpa.entity.account;


import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "view_admin_associate_mamber_list")
@DynamicUpdate
public class AccountAssoiateViewEntity {
    @Id
    @Column(name = "profile_id")
    private Long profileId;

    @Column(name = "accnt_id")
    private Long accntId;

    @Column(name = "accnt_email")
    private String accntEmail;

    @Column(name = "passport_first_name")
    private String passportFirstName;

    @Column(name = "passport_middle_name")
    private String passportMiddleName;

    @Column(name = "passport_last_name")
    private String passportLastName;

    @Column(name = "full_name")
    private String fullName;

    @Enumerated(EnumType.STRING)
    @Column(name = "stage_name_yn", length = 1)
    private YnTypeEnum stageNameYn;

    @Column(name = "bth_year")
    private String bthYear;

    @Column(name = "bth_month")
    private String bthMonth;

    @Column(name = "bth_day")
    private String bthDay;

    @Column(name = "gender")
    private String gender;

    @Column(name = "reg_dt")
    private LocalDateTime regDt;

    @Column(name = "mod_dt")
    private LocalDateTime modDt;

    @Builder
    AccountAssoiateViewEntity(
            Long profileId,
            Long accntId,
            String accntEmail,
            String passportFirstName,
            String passportMiddleName,
            String passportLastName,
            String fullName,
            YnTypeEnum stageNameYn,
            String bthYear,
            String bthMonth,
            String bthDay,
            String gender,
            LocalDateTime regDt,
            LocalDateTime modDt
    ) {
        this.profileId = profileId;
        this.accntId = accntId;
        this.accntEmail = accntEmail;
        this.passportFirstName = passportFirstName;
        this.passportMiddleName = passportMiddleName;
        this.passportLastName = passportLastName;
        this.fullName = fullName;
        this.stageNameYn = stageNameYn;
        this.bthYear = bthYear;
        this.bthMonth = bthMonth;
        this.bthDay = bthDay;
        this.gender = gender;
        this.regDt = regDt;
        this.modDt = modDt;
    }

}
