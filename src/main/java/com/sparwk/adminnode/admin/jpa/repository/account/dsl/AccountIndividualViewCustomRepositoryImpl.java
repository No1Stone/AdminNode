package com.sparwk.adminnode.admin.jpa.repository.account.dsl;

import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.sparwk.adminnode.admin.biz.v1.account.dto.IndividualMemberViewListDto;
import com.sparwk.adminnode.admin.jpa.entity.PeriodTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.account.VerificationIndividualAccountCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.account.VerificationIndividualAccountFilterEnum;
import com.sparwk.adminnode.admin.jpa.entity.account.VerificationIndividualAccountSorterEnum;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import static com.sparwk.adminnode.admin.jpa.entity.account.QAccountIndividualViewEntity.accountIndividualViewEntity;


@Repository
public class AccountIndividualViewCustomRepositoryImpl implements AccountIndividualViewCustomRepository {

    private final JPAQueryFactory jpaQueryFactory;

    public AccountIndividualViewCustomRepositoryImpl(JPAQueryFactory jpaQueryFactory) {
        this.jpaQueryFactory = jpaQueryFactory;
    }

    //인증하기 전 User List
    @Override
    public List<IndividualMemberViewListDto> findQryVerificationMember(VerificationIndividualAccountCateEnum cate,
                                                                     String val,
                                                                     PeriodTypeEnum periodType,
                                                                     String sdate,
                                                                     String edate,
                                                                     VerificationIndividualAccountFilterEnum filter,
                                                                     VerificationIndividualAccountSorterEnum sorter,
                                                                     PageRequest pageRequest) {
        return jpaQueryFactory.select(Projections.bean(IndividualMemberViewListDto.class,
                        accountIndividualViewEntity.accntId,
                        accountIndividualViewEntity.profileId,
                        accountIndividualViewEntity.accntEmail,
                        accountIndividualViewEntity.passportFirstName,
                        accountIndividualViewEntity.passportMiddleName,
                        accountIndividualViewEntity.passportLastName,
                        accountIndividualViewEntity.fullName.as("profileFullName"),
                        accountIndividualViewEntity.companyName,
                        accountIndividualViewEntity.rolesName,
                        accountIndividualViewEntity.passportVerifyYn,
                        accountIndividualViewEntity.ipiVerifyYn,
                        accountIndividualViewEntity.regDt
                        )
                )
                .from(accountIndividualViewEntity)
                .where(valLike(cate, val), filterLike(filter))
                .limit(pageRequest.getPageSize())
                .offset(pageRequest.getPageSize() * (pageRequest.getPageNumber() - 1))
                .orderBy(sorterOrder(sorter).stream().toArray(OrderSpecifier[]::new))
                .fetch();
    }

    //인증하기 전 User data count
    @Override
    public long countQryVerificationMember(VerificationIndividualAccountCateEnum cate,
                                         String val,
                                         PeriodTypeEnum periodType,
                                         String sdate,
                                         String edate,
                                         VerificationIndividualAccountFilterEnum filter) {
        return jpaQueryFactory.select(
                        accountIndividualViewEntity.accntId.count()
                )
                .from(accountIndividualViewEntity)
                .where(valLike(cate, val), filterLike(filter))
                .fetchOne();
    }



    private BooleanExpression filterLike(VerificationIndividualAccountFilterEnum filter) {
        if (filter != null) {
            switch (filter) {
                case All:
                    return null;
                case PassportVerify:
                    return accountIndividualViewEntity.passportVerifyYn.eq(YnTypeEnum.Y);
                case PassportUnconfirmed:
                    return accountIndividualViewEntity.passportVerifyYn.isNull();
                case PassportReject:
                    return accountIndividualViewEntity.passportVerifyYn.eq(YnTypeEnum.N);
                case IpiVerify:
                    return accountIndividualViewEntity.ipiVerifyYn.eq(YnTypeEnum.Y);
                case IpiUnconfirmed:
                    return accountIndividualViewEntity.ipiVerifyYn.isNull();
                case IpiReject:
                    return accountIndividualViewEntity.ipiVerifyYn.eq(YnTypeEnum.N);
            }
        }
        return null;

    }

    private BooleanExpression valLike(VerificationIndividualAccountCateEnum cate, String val) {
        if (cate != null && val != null && val.length() > 0) {
            switch (cate) {
                case All:
                    return accountIndividualViewEntity.accntEmail.toLowerCase().contains(val.toLowerCase())
                            .or(accountIndividualViewEntity.fullName.toLowerCase().contains(val.toLowerCase()))
                            .or(accountIndividualViewEntity.companyName.toLowerCase().contains(val.toLowerCase()));
                case Email:
                    return accountIndividualViewEntity.accntEmail.toLowerCase().contains(val.toLowerCase());
                case FullName:
                    return accountIndividualViewEntity.fullName.toLowerCase().contains(val.toLowerCase());
                case Company:
                    return accountIndividualViewEntity.companyName.toLowerCase().contains(val.toLowerCase());
                case Role:
                    return accountIndividualViewEntity.rolesName.toLowerCase().contains(val.toLowerCase());
                default:
                    return null;
            }
        }
        return null;
    }

    private BooleanExpression termBetween(PeriodTypeEnum periodType, String  sdate, String edate) {

        if (sdate != null && edate != null && periodType != null) {

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
            LocalDateTime searchStartDate = LocalDateTime.parse(sdate + " 00:00:00.000", formatter);
            LocalDateTime searchEndDate = LocalDateTime.parse(edate + " 23:59:59.999", formatter);

            switch (periodType) {
                case Created:
                    return accountIndividualViewEntity.regDt.between(searchStartDate, searchEndDate);
                default:
                    return null;
            }
        }
        return null;
    }

    private List<OrderSpecifier> sorterOrder(VerificationIndividualAccountSorterEnum sorter) {

        List<OrderSpecifier> orders = new ArrayList<>();

        if (sorter == null) {
            orders.add(accountIndividualViewEntity.regDt.desc());
            return orders;
        }

        if (sorter == VerificationIndividualAccountSorterEnum.EmailAsc) {
            orders.add(accountIndividualViewEntity.accntEmail.asc());
            return orders;
        }

        if (sorter == VerificationIndividualAccountSorterEnum.EmailDesc) {
            orders.add(accountIndividualViewEntity.accntEmail.desc());
            return orders;
        }

        if (sorter == VerificationIndividualAccountSorterEnum.FullNameAsc) {
            orders.add(accountIndividualViewEntity.passportFirstName.asc());
            return orders;
        }

        if (sorter == VerificationIndividualAccountSorterEnum.FullNameDesc) {
            orders.add(accountIndividualViewEntity.passportFirstName.desc());
            return orders;
        }

        if (sorter == VerificationIndividualAccountSorterEnum.CompanyAsc) {
            orders.add(accountIndividualViewEntity.companyName.asc());
            return orders;
        }

        if (sorter == VerificationIndividualAccountSorterEnum.CompanyDesc) {
            orders.add(accountIndividualViewEntity.companyName.desc());
            return orders;
        }

        if (sorter == VerificationIndividualAccountSorterEnum.RolesAsc) {
            orders.add(accountIndividualViewEntity.rolesName.asc());
            return orders;
        }

        if (sorter == VerificationIndividualAccountSorterEnum.RolesDesc) {
            orders.add(accountIndividualViewEntity.rolesName.desc());
            return orders;
        }

        if (sorter == VerificationIndividualAccountSorterEnum.CreatedAsc) {
            orders.add(accountIndividualViewEntity.regDt.asc());
            return orders;
        }

        if (sorter == VerificationIndividualAccountSorterEnum.CreatedDesc) {
            orders.add(accountIndividualViewEntity.regDt.desc());
            return orders;
        }

        orders.add(accountIndividualViewEntity.regDt.desc());
        return orders;
    }
}
