package com.sparwk.adminnode.admin.jpa.entity.email;

import lombok.Getter;

@Getter
public enum EmailCateEnum {

    All("All"),
    Subject("Subject"),
    RegUsrName("RegUsrName")
    ;

    private String cate;

    EmailCateEnum(String cate) {
        this.cate = cate;
    }
}
