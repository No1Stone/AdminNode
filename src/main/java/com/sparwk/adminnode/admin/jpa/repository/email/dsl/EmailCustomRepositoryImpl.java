package com.sparwk.adminnode.admin.jpa.repository.email.dsl;

import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.sparwk.adminnode.admin.biz.v1.email.dto.EmailListDto;
import com.sparwk.adminnode.admin.biz.v1.email.dto.EmailMemberListDto;
import com.sparwk.adminnode.admin.biz.v1.email.dto.EmailMemberMailListDto;
import com.sparwk.adminnode.admin.jpa.entity.commonCode.CommonSorterEnum2;
import com.sparwk.adminnode.admin.jpa.entity.email.EmailCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.email.EmailMemberCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.email.EmailSorterEnum;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

import static com.sparwk.adminnode.admin.jpa.entity.admin.QAdminEntity.adminEntity;
import static com.sparwk.adminnode.admin.jpa.entity.commonCode.QCommonOrganizationCodeEntity.commonOrganizationCodeEntity;
import static com.sparwk.adminnode.admin.jpa.entity.email.QEmailEntity.emailEntity;
import static com.sparwk.adminnode.admin.jpa.entity.email.QEmailMemberViewEntity.emailMemberViewEntity;

@Repository
public class EmailCustomRepositoryImpl implements EmailCustomRepository {

    private final JPAQueryFactory jpaQueryFactory;

    public EmailCustomRepositoryImpl(JPAQueryFactory jpaQueryFactory) {
        this.jpaQueryFactory = jpaQueryFactory;
    }

    @Override
    public List<EmailListDto> findQryAll(EmailCateEnum cate,
                                         String val,
                                         EmailSorterEnum sorter,
                                         PageRequest pageRequest) {
        return jpaQueryFactory.select(Projections.bean(EmailListDto.class,
                                emailEntity.emailSeq,
                                emailEntity.subject,
                                emailEntity.maxAttachCnt,
                                emailEntity.memberCnt,
                                emailEntity.lastSendDt,
                                emailEntity.regUsr,
                                adminEntity.fullName.as("regUsrName"),
                                emailEntity.regDt
                        )
                )
                .from(emailEntity)
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(emailEntity.regUsr))
                .where(valLike(cate, val))
                .limit(pageRequest.getPageSize())
                .offset(pageRequest.getPageSize() * (pageRequest.getPageNumber() - 1))
                .orderBy(sorterOrder(sorter))
                .fetch();
    }

    @Override
    public long countQryAll(EmailCateEnum cate,
                            String val,
                            EmailSorterEnum sorter) {
        return jpaQueryFactory.select(
                                emailEntity.emailSeq.count()
                )
                .from(emailEntity)
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(emailEntity.regUsr))
                .where(valLike(cate, val))
                .fetchOne();
    }

    @Override
    public List<EmailMemberMailListDto> findQryMemberAll(EmailMemberCateEnum cate,
                                                         String val,
                                                         String indiYn,
                                                         String comYn,
                                                         String etcYn) {
        return jpaQueryFactory.select(Projections.bean(EmailMemberMailListDto.class,
                        emailMemberViewEntity.accntEmail.as("email")
                        )
                )
                .from(emailMemberViewEntity)
                .where(valLike2(cate, val), checkYn(indiYn, comYn, etcYn))
                .orderBy(emailMemberViewEntity.fullName.asc())
                .fetch();
    }

    @Override
    public List<EmailMemberListDto> findQryMemberList(EmailMemberCateEnum cate,
                                                      String val,
                                                      String indiYn,
                                                      String comYn,
                                                      String etcYn,
                                                      PageRequest pageRequest) {
        return jpaQueryFactory.select(Projections.bean(EmailMemberListDto.class,
                                emailMemberViewEntity.profileId,
                                emailMemberViewEntity.fullName,
                                emailMemberViewEntity.accntEmail,
                                emailMemberViewEntity.profileType
                        )
                )
                .from(emailMemberViewEntity)
                .where(valLike2(cate, val), checkYn(indiYn, comYn, etcYn))
                .limit(pageRequest.getPageSize())
                .offset(pageRequest.getPageSize() * (pageRequest.getPageNumber() - 1))
                .orderBy(emailMemberViewEntity.fullName.asc())
                .fetch();
    }

    @Override
    public long countQryMember(EmailMemberCateEnum cate,
                            String val,
                            String indiYn,
                            String comYn,
                            String etcYn) {
        return jpaQueryFactory.select(
                        emailMemberViewEntity.profileId.count()
                )
                .from(emailMemberViewEntity)
                .where(valLike2(cate, val), checkYn(indiYn, comYn, etcYn))
                .fetchOne();
    }

    private BooleanExpression valLike(EmailCateEnum cate, String val) {
        if (cate !=null && val != null && val.length() > 0) {
            switch (cate) {
                case All:
                    return emailEntity.subject.toLowerCase().contains(val.toLowerCase());
                case Subject:
                    return emailEntity.subject.toLowerCase().contains(val.toLowerCase());
                case RegUsrName:
                    return adminEntity.fullName.toLowerCase().contains(val.toLowerCase());
            }
        }
        return null;
    }

    private BooleanExpression valLike2(EmailMemberCateEnum cate, String val) {
        if (cate !=null && val != null && val.length() > 0) {
            switch (cate) {
                case All:
                    return emailMemberViewEntity.fullName.toLowerCase().contains(val.toLowerCase())
                            .or(emailMemberViewEntity.accntEmail.toLowerCase().contains(val.toLowerCase()));
                case Name:
                    return emailMemberViewEntity.fullName.toLowerCase().contains(val.toLowerCase());
                case Email:
                    return emailMemberViewEntity.accntEmail.toLowerCase().contains(val.toLowerCase());
            }
        }
        return null;
    }

    private BooleanExpression checkYn(String val1, String val2, String val3) {

        List<String> where = new ArrayList<>();
        if (val1.equals("Y")) {
            where.add("I");
        }
        if (val2.equals("Y")) {
            where.add("C");
        }
        if (val3.equals("Y")) {
            where.add("E");
        }

        if(where.size() > 0)
            return emailMemberViewEntity.profileType.in(where);
        else
            return null;
    }

    private OrderSpecifier sorterOrder(EmailSorterEnum sorter) {
        if (sorter == null)
            return emailEntity.regDt.desc();

        if (sorter == EmailSorterEnum.SubjectAsc)
            return emailEntity.subject.asc();

        if (sorter == EmailSorterEnum.SubjectDesc)
            return emailEntity.subject.desc();

        if (sorter == EmailSorterEnum.MemberAsc)
            return emailEntity.memberCnt.asc();

        if (sorter == EmailSorterEnum.MemberDesc)
            return emailEntity.memberCnt.desc();

        if (sorter == EmailSorterEnum.SdateAsc)
            return emailEntity.lastSendDt.asc();

        if (sorter == EmailSorterEnum.SdateDesc)
            return emailEntity.lastSendDt.desc();

        if (sorter == EmailSorterEnum.CreaterNameAsc)
            return adminEntity.fullName.asc();

        if (sorter == EmailSorterEnum.CreaterNameDesc)
            return adminEntity.fullName.desc();

        return emailEntity.regDt.desc();
    }

}
