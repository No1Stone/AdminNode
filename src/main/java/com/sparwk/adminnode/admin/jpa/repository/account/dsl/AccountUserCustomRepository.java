package com.sparwk.adminnode.admin.jpa.repository.account.dsl;

import com.sparwk.adminnode.admin.biz.v1.account.dto.AccountMemberMailDto;
import com.sparwk.adminnode.admin.biz.v1.account.dto.AccountPassportDto;
import com.sparwk.adminnode.admin.biz.v1.account.dto.VerifiedMemberListDto;
import com.sparwk.adminnode.admin.biz.v1.profile.dto.ProfileCurrentPositionDto;
import com.sparwk.adminnode.admin.biz.v1.profile.dto.ProfileMetaDataDto;
import com.sparwk.adminnode.admin.biz.v1.profile.dto.ProfileSnsDto;
import com.sparwk.adminnode.admin.jpa.entity.PeriodTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.account.MaintenanceVerifiedMemberCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.account.MaintenanceVerifiedMemberSorterEnum;
import org.springframework.data.domain.PageRequest;

import java.util.List;
import java.util.Optional;

public interface AccountUserCustomRepository {
    //인증받은 User List
    List<VerifiedMemberListDto> findQryVerifiedMember(MaintenanceVerifiedMemberCateEnum cate,
                                                      String val,
                                                      PeriodTypeEnum periodType,
                                                      String sdate,
                                                      String edate,
                                                      MaintenanceVerifiedMemberSorterEnum sorter,
                                                      PageRequest pageRequest);

    //인증받은 User List
    long countQryVerifiedMember(MaintenanceVerifiedMemberCateEnum cate,
                                                      String val,
                                                      PeriodTypeEnum periodType,
                                                      String sdate,
                                                      String edate);

    List<ProfileSnsDto> findQrySns(Long profileId);
    List<ProfileMetaDataDto> findQryGender(Long profileId);
    List<ProfileMetaDataDto> findQryLanuage(Long profileId);
    List<ProfileMetaDataDto> findQryRoles(Long profileId);
    List<ProfileCurrentPositionDto> findQryPosition(Long profileId);
    List<ProfileMetaDataDto> findQryTerritorisOrCountry(Long profileId);
    List<ProfileMetaDataDto> findQryCommon(String pcode, Long profileId);
    List<ProfileMetaDataDto> findQrySongs(String pcode, Long profileId);

    Optional<AccountPassportDto> finQryPassport(Long accntId);
    Optional<ProfileMetaDataDto> findQryCurrentCountry(Long profileId);
    Optional<ProfileMetaDataDto> findQryHomeTownCountry(Long profileId);
    Optional<ProfileMetaDataDto> findQryPhoneCountryCdName(Long accntId);
    Optional<AccountMemberMailDto> findQryMemberMail(Long profileId);

}
