package com.sparwk.adminnode.admin.jpa.repository.projects;

import com.sparwk.adminnode.admin.jpa.entity.projects.ProjectEntity;
import com.sparwk.adminnode.admin.jpa.repository.projects.dsl.ProjectCustomRepository;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProjectRepository extends JpaRepository<ProjectEntity, Long>, ProjectCustomRepository {

}
