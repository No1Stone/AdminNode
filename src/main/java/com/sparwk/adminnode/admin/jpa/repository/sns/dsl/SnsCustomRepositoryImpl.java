package com.sparwk.adminnode.admin.jpa.repository.sns.dsl;

import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.sparwk.adminnode.admin.biz.v1.sns.dto.SnsDto;
import com.sparwk.adminnode.admin.jpa.entity.PeriodTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.admin.QAdminEntity;
import com.sparwk.adminnode.admin.jpa.entity.commonCode.CommonSorterEnum2;
import com.sparwk.adminnode.admin.jpa.entity.sns.SnsCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.sns.SnsSorterEnum;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static com.sparwk.adminnode.admin.jpa.entity.admin.QAdminEntity.adminEntity;
import static com.sparwk.adminnode.admin.jpa.entity.commonCode.QCommonOrganizationCodeEntity.commonOrganizationCodeEntity;
import static com.sparwk.adminnode.admin.jpa.entity.sns.QSnsEntity.snsEntity;

@Repository
public class SnsCustomRepositoryImpl implements SnsCustomRepository {

    private final JPAQueryFactory jpaQueryFactory;

    public SnsCustomRepositoryImpl(JPAQueryFactory jpaQueryFactory) {
        this.jpaQueryFactory = jpaQueryFactory;
    }

    @Override
    public List<SnsDto> findQryAll(SnsCateEnum cate,
                                   String val,
                                   YnTypeEnum useType,
                                   PeriodTypeEnum periodType,
                                   String sdate,
                                   String edate,
                                   SnsSorterEnum sorter,
                                   PageRequest pageRequest) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(Projections.bean(SnsDto.class,
                                snsEntity.snsSeq,
                                snsEntity.snsName,
                                snsEntity.snsCd,
                                snsEntity.snsIconUrl,
                                snsEntity.useType,
                                snsEntity.regUsr,
                                adminEntity.fullName.as("regUsrName"),
                                snsEntity.regDt,
                                snsEntity.modUsr,
                                adminEntity2.fullName.as("modUsrName"),
                                snsEntity.modDt
                        )
                )
                .from(snsEntity)
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(snsEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(snsEntity.modUsr))
                .where(valLike(cate, val), useTypeEq(useType), termBetween(periodType, sdate, edate))
                .limit(pageRequest.getPageSize())
                .offset(pageRequest.getPageSize() * (pageRequest.getPageNumber() - 1))
                .orderBy(sorterOrder(sorter))
                .fetch();
    }

    @Override
    public long countQryAll(SnsCateEnum cate,
                                   String val,
                                   YnTypeEnum useType,
                                   PeriodTypeEnum periodType,
                                   String sdate,
                                   String edate) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(
                                snsEntity.snsSeq.count()
                )
                .from(snsEntity)
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(snsEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(snsEntity.modUsr))
                .where(valLike(cate, val), useTypeEq(useType), termBetween(periodType, sdate, edate))
                .fetchOne();
    }

    @Override
    public List<SnsDto> findQryUseY(SnsCateEnum cate, String val) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(Projections.bean(SnsDto.class,
                                snsEntity.snsSeq,
                                snsEntity.snsName,
                                snsEntity.snsCd,
                                snsEntity.snsIconUrl,
                                snsEntity.useType,
                                snsEntity.regUsr,
                                adminEntity.fullName.as("regUsrName"),
                                snsEntity.regDt,
                                snsEntity.modUsr,
                                adminEntity2.fullName.as("modUsrName"),
                                snsEntity.modDt
                        )
                )
                .from(snsEntity)
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(snsEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(snsEntity.modUsr))
                .where(valLike(cate, val), useTypeEq(YnTypeEnum.Y))
                .orderBy(sorterOrder(SnsSorterEnum.NameAsc))
                .fetch();
    }

    @Override
    public long countQryUseY(SnsCateEnum cate,
                            String val) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(
                        snsEntity.snsSeq.count()
                )
                .from(snsEntity)
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(snsEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(snsEntity.modUsr))
                .where(valLike(cate, val), useTypeEq(YnTypeEnum.Y))
                .fetchOne();
    }

    private BooleanExpression valLike(SnsCateEnum cate, String val) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        if (cate != null && val != null && val.length() > 0) {
            switch (cate) {
                case All:
                    return snsEntity.snsName.toLowerCase().contains(val.toLowerCase());
                case SNS:
                    return snsEntity.snsName.toLowerCase().contains(val.toLowerCase());
                case RegUsrName:
                    return adminEntity.fullName.toLowerCase().contains(val.toLowerCase());
                case ModUsrName:
                    return adminEntity2.fullName.toLowerCase().contains(val.toLowerCase());
            }
        }
        return null;
    }

    private BooleanExpression useTypeEq(YnTypeEnum useType) {
        if (useType != null)
            return snsEntity.useType.eq(useType);
        return null;
    }

    private BooleanExpression termBetween(PeriodTypeEnum periodType, String  sdate, String edate) {

        if (sdate != null && edate != null && periodType != null) {

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
            LocalDateTime searchStartDate = LocalDateTime.parse(sdate + " 00:00:00.000", formatter);
            LocalDateTime searchEndDate = LocalDateTime.parse(edate + " 23:59:59.999", formatter);

            switch (periodType) {
                case Created:
                    return snsEntity.regDt.between(searchStartDate, searchEndDate);
                case LastModified:
                    return snsEntity.modDt.between(searchStartDate, searchEndDate);
                default:
                    return null;
            }
        }
        return null;
    }

    private OrderSpecifier sorterOrder(SnsSorterEnum sorter) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        if (sorter == null)
            return snsEntity.regDt.desc();

        if (sorter == SnsSorterEnum.NameAsc)
            return snsEntity.snsName.asc();

        if (sorter == SnsSorterEnum.NameDesc)
            return snsEntity.snsName.desc();

        if (sorter == SnsSorterEnum.UseYnAsc)
            return snsEntity.useType.asc();

        if (sorter == SnsSorterEnum.UseYnDesc)
            return snsEntity.useType.desc();

        if (sorter == SnsSorterEnum.LastModifiedAsc)
            return snsEntity.modDt.asc();

        if (sorter == SnsSorterEnum.LastModifiedDesc)
            return snsEntity.modDt.desc();

        if (sorter == SnsSorterEnum.CreatedAsc)
            return snsEntity.regDt.asc();

        if (sorter == SnsSorterEnum.CreatedDesc)
            return snsEntity.regDt.desc();

        if (sorter == SnsSorterEnum.CreaterNameAsc)
            return adminEntity.fullName.asc();

        if (sorter == SnsSorterEnum.CreaterNameDesc)
            return adminEntity.fullName.desc();

        if (sorter == SnsSorterEnum.ModifierNameAsc)
            return adminEntity2.fullName.asc();

        if (sorter == SnsSorterEnum.ModifierNameDesc)
            return adminEntity2.fullName.desc();

        return snsEntity.regDt.desc();
    }

}
