package com.sparwk.adminnode.admin.jpa.repository.account;

import com.sparwk.adminnode.admin.jpa.entity.account.AccountPassportEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountPassportRepository extends JpaRepository<AccountPassportEntity, Long> {

}
