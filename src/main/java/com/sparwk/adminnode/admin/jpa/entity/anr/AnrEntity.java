package com.sparwk.adminnode.admin.jpa.entity.anr;

import com.sparwk.adminnode.admin.jpa.entity.BaseEntity;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name="tb_admin_anr_code")
public class AnrEntity extends BaseEntity {

    @Id
    @GeneratedValue(
            generator = "tb_admin_anr_code_seq"
    )
    @Column(name = "anr_seq")
    private Long anrSeq;

    @Column(name = "anr_service_name", length = 50)
    private String anrServiceName;

    @Column(name = "anr_cd", length = 9)
    private String anrCd;

    @Column(name = "anr_group_cd", length = 9)
    private String anrGroupCd;

    @Column(name = "description", length = 500)
    private String description;

    @Enumerated(EnumType.STRING)
    @Column(name = "use_yn", length = 1)
    private YnTypeEnum useType;

    @Builder
    public AnrEntity(
                            Long anrSeq,
                            String anrServiceName,
                            String anrCd,
                            String anrGroupCd,
                            String description,
                            YnTypeEnum useType
                          ) {
        this.anrSeq = anrSeq;
        this.anrServiceName = anrServiceName;
        this.anrCd = anrCd;
        this.anrGroupCd = anrGroupCd;
        this.description = description;
        this.useType = useType;
    }

    public void updateYn(YnTypeEnum useType) {
        this.useType = useType;
    }

    public void update(
            String anrServiceName,
            String anrCd,
            String anrGroupCd,
            String description,
            YnTypeEnum useType
                        ) {
        this.anrServiceName = anrServiceName;
        this.anrCd = anrCd;
        this.anrGroupCd = anrGroupCd;
        this.description = description;
        this.useType = useType;
    }


}
