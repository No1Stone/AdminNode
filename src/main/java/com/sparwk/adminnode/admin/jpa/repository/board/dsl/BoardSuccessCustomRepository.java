package com.sparwk.adminnode.admin.jpa.repository.board.dsl;

import com.sparwk.adminnode.admin.biz.v1.board.dto.BoardNewsDto;
import com.sparwk.adminnode.admin.biz.v1.board.dto.BoardSuccessDto;
import com.sparwk.adminnode.admin.jpa.entity.PeriodTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.board.BoardCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.board.BoardSorterEnum;
import com.sparwk.adminnode.admin.jpa.entity.board.SuccessCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.board.SuccessSorterEnum;
import org.springframework.data.domain.PageRequest;

import java.util.List;

public interface BoardSuccessCustomRepository {

    List<BoardSuccessDto> findQryAll(    SuccessCateEnum cate,
                                      String val,
                                      YnTypeEnum useType,
                                      PeriodTypeEnum periodType,
                                      String sdate,
                                      String edate,
                                         SuccessSorterEnum sorter,
                                      PageRequest pageRequest
    );

    long countQryAll(    SuccessCateEnum cate,
                                         String val,
                                         YnTypeEnum useType,
                                         PeriodTypeEnum periodType,
                                         String sdate,
                                         String edate
    );

    List<BoardSuccessDto> findQryUseY();
    long countQryUseY();

    long deleteQryList(Long[] id);

}
