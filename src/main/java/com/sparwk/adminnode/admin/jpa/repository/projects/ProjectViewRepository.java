package com.sparwk.adminnode.admin.jpa.repository.projects;

import com.sparwk.adminnode.admin.jpa.entity.projects.ProjectViewEntity;
import com.sparwk.adminnode.admin.jpa.repository.projects.dsl.ProjectViewCustomRepository;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProjectViewRepository extends JpaRepository<ProjectViewEntity, Long>, ProjectViewCustomRepository {

}
