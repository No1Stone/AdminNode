package com.sparwk.adminnode.admin.jpa.repository.timezone;

import com.sparwk.adminnode.admin.jpa.entity.timezone.TimezoneEntity;
import com.sparwk.adminnode.admin.jpa.repository.timezone.dsl.TimezoneCustomRepository;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TimezoneRepository extends JpaRepository<TimezoneEntity, Long>, TimezoneCustomRepository {
}
