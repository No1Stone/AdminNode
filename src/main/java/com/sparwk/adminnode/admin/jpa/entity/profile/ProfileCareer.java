package com.sparwk.adminnode.admin.jpa.entity.profile;


import com.sparwk.adminnode.admin.jpa.entity.BaseEntity;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.profile.id.ProfileCareerId;
import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_profile_career")
@DynamicUpdate
@IdClass(ProfileCareerId.class)
public class ProfileCareer extends BaseEntity {

    @Id
    @Column(name = "profile_id")
    private Long profileId;
    @Id
    @Column(name = "career_organization_nm", length = 50)
    private String careerOrganizationNm;

    @Column(name = "dept_role_nm", nullable = true, length = 100)
    private String deptRoleNm;
    @Column(name = "location_city_country_cd", nullable = true, length = 9)
    private String locationCityCountryCd;
    @Column(name = "location_city_nm", nullable = true, length = 50)
    private String locationCityNm;
    @Enumerated(EnumType.STRING)
    @Column(name = "present_yn", nullable = true, length = 1)
    private YnTypeEnum presentYn;
    @Column(name = "career_seq", nullable = true)
    private Long careerSeq;
    @Column(name = "career_start_dt", nullable = true)
    private LocalDateTime careerStartDt;
    @Column(name = "career_end_dt", nullable = true)
    private LocalDateTime careerEndDt;

    @Builder
    ProfileCareer(
            Long profileId,
            String careerOrganizationNm,
            String deptRoleNm,
            String locationCityCountryCd,
            String locationCityNm,
            YnTypeEnum presentYn,
            Long careerSeq,
            LocalDateTime careerStartDt,
            LocalDateTime careerEndDt
            ){
        this.profileId=profileId;
        this.careerOrganizationNm=careerOrganizationNm;
        this.deptRoleNm=deptRoleNm;
        this.locationCityCountryCd=locationCityCountryCd;
        this.locationCityNm=locationCityNm;
        this.presentYn=presentYn;
        this.careerSeq=careerSeq;
        this.careerStartDt=careerStartDt;
        this.careerEndDt=careerEndDt;
    }

}
