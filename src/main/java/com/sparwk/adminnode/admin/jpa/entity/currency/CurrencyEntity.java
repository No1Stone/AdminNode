package com.sparwk.adminnode.admin.jpa.entity.currency;

import com.sparwk.adminnode.admin.jpa.entity.BaseEntity;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name="tb_admin_currency_code")
public class CurrencyEntity extends BaseEntity implements Serializable {

    @Id
    @GeneratedValue(
            generator = "tb_admin_currency_code_currency_seq_seq"
    )
    @Column(name = "currency_seq")
    private Long currencySeq;

    @Column(name = "currency", length = 100)
    private String currency;

    @Column(name = "currency_cd", length = 9)
    private String currencyCd;

    @Column(name = "iso_code", length = 3)
    private String isoCode;

    @Column(name = "symbol", length = 20)
    private String symbol;

    @Column(name = "country", length = 600)
    private String country;

    @Enumerated(EnumType.STRING)
    @Column(name = "use_yn", length = 1)
    private YnTypeEnum useType;

    @Builder
    public CurrencyEntity(
                        Long currencySeq,
                        String currency,
                        String currencyCd,
                        String isoCode,
                        String symbol,
                        String country,
                        YnTypeEnum useType
                ) {
        this.currencySeq = currencySeq;
        this.currency = currency;
        this.currencyCd = currencyCd;
        this.isoCode = isoCode;
        this.symbol = symbol;
        this.country = country;
        this.useType = useType;
    }

    public void updateYn(YnTypeEnum useType) {
        this.useType = useType;
    }

    public void update(
                        String currency,
                        String currencyCd,
                        String isoCode,
                        String symbol,
                        String country,
                        YnTypeEnum useType
                ) {
        this.currency = currency;
        this.currencyCd = currencyCd;
        this.isoCode = isoCode;
        this.symbol = symbol;
        this.country = country;
        this.useType = useType;
    }

}
