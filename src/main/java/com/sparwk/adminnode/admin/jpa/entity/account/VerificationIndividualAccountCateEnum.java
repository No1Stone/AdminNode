package com.sparwk.adminnode.admin.jpa.entity.account;

import lombok.Getter;

@Getter
public enum VerificationIndividualAccountCateEnum {

    All("All"),
    Email("Email"),
    FullName("FullName"),
    Company("Company"),
    Role("Role");

    private String cate;

    VerificationIndividualAccountCateEnum(String cate) {
        this.cate = cate;
    }
}
