package com.sparwk.adminnode.admin.jpa.repository.test;

import com.sparwk.adminnode.admin.jpa.entity.test.TestExcelEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TestExcelRepository extends JpaRepository<TestExcelEntity, Long> {
}
