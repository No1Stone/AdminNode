
package com.sparwk.adminnode.admin.jpa.repository.anr.dsl;

import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.sparwk.adminnode.admin.biz.v1.anr.dto.AnrDto;
import com.sparwk.adminnode.admin.biz.v1.anr.dto.AnrExcelDto;
import com.sparwk.adminnode.admin.jpa.entity.PeriodTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.admin.QAdminEntity;
import com.sparwk.adminnode.admin.jpa.entity.anr.AnrCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.anr.AnrSorterEnum;
import com.sparwk.adminnode.admin.jpa.entity.commonCode.CommonSorterEnum2;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static com.sparwk.adminnode.admin.jpa.entity.admin.QAdminEntity.adminEntity;
import static com.sparwk.adminnode.admin.jpa.entity.anr.QAnrEntity.anrEntity;
import static com.sparwk.adminnode.admin.jpa.entity.commonCode.QCommonDetailCodeEntity.commonDetailCodeEntity;
import static com.sparwk.adminnode.admin.jpa.entity.commonCode.QCommonOrganizationCodeEntity.commonOrganizationCodeEntity;

@Repository
public class AnrCustomRepositoryImpl implements AnrCustomRepository {

    private final JPAQueryFactory jpaQueryFactory;

    public AnrCustomRepositoryImpl(JPAQueryFactory jpaQueryFactory) {
        this.jpaQueryFactory = jpaQueryFactory;
    }

    @Override
    public List<AnrDto> findQryAll(String pcode,
                                    AnrCateEnum cate,
                                   String val,
                                   YnTypeEnum useType,
                                   PeriodTypeEnum periodType,
                                   String sdate,
                                   String edate,
                                   AnrSorterEnum sorter,
                                   PageRequest pageRequest) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(Projections.bean(AnrDto.class,
                                anrEntity.anrSeq,
                                anrEntity.anrServiceName,
                                anrEntity.anrGroupCd,
                        commonDetailCodeEntity.val.as("anrGroupCdName"),
                                anrEntity.anrCd,
                                anrEntity.description,
                                anrEntity.useType,
                        anrEntity.regUsr,
                                adminEntity.fullName.as("regUsrName"),
                        anrEntity.regDt,
                        anrEntity.modUsr,
                                adminEntity2.fullName.as("modUsrName"),
                        anrEntity.modDt
                        )
                )
                .from(anrEntity)
                .leftJoin(commonDetailCodeEntity).on(anrEntity.anrGroupCd.eq(commonDetailCodeEntity.dcode))
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(commonOrganizationCodeEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(commonOrganizationCodeEntity.modUsr))
                .where(valLike(cate, val), useTypeEq(useType), termBetween(periodType, sdate, edate))
                .limit(pageRequest.getPageSize())
                .offset(pageRequest.getPageSize() * (pageRequest.getPageNumber() - 1))
                .orderBy(sorterOrder(sorter))
                .fetch();
    }

    @Override
    public long countQryAll(String pcode,
                                   AnrCateEnum cate,
                                   String val,
                                   YnTypeEnum useType,
                                   PeriodTypeEnum periodType,
                                   String sdate,
                                   String edate) {
        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(
                        anrEntity.anrSeq.count()
                )
                .from(anrEntity)
                .leftJoin(commonDetailCodeEntity).on(anrEntity.anrGroupCd.eq(commonDetailCodeEntity.dcode))
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(commonOrganizationCodeEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(commonOrganizationCodeEntity.modUsr))
                .where(valLike(cate, val), useTypeEq(useType), termBetween(periodType, sdate, edate))
                .fetchOne();
    }

    @Override
    public List<AnrExcelDto> findExcelList() {

        return jpaQueryFactory.select(Projections.bean(AnrExcelDto.class,
                        anrEntity.anrSeq,
                        anrEntity.anrServiceName,
                        anrEntity.anrCd,
                        anrEntity.anrGroupCd,
                        commonDetailCodeEntity.val.as("anrGroupCdName"),
                        anrEntity.useType,
                        anrEntity.description
                        )
                )
                .from(anrEntity)
                .leftJoin(commonDetailCodeEntity).on(anrEntity.anrGroupCd.eq(commonDetailCodeEntity.dcode))
                .orderBy(sorterOrder(AnrSorterEnum.GroupAsc), sorterOrder(AnrSorterEnum.NameAsc))
                .fetch();
    }

    private BooleanExpression valLike(AnrCateEnum cate, String val) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        if (cate != null && val != null && val.length() > 0) {
            switch (cate) {
                case All:
                    return anrEntity.anrServiceName.toLowerCase().contains(val.toLowerCase())
                            .or(commonDetailCodeEntity.val.toLowerCase().contains(val.toLowerCase())
                                    .and(commonDetailCodeEntity.pcode.eq("ARG"))
                            ).or(anrEntity.description.toLowerCase().contains(val.toLowerCase()));
                case ServiceName:
                    return anrEntity.anrServiceName.toLowerCase().contains(val.toLowerCase());
                case Description:
                    return anrEntity.description.toLowerCase().contains(val.toLowerCase());
                case Group:
                    return commonDetailCodeEntity.val.toLowerCase().contains(val.toLowerCase())
                            .and(commonDetailCodeEntity.pcode.eq("ARG"));
                case RegUsrName:
                    return adminEntity.fullName.toLowerCase().contains(val.toLowerCase());
                case ModUsrName:
                    return adminEntity2.fullName.toLowerCase().contains(val.toLowerCase());
            }
        }
        return null;
    }

    private BooleanExpression useTypeEq(YnTypeEnum useType) {
        if (useType != null)
            return anrEntity.useType.eq(useType);
        return null;
    }

    private BooleanExpression termBetween(PeriodTypeEnum periodType, String  sdate, String edate) {

        if (sdate != null && edate != null && periodType != null) {

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
            LocalDateTime searchStartDate = LocalDateTime.parse(sdate + " 00:00:00.000", formatter);
            LocalDateTime searchEndDate = LocalDateTime.parse(edate + " 23:59:59.999", formatter);

            switch (periodType) {
                case Created:
                    return anrEntity.regDt.between(searchStartDate, searchEndDate);
                case LastModified:
                    return anrEntity.modDt.between(searchStartDate, searchEndDate);
                default:
                    return null;
            }
        }
        return null;
    }

    private OrderSpecifier sorterOrder(AnrSorterEnum sorter) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        if (sorter == null)
            return anrEntity.regDt.desc();

        if (sorter == AnrSorterEnum.NameAsc)
            return anrEntity.anrServiceName.asc();

        if (sorter == AnrSorterEnum.NameDesc)
            return anrEntity.anrServiceName.desc();

        if (sorter == AnrSorterEnum.UseYnAsc)
            return anrEntity.useType.asc();

        if (sorter == AnrSorterEnum.UseYnDesc)
            return anrEntity.useType.desc();

        if (sorter == AnrSorterEnum.LastModifiedAsc)
            return anrEntity.modDt.asc();

        if (sorter == AnrSorterEnum.LastModifiedDesc)
            return anrEntity.modDt.desc();

        if (sorter == AnrSorterEnum.CreatedAsc)
            return anrEntity.regDt.asc();

        if (sorter == AnrSorterEnum.CreatedDesc)
            return anrEntity.regDt.desc();

        if (sorter == AnrSorterEnum.GroupAsc)
            return commonDetailCodeEntity.val.asc();

        if (sorter == AnrSorterEnum.GroupDesc)
            return commonDetailCodeEntity.val.desc();

        if (sorter == AnrSorterEnum.DescriptionAsc)
            return commonDetailCodeEntity.description.asc();

        if (sorter == AnrSorterEnum.DescriptionDesc)
            return commonDetailCodeEntity.description.desc();

        if (sorter == AnrSorterEnum.CreaterNameAsc)
            return adminEntity.fullName.asc();

        if (sorter == AnrSorterEnum.CreaterNameDesc)
            return adminEntity.fullName.desc();

        if (sorter == AnrSorterEnum.ModifierNameAsc)
            return adminEntity2.fullName.asc();

        if (sorter == AnrSorterEnum.ModifierNameDesc)
            return adminEntity2.fullName.desc();

        return anrEntity.regDt.desc();
    }

}
