package com.sparwk.adminnode.admin.jpa.entity.currency;

import lombok.Getter;

@Getter
public enum CurrencyCateEnum {


    All("All"),Currency("Currency"), Country("Country"), ISO_4217("ISO_4217"), RegUsrName("RegUsrName"), ModUsrName("ModUsrName");

    private String currencyCateEnum;

    CurrencyCateEnum(String currencyCateEnum) {
        this.currencyCateEnum = currencyCateEnum;
    }
}
