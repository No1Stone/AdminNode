package com.sparwk.adminnode.admin.jpa.repository.sns;

import com.sparwk.adminnode.admin.jpa.entity.country.CountryEntity;
import com.sparwk.adminnode.admin.jpa.entity.sns.SnsEntity;
import com.sparwk.adminnode.admin.jpa.repository.codeSeq.CodeSeqRepository;
import com.sparwk.adminnode.admin.jpa.repository.country.dsl.CountryCustomRepository;
import com.sparwk.adminnode.admin.jpa.repository.sns.dsl.SnsCustomRepository;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SnsRepository extends JpaRepository<SnsEntity, Long>, SnsCustomRepository, CodeSeqRepository {
    SnsEntity findBySnsCd(String snsCd);
}
