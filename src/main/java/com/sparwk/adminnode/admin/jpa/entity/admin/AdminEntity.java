package com.sparwk.adminnode.admin.jpa.entity.admin;

import com.sparwk.adminnode.admin.jpa.entity.BaseEntity;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import lombok.*;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name="tb_admin")
public class AdminEntity extends BaseEntity {

    @Id
    @Column(name = "admin_id")
    @GeneratedValue(generator = "accnt_id_seq")
    private Long adminId;

    @Column(name = "admin_email", length = 30)
    private String adminEmail;

    @Column(name = "full_name", length = 50)
    private String fullName;

    @Column(name = "description", length = 500)
    private String description;

    @Enumerated(EnumType.STRING)
    @Column(name = "use_yn", length = 1)
    private YnTypeEnum useType;

    @Column(name = "dial",  length = 10)
    private String dial;

    @Column(name = "permission_assign_seq")
    private Long permissionAssignSeq;

    @Column(name = "phone_number", length = 30)
    private String phoneNumber;

    @Enumerated(EnumType.STRING)
    @Column(name = "lock_yn", length = 1)
    private YnTypeEnum lockType;

    @Builder
    public AdminEntity(
            Long adminId,
            String adminEmail,
            String fullName,
            String description,
            YnTypeEnum useType,
            String dial,
            Long permissionAssignSeq,
            String phoneNumber,
            YnTypeEnum lockType
                          ) {
        this.adminId = adminId;
        this.adminEmail = adminEmail;
        this.fullName = fullName;
        this.description = description;
        this.useType = useType;
        this.dial = dial;
        this.permissionAssignSeq = permissionAssignSeq;
        this.phoneNumber = phoneNumber;
        this.lockType= lockType;
    }

    public void updateYn(YnTypeEnum useType) {
        this.useType = useType;
    }

    public void updateLockYn(YnTypeEnum lockType) {
        this.lockType = lockType;
    }

    public void update(
            String adminEmail,
            String fullName,
            String description,
            YnTypeEnum useType,
            String dial,
            Long permissionAssignSeq,
            String phoneNumber,
            YnTypeEnum lockType
                        ) {
        this.adminEmail = adminEmail;
        this.fullName = fullName;
        this.description = description;
        this.useType = useType;
        this.dial = dial;
        this.permissionAssignSeq = permissionAssignSeq;
        this.phoneNumber = phoneNumber;
        this.lockType = lockType;
    }

}
