package com.sparwk.adminnode.admin.jpa.entity.account;

import lombok.Getter;

@Getter
public enum MaintenanceVerifiedMemberSorterEnum {

    EmailAsc("EmailAsc"),
    EmailDesc("EmailDesc"),
    FullNameAsc("FullNameAsc"),
    FullNameDesc("FullNameDesc"),
    CountryAsc("CountryAsc"),
    CountryDesc("CountryDesc"),
    IpiAsc("IpiAsc"),
    IpiDesc("IpiDesc"),
    IsniAsc("IsniAsc"),
    IsniDesc("IsniDesc"),
    CreatedAsc("CreatedAsc"),
    CreatedDesc("CreatedDesc");

    private String sorter;

    MaintenanceVerifiedMemberSorterEnum(String sorter) {
        this.sorter = sorter;
    }
}
