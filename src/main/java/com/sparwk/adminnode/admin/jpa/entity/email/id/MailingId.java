package com.sparwk.adminnode.admin.jpa.entity.email.id;

import lombok.Data;

import java.io.Serializable;

@Data
public class MailingId implements Serializable {
    private String toEmail;
    private String fromEmail;
    private String subject;
}