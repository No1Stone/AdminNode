package com.sparwk.adminnode.admin.jpa.entity.commonCode;

import lombok.Getter;

@Getter
public enum CommonCateEnum {

    All("All"), Value("Value"), Description("Description"), RegUsrName("RegUsrName"), ModUsrName("ModUsrName");

    private String commonCateEnum;

    CommonCateEnum(String commonCateEnum) {
        this.commonCateEnum = commonCateEnum;
    }
}
