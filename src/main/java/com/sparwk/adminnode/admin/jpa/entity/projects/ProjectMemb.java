package com.sparwk.adminnode.admin.jpa.entity.projects;

import com.sparwk.adminnode.admin.jpa.entity.BaseEntity;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.projects.id.ProjectMembId;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor
@IdClass(ProjectMembId.class)
@Table(name = "tb_project_memb")
public class ProjectMemb extends BaseEntity {

    @Id
    @Column(name = "proj_id", nullable = false)
    private long projId;
    @Id
    @Column(name = "profile_id", nullable = false)
    private long profileId;
    @Column(name = "patp_sdt", nullable = true)
    private LocalDateTime patpSdt;
    @Column(name = "patp_edt", nullable = true)
    private LocalDateTime patpEdt;
    @Column(name = "active_yn", nullable = true)
    private YnTypeEnum activeYn;
    @Column(name = "confirm_yn", nullable = true)
    private YnTypeEnum confirmYn;
    @Column(name = "apply_stat", nullable = true)
    private String applyStat;
    @Column(name = "invt_stat", nullable = true)
    private String invtStat;
    @Column(name = "invt_dt", nullable = true)
    private LocalDateTime invtDt;
    @Column(name = "ban_yn", nullable = true)
    private YnTypeEnum banYn;
    @Column(name = "ban_dt", nullable = true)
    private LocalDateTime banDt;
    @Column(name = "quit_yn", nullable = true)
    private YnTypeEnum quitYn;
    @Column(name = "quit_dt", nullable = true)
    private LocalDateTime quitDt;
    @Column(name = "reg_usr", nullable = true)
    private Long regUsr;
    @Column(name = "reg_dt", nullable = true)
    private LocalDateTime regDt;
    @Column(name = "mod_usr", nullable = true)
    private Long modUsr;
    @Column(name = "mod_dt", nullable = true)
    private LocalDateTime modDt;

    @Builder
    ProjectMemb(
        long projId,
        long profileId,
        LocalDateTime patpSdt,
        LocalDateTime patpEdt,
        YnTypeEnum activeYn,
        YnTypeEnum confirmYn,
        String applyStat,
        String invtStat,
        LocalDateTime invtDt,
        YnTypeEnum banYn,
        LocalDateTime banDt,
        YnTypeEnum quitYn,
        LocalDateTime quitDt,
        Long regUsr,
        LocalDateTime regDt,
        Long modUsr,
        LocalDateTime modDt
    ) {
        this.projId = projId;
        this.profileId = profileId;
        this.patpSdt = patpSdt;
        this.patpEdt = patpEdt;
        this.activeYn = activeYn;
        this.confirmYn = confirmYn;
        this.applyStat = applyStat;
        this.invtStat = invtStat;
        this.invtDt = invtDt;
        this.banYn = banYn;
        this.banDt = banDt;
        this.quitYn = quitYn;
        this.quitDt = quitDt;
        this.regUsr = regUsr;
        this.regDt = regDt;
        this.modUsr = modUsr;
        this.modDt = modDt;
    }

//    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
//    @JoinColumn(name = "proj_id", referencedColumnName = "proj_id",insertable = false,updatable = false)
//    private Project project;
}
