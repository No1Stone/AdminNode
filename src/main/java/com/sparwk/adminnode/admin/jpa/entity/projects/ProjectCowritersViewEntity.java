package com.sparwk.adminnode.admin.jpa.entity.projects;

import com.sparwk.adminnode.admin.jpa.entity.projects.id.ProjectCowritersId;
import lombok.*;

import javax.persistence.*;

@Entity
@Getter
@Setter
@IdClass(ProjectCowritersId.class)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "view_admin_project_cowriters_list")
public class ProjectCowritersViewEntity {
    @Id
    @Column(name = "proj_id", nullable = true)
    private Long projId;
    @Id
    @Column(name = "profile_id", nullable = true)
    private Long profileId;
    @Column(name = "full_name", nullable = true)
    private String fullName;
    @Column(name = "accnt_email", nullable = true)
    private String accntEmail;
    @Column(name = "headline", nullable = true)
    private String headline;
    @Column(name = "phone_number", nullable = true)
    private String phoneNumber;
    @Column(name = "profile_company_name", nullable = true)
    private String companyName;
    @Column(name = "cnt", nullable = true)
    private int cnt;

    @Builder
    ProjectCowritersViewEntity(
            Long projId,
            Long profileId,
            String fullName,
            String accntEmail,
            String phoneNumber,
            String companyName,
            int cnt
    ) {
        this.projId = projId;
        this.profileId = profileId;
        this.fullName = fullName;
        this.accntEmail = accntEmail;
        this.phoneNumber = phoneNumber;
        this.companyName = companyName;
        this.cnt = cnt;
    }

}
