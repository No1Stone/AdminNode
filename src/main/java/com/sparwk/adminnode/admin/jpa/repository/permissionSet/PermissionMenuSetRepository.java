package com.sparwk.adminnode.admin.jpa.repository.permissionSet;

import com.sparwk.adminnode.admin.jpa.entity.permissionSet.PermissionMenuSetEntity;
import com.sparwk.adminnode.admin.jpa.repository.permissionSet.dsl.PermissionMenuSetCustomRepository;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface PermissionMenuSetRepository extends JpaRepository<PermissionMenuSetEntity, Long>, PermissionMenuSetCustomRepository {
    Optional<PermissionMenuSetEntity> findByAdminMenuIdAndAdminPermissionId(Long menuI, Long adminPemissionId);

    List<PermissionMenuSetEntity> findByAdminPermissionId(Long permissionId);
    List<PermissionMenuSetEntity> findByAdminMenuId(Long permissionId);

    void deleteByAdminPermissionId(Long id);
}
