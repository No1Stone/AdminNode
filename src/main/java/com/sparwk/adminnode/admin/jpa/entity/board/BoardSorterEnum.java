package com.sparwk.adminnode.admin.jpa.entity.board;

import lombok.Getter;

@Getter
public enum BoardSorterEnum {

    NameAsc("NameAsc"), NameDesc("NameDesc"),
    TopicAsc("TopicAsc"), TopicDesc("TopicDesc"),
    TitleAsc("TitleAsc"), TitleDesc("TitleDesc"),
    UseAsc("UseAsc"), UseDesc("UseDesc"),
    LastModifiedAsc("LastModifiedAsc"), LastModifiedDesc("LastModifiedDesc"),
    CreatedAsc("CreatedAsc"), CreatedDesc("CreatedDesc"),
    CreaterNameAsc("CreaterNameAsc"), CreaterNameDesc("CreaterNameDesc"),
    ModifierNameAsc("ModifierNameAsc"), ModifierNameDesc("ModifierNameDesc")
    ;

    private String sorter;

    BoardSorterEnum(String sorter) {
        this.sorter = sorter;
    }

}
