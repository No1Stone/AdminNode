package com.sparwk.adminnode.admin.jpa.repository.admin;

import com.sparwk.adminnode.admin.jpa.entity.admin.AdminEntity;
import com.sparwk.adminnode.admin.jpa.repository.admin.dsl.AdminCustomRepository;
import com.sparwk.adminnode.admin.jpa.repository.codeSeq.CodeSeqRepository;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AdminRepository extends JpaRepository<AdminEntity, Long>,
        AdminCustomRepository, CodeSeqRepository {

}
