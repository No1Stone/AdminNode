package com.sparwk.adminnode.admin.jpa.entity.profile;

import com.sparwk.adminnode.admin.jpa.entity.BaseEntity;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "over_view")
@DynamicUpdate
@TableGenerator(
        name = "tb_profile_seq_generator",
        table = "tb_profile_sequences",
        pkColumnValue = "tb_profile_seq",
        allocationSize = 1,
        initialValue = 1000000
)
public class ProfileGroupEntity extends BaseEntity {
    @Id
    @GeneratedValue(
            strategy = GenerationType.TABLE,
            generator = "tb_profile_seq_generator")
    @Column(name = "profile_id")
    private Long profileId;

    @Column(name = "accnt_id")
    private Long accntId;

    @Column(name = "mattermost_id", nullable = true, length = 10)
    private String mattermostId;

    @Column(name = "profile_group_name", nullable = true, length = 100)
    private String profileGroupName;

    @Column(name = "profile_img_url", nullable = true, length = 200)
    private String profileImgUrl;

    @Column(name = "profile_bgd_img_url", nullable = true, length = 200)
    private String profileBgdImgUrl;

    @Column(name = "headline", nullable = true, length = 100)
    private String headline;

    @Column(name = "over_view", nullable = true, length = 2000)
    private String overview;

    @Column(name = "web_site", nullable = true)
    private String website;

    @Builder
    ProfileGroupEntity(
            Long profileId,
            Long accntId,
            String mattermostId,
            String profileGroupName,
            String profileImgUrl,
            String profileBgdImgUrl,
            String headline,
            //String bio,
            String overview,
            String website

    ) {
        this.profileId = profileId;
        this.accntId = accntId;
        this.profileGroupName = profileGroupName;
        this.mattermostId = mattermostId;
        this.profileImgUrl = profileImgUrl;
        this.profileBgdImgUrl = profileBgdImgUrl;
        this.headline = headline;
        //this.bio = bio;
        this.overview = overview;
        this.website = website;
    }

}
