package com.sparwk.adminnode.admin.jpa.entity.songCode;

import lombok.Getter;

@Getter
public enum SongCateEnum {

    All("All"), Value("Value"), Description("Description"), RegUsrName("RegUsrName"), ModUsrName("ModUsrName");

    private String songCateEnum;

    SongCateEnum(String songCateEnum) {
        this.songCateEnum = songCateEnum;
    }
}
