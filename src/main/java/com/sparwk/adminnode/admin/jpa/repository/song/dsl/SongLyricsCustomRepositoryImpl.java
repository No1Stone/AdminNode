package com.sparwk.adminnode.admin.jpa.repository.song.dsl;

import com.querydsl.core.types.Projections;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.sparwk.adminnode.admin.biz.v1.song.dto.SongLyricsLangDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.sparwk.adminnode.admin.jpa.entity.language.QLanguageEntity.languageEntity;
import static com.sparwk.adminnode.admin.jpa.entity.song.QSongLyricsLangEntity.songLyricsLangEntity;

@Repository
public class SongLyricsCustomRepositoryImpl implements SongLyricsCustomRepository {

    private final JPAQueryFactory jpaQueryFactory;

    private final Logger logger = LoggerFactory.getLogger(SongLyricsCustomRepositoryImpl.class);

    public SongLyricsCustomRepositoryImpl(JPAQueryFactory jpaQueryFactory) {
        this.jpaQueryFactory = jpaQueryFactory;
    }

    @Override
    public List<SongLyricsLangDto> findQryLyricsLang(Long id) {
        return jpaQueryFactory.select(Projections.bean(SongLyricsLangDto.class,
                        songLyricsLangEntity.songId,
                        songLyricsLangEntity.langCd,
                        languageEntity.language
                        )
                )
                .from(songLyricsLangEntity)
                .leftJoin(languageEntity).on(languageEntity.languageCd.eq(songLyricsLangEntity.langCd))
                .fetch();
    }
}
