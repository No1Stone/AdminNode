package com.sparwk.adminnode.admin.jpa.entity.account;

import lombok.Getter;

@Getter
public enum MaintenanceAssociateMemberCateEnum {

    All("All"),
    Email("Email"),
    FullName("FullName"),
    StageName("StageName"),
    Gender("Gender");

    private String cate;

    MaintenanceAssociateMemberCateEnum(String cate) {
        this.cate = cate;
    }
}
