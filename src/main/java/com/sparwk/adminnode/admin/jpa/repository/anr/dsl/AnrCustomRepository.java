package com.sparwk.adminnode.admin.jpa.repository.anr.dsl;

import com.sparwk.adminnode.admin.biz.v1.anr.dto.AnrDto;
import com.sparwk.adminnode.admin.biz.v1.anr.dto.AnrExcelDto;
import com.sparwk.adminnode.admin.jpa.entity.PeriodTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.anr.AnrCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.anr.AnrSorterEnum;
import org.springframework.data.domain.PageRequest;

import java.util.List;

public interface AnrCustomRepository {
    List<AnrDto> findQryAll(String pcode,
                            AnrCateEnum cate,
                            String val,
                            YnTypeEnum useType,
                            PeriodTypeEnum periodType,
                            String sdate,
                            String edate,
                            AnrSorterEnum sorter,
                            PageRequest pageRequest);
   long countQryAll(String pcode,
                            AnrCateEnum cate,
                            String val,
                            YnTypeEnum useType,
                            PeriodTypeEnum periodType,
                            String sdate,
                            String edate);

   List<AnrExcelDto> findExcelList();
}