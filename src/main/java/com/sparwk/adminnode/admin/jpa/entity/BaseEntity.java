package com.sparwk.adminnode.admin.jpa.entity;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import java.time.LocalDateTime;

@Getter
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public abstract class BaseEntity {

//    @Column(name="create_ip", length = 50)
//    private String createIp;
    @CreatedBy
    @Column(name="reg_usr", length = 50, updatable = false)
    private Long regUsr;
    @CreatedDate
    @Column(name="reg_dt", updatable = false)
    private LocalDateTime regDt;

//    @Column(name="last_modified_ip", length = 50)
//    private String lastModifiedIp;
    @LastModifiedBy
    @Column(name="mod_usr", length = 50)
    private Long modUsr;
    @LastModifiedDate
    @Column(name="mod_dt")
    private LocalDateTime modDt;

}
