package com.sparwk.adminnode.admin.jpa.repository.commonCode;

import com.sparwk.adminnode.admin.jpa.entity.commonCode.CommonCodeEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CommonCodeRepository extends JpaRepository<CommonCodeEntity, Long> {

    CommonCodeEntity findByCode(String val);

}
