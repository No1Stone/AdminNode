package com.sparwk.adminnode.admin.jpa.repository.role.dsl;

import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.sparwk.adminnode.admin.biz.v1.role.dto.RoleDetailCodeDto;
import com.sparwk.adminnode.admin.biz.v1.role.dto.RoleDetailCodeExcelDto;
import com.sparwk.adminnode.admin.jpa.entity.FormatTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.PeriodTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.admin.QAdminEntity;
import com.sparwk.adminnode.admin.jpa.entity.commonCode.CommonSorterEnum2;
import com.sparwk.adminnode.admin.jpa.entity.role.RoleCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.role.RoleDetailCodeSorterEnum;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static com.sparwk.adminnode.admin.jpa.entity.admin.QAdminEntity.adminEntity;
import static com.sparwk.adminnode.admin.jpa.entity.commonCode.QCommonOrganizationCodeEntity.commonOrganizationCodeEntity;
import static com.sparwk.adminnode.admin.jpa.entity.role.QRoleCodeEntity.roleCodeEntity;
import static com.sparwk.adminnode.admin.jpa.entity.role.QRoleDetailCodeEntity.roleDetailCodeEntity;
import static com.sparwk.adminnode.admin.jpa.entity.songCode.QSongDetailCodeEntity.songDetailCodeEntity;


@Repository
public class RoleDetailCodeCustomRepositoryImpl implements RoleDetailCodeCustomRepository {

    private final JPAQueryFactory jpaQueryFactory;

    public RoleDetailCodeCustomRepositoryImpl(JPAQueryFactory jpaQueryFactory) {
        this.jpaQueryFactory = jpaQueryFactory;
    }

    @Override
    public List<RoleDetailCodeDto> findQryAll(String pcode,
                                              RoleCateEnum cate,
                                              String val,
                                              FormatTypeEnum formatType,
                                              YnTypeEnum useType,
                                              PeriodTypeEnum periodType,
                                              String sdate,
                                              String edate,
                                              RoleDetailCodeSorterEnum sorter,
                                              PageRequest pageRequest) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(Projections.bean(RoleDetailCodeDto.class,
                                roleCodeEntity.code.as("pcode"),
                                roleCodeEntity.val.as("pcodeVal"),
                                roleDetailCodeEntity.roleDetailCodeSeq,
                                roleDetailCodeEntity.dcode,
                                roleDetailCodeEntity.role,
                                roleDetailCodeEntity.formatType,
                                roleDetailCodeEntity.useType,
                                roleDetailCodeEntity.popularType,
                                roleDetailCodeEntity.matchingRoleYn,
                                roleDetailCodeEntity.creditRoleYn,
                                roleDetailCodeEntity.splitSheetRoleYn,
                                roleDetailCodeEntity.hit,
                                roleDetailCodeEntity.abbeviation,
                                roleDetailCodeEntity.customCode,
                                roleDetailCodeEntity.description,
                                roleDetailCodeEntity.regUsr,
                                adminEntity.fullName.as("regUsrName"),
                                roleDetailCodeEntity.regDt,
                                roleDetailCodeEntity.modUsr,
                                adminEntity2.fullName.as("modUsrName"),
                                roleDetailCodeEntity.modDt
                        )
                )
                .from(roleDetailCodeEntity)
                .join(roleDetailCodeEntity.roleCodeEntity, roleCodeEntity)
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(roleDetailCodeEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(roleDetailCodeEntity.modUsr))
                .where(rolePcodeEq(pcode), roleLike(cate, val), cdFormatEq(formatType), useTypeEq(useType), termBetween(periodType, sdate, edate) )
                .limit(pageRequest.getPageSize())
                .offset(pageRequest.getPageSize() * (pageRequest.getPageNumber() - 1))
                .orderBy(sorterOrder(sorter))
                .fetch();
    }

    @Override
    public long countQryAll(String pcode,
                                              RoleCateEnum cate,
                                              String val,
                                              FormatTypeEnum formatType,
                                              YnTypeEnum useType,
                                              PeriodTypeEnum periodType,
                                              String sdate,
                                              String edate) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(
                                roleCodeEntity.code.count()
                )
                .from(roleDetailCodeEntity)
                .join(roleDetailCodeEntity.roleCodeEntity, roleCodeEntity)
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(roleDetailCodeEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(roleDetailCodeEntity.modUsr))
                .where(rolePcodeEq(pcode), roleLike(cate, val), cdFormatEq(formatType), useTypeEq(useType), termBetween(periodType, sdate, edate) )
                .fetchOne();
    }

    @Override
    public List<RoleDetailCodeDto> findQryUseY(String pcode, RoleCateEnum cate, RoleDetailCodeSorterEnum sorter, String val, FormatTypeEnum cdFormatType) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(Projections.bean(RoleDetailCodeDto.class,
                                roleCodeEntity.code.as("pcode"),
                                roleCodeEntity.val.as("pcodeVal"),
                                roleDetailCodeEntity.roleDetailCodeSeq,
                                roleDetailCodeEntity.dcode,
                                roleDetailCodeEntity.role,
                                roleDetailCodeEntity.formatType,
                                roleDetailCodeEntity.useType,
                                roleDetailCodeEntity.popularType,
                                roleDetailCodeEntity.matchingRoleYn,
                                roleDetailCodeEntity.creditRoleYn,
                                roleDetailCodeEntity.splitSheetRoleYn,
                                roleDetailCodeEntity.hit,
                                roleDetailCodeEntity.abbeviation,
                                roleDetailCodeEntity.customCode,
                                roleDetailCodeEntity.description,
                                roleDetailCodeEntity.regUsr,
                                adminEntity.fullName.as("regUsrName"),
                                roleDetailCodeEntity.regDt,
                                roleDetailCodeEntity.modUsr,
                                adminEntity2.fullName.as("modUsrName"),
                                roleDetailCodeEntity.modDt
                        )
                )
                .from(roleDetailCodeEntity)
                .join(roleDetailCodeEntity.roleCodeEntity, roleCodeEntity)
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(roleDetailCodeEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(roleDetailCodeEntity.modUsr))
                .where(rolePcodeEq(pcode), roleLike(cate, val), cdFormatEq(cdFormatType), useTypeEq(YnTypeEnum.Y))
                .orderBy(sorterOrder(sorter))
                .fetch();
    }

    @Override
    public long countQryUseY(String pcode, RoleCateEnum cate, RoleDetailCodeSorterEnum sorter, String val, FormatTypeEnum cdFormatType) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(
                        roleCodeEntity.code.count()
                )
                .from(roleDetailCodeEntity)
                .join(roleDetailCodeEntity.roleCodeEntity, roleCodeEntity)
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(roleDetailCodeEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(roleDetailCodeEntity.modUsr))
                .where(rolePcodeEq(pcode), roleLike(cate, val), cdFormatEq(cdFormatType), useTypeEq(YnTypeEnum.Y))
                .fetchOne();
    }

    @Override
    public List<RoleDetailCodeDto> findPopularUseY(String pcode, RoleDetailCodeSorterEnum sorter) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(Projections.bean(RoleDetailCodeDto.class,
                                roleCodeEntity.code.as("pcode"),
                                roleCodeEntity.val.as("pcodeVal"),
                                roleDetailCodeEntity.roleDetailCodeSeq,
                                roleDetailCodeEntity.dcode,
                                roleDetailCodeEntity.role,
                                roleDetailCodeEntity.formatType,
                                roleDetailCodeEntity.useType,
                                roleDetailCodeEntity.popularType,
                                roleDetailCodeEntity.matchingRoleYn,
                                roleDetailCodeEntity.creditRoleYn,
                                roleDetailCodeEntity.splitSheetRoleYn,
                                roleDetailCodeEntity.hit,
                                roleDetailCodeEntity.abbeviation,
                                roleDetailCodeEntity.customCode,
                                roleDetailCodeEntity.description,
                                roleDetailCodeEntity.regUsr,
                                adminEntity.fullName.as("regUsrName"),
                                roleDetailCodeEntity.regDt,
                                roleDetailCodeEntity.modUsr,
                                adminEntity2.fullName.as("modUsrName"),
                                roleDetailCodeEntity.modDt
                        )
                )
                .from(roleDetailCodeEntity)
                .join(roleDetailCodeEntity.roleCodeEntity, roleCodeEntity)
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(roleDetailCodeEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(roleDetailCodeEntity.modUsr))
                .where(rolePcodeEq(pcode), popularTypeEq(YnTypeEnum.Y))
                .orderBy(sorterOrder(sorter))
                .fetch();
    }

    @Override
    public long countPopularUseY(String pcode, RoleDetailCodeSorterEnum sorter) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(
                                roleCodeEntity.code.count()
                )
                .from(roleDetailCodeEntity)
                .join(roleDetailCodeEntity.roleCodeEntity, roleCodeEntity)
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(roleDetailCodeEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(roleDetailCodeEntity.modUsr))
                .where(rolePcodeEq(pcode), popularTypeEq(YnTypeEnum.Y))
                .fetchOne();
    }

    @Override
    public List<RoleDetailCodeDto> findMatchingUseY(String pcode, RoleDetailCodeSorterEnum sorter) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(Projections.bean(RoleDetailCodeDto.class,
                                roleCodeEntity.code.as("pcode"),
                                roleCodeEntity.val.as("pcodeVal"),
                                roleDetailCodeEntity.roleDetailCodeSeq,
                                roleDetailCodeEntity.dcode,
                                roleDetailCodeEntity.role,
                                roleDetailCodeEntity.formatType,
                                roleDetailCodeEntity.useType,
                                roleDetailCodeEntity.popularType,
                                roleDetailCodeEntity.matchingRoleYn,
                                roleDetailCodeEntity.creditRoleYn,
                                roleDetailCodeEntity.splitSheetRoleYn,
                                roleDetailCodeEntity.hit,
                                roleDetailCodeEntity.abbeviation,
                                roleDetailCodeEntity.customCode,
                                roleDetailCodeEntity.description,
                                roleDetailCodeEntity.regUsr,
                                adminEntity.fullName.as("regUsrName"),
                                roleDetailCodeEntity.regDt,
                                roleDetailCodeEntity.modUsr,
                                adminEntity2.fullName.as("modUsrName"),
                                roleDetailCodeEntity.modDt
                        )
                )
                .from(roleDetailCodeEntity)
                .join(roleDetailCodeEntity.roleCodeEntity, roleCodeEntity)
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(roleDetailCodeEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(roleDetailCodeEntity.modUsr))
                .where(rolePcodeEq(pcode), matchingEq(YnTypeEnum.Y))
                .orderBy(sorterOrder(sorter))
                .fetch();
    }

    @Override
    public long countMatchingUseY(String pcode, RoleDetailCodeSorterEnum sorter) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(
                                roleCodeEntity.code.count()
                )
                .from(roleDetailCodeEntity)
                .join(roleDetailCodeEntity.roleCodeEntity, roleCodeEntity)
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(roleDetailCodeEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(roleDetailCodeEntity.modUsr))
                .where(rolePcodeEq(pcode), matchingEq(YnTypeEnum.Y))
                .fetchOne();
    }

    @Override
    public List<RoleDetailCodeDto> findCreditUseY(String pcode, RoleDetailCodeSorterEnum sorter) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(Projections.bean(RoleDetailCodeDto.class,
                                roleCodeEntity.code.as("pcode"),
                                roleCodeEntity.val.as("pcodeVal"),
                                roleDetailCodeEntity.roleDetailCodeSeq,
                                roleDetailCodeEntity.dcode,
                                roleDetailCodeEntity.role,
                                roleDetailCodeEntity.formatType,
                                roleDetailCodeEntity.useType,
                                roleDetailCodeEntity.popularType,
                                roleDetailCodeEntity.matchingRoleYn,
                                roleDetailCodeEntity.creditRoleYn,
                                roleDetailCodeEntity.splitSheetRoleYn,
                                roleDetailCodeEntity.hit,
                                roleDetailCodeEntity.abbeviation,
                                roleDetailCodeEntity.customCode,
                                roleDetailCodeEntity.description,
                                roleDetailCodeEntity.regUsr,
                                adminEntity.fullName.as("regUsrName"),
                                roleDetailCodeEntity.regDt,
                                roleDetailCodeEntity.modUsr,
                                adminEntity2.fullName.as("modUsrName"),
                                roleDetailCodeEntity.modDt
                        )
                )
                .from(roleDetailCodeEntity)
                .join(roleDetailCodeEntity.roleCodeEntity, roleCodeEntity)
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(roleDetailCodeEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(roleDetailCodeEntity.modUsr))
                .where(rolePcodeEq(pcode), creditEq(YnTypeEnum.Y))
                .orderBy(sorterOrder(sorter))
                .fetch();
    }

    @Override
    public long countCreditUseY(String pcode, RoleDetailCodeSorterEnum sorter) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(
                                roleCodeEntity.code.count()
                )
                .from(roleDetailCodeEntity)
                .join(roleDetailCodeEntity.roleCodeEntity, roleCodeEntity)
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(roleDetailCodeEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(roleDetailCodeEntity.modUsr))
                .where(rolePcodeEq(pcode), creditEq(YnTypeEnum.Y))
                .fetchOne();
    }

    @Override
    public List<RoleDetailCodeDto> findSplitSheetUseY(String pcode, RoleDetailCodeSorterEnum sorter) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(Projections.bean(RoleDetailCodeDto.class,
                                roleCodeEntity.code.as("pcode"),
                                roleCodeEntity.val.as("pcodeVal"),
                                roleDetailCodeEntity.roleDetailCodeSeq,
                                roleDetailCodeEntity.dcode,
                                roleDetailCodeEntity.role,
                                roleDetailCodeEntity.formatType,
                                roleDetailCodeEntity.useType,
                                roleDetailCodeEntity.popularType,
                                roleDetailCodeEntity.matchingRoleYn,
                                roleDetailCodeEntity.creditRoleYn,
                                roleDetailCodeEntity.splitSheetRoleYn,
                                roleDetailCodeEntity.hit,
                                roleDetailCodeEntity.abbeviation,
                                roleDetailCodeEntity.customCode,
                                roleDetailCodeEntity.description,
                                roleDetailCodeEntity.regUsr,
                                adminEntity.fullName.as("regUsrName"),
                                roleDetailCodeEntity.regDt,
                                roleDetailCodeEntity.modUsr,
                                adminEntity2.fullName.as("modUsrName"),
                                roleDetailCodeEntity.modDt
                        )
                )
                .from(roleDetailCodeEntity)
                .join(roleDetailCodeEntity.roleCodeEntity, roleCodeEntity)
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(roleDetailCodeEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(roleDetailCodeEntity.modUsr))
                .where(rolePcodeEq(pcode), splitSheetEq(YnTypeEnum.Y))
                .orderBy(sorterOrder(sorter))
                .fetch();
    }

    @Override
    public long countSplitSheetUseY(String pcode, RoleDetailCodeSorterEnum sorter) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(
                                roleCodeEntity.code.count()
                )
                .from(roleDetailCodeEntity)
                .join(roleDetailCodeEntity.roleCodeEntity, roleCodeEntity)
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(roleDetailCodeEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(roleDetailCodeEntity.modUsr))
                .where(rolePcodeEq(pcode), splitSheetEq(YnTypeEnum.Y))
                .fetchOne();
    }

    @Override
    public List<RoleDetailCodeExcelDto> findExcelList(String pcode) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(Projections.bean(RoleDetailCodeExcelDto.class,
                                roleCodeEntity.code.as("pcode"),
                                roleCodeEntity.val.as("pcodeVal"),
                                roleDetailCodeEntity.roleDetailCodeSeq,
                                roleDetailCodeEntity.dcode,
                                roleDetailCodeEntity.role,
                                roleDetailCodeEntity.formatType,
                                roleDetailCodeEntity.useType,
                                roleDetailCodeEntity.popularType,
                                roleDetailCodeEntity.matchingRoleYn,
                                roleDetailCodeEntity.creditRoleYn,
                                roleDetailCodeEntity.splitSheetRoleYn,
                                roleDetailCodeEntity.hit,
                                roleDetailCodeEntity.abbeviation,
                                roleDetailCodeEntity.customCode,
                                roleDetailCodeEntity.description
                        )
                )
                .from(roleDetailCodeEntity)
                .join(roleDetailCodeEntity.roleCodeEntity, roleCodeEntity)
                .orderBy(sorterOrder(RoleDetailCodeSorterEnum.FormatAsc), sorterOrder(RoleDetailCodeSorterEnum.RoleAsc))
                .fetch();
    }

    public void deleteQryId(Long id){

        if(id > 0) {
            jpaQueryFactory.delete(roleDetailCodeEntity)
                    .where(idEq(id))
                    .execute();
        }
    }

    public void updateHit(Long id) {
        if(id > 0) {
            jpaQueryFactory.update(roleDetailCodeEntity)
                    .set(roleDetailCodeEntity.hit,roleDetailCodeEntity.hit.add(1))
                    .where(idEq(id))
                    .execute();
        }
    }

     private BooleanExpression rolePcodeEq(String code) {
            if (code != null && code.length() > 0)
                return roleDetailCodeEntity.pcode.eq(code);
            return null;
    }


    private BooleanExpression roleLike(RoleCateEnum cate, String val) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        if (cate !=null && val != null && val.length() > 0) {
            switch (cate) {
                case All:
                    return roleDetailCodeEntity.role.toLowerCase().contains(val.toLowerCase())
                            .or(roleDetailCodeEntity.description.toLowerCase().contains(val.toLowerCase()));
                case Role:
                    return roleDetailCodeEntity.role.toLowerCase().contains(val.toLowerCase());
                case Abbreviation:
                    return roleDetailCodeEntity.abbeviation.toLowerCase().contains(val.toLowerCase());
                case Code:
                    return roleDetailCodeEntity.customCode.toLowerCase().contains(val.toLowerCase());
                case Description:
                    return roleDetailCodeEntity.description.toLowerCase().contains(val.toLowerCase());
                case RegUsrName:
                    return adminEntity.fullName.toLowerCase().contains(val.toLowerCase());
                case ModUsrName:
                    return adminEntity2.fullName.toLowerCase().contains(val.toLowerCase());
            }
        }
        return null;
    }

    private BooleanExpression termBetween(PeriodTypeEnum periodType, String  sdate, String edate) {

        if (sdate != null && edate != null && periodType != null) {

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
            LocalDateTime searchStartDate = LocalDateTime.parse(sdate + " 00:00:00.000", formatter);
            LocalDateTime searchEndDate = LocalDateTime.parse(edate + " 23:59:59.999", formatter);

            switch (periodType) {
                case Created:
                    return songDetailCodeEntity.regDt.between(searchStartDate, searchEndDate);
                case LastModified:
                    return songDetailCodeEntity.modDt.between(searchStartDate, searchEndDate);
                default:
                    return null;
            }
        }
        return null;
    }

    private BooleanExpression dcodeEq(String code) {
        if (code != null && code.length() > 0)
            return roleDetailCodeEntity.dcode.eq(code);
        return null;
    }

    private BooleanExpression idEq(Long id) {
        if (id != null)
            return roleDetailCodeEntity.roleDetailCodeSeq.eq(id);
        return null;
    }

    private BooleanExpression cdFormatEq(FormatTypeEnum formatType) {
        if (formatType != null)
            return roleDetailCodeEntity.formatType.eq(formatType);
        return null;
    }

    private BooleanExpression useTypeEq(YnTypeEnum useType) {
        if (useType != null)
            return roleDetailCodeEntity.useType.eq(useType);
        return null;
    }

    private BooleanExpression popularTypeEq(YnTypeEnum popularType) {
        if (popularType != null)
            return roleDetailCodeEntity.popularType.eq(popularType);
        return null;
    }

    private BooleanExpression matchingEq(YnTypeEnum popularType) {
        if (popularType != null)
            return roleDetailCodeEntity.matchingRoleYn.eq(popularType);
        return null;
    }

    private BooleanExpression creditEq(YnTypeEnum popularType) {
        if (popularType != null)
            return roleDetailCodeEntity.creditRoleYn.eq(popularType);
        return null;
    }

    private BooleanExpression splitSheetEq(YnTypeEnum popularType) {
        if (popularType != null)
            return roleDetailCodeEntity.splitSheetRoleYn.eq(popularType);
        return null;
    }

    private OrderSpecifier sorterOrder(RoleDetailCodeSorterEnum sorter) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        if (sorter == null)
            return roleDetailCodeEntity.regDt.desc();

        if (sorter == RoleDetailCodeSorterEnum.RoleAsc)
            return roleDetailCodeEntity.role.asc();

        if (sorter == RoleDetailCodeSorterEnum.RoleDesc)
            return roleDetailCodeEntity.role.desc();

        if (sorter == RoleDetailCodeSorterEnum.FormatAsc)
            return roleDetailCodeEntity.formatType.asc();

        if (sorter == RoleDetailCodeSorterEnum.FormatDesc)
            return roleDetailCodeEntity.formatType.desc();

        if (sorter == RoleDetailCodeSorterEnum.MRAsc)
            return roleDetailCodeEntity.matchingRoleYn.asc();

        if (sorter == RoleDetailCodeSorterEnum.MRDesc)
            return roleDetailCodeEntity.matchingRoleYn.desc();

        if (sorter == RoleDetailCodeSorterEnum.CRAsc)
            return roleDetailCodeEntity.creditRoleYn.asc();

        if (sorter == RoleDetailCodeSorterEnum.CRDesc)
            return roleDetailCodeEntity.creditRoleYn.desc();

        if (sorter == RoleDetailCodeSorterEnum.SSRAsc)
            return roleDetailCodeEntity.splitSheetRoleYn.asc();

        if (sorter == RoleDetailCodeSorterEnum.SSRDesc)
            return roleDetailCodeEntity.splitSheetRoleYn.desc();

        if (sorter == RoleDetailCodeSorterEnum.PopularAsc)
            return roleDetailCodeEntity.popularType.asc();

        if (sorter == RoleDetailCodeSorterEnum.PopularDesc)
            return roleDetailCodeEntity.popularType.desc();

        if (sorter == RoleDetailCodeSorterEnum.UseYnAsc)
            return roleDetailCodeEntity.useType.asc();

        if (sorter == RoleDetailCodeSorterEnum.UseYnDesc)
            return roleDetailCodeEntity.useType.desc();

        if (sorter == RoleDetailCodeSorterEnum.LastModifiedAsc)
            return roleDetailCodeEntity.modDt.asc();

        if (sorter == RoleDetailCodeSorterEnum.LastModifiedDesc)
            return roleDetailCodeEntity.modDt.desc();

        if (sorter == RoleDetailCodeSorterEnum.CreatedAsc)
            return roleDetailCodeEntity.regDt.asc();

        if (sorter == RoleDetailCodeSorterEnum.CreatedDesc)
            return roleDetailCodeEntity.regDt.desc();

        if (sorter == RoleDetailCodeSorterEnum.DescriptionAsc)
            return roleDetailCodeEntity.description.asc();

        if (sorter == RoleDetailCodeSorterEnum.DescriptionDesc)
            return roleDetailCodeEntity.description.desc();

        if (sorter == RoleDetailCodeSorterEnum.CreaterNameAsc)
            return adminEntity.fullName.asc();

        if (sorter == RoleDetailCodeSorterEnum.CreaterNameDesc)
            return adminEntity.fullName.desc();

        if (sorter == RoleDetailCodeSorterEnum.ModifierNameAsc)
            return adminEntity2.fullName.asc();

        if (sorter == RoleDetailCodeSorterEnum.ModifierNameDesc)
            return adminEntity2.fullName.desc();

        return roleDetailCodeEntity.regDt.desc();
    }

}
