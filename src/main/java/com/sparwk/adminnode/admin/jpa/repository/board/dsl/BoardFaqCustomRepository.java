package com.sparwk.adminnode.admin.jpa.repository.board.dsl;

import com.sparwk.adminnode.admin.biz.v1.board.dto.BoardFaqDto;
import com.sparwk.adminnode.admin.jpa.entity.PeriodTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.board.BoardCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.board.BoardSorterEnum;
import org.springframework.data.domain.PageRequest;

import java.util.List;

public interface BoardFaqCustomRepository {

    List<BoardFaqDto> findQryAll(String type,
                                 String pcode,
                                 BoardCateEnum cate,
                                 String val,
                                 YnTypeEnum useType,
                                 PeriodTypeEnum periodType,
                                 String sdate,
                                 String edate,
                                 BoardSorterEnum sorter,
                                 PageRequest pageRequest
    );


    long countQryAll(String type,
                                 String pcode,
                                 BoardCateEnum cate,
                                 String val,
                                 YnTypeEnum useType,
                                 PeriodTypeEnum periodType,
                                 String sdate,
                                 String edate
    );

    List<BoardFaqDto> findQryUseY(String type, String pcode, BoardCateEnum cate, String val, PageRequest pageRequest);
    long countQryUseY(String type, String pcode, BoardCateEnum cate, String val);

    long deleteQryList(Long[] id);

}
