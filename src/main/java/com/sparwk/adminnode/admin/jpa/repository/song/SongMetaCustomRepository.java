package com.sparwk.adminnode.admin.jpa.repository.song;

import com.sparwk.adminnode.admin.jpa.entity.song.SongMetaCustomEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SongMetaCustomRepository extends JpaRepository<SongMetaCustomEntity, Long> {
    List<SongMetaCustomEntity> findBySongIdOrderByMetadataSeq(Long id);
}
