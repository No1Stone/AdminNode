package com.sparwk.adminnode.admin.jpa.repository.commonCode.dsl;

import com.sparwk.adminnode.admin.biz.v1.commonCode.dto.CommonOrganizationCodeDto;
import com.sparwk.adminnode.admin.biz.v1.commonCode.dto.CommonOrganizationCodeExcelDto;
import com.sparwk.adminnode.admin.jpa.entity.PeriodTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.commonCode.CommonCateEnum3;
import com.sparwk.adminnode.admin.jpa.entity.commonCode.CommonSorterEnum2;
import org.springframework.data.domain.PageRequest;

import java.util.List;

public interface CommonOrganizationCodeCustomRepository {
    List<CommonOrganizationCodeDto> findQryAll(String pcode,
                                         String code,
                                         CommonCateEnum3 cate,
                                         String val,
                                         YnTypeEnum useType,
                                         PeriodTypeEnum periodType,
                                         String sdate,
                                         String edate,
                                         CommonSorterEnum2 sorter,
                                         PageRequest pageRequest
    );

    long countQryAll(String pcode,
                     String code,
                     CommonCateEnum3 cate,
                     String val,
                     YnTypeEnum useType,
                     PeriodTypeEnum periodType,
                     String sdate,
                     String edate
    );

    List<CommonOrganizationCodeDto> findQryUseY(String pcode, String code, CommonCateEnum3 cate, String val);

    long countQryUseY(String pcode, String code, CommonCateEnum3 cate, String val);

    List<CommonOrganizationCodeDto> findPopularUseY(String pcode, String code, CommonCateEnum3 cate, String val);

    long countPopularUseY(String pcode, String code, CommonCateEnum3 cate, String val);

    List<CommonOrganizationCodeExcelDto> findExcelList(String pcode);

    void deleteQryId(Long id);

}
