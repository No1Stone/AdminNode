package com.sparwk.adminnode.admin.jpa.repository.timezone.dsl;

import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.sparwk.adminnode.admin.biz.v1.timezone.dto.TimezoneDto;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.admin.QAdminEntity;
import com.sparwk.adminnode.admin.jpa.entity.commonCode.CommonSorterEnum2;
import com.sparwk.adminnode.admin.jpa.entity.timezone.TimezoneCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.timezone.TimezoneSorterEnum;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.sparwk.adminnode.admin.jpa.entity.admin.QAdminEntity.adminEntity;
import static com.sparwk.adminnode.admin.jpa.entity.commonCode.QCommonOrganizationCodeEntity.commonOrganizationCodeEntity;
import static com.sparwk.adminnode.admin.jpa.entity.timezone.QTimezoneEntity.timezoneEntity;

@Repository
public class TimezoneCustomRepositoryImpl implements TimezoneCustomRepository {

    private final JPAQueryFactory jpaQueryFactory;

    public TimezoneCustomRepositoryImpl(JPAQueryFactory jpaQueryFactory) {
        this.jpaQueryFactory = jpaQueryFactory;
    }

    @Override
    public List<TimezoneDto> findQryAll(TimezoneCateEnum cate, String val, YnTypeEnum useType, TimezoneSorterEnum sorter, PageRequest pageRequest) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(Projections.bean(TimezoneDto.class,
                                timezoneEntity.timezoneSeq,
                                timezoneEntity.timezoneName,
                                timezoneEntity.continent,
                                timezoneEntity.city,
                                timezoneEntity.utcHour,
                                timezoneEntity.diffTime,
                                timezoneEntity.useType,
                                timezoneEntity.regUsr,
                                adminEntity.fullName.as("regUsrName"),
                                timezoneEntity.regDt,
                                timezoneEntity.modUsr,
                                adminEntity2.fullName.as("modUsrName"),
                                timezoneEntity.modDt
                        )
                )
                .from(timezoneEntity)
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(timezoneEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(timezoneEntity.modUsr))
                .where(valLike(cate, val), useTypeEq(useType))
                .limit(pageRequest.getPageSize())
                .offset(pageRequest.getPageSize() * (pageRequest.getPageNumber() - 1))
                .orderBy(sorterOrder(sorter))
                .fetch();
    }

    @Override
    public long countQryAll(TimezoneCateEnum cate, String val, YnTypeEnum useType) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(
                                timezoneEntity.timezoneSeq.count()
                )
                .from(timezoneEntity)
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(timezoneEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(timezoneEntity.modUsr))
                .where(valLike(cate, val), useTypeEq(useType))
                .fetchOne();
    }

    @Override
    public List<TimezoneDto> findQryUseY() {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(Projections.bean(TimezoneDto.class,
                        timezoneEntity.timezoneSeq,
                        timezoneEntity.timezoneName,
                        timezoneEntity.continent,
                        timezoneEntity.city,
                        timezoneEntity.utcHour,
                        timezoneEntity.diffTime,
                        timezoneEntity.useType,
                        timezoneEntity.regUsr,
                        adminEntity.fullName.as("regUsrName"),
                        timezoneEntity.regDt,
                        timezoneEntity.modUsr,
                        adminEntity2.fullName.as("modUsrName"),
                        timezoneEntity.modDt
                        )
                )
                .from(timezoneEntity)
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(timezoneEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(timezoneEntity.modUsr))
                .where(useTypeEq(YnTypeEnum.Y))
                .orderBy(sorterOrder(TimezoneSorterEnum.TimezoneAsc))
                .fetch();
    }

    @Override
    public long countQryUseY() {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(
                        timezoneEntity.timezoneSeq.count()
                )
                .from(timezoneEntity)
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(timezoneEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(timezoneEntity.modUsr))
                .where(useTypeEq(YnTypeEnum.Y))
                .fetchOne();
    }

    private BooleanExpression valLike(TimezoneCateEnum cate, String val) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        if (cate != null && val != null && val.length() > 0) {
            switch (cate) {
                case All:
                    return timezoneEntity.timezoneName.toLowerCase().contains(val.toLowerCase());
                case Timezone:
                    return timezoneEntity.timezoneName.toLowerCase().contains(val.toLowerCase());
                case RegUsrName:
                    return adminEntity.fullName.toLowerCase().contains(val.toLowerCase());
                case ModUsrName:
                    return adminEntity2.fullName.toLowerCase().contains(val.toLowerCase());
            }
        }
        return null;
    }

    private BooleanExpression idEq(Long id) {
        if (id != null)
            return timezoneEntity.timezoneSeq.eq(id);
        return null;
    }

    private BooleanExpression useTypeEq(YnTypeEnum useType) {
        if (useType != null)
            return timezoneEntity.useType.eq(useType);
        return null;
    }

    private OrderSpecifier sorterOrder(TimezoneSorterEnum sorter) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        if (sorter == null)
            return timezoneEntity.regDt.desc();

        if (sorter == TimezoneSorterEnum.TimezoneAsc)
            return timezoneEntity.timezoneName.asc();

        if (sorter == TimezoneSorterEnum.TimezoneDesc)
            return timezoneEntity.timezoneName.desc();

        if (sorter == TimezoneSorterEnum.ContinentAsc)
            return timezoneEntity.continent.asc();

        if (sorter == TimezoneSorterEnum.ContinentDesc)
            return timezoneEntity.continent.desc();

        if (sorter == TimezoneSorterEnum.CityAsc)
            return timezoneEntity.city.asc();

        if (sorter == TimezoneSorterEnum.CityDesc)
            return timezoneEntity.city.desc();

        if (sorter == TimezoneSorterEnum.DiffAsc)
            return timezoneEntity.diffTime.asc();

        if (sorter == TimezoneSorterEnum.DiffDesc)
            return timezoneEntity.diffTime.desc();

        if (sorter == TimezoneSorterEnum.UTCAsc)
            return timezoneEntity.utcHour.asc();

        if (sorter == TimezoneSorterEnum.UTCDesc)
            return timezoneEntity.utcHour.desc();

        if (sorter == TimezoneSorterEnum.CreaterNameAsc)
            return adminEntity.fullName.asc();

        if (sorter == TimezoneSorterEnum.CreaterNameDesc)
            return adminEntity.fullName.desc();

        if (sorter == TimezoneSorterEnum.ModifierNameAsc)
            return adminEntity2.fullName.asc();

        if (sorter == TimezoneSorterEnum.ModifierNameDesc)
            return adminEntity2.fullName.desc();

        return timezoneEntity.regDt.desc();
    }

}
