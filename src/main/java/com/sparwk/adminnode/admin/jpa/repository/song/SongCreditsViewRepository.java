package com.sparwk.adminnode.admin.jpa.repository.song;

import com.sparwk.adminnode.admin.jpa.entity.song.SongCreditsViewEntity;
import com.sparwk.adminnode.admin.jpa.entity.song.SongSplitsheetViewEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SongCreditsViewRepository extends JpaRepository<SongCreditsViewEntity, Long> {
    List<SongCreditsViewEntity> findBySongId(Long id);
}
