package com.sparwk.adminnode.admin.jpa.entity.permissionSetGroups;

import lombok.Getter;

@Getter
public enum PermissionSetGroupsCateEnum {

    All("All"),
    GroupsLabel("GroupsLabel"),
    Description("Description");

    private String permissionSetGroupsCateEnum;

    PermissionSetGroupsCateEnum(String permissionSetGroupsCateEnum) {
        this.permissionSetGroupsCateEnum = permissionSetGroupsCateEnum;
    }
}
