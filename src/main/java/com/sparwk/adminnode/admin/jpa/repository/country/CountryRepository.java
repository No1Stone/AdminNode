package com.sparwk.adminnode.admin.jpa.repository.country;

import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.country.CountryEntity;
import com.sparwk.adminnode.admin.jpa.repository.codeSeq.CodeSeqRepository;
import com.sparwk.adminnode.admin.jpa.repository.country.dsl.CountryCustomRepository;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CountryRepository extends JpaRepository<CountryEntity, Long>, CountryCustomRepository, CodeSeqRepository {
    CountryEntity findByCountry(String country);
    CountryEntity findByCountryCd(String countryCd);

    Long countByUseType(YnTypeEnum ynTypeEnum);
    Long countByCountry(String val);
}
