package com.sparwk.adminnode.admin.jpa.entity.popup;

import lombok.Getter;

@Getter
public enum PopupCateEnum {

    All("All"), Title("Title"), Status("Status"), RegUsrName("RegUsrName"), ModUsrName("ModUsrName");

    private String popupCateEnum;

    PopupCateEnum(String popupCateEnum) {
        this.popupCateEnum = popupCateEnum;
    }
}
