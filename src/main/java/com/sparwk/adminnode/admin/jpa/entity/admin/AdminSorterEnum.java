package com.sparwk.adminnode.admin.jpa.entity.admin;

import lombok.Getter;

@Getter
public enum AdminSorterEnum {

    AdminIdAsc("AdminIdAsc"), AdminIdDesc("AdminIdDesc"),
    PermissionAssignAsc("PermissionAssignAsc"), PermissionAssignDesc("PermissionAssignDesc"),
    FullNameAsc("FullNameAsc"), FullNameDesc("FullNameDesc"),
    DescriptionAsc("DescriptionAsc"), DescriptionDesc("DescriptionDesc"),
    LastModifiedAsc("LastModifiedAsc"), LastModifiedDesc("LastModifiedDesc"),
    CreatedAsc("CreatedAsc"), CreatedDesc("CreatedDesc"),
    UseYnAsc("UseYnAsc"), UseYnDesc("UseYnDesc"),
    CreaterNameAsc("CreaterNameAsc"), CreaterNameDesc("CreaterNameDesc"),
    ModifierNameAsc("ModifierNameAsc"), ModifierNameDesc("ModifierNameDesc")
    ;

    private String sorter;

    AdminSorterEnum(String sorter) {
        this.sorter = sorter;
    }

}
