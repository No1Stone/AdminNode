package com.sparwk.adminnode.admin.jpa.entity.profile;


import com.sparwk.adminnode.admin.jpa.entity.BaseEntity;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_profile_company_studio")
@DynamicUpdate
public class ProfileCompanyStudioEntity extends BaseEntity {

    @Id
    @Column(name = "studio_id")
    private Long studioId;
    @Column(name = "profile_id")
    private Long profileId;
    @Column(name = "owner_id")
    private Long ownerId;
    @Enumerated(EnumType.STRING)
    @Column(name = "skip_yn", nullable = true, length = 1)
    private YnTypeEnum skipYn;
    @Column(name = "studio_name", nullable = true, length = 50)
    private String studioName;
    @Column(name = "business_location_cd", nullable = true, length = 9)
    private String businessLocationCd;
    @Column(name = "post_cd", nullable = true, length = 50)
    private String postCd;
    @Column(name = "region", nullable = true, length = 20)
    private String region;
    @Column(name = "city", nullable = true, length = 20)
    private String city;
    @Column(name = "addr1", nullable = true, length = 200)
    private String addr1;
    @Column(name = "addr2", nullable = true, length = 200)
    private String addr2;


    @Builder
    ProfileCompanyStudioEntity(
            Long studioId,
            Long profileId,
            Long ownerId,
            YnTypeEnum skipYn,
            String studioName,
            String businessLocationCd,
            String postCd,
            String region,
            String city,
            String addr1,
            String addr2
            ) {
        this.studioId = studioId;
        this.profileId = profileId;
        this.ownerId = ownerId;
        this.skipYn = skipYn;
        this.studioName = studioName;
        this.businessLocationCd = businessLocationCd;
        this.postCd = postCd;
        this.region = region;
        this.city = city;
        this.addr1 = addr1;
        this.addr2 = addr2;

    }

}
