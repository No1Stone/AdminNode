package com.sparwk.adminnode.admin.jpa.repository.languagePack.dsl;

import com.sparwk.adminnode.admin.biz.v1.languagePack.dto.LanguagePackDto;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.languagePack.LanguagePackCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.languagePack.LanguagePackSorterEnum;
import org.springframework.data.domain.PageRequest;

import java.util.List;

public interface LanguagePackCustomRepository {

    List<LanguagePackDto> findQryAll(LanguagePackCateEnum languageCateEnum,
                                     String val,
                                     YnTypeEnum useType,
                                     LanguagePackSorterEnum sorter,
                                     PageRequest pageRequest);
    long countQryAll(LanguagePackCateEnum languageCateEnum,
                                 String val,
                                 YnTypeEnum useType);

    void updateQryUseYn(String val, YnTypeEnum useType);

    int findQryMaxVersion(String val);
}
