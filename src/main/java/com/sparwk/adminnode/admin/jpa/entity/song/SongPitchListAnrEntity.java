package com.sparwk.adminnode.admin.jpa.entity.song;

import com.sparwk.adminnode.admin.jpa.entity.BaseEntity;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_pitchlist_anr")
public class SongPitchListAnrEntity extends BaseEntity {
    @Id
    @Column(name = "pitchlist_anr_seq", nullable = true)
    private long pitchlistAnrSeq;
    @Column(name = "pitchlist_id", nullable = true)
    private long pitchlistId;
    @Column(name = "profile_id", nullable = true)
    private long profileId;
    @Column(name = "anr_profile_company_id", nullable = true)
    private long anrProfileCompanyId;
    @Column(name = "pitch_status", length = 9)
    private String pitchStatus;
    @Enumerated(EnumType.STRING)
    @Column(name = "pitch_yn", nullable = true, length = 1)
    private YnTypeEnum pitchYn;

    @Builder
    SongPitchListAnrEntity(
            long pitchlistAnrSeq,
            long pitchlistId,
            long profileId,
            long anrProfileCompanyId,
            String pitchStatus,
            YnTypeEnum pitchYn
    ) {
        this.pitchlistAnrSeq = pitchlistAnrSeq;
        this.pitchlistId = pitchlistId;
        this.profileId = profileId;
        this.profileId = profileId;
        this.anrProfileCompanyId = anrProfileCompanyId;
        this.pitchStatus = pitchStatus;
        this.pitchYn = pitchYn;
    }
}
