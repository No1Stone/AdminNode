package com.sparwk.adminnode.admin.jpa.repository.account.dsl;

import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.CaseBuilder;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.sparwk.adminnode.admin.biz.v1.account.dto.AccountMemberMailDto;
import com.sparwk.adminnode.admin.biz.v1.account.dto.AccountPassportDto;
import com.sparwk.adminnode.admin.biz.v1.account.dto.VerifiedMemberListDto;
import com.sparwk.adminnode.admin.biz.v1.profile.dto.ProfileCurrentPositionDto;
import com.sparwk.adminnode.admin.biz.v1.profile.dto.ProfileMetaDataDto;
import com.sparwk.adminnode.admin.biz.v1.profile.dto.ProfileSnsDto;
import com.sparwk.adminnode.admin.jpa.entity.PeriodTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.account.MaintenanceVerifiedMemberCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.account.MaintenanceVerifiedMemberSorterEnum;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import static com.sparwk.adminnode.admin.jpa.entity.account.QAccountEntity.accountEntity;
import static com.sparwk.adminnode.admin.jpa.entity.account.QAccountPassportEntity.accountPassportEntity;
import static com.sparwk.adminnode.admin.jpa.entity.commonCode.QCommonDetailCodeEntity.commonDetailCodeEntity;
import static com.sparwk.adminnode.admin.jpa.entity.continent.QContinentEntity.continentEntity;
import static com.sparwk.adminnode.admin.jpa.entity.country.QCountryEntity.countryEntity;
import static com.sparwk.adminnode.admin.jpa.entity.language.QLanguageEntity.languageEntity;
import static com.sparwk.adminnode.admin.jpa.entity.profile.QProfileCompanyEntity.profileCompanyEntity;
import static com.sparwk.adminnode.admin.jpa.entity.profile.QProfileEntity.profileEntity;
import static com.sparwk.adminnode.admin.jpa.entity.profile.QProfileGenderEntity.profileGenderEntity;
import static com.sparwk.adminnode.admin.jpa.entity.profile.QProfileInterestMeta.profileInterestMeta;
import static com.sparwk.adminnode.admin.jpa.entity.profile.QProfileLanguageEntity.profileLanguageEntity;
import static com.sparwk.adminnode.admin.jpa.entity.profile.QProfileOnthewebEntity.profileOnthewebEntity;
import static com.sparwk.adminnode.admin.jpa.entity.profile.QProfilePositionEntity.profilePositionEntity;
import static com.sparwk.adminnode.admin.jpa.entity.role.QRoleDetailCodeEntity.roleDetailCodeEntity;
import static com.sparwk.adminnode.admin.jpa.entity.songCode.QSongDetailCodeEntity.songDetailCodeEntity;




@Repository
public class AccountUserCustomRepositoryImpl implements AccountUserCustomRepository {

    private final JPAQueryFactory jpaQueryFactory;

    public AccountUserCustomRepositoryImpl(JPAQueryFactory jpaQueryFactory) {
        this.jpaQueryFactory = jpaQueryFactory;
    }

    //인증받은 User List
    @Override
    public List<VerifiedMemberListDto> findQryVerifiedMember(MaintenanceVerifiedMemberCateEnum cate,
                                                             String val,
                                                             PeriodTypeEnum periodType,
                                                             String sdate,
                                                             String edate,
                                                             MaintenanceVerifiedMemberSorterEnum sorter,
                                                             PageRequest pageRequest) {
        return jpaQueryFactory.select(Projections.bean(VerifiedMemberListDto.class,
                                accountEntity.accntId.as("accntId"),
                                profileEntity.profileId.as("profileId"),
                                accountEntity.accntTypeCd.as("accntTypeCd"),
                                accountEntity.accntEmail.as("accntEmail"),
                                accountPassportEntity.passportFirstName,
                                accountPassportEntity.passportMiddleName,
                                accountPassportEntity.passportLastName,
                                profileEntity.fullName.as("profileFullName"),
                                profileEntity.stageNameYn,
                                accountEntity.countryCd,
                                countryEntity.country.as("countryCdName"),
                                profileEntity.regDt.as("regDt"),
                                profileEntity.ipiInfo,
                                profileEntity.caeInfo,
                                profileEntity.isniInfo,
                                accountEntity.verifyYn
                        )
                )
                .from(profileEntity)
                .leftJoin(accountEntity).on(profileEntity.accntId.eq(accountEntity.accntId))
                .leftJoin(accountPassportEntity).on(profileEntity.accntId.eq(accountPassportEntity.accntId))
                .leftJoin(countryEntity).on(accountEntity.countryCd.eq(countryEntity.countryCd))
                .where(accountEntity.verifyYn.eq(YnTypeEnum.Y), accountEntity.accntTypeCd.eq("user"), valLike(cate, val), termBetween(periodType, sdate, edate))
                .limit(pageRequest.getPageSize())
                .offset(pageRequest.getPageSize() * (pageRequest.getPageNumber() - 1))
                .orderBy(sorterOrder(sorter).stream().toArray(OrderSpecifier[]::new))
                .fetch();
    }

    //인증받은 User List
    @Override
    public long countQryVerifiedMember(MaintenanceVerifiedMemberCateEnum cate,
                                                             String val,
                                                             PeriodTypeEnum periodType,
                                                             String sdate,
                                                             String edate) {
        return jpaQueryFactory.select(
                        accountEntity.accntId.count()
                )
                .from(profileEntity)
                .leftJoin(accountEntity).on(profileEntity.accntId.eq(accountEntity.accntId))
                .leftJoin(accountPassportEntity).on(profileEntity.accntId.eq(accountPassportEntity.accntId))
                .where(accountEntity.verifyYn.eq(YnTypeEnum.Y), accountEntity.accntTypeCd.eq("user"), valLike(cate, val), termBetween(periodType, sdate, edate))
                .fetchOne();
    }

    @Override
    public List<ProfileSnsDto> findQrySns(Long profileId) {
        return jpaQueryFactory.select(Projections.bean(ProfileSnsDto.class,
                        profileOnthewebEntity.profileId.as("profileId"),
                        profileOnthewebEntity.snsTypeCd.as("snsTypeCd"),
                        commonDetailCodeEntity.val.as("snsTypeCdName"),
                        profileOnthewebEntity.snsUrl
                        )
                )
                .from(profileOnthewebEntity)
                .leftJoin(commonDetailCodeEntity).on(profileOnthewebEntity.snsTypeCd.eq(commonDetailCodeEntity.dcode))
                .where(profileOnthewebEntity.profileId.eq(profileId))
                .orderBy(commonDetailCodeEntity.dcode.asc())
                .fetch();
    }

    @Override
    public List<ProfileMetaDataDto> findQryGender(Long profileId) {
        return jpaQueryFactory.select(Projections.bean(ProfileMetaDataDto.class,
                        profileGenderEntity.profileId.as("profileId"),
                        commonDetailCodeEntity.pcode.as("attrTypeCd"),
                        profileGenderEntity.genderCd.as("attrDtlCd"),
                            commonDetailCodeEntity.val.as("attrDtlCdVal")
                        )
                )
                .from(profileGenderEntity)
                .leftJoin(commonDetailCodeEntity).on(profileGenderEntity.genderCd.eq(commonDetailCodeEntity.dcode))
                .where(profileGenderEntity.profileId.eq(profileId))
                .orderBy(commonDetailCodeEntity.val.asc())
                .fetch();
    }

    @Override
    public List<ProfileMetaDataDto> findQryLanuage(Long profileId) {
        return jpaQueryFactory.select(Projections.bean(ProfileMetaDataDto.class,
                        profileLanguageEntity.profileId.as("profileId"),
                        languageEntity.languageCd.as("attrTypeCd"),
                        profileLanguageEntity.languageCd.as("attrDtlCd"),
                        languageEntity.language.as("attrDtlCdVal")
                        )
                )
                .from(profileLanguageEntity)
                .leftJoin(languageEntity).on(profileLanguageEntity.languageCd.eq(languageEntity.languageCd))
                .where(profileLanguageEntity.profileId.eq(profileId))
                .orderBy(languageEntity.languageCd.asc())
                .fetch();
    }

    @Override
    public List<ProfileMetaDataDto> findQryRoles(Long profileId) {
        return jpaQueryFactory.select(Projections.bean(ProfileMetaDataDto.class,
                        profileInterestMeta.profileId.as("profileId"),
                        roleDetailCodeEntity.dcode.as("attrTypeCd"),
                        profileInterestMeta.detailTypeCd.as("attrDtlCd"),
                        roleDetailCodeEntity.role.as("attrDtlCdVal")
                        )
                )
                .from(profileInterestMeta)
                .leftJoin(roleDetailCodeEntity).on(profileInterestMeta.detailTypeCd.eq(roleDetailCodeEntity.dcode))
                .where(profileInterestMeta.profileId.eq(profileId), profileInterestMeta.kindTypeCd.eq("DEX"))
                .orderBy(roleDetailCodeEntity.role.asc())
                .fetch();
    }

    @Override
    public List<ProfileCurrentPositionDto> findQryPosition(Long profileId) {
        return jpaQueryFactory.select(Projections.bean(ProfileCurrentPositionDto.class,
                        profilePositionEntity.profilePositionSeq,
                        profilePositionEntity.profileId,
                        profilePositionEntity.companyProfileId,
                        profileCompanyEntity.profileCompanyName.as("profileCompanyName"),
                        profilePositionEntity.deptRoleInfo,
                        profilePositionEntity.artistYn,
                        profilePositionEntity.anrYn,
                        profilePositionEntity.primaryYn,
                        profilePositionEntity.companyVerifyYn,
                        profilePositionEntity.creatorYn
                        )
                )
                .from(profilePositionEntity)
                .leftJoin(profileCompanyEntity).on(profilePositionEntity.companyProfileId.eq(profileCompanyEntity.profileId))
                .where(profilePositionEntity.profileId.eq(profileId))
                .orderBy(profileCompanyEntity.profileCompanyName.asc())
                .fetch();
    }

    @Override
    public List<ProfileMetaDataDto> findQryTerritorisOrCountry(Long profileId) {
        return jpaQueryFactory.select(Projections.bean(ProfileMetaDataDto.class,
                        profileInterestMeta.profileId.as("profileId"),

                        profileInterestMeta.detailTypeCd.as("attrDtlCd"),
                        new CaseBuilder()
                                .when(profileInterestMeta.detailTypeCd.contains("CNT"))
                                .then("CNT")
                                .otherwise("CTT")
                                .as("attrTypeCd"),
                        new CaseBuilder()
                                .when(profileInterestMeta.detailTypeCd.contains("CNT"))
                                    .then(countryEntity.country)
                                    .otherwise(continentEntity.continent)
                                .as("attrDtlCdVal")
                        )
                )
                .from(profileInterestMeta)
                .leftJoin(continentEntity).on(profileInterestMeta.detailTypeCd.eq(continentEntity.code))
                .leftJoin(countryEntity).on(profileInterestMeta.detailTypeCd.eq(countryEntity.countryCd))
                .where(profileInterestMeta.profileId.eq(profileId))
                .orderBy(profileInterestMeta.detailTypeCd.asc())
                .fetch();
    }

    @Override
    public List<ProfileMetaDataDto> findQryCommon(String pcode, Long profileId) {
        return jpaQueryFactory.select(Projections.bean(ProfileMetaDataDto.class,
                                profileInterestMeta.profileId.as("profileId"),
                                commonDetailCodeEntity.pcode.as("attrTypeCd"),
                                profileInterestMeta.detailTypeCd.as("attrDtlCd"),
                                commonDetailCodeEntity.val.as("attrDtlCdVal")
                        )
                )
                .from(profileInterestMeta)
                .leftJoin(commonDetailCodeEntity).on(profileInterestMeta.detailTypeCd.eq(commonDetailCodeEntity.dcode))
                .where(profileInterestMeta.profileId.eq(profileId), profileInterestMeta.kindTypeCd.eq(pcode))
                .orderBy(commonDetailCodeEntity.val.asc())
                .fetch();
    }

    @Override
    public List<ProfileMetaDataDto> findQrySongs(String pcode, Long profileId) {
        return jpaQueryFactory.select(Projections.bean(ProfileMetaDataDto.class,
                                profileInterestMeta.profileId.as("profileId"),
                                songDetailCodeEntity.pcode.as("attrTypeCd"),
                                profileInterestMeta.detailTypeCd.as("attrDtlCd"),
                                songDetailCodeEntity.val.as("attrDtlCdVal")
                        )
                )
                .from(profileInterestMeta)
                .leftJoin(songDetailCodeEntity).on(profileInterestMeta.detailTypeCd.eq(songDetailCodeEntity.dcode))
                .where(profileInterestMeta.profileId.eq(profileId), profileInterestMeta.kindTypeCd.eq(pcode))
                .orderBy(songDetailCodeEntity.val.asc())
                .fetch();
    }

    @Override
    public Optional<AccountPassportDto> finQryPassport(Long accntId) {
        return Optional.ofNullable(jpaQueryFactory.select(Projections.bean(AccountPassportDto.class,
                                accountPassportEntity.accntId,
                                accountPassportEntity.passportFirstName,
                                accountPassportEntity.passportMiddleName,
                                accountPassportEntity.passportLastName,
                                accountPassportEntity.passportVerifyYn,
                                accountPassportEntity.passportImgFileUrl,
                                accountPassportEntity.countryCd.as("passportCountryCd"),
                                countryEntity.country.as("passportCountryCdName")
                        )
                )
                .from(accountPassportEntity)
                .leftJoin(countryEntity).on(accountPassportEntity.countryCd.eq(countryEntity.countryCd))
                .where(accountPassportEntity.accntId.eq(accntId))
                .fetchOne());
    }

    @Override
    public Optional<ProfileMetaDataDto> findQryCurrentCountry(Long profileId) {
        return Optional.ofNullable(jpaQueryFactory.select(Projections.bean(ProfileMetaDataDto.class,
                        profileEntity.profileId.as("profileId"),
                        countryEntity.countryCd.as("attrTypeCd"),
                        profileEntity.currentCityCountryCd.as("attrDtlCd"),
                        countryEntity.country.as("attrDtlCdVal")
                        )
                )
                .from(profileEntity)
                .leftJoin(countryEntity).on(profileEntity.currentCityCountryCd.eq(countryEntity.countryCd))
                .where(profileEntity.profileId.eq(profileId))
                .fetchOne());
    }

    @Override
    public Optional<ProfileMetaDataDto> findQryHomeTownCountry(Long profileId) {
        return Optional.ofNullable(jpaQueryFactory.select(Projections.bean(ProfileMetaDataDto.class,
                                profileEntity.profileId.as("profileId"),
                                countryEntity.countryCd.as("attrTypeCd"),
                                profileEntity.homeTownCountryCd.as("attrDtlCd"),
                                countryEntity.country.as("attrDtlCdVal")
                        )
                )
                .from(profileEntity)
                .leftJoin(countryEntity).on(profileEntity.homeTownCountryCd.eq(countryEntity.countryCd))
                .where(profileEntity.profileId.eq(profileId))
                .fetchOne());
    }

    @Override
    public Optional<ProfileMetaDataDto> findQryPhoneCountryCdName(Long accountId) {
        return Optional.ofNullable(jpaQueryFactory.select(Projections.bean(ProfileMetaDataDto.class,
                                accountEntity.accntId.as("profileId"),
                                countryEntity.countryCd.as("attrTypeCd"),
                                accountEntity.countryCd.as("attrDtlCd"),
                                countryEntity.country.as("attrDtlCdVal")
                        )
                )
                .from(accountEntity)
                .leftJoin(countryEntity).on(accountEntity.countryCd.eq(countryEntity.countryCd))
                .where(accountEntity.accntId.eq(accountId))
                .fetchOne());
    }

    @Override
    public Optional<AccountMemberMailDto> findQryMemberMail(Long profileId) {
        return Optional.ofNullable(jpaQueryFactory.select(Projections.bean(AccountMemberMailDto.class,
                        profileEntity.profileId,
                        profileEntity.accntId,
                        profileEntity.fullName.as("profileFullName"),
                        countryEntity.country.as("currentCityCountryCdName"),
                        profileEntity.currentCityNm,
                        profileEntity.profileImgUrl
                        )
                )
                .from(profileEntity)
                .leftJoin(countryEntity).on(profileEntity.currentCityCountryCd.eq(countryEntity.countryCd))
                .where(profileEntity.profileId.eq(profileId))
                .fetchOne());
    }

    private BooleanExpression valLike(MaintenanceVerifiedMemberCateEnum cate, String val) {
        if (cate != null && val != null && val.length() > 0) {
            switch (cate) {
                case All:
                    return accountEntity.accntEmail.toLowerCase().contains(val.toLowerCase())
                            .or(profileEntity.fullName.toLowerCase().contains(val.toLowerCase()));
                case Email:
                    return accountEntity.accntEmail.toLowerCase().contains(val.toLowerCase());
                case FullName:
                    return profileEntity.fullName.toLowerCase().contains(val.toLowerCase())
                            .and(profileEntity.stageNameYn.eq(YnTypeEnum.N).or(profileEntity.stageNameYn.isNull()));
                case StageName:
                    return profileEntity.fullName.toLowerCase().contains(val.toLowerCase())
                            .and(profileEntity.stageNameYn.eq(YnTypeEnum.Y));
            }
        }
        return null;
    }

    private BooleanExpression termBetween(PeriodTypeEnum periodType, String  sdate, String edate) {

        if (sdate != null && edate != null && periodType != null) {

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
            LocalDateTime searchStartDate = LocalDateTime.parse(sdate + " 00:00:00.000", formatter);
            LocalDateTime searchEndDate = LocalDateTime.parse(edate + " 23:59:59.999", formatter);

            switch (periodType) {
                case Created:
                    return accountEntity.regDt.between(searchStartDate, searchEndDate);
                case LastModified:
                    return accountEntity.modDt.between(searchStartDate, searchEndDate);
                default:
                    return null;
            }
        }
        return null;
    }

    private List<OrderSpecifier> sorterOrder(MaintenanceVerifiedMemberSorterEnum sorter) {

        List<OrderSpecifier> orders = new ArrayList<>();

        if (sorter == null) {
            orders.add(accountEntity.regDt.desc());
            return orders;
        }

        if (sorter == MaintenanceVerifiedMemberSorterEnum.FullNameAsc) {
            orders.add(profileEntity.fullName.asc());
            orders.add(profileEntity.stageNameYn.asc());
            return orders;
        }

        if (sorter == MaintenanceVerifiedMemberSorterEnum.FullNameDesc) {
            orders.add(profileEntity.fullName.asc());
            orders.add(profileEntity.stageNameYn.asc());
            return orders;
        }

        if (sorter == MaintenanceVerifiedMemberSorterEnum.CreatedAsc) {
            orders.add(accountEntity.regDt.asc());
            return orders;
        }

        if (sorter == MaintenanceVerifiedMemberSorterEnum.CreatedDesc) {
            orders.add(accountEntity.regDt.desc());
            return orders;
        }

        if (sorter == MaintenanceVerifiedMemberSorterEnum.EmailAsc) {
            orders.add(accountEntity.accntEmail.asc());
            return orders;
        }

        if (sorter == MaintenanceVerifiedMemberSorterEnum.EmailDesc) {
            orders.add(accountEntity.accntEmail.desc());
            return orders;
        }

        if (sorter == MaintenanceVerifiedMemberSorterEnum.CountryAsc) {
            orders.add(countryEntity.country.asc());
            return orders;
        }

        if (sorter == MaintenanceVerifiedMemberSorterEnum.CountryDesc) {
            orders.add(countryEntity.country.desc());
            return orders;
        }

        if (sorter == MaintenanceVerifiedMemberSorterEnum.IpiAsc) {
            orders.add(profileEntity.ipiInfo.asc());
            return orders;
        }

        if (sorter == MaintenanceVerifiedMemberSorterEnum.IpiDesc) {
            orders.add(profileEntity.ipiInfo.desc());
            return orders;
        }

        if (sorter == MaintenanceVerifiedMemberSorterEnum.IsniAsc) {
            orders.add(profileEntity.isniInfo.asc());
            return orders;
        }

        if (sorter == MaintenanceVerifiedMemberSorterEnum.IsniDesc) {
            orders.add(profileEntity.isniInfo.desc());
            return orders;
        }

        orders.add(accountEntity.regDt.desc());
        return orders;
    }
   
}
