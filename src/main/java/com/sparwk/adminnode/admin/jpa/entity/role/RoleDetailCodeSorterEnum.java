package com.sparwk.adminnode.admin.jpa.entity.role;

import lombok.Getter;

@Getter
public enum RoleDetailCodeSorterEnum {

    RoleAsc("RoleAsc"), RoleDesc("RoleDesc"),
    FormatAsc("FormatAsc"), FormatDesc("FormatDesc"),
    MRAsc("MRAsc"), MRDesc("MRDesc"),
    CRAsc("CRAsc"), CRDesc("CRDesc"),
    SSRAsc("SSRAsc"), SSRDesc("SSRDesc"),
    DescriptionAsc("DescriptionAsc"), DescriptionDesc("DescriptionDesc"),
    PopularAsc("PopularRAsc"), PopularDesc("PopularDesc"),
    UseYnAsc("UseYnAsc"), UseYnDesc("UseYnDesc"),
    LastModifiedAsc("LastModifiedAsc"), LastModifiedDesc("LastModifiedDesc"),
    CreatedAsc("CreatedAsc"), CreatedDesc("CreatedDesc"),
    CreaterNameAsc("CreaterNameAsc"), CreaterNameDesc("CreaterNameDesc"),
    ModifierNameAsc("ModifierNameAsc"), ModifierNameDesc("ModifierNameDesc")
    ;

    private String sorter;

    RoleDetailCodeSorterEnum(String sorter) {
        this.sorter = sorter;
    }

}
