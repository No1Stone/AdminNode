package com.sparwk.adminnode.admin.jpa.repository.languagePack;

import com.sparwk.adminnode.admin.biz.v1.languagePack.dto.LanguagePackDto;
import com.sparwk.adminnode.admin.jpa.entity.languagePack.LanguagePackEntity;
import com.sparwk.adminnode.admin.jpa.repository.languagePack.dsl.LanguagePackCustomRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface LanguagePackRepository extends JpaRepository<LanguagePackEntity, Long>, LanguagePackCustomRepository {
    Long countByPackId(String val);

    // 일반 SQL쿼리 - 사용안함
    @Query(value = "SELECT a.*, c.language as languageCdName\n" +
            "FROM tb_language_pack a\n" +
            "LEFT JOIN tb_language_pack b\n" +
            "ON a.language_cd = b.language_cd AND a.pack_version < b.pack_version\n" +
            "LEFT JOIN tb_admin_language_code c\n" +
            "ON a.language_cd = c.language_cd\n" +
            "WHERE b.language_cd IS NULL AND (c.language like %:val%)", nativeQuery = true)
    public List<LanguagePackDto> findQryGroupSQLAllCate(@Param("val") String val);
}
