package com.sparwk.adminnode.admin.jpa.repository.projects.dsl;

import com.sparwk.adminnode.admin.biz.v1.projects.dto.*;
import com.sparwk.adminnode.admin.jpa.entity.PeriodTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.projects.ProjcetCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.projects.ProjectSorterEnum;
import org.springframework.data.domain.PageRequest;

import java.util.List;

public interface ProjectViewCustomRepository {
    List<ProjectViewListDto> findQryAll(ProjcetCateEnum cate,
                                        String val,
                                        YnTypeEnum completeVal,
                                        PeriodTypeEnum periodType,
                                        String sdate,
                                        String edate,
                                        ProjectSorterEnum sorter,
                                        PageRequest pageRequest);

    long countQryAll(ProjcetCateEnum cate,
                                        String val,
                                        YnTypeEnum completeVal,
                                        PeriodTypeEnum periodType,
                                        String sdate,
                                        String edate);
}
