package com.sparwk.adminnode.admin.jpa.entity.role;

import com.sparwk.adminnode.admin.jpa.entity.BaseEntity;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name="tb_admin_role_code")
public class RoleCodeEntity extends BaseEntity implements Serializable {

    @Id
    @GeneratedValue(
            generator = "tb_admin_role_code_role_code_seq_seq"
    )
    @Column(name = "role_code_seq")
    private Long roleCodeSeq;

    @Column(name = "code", length = 3, unique = true)
    private String code;

    @Column(name = "val", length = 30)
    private String val;

    @Enumerated(EnumType.STRING)
    @Column(name = "use_yn", length = 1)
    private YnTypeEnum useType;

    @OneToMany(mappedBy = "roleCodeEntity", cascade = CascadeType.ALL)
    List<RoleDetailCodeEntity> roleDetailCodeEntity = new ArrayList<>();

    @Builder
    public RoleCodeEntity(
                            Long roleCodeSeq,
                            String code,
                            String val,
                            YnTypeEnum useType
                        ) {
        this.roleCodeSeq = roleCodeSeq;
        this.code = code;
        this.val = val;
        this.useType = useType;
    }


    public void updateYn(YnTypeEnum useType) {
        this.useType = useType;
    }

    public void update(
                            String code,
                            String val,
                            YnTypeEnum useType
                        ) {
        this.code = code;
        this.val = val;
        this.useType = useType;
    }

}
