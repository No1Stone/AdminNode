package com.sparwk.adminnode.admin.jpa.entity.profile.id;

import lombok.Data;

import java.io.Serializable;

@Data
public class ProfileOnthewebId implements Serializable {

    private String snsTypeCd;
    private Long profileId;


}
