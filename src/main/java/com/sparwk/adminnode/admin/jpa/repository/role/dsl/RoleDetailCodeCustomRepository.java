package com.sparwk.adminnode.admin.jpa.repository.role.dsl;

import com.sparwk.adminnode.admin.biz.v1.role.dto.RoleDetailCodeDto;
import com.sparwk.adminnode.admin.biz.v1.role.dto.RoleDetailCodeExcelDto;
import com.sparwk.adminnode.admin.jpa.entity.FormatTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.PeriodTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.role.RoleCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.role.RoleDetailCodeSorterEnum;
import org.springframework.data.domain.PageRequest;

import java.util.List;

public interface RoleDetailCodeCustomRepository {
    List<RoleDetailCodeDto> findQryAll(String pcode,
                                       RoleCateEnum cate,
                                       String val,
                                       FormatTypeEnum cdFormatType,
                                       YnTypeEnum useType,
                                       PeriodTypeEnum periodType,
                                       String sdate,
                                       String edate,
                                       RoleDetailCodeSorterEnum sorter,
                                       PageRequest pageRequest);
    long countQryAll(String pcode,
                                       RoleCateEnum cate,
                                       String val,
                                       FormatTypeEnum cdFormatType,
                                       YnTypeEnum useType,
                                       PeriodTypeEnum periodType,
                                       String sdate,
                                       String edate);
    List<RoleDetailCodeDto> findQryUseY(String pcode, RoleCateEnum cate, RoleDetailCodeSorterEnum sorter, String val, FormatTypeEnum cdFormatType);
    long countQryUseY(String pcode, RoleCateEnum cate, RoleDetailCodeSorterEnum sorter, String val, FormatTypeEnum cdFormatType);
    List<RoleDetailCodeDto> findPopularUseY(String pcode, RoleDetailCodeSorterEnum sorter);
    long countPopularUseY(String pcode, RoleDetailCodeSorterEnum sorter);
    List<RoleDetailCodeDto> findMatchingUseY(String pcode, RoleDetailCodeSorterEnum sorter);
    long countMatchingUseY(String pcode, RoleDetailCodeSorterEnum sorter);
    List<RoleDetailCodeDto> findCreditUseY(String pcode, RoleDetailCodeSorterEnum sorter);
    long countCreditUseY(String pcode, RoleDetailCodeSorterEnum sorter);
    List<RoleDetailCodeDto> findSplitSheetUseY(String pcode, RoleDetailCodeSorterEnum sorter);
    long countSplitSheetUseY(String pcode, RoleDetailCodeSorterEnum sorter);
    List<RoleDetailCodeExcelDto> findExcelList(String pcode);

    void deleteQryId(Long id);

}
