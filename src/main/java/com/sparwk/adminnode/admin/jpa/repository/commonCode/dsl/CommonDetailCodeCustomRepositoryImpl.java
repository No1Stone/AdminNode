package com.sparwk.adminnode.admin.jpa.repository.commonCode.dsl;

import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.sparwk.adminnode.admin.biz.v1.commonCode.dto.*;
import com.sparwk.adminnode.admin.jpa.entity.PeriodTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.admin.QAdminEntity;
import com.sparwk.adminnode.admin.jpa.entity.commonCode.CommonCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.commonCode.CommonCateEnum2;
import com.sparwk.adminnode.admin.jpa.entity.commonCode.CommonSorterEnum;
import com.sparwk.adminnode.admin.jpa.entity.commonCode.CommonSorterEnum3;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static com.sparwk.adminnode.admin.jpa.entity.admin.QAdminEntity.adminEntity;
import static com.sparwk.adminnode.admin.jpa.entity.commonCode.QCommonCodeEntity.commonCodeEntity;
import static com.sparwk.adminnode.admin.jpa.entity.commonCode.QCommonDetailCodeEntity.commonDetailCodeEntity;

@Repository
public class CommonDetailCodeCustomRepositoryImpl implements CommonDetailCodeCustomRepository {

    private final JPAQueryFactory jpaQueryFactory;

    public CommonDetailCodeCustomRepositoryImpl(JPAQueryFactory jpaQueryFactory) {
        this.jpaQueryFactory = jpaQueryFactory;
    }

    @Override
    public List<CommonCateListDto> findQryCateList(String pcode) {
        return jpaQueryFactory.select(Projections.bean(CommonCateListDto.class,
                                commonCodeEntity.code.as("cateCd"),
                                commonCodeEntity.val.as("cateCdName")
                        )
                )
                .from(commonDetailCodeEntity)
                .where(commonPcodeEq(pcode), useTypeEq(YnTypeEnum.Y) )
                .orderBy(sorterOrder(CommonSorterEnum.NameAsc))
                .fetch();
    }

    @Override
    public List<CommonDetailCodeDto> findQryAll(String pcode,
                                                String code,
                                                CommonCateEnum cate,
                                                String val,
                                                YnTypeEnum useType,
                                                PeriodTypeEnum periodType,
                                                String sdate,
                                                String edate,
                                                CommonSorterEnum sorter,
                                                PageRequest pageRequest) {
        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(Projections.bean(CommonDetailCodeDto.class,
                                commonCodeEntity.code.as("pcode"),
                                commonCodeEntity.val.as("pcodeVal"),
                                commonDetailCodeEntity.commonDetailCodeSeq,
                                commonDetailCodeEntity.dcode,
                                commonDetailCodeEntity.val,
                                commonDetailCodeEntity.useType,
                                commonDetailCodeEntity.popularType,
                                commonDetailCodeEntity.description,
                                commonDetailCodeEntity.sortIndex,
                                commonDetailCodeEntity.hit,
                                commonDetailCodeEntity.regUsr,
                                adminEntity.fullName.as("regUsrName"),
                                commonDetailCodeEntity.regDt,
                                commonDetailCodeEntity.modUsr,
                                adminEntity2.fullName.as("modUsrName"),
                                commonDetailCodeEntity.modDt
                        )
                )
                .from(commonDetailCodeEntity)
                .join(commonDetailCodeEntity.commonCodeEntity, commonCodeEntity)
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(commonDetailCodeEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(commonDetailCodeEntity.modUsr))
                .where(commonPcodeEq(pcode), codeEq(code), valLike(cate, val), useTypeEq(useType), termBetween(periodType, sdate, edate))
                .limit(pageRequest.getPageSize())
                .offset(pageRequest.getPageSize() * (pageRequest.getPageNumber() - 1))
                .orderBy(sorterOrder(sorter))
                .fetch();
    }

    @Override
    public long countQryAll(String pcode,
                                                String code,
                                                CommonCateEnum cate,
                                                String val,
                                                YnTypeEnum useType,
                                                PeriodTypeEnum periodType,
                                                String sdate,
                                                String edate) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(
                                commonCodeEntity.code.count()
                )
                .from(commonDetailCodeEntity)
                .join(commonDetailCodeEntity.commonCodeEntity, commonCodeEntity)
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(commonDetailCodeEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(commonDetailCodeEntity.modUsr))
                .where(commonPcodeEq(pcode), codeEq(code), valLike(cate, val), useTypeEq(useType), termBetween(periodType, sdate, edate))
                .fetchOne();
    }

    @Override
    public List<CommonDetailCodeDto> findQryUseY(String pcode, String code, CommonCateEnum cate, String val) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(Projections.bean(CommonDetailCodeDto.class,
                                commonCodeEntity.code.as("pcode"),
                                commonCodeEntity.val.as("pcodeVal"),
                                commonDetailCodeEntity.commonDetailCodeSeq,
                                commonDetailCodeEntity.dcode,
                                commonDetailCodeEntity.val,
                                commonDetailCodeEntity.useType,
                                commonDetailCodeEntity.popularType,
                                commonDetailCodeEntity.description,
                                commonDetailCodeEntity.sortIndex,
                                commonDetailCodeEntity.hit,
                                commonDetailCodeEntity.regUsr,
                                adminEntity.fullName.as("regUsrName"),
                                commonDetailCodeEntity.regDt,
                                commonDetailCodeEntity.modUsr,
                                adminEntity2.fullName.as("modUsrName"),
                                commonDetailCodeEntity.modDt
                        )
                )
                .from(commonDetailCodeEntity)
                .join(commonDetailCodeEntity.commonCodeEntity, commonCodeEntity)
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(commonDetailCodeEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(commonDetailCodeEntity.modUsr))
                .where(commonPcodeEq(pcode), codeEq(code), valLike(cate, val), useTypeEq(YnTypeEnum.Y))
                .orderBy(sorterOrder(CommonSorterEnum.NameAsc))
                .fetch();
    }

    @Override
    public long countQryUseY(String pcode, String code, CommonCateEnum cate, String val) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(
                        commonCodeEntity.code.count()
                )
                .from(commonDetailCodeEntity)
                .join(commonDetailCodeEntity.commonCodeEntity, commonCodeEntity)
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(commonDetailCodeEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(commonDetailCodeEntity.modUsr))
                .where(commonPcodeEq(pcode), codeEq(code), valLike(cate, val), useTypeEq(YnTypeEnum.Y))
                .fetchOne();
    }

    @Override
    public List<CommonDetailCodeDto> findPopularUseY(String pcode, String code, CommonCateEnum cate, String val) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(Projections.bean(CommonDetailCodeDto.class,
                                commonCodeEntity.code.as("pcode"),
                                commonCodeEntity.val.as("pcodeVal"),
                                commonDetailCodeEntity.commonDetailCodeSeq,
                                commonDetailCodeEntity.dcode,
                                commonDetailCodeEntity.val,
                                commonDetailCodeEntity.useType,
                                commonDetailCodeEntity.popularType,
                                commonDetailCodeEntity.description,
                                commonDetailCodeEntity.sortIndex,
                                commonDetailCodeEntity.hit,
                                commonDetailCodeEntity.regUsr,
                                adminEntity.fullName.as("regUsrName"),
                                commonDetailCodeEntity.regDt,
                                commonDetailCodeEntity.modUsr,
                                adminEntity2.fullName.as("modUsrName"),
                                commonDetailCodeEntity.modDt
                        )
                )
                .from(commonDetailCodeEntity)
                .join(commonDetailCodeEntity.commonCodeEntity, commonCodeEntity)
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(commonDetailCodeEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(commonDetailCodeEntity.modUsr))
                .where(commonPcodeEq(pcode), codeEq(code), valLike(cate, val), popularTypeEq(YnTypeEnum.Y))
                .orderBy(sorterOrder(CommonSorterEnum.NameAsc))
                .fetch();
    }

    @Override
    public long countPopularUseY(String pcode, String code, CommonCateEnum cate, String val) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(
                        commonCodeEntity.code.count()
                )
                .from(commonDetailCodeEntity)
                .join(commonDetailCodeEntity.commonCodeEntity, commonCodeEntity)
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(commonDetailCodeEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(commonDetailCodeEntity.modUsr))
                .where(commonPcodeEq(pcode), codeEq(code), valLike(cate, val), popularTypeEq(YnTypeEnum.Y))
                .fetchOne();
    }

    @Override
    public List<CommonDetailCodeExcelDto> findExcelList(String pcode) {

        return jpaQueryFactory.select(Projections.bean(CommonDetailCodeExcelDto.class,
                                commonCodeEntity.code.as("pcode"),
                                commonCodeEntity.val.as("pcodeVal"),
                                commonDetailCodeEntity.commonDetailCodeSeq,
                                commonDetailCodeEntity.dcode,
                                commonDetailCodeEntity.val,
                                commonDetailCodeEntity.useType,
                                commonDetailCodeEntity.popularType,
                                commonDetailCodeEntity.description
                        )
                )
                .from(commonDetailCodeEntity)
                .join(commonDetailCodeEntity.commonCodeEntity, commonCodeEntity)
                .where(commonPcodeEq(pcode))
                .orderBy(sorterOrder(CommonSorterEnum.DcodeAsc))
                .fetch();
    }

    @Override
    public List<CommonDetailCodeDto> findQryCateUseY(CommonCateEnum2 cate,
                                                     String val,
                                                     YnTypeEnum useType,
                                                     CommonSorterEnum sorter,
                                                     PageRequest pageRequest
    ) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(Projections.bean(CommonDetailCodeDto.class,
                                commonCodeEntity.code.as("pcode"),
                                commonCodeEntity.val.as("pcodeVal"),
                                commonDetailCodeEntity.commonDetailCodeSeq,
                                commonDetailCodeEntity.dcode,
                                commonDetailCodeEntity.val,
                                commonDetailCodeEntity.useType,
                                commonDetailCodeEntity.popularType,
                                commonDetailCodeEntity.description,
                                commonDetailCodeEntity.sortIndex,
                                commonDetailCodeEntity.hit,
                                commonDetailCodeEntity.regUsr,
                                adminEntity.fullName.as("regUsrName"),
                                commonDetailCodeEntity.regDt,
                                commonDetailCodeEntity.modUsr,
                                adminEntity2.fullName.as("modUsrName"),
                                commonDetailCodeEntity.modDt
                        )
                )
                .from(commonDetailCodeEntity)
                .join(commonDetailCodeEntity.commonCodeEntity, commonCodeEntity)
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(commonDetailCodeEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(commonDetailCodeEntity.modUsr))
                .where(commonDetailCodeEntity.commonCodeEntity.code.in(new String[]{"HSC", "HSN", "HQA", "TAP"}), valLike2(cate, val), useTypeEq(YnTypeEnum.Y))
                .orderBy(sorterOrder(CommonSorterEnum.NameAsc))
                .fetch();
    }

    @Override
    public long countQryCateUseY(CommonCateEnum2 cate,
                                 String val,
                                 YnTypeEnum useType) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(
                        commonCodeEntity.code.count()
                )
                .from(commonDetailCodeEntity)
                .join(commonDetailCodeEntity.commonCodeEntity, commonCodeEntity)
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(commonDetailCodeEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(commonDetailCodeEntity.modUsr))
                .where(commonDetailCodeEntity.commonCodeEntity.code.in(new String[]{"HSC", "HSN", "HQA", "TAP"}), valLike2(cate, val), useTypeEq(YnTypeEnum.Y))
                .fetchOne();
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    @Override
    public List<CateCodeListDto> findQryCateCodeList(CommonCateEnum2 cate,
                                                     String val,
                                                     YnTypeEnum useType,
                                                     CommonSorterEnum3 sorter,
                                                     PageRequest pageRequest
    ) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(Projections.bean(CateCodeListDto.class,
                                commonCodeEntity.code.as("pcode"),
                                commonCodeEntity.val.as("pcodeVal"),
                                commonDetailCodeEntity.commonDetailCodeSeq,
                                commonDetailCodeEntity.dcode,
                                commonDetailCodeEntity.val,
                                commonDetailCodeEntity.useType,
                                commonDetailCodeEntity.sortIndex,
                                commonDetailCodeEntity.regUsr,
                                adminEntity.fullName.as("regUsrName"),
                                commonDetailCodeEntity.regDt,
                                commonDetailCodeEntity.modUsr,
                                adminEntity2.fullName.as("modUsrName"),
                                commonDetailCodeEntity.modDt
                        )
                )
                .from(commonDetailCodeEntity)
                .join(commonDetailCodeEntity.commonCodeEntity, commonCodeEntity)
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(commonDetailCodeEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(commonDetailCodeEntity.modUsr))
                .where(commonDetailCodeEntity.pcode.in(new String[]{"HSC", "HSN", "HQA", "TAP"}), valLike2(cate, val), useTypeEq(useType))
                .limit(pageRequest.getPageSize())
                .offset(pageRequest.getPageSize() * (pageRequest.getPageNumber() - 1))
                .orderBy(sorterOrder2(sorter))
                .fetch();
    }

    @Override
    public long countQryCateCodeList(CommonCateEnum2 cate,
                                 String val,
                                 YnTypeEnum useType) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(
                        commonCodeEntity.code.count()
                )
                .from(commonDetailCodeEntity)
                .join(commonDetailCodeEntity.commonCodeEntity, commonCodeEntity)
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(commonDetailCodeEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(commonDetailCodeEntity.modUsr))
                .where(commonDetailCodeEntity.pcode.in(new String[]{"HSC", "HSN", "HQA", "TAP"}), valLike2(cate, val), useTypeEq(useType))
                .fetchOne();
    }

    @Override
    public  List<CommonCodeDto> findQryCatePCodeList() {
        return jpaQueryFactory.select(Projections.bean(CommonCodeDto.class,
                                commonCodeEntity.code,
                                commonCodeEntity.commonCodeSeq,
                        commonCodeEntity.val,
                        commonCodeEntity.useType,
                        commonCodeEntity.regUsr,
                        commonCodeEntity.regDt,
                        commonCodeEntity.modUsr,
                        commonCodeEntity.modDt
                        )
                )
                .from(commonCodeEntity)
                .where(commonCodeEntity.code.in(new String[]{"HSC", "HSN", "HQA", "TAP"}))
                .orderBy(commonCodeEntity.val.asc())
                .fetch();
    }

    public void updateHit(Long id) {
        if (id > 0) {
            jpaQueryFactory.update(commonDetailCodeEntity)
                    .set(commonDetailCodeEntity.hit, commonDetailCodeEntity.hit.add(1))
                    .where(idEq(id))
                    .execute();
        }
    }

    @Override
    public void deleteQryId(Long id) {

        if (id > 0) {
            jpaQueryFactory.delete(commonDetailCodeEntity)
                    .where(idEq(id))
                    .execute();
        }
    }

    private BooleanExpression commonPcodeEq(String code) {
        if (code != null && code.length() > 0)
            return commonDetailCodeEntity.commonCodeEntity.code.eq(code);
        return null;
    }

    private BooleanExpression codeEq(String code) {
        if (code != null && code.length() > 0)
            return commonDetailCodeEntity.dcode.eq(code);
        return null;
    }

    private BooleanExpression valLike(CommonCateEnum cate, String val) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        if (cate != null && val != null && val.length() > 0) {
            switch (cate) {
                case All:
                    return commonDetailCodeEntity.val.toLowerCase().contains(val.toLowerCase())
                            .or(commonDetailCodeEntity.description.toLowerCase().contains(val.toLowerCase()));
                case Value:
                    return commonDetailCodeEntity.val.toLowerCase().contains(val.toLowerCase());
                case Description:
                    return commonDetailCodeEntity.description.toLowerCase().contains(val.toLowerCase());
                case RegUsrName:
                    return adminEntity.fullName.toLowerCase().contains(val.toLowerCase());
                case ModUsrName:
                    return adminEntity2.fullName.toLowerCase().contains(val.toLowerCase());
            }
        }
        return null;
    }

    private BooleanExpression valLike2(CommonCateEnum2 cate, String val) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        if (cate != null && val != null && val.length() > 0) {
            switch (cate) {
                case All:
                    return commonDetailCodeEntity.val.toLowerCase().contains(val.toLowerCase())
                            .or(commonCodeEntity.val.toLowerCase().contains(val.toLowerCase()));
                case Category:
                    return commonCodeEntity.val.toLowerCase().contains(val.toLowerCase());
                case Menu:
                    return commonDetailCodeEntity.val.toLowerCase().contains(val.toLowerCase());
                case RegUsrName:
                    return adminEntity.fullName.toLowerCase().contains(val.toLowerCase());
                case ModUsrName:
                    return adminEntity2.fullName.toLowerCase().contains(val.toLowerCase());
            }
        }
        return null;
    }

    private BooleanExpression idEq(Long id) {
        if (id != null)
            return commonDetailCodeEntity.commonDetailCodeSeq.eq(id);
        return null;
    }

    private BooleanExpression useTypeEq(YnTypeEnum useType) {
        if (useType != null)
            return commonDetailCodeEntity.useType.eq(useType);
        return null;
    }

    private BooleanExpression popularTypeEq(YnTypeEnum popularType) {
        if (popularType != null)
            return commonDetailCodeEntity.popularType.eq(popularType);
        return null;
    }

    private BooleanExpression termBetween(PeriodTypeEnum periodType, String sdate, String edate) {

        if (sdate != null && edate != null && periodType != null) {

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
            LocalDateTime searchStartDate = LocalDateTime.parse(sdate + " 00:00:00.000", formatter);
            LocalDateTime searchEndDate = LocalDateTime.parse(edate + " 23:59:59.999", formatter);

            switch (periodType) {
                case Created:
                    return commonDetailCodeEntity.regDt.between(searchStartDate, searchEndDate);
                case LastModified:
                    return commonDetailCodeEntity.modDt.between(searchStartDate, searchEndDate);
                default:
                    return null;
            }
        }
        return null;
    }

    private OrderSpecifier sorterOrder(CommonSorterEnum sorter) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        if (sorter == null)
            return commonDetailCodeEntity.modDt.asc();

        if (sorter == CommonSorterEnum.NameAsc)
            return commonDetailCodeEntity.val.asc();

        if (sorter == CommonSorterEnum.NameDesc)
            return commonDetailCodeEntity.val.desc();

        if (sorter == CommonSorterEnum.UseYnAsc)
            return commonDetailCodeEntity.useType.asc();

        if (sorter == CommonSorterEnum.UseYnDesc)
            return commonDetailCodeEntity.useType.desc();

        if (sorter == CommonSorterEnum.LastModifiedAsc)
            return commonDetailCodeEntity.modDt.asc();

        if (sorter == CommonSorterEnum.LastModifiedDesc)
            return commonDetailCodeEntity.modDt.desc();

        if (sorter == CommonSorterEnum.CreatedAsc)
            return commonDetailCodeEntity.regDt.asc();

        if (sorter == CommonSorterEnum.CreatedDesc)
            return commonDetailCodeEntity.regDt.desc();

        if (sorter == CommonSorterEnum.DcodeAsc)
            return commonDetailCodeEntity.dcode.asc();

        if (sorter == CommonSorterEnum.DcodeDesc)
            return commonDetailCodeEntity.dcode.desc();

        if (sorter == CommonSorterEnum.DescriptionAsc)
            return commonDetailCodeEntity.description.asc();

        if (sorter == CommonSorterEnum.DescriptionDesc)
            return commonDetailCodeEntity.description.desc();

        if (sorter == CommonSorterEnum.CreaterNameAsc)
            return adminEntity.fullName.asc();

        if (sorter == CommonSorterEnum.CreaterNameDesc)
            return adminEntity.fullName.desc();

        if (sorter == CommonSorterEnum.ModifierNameAsc)
            return adminEntity2.fullName.asc();

        if (sorter == CommonSorterEnum.ModifierNameDesc)
            return adminEntity2.fullName.desc();

        return commonDetailCodeEntity.regDt.desc();
    }

    private OrderSpecifier sorterOrder2(CommonSorterEnum3 sorter) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        if (sorter == null)
            return commonDetailCodeEntity.regDt.desc();

        if (sorter == CommonSorterEnum3.MenuAsc)
            return commonDetailCodeEntity.val.asc();

        if (sorter == CommonSorterEnum3.MenuDesc)
            return commonDetailCodeEntity.val.desc();

        if (sorter == CommonSorterEnum3.UseYnAsc)
            return commonDetailCodeEntity.useType.asc();

        if (sorter == CommonSorterEnum3.UseYnDesc)
            return commonDetailCodeEntity.useType.desc();

        if (sorter == CommonSorterEnum3.LastModifiedAsc)
            return commonDetailCodeEntity.modDt.asc();

        if (sorter == CommonSorterEnum3.LastModifiedDesc)
            return commonDetailCodeEntity.modDt.desc();

        if (sorter == CommonSorterEnum3.CreatedAsc)
            return commonDetailCodeEntity.regDt.asc();

        if (sorter == CommonSorterEnum3.CreatedDesc)
            return commonDetailCodeEntity.regDt.desc();

        if (sorter == CommonSorterEnum3.CategoryAsc)
            return commonCodeEntity.val.asc();

        if (sorter == CommonSorterEnum3.CreatedDesc)
            return commonCodeEntity.val.desc();

        if (sorter == CommonSorterEnum3.DescriptionAsc)
            return commonDetailCodeEntity.description.asc();

        if (sorter == CommonSorterEnum3.DescriptionDesc)
            return commonDetailCodeEntity.description.desc();

        if (sorter == CommonSorterEnum3.CreaterNameAsc)
            return adminEntity.fullName.asc();

        if (sorter == CommonSorterEnum3.CreaterNameDesc)
            return adminEntity.fullName.desc();

        if (sorter == CommonSorterEnum3.ModifierNameAsc)
            return adminEntity2.fullName.asc();

        if (sorter == CommonSorterEnum3.ModifierNameDesc)
            return adminEntity2.fullName.desc();

        return commonDetailCodeEntity.regDt.desc();
    }

}
