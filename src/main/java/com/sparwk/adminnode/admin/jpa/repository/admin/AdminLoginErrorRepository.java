package com.sparwk.adminnode.admin.jpa.repository.admin;

import com.sparwk.adminnode.admin.jpa.entity.admin.AdminLoginErrorEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AdminLoginErrorRepository extends JpaRepository<AdminLoginErrorEntity, Long> {

}
