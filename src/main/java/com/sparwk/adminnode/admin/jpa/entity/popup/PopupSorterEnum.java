package com.sparwk.adminnode.admin.jpa.entity.popup;

import lombok.Getter;

@Getter
public enum PopupSorterEnum {

    TitleAsc("TitleAsc"), TitleDesc("TitleDesc"),
    StatusAsc("StatusAsc"), StatusDesc("StatusDesc"),
    SdateAsc("SdateAsc"), SdateDesc("SdateDesc"),
    EdateAsc("EdateAsc"), EdateDesc("EdateDesc"),
    CreatedAsc("CreatedAsc"), CreatedDesc("CreatedDesc"),
    CreaterNameAsc("CreaterNameAsc"), CreaterNameDesc("CreaterNameDesc"),
    ModifierNameAsc("ModifierNameAsc"), ModifierNameDesc("ModifierNameDesc")
    ;

    private String sorter;

    PopupSorterEnum(String sorter) {
        this.sorter = sorter;
    }

}
