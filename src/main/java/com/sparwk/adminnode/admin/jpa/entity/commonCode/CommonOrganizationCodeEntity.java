package com.sparwk.adminnode.admin.jpa.entity.commonCode;

import com.sparwk.adminnode.admin.jpa.entity.BaseEntity;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.country.CountryEntity;
import lombok.*;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Setter
@Table(name = "tb_admin_common_organization_code")
public class CommonOrganizationCodeEntity extends BaseEntity {

    @Id
    @GeneratedValue(
            generator = "tb_admin_common_organization_code_seq"
    )
    @Column(name = "common_organization_code_seq")
    private Long commonOrganizationCodeSeq;

    @Column(name = "pcode", length = 3)
    private String pcode;

    @Column(name = "dcode", length = 10)
    private String dcode;

    @Column(name = "val", length = 50)
    private String val;

    @Column(name = "country_cd", length = 9)
    private String countryCd;

    @Column(name = "description", length = 2000)
    private String description;

    @Column(name = "sort_index")
    private int sortIndex;

    @Column(name = "hit")
    private int hit;

    @Enumerated(EnumType.STRING)
    @Column(name = "use_yn", length = 1)
    private YnTypeEnum useType;

    @Enumerated(EnumType.STRING)
    @Column(name = "popular_yn", length = 1)
    private YnTypeEnum popularType;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "pcode", referencedColumnName = "code", insertable = false, updatable = false)
    private CommonCodeEntity commonCodeEntity;

//    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
//    @JoinColumn(name = "country_cd", referencedColumnName = "country_cd", insertable = false, updatable = false)
//    private CountryEntity countryEntity;

    @Builder
    public CommonOrganizationCodeEntity(
            Long commonOrganizationCodeSeq,
            String pcode,
            String dcode,
            String val,
            String countryCd,
            String description,
            int sortIndex,
            int hit,
            YnTypeEnum useType,
            YnTypeEnum popularType,
            CommonCodeEntity commonCodeEntity
            //,CountryEntity countryEntity
    ) {
        this.commonOrganizationCodeSeq = commonOrganizationCodeSeq;
        this.pcode = pcode;
        this.dcode = dcode;
        this.val = val;
        this.countryCd = countryCd;
        this.description = description;
        this.sortIndex = sortIndex;
        this.hit = hit;
        this.useType = useType;
        this.popularType = popularType;
        this.commonCodeEntity = commonCodeEntity;
        //this.countryEntity = countryEntity;
    }


    public void updateYn(YnTypeEnum useType) {
        this.useType = useType;
    }

    public void updatePopularYn(YnTypeEnum popularType) {
        this.popularType = popularType;
    }

    public void update(
            String pcode,
            String dcode,
            String val,
            String countryCd,
            String description,
            int sortIndex,
            YnTypeEnum useType,
            YnTypeEnum popularType
    ) {
        this.pcode = pcode;
        this.dcode = dcode;
        this.val = val;
        this.countryCd = countryCd;
        this.description = description;
        this.sortIndex = sortIndex;
        this.useType = useType;
        this.popularType = popularType;
    }

}
