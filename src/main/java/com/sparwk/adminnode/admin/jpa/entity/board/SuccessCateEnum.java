package com.sparwk.adminnode.admin.jpa.entity.board;

import lombok.Getter;

@Getter
public enum SuccessCateEnum {

    All("All"), Title("Title"), Name("Name"),
    RegUsrName("RegUsrName"), ModUsrName("ModUsrName")
    ;

    private String boardCateEnum;

    SuccessCateEnum(String BoardCateEnum) {
        this.boardCateEnum = boardCateEnum;
    }
}
