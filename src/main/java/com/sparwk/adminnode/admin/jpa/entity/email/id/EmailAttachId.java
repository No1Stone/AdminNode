package com.sparwk.adminnode.admin.jpa.entity.email.id;

import lombok.Data;

import java.io.Serializable;

@Data
public class EmailAttachId implements Serializable {
    private Long emailSeq;
    private int attachOrder;
}
