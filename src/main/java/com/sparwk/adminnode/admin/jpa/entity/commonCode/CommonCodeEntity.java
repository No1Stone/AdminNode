package com.sparwk.adminnode.admin.jpa.entity.commonCode;

import com.sparwk.adminnode.admin.jpa.entity.BaseEntity;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name="tb_admin_common_code")
@IdClass(CommonIdClass.class)
public class CommonCodeEntity extends BaseEntity {

    @Id
    @GeneratedValue(
            generator = "tb_admin_common_code_seq"
    )
    @Column(name = "common_code_seq")
    private Long commonCodeSeq;

    @Column(name = "code", length = 3, unique = true)
    private String code;

    @Column(name = "val", length = 50)
    private String val;

    @Enumerated(EnumType.STRING)
    @Column(name = "use_yn", length = 1)
    private YnTypeEnum useType;

    @Column(name = "max_num")
    private int maxNum;

    @Column(name = "format", length = 20)
    private String format;

    @OneToMany(mappedBy = "commonCodeEntity", cascade = CascadeType.ALL)
    List<CommonDetailCodeEntity> commonDetailCodeEntity = new ArrayList<>();

    @Builder
    public CommonCodeEntity(
            Long commonCodeSeq,
            String code,
            String val,
            YnTypeEnum useType,
            int maxNum,
            String format
    ) {
        this.commonCodeSeq = commonCodeSeq;
        this.code = code;
        this.val = val;
        this.useType = useType;
        this.maxNum = maxNum;
        this.format = format;
    }


    public void updateYn(YnTypeEnum useType) {
        this.useType = useType;
    }

    public void update(
            String code,
            String val,
            YnTypeEnum useType
    ) {
        this.code = code;
        this.val = val;
        this.useType = useType;
    }

}
