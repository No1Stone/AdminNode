package com.sparwk.adminnode.admin.jpa.repository.passwordPolicy;

import com.sparwk.adminnode.admin.jpa.entity.passwordPolicy.PasswordPolicyEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PasswordPolicyRepository extends JpaRepository<PasswordPolicyEntity, Long> {
}
