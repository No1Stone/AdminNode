package com.sparwk.adminnode.admin.jpa.repository.account;

import com.sparwk.adminnode.admin.jpa.entity.account.AccountAssoiateViewEntity;
import com.sparwk.adminnode.admin.jpa.entity.account.AccountIndividualViewEntity;
import com.sparwk.adminnode.admin.jpa.repository.account.dsl.AccountAssoiateViewCustomRepository;
import com.sparwk.adminnode.admin.jpa.repository.account.dsl.AccountIndividualViewCustomRepository;
import com.sparwk.adminnode.admin.jpa.repository.codeSeq.CodeSeqRepository;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountAssoiateViewRepository extends JpaRepository<AccountAssoiateViewEntity, Long>,
        AccountAssoiateViewCustomRepository, CodeSeqRepository {
}
