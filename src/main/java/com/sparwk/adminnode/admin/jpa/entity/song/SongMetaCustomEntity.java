package com.sparwk.adminnode.admin.jpa.entity.song;

import com.sparwk.adminnode.admin.jpa.entity.BaseEntity;
import com.sparwk.adminnode.admin.jpa.entity.song.id.SongMetaCustomId;
import com.sparwk.adminnode.admin.jpa.entity.song.id.SongMetaId;
import lombok.*;

import javax.persistence.*;

@Entity
@Getter
@Setter
@IdClass(SongMetaCustomId.class)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_song_metadata_custom")
public class SongMetaCustomEntity extends BaseEntity {
    @Id
    @Column(name = "song_id", nullable = true)
    private long songId;
    @Id
    @Column(name = "metadata_seq", nullable = true)
    private long metadataSeq;
    @Column(name = "metadata_name", nullable = true)
    private String metadataName;
    @Column(name = "metadata_value", nullable = true)
    private String metadataValue;

    @Builder
    SongMetaCustomEntity(
            long songId,
            long metadataSeq,
            String metadataName,
            String metadataValue
    ) {
        this.songId = songId;
        this.metadataSeq = metadataSeq;
        this.metadataName = metadataName;
        this.metadataValue = metadataValue;
    }
}
