package com.sparwk.adminnode.admin.jpa.entity.permissionSet;

import com.sparwk.adminnode.admin.jpa.entity.BaseEntity;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name="tb_admin_menu")
public class AdminMenuEntity extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "menu_seq")
    private Long menuSeq;

    @Column(name = "menu_label", nullable = false, length = 100)
    private String menuLabel;

    @Column(name = "menu_depth")
    private int menuDepth;

    @Column(name = "upp_menu_seq")
    private Long uppMenuSeq;

    @Column(name = "sort_index")
    private int sortIndex;

    @Column(name = "description", nullable = false, length = 2000)
    private String description;

    @Enumerated(EnumType.STRING)
    @Column(name = "use_yn", length = 1)
    private YnTypeEnum useType;

    @Builder
    public AdminMenuEntity(
                            Long menuSeq,
                            String menuLabel,
                            int menuDepth,
                            Long uppMenuSeq,
                            int sortIndex,
                            String description,
                            YnTypeEnum useType
                        ) {
        this.menuSeq = menuSeq;
        this.menuLabel = menuLabel;
        this.menuDepth = menuDepth;
        this.uppMenuSeq = uppMenuSeq;
        this.sortIndex = sortIndex;
        this.description = description;
        this.useType = useType;
    }


    public void updateYn(YnTypeEnum useType) {
        this.useType = useType;
    }

    public void update(
            String menuLabel,
            int menuDepth,
            Long uppMenuSeq,
            String description,
            YnTypeEnum useType
                        ) {
        this.menuLabel = menuLabel;
        this.menuDepth = menuDepth;
        this.uppMenuSeq = uppMenuSeq;
        this.description = description;
        this.useType = useType;
    }
}
