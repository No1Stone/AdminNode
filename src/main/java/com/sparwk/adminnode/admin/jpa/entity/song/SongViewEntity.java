package com.sparwk.adminnode.admin.jpa.entity.song;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "view_admin_song_list")
public class SongViewEntity {
    @Id
    @Column(name = "song_id", nullable = true)
    private Long songId;
    @Column(name = "song_title", nullable = true)
    private String songTitle;
    @Column(name = "avatar_file_url", nullable = true)
    private String avatarFileUrl;
    @Column(name = "song_file_path", nullable = true)
    private String songFilePath;
    @Column(name = "song_owner", nullable = true)
    private Long songOwner;
    @Column(name = "song_owner_name", nullable = true)
    private String songOwnerName;
    @Column(name = "song_cowriters_name", nullable = true)
    private String songCowritersName;
    @Column(name = "proj_title", nullable = true)
    private String projTitle;
    @Column(name = "song_genre_cd", nullable = true)
    private String songGenreCd;
    @Column(name = "song_genre_name", nullable = true)
    private String songGenreName;
    @Column(name = "song_subgenre_cd", nullable = true)
    private String songSubgenreCd;
    @Column(name = "song_subgenre_name", nullable = true)
    private String songSubgenreName;
    @Column(name = "song_mood_cd", nullable = true)
    private String songMoodCd;
    @Column(name = "song_mood_name", nullable = true)
    private String songMoodName;
    @Column(name = "song_theme_cd", nullable = true)
    private String songThemeCd;
    @Column(name = "song_theme_name", nullable = true)
    private String songThemeName;
    @Column(name = "song_version_cd", nullable = true)
    private String songVersionCd;
    @Column(name = "song_version_cd_name", nullable = true)
    private String songVersionCdName;
    @Column(name = "song_status_cd", nullable = true)
    private String songStatusCd;
    @Column(name = "song_status_cd_name", nullable = true)
    private String songStatusCdName;
    @Column(name = "song_lang_cd", nullable = true)
    private String songLangCd;
    @Column(name = "song_lang_cd_name", nullable = true)
    private String songLangCdName;
    @Column(name = "bpm", nullable = true)
    private long bpm;
    @Column(name = "duration", nullable = true)
    private String duration;
    @Column(name = "tempo", nullable = true)
    private String tempo;
    @Column(name = "isrc", nullable = true)
    private String isrc;
    @Column(name = "key_signature", nullable = true)
    private String keySignature;
    @Column(name = "time_signature", nullable = true)
    private String timeSignature;
    @Column(name = "lyrics", nullable = true)
    private String lyrics;
    @Column(name = "lyrics_comt", nullable = true)
    private String lyricsComt;
    @Column(name = "reg_usr", nullable = true)
    private Long regUsr;
    @Column(name = "reg_usr_name", nullable = true)
    private String regUsrName;
    @Column(name = "reg_dt", updatable = false)
    private LocalDateTime regDt;
    @Column(name = "mod_usr", nullable = true)
    private Long modUsr;
    @Column(name = "mod_usr_name", nullable = true)
    private String modUsrName;
    @Column(name = "mod_dt", updatable = false)
    private LocalDateTime modDt;


    @Builder
    SongViewEntity(
            Long songId,
            String songTitle,
            String avatarFileUrl,
            String songFilePath,
            Long songOwner,
            String songOwnerName,
            String songCowritersName,
            String projTitle,
            String songGenreCd,
            String songGenreName,
            String songSubgenreCd,
            String songSubgenreName,
            String songMoodCd,
            String songMoodName,
            String songThemeCd,
            String songThemeName,
            String songVersionCd,
            String songVersionCdName,
            String songStatusCd,
            String songStatusCdName,
            String songLangCd,
            String songLangCdName,
            long bpm,
            String duration,
            String tempo,
            String isrc,
            String keySignature,
            String timeSignature,
            String lyrics,
            String lyricsComt,
            Long regUsr,
            String regUsrName,
            LocalDateTime regDt,
            Long modUsr,
            String modUsrName,
            LocalDateTime modDt
    ) {
        this.songId = songId;
        this.songTitle = songTitle;
        this.avatarFileUrl = avatarFileUrl;
        this.songFilePath = songFilePath;
        this.songOwner = songOwner;
        this.songOwnerName = songOwnerName;
        this.songCowritersName = songCowritersName;
        this.projTitle = projTitle;
        this.songGenreCd = songGenreCd;
        this.songGenreName = songGenreName;
        this.songSubgenreCd = songSubgenreCd;
        this.songSubgenreName = songSubgenreName;
        this.songMoodCd = songMoodCd;
        this.songMoodName = songMoodName;
        this.songThemeCd = songThemeCd;
        this.songThemeName = songThemeName;
        this.songVersionCd = songVersionCd;
        this.songVersionCdName = songVersionCdName;
        this.songStatusCd = songStatusCd;
        this.songStatusCdName = songStatusCdName;
        this.songLangCd = songLangCd;
        this.songLangCdName = songLangCdName;
        this.bpm = bpm;
        this.duration = duration;
        this.tempo = tempo;
        this.isrc = isrc;
        this.keySignature = keySignature;
        this.timeSignature = timeSignature;
        this.lyrics = lyrics;
        this.lyricsComt = lyricsComt;
        this.regUsr = regUsr;
        this.regUsrName = regUsrName;
        this.regDt = regDt;
        this.modUsr = modUsr;
        this.modUsrName = modUsrName;
        this.modDt = modDt;
    }

}
