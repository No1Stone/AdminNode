package com.sparwk.adminnode.admin.jpa.entity.projects.id;

import lombok.Data;

import java.io.Serializable;

@Data
public class ProjectReferenceFileId implements Serializable {
    private long projId;
    private long fileFefSeq;
}
