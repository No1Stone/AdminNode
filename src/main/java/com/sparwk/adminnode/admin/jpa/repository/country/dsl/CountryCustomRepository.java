package com.sparwk.adminnode.admin.jpa.repository.country.dsl;

import com.sparwk.adminnode.admin.biz.v1.country.dto.CountryDto;
import com.sparwk.adminnode.admin.biz.v1.country.dto.CountryExcelDto;
import com.sparwk.adminnode.admin.jpa.entity.PeriodTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.country.CountryCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.country.CountrySorterEnum;
import org.springframework.data.domain.PageRequest;

import java.util.List;

public interface CountryCustomRepository {
    List<CountryDto> findQryAll(CountryCateEnum cate,
                                String val,
                                YnTypeEnum useType,
                                PeriodTypeEnum periodType,
                                String sdate,
                                String edate,
                                CountrySorterEnum sorter,
                                PageRequest pageRequest);
    long countQryAll(           CountryCateEnum cate,
                                String val,
                                YnTypeEnum useType,
                                PeriodTypeEnum periodType,
                                String sdate,
                                String edate);

    List<CountryDto> findQryUseY(CountryCateEnum cate, String val);
    long countQryUseY(CountryCateEnum cate, String val);
    List<CountryExcelDto> findExcelList();
}
