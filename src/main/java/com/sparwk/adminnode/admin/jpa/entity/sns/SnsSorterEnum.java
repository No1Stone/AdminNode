package com.sparwk.adminnode.admin.jpa.entity.sns;

import lombok.Getter;

@Getter
public enum SnsSorterEnum {

    NameAsc("NameAsc"), NameDesc("NameDesc"),
    UseYnAsc("UseYnAsc"), UseYnDesc("UseYnDesc"),
    LastModifiedAsc("LastModifiedAsc"), LastModifiedDesc("LastModifiedDesc"),
    CreatedAsc("CreatedAsc"), CreatedDesc("CreatedDesc"),
    CreaterNameAsc("CreaterNameAsc"), CreaterNameDesc("CreaterNameDesc"),
    ModifierNameAsc("ModifierNameAsc"), ModifierNameDesc("ModifierNameDesc")
    ;

    private String sorter;

    SnsSorterEnum(String sorter) {
        this.sorter = sorter;
    }

}
