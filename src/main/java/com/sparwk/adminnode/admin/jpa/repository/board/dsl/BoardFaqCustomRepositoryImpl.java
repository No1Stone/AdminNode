package com.sparwk.adminnode.admin.jpa.repository.board.dsl;

import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.sparwk.adminnode.admin.biz.v1.board.dto.BoardFaqDto;
import com.sparwk.adminnode.admin.jpa.entity.PeriodTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.admin.QAdminEntity;
import com.sparwk.adminnode.admin.jpa.entity.board.BoardCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.board.BoardSorterEnum;
import com.sparwk.adminnode.admin.jpa.entity.commonCode.CommonSorterEnum2;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static com.sparwk.adminnode.admin.jpa.entity.admin.QAdminEntity.adminEntity;
import static com.sparwk.adminnode.admin.jpa.entity.board.QBoardFaqEntity.boardFaqEntity;
import static com.sparwk.adminnode.admin.jpa.entity.board.QBoardPolicyEntity.boardPolicyEntity;
import static com.sparwk.adminnode.admin.jpa.entity.commonCode.QCommonDetailCodeEntity.commonDetailCodeEntity;
import static com.sparwk.adminnode.admin.jpa.entity.commonCode.QCommonOrganizationCodeEntity.commonOrganizationCodeEntity;

@Repository
public class BoardFaqCustomRepositoryImpl implements BoardFaqCustomRepository {

    private final JPAQueryFactory jpaQueryFactory;

    public BoardFaqCustomRepositoryImpl(JPAQueryFactory jpaQueryFactory) {
        this.jpaQueryFactory = jpaQueryFactory;
    }

    @Override
    public List<BoardFaqDto> findQryAll(String type,
                                        String pcode,
                                        BoardCateEnum cate,
                                        String val,
                                        YnTypeEnum useType,
                                        PeriodTypeEnum periodType,
                                        String sdate,
                                        String edate,
                                        BoardSorterEnum sorter,
                                        PageRequest pageRequest) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(Projections.bean(BoardFaqDto.class,

                        boardFaqEntity.faqId,
                        boardFaqEntity.cateCd,
                        commonDetailCodeEntity.val.as("cateCdName"),
                        boardFaqEntity.title,
                        boardFaqEntity.content,
                        boardFaqEntity.useYn.as("useType"),
                        boardFaqEntity.attachYn,
                        boardFaqEntity.hit,
                        boardFaqEntity.regUsr,
                        adminEntity.fullName.as("regUsrName"),
                        boardFaqEntity.regDt,
                        boardFaqEntity.modUsr,
                        adminEntity2.fullName.as("modUsrName"),
                        boardFaqEntity.modDt
                        )
                )
                .from(boardFaqEntity)
                .leftJoin(commonDetailCodeEntity).on(boardFaqEntity.cateCd.eq(commonDetailCodeEntity.dcode))
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(boardFaqEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(boardFaqEntity.modUsr))
                .where(boardPcodeEq(pcode), valLike(cate, val), useTypeEq(useType), termBetween(periodType, sdate, edate))
                .limit(pageRequest.getPageSize())
                .offset(pageRequest.getPageSize() * (pageRequest.getPageNumber() - 1))
                .orderBy(sorterOrder(sorter))
                .fetch();
    }

    @Override
    public long countQryAll(String type,
                                        String pcode,
                                        BoardCateEnum cate,
                                        String val,
                                        YnTypeEnum useType,
                                        PeriodTypeEnum periodType,
                                        String sdate,
                                        String edate) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(
                        boardFaqEntity.faqId.count()
                )
                .from(boardFaqEntity)
                .leftJoin(commonDetailCodeEntity).on(boardFaqEntity.cateCd.eq(commonDetailCodeEntity.description))
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(boardFaqEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(boardFaqEntity.modUsr))
                .where(boardPcodeEq(pcode), valLike(cate, val), useTypeEq(useType), termBetween(periodType, sdate, edate))
                .fetchOne();
    }

    @Override
    public List<BoardFaqDto> findQryUseY(String type,
                                          String pcode,
                                         BoardCateEnum cate,
                                         String val,
                                          PageRequest pageRequest
                                        ) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(Projections.bean(BoardFaqDto.class,

                                boardFaqEntity.faqId,
                                boardFaqEntity.cateCd,
                                commonDetailCodeEntity.val.as("cateCdName"),
                                boardFaqEntity.title,
                                boardFaqEntity.content,
                                boardFaqEntity.useYn.as("useType"),
                                boardFaqEntity.attachYn,
                                boardFaqEntity.hit,
                                boardFaqEntity.regUsr,
                                adminEntity.fullName.as("regUsrName"),
                                boardFaqEntity.regDt,
                                boardFaqEntity.modUsr,
                                adminEntity2.fullName.as("modUsrName"),
                                boardFaqEntity.modDt
                        )
                )
                .from(boardFaqEntity)
                .leftJoin(commonDetailCodeEntity).on(boardFaqEntity.cateCd.eq(commonDetailCodeEntity.dcode))
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(boardFaqEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(boardFaqEntity.modUsr))
                .where(boardPcodeEq(pcode), valLike(cate, val), useTypeEq(YnTypeEnum.Y))
                .limit(pageRequest.getPageSize())
                .offset(pageRequest.getPageSize() * (pageRequest.getPageNumber() - 1))
                .orderBy(sorterOrder(BoardSorterEnum.CreatedDesc))
                .fetch();
    }

    @Override
    public long countQryUseY(String type,
                                         String pcode,
                                         BoardCateEnum cate,
                                         String val
    ) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(
                                boardFaqEntity.faqId.count()
                )
                .from(boardFaqEntity)
                .leftJoin(commonDetailCodeEntity).on(boardFaqEntity.cateCd.eq(commonDetailCodeEntity.dcode))
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(boardFaqEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(boardFaqEntity.modUsr))
                .where(boardPcodeEq(pcode), valLike(cate, val), useTypeEq(YnTypeEnum.Y))
                .fetchOne();
    }

    @Override
    public long deleteQryList(Long[] id) {
        return jpaQueryFactory.delete(boardFaqEntity)
                .where(boardFaqEntity.faqId.in(id))
                .execute();
    }

    private BooleanExpression boardPcodeEq(String code) {
        if (code != null && code.length() > 0)
            return boardFaqEntity.cateCd.eq(code);
        return null;
    }

    private BooleanExpression valLike(BoardCateEnum cate, String val) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        if (cate != null && val != null && val.length() > 0) {
            switch (cate) {
                case All:
                    return boardFaqEntity.title.toLowerCase().contains(val.toLowerCase())
                            .or(boardFaqEntity.content.toLowerCase().contains(val.toLowerCase()));
                case Title:
                    return boardFaqEntity.title.toLowerCase().contains(val.toLowerCase());
                case Content:
                    return boardFaqEntity.content.toLowerCase().contains(val.toLowerCase());
                case RegUsrName:
                    return adminEntity.fullName.toLowerCase().contains(val.toLowerCase());
                case ModUsrName:
                    return adminEntity2.fullName.toLowerCase().contains(val.toLowerCase());
            }
        }
        return null;
    }

    private BooleanExpression idEq(Long id) {
        if (id != null)
            return boardFaqEntity.faqId.eq(id);
        return null;
    }

    private BooleanExpression useTypeEq(YnTypeEnum useType) {
        if (useType != null)
            return boardFaqEntity.useYn.eq(useType);
        return null;
    }

    private BooleanExpression termBetween(PeriodTypeEnum periodType, String sdate, String edate) {

        if (sdate != null && edate != null && periodType != null) {

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
            LocalDateTime searchStartDate = LocalDateTime.parse(sdate + " 00:00:00.000", formatter);
            LocalDateTime searchEndDate = LocalDateTime.parse(edate + " 23:59:59.999", formatter);

            switch (periodType) {
                case Created:
                    return boardFaqEntity.regDt.between(searchStartDate, searchEndDate);
                case LastModified:
                    return boardFaqEntity.modDt.between(searchStartDate, searchEndDate);
                default:
                    return null;
            }
        }
        return null;
    }

    private OrderSpecifier sorterOrder(BoardSorterEnum sorter) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        if (sorter == null)
            return boardFaqEntity.regDt.desc();

        if (sorter == BoardSorterEnum.NameAsc)
            return boardFaqEntity.title.asc();

        if (sorter == BoardSorterEnum.NameDesc)
            return boardFaqEntity.title.desc();

        if (sorter == BoardSorterEnum.TopicAsc)
            return commonDetailCodeEntity.val.asc();

        if (sorter == BoardSorterEnum.TopicDesc)
            return commonDetailCodeEntity.val.desc();

        if (sorter == BoardSorterEnum.LastModifiedAsc)
            return boardFaqEntity.modDt.asc();

        if (sorter == BoardSorterEnum.LastModifiedDesc)
            return boardFaqEntity.modDt.desc();

        if (sorter == BoardSorterEnum.CreatedAsc)
            return boardFaqEntity.regDt.asc();

        if (sorter == BoardSorterEnum.CreatedDesc)
            return boardFaqEntity.regDt.desc();

        if (sorter == BoardSorterEnum.CreaterNameAsc)
            return adminEntity.fullName.asc();

        if (sorter == BoardSorterEnum.CreaterNameDesc)
            return adminEntity.fullName.desc();

        if (sorter == BoardSorterEnum.ModifierNameAsc)
            return adminEntity2.fullName.asc();

        if (sorter == BoardSorterEnum.ModifierNameDesc)
            return adminEntity2.fullName.desc();

        return boardFaqEntity.regDt.desc();
    }



}
