package com.sparwk.adminnode.admin.jpa.repository.email.dsl;

import com.sparwk.adminnode.admin.biz.v1.email.dto.EmailListDto;
import com.sparwk.adminnode.admin.biz.v1.email.dto.EmailMemberListDto;
import com.sparwk.adminnode.admin.biz.v1.email.dto.EmailMemberMailListDto;
import com.sparwk.adminnode.admin.jpa.entity.email.EmailCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.email.EmailMemberCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.email.EmailSorterEnum;
import org.springframework.data.domain.PageRequest;

import java.util.List;

public interface EmailCustomRepository {

    List<EmailListDto> findQryAll(EmailCateEnum cate,
                                  String val,
                                  EmailSorterEnum sorter,
                                  PageRequest pageRequest);
    long countQryAll(EmailCateEnum cate,
                     String val,
                     EmailSorterEnum sorter);

    List<EmailMemberListDto> findQryMemberList(EmailMemberCateEnum cate,
                                               String val,
                                               String IndiYn,
                                               String comYn,
                                               String etcYn,
                                               PageRequest pageRequest);

    List<EmailMemberMailListDto> findQryMemberAll(EmailMemberCateEnum cate,
                                                  String val,
                                                  String IndiYn,
                                                  String comYn,
                                                  String etcYn);
    long countQryMember(EmailMemberCateEnum cate,
                           String val,
                           String IndiYn,
                           String comYn,
                           String etcYn);
}
