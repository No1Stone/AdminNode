package com.sparwk.adminnode.admin.jpa.repository.board;

import com.sparwk.adminnode.admin.biz.v1.board.dto.BoardAttachDto;
import com.sparwk.adminnode.admin.jpa.entity.board.BoardAttachEntity;
import com.sparwk.adminnode.admin.jpa.repository.board.dsl.BoardAttachCustomRepository;
import com.sparwk.adminnode.admin.jpa.repository.codeSeq.CodeSeqRepository;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BoardAttachRepository extends JpaRepository<BoardAttachEntity, Long>, BoardAttachCustomRepository, CodeSeqRepository {
    List<BoardAttachEntity> findByBoardId(Long id);

    //파일 삭제
    void deleteByBoardId(Long BoardId);

    //숫자 확인
    int countByBoardId(Long id);
}
