package com.sparwk.adminnode.admin.jpa.repository.board.dsl;

import com.sparwk.adminnode.admin.biz.v1.board.dto.BoardQnaDto;
import com.sparwk.adminnode.admin.jpa.entity.board.QnaCateEnum;
import org.springframework.data.domain.PageRequest;

import java.util.List;

public interface BoardQnaCustomRepository {

    List<BoardQnaDto> findQryQna(String type,

                                     String val,
                                 QnaCateEnum cate,
                                     PageRequest pageRequest
    );

    long countQryAll(String type,
                     String val,
                     QnaCateEnum cate
    );

}
