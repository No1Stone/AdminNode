package com.sparwk.adminnode.admin.jpa.repository.anr;

import com.sparwk.adminnode.admin.jpa.entity.anr.AnrEntity;
import com.sparwk.adminnode.admin.jpa.repository.anr.dsl.AnrCustomRepository;
import com.sparwk.adminnode.admin.jpa.repository.codeSeq.CodeSeqRepository;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AnrRepository extends JpaRepository<AnrEntity, Long>, AnrCustomRepository, CodeSeqRepository {
    Optional<AnrEntity> findByAnrCd(String anrCd);
}
