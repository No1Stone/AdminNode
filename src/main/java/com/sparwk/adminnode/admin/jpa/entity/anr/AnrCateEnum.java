package com.sparwk.adminnode.admin.jpa.entity.anr;

import lombok.Getter;

@Getter
public enum AnrCateEnum {

    All("All"),
    ServiceName("ServiceName"),
    Group("Group"),
    Description("Description"),
    RegUsrName("RegUsrName"),
    ModUsrName("ModUsrName")
    ;

    private String anrCateEnum;

    AnrCateEnum(String anrCateEnum) {
        this.anrCateEnum = anrCateEnum;
    }
}
