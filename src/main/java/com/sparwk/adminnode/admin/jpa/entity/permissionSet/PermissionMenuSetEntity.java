package com.sparwk.adminnode.admin.jpa.entity.permissionSet;

import com.sparwk.adminnode.admin.jpa.entity.BaseEntity;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.permissionSet.id.PermissionMenuSetId;
import com.sparwk.adminnode.admin.jpa.entity.projects.id.ProjectBookmarkId;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@IdClass(PermissionMenuSetId.class)
@Table(name="tb_admin_permission_menu")
public class PermissionMenuSetEntity extends BaseEntity {

    @Id
    @Column(name = "admin_menu_id")
    private Long adminMenuId;

    @Id
    @Column(name = "admin_permission_id")
    private Long adminPermissionId;

    @Enumerated(EnumType.STRING)
    @Column(name = "create_yn", length = 1)
    private YnTypeEnum createYn;

    @Enumerated(EnumType.STRING)
    @Column(name = "read_yn", length = 1)
    private YnTypeEnum readYn;

    @Enumerated(EnumType.STRING)
    @Column(name = "edit_yn", length = 1)
    private YnTypeEnum editYn;

    @Enumerated(EnumType.STRING)
    @Column(name = "delete_yn", length = 1)
    private YnTypeEnum deleteYn;


    @Builder
    public PermissionMenuSetEntity(
                            Long adminMenuId,
                            Long adminPermissionId,
                            YnTypeEnum createYn,
                            YnTypeEnum readYn,
                            YnTypeEnum editYn,
                            YnTypeEnum deleteYn
                        ) {
        this.adminMenuId = adminMenuId;
        this.adminPermissionId = adminPermissionId;
        this.createYn = createYn;
        this.readYn = readYn;
        this.editYn = editYn;
        this.deleteYn = deleteYn;
    }


    public void update(
            YnTypeEnum createYn,
            YnTypeEnum readYn,
            YnTypeEnum editYn,
            YnTypeEnum deleteYn
                        ) {
        this.createYn = createYn;
        this.readYn = readYn;
        this.editYn = editYn;
        this.deleteYn = deleteYn;
    }
}
