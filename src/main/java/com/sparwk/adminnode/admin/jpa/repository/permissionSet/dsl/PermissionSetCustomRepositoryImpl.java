package com.sparwk.adminnode.admin.jpa.repository.permissionSet.dsl;

import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.sparwk.adminnode.admin.biz.v1.permissionSet.dto.PermissionSetDto;
import com.sparwk.adminnode.admin.jpa.entity.PeriodTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.admin.QAdminEntity;
import com.sparwk.adminnode.admin.jpa.entity.commonCode.CommonSorterEnum2;
import com.sparwk.adminnode.admin.jpa.entity.permissionSet.PermissionSetCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.permissionSet.PermissionSetSorterEnum;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static com.sparwk.adminnode.admin.jpa.entity.admin.QAdminEntity.adminEntity;
import static com.sparwk.adminnode.admin.jpa.entity.commonCode.QCommonOrganizationCodeEntity.commonOrganizationCodeEntity;
import static com.sparwk.adminnode.admin.jpa.entity.permissionSet.QPermissionSetEntity.permissionSetEntity;
import static com.sparwk.adminnode.admin.jpa.entity.permissionSet.QPermissionSetEntity.permissionSetEntity;

@Repository
public class PermissionSetCustomRepositoryImpl implements PermissionSetCustomRepository {

    private final JPAQueryFactory jpaQueryFactory;

    public PermissionSetCustomRepositoryImpl(JPAQueryFactory jpaQueryFactory) {
        this.jpaQueryFactory = jpaQueryFactory;
    }

    @Override
    public List<PermissionSetDto> findQryAll(PermissionSetCateEnum cate,
                                             String labelVal,
                                             YnTypeEnum useType,
                                             PeriodTypeEnum periodType,
                                             String sdate,
                                             String edate,
                                             PermissionSetSorterEnum sorter,
                                             PageRequest pageRequest) {
        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(Projections.bean(PermissionSetDto.class,
                        permissionSetEntity.adminPermissionId,
                        permissionSetEntity.labelVal,
                        permissionSetEntity.useType,
                        permissionSetEntity.cdDesc,
                        permissionSetEntity.cdOrd,
                        permissionSetEntity.regUsr,
                        adminEntity.fullName.as("regUsrName"),
                        permissionSetEntity.regDt,
                        permissionSetEntity.modUsr,
                        adminEntity2.fullName.as("modUsrName"),
                        permissionSetEntity.modDt
                        )
                )
                .from(permissionSetEntity)
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(permissionSetEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(permissionSetEntity.modUsr))
                .where(cdValLike(cate, labelVal), useTypeEq(useType), termBetween(periodType, sdate, edate))
                .limit(pageRequest.getPageSize())
                .offset(pageRequest.getPageSize() * (pageRequest.getPageNumber() - 1))
                .orderBy(sorterOrder(sorter))
                .fetch();
    }

    @Override
    public long countQryAll(PermissionSetCateEnum cate,
                                             String labelVal,
                                             YnTypeEnum useType,
                                             PeriodTypeEnum periodType,
                                             String sdate,
                                             String edate) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(
                                permissionSetEntity.adminPermissionId.count()
                )
                .from(permissionSetEntity)
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(permissionSetEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(permissionSetEntity.modUsr))
                .where(cdValLike(cate, labelVal), useTypeEq(useType), termBetween(periodType, sdate, edate))
                .fetchOne();
    }

    @Override
    public List<PermissionSetDto> findQryUseY(PermissionSetCateEnum cate, String labelVal) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(Projections.bean(PermissionSetDto.class,
                                permissionSetEntity.adminPermissionId,
                                permissionSetEntity.labelVal,
                                permissionSetEntity.useType,
                                permissionSetEntity.cdDesc,
                                permissionSetEntity.cdOrd,
                                permissionSetEntity.regUsr,
                                adminEntity.fullName.as("regUsrName"),
                                permissionSetEntity.regDt,
                                permissionSetEntity.modUsr,
                                adminEntity2.fullName.as("modUsrName"),
                                permissionSetEntity.modDt
                        )
                )
                .from(permissionSetEntity)
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(permissionSetEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(permissionSetEntity.modUsr))
                .where(cdValLike(cate, labelVal), useTypeEq(YnTypeEnum.Y))
                .orderBy(sorterOrder(PermissionSetSorterEnum.NameAsc))
                .fetch();
    }

     private BooleanExpression cdValLike(PermissionSetCateEnum cate, String val) {

         //검색 때문에 추가
         QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

         if (cate !=null && val != null && val.length() > 0) {
             switch (cate) {
                 case All:
                     return permissionSetEntity.labelVal.toLowerCase().contains(val.toLowerCase())
                             .or(permissionSetEntity.cdDesc.toLowerCase().contains(val.toLowerCase()));
                 case Label:
                     return permissionSetEntity.labelVal.toLowerCase().contains(val.toLowerCase());
                 case Description:
                     return permissionSetEntity.cdDesc.toLowerCase().contains(val.toLowerCase());
                 case RegUsrName:
                     return adminEntity.fullName.toLowerCase().contains(val.toLowerCase());
                 case ModUsrName:
                     return adminEntity2.fullName.toLowerCase().contains(val.toLowerCase());
             }
         }
         return null;
    }

    private BooleanExpression useTypeEq(YnTypeEnum useType) {
        if (useType != null)
            return permissionSetEntity.useType.eq(useType);
        return null;
    }

    private BooleanExpression termBetween(PeriodTypeEnum periodType, String  sdate, String edate) {

        if (sdate != null && edate != null && periodType != null) {

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
            LocalDateTime searchStartDate = LocalDateTime.parse(sdate + " 00:00:00.000", formatter);
            LocalDateTime searchEndDate = LocalDateTime.parse(edate + " 23:59:59.999", formatter);

            switch (periodType) {
                case Created:
                    return permissionSetEntity.regDt.between(searchStartDate, searchEndDate);
                case LastModified:
                    return permissionSetEntity.modDt.between(searchStartDate, searchEndDate);
                default:
                    return null;
            }
        }
        return null;
    }

    private OrderSpecifier sorterOrder(PermissionSetSorterEnum sorter) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        if (sorter == null)
            return permissionSetEntity.regDt.desc();

        if (sorter == PermissionSetSorterEnum.NameAsc)
            return permissionSetEntity.labelVal.asc();

        if (sorter == PermissionSetSorterEnum.NameDesc)
            return permissionSetEntity.labelVal.desc();

        if (sorter == PermissionSetSorterEnum.UseYnAsc)
            return permissionSetEntity.useType.asc();

        if (sorter == PermissionSetSorterEnum.UseYnDesc)
            return permissionSetEntity.useType.desc();

        if (sorter == PermissionSetSorterEnum.LastModifiedAsc)
            return permissionSetEntity.modDt.asc();

        if (sorter == PermissionSetSorterEnum.LastModifiedDesc)
            return permissionSetEntity.modDt.desc();

        if (sorter == PermissionSetSorterEnum.CreatedAsc)
            return permissionSetEntity.regDt.asc();

        if (sorter == PermissionSetSorterEnum.CreatedDesc)
            return permissionSetEntity.regDt.desc();

        if (sorter == PermissionSetSorterEnum.CreaterNameAsc)
            return adminEntity.fullName.asc();

        if (sorter == PermissionSetSorterEnum.CreaterNameDesc)
            return adminEntity.fullName.desc();

        if (sorter == PermissionSetSorterEnum.ModifierNameAsc)
            return adminEntity2.fullName.asc();

        if (sorter == PermissionSetSorterEnum.ModifierNameDesc)
            return adminEntity2.fullName.desc();

        return permissionSetEntity.regDt.desc();
    }

}
