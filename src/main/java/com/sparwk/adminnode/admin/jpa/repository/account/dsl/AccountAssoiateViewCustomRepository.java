package com.sparwk.adminnode.admin.jpa.repository.account.dsl;

import com.sparwk.adminnode.admin.biz.v1.account.dto.AssoiateMemberViewListDto;
import com.sparwk.adminnode.admin.jpa.entity.PeriodTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.account.MaintenanceAssociateMemberCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.account.MaintenanceAssociateMemberSorterEnum;
import org.springframework.data.domain.PageRequest;

import java.util.List;

public interface AccountAssoiateViewCustomRepository {
    //인증하기 전 User List
    List<AssoiateMemberViewListDto> findQryAssociateMember(MaintenanceAssociateMemberCateEnum cate,
                                                           String val,
                                                           PeriodTypeEnum periodType,
                                                           String sdate,
                                                           String edate,
                                                           MaintenanceAssociateMemberSorterEnum sorter,
                                                           PageRequest pageRequest);

    //인증하기 전 User List
    long countQryAssociateMember(MaintenanceAssociateMemberCateEnum cate,
                                                           String val,
                                                           PeriodTypeEnum periodType,
                                                           String sdate,
                                                           String edate);

}
