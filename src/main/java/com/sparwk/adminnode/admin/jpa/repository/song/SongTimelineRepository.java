package com.sparwk.adminnode.admin.jpa.repository.song;

import com.sparwk.adminnode.admin.jpa.entity.song.SongTimelineEntity;
import com.sparwk.adminnode.admin.jpa.repository.song.dsl.SongTimelineCustomRepository;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SongTimelineRepository extends JpaRepository<SongTimelineEntity, Long>, SongTimelineCustomRepository {

}
