package com.sparwk.adminnode.admin.jpa.entity.admin;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name="tb_admin_login_log")
public class AdminLoginLogEntity {

    @Id
    @GeneratedValue(
            generator = "tb_admin_login_log_seq"
    )
    @Column(name = "admin_login_log_seq")
    private Long adminLoginLogSeq;

    @Column(name = "connect_time")
    private LocalDateTime connectTime;

    @Column(name = "connect_result", length = 30)
    private String connectResult;

    @Column(name = "admin_id")
    private Long adminId;

    @Column(name = "connect_browser", length = 30)
    private String connectBrowser;

    @Column(name = "connect_os", length = 30)
    private String connectOs;

    @Column(name = "connect_device", length = 30)
    private String connectDevice;

    @Column(name = "connect_ip", length = 30)
    private String connectIp;




    @Builder
    public AdminLoginLogEntity(
            Long adminLoginLogSeq,
            LocalDateTime connectTime,
            String connectResult,
            Long adminId,
            String connectBrowser,
            String connectOs,
            String connectDevice,
            String connectIp
    ) {
        this.adminLoginLogSeq = adminLoginLogSeq;
        this.connectTime = connectTime;
        this.connectResult = connectResult;
        this.adminId = adminId;
        this.connectBrowser = connectBrowser;
        this.connectOs = connectOs;
        this.connectDevice = connectDevice;
        this.connectIp = connectIp;
    }

}
