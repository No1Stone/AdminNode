package com.sparwk.adminnode.admin.jpa.entity.account;

import lombok.Getter;

@Getter
public enum VerificationCompanyAccountSorterEnum {

    EmailAsc("EmailAsc"),
    EmailDesc("EmailDesc"),
    CompanyTypeAsc("CompanyTypeAsc"),
    CompanyTypeDesc("CompanyTypeDesc"),
    CompanyNameAsc("CompanyNameAsc"),
    CompanyNameDesc("CompanyNameDesc"),
    BusinessLocationAsc("BusinessLocationAsc"),
    BusinessLocationDesc("BusinessLocationDesc"),
    CreatedAsc("CreatedAsc"),
    CreatedDesc("CreatedDesc");

    private String sorter;

    VerificationCompanyAccountSorterEnum(String sorter) {
        this.sorter = sorter;
    }
}
