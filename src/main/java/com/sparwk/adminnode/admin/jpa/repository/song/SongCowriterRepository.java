package com.sparwk.adminnode.admin.jpa.repository.song;

import com.sparwk.adminnode.admin.jpa.entity.song.SongCowriterEntity;
import com.sparwk.adminnode.admin.jpa.repository.song.dsl.SongCustomRepository;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface SongCowriterRepository extends JpaRepository<SongCowriterEntity, Long>, SongCustomRepository {

    Optional<SongCowriterEntity> findBySongIdAndProfileId(Long songId, Long ProfileId);
}
