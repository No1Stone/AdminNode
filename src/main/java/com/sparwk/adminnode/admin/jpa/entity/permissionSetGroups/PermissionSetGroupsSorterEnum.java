package com.sparwk.adminnode.admin.jpa.entity.permissionSetGroups;

import lombok.Getter;

@Getter
public enum PermissionSetGroupsSorterEnum {

    NameAsc("NameAsc"), NameDesc("NameDesc"),
    UseYnAsc("UseYnAsc"), UseYnDesc("UseYnDesc"),
    LastModifiedAsc("LastModifiedAsc"), LastModifiedDesc("LastModifiedDesc"),
    CreatedAsc("CreatedAsc"), CreatedDesc("CreatedDesc"),
    DcodeAsc("DcodeAsc"), DcodeDesc("DcodeDesc");

    private String sorter;

    PermissionSetGroupsSorterEnum(String sorter) {
        this.sorter = sorter;
    }

}
