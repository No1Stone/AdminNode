package com.sparwk.adminnode.admin.jpa.repository.currency.dsl;

import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.sparwk.adminnode.admin.biz.v1.currency.dto.CurrencyDto;
import com.sparwk.adminnode.admin.biz.v1.currency.dto.CurrencyExcelDto;
import com.sparwk.adminnode.admin.jpa.entity.PeriodTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.admin.QAdminEntity;
import com.sparwk.adminnode.admin.jpa.entity.commonCode.CommonSorterEnum2;
import com.sparwk.adminnode.admin.jpa.entity.currency.CurrencyCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.currency.CurrencySorterEnum;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static com.sparwk.adminnode.admin.jpa.entity.admin.QAdminEntity.adminEntity;
import static com.sparwk.adminnode.admin.jpa.entity.commonCode.QCommonOrganizationCodeEntity.commonOrganizationCodeEntity;
import static com.sparwk.adminnode.admin.jpa.entity.currency.QCurrencyEntity.currencyEntity;

@Repository
public class CurrencyCustomRepositoryImpl implements CurrencyCustomRepository {

    private final JPAQueryFactory jpaQueryFactory;

    public CurrencyCustomRepositoryImpl(JPAQueryFactory jpaQueryFactory) {
        this.jpaQueryFactory = jpaQueryFactory;
    }

    @Override
    public List<CurrencyDto> findQryAll(CurrencyCateEnum cate,
                                        String val,
                                        YnTypeEnum useType,
                                        PeriodTypeEnum periodType,
                                        String sdate,
                                        String edate,
                                        CurrencySorterEnum sorter,
                                        PageRequest pageRequest) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(Projections.bean(CurrencyDto.class,
                                currencyEntity.currencySeq,
                                currencyEntity.currency,
                                currencyEntity.currencyCd,
                                currencyEntity.isoCode,
                                currencyEntity.symbol,
                                currencyEntity.country,
                                currencyEntity.useType,
                                currencyEntity.regUsr,
                                adminEntity.fullName.as("regUsrName"),
                                currencyEntity.regDt,
                                currencyEntity.modUsr,
                                adminEntity2.fullName.as("modUsrName"),
                                currencyEntity.modDt
                        )
                )
                .from(currencyEntity)
                .where(valLike(cate, val), useTypeEq(useType), termBetween(periodType, sdate, edate))
                .limit(pageRequest.getPageSize())
                .offset(pageRequest.getPageSize() * (pageRequest.getPageNumber() - 1))
                .orderBy(sorterOrder(sorter))
                .fetch();
    }

    @Override
    public long countQryAll(CurrencyCateEnum cate,
                                        String val,
                                        YnTypeEnum useType,
                                        PeriodTypeEnum periodType,
                                        String sdate,
                                        String edate) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(
                                currencyEntity.currencySeq.count()
                )
                .from(currencyEntity)
                .where(valLike(cate, val), useTypeEq(useType), termBetween(periodType, sdate, edate))
                .fetchOne();
    }

    @Override
    public List<CurrencyDto> findQryUseY(CurrencyCateEnum cate, String val) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(Projections.bean(CurrencyDto.class,
                                currencyEntity.currencySeq,
                                currencyEntity.currency,
                                currencyEntity.currencyCd,
                                currencyEntity.isoCode,
                                currencyEntity.symbol,
                                currencyEntity.country,
                                currencyEntity.useType,
                                currencyEntity.regUsr,
                                adminEntity.fullName.as("regUsrName"),
                                currencyEntity.regDt,
                                currencyEntity.modUsr,
                                adminEntity2.fullName.as("modUsrName"),
                                currencyEntity.modDt
                        )
                )
                .from(currencyEntity)
                .where(valLike(cate, val), useTypeEq(YnTypeEnum.Y))
                .orderBy(sorterOrder(CurrencySorterEnum.CurrencyAsc))
                .fetch();
    }

    @Override
    public long countQryUseY(CurrencyCateEnum cate, String val) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(
                                currencyEntity.currencySeq.count()
                )
                .from(currencyEntity)
                .where(valLike(cate, val), useTypeEq(YnTypeEnum.Y))
                .fetchOne();
    }

    @Override
    public List<CurrencyExcelDto> findExcelList() {

        return jpaQueryFactory.select(Projections.bean(CurrencyExcelDto.class,
                                currencyEntity.currencySeq,
                                currencyEntity.currency,
                                currencyEntity.currencyCd,
                                currencyEntity.isoCode,
                                currencyEntity.symbol,
                                currencyEntity.country,
                                currencyEntity.useType
                        )
                )
                .from(currencyEntity)
                .orderBy(sorterOrder(CurrencySorterEnum.CurrencyAsc))
                .fetch();
    }

    private BooleanExpression valLike(CurrencyCateEnum cate, String val) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        if (cate !=null && val != null && val.length() > 0) {
            switch (cate) {
                case All:
                    return currencyEntity.currency.toLowerCase().contains(val.toLowerCase())
                            .or(currencyEntity.country.toLowerCase().contains(val.toLowerCase()))
                            .or(currencyEntity.isoCode.toLowerCase().contains(val.toLowerCase()));
                case Currency:
                    return currencyEntity.currency.toLowerCase().contains(val.toLowerCase());
                case Country:
                    return currencyEntity.country.toLowerCase().contains(val.toLowerCase());
                case ISO_4217:
                    return currencyEntity.isoCode.toLowerCase().contains(val.toLowerCase());
                case RegUsrName:
                    return adminEntity.fullName.toLowerCase().contains(val.toLowerCase());
                case ModUsrName:
                    return adminEntity2.fullName.toLowerCase().contains(val.toLowerCase());
            }
        }
        return null;
    }

    private BooleanExpression idEq(Long id) {
        if (id != null)
            return currencyEntity.currencySeq.eq(id);
        return null;
    }

    private BooleanExpression useTypeEq(YnTypeEnum useType) {
        if (useType != null)
            return currencyEntity.useType.eq(useType);
        return null;
    }

    private BooleanExpression termBetween(PeriodTypeEnum periodType, String  sdate, String edate) {

        if (sdate != null && edate != null && periodType != null) {

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
            LocalDateTime searchStartDate = LocalDateTime.parse(sdate + " 00:00:00.000", formatter);
            LocalDateTime searchEndDate = LocalDateTime.parse(edate + " 23:59:59.999", formatter);

            switch (periodType) {
                case Created:
                    return currencyEntity.regDt.between(searchStartDate, searchEndDate);
                case LastModified:
                    return currencyEntity.modDt.between(searchStartDate, searchEndDate);
                default:
                    return null;
            }
        }
        return null;
    }

    private OrderSpecifier sorterOrder(CurrencySorterEnum sorter) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        if (sorter == null)
            return currencyEntity.regDt.desc();

        if (sorter == CurrencySorterEnum.CurrencyAsc)
            return currencyEntity.currency.asc();

        if (sorter == CurrencySorterEnum.CurrencyDesc)
            return currencyEntity.currency.desc();

        if (sorter == CurrencySorterEnum.UseYnAsc)
            return currencyEntity.useType.asc();

        if (sorter == CurrencySorterEnum.UseYnDesc)
            return currencyEntity.useType.desc();

        if (sorter == CurrencySorterEnum.LastModifiedAsc)
            return currencyEntity.modDt.asc();

        if (sorter == CurrencySorterEnum.LastModifiedDesc)
            return currencyEntity.modDt.desc();

        if (sorter == CurrencySorterEnum.CreatedAsc)
            return currencyEntity.regDt.asc();

        if (sorter == CurrencySorterEnum.CreatedDesc)
            return currencyEntity.regDt.desc();

        if (sorter == CurrencySorterEnum.IsoAsc)
            return currencyEntity.isoCode.asc();

        if (sorter == CurrencySorterEnum.IsoDesc)
            return currencyEntity.isoCode.desc();

        if (sorter == CurrencySorterEnum.CountryAsc)
            return currencyEntity.country.asc();

        if (sorter == CurrencySorterEnum.CountryDesc)
            return currencyEntity.country.desc();

        if (sorter == CurrencySorterEnum.SymbolAsc)
            return currencyEntity.currency.asc();

        if (sorter == CurrencySorterEnum.SymbolDesc)
            return currencyEntity.currency.desc();

        if (sorter == CurrencySorterEnum.CreaterNameAsc)
            return adminEntity.fullName.asc();

        if (sorter == CurrencySorterEnum.CreaterNameDesc)
            return adminEntity.fullName.desc();

        if (sorter == CurrencySorterEnum.ModifierNameAsc)
            return adminEntity2.fullName.asc();

        if (sorter == CurrencySorterEnum.ModifierNameDesc)
            return adminEntity2.fullName.desc();

        return currencyEntity.regDt.desc();
    }

}
