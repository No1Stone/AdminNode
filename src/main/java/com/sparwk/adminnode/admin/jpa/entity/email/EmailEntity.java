package com.sparwk.adminnode.admin.jpa.entity.email;

import com.sparwk.adminnode.admin.jpa.entity.BaseEntity;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name="tb_email")
public class EmailEntity extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "email_seq")
    private Long emailSeq;

    @Column(name = "subject", nullable = false, length = 200)
    private String subject;

    @Column(name = "from_email", nullable = false, length = 100)
    private String fromEmail;

    @Column(name = "to_email", nullable = false, length = 100)
    private String toEmail;

    @Column(name = "max_attach_cnt")
    private int maxAttachCnt;

    @Column(name = "member_cnt")
    private int memberCnt;

    @Column(name = "content", nullable = false)
    private String content;

    @CreatedDate
    @Column(name="last_send_dt")
    private LocalDateTime lastSendDt;

    @Builder
    public EmailEntity(
                            Long emailSeq,
                            String subject,
                            String fromEmail,
                            String toEmail,
                            int maxAttachCnt,
                            int memberCnt,
                            String content,
                            LocalDateTime lastSendDt
                        ) {
        this.emailSeq = emailSeq;
        this.subject = subject;
        this.fromEmail = fromEmail;
        this.toEmail = toEmail;
        this.maxAttachCnt = maxAttachCnt;
        this.memberCnt = memberCnt;
        this.content = content;
        this.lastSendDt = lastSendDt;
    }


    public void lastSend(LocalDateTime lastSendDt) {
        this.lastSendDt = lastSendDt;
    }

    public void update(
            String subject,
            String fromEmail,
            int maxAttachCnt,
            int memberCnt,
            String content,
            LocalDateTime lastSendDt
                        ) {
        this.subject = subject;
        this.fromEmail = fromEmail;
        this.maxAttachCnt = maxAttachCnt;
        this.memberCnt = memberCnt;
        this.content = content;
        this.lastSendDt = lastSendDt;
    }
}
