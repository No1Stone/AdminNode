package com.sparwk.adminnode.admin.jpa.entity.profile;


import com.sparwk.adminnode.admin.jpa.entity.BaseEntity;
import com.sparwk.adminnode.admin.jpa.entity.profile.id.ProfileCompanyPartnerId;
import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_profile_company_partner")
@DynamicUpdate
@IdClass(ProfileCompanyPartnerId.class)
public class ProfileCompanyPartnerEntity extends BaseEntity {

    @Id
    @Column(name = "profile_id")
    private Long profileId;
    @Column(name = "partner_id")
    private Long partnerId;
    @Column(name = "relation_type", nullable = true, length = 9)
    private String relationType;
    @Column(name = "relation_status", nullable = true, length = 9)
    private String relationStatus;


    @Builder
    ProfileCompanyPartnerEntity(
            Long profileId,
            Long partnerId,
            String relationType,
            String relationStatus
    ) {
        this.profileId = profileId;
        this.partnerId = partnerId;
        this.relationType = relationType;
        this.relationStatus = relationStatus;
    }

}
