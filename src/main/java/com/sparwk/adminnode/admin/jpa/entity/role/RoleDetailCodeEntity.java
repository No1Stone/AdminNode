package com.sparwk.adminnode.admin.jpa.entity.role;

import com.sparwk.adminnode.admin.jpa.entity.BaseEntity;
import com.sparwk.adminnode.admin.jpa.entity.FormatTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name="tb_admin_role_detail_code")
public class RoleDetailCodeEntity extends BaseEntity implements Serializable {

    @Id
    @GeneratedValue(
            generator = "tb_admin_role_detail_code_seq"
    )
    @Column(name = "role_detail_code_seq")
    private Long roleDetailCodeSeq;

    @Column(name = "pcode", length = 3)
    private String pcode;

    @Column(name = "dcode", length = 9, unique = true)
    private String dcode;

    @Enumerated(EnumType.STRING)
    @Column(name = "format", length = 10)
    private FormatTypeEnum formatType;

    @Column(name = "role", length = 50)
    private String role;

    @Column(name = "abbeviation", length = 10)
    private String abbeviation;

    @Column(name = "custom_code", length = 10)
    private String customCode;

    @Column(name = "description", length = 2000)
    private String description;

    @Column(name = "hit")
    private Long hit;

    @Enumerated(EnumType.STRING)
    @Column(name = "use_yn", length = 1)
    private YnTypeEnum useType;

    @Enumerated(EnumType.STRING)
    @Column(name = "popular_yn", length = 1)
    private YnTypeEnum popularType;

    @Enumerated(EnumType.STRING)
    @Column(name = "matching_role_yn", length = 1)
    private YnTypeEnum matchingRoleYn;

    @Enumerated(EnumType.STRING)
    @Column(name = "credit_role_yn", length = 1)
    private YnTypeEnum creditRoleYn;

    @Enumerated(EnumType.STRING)
    @Column(name = "split_sheet_role_yn", length = 1)
    private YnTypeEnum splitSheetRoleYn;


    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "pcode", referencedColumnName = "code",insertable = false,updatable = false)
    private RoleCodeEntity roleCodeEntity;

    @Builder
    public RoleDetailCodeEntity(
                                Long roleDetailCodeSeq,
                                String pcode,
                                String dcode,
                                FormatTypeEnum formatType,
                                String role,
                                String abbeviation,
                                String customCode,
                                String description,
                                YnTypeEnum useType,
                                YnTypeEnum popularType,
                                YnTypeEnum matchingRoleYn,
                                YnTypeEnum creditRoleYn,
                                YnTypeEnum splitSheetRoleYn,
                                Long hit,
                                RoleCodeEntity roleCodeEntity
                          ) {
        this.roleDetailCodeSeq = roleDetailCodeSeq;
        this.pcode = pcode;
        this.dcode = dcode;
        this.formatType = formatType;
        this.role = role;
        this.abbeviation = abbeviation;
        this.customCode = customCode;
        this.description = description;
        this.useType = useType;
        this.popularType = popularType;
        this.matchingRoleYn = matchingRoleYn;
        this.creditRoleYn = creditRoleYn;
        this.splitSheetRoleYn = splitSheetRoleYn;
        this.hit = hit;
        this.roleCodeEntity = roleCodeEntity;
    }

    public void setRoleCode(RoleCodeEntity roleCodeEntity){
        if(this.roleCodeEntity != null) {
            this.roleCodeEntity.getRoleDetailCodeEntity().remove(this);
        }
        this.roleCodeEntity = roleCodeEntity;
        roleCodeEntity.getRoleDetailCodeEntity().add(this);
    }

    public void updateYn(YnTypeEnum useType) {
        this.useType = useType;
    }
    public void updatePopularYn(YnTypeEnum popularType) {
        this.popularType = popularType;
    }
    public void updateMatchingRoleYn(YnTypeEnum matchingRoleYn) {
        this.matchingRoleYn = matchingRoleYn;
    }
    public void updateCreditRoleYn(YnTypeEnum creditRoleYn) {
        this.creditRoleYn = creditRoleYn;
    }
    public void updateSplitSheetRoleYn(YnTypeEnum splitSheetRoleYn) {
        this.splitSheetRoleYn = splitSheetRoleYn;
    }


    public void update(
                                String pcode,
                                String dcode,
                                FormatTypeEnum formatType,
                                String role,
                                String abbeviation,
                                String customCode,
                                String description,
                                YnTypeEnum useType,
                                YnTypeEnum popularType,
                                YnTypeEnum matchingRoleYn,
                                YnTypeEnum creditRoleYn,
                                YnTypeEnum splitSheetRoleYn
                            ) {
        this.pcode = pcode;
        this.dcode = dcode;
        this.formatType = formatType;
        this.role = role;
        this.abbeviation = abbeviation;
        this.customCode = customCode;
        this.description = description;
        this.useType = useType;
        this.popularType = popularType;
        this.matchingRoleYn = matchingRoleYn;
        this.creditRoleYn = creditRoleYn;
        this.splitSheetRoleYn = splitSheetRoleYn;
    }

}
