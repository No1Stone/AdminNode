package com.sparwk.adminnode.admin.jpa.repository.permissionSetGroups.dsl;

import com.sparwk.adminnode.admin.biz.v1.permissionSetGroups.dto.PermissionSetGroupsDto;
import com.sparwk.adminnode.admin.jpa.entity.PeriodTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.permissionSetGroups.PermissionSetGroupsCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.permissionSetGroups.PermissionSetGroupsSorterEnum;
import org.springframework.data.domain.PageRequest;

import java.util.List;

public interface PermissionSetGroupsCustomRepository {
    List<PermissionSetGroupsDto> findQryAll(PermissionSetGroupsCateEnum cate,
                                            String cdLabelVal,
                                            YnTypeEnum useType,
                                            PeriodTypeEnum periodType,
                                            String sdate,
                                            String edate,
                                            PermissionSetGroupsSorterEnum sorter,
                                            PageRequest pageRequest);

    List<PermissionSetGroupsDto> findQryUseY(PermissionSetGroupsCateEnum cate, String cdLabelVal);
}
