package com.sparwk.adminnode.admin.jpa.entity.board;

import lombok.Getter;

@Getter
public enum PolicyCateEnum {

    All("All"), Title("Title"), Category("Category"),
    RegUsrName("RegUsrName"), ModUsrName("ModUsrName");

    private String boardCateEnum;

    PolicyCateEnum(String BoardCateEnum) {
        this.boardCateEnum = boardCateEnum;
    }
}
