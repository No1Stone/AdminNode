package com.sparwk.adminnode.admin.jpa.repository.song.dsl;

import com.sparwk.adminnode.admin.biz.v1.song.dto.SongLyricsLangDto;

import java.util.List;

public interface SongLyricsCustomRepository {

    List<SongLyricsLangDto> findQryLyricsLang(Long id);

}
