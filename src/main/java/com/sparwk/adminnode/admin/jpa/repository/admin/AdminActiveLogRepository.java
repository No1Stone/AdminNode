package com.sparwk.adminnode.admin.jpa.repository.admin;

import com.sparwk.adminnode.admin.jpa.entity.admin.AdminActiveLogEntity;
import com.sparwk.adminnode.admin.jpa.repository.admin.dsl.AdminActiveLogCustomRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface AdminActiveLogRepository extends JpaRepository<AdminActiveLogEntity, Long>, AdminActiveLogCustomRepository {

    // 일반 SQL쿼리
    @Query(value = "SELECT\n" +
            "          CASE\n" +
            "                WHEN (( SELECT count(tp.profile_id) AS count FROM tb_profile tp WHERE tp.profile_id = :usr)) > 0 \n" +
            "                    THEN ( SELECT tp.full_name FROM tb_profile tp WHERE tp.profile_id = :usr)\n" +
            "                WHEN (( SELECT count(tpc.profile_id) AS count FROM tb_profile_company tpc WHERE tpc.profile_id = :usr)) > 0 \n" +
            "                    THEN ( SELECT tpc.profile_company_name FROM tb_profile_company tpc WHERE tpc.profile_id = :usr)\n" +
            "                WHEN (( SELECT count(ta.admin_id) AS count FROM tb_admin ta WHERE ta.admin_id = :usr)) > 0 \n" +
            "                    THEN ( SELECT ta.full_name FROM tb_admin ta WHERE ta.admin_id = :usr)\n" +
            "                ELSE ''" +
            "        END AS reg_usr_name"
            , nativeQuery = true)
    public String findQryUsrName(Long usr);


}
