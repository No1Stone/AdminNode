package com.sparwk.adminnode.admin.jpa.repository.commonCode;

import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.commonCode.CommonDetailCodeEntity;
import com.sparwk.adminnode.admin.jpa.repository.codeSeq.CodeSeqRepository;
import com.sparwk.adminnode.admin.jpa.repository.commonCode.dsl.CommonDetailCodeCustomRepository;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CommonDetailCodeRepository extends JpaRepository<CommonDetailCodeEntity, Long>, CommonDetailCodeCustomRepository, CodeSeqRepository {
    List<CommonDetailCodeEntity> findByPcodeAndUseType(String pcode, YnTypeEnum useType);
    CommonDetailCodeEntity findByDcode(String code);
    CommonDetailCodeEntity findByVal(String val);
    Long countByVal(String val);

}
