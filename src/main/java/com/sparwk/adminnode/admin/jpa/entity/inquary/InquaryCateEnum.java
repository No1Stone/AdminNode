package com.sparwk.adminnode.admin.jpa.entity.inquary;

import lombok.Getter;

@Getter
public enum InquaryCateEnum {

    All("All"),
    Email("Email"),
    Browser("Browser"),
    OS("OS"),
    Device("Device"),
    IP("IP")

    ;

    private String cate;

    InquaryCateEnum(String cate) {
        this.cate = cate;
    }
}
