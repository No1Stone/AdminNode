package com.sparwk.adminnode.admin.jpa.repository.role;

import com.sparwk.adminnode.admin.jpa.entity.role.RoleDetailCodeEntity;
import com.sparwk.adminnode.admin.jpa.repository.codeSeq.CodeSeqRepository;
import com.sparwk.adminnode.admin.jpa.repository.role.dsl.RoleDetailCodeCustomRepository;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RoleDetailCodeRepository extends JpaRepository<RoleDetailCodeEntity, Long>, RoleDetailCodeCustomRepository, CodeSeqRepository {
    Optional<RoleDetailCodeEntity> findByDcode(String code);
    Long countByRole(String val);
}
