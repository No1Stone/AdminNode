package com.sparwk.adminnode.admin.jpa.repository.permissionSetGroups;

import com.sparwk.adminnode.admin.jpa.entity.permissionSetGroups.PermissionSetGroupsEntity;
import com.sparwk.adminnode.admin.jpa.repository.permissionSetGroups.dsl.PermissionSetGroupsCustomRepository;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PermissionSetGroupsRepository extends JpaRepository<PermissionSetGroupsEntity, Long>, PermissionSetGroupsCustomRepository {

}
