package com.sparwk.adminnode.admin.jpa.repository.admin.dsl;

import com.sparwk.adminnode.admin.biz.v1.admin.dto.AdminDto;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.admin.AdminCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.admin.AdminSorterEnum;
import org.springframework.data.domain.PageRequest;

import java.util.List;

public interface AdminCustomRepository {
    List<AdminDto> findQryAll(AdminCateEnum cate,
                              String val,
                              YnTypeEnum useType,
                              AdminSorterEnum sorter,
                              PageRequest pageRequest);
    long countQryAll(AdminCateEnum cate,
                     String val,
                     YnTypeEnum useType);

}
