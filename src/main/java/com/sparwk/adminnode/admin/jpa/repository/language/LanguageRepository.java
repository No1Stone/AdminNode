package com.sparwk.adminnode.admin.jpa.repository.language;

import com.sparwk.adminnode.admin.biz.v1.language.dto.LanguageDto;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.language.LanguageEntity;
import com.sparwk.adminnode.admin.jpa.repository.codeSeq.CodeSeqRepository;
import com.sparwk.adminnode.admin.jpa.repository.language.dsl.LanguageCustomRepository;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface LanguageRepository extends JpaRepository<LanguageEntity, Long>, LanguageCustomRepository, CodeSeqRepository {
    Long countByLanguage(String val);

    Optional<LanguageEntity> findByLanguageCd(String val);


    List<LanguageEntity> findByUseTypeAndLanguage(YnTypeEnum ynTypeEnum, String val);
    List<LanguageEntity> findByUseType(YnTypeEnum ynTypeEnum);
}
