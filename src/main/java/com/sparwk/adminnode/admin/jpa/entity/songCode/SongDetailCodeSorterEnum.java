package com.sparwk.adminnode.admin.jpa.entity.songCode;

import lombok.Getter;

@Getter
public enum SongDetailCodeSorterEnum {

    NameAsc("NameAsc"), NameDesc("NameDesc"),
    FormatAsc("FormatAsc"), FormatDesc("FormatDesc"),
    DescriptionAsc("DescriptionAsc"), DescriptionDesc("DescriptionDesc"),
    UseYnAsc("UseYnAsc"), UseYnDesc("UseYnDesc"),
    PopularAsc("PopularAsc"), PopularDesc("PopularDesc"),
    LastModifiedAsc("LastModifiedAsc"), LastModifiedDesc("LastModifiedDesc"),
    CreatedAsc("CreatedAsc"), CreatedDesc("CreatedDesc"),
    CreaterNameAsc("CreaterNameAsc"), CreaterNameDesc("CreaterNameDesc"),
    ModifierNameAsc("ModifierNameAsc"), ModifierNameDesc("ModifierNameDesc")
    ;

    private String sorter;

    SongDetailCodeSorterEnum(String sorter) {
        this.sorter = sorter;
    }

}
