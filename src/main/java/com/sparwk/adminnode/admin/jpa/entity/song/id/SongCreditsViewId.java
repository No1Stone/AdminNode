package com.sparwk.adminnode.admin.jpa.entity.song.id;

import lombok.Data;

import java.io.Serializable;

@Data
public class SongCreditsViewId implements Serializable {
    private Long songId;
    private Long profileId;
}
