package com.sparwk.adminnode.admin.jpa.entity.admin;

import lombok.Getter;

@Getter
public enum AdminCateEnum {

    All("All"),
    Name("Name"),
    Email("Email"),
    PemissionAssign("PemissionAssign"),
    Description("Description"),
    RegUsrName("RegUsrName"),
    ModUsrName("ModUsrName");

    private String cate;

    AdminCateEnum(String cate) {
        this.cate = cate;
    }
}
