package com.sparwk.adminnode.admin.jpa.entity.profile;

import com.sparwk.adminnode.admin.jpa.entity.BaseEntity;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_profile_company")
@DynamicUpdate
@TableGenerator(
        name = "tb_profile_seq_generator",
        table = "tb_profile_sequences",
        pkColumnValue = "tb_profile_seq",
        allocationSize = 1,
        initialValue = 1000000
)
public class ProfileCompanyEntity extends BaseEntity {
    @Id
    @GeneratedValue(
            strategy = GenerationType.TABLE,
            generator = "tb_profile_seq_generator")
    @Column(name = "profile_id")
    private Long profileId;

    @Column(name = "accnt_id")
    private Long accntId;

    @Column(name = "mattermost_id", nullable = true, length = 10)
    private String mattermostId;

    @Column(name = "profile_img_url", nullable = true, length = 200)
    private String profileImgUrl;

    @Column(name = "profile_bgd_img_url", nullable = true, length = 200)
    private String profileBgdImgUrl;

    @Column(name = "headline", nullable = true, length = 100)
    private String headline;

    @Column(name = "bio", nullable = true, length = 100)
    private String bio;

    @Column(name = "com_info_overview", nullable = true, length = 2000)
    private String comInfoOverview;

    @Column(name = "com_info_website", nullable = true, length = 100)
    private String comInfoWebsite;

    @Column(name = "com_info_phone", nullable = true, length = 30)
    private String comInfoPhone;

    @Column(name = "com_info_email", nullable = true, length = 50)
    private String comInfoEmail;

    @Column(name = "com_info_found", nullable = true, length = 10)
    private String comInfoFound;

    @Column(name = "com_info_country_cd", nullable = true, length = 9)
    private String comInfoCountryCd;

    @Column(name = "com_info_address", nullable = true, length = 200)
    private String comInfoAddress;

    @Column(name = "com_info_lat", nullable = true, length = 30)
    private String comInfoLat;

    @Column(name = "com_info_lon", nullable = true, length = 30)
    private String comInfoLon;

    @Column(name = "ipi_number", nullable = true, length = 30)
    private String ipiNumber;

    @Enumerated(EnumType.STRING)
    @Column(name = "ipi_number_varify_yn", nullable = true, length = 1)
    private YnTypeEnum ipiNumberVarifyYn;

    @Column(name = "vat_number", nullable = true, length = 30)
    private String vatNumber;

    @Enumerated(EnumType.STRING)
    @Column(name = "vat_number_varify_yn", nullable = true, length = 1)
    private YnTypeEnum vatNumberVarifyYn;

    @Column(name = "profile_company_name", nullable = true, length = 100)
    private String profileCompanyName;


    @Builder
    ProfileCompanyEntity(
            Long profileId,
            Long accntId,
            String mattermostId,
            String profileImgUrl,
            String profileBgdImgUrl,
            String headline,
            String bio,
            String comInfoOverview,
            String comInfoWebsite,
            String comInfoPhone,
            String comInfoEmail,
            String comInfoFound,
            String comInfoCountryCd,
            String comInfoAddress,
            String comInfoLat,
            String comInfoLon,
            String ipiNumber,
            YnTypeEnum ipiNumberVarifyYn,
            String vatNumber,
            YnTypeEnum vatNumberVarifyYn,
            String profileCompanyName
    ) {
        this.profileId = profileId;
        this.accntId = accntId;
        this.mattermostId = mattermostId;
        this.profileImgUrl = profileImgUrl;
        this.profileBgdImgUrl = profileBgdImgUrl;
        this.headline = headline;
        this.bio = bio;
        this.comInfoOverview = comInfoOverview;
        this.comInfoWebsite = comInfoWebsite;
        this.comInfoPhone = comInfoPhone;
        this.comInfoEmail = comInfoEmail;
        this.comInfoFound = comInfoFound;
        this.comInfoCountryCd = comInfoCountryCd;
        this.comInfoAddress = comInfoAddress;
        this.comInfoLat = comInfoLat;
        this.comInfoLon = comInfoLon;
        this.ipiNumber = ipiNumber;
        this.ipiNumberVarifyYn = ipiNumberVarifyYn;
        this.vatNumber = vatNumber;
        this.vatNumberVarifyYn = vatNumberVarifyYn;
        this.profileCompanyName = profileCompanyName;
    }

    public void updateIpiNumberVarifyYn(
            YnTypeEnum ipiNumberVarifyYn
    ) {
        this.ipiNumberVarifyYn = ipiNumberVarifyYn;

    }

}
