package com.sparwk.adminnode.admin.jpa.repository.song;

import com.sparwk.adminnode.admin.jpa.entity.song.SongSplitsheetViewEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SongSplitsheetViewRepository extends JpaRepository<SongSplitsheetViewEntity, Long> {
    List<SongSplitsheetViewEntity> findBySongId(Long id);
}
