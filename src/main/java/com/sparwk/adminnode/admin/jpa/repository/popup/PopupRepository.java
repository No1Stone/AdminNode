package com.sparwk.adminnode.admin.jpa.repository.popup;

import com.sparwk.adminnode.admin.jpa.entity.popup.PopupEntity;
import com.sparwk.adminnode.admin.jpa.repository.codeSeq.CodeSeqRepository;
import com.sparwk.adminnode.admin.jpa.repository.popup.dsl.PopupCustomRepository;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PopupRepository extends JpaRepository<PopupEntity, Long>, PopupCustomRepository, CodeSeqRepository {
    PopupEntity findByPopupStatusCd(String popupStatusCd);
}
