package com.sparwk.adminnode.admin.jpa.entity.profile;


import com.sparwk.adminnode.admin.jpa.entity.BaseEntity;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_profile_group_contact")
@DynamicUpdate
public class ProfileGroupConcatEntity extends BaseEntity {

    @Id
    @Column(name = "profile_id")
    private Long profileId;
    @Column(name = "profile_contact_img_url", nullable = true, length = 200)
    private String profileContactImgUrl;
    @Column(name = "profile_contact_description", nullable = true)
    private String profileContactDescription;
    @Column(name = "contct_first_name", nullable = false, length = 50)
    private String contctFirstName;
    @Column(name = "contct_midle_name", nullable = true, length = 50)
    private String contctMidleName;
    @Column(name = "contct_last_name", nullable = false, length = 50)
    private String contctLastName;
    @Column(name = "contct_email", nullable = false, length = 100)
    private String contctEmail;
    @Column(name = "country_cd", nullable = true, length = 9)
    private String countryCd;
    @Column(name = "contct_phone_number", nullable = false, length = 20)
    private String contctPhoneNumber;
    @Enumerated(EnumType.STRING)
    @Column(name = "verify_phone_yn", nullable = false, length = 1)
    private YnTypeEnum verifyPhoneYn;


    @Builder
    ProfileGroupConcatEntity(
            Long profileId,
            String profileContactImgUrl,
            String profileContactDescription,
            String contctFirstName,
            String contctMidleName,
            String contctLastName,
            String contctEmail,
            String countryCd,
            String contctPhoneNumber,
            YnTypeEnum verifyPhoneYn
    ) {
        this.profileId = profileId;
        this.profileContactImgUrl = profileContactImgUrl;
        this.profileContactDescription = profileContactDescription;
        this.contctFirstName = contctFirstName;
        this.contctMidleName = contctMidleName;
        this.contctLastName = contctLastName;
        this.contctEmail = contctEmail;
        this.countryCd = countryCd;
        this.contctPhoneNumber = contctPhoneNumber;
        this.verifyPhoneYn = verifyPhoneYn;
    }

}
