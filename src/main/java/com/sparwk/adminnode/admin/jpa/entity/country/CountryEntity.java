package com.sparwk.adminnode.admin.jpa.entity.country;

import com.sparwk.adminnode.admin.jpa.entity.BaseEntity;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.continent.ContinentEntity;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name="tb_admin_country_code")
public class CountryEntity extends BaseEntity {

    @Id
    @GeneratedValue(
            generator = "tb_admin_country_code_seq"
    )
    @Column(name = "country_seq")
    private Long countrySeq;

    @Column(name = "country_cd", length = 9)
    private String countryCd;

    @Column(name = "country", length = 100)
    private String country;

    @Column(name = "continent_code", length = 2)
    private String continentCode;

    @Column(name = "iso2", length = 2)
    private String iso2;

    @Column(name = "iso3", length = 3)
    private String iso3;

    @Column(name = "nmr", length = 5)
    private String nmr;

    @Column(name = "dial", length = 10)
    private String dial;

    @Column(name = "description", length = 1000)
    private String description;

    @Enumerated(EnumType.STRING)
    @Column(name = "use_yn", length = 1)
    private YnTypeEnum useType;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "continent_code", referencedColumnName = "code",insertable = false,updatable = false)
    private ContinentEntity continentEntity;

    @Builder
    public CountryEntity(
                            Long countrySeq,
                            String country,
                            String countryCd,
                            String iso2,
                            String iso3,
                            String continentCode,
                            String nmr,
                            String dial,
                            String description,
                            YnTypeEnum useType,
                            ContinentEntity continentEntity
                          ) {
        this.countrySeq = countrySeq;
        this.country = country;
        this.countryCd = countryCd;
        this.iso2 = iso2;
        this.iso3 = iso3;
        this.continentCode = continentCode;
        this.nmr = nmr;
        this.dial = dial;
        this.description = description;
        this.useType = useType;
        this.continentEntity = continentEntity;
    }

    public void updateYn(YnTypeEnum useType) {
        this.useType = useType;
    }

    public void update(
                            String country,
                            String countryCd,
                            String iso2,
                            String iso3,
                            String continentCode,
                            String nmr,
                            String dial,
                            String description,
                            YnTypeEnum useType,
                            ContinentEntity continentEntity
                        ) {
        this.country = country;
        this.countryCd = countryCd;
        this.iso2 = iso2;
        this.iso3 = iso3;
        this.continentCode = continentCode;
        this.nmr = nmr;
        this.dial = dial;
        this.description = description;
        this.useType = useType;
        this.continentEntity = continentEntity;
    }


}
