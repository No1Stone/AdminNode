package com.sparwk.adminnode.admin.jpa.repository.songCode.dsl;

import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.sparwk.adminnode.admin.biz.v1.songCode.dto.SongDetailCodeDto;
import com.sparwk.adminnode.admin.biz.v1.songCode.dto.SongDetailCodeExcelDto;
import com.sparwk.adminnode.admin.jpa.entity.*;
import com.sparwk.adminnode.admin.jpa.entity.admin.QAdminEntity;
import com.sparwk.adminnode.admin.jpa.entity.commonCode.CommonSorterEnum2;
import com.sparwk.adminnode.admin.jpa.entity.songCode.SongCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.songCode.SongDetailCodeSorterEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static com.sparwk.adminnode.admin.jpa.entity.admin.QAdminEntity.adminEntity;
import static com.sparwk.adminnode.admin.jpa.entity.commonCode.QCommonOrganizationCodeEntity.commonOrganizationCodeEntity;
import static com.sparwk.adminnode.admin.jpa.entity.songCode.QSongCodeEntity.songCodeEntity;
import static com.sparwk.adminnode.admin.jpa.entity.songCode.QSongDetailCodeEntity.songDetailCodeEntity;

@Repository
public class SongDetailCodeCustomRepositoryImpl implements SongDetailCodeCustomRepository {

    private final JPAQueryFactory jpaQueryFactory;

    private final Logger logger = LoggerFactory.getLogger(SongDetailCodeCustomRepositoryImpl.class);

    public SongDetailCodeCustomRepositoryImpl(JPAQueryFactory jpaQueryFactory) {
        this.jpaQueryFactory = jpaQueryFactory;
    }

    @Override
    public List<SongDetailCodeDto> findQryAll(String pcode,
                                              SongCateEnum cate,
                                              String val,
                                              FormatTypeEnum formatType,
                                              YnTypeEnum useType,
                                              PeriodTypeEnum periodType,
                                              String sdate,
                                              String edate,
                                              SongDetailCodeSorterEnum sorter,
                                              PageRequest pageRequest) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(Projections.bean(SongDetailCodeDto.class,
                                songCodeEntity.code.as("pcode"),
                                songCodeEntity.val.as("pcodeVal"),
                                songDetailCodeEntity.songDetailCodeSeq,
                                songDetailCodeEntity.dcode,
                                songDetailCodeEntity.val,
                                songDetailCodeEntity.formatType,
                                songDetailCodeEntity.useType,
                                songDetailCodeEntity.popularType,
                                songDetailCodeEntity.description,
                                songDetailCodeEntity.sortIndex,
                                songDetailCodeEntity.hit,
                                songDetailCodeEntity.regUsr,
                                adminEntity.fullName.as("regUsrName"),
                                songDetailCodeEntity.regDt,
                                songDetailCodeEntity.modUsr,
                                adminEntity2.fullName.as("modUsrName"),
                                songDetailCodeEntity.modDt
                        )
                )
                .from(songDetailCodeEntity)
                .join(songDetailCodeEntity.songCodeEntity, songCodeEntity)
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(songDetailCodeEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(songDetailCodeEntity.modUsr))
                .where(songPcodeEq(pcode), valLike(cate, val), cdFormatEq(formatType), useTypeEq(useType), termBetween(periodType, sdate, edate))
                .limit(pageRequest.getPageSize())
                .offset(pageRequest.getPageSize() * (pageRequest.getPageNumber() - 1))
                .orderBy(sorterOrder(sorter))
                .fetch();
    }

    @Override
    public long countQryAll(String pcode,
                                              SongCateEnum cate,
                                              String val,
                                              FormatTypeEnum formatType,
                                              YnTypeEnum useType,
                                              PeriodTypeEnum periodType,
                                              String sdate,
                                              String edate) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(
                                songCodeEntity.code.count()
                )
                .from(songDetailCodeEntity)
                .join(songDetailCodeEntity.songCodeEntity, songCodeEntity)
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(songDetailCodeEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(songDetailCodeEntity.modUsr))
                .where(songPcodeEq(pcode), valLike(cate, val), cdFormatEq(formatType), useTypeEq(useType), termBetween(periodType, sdate, edate))
                .fetchOne();
    }

    @Override
    public List<SongDetailCodeDto> findQryUseY(String pcode, String val) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(Projections.bean(SongDetailCodeDto.class,
                                songCodeEntity.code.as("pcode"),
                                songCodeEntity.val.as("pcodeVal"),
                                songDetailCodeEntity.songDetailCodeSeq,
                                songDetailCodeEntity.dcode,
                                songDetailCodeEntity.val,
                                songDetailCodeEntity.formatType,
                                songDetailCodeEntity.useType,
                                songDetailCodeEntity.popularType,
                                songDetailCodeEntity.description,
                                songDetailCodeEntity.sortIndex,
                                songDetailCodeEntity.hit,
                                songDetailCodeEntity.regUsr,
                                adminEntity.fullName.as("regUsrName"),
                                songDetailCodeEntity.regDt,
                                songDetailCodeEntity.modUsr,
                                adminEntity2.fullName.as("modUsrName"),
                                songDetailCodeEntity.modDt
                        )
                )
                .from(songDetailCodeEntity)
                .join(songDetailCodeEntity.songCodeEntity, songCodeEntity)
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(songDetailCodeEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(songDetailCodeEntity.modUsr))
                .where(songPcodeEq(pcode), useTypeEq(YnTypeEnum.Y))
                .orderBy(sorterOrder(SongDetailCodeSorterEnum.NameAsc))
                .fetch();
    }

    @Override
    public long countQryUseY(String pcode, String val) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(
                        songCodeEntity.code.count()
                )
                .from(songDetailCodeEntity)
                .join(songDetailCodeEntity.songCodeEntity, songCodeEntity)
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(songDetailCodeEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(songDetailCodeEntity.modUsr))
                .where(songPcodeEq(pcode), useTypeEq(YnTypeEnum.Y))
                .fetchOne();
    }

    @Override
    public List<SongDetailCodeDto> findPopularUseY(String pcode) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(Projections.bean(SongDetailCodeDto.class,
                                songCodeEntity.code.as("pcode"),
                                songCodeEntity.val.as("pcodeVal"),
                                songDetailCodeEntity.songDetailCodeSeq,
                                songDetailCodeEntity.dcode,
                                songDetailCodeEntity.val,
                                songDetailCodeEntity.formatType,
                                songDetailCodeEntity.useType,
                                songDetailCodeEntity.popularType,
                                songDetailCodeEntity.description,
                                songDetailCodeEntity.sortIndex,
                                songDetailCodeEntity.hit,
                                songDetailCodeEntity.regUsr,
                                adminEntity.fullName.as("regUsrName"),
                                songDetailCodeEntity.regDt,
                                songDetailCodeEntity.modUsr,
                                adminEntity2.fullName.as("modUsrName"),
                                songDetailCodeEntity.modDt
                        )
                )
                .from(songDetailCodeEntity)
                .join(songDetailCodeEntity.songCodeEntity, songCodeEntity)
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(songDetailCodeEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(songDetailCodeEntity.modUsr))
                .where(songPcodeEq(pcode), popularTypeEq(YnTypeEnum.Y))
                .orderBy(sorterOrder(SongDetailCodeSorterEnum.NameAsc))
                .fetch();
    }

    @Override
    public long countPopularUseY(String pcode) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(
                        songCodeEntity.code.count()
                )
                .from(songDetailCodeEntity)
                .join(songDetailCodeEntity.songCodeEntity, songCodeEntity)
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(songDetailCodeEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(songDetailCodeEntity.modUsr))
                .where(songPcodeEq(pcode), useTypeEq(YnTypeEnum.Y))
                .fetchOne();
    }

    @Override
    public List<SongDetailCodeExcelDto> findExcelList(String pcode) {
        return jpaQueryFactory.select(Projections.bean(SongDetailCodeExcelDto.class,
                                songCodeEntity.code.as("pcode"),
                                songCodeEntity.val.as("pcodeVal"),
                                songDetailCodeEntity.songDetailCodeSeq,
                                songDetailCodeEntity.dcode,
                                songDetailCodeEntity.val,
                                songDetailCodeEntity.formatType,
                                songDetailCodeEntity.useType,
                                songDetailCodeEntity.popularType,
                                songDetailCodeEntity.description
                        )
                )
                .from(songDetailCodeEntity)
                .join(songDetailCodeEntity.songCodeEntity, songCodeEntity)
                .where(songPcodeEq(pcode))
                //.limit(pageRequest.getPageSize())
                //.offset(pageRequest.getPageSize() * (pageRequest.getPageNumber() - 1))
                .orderBy(sorterOrder(SongDetailCodeSorterEnum.NameAsc))
                .fetch();
    }

    public void updateHit(Long id) {
        if(id > 0) {
            jpaQueryFactory.update(songDetailCodeEntity)
                    .set(songDetailCodeEntity.hit,songDetailCodeEntity.hit.add(1))
                    .where(idEq(id))
                    .execute();
        }
    }

    public void deleteQryId(Long id){

        if(id > 0) {
            jpaQueryFactory.delete(songDetailCodeEntity)
                    .where(idEq(id))
                    .execute();
        }
    }

     private BooleanExpression songPcodeEq(String code) {
            if (code != null && code.length() > 0)
                return songDetailCodeEntity.songCodeEntity.code.eq(code);
            return null;
    }

    private BooleanExpression valLike(SongCateEnum cate, String val) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        if (cate !=null && val != null && val.length() > 0) {
            switch (cate) {
                case All:
                    return songDetailCodeEntity.val.toLowerCase().contains(val.toLowerCase())
                            .or(songDetailCodeEntity.description.toLowerCase().contains(val.toLowerCase()));
                case Value:
                    return songDetailCodeEntity.val.toLowerCase().contains(val.toLowerCase());
                case Description:
                    return songDetailCodeEntity.description.toLowerCase().contains(val.toLowerCase());
                case RegUsrName:
                    return adminEntity.fullName.toLowerCase().contains(val.toLowerCase());
                case ModUsrName:
                    return adminEntity2.fullName.toLowerCase().contains(val.toLowerCase());
            }
        }
        return null;
    }

    private BooleanExpression idEq(Long id) {
        if (id != null)
            return songDetailCodeEntity.songDetailCodeSeq.eq(id);
        return null;
    }

    private BooleanExpression cdFormatEq(FormatTypeEnum formatType) {
        if (formatType != null)
            return songDetailCodeEntity.formatType.eq(formatType);
        return null;
    }

    private BooleanExpression useTypeEq(YnTypeEnum useType) {
        if (useType != null)
            return songDetailCodeEntity.useType.eq(useType);
        return null;
    }

    private BooleanExpression popularTypeEq(YnTypeEnum popularType) {
        if (popularType != null)
            return songDetailCodeEntity.popularType.eq(popularType);
        return null;
    }

    private BooleanExpression termBetween(PeriodTypeEnum periodType, String  sdate, String edate) {

        if (sdate != null && edate != null && periodType != null) {

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
            LocalDateTime searchStartDate = LocalDateTime.parse(sdate + " 00:00:00.000", formatter);
            LocalDateTime searchEndDate = LocalDateTime.parse(edate + " 23:59:59.999", formatter);

            switch (periodType) {
                case Created:
                    return songDetailCodeEntity.regDt.between(searchStartDate, searchEndDate);
                case LastModified:
                    return songDetailCodeEntity.modDt.between(searchStartDate, searchEndDate);
                default:
                    return null;
            }
        }
        return null;
    }


    private OrderSpecifier sorterOrder(SongDetailCodeSorterEnum sorter) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        if (sorter == null)
            return songDetailCodeEntity.regDt.desc();

        if (sorter == SongDetailCodeSorterEnum.NameAsc)
            return songDetailCodeEntity.val.asc();

        if (sorter == SongDetailCodeSorterEnum.NameDesc)
            return songDetailCodeEntity.val.desc();

        if (sorter == SongDetailCodeSorterEnum.FormatAsc)
            return songDetailCodeEntity.formatType.asc();

        if (sorter == SongDetailCodeSorterEnum.FormatDesc)
            return songDetailCodeEntity.formatType.desc();

        if (sorter == SongDetailCodeSorterEnum.UseYnAsc)
            return songDetailCodeEntity.useType.asc();

        if (sorter == SongDetailCodeSorterEnum.UseYnDesc)
            return songDetailCodeEntity.useType.desc();

        if (sorter == SongDetailCodeSorterEnum.PopularAsc)
            return songDetailCodeEntity.popularType.asc();

        if (sorter == SongDetailCodeSorterEnum.PopularDesc)
            return songDetailCodeEntity.popularType.desc();

        if (sorter == SongDetailCodeSorterEnum.LastModifiedAsc)
            return songDetailCodeEntity.modDt.asc();

        if (sorter == SongDetailCodeSorterEnum.LastModifiedDesc)
            return songDetailCodeEntity.modDt.desc();

        if (sorter == SongDetailCodeSorterEnum.CreatedAsc)
            return songDetailCodeEntity.regDt.asc();

        if (sorter == SongDetailCodeSorterEnum.CreatedDesc)
            return songDetailCodeEntity.regDt.desc();

        if (sorter == SongDetailCodeSorterEnum.DescriptionAsc)
            return songDetailCodeEntity.description.asc();

        if (sorter == SongDetailCodeSorterEnum.DescriptionDesc)
            return songDetailCodeEntity.description.desc();

        if (sorter == SongDetailCodeSorterEnum.CreaterNameAsc)
            return adminEntity.fullName.asc();

        if (sorter == SongDetailCodeSorterEnum.CreaterNameDesc)
            return adminEntity.fullName.desc();

        if (sorter == SongDetailCodeSorterEnum.ModifierNameAsc)
            return adminEntity2.fullName.asc();

        if (sorter == SongDetailCodeSorterEnum.ModifierNameDesc)
            return adminEntity2.fullName.desc();

        return songDetailCodeEntity.regDt.desc();
    }

}
