package com.sparwk.adminnode.admin.jpa.entity.country;

import lombok.Getter;

@Getter
public enum CountrySorterEnum {

    CountryAsc("CountryAsc"), CountryDesc("CountryDesc"),
    ContinentAsc("ContinentAsc"), ContinentDesc("ContinentDesc"),
    Iso2Asc("Iso2Asc"), Iso2Desc("Iso2Desc"),
    Iso3Asc("Iso3Asc"), Iso3Desc("Iso3Desc"),
    DialAsc("DialAsc"), DialDesc("DialDesc"),
    DescriptionAsc("DescriptionAsc"), DescriptionDesc("DescriptionDesc"),
    UseYnAsc("UseYnAsc"), UseYnDesc("UseYnDesc"),
    LastModifiedAsc("LastModifiedAsc"), LastModifiedDesc("LastModifiedDesc"),
    CreatedAsc("CreatedAsc"), CreatedDesc("CreatedDesc"),
    CreaterNameAsc("CreaterNameAsc"), CreaterNameDesc("CreaterNameDesc"),
    ModifierNameAsc("ModifierNameAsc"), ModifierNameDesc("ModifierNameDesc")
    ;

    private String sorter;

    CountrySorterEnum(String sorter) {
        this.sorter = sorter;
    }

}
