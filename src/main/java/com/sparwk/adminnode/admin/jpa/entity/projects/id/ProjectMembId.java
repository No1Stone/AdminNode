package com.sparwk.adminnode.admin.jpa.entity.projects.id;

import lombok.Data;

import java.io.Serializable;

@Data
public class ProjectMembId implements Serializable {
    private long profileId;
    private long projId;
}
