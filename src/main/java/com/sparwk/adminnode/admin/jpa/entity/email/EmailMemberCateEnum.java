package com.sparwk.adminnode.admin.jpa.entity.email;

import lombok.Getter;

@Getter
public enum EmailMemberCateEnum {

    All("All"),
    Name("Name"),
    Email("Email");

    private String cate;

    EmailMemberCateEnum(String cate) {
        this.cate = cate;
    }
}
