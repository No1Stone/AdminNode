package com.sparwk.adminnode.admin.jpa.entity.song;

import com.sparwk.adminnode.admin.jpa.entity.BaseEntity;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_song_cowriter")
public class SongCowriterEntity extends BaseEntity {
    @Id
    @Column(name = "song_cowriter_seq", nullable = true)
    private Long songCowriterSeq;

    @Column(name = "song_id", nullable = true)
    private long songId;
    @Column(name = "profile_id", nullable = true)
    private long profileId;
    @Column(name = "rate_share", nullable = true)
    private Float rateShare;
    @Column(name = "accept_yn", nullable = true, length = 1)
    private String acceptYn;
    @Column(name = "person_id_type", nullable = true, length = 9)
    private String personIdYype;
    @Column(name = "person_id_number", nullable = true, length = 25)
    private String personIdNumber;
    @Column(name = "op_profile_id", nullable = true)
    private Long opProfileId;
    @Column(name = "pro_profile_id", nullable = true)
    private Long proProfileId;
    @Column(name = "accept_dt", nullable = true)
    private LocalDateTime acceptDt;
    @Column(name = "copyright_control_yn", nullable = true, length = 1)
    private String copyrightControlYn;
    @Column(name = "rate_share_comt", nullable = true, length = 200)
    private String rateShareComt;

    @Builder
    SongCowriterEntity(
            Long songCowriterSeq,
            long songId,
            long profileId,
            Float rateShare,
            String acceptYn,
            String personIdYype,
            String personIdNumber,
            Long opProfileId,
            Long proProfileId,
            LocalDateTime acceptDt,
            String copyrightControlYn,
            String rateShareComt
    ) {
        this.songCowriterSeq = songCowriterSeq;
        this.songId = songId;
        this.profileId = profileId;
        this.rateShare = rateShare;
        this.acceptYn = acceptYn;
        this.personIdYype = personIdYype;
        this.personIdNumber = personIdNumber;
        this.opProfileId = opProfileId;
        this.proProfileId = proProfileId;
        this.acceptDt = acceptDt;
        this.copyrightControlYn = copyrightControlYn;
        this.rateShareComt = rateShareComt;

    }
}
