package com.sparwk.adminnode.admin.jpa.repository.song.dsl;

import com.sparwk.adminnode.admin.biz.v1.song.dto.SongViewListDto;
import com.sparwk.adminnode.admin.jpa.entity.PeriodTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.song.SongCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.song.SongSorterEnum;
import org.springframework.data.domain.PageRequest;

import java.util.List;

public interface SongViewCustomRepository {
    List<SongViewListDto> findQryAll(SongCateEnum cate,
                                     String val,
                                     YnTypeEnum completeVal,
                                     PeriodTypeEnum periodType,
                                     String sdate,
                                     String edate,
                                     SongSorterEnum sorter,
                                     PageRequest pageRequest);

    long countQryAll(SongCateEnum cate,
                                     String val,
                                     YnTypeEnum completeVal,
                                     PeriodTypeEnum periodType,
                                     String sdate,
                                     String edate);

}
