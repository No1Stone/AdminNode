package com.sparwk.adminnode.admin.jpa.repository.continent.dsl;

import com.sparwk.adminnode.admin.biz.v1.continent.dto.ContinentDto;
import com.sparwk.adminnode.admin.biz.v1.continent.dto.ContinentExcelDto;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.continent.ContinentCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.continent.ContinentSorterEnum;
import org.springframework.data.domain.PageRequest;

import java.util.List;

public interface ContinentCustomRepository {

    List<ContinentDto> findQryAll(ContinentCateEnum cate, String val, YnTypeEnum useType, ContinentSorterEnum sorter, PageRequest pageRequest);

    long countQryAll(ContinentCateEnum cate, String val, YnTypeEnum useType);

    List<ContinentDto> findQryUseY();

    long countQryUseY();

    List<ContinentExcelDto> findExcelList();

}
