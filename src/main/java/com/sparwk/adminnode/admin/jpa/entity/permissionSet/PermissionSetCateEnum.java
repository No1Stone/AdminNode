package com.sparwk.adminnode.admin.jpa.entity.permissionSet;

import lombok.Getter;

@Getter
public enum PermissionSetCateEnum {

    All("All"),
    Label("Label"),
    Group("Group"),
    Description("Description"),
    RegUsrName("RegUsrName"),
    ModUsrName("ModUsrName")
    ;

    private String permissionSetCateEnum;

    PermissionSetCateEnum(String permissionSetCateEnum) {
        this.permissionSetCateEnum = permissionSetCateEnum;
    }
}
