package com.sparwk.adminnode.admin.jpa.entity.song;

import com.sparwk.adminnode.admin.jpa.entity.BaseEntity;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.song.id.SongLyricsId;
import com.sparwk.adminnode.admin.jpa.entity.song.id.SongMetaId;
import lombok.*;

import javax.persistence.*;

@Entity
@Getter
@Setter
@IdClass(SongMetaId.class)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_song_metadata")
public class SongMetaEntity extends BaseEntity {
    @Id
    @Column(name = "song_id", nullable = true)
    private long songId;
    @Id
    @Column(name = "attr_type_cd", nullable = true)
    private String attrTypeCd;
    @Id
    @Column(name = "attr_dtl_cd", nullable = true)
    private String attrDtlCd;

    @Builder
    SongMetaEntity(
            long songId,
            String attrTypeCd,
            String attrDtlCd
    ) {
        this.songId = songId;
        this.attrTypeCd = attrTypeCd;
        this.attrDtlCd = attrDtlCd;
    }
}
