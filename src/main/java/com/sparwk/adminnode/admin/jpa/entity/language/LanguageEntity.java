package com.sparwk.adminnode.admin.jpa.entity.language;

import com.sparwk.adminnode.admin.jpa.entity.BaseEntity;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name="tb_admin_language_code")
public class LanguageEntity extends BaseEntity implements Serializable {

    @Id
    @GeneratedValue(
            generator = "tb_admin_language_code_seq"
    )
    @Column(name = "language_seq")
    private Long languageSeq;

    @Column(name = "language", length = 100)
    private String language;

    @Column(name = "language_family", length = 30)
    private String languageFamily;

    @Column(name = "native_language", length = 60)
    private String nativeLanguage;

    @Column(name = "iso639_1", length = 2)
    private String iso639_1;

    @Column(name = "iso639_2t", length = 3)
    private String iso639_2t;

    @Column(name = "iso639_2b", length = 3)
    private String iso639_2b;

    @Column(name = "language_cd", length = 9)
    private String languageCd;

    @Enumerated(EnumType.STRING)
    @Column(name = "use_yn", length = 1)
    private YnTypeEnum useType;

    @Builder
    public LanguageEntity(
                            Long languageSeq,
                            String language,
                            String languageFamily,
                            String nativeLanguage,
                            String iso639_1,
                            String iso639_2t,
                            String iso639_2b,
                            String languageCd,
                            YnTypeEnum useType
                          ) {
        this.languageSeq = languageSeq;
        this.language = language;
        this.languageFamily = languageFamily;
        this.nativeLanguage = nativeLanguage;
        this.iso639_1 = iso639_1;
        this.iso639_2t = iso639_2t;
        this.iso639_2b = iso639_2b;
        this.languageCd = languageCd;
        this.useType = useType;
    }

    public void updateYn(YnTypeEnum useType) {
        this.useType = useType;
    }

    public void update(
                            String language,
                            String languageFamily,
                            String nativeLanguage,
                            String iso639_1,
                            String iso639_2t,
                            String iso639_2b,
                            YnTypeEnum useType
                        ) {
        this.language = language;
        this.languageFamily = languageFamily;
        this.nativeLanguage = nativeLanguage;
        this.iso639_1 = iso639_1;
        this.iso639_2t = iso639_2t;
        this.iso639_2b = iso639_2b;
        this.useType = useType;
    }

}
