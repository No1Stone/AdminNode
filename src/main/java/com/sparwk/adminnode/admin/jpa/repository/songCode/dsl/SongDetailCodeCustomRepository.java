package com.sparwk.adminnode.admin.jpa.repository.songCode.dsl;

import com.sparwk.adminnode.admin.biz.v1.songCode.dto.SongDetailCodeDto;
import com.sparwk.adminnode.admin.biz.v1.songCode.dto.SongDetailCodeExcelDto;
import com.sparwk.adminnode.admin.jpa.entity.FormatTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.PeriodTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.songCode.SongCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.songCode.SongDetailCodeSorterEnum;
import org.springframework.data.domain.PageRequest;

import java.util.List;

public interface SongDetailCodeCustomRepository {
    List<SongDetailCodeDto> findQryAll(String pcode,
                                       SongCateEnum cate,
                                       String val,
                                       FormatTypeEnum cdFormatType,
                                       YnTypeEnum useType,
                                       PeriodTypeEnum periodType,
                                       String sdate,
                                       String edate,
                                       SongDetailCodeSorterEnum sorter,
                                       PageRequest pageRequest
    );
    long countQryAll(String pcode,
                                       SongCateEnum cate,
                                       String val,
                                       FormatTypeEnum cdFormatType,
                                       YnTypeEnum useType,
                                       PeriodTypeEnum periodType,
                                       String sdate,
                                       String edate
    );
    List<SongDetailCodeDto> findQryUseY(String pcode, String val);
    long countQryUseY(String pcode, String val);
    List<SongDetailCodeDto> findPopularUseY(String pcode);
    long countPopularUseY(String pcode);
    List<SongDetailCodeExcelDto> findExcelList(String pcode);

    void updateHit(Long id);

    void deleteQryId(Long id);

}
