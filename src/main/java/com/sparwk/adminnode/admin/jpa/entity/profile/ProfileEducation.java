package com.sparwk.adminnode.admin.jpa.entity.profile;


import com.sparwk.adminnode.admin.jpa.entity.BaseEntity;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.profile.id.ProfileEducationId;
import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_profile_education")
@DynamicUpdate
@IdClass(ProfileEducationId.class)
public class ProfileEducation extends BaseEntity {

    @Id
    @Column(name = "edu_organization_nm", length = 50)
    private String eduOrganizationNm;

    @Id
    @Column(name = "profile_id")
    private Long profileId;
    @Column(name = "edu_course_nm", nullable = true, length = 100)
    private String eduCourseNm;
    @Column(name = "location_city_country_cd", nullable = true, length = 9)
    private String locationCityCountryCd;
    @Column(name = "location_city_nm", nullable = true, length = 50)
    private String locationCityNm;
    @Enumerated(EnumType.STRING)
    @Column(name = "present_yn", nullable = true, length = 1)
    private YnTypeEnum presentYn;
    @Column(name = "edu_seq", nullable = true)
    private Long eduSeq;
    @Column(name = "edu_start_dt", nullable = true)
    private LocalDateTime eduStartDt;
    @Column(name = "edu_end_dt", nullable = true)
    private LocalDateTime eduEndDt;

    @Builder
    ProfileEducation(
            Long profileId,
            String eduOrganizationNm,
            String eduCourseNm,
            String locationCityCountryCd,
            String locationCityNm,
            YnTypeEnum presentYn,
            Long eduSeq,
            LocalDateTime eduStartDt,
            LocalDateTime eduEndDt
    ) {
        this.profileId = profileId;
        this.eduOrganizationNm = eduOrganizationNm;
        this.eduCourseNm = eduCourseNm;
        this.locationCityCountryCd = locationCityCountryCd;
        this.locationCityNm = locationCityNm;
        this.presentYn = presentYn;
        this.eduSeq = eduSeq;
        this.eduStartDt = eduStartDt;
        this.eduEndDt = eduEndDt;
    }

}
