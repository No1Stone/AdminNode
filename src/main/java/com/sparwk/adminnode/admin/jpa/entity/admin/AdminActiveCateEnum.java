package com.sparwk.adminnode.admin.jpa.entity.admin;

import lombok.Getter;

@Getter
public enum AdminActiveCateEnum {

    All("All"),
    Name("Name"),
    AdminId("AdminId"),
    Created("Created"),
    Menu("Menu");

    private String cate;

    AdminActiveCateEnum(String cate) {
        this.cate = cate;
    }
}
