package com.sparwk.adminnode.admin.jpa.entity.permissionSet;

import lombok.Getter;

@Getter
public enum PermissionSetSorterEnum {

    NameAsc("NameAsc"), NameDesc("NameDesc"),
    UseYnAsc("UseYnAsc"), UseYnDesc("UseYnDesc"),
    LastModifiedAsc("LastModifiedAsc"), LastModifiedDesc("LastModifiedDesc"),
    CreatedAsc("CreatedAsc"), CreatedDesc("CreatedDesc"),
    DcodeAsc("DcodeAsc"), DcodeDesc("DcodeDesc"),
    CreaterNameAsc("CreaterNameAsc"), CreaterNameDesc("CreaterNameDesc"),
    ModifierNameAsc("ModifierNameAsc"), ModifierNameDesc("ModifierNameDesc")
    ;

    private String sorter;

    PermissionSetSorterEnum(String sorter) {
        this.sorter = sorter;
    }

}
