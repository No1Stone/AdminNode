package com.sparwk.adminnode.admin.jpa.entity.profile;


import com.sparwk.adminnode.admin.jpa.entity.BaseEntity;
import com.sparwk.adminnode.admin.jpa.entity.profile.id.ProfileSkillId;
import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_profile_skill")
@DynamicUpdate
@IdClass(ProfileSkillId.class)
public class ProfileSkillEntity extends BaseEntity {

    @Id
    @Column(name = "skill_type_cd", length = 9)
    private String skillTypeCd;

    @Id
    @Column(name = "profile_id")
    private Long profileId;

    @Column(name = "skill_detail_type_cd", nullable = true, length = 9)
    private String skillDetailTypeCd;

    @Builder
    ProfileSkillEntity(Long profileId,
                       String skillTypeCd,
                       String skillDetailTypeCd
                 ){
        this.profileId=profileId;
        this.skillTypeCd=skillTypeCd;
        this.skillDetailTypeCd=skillDetailTypeCd;
    }

}
