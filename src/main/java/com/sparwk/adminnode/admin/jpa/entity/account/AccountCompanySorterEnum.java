package com.sparwk.adminnode.admin.jpa.entity.account;

import lombok.Getter;

@Getter
public enum AccountCompanySorterEnum {

    CompanyNameAsc("CompanyNameAsc"), CompanyNameDesc("CompanyNameDesc"),
    CompanyTypeAsc("CompanyTypeAsc"), CompanyTypeDesc("CompanyTypeDesc"),
    CreatedAsc("CreatedAsc"), CreatedDesc("CreatedDesc"),
    LocationAsc("LocationAsc"), LocationDesc("LocationDesc"),
    EmailAsc("EmailAsc"), EmailDesc("EmailDesc");

    private String sorter;

    AccountCompanySorterEnum(String sorter) {
        this.sorter = sorter;
    }

}
