package com.sparwk.adminnode.admin.jpa.repository.board.dsl;

import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.sparwk.adminnode.admin.biz.v1.board.dto.BoardPolicyDto;
import com.sparwk.adminnode.admin.jpa.entity.PeriodTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.admin.QAdminEntity;
import com.sparwk.adminnode.admin.jpa.entity.board.PolicyCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.board.PolicySorterEnum;
import com.sparwk.adminnode.admin.jpa.entity.commonCode.CommonSorterEnum2;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static com.sparwk.adminnode.admin.jpa.entity.admin.QAdminEntity.adminEntity;
import static com.sparwk.adminnode.admin.jpa.entity.board.QBoardPolicyEntity.boardPolicyEntity;
import static com.sparwk.adminnode.admin.jpa.entity.commonCode.QCommonDetailCodeEntity.commonDetailCodeEntity;
import static com.sparwk.adminnode.admin.jpa.entity.commonCode.QCommonOrganizationCodeEntity.commonOrganizationCodeEntity;

@Repository
public class BoardPolicyCustomRepositoryImpl implements BoardPolicyCustomRepository {

    private final JPAQueryFactory jpaQueryFactory;

    public BoardPolicyCustomRepositoryImpl(JPAQueryFactory jpaQueryFactory) {
        this.jpaQueryFactory = jpaQueryFactory;
    }

    @Override
    public List<BoardPolicyDto> findQryAll(String pcode,
                                        PolicyCateEnum cate,
                                        String val,
                                        YnTypeEnum useType,
                                        PeriodTypeEnum periodType,
                                        String sdate,
                                        String edate,
                                        PolicySorterEnum sorter,
                                        PageRequest pageRequest) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(Projections.bean(BoardPolicyDto.class,

                        boardPolicyEntity.policyId,
                        boardPolicyEntity.cateCd,
                        commonDetailCodeEntity.val.as("cateCdName"),
                        boardPolicyEntity.title,
                        boardPolicyEntity.content,
                        boardPolicyEntity.useYn.as("useType"),
                        boardPolicyEntity.hit,
                        boardPolicyEntity.regUsr,
                        adminEntity.fullName.as("regUsrName"),
                        boardPolicyEntity.regDt,
                        boardPolicyEntity.modUsr,
                        adminEntity2.fullName.as("modUsrName"),
                        boardPolicyEntity.modDt
                        )
                )
                .from(boardPolicyEntity)
                .leftJoin(commonDetailCodeEntity).on(boardPolicyEntity.cateCd.eq(commonDetailCodeEntity.dcode))
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(boardPolicyEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(boardPolicyEntity.modUsr))
                .where(boardPcodeEq(pcode), valLike(cate, val), useTypeEq(useType), termBetween(periodType, sdate, edate))
                .limit(pageRequest.getPageSize())
                .offset(pageRequest.getPageSize() * (pageRequest.getPageNumber() - 1))
                .orderBy(sorterOrder(sorter))
                .fetch();
    }

    @Override
    public long countQryAll(String pcode,
                                           PolicyCateEnum cate,
                                           String val,
                                           YnTypeEnum useType,
                                           PeriodTypeEnum periodType,
                                           String sdate,
                                           String edate) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(
                                boardPolicyEntity.policyId.count()
                )
                .from(boardPolicyEntity)
                .leftJoin(commonDetailCodeEntity).on(boardPolicyEntity.cateCd.eq(commonDetailCodeEntity.dcode))
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(boardPolicyEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(boardPolicyEntity.modUsr))
                .where(boardPcodeEq(pcode), valLike(cate, val), useTypeEq(useType), termBetween(periodType, sdate, edate))
                .fetchOne();
    }

    @Override
    public List<BoardPolicyDto> findQryUseY(String pcode,
                                         PolicyCateEnum cate,
                                         String val,
                                          PageRequest pageRequest
                                        ) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(Projections.bean(BoardPolicyDto.class,

                                boardPolicyEntity.policyId,
                                boardPolicyEntity.cateCd,
                                commonDetailCodeEntity.val.as("cateCdName"),
                                boardPolicyEntity.title,
                                boardPolicyEntity.content,
                                boardPolicyEntity.useYn.as("useType"),
                                boardPolicyEntity.hit,
                                boardPolicyEntity.regUsr,
                                adminEntity.fullName.as("regUsrName"),
                                boardPolicyEntity.regDt,
                                boardPolicyEntity.modUsr,
                                adminEntity2.fullName.as("modUsrName"),
                                boardPolicyEntity.modDt
                        )
                )
                .from(boardPolicyEntity)
                .leftJoin(commonDetailCodeEntity).on(boardPolicyEntity.cateCd.eq(commonDetailCodeEntity.dcode))
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(boardPolicyEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(boardPolicyEntity.modUsr))
                .where(boardPcodeEq(pcode), valLike(cate, val), useTypeEq(YnTypeEnum.Y))
                .limit(pageRequest.getPageSize())
                .offset(pageRequest.getPageSize() * (pageRequest.getPageNumber() - 1))
                .orderBy(sorterOrder(PolicySorterEnum.CreatedDesc))
                .fetch();
    }

    @Override
    public long countQryUseY(String pcode,
                                            PolicyCateEnum cate,
                                            String val
    ) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(
                                boardPolicyEntity.policyId.count()
                )
                .from(boardPolicyEntity)
                .leftJoin(commonDetailCodeEntity).on(boardPolicyEntity.cateCd.eq(commonDetailCodeEntity.dcode))
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(boardPolicyEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(boardPolicyEntity.modUsr))
                .where(boardPcodeEq(pcode), valLike(cate, val), useTypeEq(YnTypeEnum.Y))
                .fetchOne();
    }

    @Override
    public long deleteQryList(Long[] id) {
        return jpaQueryFactory.delete(boardPolicyEntity)
                .where(boardPolicyEntity.policyId.in(id))
                .execute();
    }

    private BooleanExpression boardPcodeEq(String code) {
        if (code != null && code.length() > 0)
            return boardPolicyEntity.cateCd.eq(code);
        return null;
    }

    private BooleanExpression valLike(PolicyCateEnum cate, String val) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        if (cate != null && val != null && val.length() > 0) {
            switch (cate) {
                case All:
                    return boardPolicyEntity.title.toLowerCase().contains(val.toLowerCase())
                            .or(boardPolicyEntity.content.toLowerCase().contains(val.toLowerCase()));
                case Title:
                    return boardPolicyEntity.title.toLowerCase().contains(val.toLowerCase());
                case Category:
                    return commonDetailCodeEntity.val.toLowerCase().contains(val.toLowerCase());
                case RegUsrName:
                    return adminEntity.fullName.toLowerCase().contains(val.toLowerCase());
                case ModUsrName:
                    return adminEntity2.fullName.toLowerCase().contains(val.toLowerCase());
            }
        }
        return null;
    }

    private BooleanExpression idEq(Long id) {
        if (id != null)
            return boardPolicyEntity.policyId.eq(id);
        return null;
    }

    private BooleanExpression useTypeEq(YnTypeEnum useType) {
        if (useType != null)
            return boardPolicyEntity.useYn.eq(useType);
        return null;
    }

    private BooleanExpression termBetween(PeriodTypeEnum periodType, String sdate, String edate) {

        if (sdate != null && edate != null && periodType != null) {

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
            LocalDateTime searchStartDate = LocalDateTime.parse(sdate + " 00:00:00.000", formatter);
            LocalDateTime searchEndDate = LocalDateTime.parse(edate + " 23:59:59.999", formatter);

            switch (periodType) {
                case Created:
                    return boardPolicyEntity.regDt.between(searchStartDate, searchEndDate);
                case LastModified:
                    return boardPolicyEntity.modDt.between(searchStartDate, searchEndDate);
                default:
                    return null;
            }
        }
        return null;
    }

    private OrderSpecifier sorterOrder(PolicySorterEnum sorter) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        if (sorter == null)
            return boardPolicyEntity.regDt.desc();

        if (sorter == PolicySorterEnum.TitleAsc)
            return boardPolicyEntity.title.asc();

        if (sorter == PolicySorterEnum.TitleDesc)
            return boardPolicyEntity.title.desc();

        if (sorter == PolicySorterEnum.CreatedAsc)
            return boardPolicyEntity.modDt.asc();

        if (sorter == PolicySorterEnum.CreatedDesc)
            return boardPolicyEntity.modDt.desc();

        if (sorter == PolicySorterEnum.UseAsc)
            return boardPolicyEntity.useYn.asc();

        if (sorter == PolicySorterEnum.UseDesc)
            return boardPolicyEntity.useYn.desc();

        if (sorter == PolicySorterEnum.CategoryAsc)
            return commonDetailCodeEntity.val.asc();

        if (sorter == PolicySorterEnum.CategoryDesc)
            return commonDetailCodeEntity.val.desc();

        if (sorter == PolicySorterEnum.CreaterNameAsc)
            return adminEntity.fullName.asc();

        if (sorter == PolicySorterEnum.CreaterNameDesc)
            return adminEntity.fullName.desc();

        if (sorter == PolicySorterEnum.ModifierNameAsc)
            return adminEntity2.fullName.asc();

        if (sorter == PolicySorterEnum.ModifierNameDesc)
            return adminEntity2.fullName.desc();

        return boardPolicyEntity.regDt.desc();
    }

}
