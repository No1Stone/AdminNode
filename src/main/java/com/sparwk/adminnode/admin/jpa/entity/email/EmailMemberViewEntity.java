package com.sparwk.adminnode.admin.jpa.entity.email;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name="view_admin_email_member")
public class EmailMemberViewEntity {

    @Id
    @Column(name = "profile_id")
    private Long profileId;

    @Column(name = "accnt_email", nullable = false, length = 100)
    private String accntEmail;

    @Column(name = "full_name")
    private String fullName;

    @Column(name = "profile_type", nullable = false)
    private String profileType;

    @Builder
    public EmailMemberViewEntity(
                            Long profileId,
                            String accntEmail,
                            String fullName,
                            String profileType
                        ) {
        this.profileId = profileId;
        this.accntEmail = accntEmail;
        this.fullName = fullName;
        this.profileType = profileType;
    }

}
