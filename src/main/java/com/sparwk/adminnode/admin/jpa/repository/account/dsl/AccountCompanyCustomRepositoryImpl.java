package com.sparwk.adminnode.admin.jpa.repository.account.dsl;

import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.sparwk.adminnode.admin.biz.v1.account.dto.*;
import com.sparwk.adminnode.admin.biz.v1.profile.dto.*;
import com.sparwk.adminnode.admin.jpa.entity.PeriodTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.account.AccountCompanyCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.account.AccountCompanySorterEnum;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import static com.sparwk.adminnode.admin.jpa.entity.account.QAccountCompanyLocationEntity.accountCompanyLocationEntity;
import static com.sparwk.adminnode.admin.jpa.entity.account.QAccountCompanyTypeEntity.accountCompanyTypeEntity;
import static com.sparwk.adminnode.admin.jpa.entity.account.QAccountEntity.accountEntity;
import static com.sparwk.adminnode.admin.jpa.entity.commonCode.QCommonDetailCodeEntity.commonDetailCodeEntity;
import static com.sparwk.adminnode.admin.jpa.entity.country.QCountryEntity.countryEntity;
import static com.sparwk.adminnode.admin.jpa.entity.profile.QProfileCompanyConcatEntity.profileCompanyConcatEntity;
import static com.sparwk.adminnode.admin.jpa.entity.profile.QProfileCompanyEntity.profileCompanyEntity;
import static com.sparwk.adminnode.admin.jpa.entity.profile.QProfileCompanyOnthewebEntity.profileCompanyOnthewebEntity;
import static com.sparwk.adminnode.admin.jpa.entity.profile.QProfileCompanyPartnerEntity.profileCompanyPartnerEntity;
import static com.sparwk.adminnode.admin.jpa.entity.profile.QProfileCompanyRosterEntity.profileCompanyRosterEntity;
import static com.sparwk.adminnode.admin.jpa.entity.profile.QProfileCompanyStudioEntity.profileCompanyStudioEntity;
import static com.sparwk.adminnode.admin.jpa.entity.profile.QProfileEntity.profileEntity;
import static com.sparwk.adminnode.admin.jpa.entity.profile.QProfilePositionEntity.profilePositionEntity;


@Repository
public class AccountCompanyCustomRepositoryImpl implements AccountCompanyCustomRepository {

    private final JPAQueryFactory jpaQueryFactory;

    public AccountCompanyCustomRepositoryImpl(JPAQueryFactory jpaQueryFactory) {
        this.jpaQueryFactory = jpaQueryFactory;
    }

    @Override
    public List<CompanyMemberListDto> findQryCompnayMember(AccountCompanyCateEnum cate,
                                                            String val,
                                                            PeriodTypeEnum periodType,
                                                            String sdate,
                                                            String edate,
                                                            AccountCompanySorterEnum sorter,
                                                            PageRequest pageRequest) {
        return jpaQueryFactory.select(Projections.bean(CompanyMemberListDto.class,
                                profileCompanyEntity.accntId.as("accntId"),
                                profileCompanyEntity.profileId.as("profileId"),
                                profileCompanyEntity.profileCompanyName.as("companyName"),
                                profileCompanyEntity.comInfoEmail.as("companyEmail"),
                                profileCompanyEntity.comInfoCountryCd.as("comInfoCountryCd"),
                                countryEntity.country.as("comInfoCountryCdName"),
                                profileCompanyEntity.ipiNumber,
                                profileCompanyEntity.vatNumber,
                                profileCompanyEntity.regDt
                        )
                )
                .from(profileCompanyEntity)
                .leftJoin(countryEntity).on(profileCompanyEntity.comInfoCountryCd.eq(countryEntity.countryCd))
                .where(valLike(cate, val), termBetween(periodType, sdate, edate))
                .limit(pageRequest.getPageSize())
                .offset(pageRequest.getPageSize() * (pageRequest.getPageNumber() - 1))
                .orderBy(sorterOrder(sorter).stream().toArray(OrderSpecifier[]::new))
                .fetch();
    }

    @Override
    public List<CompanyVerifyListDto> findQryCompnayVerify(AccountCompanyCateEnum cate,
                                                           String val,
                                                           PeriodTypeEnum periodType,
                                                           String sdate,
                                                           String edate,
                                                           AccountCompanySorterEnum sorter,
                                                           PageRequest pageRequest) {
        return jpaQueryFactory.select(Projections.bean(CompanyVerifyListDto.class,
                                profileCompanyEntity.accntId.as("accntId"),
                                profileCompanyEntity.profileId.as("profileId"),
                                profileCompanyEntity.comInfoEmail.as("companyEmail"),
                                profileCompanyEntity.profileCompanyName.as("companyName"),
                                profileCompanyEntity.ipiNumberVarifyYn,
                                profileCompanyEntity.vatNumberVarifyYn,
                                profileCompanyEntity.regDt
                        )
                )
                .from(profileCompanyEntity)
                .leftJoin(countryEntity).on(profileCompanyEntity.comInfoCountryCd.eq(countryEntity.countryCd))
                .where(valLike(cate, val), termBetween(periodType, sdate, edate))
                .limit(pageRequest.getPageSize())
                .offset(pageRequest.getPageSize() * (pageRequest.getPageNumber() - 1))
                .orderBy(sorterOrder(sorter).stream().toArray(OrderSpecifier[]::new))
                .fetch();
    }

    @Override
    public List<AccountCompanyTypeDto> finQryCompanyType(Long accntId) {
        return jpaQueryFactory.select(Projections.bean(AccountCompanyTypeDto.class,
                                accountCompanyTypeEntity.accntId,
                                accountCompanyTypeEntity.companyCd.as("companyTypeCd"),
                                commonDetailCodeEntity.val.as("companyTypeCdName"),
                                accountCompanyTypeEntity.companyLicenseFileUrl,
                                accountCompanyTypeEntity.companyLicenseVerifyYn,
                                accountCompanyTypeEntity.companyLicenseFileName
                        )
                )
                .from(accountCompanyTypeEntity)
                .leftJoin(commonDetailCodeEntity).on(accountCompanyTypeEntity.companyCd.eq(commonDetailCodeEntity.dcode))
                .where(accountCompanyTypeEntity.accntId.eq(accntId))
                .fetch();
    }

    @Override
    public List<AccountCompanyLocationDto> finQryCompanyLocation(Long accntId) {
        return jpaQueryFactory.select(Projections.bean(AccountCompanyLocationDto.class,
                                accountCompanyLocationEntity.accntId,
                                accountCompanyLocationEntity.locationCd,
                                countryEntity.country.as("locationCdName")
                        )
                )
                .from(accountCompanyLocationEntity)
                .leftJoin(countryEntity).on(accountCompanyLocationEntity.locationCd.eq(countryEntity.countryCd))
                .where(accountCompanyLocationEntity.accntId.eq(accntId))
                .fetch();
    }

    @Override
    public List<ProfileCompanySnsDto> findQryCompanySns(Long profileId) {
        return jpaQueryFactory.select(Projections.bean(ProfileCompanySnsDto.class,
                        profileCompanyOnthewebEntity.profileId.as("profileId"),
                        profileCompanyOnthewebEntity.snsTypeCd.as("snsTypeCd"),
                        commonDetailCodeEntity.val.as("snsTypeCdName"),
                        profileCompanyOnthewebEntity.snsUrl
                        )
                )
                .from(profileCompanyOnthewebEntity)
                .leftJoin(commonDetailCodeEntity).on(profileCompanyOnthewebEntity.snsTypeCd.eq(commonDetailCodeEntity.dcode))
                .where(profileCompanyOnthewebEntity.profileId.eq(profileId))
                .orderBy(commonDetailCodeEntity.dcode.asc())
                .fetch();
    }

    @Override
    public List<ProfileCompanyRosteredDto> findQryCompanyRoster(Long companyProfileId) {
        return jpaQueryFactory.select(Projections.bean(ProfileCompanyRosteredDto.class,
                                profileCompanyRosterEntity.profileId.as("companyProfileId"),
                                profileEntity.accntId,
                                profilePositionEntity.profileId,
                                profileEntity.fullName,
                                profilePositionEntity.primaryYn,
                                profilePositionEntity.anrYn,
                                profilePositionEntity.artistYn,
                                profilePositionEntity.creatorYn,
                                accountEntity.accntEmail,
                                profileCompanyRosterEntity.joiningStartDt,
                                profileCompanyRosterEntity.joiningEndDt,
                                profileCompanyRosterEntity.joiningEndYn

                        )
                )
                .from(profileCompanyRosterEntity)
                .leftJoin(profilePositionEntity).on(profilePositionEntity.profilePositionSeq.eq(profileCompanyRosterEntity.profilePositionSeq))
                .leftJoin(profileEntity).on(profileEntity.profileId.eq(profilePositionEntity.profileId))
                .leftJoin(accountEntity).on(accountEntity.accntId.eq(profileEntity.accntId))
                .where(profileCompanyRosterEntity.rosterStatus.eq("RSS000001"), profileCompanyRosterEntity.profileId.eq(companyProfileId))
                .orderBy(profileEntity.fullName.asc(), profileCompanyRosterEntity.joiningStartDt.asc())
                .fetch();
    }

    @Override
    public List<ProfileCompanyPartnerDto> findQryCompanyPartner(Long companyProfileId) {
        return jpaQueryFactory.select(Projections.bean(ProfileCompanyPartnerDto.class,
                        profileCompanyPartnerEntity.profileId,
                        profileCompanyPartnerEntity.partnerId,
                        profileCompanyEntity.accntId.as("partnerAccntId"),
                        profileCompanyEntity.profileCompanyName.as("partnerCompanyName"),
                        profileCompanyEntity.comInfoEmail.as("partnerCompanyEmail"),
                        profileCompanyEntity.comInfoCountryCd.as("partnerCompanyCountryCd"),
                        countryEntity.country.as("partnerCompanyCountryName")
                        )
                )
                .from(profileCompanyPartnerEntity)
                .leftJoin(profileCompanyEntity).on(profileCompanyPartnerEntity.partnerId.eq(profileCompanyEntity.profileId))
                .leftJoin(countryEntity).on(profileCompanyEntity.comInfoCountryCd.eq(countryEntity.countryCd))
                .where(profileCompanyPartnerEntity.profileId.eq(companyProfileId))
                .orderBy(profileCompanyEntity.profileCompanyName.asc())
                .fetch();
    }

    @Override
    public List<ProfileCompanyStudioDto> findQryCompanyStudio(Long companyProfileId) {
        return jpaQueryFactory.select(Projections.bean(ProfileCompanyStudioDto.class,
                        profileCompanyStudioEntity.profileId,
                        profileCompanyStudioEntity.studioId,
                        profileCompanyStudioEntity.studioName,
                        profileCompanyStudioEntity.businessLocationCd,
                        countryEntity.country.as("businessLocationCdName"),
                        profileCompanyStudioEntity.postCd,
                        profileCompanyStudioEntity.region,
                        profileCompanyStudioEntity.city,
                        profileCompanyStudioEntity.addr1,
                        profileCompanyStudioEntity.addr2

                        )
                )
                .from(profileCompanyStudioEntity)
                .leftJoin(countryEntity).on(profileCompanyStudioEntity.businessLocationCd.eq(countryEntity.countryCd))
                .where(profileCompanyStudioEntity.profileId.eq(companyProfileId))
                .orderBy(profileCompanyStudioEntity.studioName.asc())
                .fetch();
    }

    @Override
    public ProfileCompanyContactDto findQryCompanyConcat(Long companyProfileId) {
        return jpaQueryFactory.select(Projections.bean(ProfileCompanyContactDto.class,
                        profileCompanyConcatEntity.profileId,
                        profileCompanyConcatEntity.contctFirstName,
                        profileCompanyConcatEntity.contctMidleName,
                        profileCompanyConcatEntity.contctLastName,
                        profileCompanyConcatEntity.contctEmail,
                        profileCompanyConcatEntity.profileContactImgUrl,
                        profileCompanyConcatEntity.profileContactDescription,
                        profileCompanyConcatEntity.contctPhoneNumber,
                        profileCompanyConcatEntity.verifyPhoneYn,
                        profileCompanyConcatEntity.countryCd,
                        countryEntity.country.as("countryCdName")
                        )
                )
                .from(profileCompanyConcatEntity)
                .leftJoin(countryEntity).on(profileCompanyConcatEntity.countryCd.eq(countryEntity.countryCd))
                .where(profileCompanyConcatEntity.profileId.eq(companyProfileId))
                .fetchOne();
    }

    private BooleanExpression valLike(AccountCompanyCateEnum cate, String val) {
        if (cate != null && val != null && val.length() > 0) {
            switch (cate) {
                case All:
                    return profileCompanyEntity.comInfoEmail.toLowerCase().contains(val.toLowerCase());
                case Email:
                    return profileCompanyEntity.comInfoEmail.toLowerCase().contains(val.toLowerCase());
                case CompanyName:
                    return profileCompanyEntity.profileCompanyName.toLowerCase().contains(val.toLowerCase());
            }
        }
        return null;
    }

    private BooleanExpression termBetween(PeriodTypeEnum periodType, String  sdate, String edate) {

        if (sdate != null && edate != null && periodType != null) {

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
            LocalDateTime searchStartDate = LocalDateTime.parse(sdate + " 00:00:00.000", formatter);
            LocalDateTime searchEndDate = LocalDateTime.parse(edate + " 23:59:59.999", formatter);

            switch (periodType) {
                case Created:
                    return profileCompanyEntity.regDt.between(searchStartDate, searchEndDate);
                case LastModified:
                    return profileCompanyEntity.modDt.between(searchStartDate, searchEndDate);
                default:
                    return null;
            }
        }
        return null;
    }

    private List<OrderSpecifier> sorterOrder(AccountCompanySorterEnum sorter) {

        List<OrderSpecifier> orders = new ArrayList<>();

        if (sorter == null) {
            orders.add(profileCompanyEntity.regDt.desc());
            return orders;
        }

        if (sorter == AccountCompanySorterEnum.CompanyNameAsc) {
            orders.add(profileCompanyEntity.profileCompanyName.asc());
            return orders;
        }

        if (sorter == AccountCompanySorterEnum.CompanyNameDesc) {
            orders.add(profileCompanyEntity.profileCompanyName.desc());
            return orders;
        }

        if (sorter == AccountCompanySorterEnum.CompanyTypeAsc) {
            orders.add(profileCompanyEntity.profileCompanyName.asc());
            return orders;
        }

        if (sorter == AccountCompanySorterEnum.CompanyTypeAsc) {
            orders.add(profileCompanyEntity.profileCompanyName.desc());
            return orders;
        }

        if (sorter == AccountCompanySorterEnum.CreatedAsc) {
            orders.add(profileCompanyEntity.regDt.asc());
            return orders;
        }

        if (sorter == AccountCompanySorterEnum.CreatedDesc) {
            orders.add(profileCompanyEntity.regDt.desc());
            return orders;
        }

        if (sorter == AccountCompanySorterEnum.EmailAsc) {
            orders.add(profileCompanyEntity.comInfoEmail.asc());
            return orders;
        }

        if (sorter == AccountCompanySorterEnum.EmailDesc) {
            orders.add(profileCompanyEntity.comInfoEmail.desc());
            return orders;
        }

        if (sorter == AccountCompanySorterEnum.LocationAsc) {
            orders.add(profileCompanyEntity.comInfoEmail.asc());
            return orders;
        }

        if (sorter == AccountCompanySorterEnum.LocationDesc) {
            orders.add(profileCompanyEntity.comInfoEmail.desc());
            return orders;
        }

        orders.add(profileCompanyEntity.regDt.desc());
        return orders;
    }


    //업데이트 처리
    public void updateQryCompanyTypeVerifyYn(Long accntId, YnTypeEnum yntype){
        jpaQueryFactory.update(accountCompanyTypeEntity)
                .set(accountCompanyTypeEntity.companyLicenseVerifyYn, yntype)
                .where(accountCompanyTypeEntity.accntId.eq(accntId))
                .execute();
    }

}
