package com.sparwk.adminnode.admin.jpa.repository.board.dsl;

import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.sparwk.adminnode.admin.biz.v1.board.dto.BoardQnaDto;
import com.sparwk.adminnode.admin.jpa.entity.PeriodTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.admin.QAdminEntity;
import com.sparwk.adminnode.admin.jpa.entity.board.QnaCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.profile.QProfileEntity;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static com.sparwk.adminnode.admin.jpa.entity.admin.QAdminEntity.adminEntity;
import static com.sparwk.adminnode.admin.jpa.entity.profile.QProfileEntity.profileEntity;
import static com.sparwk.adminnode.admin.jpa.entity.board.QBoardQnaEntity.boardQnaEntity;
import static com.sparwk.adminnode.admin.jpa.entity.commonCode.QCommonDetailCodeEntity.commonDetailCodeEntity;

@Repository
public class BoardQnaCustomRepositoryImpl implements BoardQnaCustomRepository {

    private final JPAQueryFactory jpaQueryFactory;

    public BoardQnaCustomRepositoryImpl(JPAQueryFactory jpaQueryFactory) {
        this.jpaQueryFactory = jpaQueryFactory;
    }

    @Override
    public List<BoardQnaDto> findQryQna(String type,
                                        String val,
                                        QnaCateEnum cate,
                                        PageRequest pageRequest) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(Projections.bean(BoardQnaDto.class,

                                boardQnaEntity.qnaId,
                                boardQnaEntity.cateCd,
                                commonDetailCodeEntity.val.as("cateCdName"),
                                boardQnaEntity.reciveEmail,
                                boardQnaEntity.reciveEmailYn,
                                boardQnaEntity.title,
                                boardQnaEntity.questionContent,
                                boardQnaEntity.questionAttachYn,
                                boardQnaEntity.answerYn,
                                boardQnaEntity.answerContent,
                                boardQnaEntity.answerAttachYn,
                                boardQnaEntity.answerEmailYn,
                                boardQnaEntity.regUsr,
                                boardQnaEntity.regDt,
                                boardQnaEntity.modUsr,
                                boardQnaEntity.modDt,
                                profileEntity.fullName.as("questionUsrName"),
                                profileEntity.profileImgUrl.as("questionProfileImgUrl"),
                                boardQnaEntity.answerRegUsr,
                                boardQnaEntity.answerRegDt,
                                boardQnaEntity.answerModUsr,
                                adminEntity.fullName.as("answerUsrName"),
                                boardQnaEntity.answerModDt
                        )
                )
                .from(boardQnaEntity)
                .leftJoin(commonDetailCodeEntity).on(boardQnaEntity.cateCd.eq(commonDetailCodeEntity.dcode))
                .leftJoin(profileEntity).on(profileEntity.profileId.eq(boardQnaEntity.regUsr))
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(boardQnaEntity.answerModUsr))
                .where(boardPcodeEq(cate), valLike(val))
                .limit(pageRequest.getPageSize())
                .offset(pageRequest.getPageSize() * (pageRequest.getPageNumber() - 1))
                .orderBy(boardQnaEntity.regDt.desc())
                .fetch();
    }

    @Override
    public long countQryAll(String type,
                                        String val,
                            QnaCateEnum cate
                                        ) {
        return jpaQueryFactory.select(
                                boardQnaEntity.qnaId.count()
                )
                .from(boardQnaEntity)
                .leftJoin(commonDetailCodeEntity).on(boardQnaEntity.cateCd.eq(commonDetailCodeEntity.dcode))
                .where(boardPcodeEq(cate), valLike(val))
                .fetchOne();
    }

    private BooleanExpression boardPcodeEq(QnaCateEnum cate) {
        if (cate != null) {
            switch (cate) {
                case All:
                    return null;
                case Unanswered:
                    return boardQnaEntity.answerYn.eq(YnTypeEnum.N);
            }
        }
        return null;
    }

    private BooleanExpression valLike(String val) {
        if (val != null && val.length() > 0)
            return boardQnaEntity.title.toLowerCase().contains(val.toLowerCase())
                    .or(boardQnaEntity.questionContent.toLowerCase().contains(val.toLowerCase()))
                    .or(boardQnaEntity.answerContent.toLowerCase().contains(val.toLowerCase()));
        return null;
    }



    private BooleanExpression idEq(Long id) {
        if (id != null)
            return boardQnaEntity.qnaId.eq(id);
        return null;
    }

    private BooleanExpression answerEq(YnTypeEnum answerYn) {
        if (answerYn != null)
            return boardQnaEntity.answerYn.eq(answerYn);
        return null;
    }

    private BooleanExpression termBetween(PeriodTypeEnum periodType, String sdate, String edate) {

        if (sdate != null && edate != null && periodType != null) {

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
            LocalDateTime searchStartDate = LocalDateTime.parse(sdate + " 00:00:00.000", formatter);
            LocalDateTime searchEndDate = LocalDateTime.parse(edate + " 23:59:59.999", formatter);

            switch (periodType) {
                case Created:
                    return boardQnaEntity.regDt.between(searchStartDate, searchEndDate);
                case LastModified:
                    return boardQnaEntity.modDt.between(searchStartDate, searchEndDate);
                default:
                    return null;
            }
        }
        return null;
    }

}
