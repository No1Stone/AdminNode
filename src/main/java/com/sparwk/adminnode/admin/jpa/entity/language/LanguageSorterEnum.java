package com.sparwk.adminnode.admin.jpa.entity.language;

import lombok.Getter;

@Getter
public enum LanguageSorterEnum {

    NameAsc("NameAsc"), NameDesc("NameDesc"),
    FamilyAsc("FamilyNameAsc"), FamilyDesc("FamilyDesc"),
    NativeAsc("NativeAsc"), NativeDesc("NativeDesc"),
    ISO639_1Asc("ISO639_1Asc"), ISO639_1Desc("ISO639_1Desc"),
    ISO639_2TAsc("ISO6392TAsc"), ISO639_2TDesc("ISO639_2TDesc"),
    ISO639_2BAsc("ISO6392BAsc"), ISO639_2BDesc("ISO639_2BDesc"),
    CreaterNameAsc("CreaterNameAsc"), CreaterNameDesc("CreaterNameDesc"),
    ModifierNameAsc("ModifierNameAsc"), ModifierNameDesc("ModifierNameDesc")
    ;

    private String sorter;

    LanguageSorterEnum(String sorter) {
        this.sorter = sorter;
    }

}
