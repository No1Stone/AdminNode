package com.sparwk.adminnode.admin.jpa.entity;

import lombok.Getter;

@Getter
public enum FormatTypeEnum {

    DDEX("DDEX"), SPARWK("SPARWK");

    private String format;

    FormatTypeEnum(String format) {
        this.format = format;
    }

}
