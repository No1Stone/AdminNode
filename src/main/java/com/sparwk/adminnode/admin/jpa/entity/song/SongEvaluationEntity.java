package com.sparwk.adminnode.admin.jpa.entity.song;

import com.sparwk.adminnode.admin.jpa.entity.BaseEntity;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.song.id.SongFileId;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_evaluation")
public class SongEvaluationEntity extends BaseEntity {
    @Id
    @Column(name = "eval_seq", nullable = true)
    private long evalSeq;
    @Column(name = "pitchlist_id", nullable = true)
    private long pitchlistId;
    @Column(name = "song_id", nullable = true)
    private long songId;
    @Column(name = "eval_owner_id", nullable = true)
    private long evalOwnerId;
    @Column(name = "pitchlist_anr_seq", nullable = true)
    private long pitchlistAnrSeq;
    @Column(name = "eval_status", nullable = true, length = 9)
    private String evalStatus;
    @Column(name = "eval_start_dt", nullable = true)
    private LocalDateTime evalStartDt;
    @Column(name = "eval_end_dt", nullable = true)
    private LocalDateTime evalEndDt;
    @Column(name = "hold_start_dt", nullable = true)
    private LocalDateTime holdStartDt;
    @Column(name = "hold_expired_dt", nullable = true)
    private LocalDateTime holdExpiredDt;
    @Column(name = "eval_templ_cd", nullable = true, length = 9)
    private String evalTemplCd;
    @Column(name = "share_url", nullable = true, length = 200)
    private int shareUrl;
    @Enumerated(EnumType.STRING)
    @Column(name = "del_yn", nullable = true, length = 1)
    private YnTypeEnum delYn;
    @Enumerated(EnumType.STRING)
    @Column(name = "confirm_yn", nullable = true, length = 1)
    private YnTypeEnum confirmYn;


    @Builder
    SongEvaluationEntity(
            long evalSeq,
            long pitchlistId,
            long songId,
            long evalOwnerId,
            long pitchlistAnrSeq,
            String evalStatus,
            LocalDateTime evalStartDt,
            LocalDateTime evalEndDt,
            LocalDateTime holdStartDt,
            LocalDateTime holdExpiredDt,
            String evalTemplCd,
            int shareUrl,
            YnTypeEnum delYn,
            YnTypeEnum confirmYn
    ) {
        this.evalSeq = evalSeq;
        this.pitchlistId = pitchlistId;
        this.songId = songId;
        this.evalOwnerId = evalOwnerId;
        this.pitchlistAnrSeq = pitchlistAnrSeq;
        this.evalStatus = evalStatus;
        this.evalStartDt = evalStartDt;
        this.evalEndDt = evalEndDt;
        this.holdStartDt = holdStartDt;
        this.holdExpiredDt = holdExpiredDt;
        this.evalTemplCd = evalTemplCd;
        this.shareUrl = shareUrl;
        this.delYn = delYn;
        this.confirmYn = confirmYn;

    }
}
