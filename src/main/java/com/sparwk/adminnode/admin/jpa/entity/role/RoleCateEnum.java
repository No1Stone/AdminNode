package com.sparwk.adminnode.admin.jpa.entity.role;

import lombok.Getter;

@Getter
public enum RoleCateEnum {

    All("All"),
    Role("Role"),
    Description("Description"),
    Abbreviation("Abbreviation"),
    Code("Code"),
    RegUsrName("RegUsrName"),
    ModUsrName("ModUsrName")
    ;

    private String roll;

    RoleCateEnum(String roll) {
        this.roll = roll;
    }
}
