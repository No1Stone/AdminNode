package com.sparwk.adminnode.admin.jpa.entity.song;

import lombok.Getter;

@Getter
public enum SongCateEnum {

    All("All"),
    Title("Title"),
    Genre("Genre"),
    SubGenre("SubGenre"),
    ProjectTitle("ProjectTitle"),
    Status("Status"),
    RegUsrName("RegUsrName"),
    ModUsrName("ModUsrName")
    ;

    private String songCateEnum;

    SongCateEnum(String songCateEnum) {
        this.songCateEnum = songCateEnum;
    }
}
