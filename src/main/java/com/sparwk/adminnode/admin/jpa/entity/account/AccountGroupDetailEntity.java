package com.sparwk.adminnode.admin.jpa.entity.account;

import com.sparwk.adminnode.admin.jpa.entity.BaseEntity;
import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_account_company_detail")
@DynamicUpdate
public class AccountGroupDetailEntity extends BaseEntity {

    @Id
    @Column(name = "accnt_id")
    private Long accntId;
    @Column(name = "account_group_name", nullable = false, length = 100)
    private String groupName;
    @Column(name = "post_cd", length = 20)
    private String postCd;
    @Column(name = "location_cd", length = 9)
    private String locationCd;
    @Column(name = "region", nullable = true, length = 50)
    private String region;
    @Column(name = "city", nullable = true, length = 20)
    private String city;
    @Column(name = "addr1", nullable = true, length = 200)
    private String addr1;
    @Column(name = "addr2", nullable = true, length = 200)
    private String addr2;

    @Builder
    AccountGroupDetailEntity(
            Long accntId,
            String groupName,
            String postCd,
            String locationCd,
            String region,
            String city,
            String addr1,
            String addr2
    ) {
        this.accntId=accntId;
        this.groupName=groupName;
        this.locationCd=locationCd;
        this.postCd=postCd;
        this.region=region;
        this.city=city;
        this.addr1=addr1;
        this.addr2=addr2;
    }

//    @OneToOne(mappedBy = "accountCompanyDetail")
//    private com.sparwk.personalizationnode.account.jpa.entity.Account account;
}
