package com.sparwk.adminnode.admin.jpa.entity.commonCode;

import com.sparwk.adminnode.admin.jpa.entity.BaseEntity;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_admin_common_detail_code")
public class CommonDetailCodeEntity extends BaseEntity {

    @Id
    @GeneratedValue(
            generator = "tb_admin_common_detail_code_seq"
    )
    @Column(name = "common_detail_code_seq")
    private Long commonDetailCodeSeq;

    @Column(name = "pcode", length = 3, unique = true)
    private String pcode;

    @Column(name = "dcode", length = 9, unique = true)
    private String dcode;

    @Column(name = "val", length = 30)
    private String val;

    @Column(name = "description", length = 2000)
    private String description;

    @Column(name = "sort_index")
    private int sortIndex;

    @Column(name = "hit")
    private int hit;

    @Enumerated(EnumType.STRING)
    @Column(name = "use_yn", length = 1)
    private YnTypeEnum useType;

    @Enumerated(EnumType.STRING)
    @Column(name = "popular_yn", length = 1)
    private YnTypeEnum popularType;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "pcode", referencedColumnName = "code", insertable = false, updatable = false)
    private CommonCodeEntity commonCodeEntity;

    @Builder
    public CommonDetailCodeEntity(
            Long commonDetailCodeSeq,
            String pcode,
            String dcode,
            String val,
            String description,
            int sortIndex,
            int hit,
            YnTypeEnum useType,
            YnTypeEnum popularType,
            CommonCodeEntity commonCodeEntity
    ) {
        this.commonDetailCodeSeq = commonDetailCodeSeq;
        this.pcode = pcode;
        this.dcode = dcode;
        this.val = val;
        this.description = description;
        this.sortIndex = sortIndex;
        this.hit = hit;
        this.useType = useType;
        this.popularType = popularType;
        this.commonCodeEntity = commonCodeEntity;
    }


    public void updateYn(YnTypeEnum useType) {
        this.useType = useType;
    }

    public void updatePopularYn(YnTypeEnum popularType) {
        this.popularType = popularType;
    }

    public void update(
            String pcode,
            String dcode,
            String val,
            String description,
            int sortIndex,
            YnTypeEnum useType,
            YnTypeEnum popularType
    ) {
        this.pcode = pcode;
        this.dcode = dcode;
        this.val = val;
        this.description = description;
        this.sortIndex = sortIndex;
        this.useType = useType;
        this.popularType = popularType;
    }

    public void cateUpdate(
            String pcode,
            String val,
            YnTypeEnum useType
    ) {
        this.pcode = pcode;
        this.val = val;
        this.useType = useType;

    }

}
