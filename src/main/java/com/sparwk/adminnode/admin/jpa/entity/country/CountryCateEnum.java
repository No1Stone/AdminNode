package com.sparwk.adminnode.admin.jpa.entity.country;

import lombok.Getter;

@Getter
public enum CountryCateEnum {

    All("All"),
    Country("Country"),
    Continent("Continent"),
    ISO_3166_1_Alpha2("ISO_3166_1_Alpha2"),
    ISO_3166_1_Alpha3("ISO_3166_1_Alpha3"),
    DialCode("DialCode"),
    Description("Description"),
    RegUsrName("RegUsrName"),
    ModUsrName("ModUsrName")
    ;

    private String cate;

    CountryCateEnum(String cate) {
        this.cate = cate;
    }
}
