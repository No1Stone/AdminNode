package com.sparwk.adminnode.admin.jpa.repository.song;

import com.sparwk.adminnode.admin.jpa.entity.song.SongLyricsEntity;
import com.sparwk.adminnode.admin.jpa.repository.song.dsl.SongLyricsCustomRepository;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface SongLyricsRepository extends JpaRepository<SongLyricsEntity, Long>, SongLyricsCustomRepository {
    Optional<SongLyricsEntity> findBySongIdAndSongFileSeq(Long id, Long seq);

    Optional<SongLyricsEntity> findBySongId(Long id);}
