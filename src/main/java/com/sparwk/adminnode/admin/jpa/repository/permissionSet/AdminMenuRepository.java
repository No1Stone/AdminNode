package com.sparwk.adminnode.admin.jpa.repository.permissionSet;

import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.permissionSet.AdminMenuEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AdminMenuRepository extends JpaRepository<AdminMenuEntity, Long> {
    List<AdminMenuEntity> findByUseTypeOrderBySortIndex(YnTypeEnum useType);
    List<AdminMenuEntity> findByUseTypeAndMenuDepthOrderBySortIndex(YnTypeEnum useType,int depth);
    List<AdminMenuEntity> findByUseTypeAndMenuDepthAndUppMenuSeqOrderBySortIndex(YnTypeEnum useType,int depth, Long uppMenuSeq);
}
