package com.sparwk.adminnode.admin.jpa.repository.codeSeq;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface CodeSeqRepository {
    @Query("select cc.maxNum from CommonCodeEntity cc where cc.code= :code")
    int getLastSeq(@Param("code") String code);

    @Modifying
    @Query("update CommonCodeEntity cc set cc.maxNum = :val where cc.code= :code")
    void updateLastSeq(@Param("code") String code, @Param("val") Integer val);

}
