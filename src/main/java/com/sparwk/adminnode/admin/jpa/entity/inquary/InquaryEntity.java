package com.sparwk.adminnode.admin.jpa.entity.inquary;

import com.sparwk.adminnode.admin.jpa.entity.BaseEntity;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name="tb_admin_login_log")
public class InquaryEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "admin_login_log_seq")
    private Long adminLoginLogSeq;

    @Column(name = "connect_time")
    private LocalDateTime connectTime;

    @Column(name = "connect_result", nullable = false, length = 30)
    private String connectResult;

    @Column(name = "admin_id")
    private Long adminId;

    @Column(name = "connect_browser", nullable = false, length = 30)
    private String connectBrowser;

    @Column(name = "connect_os", nullable = false, length = 30)
    private String connectOs;

    @Column(name = "connect_device", nullable = false, length = 30)
    private String connectDevice;

    @Column(name = "connect_ip", nullable = false, length = 30)
    private String connectIp;


    @Builder
    public InquaryEntity(
                            Long adminLoginLogSeq,
                            LocalDateTime connectTime,
                            String connectResult,
                            Long adminId,
                            String connectBrowser,
                            String connectOs,
                            String connectDevice,
                            String connectIp
                        ) {
        this.adminLoginLogSeq = adminLoginLogSeq;
        this.connectTime = connectTime;
        this.connectResult = connectResult;
        this.adminId = adminId;
        this.connectBrowser = connectBrowser;
        this.connectOs = connectOs;
        this.connectDevice = connectDevice;
        this.connectIp = connectIp;
    }

}
