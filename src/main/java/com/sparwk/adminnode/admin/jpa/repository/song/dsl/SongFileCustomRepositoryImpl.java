package com.sparwk.adminnode.admin.jpa.repository.song.dsl;

import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.sparwk.adminnode.admin.biz.v1.song.dto.SongFileDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import static com.sparwk.adminnode.admin.jpa.entity.song.QSongFileEntity.songFileEntity;

@Repository
public class SongFileCustomRepositoryImpl implements SongFileCustomRepository {

    private final JPAQueryFactory jpaQueryFactory;

    private final Logger logger = LoggerFactory.getLogger(SongFileCustomRepositoryImpl.class);

    public SongFileCustomRepositoryImpl(JPAQueryFactory jpaQueryFactory) {
        this.jpaQueryFactory = jpaQueryFactory;
    }

    @Override
    public SongFileDto findQrySongFile(Long id) {

        //고정갑, 상수
        return jpaQueryFactory.select(Projections.bean(SongFileDto.class,
                        songFileEntity.songId,
                        songFileEntity.songFileSeq,
                        songFileEntity.songFileName,
                        songFileEntity.songFilePath,
                        songFileEntity.songFileComt,
                        songFileEntity.songFileSize,
                        songFileEntity.regDt
                        )
                )
                .from(songFileEntity)
                .where(songFileEntity.songId.eq(id))
                .fetchOne();

    }
}
