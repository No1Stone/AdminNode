package com.sparwk.adminnode.admin.jpa.entity.song;

import com.sparwk.adminnode.admin.jpa.entity.BaseEntity;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.song.id.SongLyricsId;
import com.sparwk.adminnode.admin.jpa.entity.song.id.SongLyricsLangId;
import lombok.*;

import javax.persistence.*;

@Entity
@Getter
@Setter
@IdClass(SongLyricsLangId.class)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_song_lyrics_lang")
public class SongLyricsLangEntity extends BaseEntity {
    @Id
    @Column(name = "song_id", nullable = true)
    private long songId;
    @Id
    @Column(name = "lang_cd", nullable = true)
    private String langCd;


    @Builder
    SongLyricsLangEntity(
            long songId,
            String langCd
    ) {
        this.songId = songId;
        this.langCd = langCd;
    }
}
