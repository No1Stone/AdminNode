package com.sparwk.adminnode.admin.jpa.repository.test;

import com.sparwk.adminnode.admin.jpa.entity.test.TestEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TestRepository extends JpaRepository<TestEntity, Long> {
}
