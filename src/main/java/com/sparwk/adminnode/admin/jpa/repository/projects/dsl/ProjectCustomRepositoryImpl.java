package com.sparwk.adminnode.admin.jpa.repository.projects.dsl;

import com.querydsl.core.types.ExpressionUtils;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.JPAExpressions;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.sparwk.adminnode.admin.biz.v1.projects.dto.*;
import com.sparwk.adminnode.admin.jpa.entity.PeriodTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.projects.ProjcetCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.projects.ProjectSorterEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static com.querydsl.core.types.dsl.Expressions.asString;
import static com.sparwk.adminnode.admin.jpa.entity.account.QAccountCompanyDetailEntity.accountCompanyDetailEntity;
import static com.sparwk.adminnode.admin.jpa.entity.commonCode.QCommonDetailCodeEntity.commonDetailCodeEntity;
import static com.sparwk.adminnode.admin.jpa.entity.country.QCountryEntity.countryEntity;
import static com.sparwk.adminnode.admin.jpa.entity.profile.QProfileCompanyEntity.profileCompanyEntity;
import static com.sparwk.adminnode.admin.jpa.entity.profile.QProfileEntity.profileEntity;
import static com.sparwk.adminnode.admin.jpa.entity.projects.QProjectEntity.projectEntity;
import static com.sparwk.adminnode.admin.jpa.entity.projects.QProjectInviteCompany.projectInviteCompany;
import static com.sparwk.adminnode.admin.jpa.entity.projects.QProjectMetadata.projectMetadata;
import static com.sparwk.adminnode.admin.jpa.entity.projects.QProjectReferenceFile.projectReferenceFile;
import static com.sparwk.adminnode.admin.jpa.entity.projects.QProjectServiceCountry.projectServiceCountry;
import static com.sparwk.adminnode.admin.jpa.entity.projects.QProjectSong.projectSong;
import static com.sparwk.adminnode.admin.jpa.entity.role.QRoleDetailCodeEntity.roleDetailCodeEntity;
import static com.sparwk.adminnode.admin.jpa.entity.song.QSongEntity.songEntity;
import static com.sparwk.adminnode.admin.jpa.entity.song.QSongViewEntity.songViewEntity;
import static com.sparwk.adminnode.admin.jpa.entity.songCode.QSongDetailCodeEntity.songDetailCodeEntity;

@Repository
public class ProjectCustomRepositoryImpl implements ProjectCustomRepository {

    private final JPAQueryFactory jpaQueryFactory;

    private final Logger logger = LoggerFactory.getLogger(ProjectCustomRepositoryImpl.class);

    public ProjectCustomRepositoryImpl(JPAQueryFactory jpaQueryFactory) {
        this.jpaQueryFactory = jpaQueryFactory;
    }

    @Override
    public List<ProjectListDto> findQryAll(ProjcetCateEnum cate,
                                           String val,
                                           YnTypeEnum completeVal,
                                           PeriodTypeEnum periodType,
                                           String sdate,
                                           String edate,
                                           ProjectSorterEnum sorter,
                                           PageRequest pageRequest) {
        return jpaQueryFactory.select(Projections.bean(ProjectListDto.class,

                                projectEntity.projId,
                                projectEntity.projTitle.as("projectTitle"),
                                projectEntity.avatarFileUrl.as("projectAvatarUrl"),
                                projectEntity.pitchProjTypeCd.as("projectType"),
                                ExpressionUtils.as(
                                        JPAExpressions.select(commonDetailCodeEntity.val)
                                                .from(commonDetailCodeEntity)
                                                .where(commonDetailCodeEntity.dcode.eq(projectEntity.pitchProjTypeCd)),
                                        "projectTypeName"),
                                projectEntity.projOwner.as("projOwner"),
                                profileEntity.fullName.as("projOwnerName"),
                                projectEntity.projIndivCompType,
                                ExpressionUtils.as(
                                        JPAExpressions.select(commonDetailCodeEntity.val)
                                                .from(commonDetailCodeEntity)
                                                .where(commonDetailCodeEntity.dcode.eq(projectEntity.projIndivCompType)),
                                        "projIndivCompTypeName"),
                                projectEntity.projCondCd.as("budget"),
                                commonDetailCodeEntity.val.as("budgetName"),
                                projectEntity.completeYn,
                                projectEntity.projDdlDt,
                                projectEntity.regUsr,
                                projectEntity.regDt,
                                projectEntity.modUsr,
                                projectEntity.modDt
                        )
                )
                .from(projectEntity)
                .leftJoin(commonDetailCodeEntity).on(commonDetailCodeEntity.dcode.eq(projectEntity.projCondCd))
                .leftJoin(songDetailCodeEntity).on(songDetailCodeEntity.dcode.eq(projectEntity.pitchProjTypeCd))
                .leftJoin(profileEntity).on(projectEntity.projOwner.eq(profileEntity.profileId))
                .where(completeYnEq(completeVal) ,valLike(cate, val), termBetween(periodType, sdate, edate))
                .limit(pageRequest.getPageSize())
                .offset(pageRequest.getPageSize() * (pageRequest.getPageNumber() - 1))
                .orderBy(sorterOrder(sorter))
                .fetch();
    }

    @Override
    public List<ProjectListDto> findQryCompany(ProjcetCateEnum cate,
                                           String val,
                                           YnTypeEnum completeVal,
                                           PeriodTypeEnum periodType,
                                           String sdate,
                                           String edate,
                                           ProjectSorterEnum sorter,
                                           PageRequest pageRequest) {
        return jpaQueryFactory.select(Projections.bean(ProjectListDto.class,

                                projectInviteCompany.projId,
                                projectEntity.projTitle.as("projectTitle"),
                                projectEntity.projOwner.as("projOwner"),
                                profileEntity.fullName.as("projOwnerName"),
                                projectEntity.projCondCd.as("budget"),
                                commonDetailCodeEntity.val.as("budgetName"),
                                projectEntity.projDdlDt,
                                projectEntity.regUsr,
                                projectEntity.regDt,
                                projectEntity.modUsr,
                                projectEntity.modDt
                        )
                )
                .from(projectInviteCompany)
                .leftJoin(profileCompanyEntity).on(profileCompanyEntity.profileId.eq(projectInviteCompany.companyProfileId))
                .leftJoin(accountCompanyDetailEntity).on(accountCompanyDetailEntity.accntId.eq(profileCompanyEntity.accntId))
                .leftJoin(projectEntity).on(projectInviteCompany.projId.eq(projectInviteCompany.projId))
                .leftJoin(profileEntity).on(profileEntity.profileId.eq(projectEntity.projOwner))
                .leftJoin(commonDetailCodeEntity).on(commonDetailCodeEntity.dcode.eq(projectEntity.projCondCd))
                .where(completeYnEq(completeVal))
                .limit(pageRequest.getPageSize())
                .offset(pageRequest.getPageSize() * (pageRequest.getPageNumber() - 1))
                .orderBy(sorterOrder(sorter))
                .fetch();
    }

    @Override
    public List<ProjectMetadataDto> findQrySongMeta(String pcode, Long projId) {

        return jpaQueryFactory.select(Projections.bean(ProjectMetadataDto.class,
                                songDetailCodeEntity.val.as("attrDtlCdVal"),
                        projectMetadata.projId,
                        projectMetadata.attrTypeCd,
                        projectMetadata.attrDtlCd
                        )
                )
                .from(projectMetadata)
                .leftJoin(songDetailCodeEntity).on(projectMetadata.attrDtlCd.eq(songDetailCodeEntity.dcode))
                .where(projectMetadata.projId.eq(projId),songDetailCodeEntity.pcode.eq(pcode))
                .fetch();

    }

    @Override
    public List<ProjectMetadataDto> findQryRolesMeta(String pcode, Long projId) {

        return jpaQueryFactory.select(Projections.bean(ProjectMetadataDto.class,
                                roleDetailCodeEntity.role.as("attrDtlCdVal"),
                                projectMetadata.projId,
                                projectMetadata.attrTypeCd,
                                projectMetadata.attrDtlCd
                        )
                )
                .from(projectMetadata)
                .leftJoin(roleDetailCodeEntity).on(projectMetadata.attrDtlCd.eq(roleDetailCodeEntity.dcode))
                .where(projectMetadata.projId.eq(projId),roleDetailCodeEntity.pcode.eq(pcode))
                .fetch();

    }

    @Override
    public List<ProjectMetadataDto> findQryCountryMeta(String pcode, Long projId) {

        return jpaQueryFactory.select(Projections.bean(ProjectMetadataDto.class,
                        countryEntity.country.as("attrDtlCdVal"),
                        projectServiceCountry.projId,
                        projectServiceCountry.serviceCntrCd.as("attrTypeCd"),
                        asString("CNT").as("attrDtlCd")
                        )
                )
                .from(projectServiceCountry)
                .leftJoin(countryEntity).on(projectServiceCountry.serviceCntrCd.eq(countryEntity.countryCd))
                .where(projectServiceCountry.projId.eq(projId))
                .fetch();

    }

    @Override
    public List<ProjectMetadataDto> findQryCommonMeta(String pcode, String code, Long projId) {

        return jpaQueryFactory.select(Projections.bean(ProjectMetadataDto.class,
                                commonDetailCodeEntity.val.as("attrDtlCdVal"),
                                projectEntity.projId,
                                commonDetailCodeEntity.pcode,
                                projectEntity.pitchProjTypeCd
                        )
                )
                .from(projectEntity)
                .leftJoin(commonDetailCodeEntity).on(joinOnEq(pcode))
                .where(commonDetailCodeEntity.pcode.eq(pcode),commonDetailCodeEntity.dcode.eq(code), projectEntity.projId.eq(projId))
                .fetch();

    }

    @Override
    public List<ProjectProfileDto> findQryProjInviteCom(Long projId) {

        return jpaQueryFactory.select(Projections.bean(ProjectProfileDto.class,
                        projectInviteCompany.projId,
                        projectInviteCompany.companyProfileId.as("profileId"),
                        accountCompanyDetailEntity.companyName.as("profileFullName")
                        )
                )
                .from(projectInviteCompany)
                .leftJoin(profileCompanyEntity).on(projectInviteCompany.companyProfileId.eq(profileCompanyEntity.profileId))
                .leftJoin(accountCompanyDetailEntity).on(profileCompanyEntity.accntId.eq(accountCompanyDetailEntity.accntId))
                .where(projectInviteCompany.projId.eq(projId))
                .fetch();

    }

    @Override
    public List<ProjectReferenceFileDto> findQryProjectReferenceFileList(Long Id) {
        return jpaQueryFactory.select(Projections.bean(ProjectReferenceFileDto.class,

                        projectReferenceFile.projId,
                        projectReferenceFile.fileFefSeq,
                        projectReferenceFile.uploadTypeCd,
                        commonDetailCodeEntity.val.as("uploadTypeCdName"),
                        projectReferenceFile.fileName,
                        projectReferenceFile.filePath,
                        projectReferenceFile.fileSize,
                        projectReferenceFile.refUrl
                        )
                )
                .from(projectReferenceFile)
                .leftJoin(commonDetailCodeEntity).on(commonDetailCodeEntity.dcode.eq(projectReferenceFile.uploadTypeCd))
                .where(projectReferenceFile.projId.eq(Id))
                .orderBy(projectReferenceFile.fileFefSeq.asc())
                .fetch();
    }

    @Override
    public ProjectMetadataDto findQryWorkLocatMeta(String pcode, String code, Long projId) {

        return jpaQueryFactory.select(Projections.bean(ProjectMetadataDto.class,
                                commonDetailCodeEntity.val.as("attrDtlCdVal"),
                                projectEntity.projId,
                                commonDetailCodeEntity.pcode,
                                projectEntity.pitchProjTypeCd
                        )
                )
                .from(projectEntity)
                .leftJoin(commonDetailCodeEntity).on(joinOnEq(pcode))
                .where(commonDetailCodeEntity.pcode.eq(pcode),commonDetailCodeEntity.dcode.eq(code), projectEntity.projId.eq(projId))
                .fetchOne();

    }


    @Override
    public List<ProjectSongDto> findQryProjectSongList(Long Id) {
        return jpaQueryFactory.select(Projections.bean(ProjectSongDto.class,

                        projectSong.projId,
                            projectSong.songId,
                        songViewEntity.songTitle,
                        songViewEntity.songGenreName.as("songGenre"),
                        songViewEntity.songSubgenreName.as("songSubgenre"),
                        songViewEntity.songLangCdName.as("songLanguage"),
                        songViewEntity.songStatusCdName.as("songStatusName")
                        )
                )
                .from(songViewEntity)
                .leftJoin(projectSong).on(projectSong.songId.eq(songViewEntity.songId))
                .where(projectSong.projId.eq(Id))
                .orderBy(songViewEntity.songTitle.asc())
                .fetch();
    }

    private BooleanExpression joinOnEq(String pcode) {
        if (pcode.equals("PJT"))
            return projectEntity.pitchProjTypeCd.eq(commonDetailCodeEntity.dcode);

        if (pcode.equals("PJC"))
            return projectEntity.projCondCd.eq(commonDetailCodeEntity.dcode);

        if (pcode.equals("PWL"))
            return projectEntity.projWorkLocat.eq(commonDetailCodeEntity.dcode);

        return projectEntity.pitchProjTypeCd.eq(commonDetailCodeEntity.dcode);
    }

    private BooleanExpression completeYnEq(YnTypeEnum val) {
        if (val == YnTypeEnum.Y)
            return projectEntity.completeYn.eq(YnTypeEnum.Y);

        if (val == YnTypeEnum.N)
            return projectEntity.completeYn.eq(YnTypeEnum.N);

        return null;
    }

    private BooleanExpression valLike(ProjcetCateEnum cate, String val) {
        if (cate !=null && val != null && val.length() > 0) {
            switch (cate) {
                case All:
                    return projectEntity.projTitle.toLowerCase().contains(val);
                            //.or(projectEntity.description.contains(val));
                case PojectTitle:
                    return projectEntity.projTitle.toLowerCase().contains(val);
                case PojectType:
                    return commonDetailCodeEntity.val.toLowerCase().contains(val);
                case Owner:
                    return profileEntity.fullName.contains(val);
                case MusicGenre:
                    return projectEntity.projCondCd.contains(val);
            }
        }
        return null;
    }

    private BooleanExpression idEq(Long id) {
        if (id != null)
            return projectEntity.projId.eq(id);
        return null;
    }

    private BooleanExpression termBetween(PeriodTypeEnum periodType, String  sdate, String edate) {

        if (sdate != null && edate != null && periodType != null) {

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
            LocalDateTime searchStartDate = LocalDateTime.parse(sdate + " 00:00:00.000", formatter);
            LocalDateTime searchEndDate = LocalDateTime.parse(edate + " 23:59:59.999", formatter);

            switch (periodType) {
                case Created:
                    return projectEntity.regDt.between(searchStartDate, searchEndDate);
                case LastModified:
                    return projectEntity.modDt.between(searchStartDate, searchEndDate);
                default:
                    return null;
            }
        }
        return null;
    }


    private OrderSpecifier sorterOrder(ProjectSorterEnum sorter) {
        if (sorter == null)
            return projectEntity.regDt.desc();

        if (sorter == ProjectSorterEnum.ProjectTitleAsc)
            return projectEntity.projTitle.asc();

        if (sorter == ProjectSorterEnum.ProjectTitleDesc)
            return projectEntity.projTitle.desc();

        if (sorter == ProjectSorterEnum.CreatedAsc)
            return projectEntity.regDt.asc();

        if (sorter == ProjectSorterEnum.CreatedDesc)
            return projectEntity.regDt.desc();

        return projectEntity.regDt.desc();
    }

}
