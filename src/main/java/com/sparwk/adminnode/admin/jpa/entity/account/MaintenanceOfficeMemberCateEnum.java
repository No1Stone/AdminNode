package com.sparwk.adminnode.admin.jpa.entity.account;

import lombok.Getter;

@Getter
public enum MaintenanceOfficeMemberCateEnum {

    All("All"),
    CompanyName("CompanyName"),
    CompanyType("CompanyType"),
    Email("Email"),
    Location("Location"),
    Ipi("IPI"),
    VatNo("VatNo");

    private String cate;

    MaintenanceOfficeMemberCateEnum(String cate) {
        this.cate = cate;
    }
}
