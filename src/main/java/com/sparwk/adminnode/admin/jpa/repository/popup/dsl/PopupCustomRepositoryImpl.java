package com.sparwk.adminnode.admin.jpa.repository.popup.dsl;

import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.sparwk.adminnode.admin.biz.v1.popup.dto.PopupDto;
import com.sparwk.adminnode.admin.jpa.entity.PeriodTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.admin.QAdminEntity;
import com.sparwk.adminnode.admin.jpa.entity.commonCode.CommonSorterEnum2;
import com.sparwk.adminnode.admin.jpa.entity.popup.PopupCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.popup.PopupSorterEnum;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static com.sparwk.adminnode.admin.jpa.entity.admin.QAdminEntity.adminEntity;
import static com.sparwk.adminnode.admin.jpa.entity.board.QBoardSuccessEntity.boardSuccessEntity;
import static com.sparwk.adminnode.admin.jpa.entity.commonCode.QCommonDetailCodeEntity.commonDetailCodeEntity;
import static com.sparwk.adminnode.admin.jpa.entity.commonCode.QCommonOrganizationCodeEntity.commonOrganizationCodeEntity;
import static com.sparwk.adminnode.admin.jpa.entity.popup.QPopupEntity.popupEntity;

@Repository
public class PopupCustomRepositoryImpl implements PopupCustomRepository {

    private final JPAQueryFactory jpaQueryFactory;

    public PopupCustomRepositoryImpl(JPAQueryFactory jpaQueryFactory) {
        this.jpaQueryFactory = jpaQueryFactory;
    }

    @Override
    public List<PopupDto> findQryAll(PopupCateEnum cate,
                                   String val,
                                   PeriodTypeEnum periodType,
                                   String sdate,
                                   String edate,
                                   PopupSorterEnum sorter,
                                   PageRequest pageRequest) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(Projections.bean(PopupDto.class,
                                popupEntity.popupSeq,
                                popupEntity.popupTitle,
                                popupEntity.popupStatusCd,
                                commonDetailCodeEntity.val.as("popupStatusCdName"),
                                popupEntity.sdate,
                                popupEntity.edate,
                                popupEntity.popupSizeWidth,
                                popupEntity.popupSizeHeight,
                                popupEntity.popupLocationWidth,
                                popupEntity.popupLocationHeight,
                                popupEntity.content,
                                popupEntity.regUsr,
                                adminEntity.fullName.as("regUsrName"),
                                popupEntity.regDt,
                                popupEntity.modUsr,
                                adminEntity2.fullName.as("modUsrName"),
                                popupEntity.modDt
                        )
                )
                .from(popupEntity)
                .leftJoin(commonDetailCodeEntity).on(popupEntity.popupStatusCd.eq(commonDetailCodeEntity.dcode))
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(popupEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(popupEntity.modUsr))
                .where(valLike(cate, val), termBetween(periodType, sdate, edate))
                .limit(pageRequest.getPageSize())
                .offset(pageRequest.getPageSize() * (pageRequest.getPageNumber() - 1))
                .orderBy(sorterOrder(sorter))
                .fetch();
    }

    @Override
    public  long countQryAll(PopupCateEnum cate,
                                     String val,
                                     PeriodTypeEnum periodType,
                                     String sdate,
                                     String edate) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(
                                popupEntity.popupSeq.count()
                )
                .from(popupEntity)
                .leftJoin(commonDetailCodeEntity).on(popupEntity.popupStatusCd.eq(commonDetailCodeEntity.dcode))
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(popupEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(popupEntity.modUsr))
                .where(valLike(cate, val), termBetween(periodType, sdate, edate))
                .fetchOne();
    }

    @Override
    public long deleteQryList(Long[] id) {
        return jpaQueryFactory.delete(popupEntity)
                .where(popupEntity.popupSeq.in(id))
                .execute();
    }

    private BooleanExpression valLike(PopupCateEnum cate, String val) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        if (cate != null && val != null && val.length() > 0) {
            switch (cate) {
                case All:
                    return popupEntity.popupTitle.toLowerCase().contains(val.toLowerCase())
                            .or(commonDetailCodeEntity.val.toLowerCase().contains(val.toLowerCase()));
                case Title:
                    return popupEntity.popupTitle.toLowerCase().contains(val.toLowerCase());
                case Status:
                    return commonDetailCodeEntity.val.toLowerCase().contains(val.toLowerCase());
                case RegUsrName:
                    return adminEntity.fullName.toLowerCase().contains(val.toLowerCase());
                case ModUsrName:
                    return adminEntity2.fullName.toLowerCase().contains(val.toLowerCase());
            }
        }
        return null;
    }

    //상태로 찾기(기능 보류 중)
    private BooleanExpression statusTypeEq(String popupStatusCd) {
        if (popupStatusCd != null)
            return popupEntity.popupStatusCd.eq(popupStatusCd);
        return null;
    }

    private BooleanExpression termBetween(PeriodTypeEnum periodType, String  sdate, String edate) {

        if (sdate != null && edate != null && periodType != null) {

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
            LocalDateTime searchStartDate = LocalDateTime.parse(sdate + " 00:00:00.000", formatter);
            LocalDateTime searchEndDate = LocalDateTime.parse(edate + " 23:59:59.999", formatter);

            switch (periodType) {
                case Created:
                    return popupEntity.regDt.between(searchStartDate, searchEndDate);
                case LastModified:
                    return popupEntity.modDt.between(searchStartDate, searchEndDate);
                default:
                    return null;
            }
        }
        return null;
    }

    private OrderSpecifier sorterOrder(PopupSorterEnum sorter) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        if (sorter == null)
            return popupEntity.regDt.desc();

        if (sorter == PopupSorterEnum.TitleAsc)
            return popupEntity.popupTitle.asc();

        if (sorter == PopupSorterEnum.TitleDesc)
            return popupEntity.popupTitle.desc();

        if (sorter == PopupSorterEnum.SdateAsc)
            return popupEntity.sdate.asc();

        if (sorter == PopupSorterEnum.SdateDesc)
            return popupEntity.sdate.desc();

        if (sorter == PopupSorterEnum.EdateAsc)
            return popupEntity.edate.asc();

        if (sorter == PopupSorterEnum.EdateDesc)
            return popupEntity.edate.desc();

        if (sorter == PopupSorterEnum.StatusAsc)
            return commonDetailCodeEntity.val.asc();

        if (sorter == PopupSorterEnum.StatusDesc)
            return commonDetailCodeEntity.val.desc();

        if (sorter == PopupSorterEnum.CreatedAsc)
            return popupEntity.regDt.asc();

        if (sorter == PopupSorterEnum.CreatedDesc)
            return popupEntity.regDt.desc();

        if (sorter == PopupSorterEnum.CreaterNameAsc)
            return adminEntity.fullName.asc();

        if (sorter == PopupSorterEnum.CreaterNameDesc)
            return adminEntity.fullName.desc();

        if (sorter == PopupSorterEnum.ModifierNameAsc)
            return adminEntity2.fullName.asc();

        if (sorter == PopupSorterEnum.ModifierNameDesc)
            return adminEntity2.fullName.desc();

        return popupEntity.regDt.desc();
    }

}
