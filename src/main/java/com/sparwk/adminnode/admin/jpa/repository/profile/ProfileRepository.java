package com.sparwk.adminnode.admin.jpa.repository.profile;

import com.sparwk.adminnode.admin.jpa.entity.profile.ProfileEntity;
import com.sparwk.adminnode.admin.jpa.repository.profile.dsl.ProfileCustomRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ProfileRepository extends JpaRepository<ProfileEntity, Long>, ProfileCustomRepository {
    
}


