package com.sparwk.adminnode.admin.jpa.entity.profile;

import com.sparwk.adminnode.admin.jpa.entity.BaseEntity;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_profile_position")
@DynamicUpdate
public class ProfilePositionEntity extends BaseEntity {


    @Id
    @Column(name = "profile_position_seq")
    private Long profilePositionSeq;

    @Column(name = "profile_id")
    private Long profileId;

    @Column(name = "company_profile_id")
    private Long companyProfileId;

    @Enumerated(EnumType.STRING)
    @Column(name = "artist_yn", nullable = true, length = 1)
    private YnTypeEnum artistYn;

    @Enumerated(EnumType.STRING)
    @Column(name = "anr_yn",length = 1)
    private YnTypeEnum anrYn;

    @Column(name = "dept_role_info", nullable = true, length = 100)
    private String deptRoleInfo;

    @Enumerated(EnumType.STRING)
    @Column(name = "primary_yn", nullable = true, length = 1)
    private YnTypeEnum primaryYn;

    @Enumerated(EnumType.STRING)
    @Column(name = "company_verify_yn", nullable = true, length = 1)
    private YnTypeEnum companyVerifyYn;

    @Enumerated(EnumType.STRING)
    @Column(name = "creator_yn", nullable = true, length = 1)
    private YnTypeEnum creatorYn;


    @Builder
    ProfilePositionEntity(
            Long profilePositionSeq,
            Long profileId,
            Long companyProfileId,
            YnTypeEnum anrYn,
            YnTypeEnum artistYn,
            String deptRoleInfo,
            YnTypeEnum primaryYn,
            YnTypeEnum companyVerifyYn,
            YnTypeEnum creatorYn
            ){
        this.profilePositionSeq = profilePositionSeq;
        this.profileId=profileId;
        this.companyProfileId=companyProfileId;
        this.anrYn=anrYn;
        this.artistYn=artistYn;
        this.deptRoleInfo=deptRoleInfo;
        this.primaryYn = primaryYn;
        this.companyVerifyYn = companyVerifyYn;
        this.creatorYn = creatorYn;
    }

}
