package com.sparwk.adminnode.admin.jpa.repository.song.dsl;

import com.sparwk.adminnode.admin.biz.v1.song.dto.SongEvaluationDtailListDto;
import com.sparwk.adminnode.admin.biz.v1.song.dto.SongEvaluationDto;
import com.sparwk.adminnode.admin.biz.v1.song.dto.SongMetadataDto;

import java.util.List;

public interface SongEvaluationCustomRepository {

    List<SongEvaluationDto> findQryEvaluation(Long id);

    List<SongEvaluationDtailListDto> findQryEvaluationDetailList(Long id, Long evalAnrSeq);

}
