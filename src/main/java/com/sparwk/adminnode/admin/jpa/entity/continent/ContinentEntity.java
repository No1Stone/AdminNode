package com.sparwk.adminnode.admin.jpa.entity.continent;

import com.sparwk.adminnode.admin.jpa.entity.BaseEntity;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name="tb_admin_continent_code")
public class ContinentEntity extends BaseEntity implements Serializable {

    @Id
    @GeneratedValue(
            generator = "tb_admin_continent_code_seq"
    )
    @Column(name = "continent_seq")
    private Long continentSeq;

    @Column(name = "continent", length = 15)
    private String continent;

    @Column(name = "continent_cd", length = 9)
    private String continentCd;

    @Column(name = "code", length = 2)
    private String code;

    @Column(name = "description", length = 1000)
    private String description;

    @Enumerated(EnumType.STRING)
    @Column(name = "use_yn", length = 1)
    private YnTypeEnum useType;

    @Builder
    public ContinentEntity(
                        Long continentSeq,
                        String continent,
                        String continentCd,
                        String code,
                        String description,
                        YnTypeEnum useType
                ) {
        this.continentSeq = continentSeq;
        this.continent = continent;
        this.continentCd = continentCd;
        this.code = code;
        this.description = description;
        this.useType = useType;
    }

    public void updateYn(YnTypeEnum useType) {
        this.useType = useType;
    }

    public void update(
                        String continent,
                        String code,
                        String description,
                        YnTypeEnum useType
                ) {
        this.continent = continent;
        this.code = code;
        this.description = description;
        this.useType = useType;
    }

}
