package com.sparwk.adminnode.admin.jpa.repository.song.dsl;

import com.querydsl.core.types.Projections;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.sparwk.adminnode.admin.biz.v1.song.dto.SongMetadataDto;
import com.sparwk.adminnode.admin.biz.v1.song.dto.SongTimelineDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.sparwk.adminnode.admin.jpa.entity.song.QSongTimelineEntity.songTimelineEntity;
import static com.sparwk.adminnode.admin.jpa.entity.commonCode.QCommonDetailCodeEntity.commonDetailCodeEntity;
import static com.sparwk.adminnode.admin.jpa.entity.profile.QProfileEntity.profileEntity;

@Repository
public class SongTimelineCustomRepositoryImpl implements SongTimelineCustomRepository {

    private final JPAQueryFactory jpaQueryFactory;

    private final Logger logger = LoggerFactory.getLogger(SongTimelineCustomRepositoryImpl.class);

    public SongTimelineCustomRepositoryImpl(JPAQueryFactory jpaQueryFactory) {
        this.jpaQueryFactory = jpaQueryFactory;
    }

    @Override
    public List<SongTimelineDto> findQrySongTimeline(Long id) {

        return jpaQueryFactory.select(Projections.bean(SongTimelineDto.class,
                        songTimelineEntity.timelineSeq,
                        songTimelineEntity.songId,
                                songTimelineEntity.componentUseId,
                                profileEntity.fullName,
                        songTimelineEntity.componetCd,
                            commonDetailCodeEntity.val.as("componetCdName"),
                        songTimelineEntity.resultMsg,
                        songTimelineEntity.regDt
                        )
                )
                .from(songTimelineEntity)
                .leftJoin(commonDetailCodeEntity).on(commonDetailCodeEntity.dcode.eq(songTimelineEntity.componetCd))
                .leftJoin(profileEntity).on(profileEntity.profileId.eq(songTimelineEntity.componentUseId))
                .where(songTimelineEntity.songId.eq(id))
                .fetch();

    }
}
