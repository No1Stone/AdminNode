package com.sparwk.adminnode.admin.jpa.entity.email;

import com.sparwk.adminnode.admin.jpa.entity.email.id.EmailAttachId;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@IdClass(EmailAttachId.class)
@Table(name="tb_email_attach")
public class EmailAttachEntity {

    @Id
    @Column(name = "email_seq")
    private Long emailSeq;

    @Id
    @Column(name = "attach_order")
    private int attachOrder;

    @Column(name = "attach_file_name", length = 200)
    private String attachFileName;

    @Column(name = "attach_file_size")
    private int attachFileSize;


    @Builder
    public EmailAttachEntity(
                            Long emailSeq,
                            int attachOrder,
                            String attachFileName,
                            int attachFileSize
                        ) {
        this.emailSeq = emailSeq;
        this.attachOrder = attachOrder;
        this.attachFileName = attachFileName;
        this.attachFileSize = attachFileSize;
    }

}
