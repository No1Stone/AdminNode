package com.sparwk.adminnode.admin.jpa.entity.account;

import lombok.Getter;

@Getter
public enum AccountCompanyCateEnum {

    All("All"),
    Email("Email"),
    CompanyName("CompanyName"),
    CompanyType("CompanyType"),
    BussinessLocation("BussinessLocation");

    private String cate;

    AccountCompanyCateEnum(String cate) {
        this.cate = cate;
    }
}
