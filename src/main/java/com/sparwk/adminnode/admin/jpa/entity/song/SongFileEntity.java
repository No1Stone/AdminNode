package com.sparwk.adminnode.admin.jpa.entity.song;

import com.sparwk.adminnode.admin.jpa.entity.BaseEntity;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.song.id.SongFileId;
import com.sparwk.adminnode.admin.jpa.entity.song.id.SongLyricsId;
import lombok.*;

import javax.persistence.*;

@Entity
@Getter
@Setter
@IdClass(SongFileId.class)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_song_file")
public class SongFileEntity extends BaseEntity {
    @Id
    @Column(name = "song_id")
    private long songId;
    @Id
    @Column(name = "song_file_seq")
    private long songFileSeq;
    @Column(name = "song_file_name", nullable = true, length = 100)
    private String songFileName;
    @Column(name = "song_file_path", nullable = true, length = 200)
    private String songFilePath;
    @Column(name = "song_file_size", nullable = true)
    private int songFileSize;
    @Column(name = "song_file_comt", nullable = true, length = 200)
    private String songFileComt;
    @Column(name = "bpm", nullable = true)
    private int bpm;
    @Column(name = "contain_sample_yn", nullable = true, length = 1)
    private String containSampleYn;
    @Column(name = "stereo_type", nullable = true, length = 9)
    private String stereoType;
    @Column(name = "bit_depth", nullable = true, length = 9)
    private String bitDepth;
    @Column(name = "sampling_rate", nullable = true, length = 10)
    private String samplingRate;



    @Builder
    SongFileEntity(
            long songId,
            long songFileSeq,
            String songFileName,
            String songFilePath,
            int songFileSize,
            String songFileComt,
            int bpm,
            String containSampleYn,
            String stereoType,
            String bitDepth,
            String samplingRate
    ) {
        this.songId = songId;
        this.songFileSeq = songFileSeq;
        this.songFileName = songFileName;
        this.songFilePath = songFilePath;
        this.songFileSize = (String.valueOf(songFileSize) == null) ? 0 : songFileSize;
        this.songFileComt = songFileComt;
        this.bpm = bpm;
        this.containSampleYn = containSampleYn;
        this.stereoType = stereoType;
        this.bitDepth = bitDepth;
        this.samplingRate = samplingRate;
    }

    public void update(
            String songFileComt
    ) {
        this.songFileComt = songFileComt;
    }
}
