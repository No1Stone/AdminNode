package com.sparwk.adminnode.admin.jpa.entity;

import lombok.Getter;

@Getter
public enum PeriodTypeEnum {

    Created("Created"), LastModified("LastModified");

    private String periodType;

    PeriodTypeEnum(String periodType) {
        this.periodType = periodType;
    }

}
