package com.sparwk.adminnode.admin.jpa.entity.sns;

import lombok.Getter;

@Getter
public enum SnsCateEnum {

    All("All"), SNS("SNS"), RegUsrName("RegUsrName"), ModUsrName("ModUsrName");

    private String snsCateEnum;

    SnsCateEnum(String snsCateEnum) {
        this.snsCateEnum = snsCateEnum;
    }
}
