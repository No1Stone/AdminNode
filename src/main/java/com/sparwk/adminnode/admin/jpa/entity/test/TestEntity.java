package com.sparwk.adminnode.admin.jpa.entity.test;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name="test_json")
public class TestEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "test_id")
    private Long testId;

    @Column(name = "ckey", length = 30)
    private String ckey;

    @Column(name = "cval", length = 30)
    private String cval;

    @Builder
    public TestEntity(String ckey, String cval) {
        this.ckey = ckey;
        this.cval = cval;
    }

    public void update(String ckey, String cval) {
        this.ckey = ckey;
        this.cval = cval;
    }

}
