package com.sparwk.adminnode.admin.jpa.entity.account;


import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_account_company_type")
@DynamicUpdate
public class AccountCompanyTypeEntity {
    @Id
    @Column(name = "accnt_id")
    private Long accntId;

    @Column(name = "company_cd", nullable = false, length = 9)
    private String companyCd;

    @Column(name = "company_license_file_url", nullable = true, length = 200)
    private String companyLicenseFileUrl;

    @Enumerated(EnumType.STRING)
    @Column(name = "company_license_verify_yn", nullable = true, length = 1)
    private YnTypeEnum companyLicenseVerifyYn;

    @Column(name = "company_license_file_name", nullable = true, length = 100)
    private String companyLicenseFileName;

    @Builder
    AccountCompanyTypeEntity(
            Long accntId,
            String companyCd,
            String companyLicenseFileUrl,
            YnTypeEnum companyLicenseVerifyYn,
            String companyLicenseFileName
    ) {
        this.accntId = accntId;
        this.companyCd = companyCd;
        this.companyLicenseFileUrl = companyLicenseFileUrl;
        this.companyLicenseVerifyYn = companyLicenseVerifyYn;
        this.companyLicenseFileName = companyLicenseFileName;
    }

//    @OneToOne(mappedBy = "accountPassport")
//    private Account account;
}
