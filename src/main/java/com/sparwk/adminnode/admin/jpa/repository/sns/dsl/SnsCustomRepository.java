package com.sparwk.adminnode.admin.jpa.repository.sns.dsl;

import com.sparwk.adminnode.admin.biz.v1.sns.dto.SnsDto;
import com.sparwk.adminnode.admin.jpa.entity.PeriodTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.sns.SnsCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.sns.SnsSorterEnum;
import org.springframework.data.domain.PageRequest;

import java.util.List;

public interface SnsCustomRepository {
    List<SnsDto> findQryAll(SnsCateEnum cate,
                            String val,
                            YnTypeEnum useType,
                            PeriodTypeEnum periodType,
                            String sdate,
                            String edate,
                            SnsSorterEnum sorter,
                            PageRequest pageRequest);
    long countQryAll(SnsCateEnum cate,
                            String val,
                            YnTypeEnum useType,
                            PeriodTypeEnum periodType,
                            String sdate,
                            String edate);

    List<SnsDto> findQryUseY(SnsCateEnum cate, String val);
    long countQryUseY(SnsCateEnum cate, String val);

}