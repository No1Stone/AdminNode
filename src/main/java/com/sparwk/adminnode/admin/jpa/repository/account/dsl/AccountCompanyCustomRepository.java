package com.sparwk.adminnode.admin.jpa.repository.account.dsl;

import com.sparwk.adminnode.admin.biz.v1.account.dto.AccountCompanyLocationDto;
import com.sparwk.adminnode.admin.biz.v1.account.dto.AccountCompanyTypeDto;
import com.sparwk.adminnode.admin.biz.v1.account.dto.CompanyMemberListDto;
import com.sparwk.adminnode.admin.biz.v1.account.dto.CompanyVerifyListDto;
import com.sparwk.adminnode.admin.biz.v1.profile.dto.*;
import com.sparwk.adminnode.admin.jpa.entity.PeriodTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.account.AccountCompanyCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.account.AccountCompanySorterEnum;
import org.springframework.data.domain.PageRequest;

import java.util.List;
import java.util.Optional;

public interface AccountCompanyCustomRepository {
    //회사정보 보기(인증전 후 포함)
    List<CompanyMemberListDto> findQryCompnayMember(AccountCompanyCateEnum cate,
                                                    String val,
                                                    PeriodTypeEnum periodType,
                                                    String sdate,
                                                    String edate,
                                                    AccountCompanySorterEnum sorter,
                                                    PageRequest pageRequest);

    //회사정보 보기 인증만 보기
    List<CompanyVerifyListDto> findQryCompnayVerify(AccountCompanyCateEnum cate,
                                                    String val,
                                                    PeriodTypeEnum periodType,
                                                    String sdate,
                                                    String edate,
                                                    AccountCompanySorterEnum sorter,
                                                    PageRequest pageRequest);

    List<AccountCompanyTypeDto> finQryCompanyType(Long accntId);
    List<AccountCompanyLocationDto> finQryCompanyLocation(Long accntId);
    List<ProfileCompanySnsDto> findQryCompanySns(Long profileId);
    List<ProfileCompanyRosteredDto> findQryCompanyRoster(Long profileId);
    List<ProfileCompanyPartnerDto> findQryCompanyPartner(Long profileId);
    List<ProfileCompanyStudioDto> findQryCompanyStudio(Long profileId);
    ProfileCompanyContactDto findQryCompanyConcat(Long profileId);

    void updateQryCompanyTypeVerifyYn(Long profileId, YnTypeEnum yntype);

}
