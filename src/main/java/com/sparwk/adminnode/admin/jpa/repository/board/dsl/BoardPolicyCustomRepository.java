package com.sparwk.adminnode.admin.jpa.repository.board.dsl;

import com.sparwk.adminnode.admin.biz.v1.board.dto.BoardFaqDto;
import com.sparwk.adminnode.admin.biz.v1.board.dto.BoardPolicyDto;
import com.sparwk.adminnode.admin.jpa.entity.PeriodTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.board.PolicyCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.board.PolicySorterEnum;
import org.springframework.data.domain.PageRequest;

import java.util.List;

public interface BoardPolicyCustomRepository {

    List<BoardPolicyDto> findQryAll(String pcode,
                                    PolicyCateEnum cate,
                                    String val,
                                    YnTypeEnum useType,
                                    PeriodTypeEnum periodType,
                                    String sdate,
                                    String edate,
                                    PolicySorterEnum sorter,
                                    PageRequest pageRequest
    );

    long countQryAll(String pcode,
                                    PolicyCateEnum cate,
                                    String val,
                                    YnTypeEnum useType,
                                    PeriodTypeEnum periodType,
                                    String sdate,
                                    String edate
    );

    List<BoardPolicyDto> findQryUseY(String pcode, PolicyCateEnum cate, String val, PageRequest pageRequest);
    long countQryUseY(String pcode, PolicyCateEnum cate, String val);

    long deleteQryList(Long[] id);
}
