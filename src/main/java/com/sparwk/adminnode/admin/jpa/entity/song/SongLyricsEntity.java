package com.sparwk.adminnode.admin.jpa.entity.song;

import com.sparwk.adminnode.admin.jpa.entity.BaseEntity;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.projects.id.ProjectSongCowriterId;
import com.sparwk.adminnode.admin.jpa.entity.song.id.SongLyricsId;
import lombok.*;

import javax.persistence.*;

@Entity
@Getter
@IdClass(SongLyricsId.class)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_song_lyrics")
public class SongLyricsEntity extends BaseEntity {
    @Id
    @Column(name = "song_id", nullable = true)
    private Long songId;
    @Id
    @Column(name = "song_file_seq", nullable = true)
    private Long songFileSeq;
    @Column(name = "lyrics", nullable = true)
    private String lyrics;
    @Column(name = "lyrics_comt", nullable = true, length = 200)
    private String lyricsComt;
    @Enumerated(EnumType.STRING)
    @Column(name = "explicit_content_yn", nullable = true)
    private YnTypeEnum explicitContentYn;


    @Builder
    SongLyricsEntity(
            Long songId,
            Long songFileSeq,
            String lyrics,
            String lyricsComt,
            YnTypeEnum explicitContentYn
    ) {
        this.songId = songId;
        this.songFileSeq = songFileSeq;
        this.lyrics = lyrics;
        this.lyricsComt = lyricsComt;
        this.explicitContentYn = explicitContentYn;
    }

    public void update(
            String lyrics,
            String lyricsComt
    ) {
        this.lyrics = lyrics;
        this.lyricsComt = lyricsComt;
    }
}
