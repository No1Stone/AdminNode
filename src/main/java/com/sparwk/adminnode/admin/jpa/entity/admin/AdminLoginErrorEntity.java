package com.sparwk.adminnode.admin.jpa.entity.admin;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name="tb_admin_login_error")
public class AdminLoginErrorEntity {

    @Id
    @GeneratedValue(
            generator = "tb_admin_login_error_seq"
    )
    @Column(name = "admin_login_error_seq")
    private Long adminLoginErrorSeq;

    @Column(name = "admin_id")
    private Long adminId;

    @Column(name = "err_cnt")
    private int errCnt;

    @Column(name = "error_last_ip", length = 30)
    private String errorLastIp;

    @Column(name = "error_last_connect")
    private LocalDateTime errorLastConnect;


    @Builder
    public AdminLoginErrorEntity(
            Long adminLoginErrorSeq,
            Long adminId,
            int errCnt,
            String errorLastIp,
            LocalDateTime errorLastConnect
    ) {
        this.adminLoginErrorSeq = adminLoginErrorSeq;
        this.adminId = adminId;
        this.errCnt = errCnt;
        this.errorLastIp = errorLastIp;
        this.errorLastConnect = errorLastConnect;
    }

}
