package com.sparwk.adminnode.admin.jpa.repository.song;

import com.sparwk.adminnode.admin.jpa.entity.profile.ProfileEntity;
import com.sparwk.adminnode.admin.jpa.entity.song.SongEntity;
import com.sparwk.adminnode.admin.jpa.repository.song.dsl.SongCustomRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface SongRepository extends JpaRepository<SongEntity, Long>, SongCustomRepository {

    // 일반 JPQL쿼리, from뒤는 엔티티 명 (소문자로 할 시 에러)
    @Query(value = "update ProfileEntity pe set pe.isniInfo = :isni where pe.profileId = :id")
    @Modifying
    public int updateQryIsniById(Long id, String isni);


    // 일반 JPQL쿼리, from뒤는 엔티티 명 (소문자로 할 시 에러).
    @Query(value = "update SongCowriterEntity sce set sce.rateShare = :rateShare where sce.profileId = :id")
    @Modifying
    public int updateQryshareById(Long id, Float rateShare);

    // 일반 JPQL쿼리, from뒤는 엔티티 명 (소문자로 할 시 에러)
    @Query(value = "update ProfileEntity pe set pe.nroInfo = :nro where pe.profileId = :id")
    @Modifying
    public int updateQryNroById(@Param(value = "id") Long id, @Param(value = "nro") String nro);

    // 일반 JPQL쿼리, from뒤는 엔티티 명 (소문자로 할 시 에러)
    @Query(value = "update ProfileEntity pe set pe.ipnInfo = :ipn where pe.profileId = :id")
    @Modifying
    public int updateQryIpnById(@Param(value = "id") Long id, @Param(value = "ipn") String ipn);
}
