package com.sparwk.adminnode.admin.jpa.entity.codeSeq;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name="tb_code_seq")
public class CodeSeqEntity {

    @Id
    @Column(name = "pcode", length = 3, unique = true)
    private String pcode;

    @Column(name = "seq")
    private int seq;

    @Column(name = "format", length = 6)
    private String format;

}
