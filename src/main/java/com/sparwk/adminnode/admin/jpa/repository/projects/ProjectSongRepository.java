package com.sparwk.adminnode.admin.jpa.repository.projects;

import com.sparwk.adminnode.admin.jpa.entity.projects.ProjectSong;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ProjectSongRepository extends JpaRepository<ProjectSong, Long> {
    Optional<ProjectSong> findBySongId(Long id);
}
