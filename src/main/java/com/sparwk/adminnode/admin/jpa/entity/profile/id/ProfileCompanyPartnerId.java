package com.sparwk.adminnode.admin.jpa.entity.profile.id;

import lombok.Data;

import java.io.Serializable;

@Data
public class ProfileCompanyPartnerId implements Serializable {

    private Long profileId;
    private Long partnerId;

}
