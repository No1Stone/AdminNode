package com.sparwk.adminnode.admin.jpa.entity.timezone;

import lombok.Getter;

@Getter
public enum TimezoneCateEnum {

    All("All"), Timezone("Timezone"), RegUsrName("RegUsrName"), ModUsrName("ModUsrName");

    private String cate;

    TimezoneCateEnum(String cate) {
        this.cate = cate;
    }
}
