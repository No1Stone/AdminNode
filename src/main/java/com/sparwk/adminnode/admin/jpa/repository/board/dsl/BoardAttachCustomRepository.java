package com.sparwk.adminnode.admin.jpa.repository.board.dsl;

import com.sparwk.adminnode.admin.biz.v1.board.dto.BoardAttachDto;

import java.util.List;

public interface BoardAttachCustomRepository {
    List<BoardAttachDto> findQryAttach(Long id, String type);

    long deleteQryAttahList(Long[] id);
}
