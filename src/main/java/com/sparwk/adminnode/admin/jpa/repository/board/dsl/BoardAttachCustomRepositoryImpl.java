package com.sparwk.adminnode.admin.jpa.repository.board.dsl;

import com.querydsl.core.types.Projections;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.sparwk.adminnode.admin.biz.v1.board.dto.BoardAttachDto;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.sparwk.adminnode.admin.jpa.entity.board.QBoardAttachEntity.boardAttachEntity;
import static com.sparwk.adminnode.admin.jpa.entity.board.QBoardFaqEntity.boardFaqEntity;

@Repository
public class BoardAttachCustomRepositoryImpl implements BoardAttachCustomRepository {

    private final JPAQueryFactory jpaQueryFactory;

    public BoardAttachCustomRepositoryImpl(JPAQueryFactory jpaQueryFactory) {
        this.jpaQueryFactory = jpaQueryFactory;
    }

    @Override
    public List<BoardAttachDto> findQryAttach(Long id, String type){
        return jpaQueryFactory.select(Projections.bean(BoardAttachDto.class,
                        boardAttachEntity.attachId,
                        boardAttachEntity.boardType,
                boardAttachEntity.boardId,
                boardAttachEntity.fileNum,
                boardAttachEntity.fileUrl,
                boardAttachEntity.fileName,
                boardAttachEntity.fileSize,
                boardAttachEntity.regDt,
                boardAttachEntity.regUsr,
                boardAttachEntity.modDt,
                boardAttachEntity.modUsr
                        )
                )
                .from(boardAttachEntity)
                .where(boardAttachEntity.boardId.eq(id), boardAttachEntity.boardType.eq(type))
                .orderBy(boardAttachEntity.fileNum.asc())
                .fetch();
    }

    @Override
    public long deleteQryAttahList(Long[] id) {
        return jpaQueryFactory.delete(boardAttachEntity)
                .where(boardAttachEntity.boardId.in(id))
                .execute();
    }


}
