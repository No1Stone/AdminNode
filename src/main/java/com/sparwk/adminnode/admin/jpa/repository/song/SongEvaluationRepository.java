package com.sparwk.adminnode.admin.jpa.repository.song;

import com.sparwk.adminnode.admin.jpa.entity.song.SongEvaluationEntity;
import com.sparwk.adminnode.admin.jpa.repository.song.dsl.SongEvaluationCustomRepository;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SongEvaluationRepository extends JpaRepository<SongEvaluationEntity, Long>, SongEvaluationCustomRepository {
}
