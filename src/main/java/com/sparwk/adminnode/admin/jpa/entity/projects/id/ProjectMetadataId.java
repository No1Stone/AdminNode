package com.sparwk.adminnode.admin.jpa.entity.projects.id;

import lombok.Data;

import java.io.Serializable;

@Data
public class ProjectMetadataId implements Serializable {
    private long projId;
    private String attrTypeCd;
    private String attrDtlCd;
}
