package com.sparwk.adminnode.admin.jpa.entity.anr;

import lombok.Getter;

@Getter
public enum AnrSorterEnum {

    NameAsc("NameAsc"), NameDesc("NameDesc"),
    UseYnAsc("UseYnAsc"), UseYnDesc("UseYnDesc"),
    LastModifiedAsc("LastModifiedAsc"), LastModifiedDesc("LastModifiedDesc"),
    DescriptionAsc("DescriptionAsc"), DescriptionDesc("DescriptionDesc"),
    CreatedAsc("CreatedAsc"), CreatedDesc("CreatedDesc"),
    GroupAsc("GroupAsc"), GroupDesc("GroupDesc"),
    CreaterNameAsc("CreaterNameAsc"), CreaterNameDesc("CreaterNameDesc"),
    ModifierNameAsc("ModifierNameAsc"), ModifierNameDesc("ModifierNameDesc")
    ;

    private String sorter;

    AnrSorterEnum(String sorter) {
        this.sorter = sorter;
    }

}
