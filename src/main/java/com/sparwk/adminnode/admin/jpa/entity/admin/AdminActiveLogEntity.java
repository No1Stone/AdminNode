package com.sparwk.adminnode.admin.jpa.entity.admin;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name="tb_admin_active_log")
public class AdminActiveLogEntity {

    @Id
    @GeneratedValue(
            generator = "tb_admin_active_log_seq"
    )
    @Column(name = "admin_active_log_seq")
    private Long adminActiveLogSeq;

    @Column(name = "active_time")
    private LocalDateTime activeTime;

    @Column(name = "admin_id")
    private Long adminId;

    @Column(name = "menu_name")
    private String menuName;

    @Column(name = "active_type", length = 1)
    private String activeType;

    @Column(name = "contents_id")
    private Long contentsId;

    @Column(name = "active_msg", length = 500)
    private String activeMsg;

    @Builder
    public AdminActiveLogEntity(
            Long adminActiveLogSeq,
            LocalDateTime activeTime,
            Long adminId,
            String menuName,
            String activeType,
            Long contentsId,
            String activeMsg
    ) {
        this.adminActiveLogSeq = adminActiveLogSeq;
        this.activeTime = activeTime;
        this.adminId = adminId;
        this.menuName = menuName;
        this.activeType = activeType;
        this.contentsId = contentsId;
        this.activeMsg = activeMsg;
    }

}
