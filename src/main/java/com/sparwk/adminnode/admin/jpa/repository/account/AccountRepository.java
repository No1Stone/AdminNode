package com.sparwk.adminnode.admin.jpa.repository.account;

import com.sparwk.adminnode.admin.jpa.entity.account.AccountEntity;
import com.sparwk.adminnode.admin.jpa.entity.account.AccountPassportEntity;
import com.sparwk.adminnode.admin.jpa.repository.account.dsl.AccountCompanyCustomRepository;
import com.sparwk.adminnode.admin.jpa.repository.account.dsl.AccountUserCustomRepository;
import com.sparwk.adminnode.admin.jpa.repository.codeSeq.CodeSeqRepository;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountRepository extends JpaRepository<AccountEntity, Long>,
        AccountUserCustomRepository, CodeSeqRepository {

}
