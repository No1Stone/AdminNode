package com.sparwk.adminnode.admin.jpa.entity.popup;

import com.sparwk.adminnode.admin.jpa.entity.BaseEntity;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name="tb_board_popup")
public class PopupEntity extends BaseEntity {

    @Id
    @GeneratedValue(
            generator = "tb_board_popup_seq"
    )
    @Column(name = "popup_seq")
    private Long popupSeq;

    @Column(name = "popup_status_cd", length = 9)
    private String popupStatusCd;

    @Column(name = "popup_title", length = 50)
    private String popupTitle;

    @Column(name = "sdate", length = 19)
    private String sdate;

    @Column(name = "edate", length = 19)
    private String edate;

    @Column(name = "popup_size_width")
    private int popupSizeWidth;

    @Column(name = "popup_size_height")
    private int popupSizeHeight;

    @Column(name = "popup_location_width")
    private int popupLocationWidth;

    @Column(name = "popup_location_height")
    private int popupLocationHeight;

    @Column(name = "content")
    private String content;



    @Builder
    public PopupEntity(
            Long popupSeq,
            String popupStatusCd,
            String popupTitle,
            String sdate,
            String edate,
            int popupSizeWidth,
            int popupSizeHeight,
            int popupLocationWidth,
            int popupLocationHeight,
            String content
                          ) {
        this.popupSeq = popupSeq;
        this.popupStatusCd = popupStatusCd;
        this.popupTitle = popupTitle;
        this.sdate = sdate;
        this.edate = edate;
        this.popupSizeWidth = popupSizeWidth;
        this.popupSizeHeight = popupSizeHeight;
        this.popupLocationWidth = popupLocationWidth;
        this.popupLocationHeight = popupLocationHeight;
        this.content = content;

    }

    public void update(
            String popupStatusCd,
            String popupTitle,
            String sdate,
            String edate,
            int popupSizeWidth,
            int popupSizeHeight,
            int popupLocationWidth,
            int popupLocationHeight,
            String content
                        ) {
        this.popupStatusCd = popupStatusCd;
        this.popupTitle = popupTitle;
        this.sdate = sdate;
        this.edate = edate;
        this.popupSizeWidth = popupSizeWidth;
        this.popupSizeHeight = popupSizeHeight;
        this.popupLocationWidth = popupLocationWidth;
        this.popupLocationHeight = popupLocationHeight;
        this.content = content;
    }


}
