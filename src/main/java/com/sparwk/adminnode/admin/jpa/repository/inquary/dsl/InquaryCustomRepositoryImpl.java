package com.sparwk.adminnode.admin.jpa.repository.inquary.dsl;

import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.sparwk.adminnode.admin.biz.v1.inquary.dto.InquaryDto;
import com.sparwk.adminnode.admin.jpa.entity.inquary.InquaryCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.inquary.InquarySorterEnum;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.sparwk.adminnode.admin.jpa.entity.inquary.QInquaryEntity.inquaryEntity;
import static com.sparwk.adminnode.admin.jpa.entity.admin.QAdminEntity.adminEntity;

@Repository
public class InquaryCustomRepositoryImpl implements InquaryCustomRepository {

    private final JPAQueryFactory jpaQueryFactory;

    public InquaryCustomRepositoryImpl(JPAQueryFactory jpaQueryFactory) {
        this.jpaQueryFactory = jpaQueryFactory;
    }

    @Override
    public List<InquaryDto> findQryAll(InquaryCateEnum cate,
                                       String val,
                                       InquarySorterEnum sorter,
                                       PageRequest pageRequest) {
        return jpaQueryFactory.select(Projections.bean(InquaryDto.class,
                                inquaryEntity.adminLoginLogSeq,
                                inquaryEntity.adminId,
                        adminEntity.adminEmail,
                        adminEntity.fullName,
                                inquaryEntity.connectResult,
                                inquaryEntity.connectDevice,
                                inquaryEntity.connectBrowser,
                                inquaryEntity.connectTime,
                                inquaryEntity.connectOs,
                                inquaryEntity.connectIp
                        )
                )
                .from(inquaryEntity)
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(inquaryEntity.adminId))
                .where(valLike(cate, val))
                .limit(pageRequest.getPageSize())
                .offset(pageRequest.getPageSize() * (pageRequest.getPageNumber() - 1))
                .orderBy(sorterOrder(sorter))
                .fetch();
    }

    @Override
    public long countQryAll(InquaryCateEnum cate,
                            String val,
                            InquarySorterEnum sorter) {
        return jpaQueryFactory.select(
                        inquaryEntity.adminId.count()
                )
                .from(inquaryEntity)
                .where(valLike(cate, val))
                .fetchOne();
    }


    private BooleanExpression valLike(InquaryCateEnum cate, String val) {
        if (cate != null && val != null && val.length() > 0) {
            switch (cate) {
                case All:
                    return adminEntity.adminEmail.toLowerCase().contains(val.toLowerCase())
                            .or(inquaryEntity.connectBrowser.toLowerCase().contains(val.toLowerCase()))
                            .or(inquaryEntity.connectDevice.toLowerCase().contains(val.toLowerCase()))
                            .or(inquaryEntity.connectOs.toLowerCase().contains(val.toLowerCase()))
                            .or(inquaryEntity.connectIp.toLowerCase().contains(val.toLowerCase()));
                case Email:
                    return adminEntity.adminEmail.toLowerCase().contains(val.toLowerCase());
                case Browser:
                    return inquaryEntity.connectBrowser.toLowerCase().contains(val.toLowerCase());
                case Device:
                    return inquaryEntity.connectDevice.toLowerCase().contains(val.toLowerCase());
                case OS:
                    return inquaryEntity.connectOs.toLowerCase().contains(val.toLowerCase());
                case IP:
                    return inquaryEntity.connectIp.toLowerCase().contains(val.toLowerCase());
            }
        }
        return null;
    }

    private OrderSpecifier sorterOrder(InquarySorterEnum sorter) {
        if (sorter == null)
            return inquaryEntity.connectTime.desc();

        if (sorter == InquarySorterEnum.EmailAsc)
            return adminEntity.adminEmail.asc();

        if (sorter == InquarySorterEnum.EmailDesc)
            return adminEntity.adminEmail.desc();

        if (sorter == InquarySorterEnum.BrowserAsc)
            return inquaryEntity.connectBrowser.asc();

        if (sorter == InquarySorterEnum.BrowserDesc)
            return inquaryEntity.connectBrowser.desc();

        if (sorter == InquarySorterEnum.OSAsc)
            return inquaryEntity.connectOs.asc();

        if (sorter == InquarySorterEnum.OSDesc)
            return inquaryEntity.connectOs.desc();

        if (sorter == InquarySorterEnum.DeviceAsc)
            return inquaryEntity.connectDevice.asc();

        if (sorter == InquarySorterEnum.DeviceDesc)
            return inquaryEntity.connectDevice.desc();

        if (sorter == InquarySorterEnum.LoginTimeAsc)
            return inquaryEntity.connectTime.asc();

        if (sorter == InquarySorterEnum.LoginTimeDesc)
            return inquaryEntity.connectTime.desc();

        if (sorter == InquarySorterEnum.IPAsc)
            return inquaryEntity.connectIp.asc();

        if (sorter == InquarySorterEnum.IPDesc)
            return inquaryEntity.connectIp.desc();

        return inquaryEntity.connectTime.desc();
    }

}
