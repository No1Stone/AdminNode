package com.sparwk.adminnode.admin.jpa.entity.passwordPolicy;

import com.sparwk.adminnode.admin.jpa.entity.BaseEntity;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name="tb_admin_password_policy")
public class PasswordPolicyEntity {

    @Id
    @Column(name = "password_policy_seq")
    private Long passwordPolicySeq;

    @Column(name = "password_expire")
    private Integer passwordExpire;

    @Column(name = "password_history")
    private Integer passwordHistory;

    @Column(name = "password_minimum_length")
    private Integer passwordMiNinimumLength;

    @Column(name = "password_complexity_requirement", length = 9)
    private String passwordComplexityRequirement;

    @Column(name = "maximum_invalid_login", length = 9)
    private String maximumInvalidLogin;

    @Column(name = "lockout_effective_period", length = 9)
    private String lockoutEffectivePeriod;

    @LastModifiedBy
    @Column(name="mod_usr")
    private Long modUsr;
    @LastModifiedDate
    @Column(name="mod_dt")
    private LocalDateTime modDt;

    @Builder
    public PasswordPolicyEntity(
            Long passwordPolicySeq,
            Integer passwordExpire,
            Integer passwordHistory,
            Integer passwordMiNinimumLength,
            String passwordComplexityRequirement,
            String maximumInvalidLogin,
            String lockoutEffectivePeriod,
            Long modUsr,
            LocalDateTime modDt
            ) {
        this.passwordPolicySeq = passwordPolicySeq;
        this.passwordExpire = passwordExpire;
        this.passwordHistory = passwordHistory;
        this.passwordMiNinimumLength = passwordMiNinimumLength;
        this.passwordComplexityRequirement = passwordComplexityRequirement;
        this.maximumInvalidLogin = maximumInvalidLogin;
        this.lockoutEffectivePeriod = lockoutEffectivePeriod;
        this.modUsr = modUsr;
        this.modDt = modDt;
    }

   public void update(
           Integer passwordExpire,
           Integer passwordHistory,
           Integer passwordMiNinimumLength,
           String passwordComplexityRequirement,
           String maximumInvalidLogin,
           String lockoutEffectivePeriod
                        ) {
       this.passwordExpire = passwordExpire;
       this.passwordHistory = passwordHistory;
       this.passwordMiNinimumLength = passwordMiNinimumLength;
       this.passwordComplexityRequirement = passwordComplexityRequirement;
       this.maximumInvalidLogin = maximumInvalidLogin;
       this.lockoutEffectivePeriod = lockoutEffectivePeriod;
    }


}
