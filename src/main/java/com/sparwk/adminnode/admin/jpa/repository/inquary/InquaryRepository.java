package com.sparwk.adminnode.admin.jpa.repository.inquary;

import com.sparwk.adminnode.admin.jpa.entity.inquary.InquaryEntity;
import com.sparwk.adminnode.admin.jpa.repository.inquary.dsl.InquaryCustomRepository;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InquaryRepository extends JpaRepository<InquaryEntity, Long>, InquaryCustomRepository {
}
