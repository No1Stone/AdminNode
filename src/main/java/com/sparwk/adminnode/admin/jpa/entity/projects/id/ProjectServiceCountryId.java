package com.sparwk.adminnode.admin.jpa.entity.projects.id;

import lombok.Data;

import java.io.Serializable;

@Data
public class ProjectServiceCountryId implements Serializable {
    private long projId;
    private String serviceCntrCd;
}
