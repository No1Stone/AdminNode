package com.sparwk.adminnode.admin.jpa.repository.account;

import com.sparwk.adminnode.admin.jpa.entity.account.AccountCompanyDetailEntity;
import com.sparwk.adminnode.admin.jpa.repository.account.dsl.AccountCompanyCustomRepository;
import com.sparwk.adminnode.admin.jpa.repository.codeSeq.CodeSeqRepository;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountCompanyRepository extends JpaRepository<AccountCompanyDetailEntity, Long>,
        AccountCompanyCustomRepository, CodeSeqRepository {

}
