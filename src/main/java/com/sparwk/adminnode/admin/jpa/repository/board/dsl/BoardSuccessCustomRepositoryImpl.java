package com.sparwk.adminnode.admin.jpa.repository.board.dsl;

import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.sparwk.adminnode.admin.biz.v1.board.dto.BoardSuccessDto;
import com.sparwk.adminnode.admin.jpa.entity.PeriodTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.admin.QAdminEntity;
import com.sparwk.adminnode.admin.jpa.entity.board.SuccessCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.board.SuccessSorterEnum;
import com.sparwk.adminnode.admin.jpa.entity.commonCode.CommonSorterEnum2;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static com.sparwk.adminnode.admin.jpa.entity.admin.QAdminEntity.adminEntity;
import static com.sparwk.adminnode.admin.jpa.entity.board.QBoardSuccessEntity.boardSuccessEntity;
import static com.sparwk.adminnode.admin.jpa.entity.commonCode.QCommonOrganizationCodeEntity.commonOrganizationCodeEntity;

@Repository
public class BoardSuccessCustomRepositoryImpl implements BoardSuccessCustomRepository {

    private final JPAQueryFactory jpaQueryFactory;

    public BoardSuccessCustomRepositoryImpl(JPAQueryFactory jpaQueryFactory) {
        this.jpaQueryFactory = jpaQueryFactory;
    }

    @Override
    public List<BoardSuccessDto> findQryAll(SuccessCateEnum cate,
                                         String val,
                                         YnTypeEnum useType,
                                         PeriodTypeEnum periodType,
                                         String sdate,
                                         String edate,
                                         SuccessSorterEnum sorter,
                                         PageRequest pageRequest) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(Projections.bean(BoardSuccessDto.class,

                        boardSuccessEntity.successId,
                        boardSuccessEntity.name,
                        boardSuccessEntity.role,
                        boardSuccessEntity.genre,
                        boardSuccessEntity.content,
                        boardSuccessEntity.useType,
                        boardSuccessEntity.imageUrl,
                        boardSuccessEntity.regUsr,
                        adminEntity.fullName.as("regUsrName"),
                        boardSuccessEntity.regDt,
                        boardSuccessEntity.modUsr,
                        adminEntity2.fullName.as("modUsrName"),
                        boardSuccessEntity.modDt
                        )
                )
                .from(boardSuccessEntity)
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(boardSuccessEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(boardSuccessEntity.modUsr))
                .where(valLike(cate, val), useTypeEq(useType), termBetween(periodType, sdate, edate))
                .limit(pageRequest.getPageSize())
                .offset(pageRequest.getPageSize() * (pageRequest.getPageNumber() - 1))
                .orderBy(sorterOrder(sorter))
                .fetch();
    }

    @Override
    public long countQryAll(SuccessCateEnum cate,
                                            String val,
                                            YnTypeEnum useType,
                                            PeriodTypeEnum periodType,
                                            String sdate,
                                            String edate) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(
                                boardSuccessEntity.successId.count()
                )
                .from(boardSuccessEntity)
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(boardSuccessEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(boardSuccessEntity.modUsr))
                .where(valLike(cate, val), useTypeEq(useType), termBetween(periodType, sdate, edate))
                .fetchOne();
    }

    @Override
    public List<BoardSuccessDto> findQryUseY() {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(Projections.bean(BoardSuccessDto.class,

                                boardSuccessEntity.successId,
                                boardSuccessEntity.name,
                                boardSuccessEntity.role,
                                boardSuccessEntity.genre,
                                boardSuccessEntity.content,
                                boardSuccessEntity.useType,
                                boardSuccessEntity.imageUrl,
                                boardSuccessEntity.regUsr,
                                adminEntity.fullName.as("regUsrName"),
                                boardSuccessEntity.regDt,
                                boardSuccessEntity.modUsr,
                                adminEntity2.fullName.as("modUsrName"),
                                boardSuccessEntity.modDt
                        )
                )
                .from(boardSuccessEntity)
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(boardSuccessEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(boardSuccessEntity.modUsr))
                .where(useTypeEq(YnTypeEnum.Y))
                .limit(1)
                .offset(0)
                .orderBy(sorterOrder(SuccessSorterEnum.CreatedDesc))
                .fetch();
    }

    @Override
    public long countQryUseY() {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(
                                boardSuccessEntity.successId.count()
                )
                .from(boardSuccessEntity)
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(boardSuccessEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(boardSuccessEntity.modUsr))
                .where(useTypeEq(YnTypeEnum.Y))
                .fetchOne();
    }

    @Override
    public long deleteQryList(Long[] id) {
        return jpaQueryFactory.delete(boardSuccessEntity)
                .where(boardSuccessEntity.successId.in(id))
                .execute();
    }

    private BooleanExpression valLike(SuccessCateEnum cate, String val) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        if (cate != null && val != null && val.length() > 0) {
            switch (cate) {
                case All:
                    return boardSuccessEntity.name.toLowerCase().contains(val.toLowerCase())
                            .or(boardSuccessEntity.role.toLowerCase().contains(val.toLowerCase()))
                            .or(boardSuccessEntity.genre.toLowerCase().contains(val.toLowerCase()));
                case Name:
                    return boardSuccessEntity.name.toLowerCase().contains(val.toLowerCase());
                case Title:
                    return boardSuccessEntity.genre.toLowerCase().contains(val.toLowerCase());
                case RegUsrName:
                    return adminEntity.fullName.toLowerCase().contains(val.toLowerCase());
                case ModUsrName:
                    return adminEntity2.fullName.toLowerCase().contains(val.toLowerCase());
            }
        }
        return null;
    }

    private BooleanExpression idEq(Long id) {
        if (id != null)
            return boardSuccessEntity.successId.eq(id);
        return null;
    }

    private BooleanExpression useTypeEq(YnTypeEnum useType) {
        if (useType != null)
            return boardSuccessEntity.useType.eq(useType);
        return null;
    }

    private BooleanExpression termBetween(PeriodTypeEnum periodType, String sdate, String edate) {

        if (sdate != null && edate != null && periodType != null) {

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
            LocalDateTime searchStartDate = LocalDateTime.parse(sdate + " 00:00:00.000", formatter);
            LocalDateTime searchEndDate = LocalDateTime.parse(edate + " 23:59:59.999", formatter);

            switch (periodType) {
                case Created:
                    return boardSuccessEntity.regDt.between(searchStartDate, searchEndDate);
                case LastModified:
                    return boardSuccessEntity.modDt.between(searchStartDate, searchEndDate);
                default:
                    return null;
            }
        }
        return null;
    }

    private OrderSpecifier sorterOrder(SuccessSorterEnum sorter) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        if (sorter == null)
            return boardSuccessEntity.regDt.desc();

        if (sorter == SuccessSorterEnum.NameAsc)
            return boardSuccessEntity.name.asc();

        if (sorter == SuccessSorterEnum.NameDesc)
            return boardSuccessEntity.name.desc();

        if (sorter == SuccessSorterEnum.UseAsc)
            return boardSuccessEntity.useType.asc();

        if (sorter == SuccessSorterEnum.UseDesc)
            return boardSuccessEntity.useType.desc();

        if (sorter == SuccessSorterEnum.LastModifiedAsc)
            return boardSuccessEntity.modDt.asc();

        if (sorter == SuccessSorterEnum.LastModifiedDesc)
            return boardSuccessEntity.modDt.desc();

        if (sorter == SuccessSorterEnum.CreatedAsc)
            return boardSuccessEntity.regDt.asc();

        if (sorter == SuccessSorterEnum.CreatedDesc)
            return boardSuccessEntity.regDt.desc();

        if (sorter == SuccessSorterEnum.TitleAsc)
            return boardSuccessEntity.genre.asc();

        if (sorter == SuccessSorterEnum.TitleDesc)
            return boardSuccessEntity.genre.desc();

        if (sorter == SuccessSorterEnum.CreaterNameAsc)
            return adminEntity.fullName.asc();

        if (sorter == SuccessSorterEnum.CreaterNameDesc)
            return adminEntity.fullName.desc();

        if (sorter == SuccessSorterEnum.ModifierNameAsc)
            return adminEntity2.fullName.asc();

        if (sorter == SuccessSorterEnum.ModifierNameDesc)
            return adminEntity2.fullName.desc();

        return boardSuccessEntity.regDt.desc();
    }

}
