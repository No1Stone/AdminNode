package com.sparwk.adminnode.admin.jpa.repository.board;

import com.sparwk.adminnode.admin.jpa.entity.board.BoardQnaEntity;
import com.sparwk.adminnode.admin.jpa.repository.board.dsl.BoardQnaCustomRepository;
import com.sparwk.adminnode.admin.jpa.repository.codeSeq.CodeSeqRepository;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BoardQnaRepository extends JpaRepository<BoardQnaEntity, Long>, BoardQnaCustomRepository, CodeSeqRepository {

}
