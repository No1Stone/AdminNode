package com.sparwk.adminnode.admin.jpa.repository.song.dsl;

import com.sparwk.adminnode.admin.biz.v1.projects.dto.ProjectMetadataDto;
import com.sparwk.adminnode.admin.biz.v1.song.dto.SongLyricsLangDto;
import com.sparwk.adminnode.admin.biz.v1.song.dto.SongMetadataDto;

import java.util.List;

public interface SongCustomRepository {

    List<SongMetadataDto> findQrySongMeta(String pcode, Long projId);

}
