package com.sparwk.adminnode.admin.jpa.entity.song.id;

import lombok.Data;

import java.io.Serializable;

@Data
public class SongMetaId implements Serializable {
    private long songId;
    private String attrTypeCd;
    private String attrDtlCd;
}
