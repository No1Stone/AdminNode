package com.sparwk.adminnode.admin.jpa.entity.languagePack;

import lombok.Getter;

@Getter
public enum LanguagePackCateEnum {

    All("All"),
    LanguagePack("LanguagePack"),
    NativeLanguage("NativeLanguage"),
    RegUsrName("RegUsrName"),
    ModUsrName("ModUsrName")
    ;

    private String cate;

    LanguagePackCateEnum(String cate) {
        this.cate = cate;
    }
}
