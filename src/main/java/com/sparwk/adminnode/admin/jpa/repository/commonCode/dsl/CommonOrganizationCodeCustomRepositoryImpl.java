package com.sparwk.adminnode.admin.jpa.repository.commonCode.dsl;

import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.sparwk.adminnode.admin.biz.v1.commonCode.dto.CommonOrganizationCodeDto;
import com.sparwk.adminnode.admin.biz.v1.commonCode.dto.CommonOrganizationCodeExcelDto;
import com.sparwk.adminnode.admin.jpa.entity.PeriodTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.admin.QAdminEntity;
import com.sparwk.adminnode.admin.jpa.entity.commonCode.CommonCateEnum3;
import com.sparwk.adminnode.admin.jpa.entity.commonCode.CommonSorterEnum;
import com.sparwk.adminnode.admin.jpa.entity.commonCode.CommonSorterEnum2;
import com.sparwk.adminnode.admin.jpa.entity.commonCode.CommonSorterEnum3;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static com.sparwk.adminnode.admin.jpa.entity.admin.QAdminEntity.adminEntity;
import static com.sparwk.adminnode.admin.jpa.entity.commonCode.QCommonCodeEntity.commonCodeEntity;
import static com.sparwk.adminnode.admin.jpa.entity.commonCode.QCommonDetailCodeEntity.commonDetailCodeEntity;
import static com.sparwk.adminnode.admin.jpa.entity.commonCode.QCommonOrganizationCodeEntity.commonOrganizationCodeEntity;
import static com.sparwk.adminnode.admin.jpa.entity.country.QCountryEntity.countryEntity;

@Repository
public class CommonOrganizationCodeCustomRepositoryImpl implements CommonOrganizationCodeCustomRepository {

    private final JPAQueryFactory jpaQueryFactory;

    public CommonOrganizationCodeCustomRepositoryImpl(JPAQueryFactory jpaQueryFactory) {
        this.jpaQueryFactory = jpaQueryFactory;
    }

    @Override
    public List<CommonOrganizationCodeDto> findQryAll(String pcode,
                                                String code,
                                                CommonCateEnum3 cate,
                                                String val,
                                                YnTypeEnum useType,
                                                PeriodTypeEnum periodType,
                                                String sdate,
                                                String edate,
                                                CommonSorterEnum2 sorter,
                                                PageRequest pageRequest) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(Projections.bean(CommonOrganizationCodeDto.class,
                                commonCodeEntity.code.as("pcode"),
                                commonCodeEntity.val.as("pcodeVal"),
                                commonOrganizationCodeEntity.commonOrganizationCodeSeq,
                                commonOrganizationCodeEntity.dcode,
                                commonOrganizationCodeEntity.val,
                                commonOrganizationCodeEntity.countryCd,
                                countryEntity.country,
                                commonOrganizationCodeEntity.useType,
                                commonOrganizationCodeEntity.popularType,
                                commonOrganizationCodeEntity.description,
                                commonOrganizationCodeEntity.sortIndex,
                                commonOrganizationCodeEntity.hit,
                                commonOrganizationCodeEntity.regUsr,
                                adminEntity.fullName.as("regUsrName"),
                                commonOrganizationCodeEntity.regDt,
                                commonOrganizationCodeEntity.modUsr,
                                adminEntity2.fullName.as("modUsrName"),
                                commonOrganizationCodeEntity.modDt
                        )
                )
                .from(commonOrganizationCodeEntity)
                .join(commonOrganizationCodeEntity.commonCodeEntity, commonCodeEntity)
                .leftJoin(countryEntity).on(countryEntity.countryCd.eq(commonOrganizationCodeEntity.countryCd))
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(commonOrganizationCodeEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(commonOrganizationCodeEntity.modUsr))
                .where(commonPcodeEq(pcode), codeEq(code), valLike(cate, val), useTypeEq(useType), termBetween(periodType, sdate, edate))
                .limit(pageRequest.getPageSize())
                .offset(pageRequest.getPageSize() * (pageRequest.getPageNumber() - 1))
                .orderBy(sorterOrder(sorter))
                .fetch();
    }

    @Override
    public long countQryAll(String pcode,
                                                      String code,
                                                      CommonCateEnum3 cate,
                                                      String val,
                                                      YnTypeEnum useType,
                                                      PeriodTypeEnum periodType,
                                                      String sdate,
                                                      String edate) {
        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(
                                commonCodeEntity.commonCodeSeq.count()
                )
                .from(commonOrganizationCodeEntity)
                .join(commonOrganizationCodeEntity.commonCodeEntity, commonCodeEntity)
                .leftJoin(countryEntity).on(countryEntity.countryCd.eq(commonOrganizationCodeEntity.countryCd))
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(commonOrganizationCodeEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(commonOrganizationCodeEntity.modUsr))
                .where(commonPcodeEq(pcode), codeEq(code), valLike(cate, val), useTypeEq(useType), termBetween(periodType, sdate, edate))
                .fetchOne();
    }

    @Override
    public List<CommonOrganizationCodeDto> findQryUseY(String pcode, String code, CommonCateEnum3 cate, String val) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(Projections.bean(CommonOrganizationCodeDto.class,
                                commonCodeEntity.code.as("pcode"),
                                commonCodeEntity.val.as("pcodeVal"),
                                commonOrganizationCodeEntity.commonOrganizationCodeSeq,
                                commonOrganizationCodeEntity.dcode,
                                commonOrganizationCodeEntity.val,
                                commonOrganizationCodeEntity.countryCd,
                                countryEntity.country,
                                commonOrganizationCodeEntity.useType,
                                commonOrganizationCodeEntity.popularType,
                                commonOrganizationCodeEntity.description,
                                commonOrganizationCodeEntity.sortIndex,
                                commonOrganizationCodeEntity.hit,
                                commonOrganizationCodeEntity.regUsr,
                                adminEntity.fullName.as("regUsrName"),
                                commonOrganizationCodeEntity.regDt,
                                commonOrganizationCodeEntity.modUsr,
                                adminEntity2.fullName.as("modUsrName"),
                                commonOrganizationCodeEntity.modDt
                        )
                )
                .from(commonOrganizationCodeEntity)
                .join(commonOrganizationCodeEntity.commonCodeEntity, commonCodeEntity)
                .leftJoin(countryEntity).on(countryEntity.countryCd.eq(commonOrganizationCodeEntity.countryCd))
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(commonOrganizationCodeEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(commonOrganizationCodeEntity.modUsr))
                .where(commonPcodeEq(pcode), codeEq(code), valLike(cate, val), useTypeEq(YnTypeEnum.Y))
                .orderBy(sorterOrder(CommonSorterEnum2.NameAsc))
                .fetch();
    }

    @Override
    public long countQryUseY(String pcode, String code, CommonCateEnum3 cate, String val) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(
                        commonCodeEntity.commonCodeSeq.count()
                )
                .from(commonOrganizationCodeEntity)
                .join(commonOrganizationCodeEntity.commonCodeEntity, commonCodeEntity)
                .leftJoin(countryEntity).on(countryEntity.countryCd.eq(commonOrganizationCodeEntity.countryCd))
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(commonOrganizationCodeEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(commonOrganizationCodeEntity.modUsr))
                .where(commonPcodeEq(pcode), codeEq(code), valLike(cate, val), useTypeEq(YnTypeEnum.Y))
                .fetchOne();
    }

    @Override
    public List<CommonOrganizationCodeDto> findPopularUseY(String pcode, String code, CommonCateEnum3 cate, String val) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(Projections.bean(CommonOrganizationCodeDto.class,
                                commonCodeEntity.code.as("pcode"),
                                commonCodeEntity.val.as("pcodeVal"),
                                commonOrganizationCodeEntity.commonOrganizationCodeSeq,
                                commonOrganizationCodeEntity.dcode,
                                commonOrganizationCodeEntity.val,
                                commonOrganizationCodeEntity.countryCd,
                                countryEntity.country,
                                commonOrganizationCodeEntity.useType,
                                commonOrganizationCodeEntity.popularType,
                                commonOrganizationCodeEntity.description,
                                commonOrganizationCodeEntity.sortIndex,
                                commonOrganizationCodeEntity.hit,
                                commonOrganizationCodeEntity.regUsr,
                                adminEntity.fullName.as("regUsrName"),
                                commonOrganizationCodeEntity.regDt,
                                commonOrganizationCodeEntity.modUsr,
                                adminEntity2.fullName.as("modUsrName"),
                                commonOrganizationCodeEntity.modDt
                        )
                )
                .from(commonOrganizationCodeEntity)
                .join(commonOrganizationCodeEntity.commonCodeEntity, commonCodeEntity)
                .leftJoin(countryEntity).on(countryEntity.countryCd.eq(commonOrganizationCodeEntity.countryCd))
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(commonOrganizationCodeEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(commonOrganizationCodeEntity.modUsr))
                .where(commonPcodeEq(pcode), codeEq(code), valLike(cate, val), popularTypeEq(YnTypeEnum.Y))
                .orderBy(sorterOrder(CommonSorterEnum2.NameAsc))
                .fetch();
    }

    @Override
    public long countPopularUseY(String pcode, String code, CommonCateEnum3 cate, String val) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(
                        commonCodeEntity.commonCodeSeq.count()
                )
                .from(commonOrganizationCodeEntity)
                .join(commonOrganizationCodeEntity.commonCodeEntity, commonCodeEntity)
                .leftJoin(countryEntity).on(countryEntity.countryCd.eq(commonOrganizationCodeEntity.countryCd))
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(commonOrganizationCodeEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(commonOrganizationCodeEntity.modUsr))
                .where(commonPcodeEq(pcode), codeEq(code), valLike(cate, val), popularTypeEq(YnTypeEnum.Y))
                .fetchOne();
    }

    @Override
    public List<CommonOrganizationCodeExcelDto> findExcelList(String pcode) {

        return jpaQueryFactory.select(Projections.bean(CommonOrganizationCodeExcelDto.class,
                                commonCodeEntity.code.as("pcode"),
                                commonCodeEntity.val.as("pcodeVal"),
                                commonOrganizationCodeEntity.commonOrganizationCodeSeq,
                                commonOrganizationCodeEntity.dcode,
                                commonOrganizationCodeEntity.val,
                                commonOrganizationCodeEntity.countryCd,
                                countryEntity.country,
                                commonOrganizationCodeEntity.useType,
                                commonOrganizationCodeEntity.popularType,
                                commonOrganizationCodeEntity.description
                        )
                )
                .from(commonOrganizationCodeEntity)
                .join(commonOrganizationCodeEntity.commonCodeEntity, commonCodeEntity)
                .leftJoin(countryEntity).on(countryEntity.countryCd.eq(commonOrganizationCodeEntity.countryCd))
                .where(commonPcodeEq(pcode))
                .orderBy(sorterOrder(CommonSorterEnum2.DcodeAsc))
                .fetch();
    }

    public void updateHit(Long id) {
        if (id > 0) {
            jpaQueryFactory.update(commonOrganizationCodeEntity)
                    .set(commonOrganizationCodeEntity.hit, commonOrganizationCodeEntity.hit.add(1))
                    .where(idEq(id))
                    .execute();
        }
    }

    @Override
    public void deleteQryId(Long id) {

        if (id > 0) {
            jpaQueryFactory.delete(commonOrganizationCodeEntity)
                    .where(idEq(id))
                    .execute();
        }
    }

    private BooleanExpression commonPcodeEq(String code) {
        if (code != null && code.length() > 0)
            return commonOrganizationCodeEntity.commonCodeEntity.code.eq(code);
        return null;
    }

    private BooleanExpression codeEq(String code) {
        if (code != null && code.length() > 0)
            return commonOrganizationCodeEntity.dcode.eq(code);
        return null;
    }

    private BooleanExpression valLike(CommonCateEnum3 cate, String val) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        if (cate != null && val != null && val.length() > 0) {
            switch (cate) {
                case All:
                    return commonOrganizationCodeEntity.val.toLowerCase().contains(val.toLowerCase())
                            .or(commonOrganizationCodeEntity.description.toLowerCase().contains(val.toLowerCase()));
                case Value:
                    return commonOrganizationCodeEntity.val.toLowerCase().contains(val.toLowerCase());
                case Country:
                    return countryEntity.country.toLowerCase().contains(val.toLowerCase());
                case Description:
                    return commonOrganizationCodeEntity.description.toLowerCase().contains(val.toLowerCase());
                case RegUsrName:
                    return adminEntity.fullName.toLowerCase().contains(val.toLowerCase());
                case ModUsrName:
                    return adminEntity2.fullName.toLowerCase().contains(val.toLowerCase());
            }
        }
        return null;
    }

    private BooleanExpression idEq(Long id) {
        if (id != null)
            return commonOrganizationCodeEntity.commonOrganizationCodeSeq.eq(id);
        return null;
    }

    private BooleanExpression useTypeEq(YnTypeEnum useType) {
        if (useType != null)
            return commonOrganizationCodeEntity.useType.eq(useType);
        return null;
    }

    private BooleanExpression popularTypeEq(YnTypeEnum popularType) {
        if (popularType != null)
            return commonOrganizationCodeEntity.popularType.eq(popularType);
        return null;
    }

    private BooleanExpression termBetween(PeriodTypeEnum periodType, String sdate, String edate) {

        if (sdate != null && edate != null && periodType != null) {

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
            LocalDateTime searchStartDate = LocalDateTime.parse(sdate + " 00:00:00.000", formatter);
            LocalDateTime searchEndDate = LocalDateTime.parse(edate + " 23:59:59.999", formatter);

            switch (periodType) {
                case Created:
                    return commonOrganizationCodeEntity.regDt.between(searchStartDate, searchEndDate);
                case LastModified:
                    return commonOrganizationCodeEntity.modDt.between(searchStartDate, searchEndDate);
                default:
                    return null;
            }
        }
        return null;
    }

    private OrderSpecifier sorterOrder(CommonSorterEnum2 sorter) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        if (sorter == null)
            return commonOrganizationCodeEntity.regDt.desc();

        if (sorter == CommonSorterEnum2.NameAsc)
            return commonOrganizationCodeEntity.val.asc();

        if (sorter == CommonSorterEnum2.NameDesc)
            return commonOrganizationCodeEntity.val.desc();

        if (sorter == CommonSorterEnum2.UseYnAsc)
            return commonOrganizationCodeEntity.useType.asc();

        if (sorter == CommonSorterEnum2.UseYnDesc)
            return commonOrganizationCodeEntity.useType.desc();

        if (sorter == CommonSorterEnum2.LastModifiedAsc)
            return commonOrganizationCodeEntity.modDt.asc();

        if (sorter == CommonSorterEnum2.LastModifiedDesc)
            return commonOrganizationCodeEntity.modDt.desc();

        if (sorter == CommonSorterEnum2.CreatedAsc)
            return commonOrganizationCodeEntity.regDt.asc();

        if (sorter == CommonSorterEnum2.CreatedDesc)
            return commonOrganizationCodeEntity.regDt.desc();

        if (sorter == CommonSorterEnum2.DcodeAsc)
            return commonOrganizationCodeEntity.dcode.asc();

        if (sorter == CommonSorterEnum2.DcodeDesc)
            return commonOrganizationCodeEntity.dcode.desc();

        if (sorter == CommonSorterEnum2.DescriptionAsc)
            return commonOrganizationCodeEntity.description.asc();

        if (sorter == CommonSorterEnum2.DescriptionDesc)
            return commonOrganizationCodeEntity.description.desc();

        if (sorter == CommonSorterEnum2.CreaterNameAsc)
            return adminEntity.fullName.asc();

        if (sorter == CommonSorterEnum2.CreaterNameDesc)
            return adminEntity.fullName.desc();

        if (sorter == CommonSorterEnum2.ModifierNameAsc)
            return adminEntity2.fullName.asc();

        if (sorter == CommonSorterEnum2.ModifierNameDesc)
            return adminEntity2.fullName.desc();

        return commonOrganizationCodeEntity.regDt.desc();
    }

}
