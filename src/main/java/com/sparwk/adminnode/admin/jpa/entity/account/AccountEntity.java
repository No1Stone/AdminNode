package com.sparwk.adminnode.admin.jpa.entity.account;


import com.sparwk.adminnode.admin.jpa.entity.BaseEntity;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_account")
@DynamicUpdate
@TableGenerator(
        name = "tb_account_seq_generator",
        table = "tb_account_sequences",
        pkColumnValue = "tb_account_seq",
        allocationSize = 1,
        initialValue = 1000000
)
public class AccountEntity extends BaseEntity {
    @Id
    @GeneratedValue(
            strategy = GenerationType.TABLE,
            generator = "tb_account_seq_generator")
    @Column(name = "accnt_id")
    private Long accntId;

    @Column(name = "accnt_repository_id", nullable = true)
    private Long accntRepositoryId;

    @Column(name = "accnt_type_cd", nullable = true, length = 9)
    private String accntTypeCd;

    @Column(name = "accnt_email", nullable = true, length = 100, unique = true)
    private String accntEmail;

    @Column(name = "accnt_pass", nullable = true, length = 200)
    private String accntPass;

    @Column(name = "country_cd", nullable = true, length = 9)
    private String countryCd;

    @Column(name = "phone_number", nullable = true, length = 20)
    private String phoneNumber;

    @Enumerated(EnumType.STRING)
    @Column(name = "accnt_login_active_yn", nullable = true, length = 1)
    private YnTypeEnum accntLoginActiveYn;

    @Column(name = "last_use_profileId")
    private Long lastUseProfileId;

    @Enumerated(EnumType.STRING)
    @Column(name = "verify_yn", nullable = true, length = 1)
    private YnTypeEnum verifyYn;

    @Enumerated(EnumType.STRING)
    @Column(name = "verify_phone_yn", nullable = true, length = 1)
    private YnTypeEnum verifyPhoneYn;

    @Enumerated(EnumType.STRING)
    @Column(name = "use_yn", nullable = true, length = 1)
    private YnTypeEnum useYn;

    @Builder
    AccountEntity(
            Long accntId,
            Long accntRepositoryId,
            String accntTypeCd,
            String accntEmail,
            String accntPass,
            String countryCd,
            String phoneNumber,
            YnTypeEnum accntLoginActiveYn,
            YnTypeEnum verifyYn,
            YnTypeEnum verifyPhoneYn,
            YnTypeEnum useYn
    ) {
        this.accntId = accntId;
        this.accntRepositoryId = accntRepositoryId;
        this.accntTypeCd = accntTypeCd;
        this.accntEmail = accntEmail;
        this.accntPass = accntPass;
        this.countryCd = countryCd;
        this.phoneNumber = phoneNumber;
        this.accntLoginActiveYn = accntLoginActiveYn;
        this.verifyYn = verifyYn;
        this.verifyPhoneYn = verifyPhoneYn;
        this.useYn = useYn;
    }

//    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = AccountCompanyDetail.class)
//    @JoinColumn(name = "accnt_id", referencedColumnName = "accnt_id")
//    private AccountCompanyDetail accountCompanyDetail;
//
//    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = AccountCompanyLocation.class)
//    @JoinColumn(name = "accnt_id", referencedColumnName = "accnt_id")
//    private AccountCompanyLocation accountCompanyLocation;
//
//    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = AccountCompanyType.class)
//    @JoinColumn(name = "accnt_id", referencedColumnName = "accnt_id")
//    private AccountCompanyType accountCompanyType;
//
//    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = AccountPassport.class)
//    @JoinColumn(name = "accnt_id", referencedColumnName = "accnt_id")
//    private AccountPassport accountPassport;
//
//    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = AccountSnsToken.class)
//    @JoinColumn(name = "accnt_id", referencedColumnName = "accnt_id")
//    private List<AccountSnsToken> accountSnsToken;

}
