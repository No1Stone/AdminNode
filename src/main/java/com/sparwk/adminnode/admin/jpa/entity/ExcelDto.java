package com.sparwk.adminnode.admin.jpa.entity;

import java.util.List;

public interface ExcelDto {

    List<String> mapToList();
}
