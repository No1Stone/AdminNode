package com.sparwk.adminnode.admin.jpa.repository.board;

import com.sparwk.adminnode.admin.jpa.entity.PeriodTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.board.BoardCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.board.BoardFaqEntity;
import com.sparwk.adminnode.admin.jpa.repository.board.dsl.BoardFaqCustomRepository;
import com.sparwk.adminnode.admin.jpa.repository.codeSeq.CodeSeqRepository;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BoardFaqRepository extends JpaRepository<BoardFaqEntity, Long>, BoardFaqCustomRepository, CodeSeqRepository {
}
