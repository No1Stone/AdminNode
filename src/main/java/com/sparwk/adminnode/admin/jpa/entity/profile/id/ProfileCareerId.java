package com.sparwk.adminnode.admin.jpa.entity.profile.id;

import lombok.Data;

import java.io.Serializable;

@Data
public class ProfileCareerId  implements Serializable {

    private Long profileId;
    private String careerOrganizationNm;

}
