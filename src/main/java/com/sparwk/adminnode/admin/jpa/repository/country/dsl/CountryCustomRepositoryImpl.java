package com.sparwk.adminnode.admin.jpa.repository.country.dsl;

import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.sparwk.adminnode.admin.biz.v1.country.dto.CountryDto;
import com.sparwk.adminnode.admin.biz.v1.country.dto.CountryExcelDto;
import com.sparwk.adminnode.admin.jpa.entity.PeriodTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.admin.QAdminEntity;
import com.sparwk.adminnode.admin.jpa.entity.commonCode.CommonSorterEnum2;
import com.sparwk.adminnode.admin.jpa.entity.country.CountryCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.country.CountrySorterEnum;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static com.sparwk.adminnode.admin.jpa.entity.admin.QAdminEntity.adminEntity;
import static com.sparwk.adminnode.admin.jpa.entity.commonCode.QCommonOrganizationCodeEntity.commonOrganizationCodeEntity;
import static com.sparwk.adminnode.admin.jpa.entity.continent.QContinentEntity.continentEntity;
import static com.sparwk.adminnode.admin.jpa.entity.country.QCountryEntity.countryEntity;

@Repository
public class CountryCustomRepositoryImpl implements CountryCustomRepository {

    private final JPAQueryFactory jpaQueryFactory;

    public CountryCustomRepositoryImpl(JPAQueryFactory jpaQueryFactory) {
        this.jpaQueryFactory = jpaQueryFactory;
    }

    @Override
    public List<CountryDto> findQryAll(CountryCateEnum cate,
                                       String val,
                                       YnTypeEnum useType,
                                       PeriodTypeEnum periodType,
                                       String sdate,
                                       String edate,
                                       CountrySorterEnum sorter,
                                       PageRequest pageRequest) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(Projections.bean(CountryDto.class,
                                continentEntity.continent.as("continentCodeVal"),
                                countryEntity.countrySeq,
                                countryEntity.country,
                                countryEntity.countryCd,
                                countryEntity.continentCode,
                                countryEntity.iso2,
                                countryEntity.iso3,
                                countryEntity.nmr,
                                countryEntity.dial,
                                countryEntity.useType,
                                countryEntity.description,
                                countryEntity.regUsr,
                                adminEntity.fullName.as("regUsrName"),
                                countryEntity.regDt,
                                countryEntity.modUsr,
                                adminEntity2.fullName.as("modUsrName"),
                                countryEntity.modDt
                        )
                )
                .from(countryEntity)
                .join(countryEntity.continentEntity, continentEntity)
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(countryEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(countryEntity.modUsr))
                .where(valLike(cate, val), useTypeEq(useType), termBetween(periodType, sdate, edate))
                .limit(pageRequest.getPageSize())
                .offset(pageRequest.getPageSize() * (pageRequest.getPageNumber() - 1))
                .orderBy(sorterOrder(sorter))
                .fetch();
    }

    @Override
    public long countQryAll(           CountryCateEnum cate,
                                       String val,
                                       YnTypeEnum useType,
                                       PeriodTypeEnum periodType,
                                       String sdate,
                                       String edate) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(
                                continentEntity.continent.count()
                )
                .from(countryEntity)
                .join(countryEntity.continentEntity, continentEntity)
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(countryEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(countryEntity.modUsr))
                .where(valLike(cate, val), useTypeEq(useType), termBetween(periodType, sdate, edate))
                .fetchOne();
    }

    @Override
    public List<CountryDto> findQryUseY(CountryCateEnum cate, String val) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(Projections.bean(CountryDto.class,
                                continentEntity.continent.as("continentCodeVal"),
                                countryEntity.countrySeq,
                                countryEntity.country,
                                countryEntity.countryCd,
                                countryEntity.continentCode,
                                countryEntity.iso2,
                                countryEntity.iso3,
                                countryEntity.nmr,
                                countryEntity.dial,
                                countryEntity.useType,
                                countryEntity.description,
                                countryEntity.regUsr,
                                adminEntity.fullName.as("regUsrName"),
                                countryEntity.regDt,
                                countryEntity.modUsr,
                                adminEntity2.fullName.as("modUsrName"),
                                countryEntity.modDt
                        )
                )
                .from(countryEntity)
                .join(countryEntity.continentEntity, continentEntity)
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(countryEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(countryEntity.modUsr))
                .where(valLike(cate, val), useTypeEq(YnTypeEnum.Y))
                .orderBy(sorterOrder(CountrySorterEnum.CountryAsc))
                .fetch();
    }

    @Override
    public long countQryUseY(CountryCateEnum cate, String val) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(
                                continentEntity.continent.count()
                )
                .from(countryEntity)
                .join(countryEntity.continentEntity, continentEntity)
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(countryEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(countryEntity.modUsr))
                .where(valLike(cate, val), useTypeEq(YnTypeEnum.Y))
                .fetchOne();
    }

    @Override
    public List<CountryExcelDto> findExcelList() {

        return jpaQueryFactory.select(Projections.bean(CountryExcelDto.class,
                                continentEntity.continent.as("continentCodeVal"),
                                countryEntity.countrySeq,
                                countryEntity.country,
                                countryEntity.countryCd,
                                countryEntity.continentCode,
                                countryEntity.iso2,
                                countryEntity.iso3,
                                countryEntity.nmr,
                                countryEntity.dial,
                                countryEntity.useType,
                                countryEntity.description
                        )
                )
                .from(countryEntity)
                .join(countryEntity.continentEntity, continentEntity)
                .orderBy(sorterOrder(CountrySorterEnum.ContinentAsc), sorterOrder(CountrySorterEnum.CountryAsc))
                .fetch();
    }

     private BooleanExpression valLike(CountryCateEnum cate, String val) {

         //검색 때문에 추가
         QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        if (cate != null && val != null && val.length() > 0) {
            switch (cate) {
                case All:
                    return countryEntity.country.toLowerCase().contains(val.toLowerCase())
                            .or(countryEntity.continentEntity.continent.toLowerCase().contains(val.toLowerCase()))
                            .or(countryEntity.description.toLowerCase().contains(val.toLowerCase()));
                case Country:
                    return countryEntity.country.toLowerCase().contains(val.toLowerCase());
                case Continent:
                    return countryEntity.continentEntity.continent.toLowerCase().contains(val.toLowerCase());
                case ISO_3166_1_Alpha2:
                    return countryEntity.iso2.toLowerCase().eq(val.toLowerCase());
                case ISO_3166_1_Alpha3:
                    return countryEntity.iso3.toLowerCase().eq(val.toLowerCase());
                case DialCode:
                    return countryEntity.dial.toLowerCase().contains(val.toLowerCase());
                case Description:
                    return countryEntity.description.toLowerCase().contains(val.toLowerCase());
                case RegUsrName:
                    return adminEntity.fullName.toLowerCase().contains(val.toLowerCase());
                case ModUsrName:
                    return adminEntity2.fullName.toLowerCase().contains(val.toLowerCase());
            }
        }
        return null;
    }

    private BooleanExpression useTypeEq(YnTypeEnum useType) {
        if (useType != null)
            return countryEntity.useType.eq(useType);
        return null;
    }

    private BooleanExpression termBetween(PeriodTypeEnum periodType, String  sdate, String edate) {

        if (sdate != null && edate != null && periodType != null) {

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
            LocalDateTime searchStartDate = LocalDateTime.parse(sdate + " 00:00:00.000", formatter);
            LocalDateTime searchEndDate = LocalDateTime.parse(edate + " 23:59:59.999", formatter);

            switch (periodType) {
                case Created:
                    return countryEntity.regDt.between(searchStartDate, searchEndDate);
                case LastModified:
                    return countryEntity.modDt.between(searchStartDate, searchEndDate);
                default:
                    return null;
            }
        }
        return null;
    }

    private OrderSpecifier sorterOrder(CountrySorterEnum sorter) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        if (sorter == null)
            return countryEntity.regDt.desc();

        if (sorter == CountrySorterEnum.ContinentAsc)
            return continentEntity.continent.asc();

        if (sorter == CountrySorterEnum.ContinentDesc)
            return continentEntity.continent.desc();

        if (sorter == CountrySorterEnum.CountryAsc)
            return countryEntity.country.asc();

        if (sorter == CountrySorterEnum.CountryDesc)
            return countryEntity.country.desc();

        if (sorter == CountrySorterEnum.UseYnAsc)
            return countryEntity.useType.asc();

        if (sorter == CountrySorterEnum.UseYnDesc)
            return countryEntity.useType.desc();

        if (sorter == CountrySorterEnum.LastModifiedAsc)
            return countryEntity.modDt.asc();

        if (sorter == CountrySorterEnum.LastModifiedDesc)
            return countryEntity.modDt.desc();

        if (sorter == CountrySorterEnum.CreatedAsc)
            return countryEntity.regDt.asc();

        if (sorter == CountrySorterEnum.CreatedDesc)
            return countryEntity.regDt.desc();

        if (sorter == CountrySorterEnum.Iso2Asc)
            return countryEntity.iso2.asc();

        if (sorter == CountrySorterEnum.Iso2Desc)
            return countryEntity.iso2.desc();

        if (sorter == CountrySorterEnum.Iso3Asc)
            return countryEntity.iso3.asc();

        if (sorter == CountrySorterEnum.Iso3Desc)
            return countryEntity.iso3.desc();

        if (sorter == CountrySorterEnum.DialAsc)
            return countryEntity.dial.asc();

        if (sorter == CountrySorterEnum.DialDesc)
            return countryEntity.dial.desc();

        if (sorter == CountrySorterEnum.DescriptionAsc)
            return countryEntity.description.asc();

        if (sorter == CountrySorterEnum.DescriptionDesc)
            return countryEntity.description.desc();

        if (sorter == CountrySorterEnum.CreaterNameAsc)
            return adminEntity.fullName.asc();

        if (sorter == CountrySorterEnum.CreaterNameDesc)
            return adminEntity.fullName.desc();

        if (sorter == CountrySorterEnum.ModifierNameAsc)
            return adminEntity2.fullName.asc();

        if (sorter == CountrySorterEnum.ModifierNameDesc)
            return adminEntity2.fullName.desc();

        return countryEntity.regDt.desc();
    }

}
