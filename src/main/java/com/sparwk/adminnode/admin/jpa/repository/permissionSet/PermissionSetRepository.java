package com.sparwk.adminnode.admin.jpa.repository.permissionSet;

import com.sparwk.adminnode.admin.jpa.entity.permissionSet.PermissionSetEntity;
import com.sparwk.adminnode.admin.jpa.repository.permissionSet.dsl.PermissionSetCustomRepository;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PermissionSetRepository extends JpaRepository<PermissionSetEntity, Long>, PermissionSetCustomRepository {
}
