package com.sparwk.adminnode.admin.jpa.entity.commonCode;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@Data
public class CommonIdClass implements Serializable {
    private String code;
}
