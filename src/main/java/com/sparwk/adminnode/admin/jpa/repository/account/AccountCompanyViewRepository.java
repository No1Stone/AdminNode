package com.sparwk.adminnode.admin.jpa.repository.account;

import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.account.AccountCompanyDetailEntity;
import com.sparwk.adminnode.admin.jpa.entity.account.AccountCompanyViewEntity;
import com.sparwk.adminnode.admin.jpa.repository.account.dsl.AccountCompanyCustomRepository;
import com.sparwk.adminnode.admin.jpa.repository.account.dsl.AccountCompanyViewCustomRepository;
import com.sparwk.adminnode.admin.jpa.repository.codeSeq.CodeSeqRepository;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountCompanyViewRepository extends JpaRepository<AccountCompanyViewEntity, Long>,
        AccountCompanyViewCustomRepository, CodeSeqRepository {
}
