package com.sparwk.adminnode.admin.jpa.repository.board.dsl;

import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.sparwk.adminnode.admin.biz.v1.board.dto.BoardNewsDto;
import com.sparwk.adminnode.admin.jpa.entity.PeriodTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.admin.QAdminEntity;
import com.sparwk.adminnode.admin.jpa.entity.board.BoardCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.board.BoardSorterEnum;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static com.sparwk.adminnode.admin.jpa.entity.admin.QAdminEntity.adminEntity;
import static com.sparwk.adminnode.admin.jpa.entity.board.QBoardNewsEntity.boardNewsEntity;
import static com.sparwk.adminnode.admin.jpa.entity.commonCode.QCommonDetailCodeEntity.commonDetailCodeEntity;

@Repository
public class BoardNewsCustomRepositoryImpl implements BoardNewsCustomRepository {

    private final JPAQueryFactory jpaQueryFactory;

    public BoardNewsCustomRepositoryImpl(JPAQueryFactory jpaQueryFactory) {
        this.jpaQueryFactory = jpaQueryFactory;
    }

    @Override
    public List<BoardNewsDto> findQryAll(String type,
                                        String pcode,
                                         BoardCateEnum cate,
                                         String val,
                                         YnTypeEnum useType,
                                         PeriodTypeEnum periodType,
                                         String sdate,
                                         String edate,
                                         BoardSorterEnum sorter,
                                         PageRequest pageRequest) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(Projections.bean(BoardNewsDto.class,

                        boardNewsEntity.newsId,
                        boardNewsEntity.cateCd,
                        commonDetailCodeEntity.val.as("cateCdName"),
                        boardNewsEntity.title,
                        boardNewsEntity.content,
                        boardNewsEntity.useYn.as("useType"),
                        boardNewsEntity.attachYn,
                        boardNewsEntity.hit,
                        boardNewsEntity.regUsr,
                        adminEntity.fullName.as("regUsrName"),
                        boardNewsEntity.regDt,
                        boardNewsEntity.modUsr,
                        adminEntity2.fullName.as("modUsrName"),
                        boardNewsEntity.modDt
                        )
                )
                .from(boardNewsEntity)
                .leftJoin(commonDetailCodeEntity).on(boardNewsEntity.cateCd.eq(commonDetailCodeEntity.dcode))
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(boardNewsEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(boardNewsEntity.modUsr))
                .where(boardPcodeEq(pcode), valLike(cate, val), useTypeEq(useType), termBetween(periodType, sdate, edate))
                .limit(pageRequest.getPageSize())
                .offset(pageRequest.getPageSize() * (pageRequest.getPageNumber() - 1))
                .orderBy(sorterOrder(sorter))
                .fetch();
    }

    @Override
    public long countQryAll(String type,
                                         String pcode,
                                         BoardCateEnum cate,
                                         String val,
                                         YnTypeEnum useType,
                                         PeriodTypeEnum periodType,
                                         String sdate,
                                         String edate) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(
                        boardNewsEntity.newsId.count()
                )
                .from(boardNewsEntity)
                .leftJoin(commonDetailCodeEntity).on(boardNewsEntity.cateCd.eq(commonDetailCodeEntity.dcode))
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(boardNewsEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(boardNewsEntity.modUsr))
                .where(boardPcodeEq(pcode), valLike(cate, val), useTypeEq(useType), termBetween(periodType, sdate, edate))
                .fetchOne();
    }

    @Override
    public List<BoardNewsDto> findQryUseY(String type,
                                          String pcode,
                                         BoardCateEnum cate,
                                         String val,
                                          PageRequest pageRequest
                                        ) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(Projections.bean(BoardNewsDto.class,

                                boardNewsEntity.newsId,
                                boardNewsEntity.cateCd,
                                commonDetailCodeEntity.val.as("cateCdName"),
                                boardNewsEntity.title,
                                boardNewsEntity.content,
                                boardNewsEntity.useYn.as("useType"),
                                boardNewsEntity.attachYn,
                                boardNewsEntity.hit,
                                boardNewsEntity.regUsr,
                                adminEntity.fullName.as("regUsrName"),
                                boardNewsEntity.regDt,
                                boardNewsEntity.modUsr,
                                adminEntity2.fullName.as("modUsrName"),
                                boardNewsEntity.modDt
                        )
                )
                .from(boardNewsEntity)
                .leftJoin(commonDetailCodeEntity).on(boardNewsEntity.cateCd.eq(commonDetailCodeEntity.dcode))
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(boardNewsEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(boardNewsEntity.modUsr))
                .where(boardPcodeEq(pcode), valLike(cate, val), useTypeEq(YnTypeEnum.Y))
                .limit(pageRequest.getPageSize())
                .offset(pageRequest.getPageSize() * (pageRequest.getPageNumber() - 1))
                .orderBy(sorterOrder(BoardSorterEnum.CreatedDesc))
                .fetch();
    }

    @Override
    public long countQryUseY(String type,
                                          String pcode,
                                          BoardCateEnum cate,
                                          String val
    ) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(
                                boardNewsEntity.newsId.count()
                )
                .from(boardNewsEntity)
                .leftJoin(commonDetailCodeEntity).on(boardNewsEntity.cateCd.eq(commonDetailCodeEntity.dcode))
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(boardNewsEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(boardNewsEntity.modUsr))
                .where(boardPcodeEq(pcode), valLike(cate, val), useTypeEq(YnTypeEnum.Y))
                .fetchOne();
    }

    @Override
    public long deleteQryList(Long[] id) {
        return jpaQueryFactory.delete(boardNewsEntity)
                .where(boardNewsEntity.newsId.in(id))
                .execute();
    }

    private BooleanExpression boardPcodeEq(String code) {
        if (code != null && code.length() > 0)
            return boardNewsEntity.cateCd.eq(code);
        return null;
    }

    private BooleanExpression valLike(BoardCateEnum cate, String val) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        if (cate != null && val != null && val.length() > 0) {
            switch (cate) {
                case All:
                    return boardNewsEntity.title.toLowerCase().contains(val.toLowerCase())
                            .or(boardNewsEntity.content.toLowerCase().contains(val.toLowerCase()));
                case Title:
                    return boardNewsEntity.title.toLowerCase().contains(val.toLowerCase());
                case Content:
                    return boardNewsEntity.content.toLowerCase().contains(val.toLowerCase());
                case RegUsrName:
                    return adminEntity.fullName.toLowerCase().contains(val.toLowerCase());
                case ModUsrName:
                    return adminEntity2.fullName.toLowerCase().contains(val.toLowerCase());
            }
        }
        return null;
    }

    private BooleanExpression idEq(Long id) {
        if (id != null)
            return boardNewsEntity.newsId.eq(id);
        return null;
    }

    private BooleanExpression useTypeEq(YnTypeEnum useType) {
        if (useType != null)
            return boardNewsEntity.useYn.eq(useType);
        return null;
    }

    private BooleanExpression termBetween(PeriodTypeEnum periodType, String sdate, String edate) {

        if (sdate != null && edate != null && periodType != null) {

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
            LocalDateTime searchStartDate = LocalDateTime.parse(sdate + " 00:00:00.000", formatter);
            LocalDateTime searchEndDate = LocalDateTime.parse(edate + " 23:59:59.999", formatter);

            switch (periodType) {
                case Created:
                    return boardNewsEntity.regDt.between(searchStartDate, searchEndDate);
                case LastModified:
                    return boardNewsEntity.modDt.between(searchStartDate, searchEndDate);
                default:
                    return null;
            }
        }
        return null;
    }

    private OrderSpecifier sorterOrder(BoardSorterEnum sorter) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        if (sorter == null)
            return boardNewsEntity.regDt.desc();

        if (sorter == BoardSorterEnum.NameAsc)
            return boardNewsEntity.title.asc();

        if (sorter == BoardSorterEnum.NameDesc)
            return boardNewsEntity.title.desc();

        if (sorter == BoardSorterEnum.TitleAsc)
            return boardNewsEntity.title.asc();

        if (sorter == BoardSorterEnum.TitleDesc)
            return boardNewsEntity.title.desc();

        if (sorter == BoardSorterEnum.LastModifiedAsc)
            return boardNewsEntity.modDt.asc();

        if (sorter == BoardSorterEnum.LastModifiedDesc)
            return boardNewsEntity.modDt.desc();

        if (sorter == BoardSorterEnum.CreatedAsc)
            return boardNewsEntity.regDt.asc();

        if (sorter == BoardSorterEnum.CreatedDesc)
            return boardNewsEntity.regDt.desc();

        if (sorter == BoardSorterEnum.TopicAsc)
            return commonDetailCodeEntity.val.asc();

        if (sorter == BoardSorterEnum.TopicDesc)
            return commonDetailCodeEntity.val.desc();

        if (sorter == BoardSorterEnum.CreaterNameAsc)
            return adminEntity.fullName.asc();

        if (sorter == BoardSorterEnum.CreaterNameDesc)
            return adminEntity.fullName.desc();

        if (sorter == BoardSorterEnum.ModifierNameAsc)
            return adminEntity2.fullName.asc();

        if (sorter == BoardSorterEnum.ModifierNameDesc)
            return adminEntity2.fullName.desc();

        return boardNewsEntity.regDt.desc();
    }

}
