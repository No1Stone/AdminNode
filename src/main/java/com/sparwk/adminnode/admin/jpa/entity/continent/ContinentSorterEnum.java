package com.sparwk.adminnode.admin.jpa.entity.continent;

import lombok.Getter;

@Getter
public enum ContinentSorterEnum {

    ContinentAsc("ContinentAsc"), ContinentDesc("ContinentDesc"),
    IsoAsc("IsoAsc"), IsoDesc("IsoDesc"),
    LastModifiedAsc("LastModifiedAsc"), LastModifiedDesc("LastModifiedDesc"),
    CreatedAsc("CreatedAsc"), CreatedDesc("CreatedDesc"),
    DescriptionAsc("DescriptionAsc"), DescriptionDesc("DescriptionDesc"),
    UseAsc("UseAsc"), UseDesc("UseDesc"),
    CreaterNameAsc("CreaterNameAsc"), CreaterNameDesc("CreaterNameDesc"),
    ModifierNameAsc("ModifierNameAsc"), ModifierNameDesc("ModifierNameDesc")
   ;

    private String sorter;

    ContinentSorterEnum(String sorter) {
        this.sorter = sorter;
    }

}
