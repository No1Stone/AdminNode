package com.sparwk.adminnode.admin.jpa.repository.song;

import com.sparwk.adminnode.admin.jpa.entity.song.SongViewEntity;
import com.sparwk.adminnode.admin.jpa.repository.song.dsl.SongViewCustomRepository;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SongViewRepository extends JpaRepository<SongViewEntity, Long>, SongViewCustomRepository {

}
