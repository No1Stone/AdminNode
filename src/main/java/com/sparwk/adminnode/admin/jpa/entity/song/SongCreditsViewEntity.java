package com.sparwk.adminnode.admin.jpa.entity.song;

import com.sparwk.adminnode.admin.jpa.entity.song.id.SongCreditsViewId;
import com.sparwk.adminnode.admin.jpa.entity.song.id.SongSplitsheetViewId;
import lombok.*;

import javax.persistence.*;

@Entity
@Getter
@Setter
@IdClass(SongCreditsViewId.class)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "view_admin_song_credits")
public class SongCreditsViewEntity {
    @Id
    @Column(name = "song_id", nullable = true)
    private Long songId;
    @Id
    @Column(name = "profile_id", nullable = true)
    private Long profileId;
    @Column(name = "full_name", nullable = true)
    private String fullName;
    @Column(name = "ipi_info", nullable = true)
    private String ipiInfo;
    @Column(name = "cae_info", nullable = true)
    private String caeInfo;
    @Column(name = "isni_info", nullable = true)
    private String isniInfo;
    @Column(name = "ipn_info", nullable = true)
    private String ipnInfo;
    @Column(name = "nro_type_name", nullable = true)
    private String nroTypeName;
    @Column(name = "nro_info", nullable = true)
    private String nroInfo;
    @Column(name = "role_name", nullable = true)
    private String roleName;

    @Builder
    SongCreditsViewEntity(
            Long songId,
            Long profileId,
            String fullName,
            String ipiInfo,
            String caeInfo,
            String isniInfo,
            String ipnInfo,
            String nroTypeName,
            String nroInfo,
            String roleName

    ) {
        this.songId = songId;
        this.profileId = profileId;
        this.fullName = fullName;
        this.ipiInfo = ipiInfo;
        this.caeInfo = caeInfo;
        this.isniInfo = isniInfo;
        this.ipnInfo = ipnInfo;
        this.nroTypeName = nroTypeName;
        this.nroInfo = nroInfo;
        this.roleName = roleName;

    }

}
