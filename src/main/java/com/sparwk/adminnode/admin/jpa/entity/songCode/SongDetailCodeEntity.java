package com.sparwk.adminnode.admin.jpa.entity.songCode;

import com.sparwk.adminnode.admin.jpa.entity.BaseEntity;
import com.sparwk.adminnode.admin.jpa.entity.FormatTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name="tb_admin_song_detail_code")
public class SongDetailCodeEntity extends BaseEntity {

    @Id
    @GeneratedValue(
            generator = "tb_admin_song_detail_code_seq"
    )
    @Column(name = "song_detail_code_seq")
    private Long songDetailCodeSeq;

    @Column(name = "pcode", length = 3, unique = true)
    private String pcode;

    @Column(name = "dcode", length = 9, unique = true)
    private String dcode;

    @Enumerated(EnumType.STRING)
    @Column(name = "format", length = 10)
    private FormatTypeEnum formatType;

    @Column(name = "val", length = 30)
    private String val;

    @Column(name = "description", length = 2000)
    private String description;

    @Column(name = "sort_index")
    private int sortIndex;

    @Column(name = "hit")
    private int hit;

    @Enumerated(EnumType.STRING)
    @Column(name = "use_yn", length = 1)
    private YnTypeEnum useType;

    @Enumerated(EnumType.STRING)
    @Column(name = "popular_yn", length = 1)
    private YnTypeEnum popularType;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "pcode", referencedColumnName = "code",insertable = false,updatable = false)
    private SongCodeEntity songCodeEntity;

    @Builder
    public SongDetailCodeEntity(
                                Long songDetailCodeSeq,
                                String pcode,
                                String dcode,
                                FormatTypeEnum formatType,
                                String val,
                                String description,
                                int sortIndex,
                                int hit,
                                YnTypeEnum useType,
                                YnTypeEnum popularType,
                                SongCodeEntity songCodeEntity
                          ) {
        this.songDetailCodeSeq = songDetailCodeSeq;
        this.pcode = pcode;
        this.dcode = dcode;
        this.formatType = formatType;
        this.val = val;
        this.description = description;
        this.sortIndex = sortIndex;
        this.hit = hit;
        this.useType = useType;
        this.popularType = popularType;
        this.songCodeEntity = songCodeEntity;
    }

    public void setSongCode(SongCodeEntity songCodeEntity){
        if(this.songCodeEntity != null) {
            this.songCodeEntity.getSongDetailCodeEntity().remove(this);
        }
        this.songCodeEntity = songCodeEntity;
        songCodeEntity.getSongDetailCodeEntity().add(this);
    }

    public void removeSongCode(SongCodeEntity songCodeEntity){
        if(this.songCodeEntity != null) {
            this.songCodeEntity.getSongDetailCodeEntity().remove(this);
        }
    }


    public void updateYn(YnTypeEnum useType) {
        this.useType = useType;
    }

    public void updatePopularYn(YnTypeEnum popularType) {
        this.popularType = popularType;
    }

    public void update(
                                String pcode,
                                String dcode,
                                FormatTypeEnum formatType,
                                String val,
                                String description,
                                int sortIndex,
                                YnTypeEnum useType,
                                YnTypeEnum popularType
                        ) {
        this.pcode = pcode;
        this.dcode = dcode;
        this.formatType = formatType;
        this.val = val;
        this.description = description;
        this.sortIndex = sortIndex;
        this.useType = useType;
        this.popularType = popularType;
    }

}
