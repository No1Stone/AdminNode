package com.sparwk.adminnode.admin.jpa.repository.email;

import com.sparwk.adminnode.admin.jpa.entity.email.MailingEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MailingRepository extends JpaRepository<MailingEntity, Long> {
}
