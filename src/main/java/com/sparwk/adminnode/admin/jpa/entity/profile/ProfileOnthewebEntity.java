package com.sparwk.adminnode.admin.jpa.entity.profile;


import com.sparwk.adminnode.admin.jpa.entity.BaseEntity;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.profile.id.ProfileOnthewebId;
import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_profile_ontheweb")
@DynamicUpdate
@IdClass(ProfileOnthewebId.class)
public class ProfileOnthewebEntity extends BaseEntity {

    @Id
    @Column(name = "profile_id")
    private Long profileId;
    @Id
    @Column(name = "sns_type_cd", nullable = true, length = 9)
    private String snsTypeCd;
    @Column(name = "sns_url", nullable = true, length = 200)
    private String snsUrl;
    @Enumerated(EnumType.STRING)
    @Column(name = "useYn", nullable = true, length = 1)
    private YnTypeEnum useYn;


    @Builder
    ProfileOnthewebEntity(
            Long profileId,
            String snsTypeCd,
            String snsUrl,
            YnTypeEnum useYn
    ) {
        this.profileId = profileId;
        this.snsTypeCd = snsTypeCd;
        this.snsUrl = snsUrl;
        this.useYn = useYn;
    }

}
