package com.sparwk.adminnode.admin.jpa.entity.song;

import com.sparwk.adminnode.admin.jpa.entity.BaseEntity;
import com.sparwk.adminnode.admin.jpa.entity.song.id.SongEvaluationRateId;
import com.sparwk.adminnode.admin.jpa.entity.song.id.SongFileId;
import lombok.*;

import javax.persistence.*;

@Entity
@Getter
@Setter
@IdClass(SongEvaluationRateId.class)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_evaluation_song_rate")
public class SongEvaluationRateEntity extends BaseEntity {
    @Id
    @Column(name = "eval_anr_seq", nullable = true)
    private long evalAnrSeq;
    @Id
    @Column(name = "eval_templ_dtl_cd", nullable = true, length = 9)
    private String evalTemplDtlCd;
    @Column(name = "eval_seq", nullable = true)
    private long evalSeq;
    @Column(name = "eval_rate", nullable = true)
    private int evalRate;

    @Builder
    SongEvaluationRateEntity(
            long evalAnrSeq,
            String evalTemplDtlCd,
            long evalSeq,
            int evalRate
    ) {
        this.evalAnrSeq = evalAnrSeq;
        this.evalTemplDtlCd = evalTemplDtlCd;
        this.evalSeq = evalSeq;
        this.evalRate = evalRate;
    }
}
