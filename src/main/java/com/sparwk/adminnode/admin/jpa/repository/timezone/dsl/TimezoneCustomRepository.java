package com.sparwk.adminnode.admin.jpa.repository.timezone.dsl;

import com.sparwk.adminnode.admin.biz.v1.timezone.dto.TimezoneDto;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.timezone.TimezoneCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.timezone.TimezoneSorterEnum;
import org.springframework.data.domain.PageRequest;

import java.util.List;

public interface TimezoneCustomRepository {

    List<TimezoneDto> findQryAll(TimezoneCateEnum cate, String val, YnTypeEnum useType, TimezoneSorterEnum sorter, PageRequest pageRequest);
    long countQryAll(TimezoneCateEnum cate, String val, YnTypeEnum useType);
    List<TimezoneDto> findQryUseY();
    long countQryUseY();

}
