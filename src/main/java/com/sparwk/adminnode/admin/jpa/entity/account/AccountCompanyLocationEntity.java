package com.sparwk.adminnode.admin.jpa.entity.account;


import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_account_company_location")
@DynamicUpdate
public class AccountCompanyLocationEntity {
    @Id
    @Column(name = "accnt_id")
    private Long accntId;

    @Column(name = "location_cd", nullable = false, length = 9)
    private String locationCd;

    @Builder
    AccountCompanyLocationEntity(
            Long accntId,
            String locationCd
    ) {
        this.accntId = accntId;
        this.locationCd = locationCd;
    }

//    @OneToOne(mappedBy = "accountPassport")
//    private Account account;
}
