package com.sparwk.adminnode.admin.jpa.entity.songCode;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@Data
public class SongIdClass implements Serializable {
    private String code;
}
