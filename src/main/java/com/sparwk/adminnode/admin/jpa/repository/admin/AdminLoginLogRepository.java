package com.sparwk.adminnode.admin.jpa.repository.admin;

import com.sparwk.adminnode.admin.jpa.entity.admin.AdminLoginLogEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AdminLoginLogRepository extends JpaRepository<AdminLoginLogEntity, Long> {

}
