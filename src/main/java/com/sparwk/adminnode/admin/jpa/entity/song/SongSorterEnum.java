package com.sparwk.adminnode.admin.jpa.entity.song;

import lombok.Getter;

@Getter
public enum SongSorterEnum {

    TitleAsc("TitleAsc"), TitleDesc("TitleDesc"),
    OwnerAsc("OwnerAsc"), OwnerDesc("OwnerDesc"),
    CowriterAsc("CowriterAsc"), CowriterDesc("CowriterDesc"),
    ProjectTitleAsc("ProjectTitleAsc"), ProjectTitleDesc("ProjectTitleDesc"),
    GenreAsc("GenreAsc"), GenreDesc("GenredDesc"),
    SubGenreAsc("SubGenreAsc"), SubGenreDesc("SubGenredDesc"),
    MoodAsc("MoodAsc"), MoodDesc("MoodDesc"),
    ThemeAsc("ThemeAsc"), ThemeDesc("ThemeDesc"),
    VersionAsc("VersionAsc"), VersionDesc("VersionDesc"),
    StatusAsc("StatusAsc"), StatusDesc("StatusDesc"),
    LanguageAsc("LanguageAsc"), LanguageDesc("LanguageDesc"),
    BpmAsc("BpmAsc"), BpmDesc("BpmDesc"),
    SongPlayTimeAsc("SongPlayTimeAsc"), SongPlayTimeDesc("SongPlayTimeDesc"),
    TempoAsc("TempoAsc"), TempoDesc("TempoDesc"),
    ISRCAsc("ISRCAsc"), ISRCDesc("ISRCDesc"),
    KeySignatureAsc("KeySignatureAsc"), KeySignatureDesc("KeySignatureDesc"),
    TimeSignatureAsc("TimeSignatureAsc"), TimeSignatureDesc("TimeSignatureDesc"),
    LyricsAsc("LyricsAsc"), LyricsDesc("LyricsDesc"),
    LyricsComtAsc("LyricsComtAsc"), LyricsComtDesc("LyricsComtDesc"),
    RegUsrNameAsc("RegUsrNameAsc"), RegUsrNameDesc("RegUsrNameDesc"),
    CreatedAsc("CreatedAsc"), CreatedDesc("CreatedDesc"),
    ModUsrNameAsc("ModUsrNameAsc"), ModUsrNameDesc("ModUsrNameDesc"),
    ModifiedAsc("ModifiedAsc"), ModifiedDesc("ModifiedDesc")
    ;

    private String sorter;

    SongSorterEnum(String sorter) {
        this.sorter = sorter;
    }

}
