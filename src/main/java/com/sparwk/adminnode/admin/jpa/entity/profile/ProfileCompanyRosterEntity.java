package com.sparwk.adminnode.admin.jpa.entity.profile;


import com.sparwk.adminnode.admin.jpa.entity.BaseEntity;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_profile_company_roster")
@DynamicUpdate
public class ProfileCompanyRosterEntity extends BaseEntity {

    @Id
    @Column(name = "roster_seq")
    private Long rosterSeq;
    @Column(name = "profile_id")
    private Long profileId;
    @Column(name = "profile_position_seq")
    private Long profilePositionSeq;
    @Column(name = "roster_status", nullable = true, length = 9)
    private String rosterStatus;
    @Column(name = "joining_start_dt", nullable = true, length = 10)
    private String joiningStartDt;
    @Column(name = "joining_end_dt", nullable = true, length = 10)
    private String joiningEndDt;
    @Enumerated(EnumType.STRING)
    @Column(name = "joining_end_yn", nullable = true, length = 1)
    private YnTypeEnum joiningEndYn;


    @Builder
    ProfileCompanyRosterEntity(
            Long rosterSeq,
            Long profileId,
            Long profilePositionSeq,
            String rosterStatus,
            String joiningStartDt,
            String joiningEndDt,
            YnTypeEnum joiningEndYn
    ) {
        this.rosterSeq = rosterSeq;
        this.profileId = profileId;
        this.profilePositionSeq = profilePositionSeq;
        this.rosterStatus = rosterStatus;
        this.joiningStartDt = joiningStartDt;
        this.joiningEndDt = joiningEndDt;
        this.joiningEndYn = joiningEndYn;
    }

}
