package com.sparwk.adminnode.admin.jpa.entity.account;


import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_account_sns_token")
@DynamicUpdate
public class AccountSnsTokenEntity {
    @Id
    @Column(name = "accnt_id")
    private Long accntId;

    @Column(name = "sns_type_cd", nullable = false, length = 50)
    private String snsTypeCd;

    @Column(name = "sns_token", nullable = true, length = 200)
    private String snsToken;

    @Builder
    AccountSnsTokenEntity(
            Long accntId,
            String snsTypeCd,
            String snsToken
    ) {
        this.accntId = accntId;
        this.snsTypeCd = snsTypeCd;
        this.snsToken = snsToken;
    }

//    @OneToOne(mappedBy = "accountPassport")
//    private Account account;
}
