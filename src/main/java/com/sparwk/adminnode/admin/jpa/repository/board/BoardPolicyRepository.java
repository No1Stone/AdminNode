package com.sparwk.adminnode.admin.jpa.repository.board;

import com.sparwk.adminnode.admin.jpa.entity.board.BoardPolicyEntity;
import com.sparwk.adminnode.admin.jpa.repository.board.dsl.BoardPolicyCustomRepository;
import com.sparwk.adminnode.admin.jpa.repository.codeSeq.CodeSeqRepository;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BoardPolicyRepository extends JpaRepository<BoardPolicyEntity, Long>, BoardPolicyCustomRepository, CodeSeqRepository {

}
