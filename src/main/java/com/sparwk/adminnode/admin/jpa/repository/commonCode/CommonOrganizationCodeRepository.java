package com.sparwk.adminnode.admin.jpa.repository.commonCode;

import com.sparwk.adminnode.admin.jpa.entity.commonCode.CommonOrganizationCodeEntity;
import com.sparwk.adminnode.admin.jpa.repository.codeSeq.CodeSeqRepository;
import com.sparwk.adminnode.admin.jpa.repository.commonCode.dsl.CommonOrganizationCodeCustomRepository;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CommonOrganizationCodeRepository extends JpaRepository<CommonOrganizationCodeEntity, Long>
        , CommonOrganizationCodeCustomRepository, CodeSeqRepository {
    Long countByVal(String val);

}
