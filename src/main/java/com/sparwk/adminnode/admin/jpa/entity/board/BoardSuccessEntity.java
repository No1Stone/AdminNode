package com.sparwk.adminnode.admin.jpa.entity.board;

import com.sparwk.adminnode.admin.jpa.entity.BaseEntity;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name="tb_board_success")
public class BoardSuccessEntity extends BaseEntity {

    @Id
    @GeneratedValue(
            generator = "tb_board_success_seq"
    )
    @Column(name = "success_id")
    private Long successId;

    @Column(name = "name", nullable = false, length = 100)
    private String name;

    @Column(name = "role", length = 100)
    private String role;

    @Column(name = "genre", length = 100)
    private String genre;

    @Column(name = "content", nullable = true)
    private String content;

    @Enumerated(EnumType.STRING)
    @Column(name = "use_yn", length = 1)
    private YnTypeEnum useType;

    @Column(name = "image_url", nullable = true, length = 200)
    private String imageUrl;

    @Builder
    public BoardSuccessEntity(
                            Long successId,
                            String name,
                            String role,
                            String genre,
                            String content,
                            YnTypeEnum useType,
                            String imageUrl
                          ) {
        this.successId = successId;
        this.name = name;
        this.role = role;
        this.genre = genre;
        this.content = content;
        this.useType = useType;
        this.imageUrl = imageUrl;
    }

    public void updateYn(YnTypeEnum useType) {
        this.useType = useType;
    }

    public void update(
            String name,
            String role,
            String genre,
            String content,
            YnTypeEnum useType,
            String imageUrl

                        ) {
        this.name = name;
        this.role = role;
        this.genre = genre;
        this.content = content;
        this.useType = useType;
        this.imageUrl = imageUrl;
    }


}
