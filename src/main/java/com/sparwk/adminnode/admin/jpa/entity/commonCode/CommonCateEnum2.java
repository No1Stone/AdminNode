package com.sparwk.adminnode.admin.jpa.entity.commonCode;

import lombok.Getter;

@Getter
public enum CommonCateEnum2 {

    All("All"), Menu("Menu"), Category("Category"), RegUsrName("RegUsrName"), ModUsrName("ModUsrName");

    private String commonCateEnum;

    CommonCateEnum2(String commonCateEnum) {
        this.commonCateEnum = commonCateEnum;
    }
}
