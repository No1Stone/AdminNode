package com.sparwk.adminnode.admin.jpa.entity.song;

import com.sparwk.adminnode.admin.jpa.entity.song.id.SongMetaCustomId;
import com.sparwk.adminnode.admin.jpa.entity.song.id.SongSplitsheetViewId;
import lombok.*;

import javax.persistence.*;

@Entity
@Getter
@Setter
@IdClass(SongSplitsheetViewId.class)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "view_admin_song_splitsheet")
public class SongSplitsheetViewEntity {
    @Id
    @Column(name = "song_id", nullable = true)
    private Long songId;
    @Id
    @Column(name = "profile_id", nullable = true)
    private Long profileId;
    @Column(name = "full_name", nullable = true)
    private String fullName;
    @Column(name = "ipi_info", nullable = true)
    private String ipiInfo;
    @Column(name = "cae_info", nullable = true)
    private String caeInfo;
    @Column(name = "isni_info", nullable = true)
    private String isniInfo;
    @Column(name = "ipn_info", nullable = true)
    private String ipnInfo;
    @Column(name = "role_name", nullable = true)
    private String roleName;
    @Column(name = "rate_share", nullable = true)
    private String rateShare;
    @Column(name = "original_publisher", nullable = true)
    private String originalPublisher;
    @Column(name = "sub_publisher", nullable = true)
    private String subPublisher;
    @Column(name = "pro", nullable = true)
    private String pro;

    @Builder
    SongSplitsheetViewEntity(
            Long songId,
            Long profileId,
            String fullName,
            String ipiInfo,
            String caeInfo,
            String isniInfo,
            String ipnInfo,
            String roleName,
            String rateShare,
            String originalPublisher,
            String subPublisher,
            String pro
    ) {
        this.songId = songId;
        this.profileId = profileId;
        this.fullName = fullName;
        this.ipiInfo = ipiInfo;
        this.caeInfo = caeInfo;
        this.isniInfo = isniInfo;
        this.ipnInfo = ipnInfo;
        this.roleName = roleName;
        this.rateShare = rateShare;
        this.originalPublisher = originalPublisher;
        this.subPublisher = subPublisher;
        this.pro = pro;
    }

}
