package com.sparwk.adminnode.admin.jpa.entity.board;


import com.sparwk.adminnode.admin.jpa.entity.BaseEntity;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import lombok.*;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_board_attach")
public class BoardAttachEntity extends BaseEntity {
    @Id
    @GeneratedValue(
            generator = "tb_board_attach_seq"
    )
    @Column(name = "attach_id")
    private Long attachId;

    @Column(name = "board_type", nullable = false, length = 20)
    private String boardType;

    @Column(name = "board_id")
    private Long boardId;

    @Column(name = "file_num", nullable = false)
    private Long fileNum;

    @Column(name = "file_url", nullable = true, length = 200)
    private String fileUrl;

    @Column(name = "file_name", nullable = true, length = 100)
    private String fileName;

    @Column(name = "file_size", nullable = true)
    private Long fileSize;


    @Builder
    public BoardAttachEntity(
            Long attachId,
            String boardType,
            Long boardId,
            Long fileNum,
            String fileUrl,
            String fileName,
            Long fileSize
    ) {
        this.attachId = attachId;
        this.boardType = boardType;
        this.boardId = boardId;
        this.fileNum = fileNum;
        this.fileUrl = fileUrl;
        this.fileName = fileName;
        this.fileSize = fileSize;
    }

    public void update(
            String fileUrl,
            String fileName,
            Long fileSize
    ) {
        this.fileUrl = fileUrl;
        this.fileName = fileName;
        this.fileSize = fileSize;
    }

    public BoardAttachEntity toEntity() {
        return new BoardAttachEntity(attachId, boardType, boardId, fileNum, fileUrl, fileName, fileSize);
    }

//    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = AccountCompanyDetail.class)
//    @JoinColumn(name = "accnt_id", referencedColumnName = "accnt_id")
//    private AccountCompanyDetail accountCompanyDetail;
//
//    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = AccountCompanyLocation.class)
//    @JoinColumn(name = "accnt_id", referencedColumnName = "accnt_id")
//    private AccountCompanyLocation accountCompanyLocation;
//
//    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = AccountCompanyType.class)
//    @JoinColumn(name = "accnt_id", referencedColumnName = "accnt_id")
//    private AccountCompanyType accountCompanyType;
//
//    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = AccountPassport.class)
//    @JoinColumn(name = "accnt_id", referencedColumnName = "accnt_id")
//    private AccountPassport accountPassport;
//
//    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = AccountSnsToken.class)
//    @JoinColumn(name = "accnt_id", referencedColumnName = "accnt_id")
//    private List<AccountSnsToken> accountSnsToken;

}
