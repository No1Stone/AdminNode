package com.sparwk.adminnode.admin.jpa.entity.board;


import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_board_qna")
@DynamicUpdate
@TableGenerator(
        name = "tb_board_seq_generator",
        table = "tb_board_sequences",
        pkColumnValue = "tb_board_seq",
        allocationSize = 1,
        initialValue = 1
)
public class BoardQnaEntity {
    @Id
    @GeneratedValue(
            strategy = GenerationType.TABLE,
            generator = "tb_board_seq_generator")
    @Column(name = "qna_id")
    private Long qnaId;

    @Column(name = "cate_cd", nullable = true, length = 9)
    private String cateCd;

    @Column(name = "recive_email", nullable = true, length = 50)
    private String reciveEmail;

    @Enumerated(EnumType.STRING)
    @Column(name = "recive_email_yn", nullable = true, length = 1)
    private YnTypeEnum reciveEmailYn;

    @Column(name = "title", nullable = true, length = 100)
    private String title;

    @Column(name = "question_content", nullable = true)
    private String questionContent;

    @Enumerated(EnumType.STRING)
    @Column(name = "question_attach_yn", nullable = true, length = 1)
    private YnTypeEnum questionAttachYn;

    @Enumerated(EnumType.STRING)
    @Column(name = "answer_yn", nullable = true, length = 1)
    private YnTypeEnum answerYn;

    @Column(name = "answer_content", nullable = true)
    private String answerContent;

    @Enumerated(EnumType.STRING)
    @Column(name = "answer_attach_yn", nullable = true, length = 1)
    private YnTypeEnum answerAttachYn;

    @Enumerated(EnumType.STRING)
    @Column(name = "answer_email_yn", nullable = true, length = 1)
    private YnTypeEnum answerEmailYn;

    @Column(name="reg_usr", updatable = false)
    private Long regUsr;

    @Column(name="reg_dt", updatable = false)
    private LocalDateTime regDt;

    @Column(name="mod_usr")
    private Long modUsr;

    @Column(name="mod_dt")
    private LocalDateTime modDt;

    @Column(name="answer_reg_usr")
    private Long answerRegUsr;

    @Column(name="answer_reg_dt")
    private LocalDateTime answerRegDt;

    @Column(name="answer_mod_usr")
    private Long answerModUsr;

    @Column(name="answer_mod_dt")
    private LocalDateTime answerModDt;


    @Builder
    BoardQnaEntity(
            Long qnaId,
            String cateCd,
            String reciveEmail,
            YnTypeEnum reciveEmailYn,
            String title,
            String questionContent,
            YnTypeEnum questionAttachYn,
            YnTypeEnum answerYn,
            String answerContent,
            YnTypeEnum answerAttachYn,
            YnTypeEnum answerEmailYn,
            Long regUsr,
            LocalDateTime regDt,
            Long modUsr,
            LocalDateTime modDt,
            Long answerRegUsr,
            LocalDateTime answerRegDt,
            Long answerModUsr,
            LocalDateTime answerModDt
    ) {
        this.qnaId=qnaId;
        this.cateCd=cateCd;
        this.reciveEmail=reciveEmail;
        this.reciveEmailYn=reciveEmailYn;
        this.title=title;
        this.questionContent=questionContent;
        this.questionAttachYn=questionAttachYn;
        this.answerYn=answerYn;
        this.answerContent=answerContent;
        this.answerAttachYn=answerAttachYn;
        this.answerEmailYn=answerEmailYn;
        this.regUsr=regUsr;
        this.regDt=regDt;
        this.modUsr=modUsr;
        this.modDt=modDt;
        this.answerRegUsr=answerRegUsr;
        this.answerRegDt=answerRegDt;
        this.answerModUsr=answerModUsr;
        this.answerModDt=answerModDt;
    }

    public void answerReg(
            YnTypeEnum answerYn,
            String answerContent,
            YnTypeEnum answerAttachYn,
            YnTypeEnum answerEmailYn,
            Long answerRegUsr,
            LocalDateTime answerRegDt,
            Long answerModUsr,
            LocalDateTime answerModDt

    ) {
        this.answerYn = answerYn;
        this.answerContent = answerContent;
        this.answerAttachYn = answerAttachYn;
        this.answerEmailYn = answerEmailYn;
        this.answerRegUsr = answerRegUsr;
        this.answerRegDt = answerRegDt;
        this.answerModUsr = answerModUsr;
        this.answerModDt = answerModDt;
    }

    public void questionUpdate(
            YnTypeEnum answerYn,
            String cateCd,
            String reciveEmail,
            YnTypeEnum reciveEmailYn,
            String title,
            String questionContent,
            YnTypeEnum questionAttachYn,
            Long modUsr,
            LocalDateTime modDt

    ) {
        this.answerYn = answerYn;
        this.cateCd = cateCd;
        this.reciveEmail = reciveEmail;
        this.reciveEmailYn = reciveEmailYn;
        this.title = title;
        this.questionContent = questionContent;
        this.questionAttachYn = questionAttachYn;
        this.modUsr = modUsr;
        this.modDt = modDt;
    }

    public void answerUpdate(
            YnTypeEnum answerYn,
            String answerContent,
            YnTypeEnum answerAttachYn,
            YnTypeEnum answerEmailYn,
            Long answerModUsr,
            LocalDateTime answerModDt

    ) {
        this.answerYn = answerYn;
        this.answerContent = answerContent;
        this.answerAttachYn = answerAttachYn;
        this.answerEmailYn = answerEmailYn;
        this.answerModUsr = answerModUsr;
        this.answerModDt = answerModDt;
    }

//    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = AccountCompanyDetail.class)
//    @JoinColumn(name = "accnt_id", referencedColumnName = "accnt_id")
//    private AccountCompanyDetail accountCompanyDetail;
//
//    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = AccountCompanyLocation.class)
//    @JoinColumn(name = "accnt_id", referencedColumnName = "accnt_id")
//    private AccountCompanyLocation accountCompanyLocation;
//
//    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = AccountCompanyType.class)
//    @JoinColumn(name = "accnt_id", referencedColumnName = "accnt_id")
//    private AccountCompanyType accountCompanyType;
//
//    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = AccountPassport.class)
//    @JoinColumn(name = "accnt_id", referencedColumnName = "accnt_id")
//    private AccountPassport accountPassport;
//
//    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = AccountSnsToken.class)
//    @JoinColumn(name = "accnt_id", referencedColumnName = "accnt_id")
//    private List<AccountSnsToken> accountSnsToken;

}
