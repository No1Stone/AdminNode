package com.sparwk.adminnode.admin.jpa.entity.account;

import lombok.Getter;

@Getter
public enum VerificationIndividualAccountSorterEnum {

    EmailAsc("EmailAsc"),
    EmailDesc("EmailDesc"),
    FullNameAsc("FullNameAsc"),
    FullNameDesc("FullNameDesc"),
    CompanyAsc("CompanyAsc"),
    CompanyDesc("CompanyDesc"),
    RolesAsc("RolesAsc"),
    RolesDesc("RolesDesc"),
    CreatedAsc("CreatedAsc"),
    CreatedDesc("CreatedDesc");

    private String sorter;

    VerificationIndividualAccountSorterEnum(String sorter) {
        this.sorter = sorter;
    }
}
