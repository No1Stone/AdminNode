package com.sparwk.adminnode.admin.jpa.entity.permissionSet.id;

import lombok.Data;

import java.io.Serializable;

@Data
public class PermissionMenuSetId implements Serializable {
    private long adminMenuId;
    private long adminPermissionId;
}
