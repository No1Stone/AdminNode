package com.sparwk.adminnode.admin.jpa.repository.song.dsl;

import com.querydsl.core.types.Projections;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.sparwk.adminnode.admin.biz.v1.song.dto.SongMetadataDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.sparwk.adminnode.admin.jpa.entity.song.QSongMetaEntity.songMetaEntity;
import static com.sparwk.adminnode.admin.jpa.entity.songCode.QSongDetailCodeEntity.songDetailCodeEntity;

@Repository
public class SongCustomRepositoryImpl implements SongCustomRepository {

    private final JPAQueryFactory jpaQueryFactory;

    private final Logger logger = LoggerFactory.getLogger(SongCustomRepositoryImpl.class);

    public SongCustomRepositoryImpl(JPAQueryFactory jpaQueryFactory) {
        this.jpaQueryFactory = jpaQueryFactory;
    }

    @Override
    public List<SongMetadataDto> findQrySongMeta(String pcode, Long id) {

        return jpaQueryFactory.select(Projections.bean(SongMetadataDto.class,
                                songDetailCodeEntity.val.as("attrDtlCdVal"),
                                songMetaEntity.songId,
                                songMetaEntity.attrTypeCd,
                                songMetaEntity.attrDtlCd
                        )
                )
                .from(songMetaEntity)
                .leftJoin(songDetailCodeEntity).on(songDetailCodeEntity.dcode.eq(songMetaEntity.attrDtlCd))
                .where(songMetaEntity.songId.eq(id),songDetailCodeEntity.pcode.eq(pcode))
                .fetch();

    }
}
