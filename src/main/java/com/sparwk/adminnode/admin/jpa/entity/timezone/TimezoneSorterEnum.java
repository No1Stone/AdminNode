package com.sparwk.adminnode.admin.jpa.entity.timezone;

import lombok.Getter;

@Getter
public enum TimezoneSorterEnum {

    TimezoneAsc("TimezoneAsc"), TimezoneDesc("TimezoneDesc"),
    ContinentAsc("ContinentAsc"), ContinentDesc("ContinentDesc"),
    CityAsc("CityAsc"), CityDesc("CityDesc"),
    UTCAsc("UTCAsc"), UTCDesc("UTCDesc"),
    DiffAsc("DiffAsc"), DiffDesc("DiffDesc"),
    CreaterNameAsc("CreaterNameAsc"), CreaterNameDesc("CreaterNameDesc"),
    ModifierNameAsc("ModifierNameAsc"), ModifierNameDesc("ModifierNameDesc")
    ;

    private String sorter;

    TimezoneSorterEnum(String sorter) {
        this.sorter = sorter;
    }

}
