package com.sparwk.adminnode.admin.jpa.entity.profile.id;

import lombok.Data;

import java.io.Serializable;

@Data
public class ProfileEducationId implements Serializable {

    private String eduOrganizationNm;
    private Long profileId;


}
