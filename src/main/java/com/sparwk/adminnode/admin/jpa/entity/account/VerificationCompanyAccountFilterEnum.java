package com.sparwk.adminnode.admin.jpa.entity.account;

import lombok.Getter;

@Getter
public enum VerificationCompanyAccountFilterEnum {

    All("All"),
    AccountVerify("AccountVerify"),
    AccountUnconfirmed("AccountUnconfirmed"),
    AccountReject("AccountReject"),
    IpiBaseNumberVerify("IPIBaseNumberVerify"),
    IpiBaseNumberUnconfirmed("IPIBaseNumberUnconfirmed"),
    IpiBaseNumberReject("IPIBaseNumberReject");

    private String filter;

    VerificationCompanyAccountFilterEnum(String filter) {
        this.filter = filter;
    }
}
