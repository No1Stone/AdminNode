package com.sparwk.adminnode.admin.jpa.entity.song;

import com.sparwk.adminnode.admin.jpa.entity.BaseEntity;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_evaluation_anr")
public class SongEvaluationAnrEntity extends BaseEntity {
    @Id
    @Column(name = "eval_anr_seq", nullable = true)
    private long evalAnrSeq;
    @Column(name = "eval_seq", nullable = true)
    private long evalSeq;
    @Column(name = "eval_anr_id", nullable = true)
    private long evalAnrId;

    @Column(name = "patp_sdt", nullable = true)
    private LocalDateTime patpSdt;
    @Column(name = "patp_edt", nullable = true)
    private LocalDateTime patpEdt;

    @Enumerated(EnumType.STRING)
    @Column(name = "active_yn", nullable = true, length = 1)
    private YnTypeEnum activeYn;
    @Enumerated(EnumType.STRING)
    @Column(name = "confirm_yn", nullable = true, length = 1)
    private YnTypeEnum confirmYn;

    @Enumerated(EnumType.STRING)
    @Column(name = "invt_stat", nullable = true, length = 1)
    private YnTypeEnum invtStat;
    @Column(name = "invt_dt", nullable = true)
    private LocalDateTime invtDt;

    @Enumerated(EnumType.STRING)
    @Column(name = "ban_yn", nullable = true, length = 1)
    private YnTypeEnum banYn;
    @Column(name = "ban_dt", nullable = true)
    private LocalDateTime banDt;

    @Enumerated(EnumType.STRING)
    @Column(name = "quit_yn", nullable = true, length = 1)
    private YnTypeEnum quitYn;
    @Column(name = "quit_dt", nullable = true)
    private LocalDateTime quitDt;

    @Builder
    SongEvaluationAnrEntity(
            long evalAnrSeq,
            long evalSeq,
            long evalAnrId,
            LocalDateTime patpSdt,
            LocalDateTime patpEdt,
            YnTypeEnum activeYn,
            YnTypeEnum confirmYn,
            YnTypeEnum invtStat,
            LocalDateTime invtDt,
            YnTypeEnum banYn,
            LocalDateTime banDt,
            YnTypeEnum quitYn,
            LocalDateTime quitDt
    ) {
        this.evalAnrSeq = evalAnrSeq;
        this.evalSeq = evalSeq;
        this.evalAnrId = evalAnrId;
        this.patpSdt = patpSdt;
        this.patpEdt = patpEdt;
        this.activeYn = activeYn;
        this.confirmYn = confirmYn;
        this.invtStat = invtStat;
        this.invtDt = invtDt;
        this.banYn = banYn;
        this.banDt = banDt;
        this.quitYn = quitYn;
        this.quitDt = quitDt;
    }
}
