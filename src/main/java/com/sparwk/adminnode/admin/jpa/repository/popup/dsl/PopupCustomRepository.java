package com.sparwk.adminnode.admin.jpa.repository.popup.dsl;

import com.sparwk.adminnode.admin.biz.v1.popup.dto.PopupDto;
import com.sparwk.adminnode.admin.jpa.entity.PeriodTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.popup.PopupCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.popup.PopupSorterEnum;
import org.springframework.data.domain.PageRequest;

import java.util.List;

public interface PopupCustomRepository {
    List<PopupDto> findQryAll(PopupCateEnum cate,
                              String val,
                              PeriodTypeEnum periodType,
                              String sdate,
                              String edate,
                              PopupSorterEnum sorter,
                              PageRequest pageRequest);

    long countQryAll(PopupCateEnum cate,
                              String val,
                              PeriodTypeEnum periodType,
                              String sdate,
                              String edate);
    long deleteQryList(Long[] id);
}