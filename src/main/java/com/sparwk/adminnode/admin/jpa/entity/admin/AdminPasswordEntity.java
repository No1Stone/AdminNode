package com.sparwk.adminnode.admin.jpa.entity.admin;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name="tb_admin_passworddd")
public class AdminPasswordEntity {

    @Id
    @Column(name = "admin_id")
    private Long adminId;

    @Column(name = "admin_password", length = 100)
    private String adminPassword;

    @Column(name = "admin_prev_password", length = 100)
    private String adminPrevPassword;

    @Column(name = "last_connect")
    private LocalDateTime lastConnect;


    @Builder
    public AdminPasswordEntity(
            Long adminId,
            String adminPassword,
            String adminPrevPassword,
            LocalDateTime lastConnect
                          ) {
        this.adminId = adminId;
        this.adminPassword = adminPassword;
        this.adminPrevPassword = adminPrevPassword;
        this.lastConnect = lastConnect;
    }

    public void updatePassword(String adminPassword,
                               String adminPrevPassword) {
        this.adminPassword = adminPassword;
        this.adminPrevPassword = adminPrevPassword;
    }

}
