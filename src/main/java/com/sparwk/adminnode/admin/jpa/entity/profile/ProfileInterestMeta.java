package com.sparwk.adminnode.admin.jpa.entity.profile;

import com.sparwk.adminnode.admin.jpa.entity.BaseEntity;
import com.sparwk.adminnode.admin.jpa.entity.profile.id.ProfileInterestMetaId;
import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_profile_interest_meta")
@DynamicUpdate
public class ProfileInterestMeta extends BaseEntity {

    @Id
    @Column(name = "profile_id", length = 9)
    private Long profileId;
    @Column(name = "kind_type_cd", nullable = true, length = 3)
    private String kindTypeCd;
    @Column(name = "detail_type_cd", nullable = true, length = 9)
    private String detailTypeCd;

    @Builder
    ProfileInterestMeta(
            Long profileId,
            String kindTypeCd,
            String detailTypeCd
    ) {
        this.profileId = profileId;
        this.kindTypeCd = kindTypeCd;
        this.detailTypeCd = detailTypeCd;
    }


}
