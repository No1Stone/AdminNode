package com.sparwk.adminnode.admin.jpa.repository.admin.dsl;

import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.sparwk.adminnode.admin.biz.v1.admin.dto.AdminDto;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.admin.AdminCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.admin.AdminSorterEnum;
import com.sparwk.adminnode.admin.jpa.entity.admin.QAdminEntity;
import com.sparwk.adminnode.admin.jpa.entity.commonCode.CommonSorterEnum2;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.sparwk.adminnode.admin.jpa.entity.admin.QAdminEntity.adminEntity;
import static com.sparwk.adminnode.admin.jpa.entity.commonCode.QCommonOrganizationCodeEntity.commonOrganizationCodeEntity;
import static com.sparwk.adminnode.admin.jpa.entity.permissionSet.QPermissionSetEntity.permissionSetEntity;

@Repository
public class AdminCustomRepositoryImpl implements AdminCustomRepository {

    private final JPAQueryFactory jpaQueryFactory;

    public AdminCustomRepositoryImpl(JPAQueryFactory jpaQueryFactory) {
        this.jpaQueryFactory = jpaQueryFactory;
    }

    @Override
    public List<AdminDto> findQryAll(AdminCateEnum cate,
                                        String val,
                                        YnTypeEnum useType,
                                        AdminSorterEnum sorter,
                                        PageRequest pageRequest) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(Projections.bean(AdminDto.class,
                                adminEntity.adminId,
                                adminEntity.adminEmail,
                                adminEntity.fullName,
                                adminEntity.dial,
                                adminEntity.phoneNumber,
                                adminEntity.permissionAssignSeq,
                        permissionSetEntity.labelVal.as("permissionAssignName"),
                                adminEntity.description,
                                adminEntity.useType,
                                adminEntity.lockType,
                                adminEntity.regUsr,
                                adminEntity.fullName.as("regUsrName"),
                                adminEntity.regDt,
                                adminEntity.modUsr,
                                adminEntity2.fullName.as("modUsrName"),
                                adminEntity.modDt
                        )
                )
                .from(adminEntity)
                .leftJoin(permissionSetEntity).on(adminEntity.permissionAssignSeq.eq(permissionSetEntity.adminPermissionId))
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(adminEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(adminEntity.modUsr))
                .where(valLike(cate, val), useTypeEq(useType))
                .limit(pageRequest.getPageSize())
                .offset(pageRequest.getPageSize() * (pageRequest.getPageNumber() - 1))
                .orderBy(sorterOrder(sorter))
                .fetch();
    }

    @Override
    public long countQryAll(AdminCateEnum cate,
                                        String val,
                                        YnTypeEnum useType) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(
                                adminEntity.adminId.count()
                )
                .from(adminEntity)
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(adminEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(adminEntity.modUsr))
                .where(valLike(cate, val), useTypeEq(useType))
                .fetchOne();
    }

    private BooleanExpression valLike(AdminCateEnum cate, String val) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        if (cate !=null && val != null && val.length() > 0) {
            switch (cate) {
                case All:
                    return adminEntity.adminEmail.toLowerCase().contains(val.toLowerCase())
                            .or(adminEntity.fullName.toLowerCase().contains(val.toLowerCase()))
                            .or(adminEntity.description.toLowerCase().contains(val.toLowerCase()));
                case Email:
                    return adminEntity.adminEmail.toLowerCase().contains(val.toLowerCase());
                case Name:
                    return adminEntity.fullName.toLowerCase().contains(val.toLowerCase());
                case PemissionAssign:
                    return permissionSetEntity.labelVal.toLowerCase().contains(val.toLowerCase());
                case Description:
                    return adminEntity.description.toLowerCase().contains(val.toLowerCase());
                case RegUsrName:
                    return adminEntity.fullName.toLowerCase().contains(val.toLowerCase());
                case ModUsrName:
                    return adminEntity2.fullName.toLowerCase().contains(val.toLowerCase());
            }
        }
        return null;
    }

    private BooleanExpression useTypeEq(YnTypeEnum useType) {
        if (useType != null)
            return adminEntity.useType.eq(useType);
        return null;
    }

    private OrderSpecifier sorterOrder(AdminSorterEnum sorter) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        if (sorter == null)
            return adminEntity.regDt.desc();

        if (sorter == AdminSorterEnum.AdminIdAsc)
            return adminEntity.adminEmail.asc();

        if (sorter == AdminSorterEnum.AdminIdDesc)
            return adminEntity.adminEmail.desc();

        if (sorter == AdminSorterEnum.FullNameAsc)
            return adminEntity.fullName.asc();

        if (sorter == AdminSorterEnum.FullNameDesc)
            return adminEntity.fullName.desc();

        if (sorter == AdminSorterEnum.UseYnAsc)
            return adminEntity.useType.asc();

        if (sorter == AdminSorterEnum.UseYnDesc)
            return adminEntity.useType.desc();

        if (sorter == AdminSorterEnum.PermissionAssignAsc)
            return permissionSetEntity.labelVal.asc();

        if (sorter == AdminSorterEnum.PermissionAssignDesc)
            return permissionSetEntity.labelVal.desc();

        if (sorter == AdminSorterEnum.CreatedAsc)
            return adminEntity.regDt.asc();

        if (sorter == AdminSorterEnum.CreatedDesc)
            return adminEntity.regDt.desc();

        if (sorter == AdminSorterEnum.LastModifiedAsc)
            return adminEntity.modDt.asc();

        if (sorter == AdminSorterEnum.LastModifiedDesc)
            return adminEntity.modDt.desc();

        if (sorter == AdminSorterEnum.DescriptionAsc)
            return adminEntity.description.asc();

        if (sorter == AdminSorterEnum.DescriptionDesc)
            return adminEntity.description.desc();

        if (sorter == AdminSorterEnum.CreaterNameAsc)
            return adminEntity.fullName.asc();

        if (sorter == AdminSorterEnum.CreaterNameDesc)
            return adminEntity.fullName.desc();

        if (sorter == AdminSorterEnum.ModifierNameAsc)
            return adminEntity2.fullName.asc();

        if (sorter == AdminSorterEnum.ModifierNameDesc)
            return adminEntity2.fullName.desc();

        return adminEntity.regDt.desc();
    }

}
