package com.sparwk.adminnode.admin.jpa.repository.projects.dsl;

import com.sparwk.adminnode.admin.biz.v1.projects.dto.ProjectCowritersViewListDto;
import com.sparwk.adminnode.admin.biz.v1.projects.dto.ProjectViewListDto;

import java.util.List;

public interface ProjectCowritersViewCustomRepository {
    List<ProjectCowritersViewListDto> findQryAll(Long id);

    long countQryAll(Long id);
}
