package com.sparwk.adminnode.admin.jpa.repository.permissionSet.dsl;

import com.sparwk.adminnode.admin.biz.v1.permissionSet.dto.PermissionSetDto;
import com.sparwk.adminnode.admin.jpa.entity.PeriodTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.permissionSet.PermissionSetCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.permissionSet.PermissionSetSorterEnum;
import org.springframework.data.domain.PageRequest;

import java.util.List;

public interface PermissionSetCustomRepository {

    List<PermissionSetDto> findQryAll(PermissionSetCateEnum cate,
                                      String labelVal,
                                      YnTypeEnum useType,
                                      PeriodTypeEnum periodType,
                                      String sdate,
                                      String edate,
                                      PermissionSetSorterEnum sorter,
                                      PageRequest pageRequest);

    long countQryAll(PermissionSetCateEnum cate,
                                      String labelVal,
                                      YnTypeEnum useType,
                                      PeriodTypeEnum periodType,
                                      String sdate,
                                      String edate);

    List<PermissionSetDto> findQryUseY(PermissionSetCateEnum cate, String labelVal);


}
