package com.sparwk.adminnode.admin.jpa.entity.projects;

import com.sparwk.adminnode.admin.jpa.entity.BaseEntity;
import com.sparwk.adminnode.admin.jpa.entity.projects.id.ProjectReferenceFileId;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor
@IdClass(ProjectReferenceFileId.class)
@Table(name = "tb_project_reference_file")
public class ProjectReferenceFile extends BaseEntity {

    @Id
    @Column(name = "proj_id", nullable = false)
    private long projId;
    @Id
    @GeneratedValue(generator = "tb_project_reference_file_seq")
    @Column(name = "file_fef_seq", nullable = false)
    private long fileFefSeq;
    @Column(name = "upload_type_cd", nullable = false)
    private String uploadTypeCd;
    @Column(name = "file_name", nullable = false)
    private String fileName;
    @Column(name = "file_path", nullable = false)
    private String filePath;
    @Column(name = "file_size", nullable = false)
    private int fileSize;
    @Column(name = "ref_url", nullable = false)
    private String refUrl;

    @Builder
    ProjectReferenceFile(
        long projId,
        long fileFefSeq,
        String uploadTypeCd,
        String fileName,
        String filePath,
        int fileSize,
        String refUrl

    ) {
        this.projId = projId;
        this.fileFefSeq = fileFefSeq;
        this.uploadTypeCd = uploadTypeCd;
        this.fileName = fileName;
        this.filePath = filePath;
        this.fileSize = fileSize;
        this.refUrl = refUrl;
    }

//    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
//    @JoinColumn(name = "proj_id", referencedColumnName = "proj_id",insertable = false,updatable = false)
//    private Project project;

}
