package com.sparwk.adminnode.admin.jpa.entity.profile;

import com.sparwk.adminnode.admin.jpa.entity.BaseEntity;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_profile")
@DynamicUpdate
@TableGenerator(
        name = "tb_profile_seq_generator",
        table = "tb_profile_sequences",
        pkColumnValue = "tb_profile_seq",
        allocationSize = 1,
        initialValue = 1000000
)
public class ProfileEntity extends BaseEntity {
    @Id
    @GeneratedValue(
            strategy = GenerationType.TABLE,
            generator = "tb_profile_seq_generator")
    @Column(name = "profile_id")
    private Long profileId;

    @Column(name = "accnt_id")
    private Long accntId;

    @Column(name = "mattermost_id", nullable = true, length = 10)
    private String mattermostId;

    @Column(name = "full_name", nullable = true, length = 100)
    private String fullName;

    @Enumerated(EnumType.STRING)
    @Column(name = "stage_name_yn", nullable = true, length = 1)
    private YnTypeEnum stageNameYn;

    @Column(name = "bth_year", nullable = true, length = 4)
    private String bthYear;

    @Column(name = "bth_month", nullable = true, length = 2)
    private String bthMonth;

    @Column(name = "bth_day", nullable = true, length = 2)
    private String bthDay;

    @Enumerated(EnumType.STRING)
    @Column(name = "hire_me_yn", nullable = true, length = 1)
    private YnTypeEnum hireMeYn;

    @Column(name = "bio", nullable = true, length = 100)
    private String bio;

    @Column(name = "headline", nullable = true, length = 100)
    private String headline;

    @Column(name = "current_city_country_cd", nullable = true, length = 9)
    private String currentCityCountryCd;

    @Column(name = "current_city_nm", nullable = true, length = 50)
    private String currentCityNm;

    @Column(name = "home_town_country_cd", nullable = true, length = 9)
    private String homeTownCountryCd;

    @Column(name = "home_town_nm", nullable = true, length = 50)
    private String homeTownNm;

    @Column(name = "ipi_info", nullable = true, length = 30)
    private String ipiInfo;

    @Column(name = "cae_info", nullable = true, length = 30)
    private String caeInfo;

    @Column(name = "isni_info", nullable = true, length = 30)
    private String isniInfo;

    @Column(name = "ipn_info", nullable = true, length = 30)
    private String ipnInfo;

    @Column(name = "nro_type_cd", nullable = true, length = 9)
    private String nroTypeCd;

    @Column(name = "nro_info", nullable = true, length = 30)
    private String nroInfo;

    @Enumerated(EnumType.STRING)
    @Column(name = "verify_yn", nullable = true, length = 1)
    private YnTypeEnum verifyYn;

    @Enumerated(EnumType.STRING)
    @Column(name = "use_yn", nullable = true, length = 1)
    private YnTypeEnum useYn;

    @Column(name = "accomplishments_info", nullable = true, length = 1000)
    private String accomplishmentsInfo;

    @Column(name = "profile_img_url", nullable = true, length = 200)
    private String profileImgUrl;

    @Column(name = "profile_bgd_img_url", nullable = true, length = 200)
    private String profileBgdImgUrl;

    @Builder
    ProfileEntity(
            Long profileId,
            String mattermostId,
            String fullName,
            YnTypeEnum stageNameYn,
            String bthYear,
            String bthMonth,
            String bthDay,
            YnTypeEnum hireMeYn,
            String bio,
            String headline,
            String currentCityCountryCd,
            String currentCityNm,
            String homeTownCountryCd,
            String homeTownNm,
            String ipiInfo,
            String caeInfo,
            String isniInfo,
            String ipnInfo,
            String nroTypeCd,
            String nroInfo,
            YnTypeEnum verifyYn,
            YnTypeEnum useYn,
            String accomplishmentsInfo,
            String profileImgUrl,
            String profileBgdImgUrl
    ) {
        this.profileId = profileId;
        this.mattermostId = mattermostId;
        this.fullName = fullName;
        this.stageNameYn = stageNameYn;
        this.bthYear = bthYear;
        this.bthMonth = bthMonth;
        this.bthDay = bthDay;
        this.hireMeYn = hireMeYn;
        this.bio = bio;
        this.headline = headline;
        this.currentCityCountryCd = currentCityCountryCd;
        this.currentCityNm = currentCityNm;
        this.homeTownCountryCd = homeTownCountryCd;
        this.homeTownNm = homeTownNm;
        this.ipiInfo = ipiInfo;
        this.caeInfo = caeInfo;
        this.isniInfo = isniInfo;
        this.ipnInfo = ipnInfo;
        this.nroTypeCd = nroTypeCd;
        this.nroInfo = nroInfo;
        this.verifyYn = verifyYn;
        this.useYn = useYn;
        this.accomplishmentsInfo = accomplishmentsInfo;
        this.profileImgUrl = profileImgUrl;
        this.profileBgdImgUrl = profileBgdImgUrl;
    }

    public void updateVerifyYn(
            YnTypeEnum verifyYn
    ) {
        this.verifyYn = verifyYn;

    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = ProfileGenderEntity.class)
    @JoinColumn(name = "profile_id", referencedColumnName = "profile_id")
    private List<ProfileGenderEntity> profileGender;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = ProfileInterestMeta.class)
    @JoinColumn(name = "profile_id", referencedColumnName = "profile_id")
    private List<ProfileInterestMeta> profileInterestMeta;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = ProfileLanguageEntity.class)
    @JoinColumn(name = "profile_id", referencedColumnName = "profile_id")
    private List<ProfileLanguageEntity> profileLanguageEntity;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = ProfilePositionEntity.class)
    @JoinColumn(name = "profile_id", referencedColumnName = "profile_id")
    private List<ProfilePositionEntity> profilePositionEntity;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = ProfileEducation.class)
    @JoinColumn(name = "profile_id", referencedColumnName = "profile_id")
    private List<ProfileEducation> profileEducation;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = ProfileCareer.class)
    @JoinColumn(name = "profile_id", referencedColumnName = "profile_id")
    private List<ProfileCareer> profileCareer;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = ProfileSkillEntity.class)
    @JoinColumn(name = "profile_id", referencedColumnName = "profile_id")
    private List<ProfileSkillEntity> profileSkillEntity;

}
