package com.sparwk.adminnode.admin.jpa.entity.account;

import lombok.Getter;

@Getter
public enum AccountUserCateEnum {

    All("All"),
    Email("Email"),
    FullName("FullName"),
    StageName("StageName"),
    Company("Company"),
    Role("Role"),
    Gender("Gender");

    private String cate;

    AccountUserCateEnum(String cate) {
        this.cate = cate;
    }
}
