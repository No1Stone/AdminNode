package com.sparwk.adminnode.admin.jpa.repository.profile;

import com.sparwk.adminnode.admin.jpa.entity.profile.ProfileCompanyEntity;
import com.sparwk.adminnode.admin.jpa.entity.profile.ProfileEntity;
import com.sparwk.adminnode.admin.jpa.repository.profile.dsl.ProfileCompanyCustomRepository;
import com.sparwk.adminnode.admin.jpa.repository.profile.dsl.ProfileCustomRepository;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProfileCompanyRepository extends JpaRepository<ProfileCompanyEntity, Long>, ProfileCompanyCustomRepository {

}
