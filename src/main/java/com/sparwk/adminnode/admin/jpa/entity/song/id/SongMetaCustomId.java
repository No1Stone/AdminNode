package com.sparwk.adminnode.admin.jpa.entity.song.id;

import lombok.Data;

import java.io.Serializable;

@Data
public class SongMetaCustomId implements Serializable {
    private long songId;
    private long metadataSeq;
}
