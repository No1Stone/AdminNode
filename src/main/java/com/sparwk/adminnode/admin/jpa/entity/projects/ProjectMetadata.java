package com.sparwk.adminnode.admin.jpa.entity.projects;


import com.sparwk.adminnode.admin.jpa.entity.BaseEntity;
import com.sparwk.adminnode.admin.jpa.entity.projects.id.ProjectMetadataId;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor
@IdClass(ProjectMetadataId.class)
@Table(name = "tb_project_metadata")
public class ProjectMetadata extends BaseEntity {

    @Id
    @Column(name = "proj_id", nullable = false)
    private long projId;
    @Id
    @Column(name = "attr_type_cd", nullable = false)
    private String attrTypeCd;
    @Id
    @Column(name = "attr_dtl_cd", nullable = false)
    private String attrDtlCd;

    @Builder
    ProjectMetadata(
            long projId,
            String attrTypeCd,
            String attrDtlCd
    ) {
            this.projId =   projId;
            this.attrTypeCd = attrTypeCd.trim();
            this.attrDtlCd   = attrDtlCd.trim();
    }

//    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
//    @JoinColumn(name = "proj_id", referencedColumnName = "proj_id",insertable = false,updatable = false)
//    private Project project;
}
