package com.sparwk.adminnode.admin.jpa.repository.account.dsl;

import com.sparwk.adminnode.admin.biz.v1.account.dto.CompanyMemberViewListDto;
import com.sparwk.adminnode.admin.jpa.entity.PeriodTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.account.*;
import org.springframework.data.domain.PageRequest;

import java.util.List;

public interface AccountCompanyViewCustomRepository {
    //회사정보 보기(인증전 후 포함)
    List<CompanyMemberViewListDto> findQryVerificationCompany(VerificationCompanyAccountCateEnum cate,
                                                            String val,
                                                            PeriodTypeEnum periodType,
                                                            String sdate,
                                                            String edate,
                                                            VerificationCompanyAccountFilterEnum filter,
                                                            VerificationCompanyAccountSorterEnum sorter,
                                                            PageRequest pageRequest);

    //회사정보 보기(인증전 후 포함)
    long countQryVerificationCompany(VerificationCompanyAccountCateEnum cate,
                                                        String val,
                                                        PeriodTypeEnum periodType,
                                                        String sdate,
                                                        String edate,
                                   VerificationCompanyAccountFilterEnum filter);


    //회사정보 보기 인증만 보기
    List<CompanyMemberViewListDto> findQryOfficeMember(MaintenanceOfficeMemberCateEnum cate,
                                                    String val,
                                                    PeriodTypeEnum periodType,
                                                    String sdate,
                                                    String edate,
                                                       MaintenanceOfficeMemberSorterEnum sorter,
                                                    PageRequest pageRequest);

    //회사정보 보기 인증만 보기
    long countQryOfficeMember(MaintenanceOfficeMemberCateEnum cate,
                                                        String val,
                                                        PeriodTypeEnum periodType,
                                                        String sdate,
                                                        String edate);


}
