package com.sparwk.adminnode.admin.jpa.entity.inquary;

import lombok.Getter;

@Getter
public enum InquarySorterEnum {

    EmailAsc("EmailAsc"), EmailDesc("EmailDesc"),
    BrowserAsc("BrowserAsc"), BrowserDesc("BrowserDesc"),
    OSAsc("OSAsc"), OSDesc("OSDesc"),
    DeviceAsc("DeviceAsc"), DeviceDesc("DeviceDesc"),
    LoginTimeAsc("LoginTimeAsc"), LoginTimeDesc("LoginTimeDesc"),
    IPAsc("IPAsc"), IPDesc("IPDesc")
            ;

    private String sorter;

    InquarySorterEnum(String sorter) {
        this.sorter = sorter;
    }

}
