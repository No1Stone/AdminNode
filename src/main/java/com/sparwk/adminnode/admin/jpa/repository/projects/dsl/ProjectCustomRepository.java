package com.sparwk.adminnode.admin.jpa.repository.projects.dsl;

import com.sparwk.adminnode.admin.biz.v1.projects.dto.*;
import com.sparwk.adminnode.admin.jpa.entity.PeriodTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.projects.ProjcetCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.projects.ProjectSorterEnum;
import org.springframework.data.domain.PageRequest;

import java.util.List;

public interface ProjectCustomRepository {
    List<ProjectListDto> findQryAll(ProjcetCateEnum cate,
                                    String val,
                                    YnTypeEnum completeVal,
                                    PeriodTypeEnum periodType,
                                    String sdate,
                                    String edate,
                                    ProjectSorterEnum sorter,
                                    PageRequest pageRequest);

    List<ProjectListDto> findQryCompany(ProjcetCateEnum cate,
                                    String val,
                                    YnTypeEnum completeVal,
                                    PeriodTypeEnum periodType,
                                    String sdate,
                                    String edate,
                                    ProjectSorterEnum sorter,
                                    PageRequest pageRequest);

    List<ProjectMetadataDto> findQrySongMeta(String pcode, Long projId);
    List<ProjectMetadataDto> findQryRolesMeta(String pcode, Long projId);
    List<ProjectMetadataDto> findQryCommonMeta(String pcode, String code, Long projId);
    List<ProjectMetadataDto> findQryCountryMeta(String pcode, Long projId);
    List<ProjectProfileDto> findQryProjInviteCom(Long projId);
    ProjectMetadataDto findQryWorkLocatMeta(String pcode, String code, Long projId);

    List<ProjectReferenceFileDto> findQryProjectReferenceFileList(Long projId);
    List<ProjectSongDto> findQryProjectSongList(Long projId);


}
