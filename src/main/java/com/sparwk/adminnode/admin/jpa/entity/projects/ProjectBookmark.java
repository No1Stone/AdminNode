package com.sparwk.adminnode.admin.jpa.entity.projects;

import com.sparwk.adminnode.admin.jpa.entity.BaseEntity;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.projects.id.ProjectBookmarkId;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor
@IdClass(ProjectBookmarkId.class)
@Table(name = "tb_project_bookmark")
public class ProjectBookmark extends BaseEntity {

    @Id
//    @GeneratedValue(generator = "tb_project_bookmark_profile_id_seq")
    @Column(name = "profile_id", nullable = false)
    private Long profileId;
    @Id
    @Column(name = "proj_id", nullable = false)
    private Long projId;
    @Column(name = "bookmark_yn", nullable = false)
    private YnTypeEnum bookmarkYn;
    @Column(name = "reg_usr", updatable = false)

    //first registrant
    @CreatedBy
    private Long regUsr;
    @Column(name = "reg_dt", updatable = false)
    @CreatedDate//serverTime
    //@CreationTimestamp//dbtime
    private LocalDateTime regDt;
    @Column(name = "mod_usr")
    @LastModifiedBy
    private Long modUsr;
    @Column(name = "mod_dt")
    @LastModifiedDate//serverTime
    //@UpdateTimestamp//dbtime
    private LocalDateTime modDt;

    @Builder
    ProjectBookmark(
            Long profileId,
            Long projId,
            YnTypeEnum bookmarkYn,
            Long regUsr,
            LocalDateTime regDt,
            Long modUsr,
            LocalDateTime modDt
    ) {
            this.profileId      = profileId;
            this.projId        = projId;
            this.bookmarkYn      = bookmarkYn;
            this.regUsr                 =       regUsr;
            this.regDt                  =       regDt;
            this.modUsr                 =       modUsr;
            this.modDt                  =       modDt;
    }

}
