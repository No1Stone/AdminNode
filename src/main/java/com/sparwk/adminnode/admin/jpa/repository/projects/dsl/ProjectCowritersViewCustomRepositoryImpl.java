package com.sparwk.adminnode.admin.jpa.repository.projects.dsl;

import com.querydsl.core.types.Projections;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.sparwk.adminnode.admin.biz.v1.projects.dto.ProjectCowritersViewListDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.sparwk.adminnode.admin.jpa.entity.projects.QProjectCowritersViewEntity.projectCowritersViewEntity;

@Repository
public class ProjectCowritersViewCustomRepositoryImpl implements ProjectCowritersViewCustomRepository {

    private final JPAQueryFactory jpaQueryFactory;

    private final Logger logger = LoggerFactory.getLogger(ProjectCowritersViewCustomRepositoryImpl.class);

    public ProjectCowritersViewCustomRepositoryImpl(JPAQueryFactory jpaQueryFactory) {
        this.jpaQueryFactory = jpaQueryFactory;
    }

    @Override
    public List<ProjectCowritersViewListDto> findQryAll(Long id) {
        return jpaQueryFactory.select(Projections.bean(ProjectCowritersViewListDto.class,

                        projectCowritersViewEntity.projId,
                        projectCowritersViewEntity.profileId,
                        projectCowritersViewEntity.fullName,
                        projectCowritersViewEntity.accntEmail,
                        projectCowritersViewEntity.phoneNumber,
                        projectCowritersViewEntity.companyName,
                        projectCowritersViewEntity.cnt
                        )
                )
                .from(projectCowritersViewEntity)
                .where(projectCowritersViewEntity.projId.eq(id))
                .fetch();
    }

    @Override
    public long countQryAll(Long id) {
        return jpaQueryFactory.select(
                        projectCowritersViewEntity.profileId.count()
                )
                .from(projectCowritersViewEntity)
                .where(projectCowritersViewEntity.projId.eq(id))
                .fetchOne();
    }
}
