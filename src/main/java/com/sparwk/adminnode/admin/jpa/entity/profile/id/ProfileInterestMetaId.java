package com.sparwk.adminnode.admin.jpa.entity.profile.id;

import lombok.Data;

import java.io.Serializable;

@Data
public class ProfileInterestMetaId implements Serializable {
    private String interestTypeCd;
    private Long profileId;
}
