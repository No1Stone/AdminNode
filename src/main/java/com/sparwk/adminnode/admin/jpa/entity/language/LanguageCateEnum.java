package com.sparwk.adminnode.admin.jpa.entity.language;

import lombok.Getter;

@Getter
public enum LanguageCateEnum {

    All("All"),
    LanguageName("LanguageName"),
    LangyageFamily("LangyageFamily"),
    NativeLanguage("NativeLanguage"),
    ISO_6391("ISO_6391"),
    ISO_6392T("ISO_6392T"),
    ISO_6392B("ISO_6392B"),
    RegUsrName("RegUsrName"),
    ModUsrName("ModUsrName")
    ;

    private String languageCateEnum;

    LanguageCateEnum(String languageCateEnum) {
        this.languageCateEnum = languageCateEnum;
    }
}
