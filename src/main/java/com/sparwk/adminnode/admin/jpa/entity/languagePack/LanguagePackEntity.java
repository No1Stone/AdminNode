package com.sparwk.adminnode.admin.jpa.entity.languagePack;

import com.sparwk.adminnode.admin.jpa.entity.BaseEntity;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name="tb_language_pack")
public class LanguagePackEntity extends BaseEntity implements Serializable {

    @Id
    @GeneratedValue(
            generator = "tb_language_pack_seq"
    )
    @Column(name = "pack_id")
    private Long packId;

    @Column(name = "language_cd", length = 9)
    private String languageCd;

    @Column(name = "national_flag_url", length = 200)
    private String nationalFlagUrl;

    @Column(name = "json_upload_url", length = 200)
    private String jsonUploadUrl;

    @Column(name = "pack_version")
    private int packVersion;

    @Enumerated(EnumType.STRING)
    @Column(name = "use_yn", length = 1)
    private YnTypeEnum useType;


    @Builder
    public LanguagePackEntity(
                            Long packId,
                            String languageCd,
                            String nationalFlagUrl,
                            String jsonUploadUrl,
                            int packVersion,
                            YnTypeEnum useType
                          ) {
        this.packId = packId;
        this.languageCd = languageCd;
        this.nationalFlagUrl = nationalFlagUrl;
        this.jsonUploadUrl = jsonUploadUrl;
        this.packVersion = packVersion;
        this.useType = useType;
    }

}
