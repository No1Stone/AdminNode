package com.sparwk.adminnode.admin.jpa.entity.projects;

import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "view_admin_project_list")
public class ProjectViewEntity {
    @Id
    @Column(name = "proj_id", nullable = true)
    private Long projId;
    @Column(name = "proj_title", nullable = true)
    private String projTitle;
    @Column(name = "avatar_file_url", nullable = true)
    private String avatarFileUrl;
    @Column(name = "pitch_proj_type_cd", nullable = true)
    private String pitchProjTypeCd;
    @Column(name = "pitch_proj_type_cd_name", nullable = true)
    private String pitchProjTypeCdName;
    @Column(name = "proj_owner", nullable = true)
    private Long projOwner;
    @Column(name = "proj_owner_name", nullable = true)
    private String projOwnerName;
    @Column(name = "comp_profile_id", nullable = true)
    private Long compProfileId;
    @Column(name = "comp_profile_name", nullable = true)
    private String compProfileName;
    @Column(name = "proj_genre_cd", nullable = true)
    private String projGenreCd;
    @Column(name = "proj_genre_name", nullable = true)
    private String projGenreName;
    @Column(name = "proj_cond_cd", nullable = true)
    private String projCondCd;
    @Column(name = "proj_cond_cd_name", nullable = true)
    private String projCondCdName;
    @Column(name = "reg_usr", nullable = true)
    private Long regUsr;
    @Column(name = "reg_dt", updatable = false)
    private LocalDateTime regDt;
    @Column(name = "mod_usr", nullable = true)
    private Long modUsr;
    @Column(name = "mod_dt", updatable = false)
    private LocalDateTime modDt;
    @Enumerated(EnumType.STRING)
    @Column(name = "complete_yn", nullable = true)
    private YnTypeEnum completeYn;

    @Builder
    ProjectViewEntity(
            Long projId,
            String projTitle,
            String avatarFileUrl,
            String pitchProjTypeCd,
            String pitchProjTypeCdName,
            Long projOwner,
            String projOwnerName,
            Long compProfileId,
            String compProfileName,
            String projGenreCd,
            String projGenreName,
            String projCondCd,
            String projCondCdName,
            Long regUsr,
            LocalDateTime regDt,
            Long modUsr,
            LocalDateTime modDt,
            YnTypeEnum completeYn
    ) {
        this.projId = projId;
        this.projTitle = projTitle;
        this.avatarFileUrl = avatarFileUrl;
        this.pitchProjTypeCd = pitchProjTypeCd;
        this.pitchProjTypeCdName = pitchProjTypeCdName;
        this.projOwner = projOwner;
        this.projOwnerName = projOwnerName;
        this.compProfileId = compProfileId;
        this.compProfileName = compProfileName;
        this.projGenreCd = projGenreCd;
        this.projGenreName = projGenreName;
        this.projCondCd = projCondCd;
        this.projCondCdName = projCondCdName;
        this.regUsr = regUsr;
        this.regDt = regDt;
        this.modUsr = modUsr;
        this.modDt = modDt;
        this.completeYn = completeYn;
    }

}
