package com.sparwk.adminnode.admin.jpa.repository.profile;

import com.sparwk.adminnode.admin.jpa.entity.profile.ProfileEntity;
import com.sparwk.adminnode.admin.jpa.entity.profile.ProfilePositionEntity;
import com.sparwk.adminnode.admin.jpa.repository.profile.dsl.ProfileCustomRepository;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProfilePositionRepository extends JpaRepository<ProfilePositionEntity, Long> {
    List<ProfilePositionEntity> findByProfileId(Long id);
}


