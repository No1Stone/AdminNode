package com.sparwk.adminnode.admin.jpa.entity.account;

import com.sparwk.adminnode.admin.jpa.entity.BaseEntity;
import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_account_company_detail")
@DynamicUpdate
public class AccountCompanyDetailEntity extends BaseEntity {

    @Id
    @Column(name = "accnt_id")
    private Long accntId;
    @Column(name = "company_name", nullable = true, length = 100)
    private String companyName;
    @Column(name = "post_cd", nullable = true, length = 20)
    private String postCd;
    @Column(name = "region", nullable = true, length = 50)
    private String region;
    @Column(name = "city", nullable = true, length = 20)
    private String city;
    @Column(name = "addr1", nullable = true, length = 200)
    private String addr1;
    @Column(name = "addr2", nullable = true, length = 200)
    private String addr2;
    @Column(name = "contct_first_name", nullable = true, length = 50)
    private String contctFirstName;
    @Column(name = "contct_midle_name", nullable = true, length = 50)
    private String contctMidleName;
    @Column(name = "contct_last_name", nullable = true, length = 50)
    private String contctLastName;
    @Column(name = "contct_email", nullable = true, length = 100)
    private String contctEmail;



    @Builder
    AccountCompanyDetailEntity(
            Long accntId,
            String companyName,
            String postCd,
            String region,
            String city,
            String addr1,
            String addr2,
            String contctFirstName,
            String contctMidleName,
            String contctLastName,
            String contctEmail
    ) {
        this.accntId=accntId;
        this.companyName=companyName;
        this.postCd=postCd;
        this.region=region;
        this.city=city;
        this.addr1=addr1;
        this.addr2=addr2;
        this.contctFirstName=contctFirstName;
        this.contctMidleName=contctMidleName;
        this.contctLastName=contctLastName;
        this.contctEmail=contctEmail;
    }

//    @OneToOne(mappedBy = "accountCompanyDetail")
//    private com.sparwk.personalizationnode.account.jpa.entity.Account account;
}
