package com.sparwk.adminnode.admin.jpa.repository.language.dsl;

import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.sparwk.adminnode.admin.biz.v1.language.dto.LanguageDto;
import com.sparwk.adminnode.admin.biz.v1.language.dto.LanguageExcelDto;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.admin.QAdminEntity;
import com.sparwk.adminnode.admin.jpa.entity.commonCode.CommonSorterEnum2;
import com.sparwk.adminnode.admin.jpa.entity.language.LanguageCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.language.LanguageSorterEnum;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.sparwk.adminnode.admin.jpa.entity.admin.QAdminEntity.adminEntity;
import static com.sparwk.adminnode.admin.jpa.entity.commonCode.QCommonOrganizationCodeEntity.commonOrganizationCodeEntity;
import static com.sparwk.adminnode.admin.jpa.entity.language.QLanguageEntity.languageEntity;

@Repository
public class LanguageCustomRepositoryImpl implements LanguageCustomRepository {

    private final JPAQueryFactory jpaQueryFactory;

    public LanguageCustomRepositoryImpl(JPAQueryFactory jpaQueryFactory) {
        this.jpaQueryFactory = jpaQueryFactory;
    }

    @Override
    public List<LanguageDto> findQryAll(LanguageCateEnum cate,
                                        String val,
                                        YnTypeEnum useType,
                                        LanguageSorterEnum sorter,
                                        PageRequest pageRequest) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(Projections.bean(LanguageDto.class,
                                languageEntity.languageSeq,
                                languageEntity.language,
                                languageEntity.languageFamily,
                                languageEntity.nativeLanguage,
                                languageEntity.useType,
                                languageEntity.iso639_1,
                                languageEntity.iso639_2b,
                                languageEntity.iso639_2t,
                                languageEntity.languageCd,
                                languageEntity.regUsr,
                                adminEntity.fullName.as("regUsrName"),
                                languageEntity.regDt,
                                languageEntity.modUsr,
                                adminEntity2.fullName.as("modUsrName"),
                                languageEntity.modDt
                        )
                )
                .from(languageEntity)
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(commonOrganizationCodeEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(commonOrganizationCodeEntity.modUsr))
                .where(valLike(cate, val), useTypeEq(useType))
                .limit(pageRequest.getPageSize())
                .offset(pageRequest.getPageSize() * (pageRequest.getPageNumber() - 1))
                .orderBy(sorterOrder(sorter))
                .fetch();
    }

    @Override
    public long countQryAll(LanguageCateEnum cate,
                                        String val,
                                        YnTypeEnum useType) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(
                                languageEntity.languageSeq.count()
                )
                .from(languageEntity)
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(commonOrganizationCodeEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(commonOrganizationCodeEntity.modUsr))
                .where(valLike(cate, val), useTypeEq(useType))
                .fetchOne();
    }

    @Override
    public List<LanguageDto> findQryUseY() {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(Projections.bean(LanguageDto.class,
                                languageEntity.languageSeq,
                                languageEntity.language,
                                languageEntity.languageFamily,
                                languageEntity.nativeLanguage,
                                languageEntity.useType,
                                languageEntity.iso639_1,
                                languageEntity.iso639_2b,
                                languageEntity.iso639_2t,
                                languageEntity.languageCd,
                                languageEntity.regUsr,
                                adminEntity.fullName.as("regUsrName"),
                                languageEntity.regDt,
                                languageEntity.modUsr,
                                adminEntity2.fullName.as("modUsrName"),
                                languageEntity.modDt
                        )
                )
                .from(languageEntity)
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(commonOrganizationCodeEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(commonOrganizationCodeEntity.modUsr))
                .where(useTypeEq(YnTypeEnum.Y))
                .orderBy(sorterOrder(LanguageSorterEnum.NameAsc))
                .fetch();
    }

    @Override
    public long countQryUseY() {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(
                        languageEntity.languageSeq.count()
                )
                .from(languageEntity)
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(commonOrganizationCodeEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(commonOrganizationCodeEntity.modUsr))
                .where(useTypeEq(YnTypeEnum.Y))
                .fetchOne();
    }

    @Override
    public List<LanguageExcelDto> findExcelList() {

        return jpaQueryFactory.select(Projections.bean(LanguageExcelDto.class,
                                languageEntity.languageSeq,
                                languageEntity.language,
                                languageEntity.languageFamily,
                                languageEntity.nativeLanguage,
                                languageEntity.useType,
                                languageEntity.iso639_1,
                                languageEntity.iso639_2b,
                                languageEntity.iso639_2t,
                                languageEntity.languageCd
                        )
                )
                .from(languageEntity)
                .orderBy(sorterOrder(LanguageSorterEnum.NameAsc))
                .fetch();
    }


    private BooleanExpression valLike(LanguageCateEnum cate, String val) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        if (cate !=null && val != null && val.length() > 0) {
            switch (cate) {
                case All:
                    return languageEntity.language.toLowerCase().contains(val.toLowerCase())
                            .or(languageEntity.nativeLanguage.toLowerCase().contains(val.toLowerCase()));
                case LanguageName:
                    return languageEntity.language.toLowerCase().contains(val.toLowerCase());
                case NativeLanguage:
                    return languageEntity.nativeLanguage.toLowerCase().contains(val.toLowerCase());
                case LangyageFamily:
                    return languageEntity.languageFamily.toLowerCase().contains(val.toLowerCase());
                case ISO_6391:
                    return languageEntity.iso639_1.toLowerCase().contains(val.toLowerCase());
                case ISO_6392B:
                    return languageEntity.iso639_2b.toLowerCase().contains(val.toLowerCase());
                case ISO_6392T:
                    return languageEntity.iso639_2t.toLowerCase().contains(val.toLowerCase());
                case RegUsrName:
                    return adminEntity.fullName.toLowerCase().contains(val.toLowerCase());
                case ModUsrName:
                    return adminEntity2.fullName.toLowerCase().contains(val.toLowerCase());
            }
        }
        return null;
    }

    private BooleanExpression idEq(Long id) {
        if (id != null)
            return languageEntity.languageSeq.eq(id);
        return null;
    }

    private BooleanExpression useTypeEq(YnTypeEnum useType) {
        if (useType != null)
            return languageEntity.useType.eq(useType);
        return null;
    }

    private OrderSpecifier sorterOrder(LanguageSorterEnum sorter) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        if (sorter == null)
            return languageEntity.regDt.desc();

        if (sorter == LanguageSorterEnum.NameAsc)
            return languageEntity.language.asc();

        if (sorter == LanguageSorterEnum.NameDesc)
            return languageEntity.language.desc();

        if (sorter == LanguageSorterEnum.FamilyAsc)
            return languageEntity.languageFamily.asc();

        if (sorter == LanguageSorterEnum.FamilyDesc)
            return languageEntity.languageFamily.desc();

        if (sorter == LanguageSorterEnum.NativeAsc)
            return languageEntity.nativeLanguage.asc();

        if (sorter == LanguageSorterEnum.NativeDesc)
            return languageEntity.nativeLanguage.desc();

        if (sorter == LanguageSorterEnum.ISO639_1Asc)
            return languageEntity.iso639_1.asc();

        if (sorter == LanguageSorterEnum.ISO639_1Desc)
            return languageEntity.iso639_1.desc();

        if (sorter == LanguageSorterEnum.ISO639_2TAsc)
            return languageEntity.iso639_2t.asc();

        if (sorter == LanguageSorterEnum.ISO639_2TDesc)
            return languageEntity.iso639_2t.desc();

        if (sorter == LanguageSorterEnum.ISO639_2BAsc)
            return languageEntity.iso639_2b.asc();

        if (sorter == LanguageSorterEnum.ISO639_2BDesc)
            return languageEntity.iso639_2b.desc();

        if (sorter == LanguageSorterEnum.CreaterNameAsc)
            return adminEntity.fullName.asc();

        if (sorter == LanguageSorterEnum.CreaterNameDesc)
            return adminEntity.fullName.desc();

        if (sorter == LanguageSorterEnum.ModifierNameAsc)
            return adminEntity2.fullName.asc();

        if (sorter == LanguageSorterEnum.ModifierNameDesc)
            return adminEntity2.fullName.desc();

        return languageEntity.regDt.desc();
    }

}
