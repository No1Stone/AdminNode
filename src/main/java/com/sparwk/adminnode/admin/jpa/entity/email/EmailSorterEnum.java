package com.sparwk.adminnode.admin.jpa.entity.email;

import lombok.Getter;

@Getter
public enum EmailSorterEnum {

    SubjectAsc("SubjectAsc"), SubjectDesc("SubjectDesc"),
    MemberAsc("MemberAsc"), MemberDesc("MemberDesc"),
    SdateAsc("SdateAsc"), SdateDesc("SdateDesc"),
    CreaterNameAsc("CreaterNameAsc"), CreaterNameDesc("CreaterNameDesc")
            ;

    private String sorter;

    EmailSorterEnum(String sorter) {
        this.sorter = sorter;
    }

}
