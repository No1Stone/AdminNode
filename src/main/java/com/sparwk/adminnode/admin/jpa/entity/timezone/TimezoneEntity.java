package com.sparwk.adminnode.admin.jpa.entity.timezone;

import com.sparwk.adminnode.admin.jpa.entity.BaseEntity;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name="tb_admin_timezone_code")
public class TimezoneEntity extends BaseEntity implements Serializable {

    @Id
    @GeneratedValue(
            generator = "tb_admin_timezone_code_timezone_seq_seq"
    )
    @Column(name = "timezone_seq")
    private Long timezoneSeq;

    @Column(name = "timezone_name", length = 100)
    private String timezoneName;

    @Column(name = "continent", length = 15)
    private String continent;

    @Column(name = "city", length = 30)
    private String city;

    @Column(name = "utc_hour", length = 30)
    private String utcHour;

    @Column(name = "diff_time")
    private int diffTime;

    @Enumerated(EnumType.STRING)
    @Column(name = "use_yn", length = 1)
    private YnTypeEnum useType;

    @Builder
    public TimezoneEntity(
                                Long timezoneSeq,
                                String timezoneName,
                                String continent,
                                String city,
                                String utcHour,
                                int diffTime,
                                YnTypeEnum useType
                        ) {
        this.timezoneSeq = timezoneSeq;
        this.timezoneName = timezoneName;
        this.continent = continent;
        this.city = city;
        this.utcHour = utcHour;
        this.diffTime = diffTime;
        this.useType = useType;
    }

    public void updateYn(YnTypeEnum useType) {
        this.useType = useType;
    }

    public void update(
                                String timezoneName,
                                String continent,
                                String city,
                                String utcHour,
                                int diffTime,
                                YnTypeEnum useType
                        ) {
        this.timezoneName = timezoneName;
        this.continent = continent;
        this.city = city;
        this.utcHour = utcHour;
        this.diffTime = diffTime;
        this.useType = useType;
    }

}
