package com.sparwk.adminnode.admin.jpa.repository.admin.dsl;

import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.sparwk.adminnode.admin.biz.v1.admin.dto.AdminActiveLogListDto;
import com.sparwk.adminnode.admin.jpa.entity.PeriodTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.admin.AdminActiveCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.admin.AdminActiveSorterEnum;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static com.sparwk.adminnode.admin.jpa.entity.admin.QAdminActiveLogEntity.adminActiveLogEntity;
import static com.sparwk.adminnode.admin.jpa.entity.admin.QAdminEntity.adminEntity;

@Repository
public class AdminActiveLogCustomRepositoryImpl implements AdminActiveLogCustomRepository {

    private final JPAQueryFactory jpaQueryFactory;

    public AdminActiveLogCustomRepositoryImpl(JPAQueryFactory jpaQueryFactory) {
        this.jpaQueryFactory = jpaQueryFactory;
    }

    @Override
    public List<AdminActiveLogListDto> findQryAll(AdminActiveCateEnum cate,
                                                  String val,
                                                  String sdate,
                                                  String edate,
                                                  AdminActiveSorterEnum sorter,
                                                  PageRequest pageRequest) {
        return jpaQueryFactory.select(Projections.bean(AdminActiveLogListDto.class,
                                adminActiveLogEntity.activeTime,
                                adminEntity.adminId,
                                adminEntity.adminEmail,
                                adminEntity.fullName,
                                adminActiveLogEntity.menuName,
                                adminActiveLogEntity.activeMsg
                        )
                )
                .from(adminActiveLogEntity)
                .leftJoin(adminEntity).on(adminActiveLogEntity.adminId.eq(adminEntity.adminId))
                .where(valLike(cate, val), termBetween(PeriodTypeEnum.Created, sdate, edate))
                .limit(pageRequest.getPageSize())
                .offset(pageRequest.getPageSize() * (pageRequest.getPageNumber() - 1))
                .orderBy(sorterOrder(sorter))
                .fetch();
    }

    @Override
    public long countQryAll(AdminActiveCateEnum cate,
                                        String val,
                                        String sdate,
                                        String edate) {
        return jpaQueryFactory.select(
                        adminActiveLogEntity.adminActiveLogSeq.count()
                )
                .from(adminActiveLogEntity)
                .leftJoin(adminEntity).on(adminActiveLogEntity.adminId.eq(adminEntity.adminId))
                .where(valLike(cate, val), termBetween(PeriodTypeEnum.Created, sdate, edate))
                .fetchOne();
    }

    private BooleanExpression valLike(AdminActiveCateEnum cate, String val) {
        if (cate !=null && val != null && val.length() > 0) {
            switch (cate) {
                case All:
                    return adminActiveLogEntity.menuName.toLowerCase().contains(val.toLowerCase())
                            .or(adminEntity.fullName.toLowerCase().contains(val.toLowerCase()))
                            .or(adminEntity.adminEmail.toLowerCase().contains(val.toLowerCase()));
                case AdminId:
                    return adminEntity.adminEmail.toLowerCase().contains(val.toLowerCase());
                case Name:
                    return adminEntity.fullName.toLowerCase().contains(val.toLowerCase());
                case Menu:
                    return adminActiveLogEntity.menuName.toLowerCase().contains(val.toLowerCase());
            }
        }
        return null;
    }

    private BooleanExpression termBetween(PeriodTypeEnum periodType, String sdate, String edate) {

        if (sdate != null && edate != null && periodType != null) {

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
            LocalDateTime searchStartDate = LocalDateTime.parse(sdate + " 00:00:00.000", formatter);
            LocalDateTime searchEndDate = LocalDateTime.parse(edate + " 23:59:59.999", formatter);

            switch (periodType) {
                case Created:
                    return adminActiveLogEntity.activeTime.between(searchStartDate, searchEndDate);
                default:
                    return null;
            }
        }
        return null;
    }

    private OrderSpecifier sorterOrder(AdminActiveSorterEnum sorter) {
        if (sorter == null)
            return adminActiveLogEntity.activeTime.desc();

        if (sorter == AdminActiveSorterEnum.AdminIdAsc)
            return adminEntity.adminEmail.asc();

        if (sorter == AdminActiveSorterEnum.AdminIdDesc)
            return adminEntity.adminEmail.desc();

        if (sorter == AdminActiveSorterEnum.FullNameAsc)
            return adminEntity.fullName.asc();

        if (sorter == AdminActiveSorterEnum.FullNameDesc)
            return adminEntity.fullName.desc();

        if (sorter == AdminActiveSorterEnum.MenuNameAsc)
            return adminActiveLogEntity.menuName.asc();

        if (sorter == AdminActiveSorterEnum.MenuNameDesc)
            return adminActiveLogEntity.menuName.desc();

        if (sorter == AdminActiveSorterEnum.CreatedAsc)
            return adminActiveLogEntity.activeTime.asc();

        if (sorter == AdminActiveSorterEnum.CreatedDesc)
            return adminActiveLogEntity.activeTime.desc();

        return adminActiveLogEntity.activeTime.desc();
    }

}
