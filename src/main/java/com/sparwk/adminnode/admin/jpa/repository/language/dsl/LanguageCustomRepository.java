package com.sparwk.adminnode.admin.jpa.repository.language.dsl;

import com.sparwk.adminnode.admin.biz.v1.language.dto.LanguageDto;
import com.sparwk.adminnode.admin.biz.v1.language.dto.LanguageExcelDto;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.language.LanguageCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.language.LanguageSorterEnum;
import org.springframework.data.domain.PageRequest;

import java.util.List;

public interface LanguageCustomRepository {

    List<LanguageDto> findQryAll(LanguageCateEnum languageCateEnum,
                                 String val,
                                 YnTypeEnum useType,
                                 LanguageSorterEnum sorter,
                                 PageRequest pageRequest);
    long countQryAll(LanguageCateEnum languageCateEnum,
                                 String val,
                                 YnTypeEnum useType);

    List<LanguageDto> findQryUseY();
    long countQryUseY();
    List<LanguageExcelDto> findExcelList();

}
