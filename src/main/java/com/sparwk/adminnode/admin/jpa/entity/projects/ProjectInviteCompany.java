package com.sparwk.adminnode.admin.jpa.entity.projects;

import com.sparwk.adminnode.admin.jpa.entity.BaseEntity;
import com.sparwk.adminnode.admin.jpa.entity.projects.id.ProjectInviteCompanyId;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor
@IdClass(ProjectInviteCompanyId.class)
@Table(name = "tb_project_invite_company")
public class ProjectInviteCompany extends BaseEntity {

    @Id
    @Column(name = "proj_id", nullable = false)
    private long projId;
    @Id
    @Column(name = "comp_profile_id", nullable = false)
    private Long companyProfileId;

    @Builder
    ProjectInviteCompany(
            long projId,
            Long companyProfileId
    ) {
            this.projId =   projId;
            this.companyProfileId = companyProfileId;
    }

//    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
//    @JoinColumn(name = "proj_id", referencedColumnName = "proj_id",insertable = false,updatable = false)
//    private Project project;

}
