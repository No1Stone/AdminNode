package com.sparwk.adminnode.admin.jpa.repository.continent.dsl;

import com.querydsl.core.types.ExpressionUtils;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.JPAExpressions;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.sparwk.adminnode.admin.biz.v1.continent.dto.ContinentDto;
import com.sparwk.adminnode.admin.biz.v1.continent.dto.ContinentExcelDto;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.admin.QAdminEntity;
import com.sparwk.adminnode.admin.jpa.entity.commonCode.CommonSorterEnum2;
import com.sparwk.adminnode.admin.jpa.entity.continent.ContinentCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.continent.ContinentSorterEnum;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.querydsl.core.types.ExpressionUtils.count;
import static com.sparwk.adminnode.admin.jpa.entity.admin.QAdminEntity.adminEntity;
import static com.sparwk.adminnode.admin.jpa.entity.commonCode.QCommonOrganizationCodeEntity.commonOrganizationCodeEntity;
import static com.sparwk.adminnode.admin.jpa.entity.continent.QContinentEntity.continentEntity;
import static com.sparwk.adminnode.admin.jpa.entity.country.QCountryEntity.countryEntity;

@Repository
public class ContinentCustomRepositoryImpl implements ContinentCustomRepository {

    private final JPAQueryFactory jpaQueryFactory;

    public ContinentCustomRepositoryImpl(JPAQueryFactory jpaQueryFactory) {
        this.jpaQueryFactory = jpaQueryFactory;
    }

    @Override
    public List<ContinentDto> findQryAll(ContinentCateEnum cate, String val, YnTypeEnum useType, ContinentSorterEnum sorter, PageRequest pageRequest) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(Projections.bean(ContinentDto.class,
                                continentEntity.continentSeq,
                                continentEntity.continent,
                                continentEntity.code,
                                continentEntity.description,
                                continentEntity.useType,
                                continentEntity.regUsr,
                                adminEntity.fullName.as("regUsrName"),
                                continentEntity.regDt,
                                continentEntity.modUsr,
                                adminEntity2.fullName.as("modUsrName"),
                                continentEntity.modDt
                        )
                )
                .from(continentEntity)
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(continentEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(continentEntity.modUsr))
                .where(valLike(cate, val), useTypeEq(useType))
                .limit(pageRequest.getPageSize())
                .offset(pageRequest.getPageSize() * (pageRequest.getPageNumber() - 1))
                .orderBy(sorterOrder(sorter))
                .fetch();
    }

    @Override
    public long countQryAll(ContinentCateEnum cate, String val, YnTypeEnum useType) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(
                                continentEntity.continentSeq.count()
                )
                .from(continentEntity)
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(continentEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(continentEntity.modUsr))
                .where(valLike(cate, val), useTypeEq(useType))
                .fetchOne();
    }

    @Override
    public List<ContinentDto> findQryUseY() {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(Projections.bean(ContinentDto.class,
                                continentEntity.continentSeq,
                                continentEntity.continent,
                                continentEntity.code,
                                continentEntity.description,
                                continentEntity.useType,
                        ExpressionUtils.as(
                                JPAExpressions.select(count(countryEntity.continentCode))
                                        .from(countryEntity)
                                        .where(countryEntity.continentCode.eq(continentEntity.code)),
                                "cnt"),
                                continentEntity.regUsr,
                                adminEntity.fullName.as("regUsrName"),
                                continentEntity.regDt,
                                continentEntity.modUsr,
                                adminEntity2.fullName.as("modUsrName"),
                                continentEntity.modDt
                        )
                )
                .from(continentEntity)
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(continentEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(continentEntity.modUsr))
                .where(useTypeEq(YnTypeEnum.Y))
                .orderBy(sorterOrder(ContinentSorterEnum.ContinentAsc))
                .fetch();
    }

    @Override
    public long countQryUseY() {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(
                        continentEntity.continentSeq.count()
                )
                .from(continentEntity)
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(continentEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(continentEntity.modUsr))
                .where(useTypeEq(YnTypeEnum.Y))
                .fetchOne();
    }

    @Override
    public List<ContinentExcelDto> findExcelList() {

        return jpaQueryFactory.select(Projections.bean(ContinentExcelDto.class,
                                continentEntity.continentSeq,
                                continentEntity.continent,
                                continentEntity.code,
                                continentEntity.description,
                                continentEntity.useType
                        )
                )
                .from(continentEntity)
                .orderBy(sorterOrder(ContinentSorterEnum.ContinentAsc))
                .fetch();
    }

    private BooleanExpression valLike(ContinentCateEnum cate, String val) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        if (cate != null && val != null && val.length() > 0) {
            switch (cate) {
                case All:
                    return continentEntity.continent.toLowerCase().contains(val.toLowerCase())
                            .or(continentEntity.code.toLowerCase().contains(val.toLowerCase()))
                            .or(continentEntity.description.toLowerCase().contains(val.toLowerCase()));
                case Continent:
                    return continentEntity.continent.toLowerCase().contains(val.toLowerCase());
                case Description:
                    return continentEntity.description.toLowerCase().contains(val.toLowerCase());
                case ISO:
                    return continentEntity.code.toLowerCase().contains(val.toLowerCase());
                case RegUsrName:
                    return adminEntity.fullName.toLowerCase().contains(val.toLowerCase());
                case ModUsrName:
                    return adminEntity2.fullName.toLowerCase().contains(val.toLowerCase());
            }
        }
        return null;
    }

    private BooleanExpression idEq(Long id) {
        if (id != null)
            return continentEntity.continentSeq.eq(id);
        return null;
    }

    private BooleanExpression useTypeEq(YnTypeEnum useType) {
        if (useType != null)
            return continentEntity.useType.eq(useType);
        return null;
    }

    private OrderSpecifier sorterOrder(ContinentSorterEnum sorter) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        if (sorter == null)
            return continentEntity.regDt.desc();

        if (sorter == ContinentSorterEnum.ContinentAsc)
            return continentEntity.continent.asc();

        if (sorter == ContinentSorterEnum.ContinentDesc)
            return continentEntity.continent.desc();

        if (sorter == ContinentSorterEnum.LastModifiedAsc)
            return continentEntity.modDt.asc();

        if (sorter == ContinentSorterEnum.LastModifiedDesc)
            return continentEntity.modDt.desc();

        if (sorter == ContinentSorterEnum.CreatedAsc)
            return continentEntity.regDt.asc();

        if (sorter == ContinentSorterEnum.CreatedDesc)
            return continentEntity.regDt.desc();

        if (sorter == ContinentSorterEnum.UseAsc)
            return continentEntity.useType.asc();

        if (sorter == ContinentSorterEnum.UseDesc)
            return continentEntity.useType.desc();

        if (sorter == ContinentSorterEnum.DescriptionAsc)
            return continentEntity.description.asc();

        if (sorter == ContinentSorterEnum.DescriptionDesc)
            return continentEntity.description.desc();

        if (sorter == ContinentSorterEnum.IsoAsc)
            return continentEntity.code.asc();

        if (sorter == ContinentSorterEnum.IsoDesc)
            return continentEntity.code.desc();

        if (sorter == ContinentSorterEnum.CreaterNameAsc)
            return adminEntity.fullName.asc();

        if (sorter == ContinentSorterEnum.CreaterNameDesc)
            return adminEntity.fullName.desc();

        if (sorter == ContinentSorterEnum.ModifierNameAsc)
            return adminEntity2.fullName.asc();

        if (sorter == ContinentSorterEnum.ModifierNameDesc)
            return adminEntity2.fullName.desc();

        return continentEntity.regDt.desc();
    }

}
