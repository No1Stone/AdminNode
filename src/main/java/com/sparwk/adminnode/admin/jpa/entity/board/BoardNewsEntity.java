package com.sparwk.adminnode.admin.jpa.entity.board;


import com.sparwk.adminnode.admin.jpa.entity.BaseEntity;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_board_news")
@DynamicUpdate
@TableGenerator(
        name = "tb_board_seq_generator",
        table = "tb_board_sequences",
        pkColumnValue = "tb_board_seq",
        allocationSize = 1,
        initialValue = 1
)
public class BoardNewsEntity extends BaseEntity {
    @Id
    @GeneratedValue(
            strategy = GenerationType.TABLE,
            generator = "tb_board_seq_generator")
    @Column(name = "news_id")
    private Long newsId;

    @Column(name = "cate_cd", nullable = true, length = 9)
    private String cateCd;

    @Column(name = "title", nullable = true, length = 100)
    private String title;

    @Column(name = "content", nullable = true)
    private String content;

    @Enumerated(EnumType.STRING)
    @Column(name = "attach_yn", nullable = true, length = 1)
    private YnTypeEnum attachYn;

    @Enumerated(EnumType.STRING)
    @Column(name = "use_yn", nullable = true, length = 1)
    private YnTypeEnum useYn;

    @Column(name = "hit", nullable = true)
    private Long hit;


    @Builder
    BoardNewsEntity(
            Long newsId,
            String cateCd,
            String title,
            String content,
            YnTypeEnum attachYn,
            YnTypeEnum useYn,
            Long hit
    ) {
        this.newsId = newsId;
        this.cateCd = cateCd;
        this.title = title;
        this.content = content;
        this.attachYn = attachYn;
        this.useYn = useYn;
        this.hit = hit;
    }

    public void update(
            String cateCd,
            String title,
            String content,
            YnTypeEnum attachYn,
            YnTypeEnum useYn
    ) {
        this.cateCd = cateCd;
        this.title = title;
        this.content = content;
        this.attachYn = attachYn;
        this.useYn = useYn;
    }

    public void updateZeroAttach(
            YnTypeEnum attachYn
    ) {
        this.attachYn = attachYn;
    }

//    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = AccountCompanyDetail.class)
//    @JoinColumn(name = "accnt_id", referencedColumnName = "accnt_id")
//    private AccountCompanyDetail accountCompanyDetail;
//
//    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = AccountCompanyLocation.class)
//    @JoinColumn(name = "accnt_id", referencedColumnName = "accnt_id")
//    private AccountCompanyLocation accountCompanyLocation;
//
//    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = AccountCompanyType.class)
//    @JoinColumn(name = "accnt_id", referencedColumnName = "accnt_id")
//    private AccountCompanyType accountCompanyType;
//
//    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = AccountPassport.class)
//    @JoinColumn(name = "accnt_id", referencedColumnName = "accnt_id")
//    private AccountPassport accountPassport;
//
//    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = AccountSnsToken.class)
//    @JoinColumn(name = "accnt_id", referencedColumnName = "accnt_id")
//    private List<AccountSnsToken> accountSnsToken;

}
