package com.sparwk.adminnode.admin.jpa.repository.commonCode.dsl;

import com.sparwk.adminnode.admin.biz.v1.commonCode.dto.*;
import com.sparwk.adminnode.admin.jpa.entity.PeriodTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.commonCode.*;
import org.springframework.data.domain.PageRequest;

import java.util.List;

public interface CommonDetailCodeCustomRepository {
    List<CommonDetailCodeDto> findQryAll(String pcode,
                                         String code,
                                         CommonCateEnum cate,
                                         String val,
                                         YnTypeEnum useType,
                                         PeriodTypeEnum periodType,
                                         String sdate,
                                         String edate,
                                         CommonSorterEnum sorter,
                                         PageRequest pageRequest
    );

    long countQryAll(String pcode,
                     String code,
                     CommonCateEnum cate,
                     String val,
                     YnTypeEnum useType,
                     PeriodTypeEnum periodType,
                     String sdate,
                     String edate
    );

    List<CommonDetailCodeDto> findQryUseY(String pcode, String code, CommonCateEnum cate, String val);

    long countQryUseY(String pcode, String code, CommonCateEnum cate, String val);

    List<CommonDetailCodeDto> findPopularUseY(String pcode, String code, CommonCateEnum cate, String val);

    long countPopularUseY(String pcode, String code, CommonCateEnum cate, String val);

    List<CommonDetailCodeExcelDto> findExcelList(String pcode);

    List<CommonCateListDto> findQryCateList(String pcode);


    List<CommonDetailCodeDto> findQryCateUseY(CommonCateEnum2 cate,
                                              String val,
                                              YnTypeEnum useType,
                                              CommonSorterEnum sorter,
                                              PageRequest pageRequest
    );

    long countQryCateUseY(CommonCateEnum2 cate,
                          String val,
                          YnTypeEnum useType
    );

    List<CateCodeListDto> findQryCateCodeList(CommonCateEnum2 cate,
                                              String val,
                                              YnTypeEnum useType,
                                              CommonSorterEnum3 sorter,
                                              PageRequest pageRequest
    );

    long countQryCateCodeList(CommonCateEnum2 cate,
                          String val,
                          YnTypeEnum useType
    );


    void deleteQryId(Long id);

    List<CommonCodeDto> findQryCatePCodeList();
}
