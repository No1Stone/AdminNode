package com.sparwk.adminnode.admin.jpa.entity.sns;

import com.sparwk.adminnode.admin.jpa.entity.BaseEntity;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name="tb_admin_sns_code")
public class SnsEntity extends BaseEntity {

    @Id
    @GeneratedValue(
            generator = "tb_admin_sns_code_seq"
    )
    @Column(name = "sns_seq")
    private Long snsSeq;

    @Column(name = "sns_cd", length = 9)
    private String snsCd;

    @Column(name = "sns_name", length = 50)
    private String snsName;

    @Column(name = "sns_icon_url", length = 200)
    private String snsIconUrl;

    @Enumerated(EnumType.STRING)
    @Column(name = "use_yn", length = 1)
    private YnTypeEnum useType;

    @Builder
    public SnsEntity(
                            Long snsSeq,
                            String snsCd,
                            String snsName,
                            String snsIconUrl,
                            YnTypeEnum useType
                          ) {
        this.snsSeq = snsSeq;
        this.snsCd = snsCd;
        this.snsName = snsName;
        this.snsIconUrl = snsIconUrl;
        this.useType = useType;
    }

    public void updateYn(YnTypeEnum useType) {
        this.useType = useType;
    }

    public void update(
            String snsName,
            String snsIconUrl,
            YnTypeEnum useType
                        ) {
        this.snsName = snsName;
        this.snsIconUrl = snsIconUrl;
        this.useType = useType;
    }


}
