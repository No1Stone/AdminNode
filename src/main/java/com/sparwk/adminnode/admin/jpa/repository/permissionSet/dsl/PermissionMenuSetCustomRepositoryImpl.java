package com.sparwk.adminnode.admin.jpa.repository.permissionSet.dsl;

import com.querydsl.core.types.ExpressionUtils;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.CaseBuilder;
import com.querydsl.jpa.JPAExpressions;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.sparwk.adminnode.admin.biz.v1.permissionSet.dto.PermissionMenuSetDto;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.sparwk.adminnode.admin.jpa.entity.permissionSet.QAdminMenuEntity.adminMenuEntity;
import static com.sparwk.adminnode.admin.jpa.entity.permissionSet.QPermissionMenuSetEntity.permissionMenuSetEntity;

@Repository
public class PermissionMenuSetCustomRepositoryImpl implements PermissionMenuSetCustomRepository {

    private final JPAQueryFactory jpaQueryFactory;

    public PermissionMenuSetCustomRepositoryImpl(JPAQueryFactory jpaQueryFactory) {
        this.jpaQueryFactory = jpaQueryFactory;
    }

    @Override
    public List<PermissionMenuSetDto> findQryMenuSetting(Long id) {
        return jpaQueryFactory.select(Projections.bean(PermissionMenuSetDto.class,
                        adminMenuEntity.menuSeq.as("menuId"),
                        adminMenuEntity.menuLabel,
                        adminMenuEntity.menuDepth,
                        adminMenuEntity.uppMenuSeq,
                        adminMenuEntity.sortIndex,
                        new CaseBuilder()
                                .when(
                                        (JPAExpressions
                                                .select(permissionMenuSetEntity.createYn.stringValue())
                                                .from(permissionMenuSetEntity)
                                                .where(permissionMenuSetEntity.adminMenuId.eq(adminMenuEntity.menuSeq),permissionMenuSetEntity.adminPermissionId.eq(id))
                                        ).eq("Y")
                                )
                                .then("Y")
                                .otherwise("N")
                                .as("createYn"),

                        new CaseBuilder()
                                .when(
                                        (JPAExpressions
                                                .select(permissionMenuSetEntity.readYn.stringValue())
                                                .from(permissionMenuSetEntity)
                                                .where(permissionMenuSetEntity.adminMenuId.eq(adminMenuEntity.menuSeq),permissionMenuSetEntity.adminPermissionId.eq(id))
                                        ).eq("Y")
                                )
                                .then("Y")
                                .otherwise("N")
                                .as("readYn"),

                        new CaseBuilder()
                                .when(
                                        (JPAExpressions
                                                .select(permissionMenuSetEntity.editYn.stringValue())
                                                .from(permissionMenuSetEntity)
                                                .where(permissionMenuSetEntity.adminMenuId.eq(adminMenuEntity.menuSeq),permissionMenuSetEntity.adminPermissionId.eq(id))
                                        ).eq("Y")
                                )
                                .then("Y")
                                .otherwise("N")
                                .as("editYn"),

                        new CaseBuilder()
                                .when(
                                        (JPAExpressions
                                                .select(permissionMenuSetEntity.deleteYn.stringValue())
                                                .from(permissionMenuSetEntity)
                                                .where(permissionMenuSetEntity.adminMenuId.eq(adminMenuEntity.menuSeq),permissionMenuSetEntity.adminPermissionId.eq(id))
                                        ).eq("Y")
                                )
                                .then("Y")
                                .otherwise("N")
                                .as("deleteYn"),

                                ExpressionUtils.as(
                                        JPAExpressions
                                                .select(permissionMenuSetEntity.regUsr)
                                                .from(permissionMenuSetEntity)
                                                .where(permissionMenuSetEntity.adminMenuId.eq(adminMenuEntity.menuSeq),permissionMenuSetEntity.adminPermissionId.eq(id))
                                , "regUsr"),

                                ExpressionUtils.as(
                                        JPAExpressions
                                                .select(permissionMenuSetEntity.regDt)
                                                .from(permissionMenuSetEntity)
                                                .where(permissionMenuSetEntity.adminMenuId.eq(adminMenuEntity.menuSeq),permissionMenuSetEntity.adminPermissionId.eq(id))
                                        , "regDt"),
                                ExpressionUtils.as(
                                        JPAExpressions
                                                .select(permissionMenuSetEntity.modUsr)
                                                .from(permissionMenuSetEntity)
                                                .where(permissionMenuSetEntity.adminMenuId.eq(adminMenuEntity.menuSeq),permissionMenuSetEntity.adminPermissionId.eq(id))
                                        , "modUsr"),

                                ExpressionUtils.as(
                                        JPAExpressions
                                                .select(permissionMenuSetEntity.modDt)
                                                .from(permissionMenuSetEntity)
                                                .where(permissionMenuSetEntity.adminMenuId.eq(adminMenuEntity.menuSeq),permissionMenuSetEntity.adminPermissionId.eq(id))
                                        , "modDt"),

                                ExpressionUtils.as(
                                        JPAExpressions
                                                .select(permissionMenuSetEntity.adminPermissionId)
                                                .from(permissionMenuSetEntity)
                                                .where(permissionMenuSetEntity.adminMenuId.eq(adminMenuEntity.menuSeq),permissionMenuSetEntity.adminPermissionId.eq(id))
                                        , "adminPermissionId")


                        )
                )
                .from(adminMenuEntity)
                .where(adminMenuEntity.useType.eq(YnTypeEnum.Y))
                .orderBy(adminMenuEntity.sortIndex.asc())
                //.orderBy(adminMenuEntity.menuSeq.asc())
                .fetch();
    }

}
