package com.sparwk.adminnode.admin.jpa.entity.email;

import com.sparwk.adminnode.admin.jpa.entity.BaseEntity;
import com.sparwk.adminnode.admin.jpa.entity.email.id.MailingId;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@IdClass(MailingId.class)
@Table(name="tb_mailing")
public class MailingEntity extends BaseEntity {

    @Id
    @Column(name = "to_email", length = 100)
    private String toEmail;

    @Id
    @Column(name = "from_email", length = 100)
    private String fromEmail;

    @Id
    @Column(name = "subject", length = 100)
    private String subject;

    @Column(name = "content")
    private String content;

    @Column(name = "send_yn", length = 1)
    private String sendYn;

    @Column(name = "status", length = 100)
    private String status;


    @Builder
    public MailingEntity(
            String toEmail,
            String fromEmail,
            String subject,
            String content,
            String sendYn,
                            String status
                        ) {
        this.toEmail = toEmail;
        this.fromEmail = fromEmail;
        this.subject = subject;
        this.content = content;
        this.sendYn = sendYn;
        this.status = status;
    }
}
