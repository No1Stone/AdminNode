package com.sparwk.adminnode.admin.jpa.entity.projects;

import lombok.Getter;

@Getter
public enum ProjectSorterEnum {

    ProjectTitleAsc("ProjectTitleAsc"), ProjectTitleDesc("ProjectTitleDesc"),
    ProjectTypeAsc("ProjectTypeAsc"), ProjectTypeDesc("ProjectTypeDesc"),
    OwnerAsc("OwnerAsc"), OwnerDesc("OwnerDesc"),
    CompanyAsc("CompanyAsc"), CompanyDesc("CompanyDesc"),
    MusicGenreAsc("MusicGenreAsc"), MusicGenreDesc("MusicGenreDesc"),
    BudgetAsc("BudgetAsc"), BudgetDesc("BudgetDesc"),
    CreatedAsc("CreatedAsc"), CreatedDesc("CreatedDesc"),
    CreaterNameAsc("CreaterNameAsc"), CreaterNameDesc("CreaterNameDesc"),
    ModifierNameAsc("ModifierNameAsc"), ModifierNameDesc("ModifierNameDesc")
    ;


    private String sorter;

    ProjectSorterEnum(String sorter) {
        this.sorter = sorter;
    }

}
