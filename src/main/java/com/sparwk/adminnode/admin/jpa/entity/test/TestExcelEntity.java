package com.sparwk.adminnode.admin.jpa.entity.test;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name="test_excel")
public class TestExcelEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "test_excel_id")
    private Long testExcelId;

    @Column(name = "code", length = 9)
    private String code;

    @Column(name = "ckey", length = 100)
    private String ckey;

    @Column(name = "cval", length = 100)
    private String cval;

    @Column(name = "cdesc", length = 1000)
    private String cdesc;

    @Builder
    public TestExcelEntity(String code, String ckey, String cval, String cdesc) {
        this.code = code;
        this.ckey = ckey;
        this.cval = cval;
        this.cdesc = cdesc;
    }

    public void update(String code, String ckey, String cval, String cdesc) {
        this.code = code;
        this.ckey = ckey;
        this.cval = cval;
        this.cdesc = cdesc;
    }

}
