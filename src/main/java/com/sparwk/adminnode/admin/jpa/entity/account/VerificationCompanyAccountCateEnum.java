package com.sparwk.adminnode.admin.jpa.entity.account;

import lombok.Getter;

@Getter
public enum VerificationCompanyAccountCateEnum {

    All("All"),
    Email("Email"),
    CompanyType("CompanyType"),
    CompanyName("CompanyName"),
    BusinessLocation("BusinessLocation");

    private String cate;

    VerificationCompanyAccountCateEnum(String cate) {
        this.cate = cate;
    }
}
