package com.sparwk.adminnode.admin.jpa.repository.song.dsl;

import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.sparwk.adminnode.admin.biz.v1.song.dto.SongViewListDto;
import com.sparwk.adminnode.admin.jpa.entity.PeriodTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.song.SongCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.song.SongSorterEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.sparwk.adminnode.admin.jpa.entity.song.QSongViewEntity.songViewEntity;

@Repository
public class SongViewCustomRepositoryImpl implements SongViewCustomRepository {

    private final JPAQueryFactory jpaQueryFactory;

    private final Logger logger = LoggerFactory.getLogger(SongViewCustomRepositoryImpl.class);

    public SongViewCustomRepositoryImpl(JPAQueryFactory jpaQueryFactory) {
        this.jpaQueryFactory = jpaQueryFactory;
    }

    @Override
    public List<SongViewListDto> findQryAll(SongCateEnum cate,
                                            String val,
                                            YnTypeEnum completeVal,
                                            PeriodTypeEnum periodType,
                                            String sdate,
                                            String edate,
                                            SongSorterEnum sorter,
                                            PageRequest pageRequest) {
        return jpaQueryFactory.select(Projections.bean(SongViewListDto.class,

                        songViewEntity.songId,
                        songViewEntity.songTitle,
                        songViewEntity.avatarFileUrl,
                        songViewEntity.songFilePath,
                        songViewEntity.songOwner,
                        songViewEntity.songOwnerName,
                        songViewEntity.songCowritersName,
                        songViewEntity.projTitle,
                        songViewEntity.songGenreCd,
                        songViewEntity.songGenreName,
                        songViewEntity.songSubgenreCd,
                        songViewEntity.songSubgenreName,
                        songViewEntity.songMoodCd,
                        songViewEntity.songMoodName,
                        songViewEntity.songThemeCd,
                        songViewEntity.songThemeName,
                        songViewEntity.songVersionCd,
                        songViewEntity.songVersionCdName,
                        songViewEntity.songStatusCd,
                        songViewEntity.songStatusCdName,
                        songViewEntity.songLangCd,
                        songViewEntity.songLangCdName,
                        songViewEntity.bpm.as("songPlayBpm"),
                        songViewEntity.duration.as("songPlayTime"),
                        songViewEntity.tempo.as("songTempo"),
                        songViewEntity.isrc.as("songISRC"),
                        songViewEntity.keySignature.as("songKeySignature"),
                        songViewEntity.timeSignature.as("songTimeSignature"),
                        songViewEntity.lyrics,
                        songViewEntity.lyricsComt,
                        songViewEntity.regUsr,
                        songViewEntity.regUsrName,
                        songViewEntity.regDt,
                        songViewEntity.modUsr,
                        songViewEntity.modUsrName,
                        songViewEntity.modDt
                        )
                )
                .from(songViewEntity)
                .where(valLike(cate, val))
                .limit(pageRequest.getPageSize())
                .offset(pageRequest.getPageSize() * (pageRequest.getPageNumber() - 1))
                .orderBy(sorterOrder(sorter))
                .fetch();
    }

    @Override
    public long countQryAll(SongCateEnum cate,
                                            String val,
                                            YnTypeEnum completeVal,
                                            PeriodTypeEnum periodType,
                                            String sdate,
                                            String edate) {
        return jpaQueryFactory.select(
                                songViewEntity.songId.count()
                )
                .from(songViewEntity)
                .where(valLike(cate, val))
                .fetchOne();
    }

    private BooleanExpression valLike(SongCateEnum cate, String val) {
        if (cate !=null && val != null && val.length() > 0) {
            switch (cate) {
                case All:
                    return songViewEntity.songTitle.toLowerCase().contains(val.toLowerCase())
                            .or(songViewEntity.songGenreName.toLowerCase().contains(val.toLowerCase()))
                            .or(songViewEntity.songSubgenreName.toLowerCase().contains(val.toLowerCase()))
                            .or(songViewEntity.projTitle.toLowerCase().contains(val.toLowerCase()));
                case Title:
                    return songViewEntity.songTitle.toLowerCase().contains(val.toLowerCase());
                case Genre:
                    return songViewEntity.songGenreName.toLowerCase().contains(val.toLowerCase());
                case SubGenre:
                    return songViewEntity.songSubgenreName.toLowerCase().contains(val.toLowerCase());
                case ProjectTitle:
                    return songViewEntity.projTitle.toLowerCase().contains(val.toLowerCase());
                case Status:
                    return songViewEntity.songStatusCdName.toLowerCase().contains(val.toLowerCase());
                case RegUsrName:
                    return songViewEntity.regUsrName.toLowerCase().contains(val.toLowerCase());
                case ModUsrName:
                    return songViewEntity.modUsrName.toLowerCase().contains(val.toLowerCase());

            }
        }
        return null;
    }


    private OrderSpecifier sorterOrder(SongSorterEnum sorter) {
        if (sorter == null)
            return songViewEntity.songId.desc();

        if (sorter == SongSorterEnum.TitleAsc)
            return songViewEntity.songTitle.asc();

        if (sorter == SongSorterEnum.TitleDesc)
            return songViewEntity.songTitle.desc();

        if (sorter == SongSorterEnum.OwnerAsc)
            return songViewEntity.songOwnerName.asc();

        if (sorter == SongSorterEnum.OwnerDesc)
            return songViewEntity.songOwner.desc();

        if (sorter == SongSorterEnum.CowriterAsc)
            return songViewEntity.songCowritersName.asc();

        if (sorter == SongSorterEnum.CowriterDesc)
            return songViewEntity.songCowritersName.desc();

        if (sorter == SongSorterEnum.ProjectTitleAsc)
            return songViewEntity.projTitle.asc();

        if (sorter == SongSorterEnum.ProjectTitleDesc)
            return songViewEntity.projTitle.desc();

        if (sorter == SongSorterEnum.GenreAsc)
            return songViewEntity.songGenreName.asc();

        if (sorter == SongSorterEnum.GenreDesc)
            return songViewEntity.songGenreName.desc();

        if (sorter == SongSorterEnum.SubGenreAsc)
            return songViewEntity.songSubgenreName.asc();

        if (sorter == SongSorterEnum.SubGenreDesc)
            return songViewEntity.songSubgenreName.desc();

        if (sorter == SongSorterEnum.MoodAsc)
            return songViewEntity.songMoodName.asc();

        if (sorter == SongSorterEnum.MoodDesc)
            return songViewEntity.songMoodName.desc();

        if (sorter == SongSorterEnum.ThemeAsc)
            return songViewEntity.songThemeName.asc();

        if (sorter == SongSorterEnum.ThemeDesc)
            return songViewEntity.songThemeName.desc();

        if (sorter == SongSorterEnum.VersionAsc)
            return songViewEntity.songVersionCdName.asc();

        if (sorter == SongSorterEnum.VersionDesc)
            return songViewEntity.songVersionCdName.desc();

        if (sorter == SongSorterEnum.StatusAsc)
            return songViewEntity.songStatusCdName.asc();

        if (sorter == SongSorterEnum.StatusDesc)
            return songViewEntity.songStatusCdName.desc();

        if (sorter == SongSorterEnum.LanguageAsc)
            return songViewEntity.songLangCdName.asc();

        if (sorter == SongSorterEnum.LanguageDesc)
            return songViewEntity.songLangCdName.desc();

        if (sorter == SongSorterEnum.BpmAsc)
            return songViewEntity.bpm.asc();

        if (sorter == SongSorterEnum.BpmDesc)
            return songViewEntity.bpm.desc();

        if (sorter == SongSorterEnum.SongPlayTimeAsc)
            return songViewEntity.duration.asc();

        if (sorter == SongSorterEnum.SongPlayTimeDesc)
            return songViewEntity.duration.desc();

        if (sorter == SongSorterEnum.TempoAsc)
            return songViewEntity.tempo.asc();

        if (sorter == SongSorterEnum.TempoDesc)
            return songViewEntity.tempo.desc();

        if (sorter == SongSorterEnum.ISRCAsc)
            return songViewEntity.isrc.asc();

        if (sorter == SongSorterEnum.ISRCDesc)
            return songViewEntity.isrc.desc();

        if (sorter == SongSorterEnum.KeySignatureAsc)
            return songViewEntity.keySignature.asc();

        if (sorter == SongSorterEnum.KeySignatureDesc)
            return songViewEntity.keySignature.desc();

        if (sorter == SongSorterEnum.TimeSignatureAsc)
            return songViewEntity.timeSignature.asc();

        if (sorter == SongSorterEnum.TimeSignatureDesc)
            return songViewEntity.timeSignature.desc();

        if (sorter == SongSorterEnum.LyricsAsc)
            return songViewEntity.lyrics.asc();

        if (sorter == SongSorterEnum.LyricsDesc)
            return songViewEntity.lyrics.desc();

        if (sorter == SongSorterEnum.LyricsComtAsc)
            return songViewEntity.lyricsComt.asc();

        if (sorter == SongSorterEnum.LyricsComtDesc)
            return songViewEntity.lyricsComt.desc();

        if (sorter == SongSorterEnum.RegUsrNameAsc)
            return songViewEntity.regUsrName.asc();

        if (sorter == SongSorterEnum.RegUsrNameDesc)
            return songViewEntity.regDt.desc();

        if (sorter == SongSorterEnum.CreatedAsc)
            return songViewEntity.regDt.asc();

        if (sorter == SongSorterEnum.CreatedDesc)
            return songViewEntity.regDt.desc();

        if (sorter == SongSorterEnum.ModUsrNameAsc)
            return songViewEntity.modUsrName.asc();

        if (sorter == SongSorterEnum.ModUsrNameDesc)
            return songViewEntity.modUsrName.desc();

        if (sorter == SongSorterEnum.ModifiedAsc)
            return songViewEntity.modDt.asc();

        if (sorter == SongSorterEnum.ModifiedDesc)
            return songViewEntity.modDt.desc();

        return songViewEntity.songId.desc();
    }


}
