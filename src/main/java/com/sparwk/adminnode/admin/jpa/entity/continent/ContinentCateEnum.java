package com.sparwk.adminnode.admin.jpa.entity.continent;

import lombok.Getter;

@Getter
public enum ContinentCateEnum {

    All("All"), Continent("Continent"), ISO("ISO"), Description("Description"), RegUsrName("RegUsrName"), ModUsrName("ModUsrName");

    private String cate;

    ContinentCateEnum(String cate) {
        this.cate = cate;
    }
}
