package com.sparwk.adminnode.admin.jpa.repository.admin.dsl;

import com.sparwk.adminnode.admin.biz.v1.admin.dto.AdminActiveLogListDto;
import com.sparwk.adminnode.admin.jpa.entity.admin.AdminActiveCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.admin.AdminActiveSorterEnum;
import org.springframework.data.domain.PageRequest;

import java.util.List;

public interface AdminActiveLogCustomRepository {
    List<AdminActiveLogListDto> findQryAll(AdminActiveCateEnum cate,
                                           String val,
                                           String sdate,
                                           String edate,
                                           AdminActiveSorterEnum sorter,
                                           PageRequest pageRequest);
    long countQryAll(AdminActiveCateEnum cate,
                     String val,
                     String sdate,
                     String edate);

}
