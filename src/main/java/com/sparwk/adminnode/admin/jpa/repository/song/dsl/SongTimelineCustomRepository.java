package com.sparwk.adminnode.admin.jpa.repository.song.dsl;

import com.sparwk.adminnode.admin.biz.v1.song.dto.SongTimelineDto;

import java.util.List;

public interface SongTimelineCustomRepository {

    List<SongTimelineDto> findQrySongTimeline(Long id);

}
