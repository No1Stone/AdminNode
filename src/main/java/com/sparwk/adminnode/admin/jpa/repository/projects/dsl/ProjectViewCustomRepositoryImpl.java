package com.sparwk.adminnode.admin.jpa.repository.projects.dsl;

import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.sparwk.adminnode.admin.biz.v1.projects.dto.ProjectViewListDto;
import com.sparwk.adminnode.admin.jpa.entity.PeriodTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.admin.QAdminEntity;
import com.sparwk.adminnode.admin.jpa.entity.commonCode.CommonSorterEnum2;
import com.sparwk.adminnode.admin.jpa.entity.projects.ProjcetCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.projects.ProjectSorterEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static com.sparwk.adminnode.admin.jpa.entity.admin.QAdminEntity.adminEntity;
import static com.sparwk.adminnode.admin.jpa.entity.commonCode.QCommonOrganizationCodeEntity.commonOrganizationCodeEntity;
import static com.sparwk.adminnode.admin.jpa.entity.language.QLanguageEntity.languageEntity;
import static com.sparwk.adminnode.admin.jpa.entity.projects.QProjectViewEntity.projectViewEntity;

@Repository
public class ProjectViewCustomRepositoryImpl implements ProjectViewCustomRepository {

    private final JPAQueryFactory jpaQueryFactory;

    private final Logger logger = LoggerFactory.getLogger(ProjectViewCustomRepositoryImpl.class);

    public ProjectViewCustomRepositoryImpl(JPAQueryFactory jpaQueryFactory) {
        this.jpaQueryFactory = jpaQueryFactory;
    }

    @Override
    public List<ProjectViewListDto> findQryAll(ProjcetCateEnum cate,
                                               String val,
                                               YnTypeEnum completeVal,
                                               PeriodTypeEnum periodType,
                                               String sdate,
                                               String edate,
                                               ProjectSorterEnum sorter,
                                               PageRequest pageRequest) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(Projections.bean(ProjectViewListDto.class,

                        projectViewEntity.projId,
                        projectViewEntity.projTitle,
                        projectViewEntity.avatarFileUrl,
                        projectViewEntity.pitchProjTypeCd,
                        projectViewEntity.pitchProjTypeCdName,
                        projectViewEntity.projOwner,
                        projectViewEntity.projOwnerName,
                        projectViewEntity.compProfileId,
                        projectViewEntity.compProfileName,
                        projectViewEntity.projGenreCd,
                        projectViewEntity.projGenreName,
                        projectViewEntity.projCondCd,
                        projectViewEntity.projCondCdName,
                        projectViewEntity.regUsr,
                        adminEntity.fullName.as("regUsrName"),
                        projectViewEntity.regDt,
                        projectViewEntity.modUsr,
                        adminEntity2.fullName.as("modUsrName"),
                        projectViewEntity.modDt,
                        projectViewEntity.completeYn
                        )
                )
                .from(projectViewEntity)
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(projectViewEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(projectViewEntity.modUsr))
                .where(completeYnEq(completeVal) ,valLike(cate, val), termBetween(periodType, sdate, edate))
                .limit(pageRequest.getPageSize())
                .offset(pageRequest.getPageSize() * (pageRequest.getPageNumber() - 1))
                .orderBy(sorterOrder(sorter))
                .fetch();
    }

    @Override
    public long countQryAll(ProjcetCateEnum cate,
                                               String val,
                                               YnTypeEnum completeVal,
                                               PeriodTypeEnum periodType,
                                               String sdate,
                                               String edate) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        return jpaQueryFactory.select(
                        projectViewEntity.projId.count()
                )
                .from(projectViewEntity)
                .leftJoin(adminEntity).on(adminEntity.adminId.eq(projectViewEntity.regUsr))
                .leftJoin(adminEntity2).on(adminEntity2.adminId.eq(projectViewEntity.modUsr))
                .where(completeYnEq(completeVal) ,valLike(cate, val), termBetween(periodType, sdate, edate))
                .fetchOne();
    }

    private BooleanExpression completeYnEq(YnTypeEnum val) {
        if (val == YnTypeEnum.Y)
            return projectViewEntity.completeYn.eq(YnTypeEnum.Y);

        if (val == YnTypeEnum.N)
            return projectViewEntity.completeYn.eq(YnTypeEnum.N);

        return null;
    }

    private BooleanExpression valLike(ProjcetCateEnum cate, String val) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        if (cate !=null && val != null && val.length() > 0) {
            switch (cate) {
                case All:
                    return projectViewEntity.projTitle.toLowerCase().contains(val.toLowerCase())
                            .or(projectViewEntity.pitchProjTypeCdName.toLowerCase().contains(val.toLowerCase()))
                            .or(projectViewEntity.projOwnerName.toLowerCase().contains(val.toLowerCase()))
                            .or(projectViewEntity.compProfileName.toLowerCase().contains(val.toLowerCase()));
                case PojectTitle:
                    return projectViewEntity.projTitle.toLowerCase().contains(val.toLowerCase());
                case PojectType:
                    return projectViewEntity.pitchProjTypeCdName.toLowerCase().contains(val.toLowerCase());
                case Owner:
                    return projectViewEntity.projOwnerName.toLowerCase().contains(val.toLowerCase());
                case Company:
                    return projectViewEntity.compProfileName.toLowerCase().contains(val.toLowerCase());
                case MusicGenre:
                    return projectViewEntity.projGenreName.contains(val.toLowerCase());
                case RegUsrName:
                    return adminEntity.fullName.toLowerCase().contains(val.toLowerCase());
                case ModUsrName:
                    return adminEntity2.fullName.toLowerCase().contains(val.toLowerCase());
            }
        }
        return null;
    }

    private BooleanExpression idEq(Long id) {
        if (id != null)
            return projectViewEntity.projId.eq(id);
        return null;
    }

    private BooleanExpression termBetween(PeriodTypeEnum periodType, String  sdate, String edate) {

        if (sdate != null && edate != null && periodType != null) {

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
            LocalDateTime searchStartDate = LocalDateTime.parse(sdate + " 00:00:00.000", formatter);
            LocalDateTime searchEndDate = LocalDateTime.parse(edate + " 23:59:59.999", formatter);

            switch (periodType) {
                case Created:
                    return projectViewEntity.regDt.between(searchStartDate, searchEndDate);
                default:
                    return null;
            }
        }
        return null;
    }


    private OrderSpecifier sorterOrder(ProjectSorterEnum sorter) {

        //검색 때문에 추가
        QAdminEntity adminEntity2 = new QAdminEntity("adminEntity2");

        if (sorter == null)
            return projectViewEntity.regDt.desc();

        if (sorter == ProjectSorterEnum.ProjectTitleAsc)
            return projectViewEntity.projTitle.asc();

        if (sorter == ProjectSorterEnum.ProjectTitleDesc)
            return projectViewEntity.projTitle.desc();

        if (sorter == ProjectSorterEnum.ProjectTypeAsc)
            return projectViewEntity.pitchProjTypeCdName.asc();

        if (sorter == ProjectSorterEnum.ProjectTypeDesc)
            return projectViewEntity.pitchProjTypeCdName.desc();

        if (sorter == ProjectSorterEnum.OwnerAsc)
            return projectViewEntity.projOwnerName.asc();

        if (sorter == ProjectSorterEnum.OwnerDesc)
            return projectViewEntity.projOwnerName.desc();

        if (sorter == ProjectSorterEnum.CompanyAsc)
            return projectViewEntity.compProfileName.asc();

        if (sorter == ProjectSorterEnum.CompanyDesc)
            return projectViewEntity.compProfileName.desc();

        if (sorter == ProjectSorterEnum.MusicGenreAsc)
            return projectViewEntity.projGenreName.asc();

        if (sorter == ProjectSorterEnum.MusicGenreDesc)
            return projectViewEntity.projGenreName.desc();

        if (sorter == ProjectSorterEnum.BudgetAsc)
            return projectViewEntity.projCondCdName.asc();

        if (sorter == ProjectSorterEnum.BudgetDesc)
            return projectViewEntity.projCondCdName.desc();

        if (sorter == ProjectSorterEnum.CreatedAsc)
            return projectViewEntity.regDt.asc();

        if (sorter == ProjectSorterEnum.CreatedDesc)
            return projectViewEntity.regDt.desc();

        if (sorter == ProjectSorterEnum.CreaterNameAsc)
            return adminEntity.fullName.asc();

        if (sorter == ProjectSorterEnum.CreaterNameDesc)
            return adminEntity.fullName.desc();

        if (sorter == ProjectSorterEnum.ModifierNameAsc)
            return adminEntity2.fullName.asc();

        if (sorter == ProjectSorterEnum.ModifierNameDesc)
            return adminEntity2.fullName.desc();

        return projectViewEntity.regDt.desc();
    }

}
