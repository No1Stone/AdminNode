package com.sparwk.adminnode.admin.jpa.entity.account;


import com.sparwk.adminnode.admin.jpa.entity.BaseEntity;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_account_group_type")
@DynamicUpdate
public class AccountGroupTypeEntity extends BaseEntity {
    @Id
    @Column(name = "accnt_id")
    private Long accntId;

    @Column(name = "group_cd", length = 9)
    private String groupCd;

    @Column(name = "group_license_file_url", nullable = true, length = 200)
    private String groupLicenseFileUrl;

    @Enumerated(EnumType.STRING)
    @Column(name = "group_license_varify_yn", nullable = true, length = 1)
    private YnTypeEnum groupLicenseVerifyYn;

    @Column(name = "group_license_file_name", nullable = true, length = 100)
    private String groupLicenseFileName;

    @Column(name = "song_data_url", nullable = true, length = 200)
    private String songDataUrl;

    @Builder
    AccountGroupTypeEntity(
            Long accntId,
            String groupCd,
            String groupLicenseFileUrl,
            YnTypeEnum groupLicenseVerifyYn,
            String groupLicenseFileName,
            String songDataUrl
    ) {
        this.accntId = accntId;
        this.groupCd = groupCd;
        this.groupLicenseFileUrl = groupLicenseFileUrl;
        this.groupLicenseVerifyYn = groupLicenseVerifyYn;
        this.groupLicenseFileName = groupLicenseFileName;
        this.songDataUrl = songDataUrl;
    }

}
