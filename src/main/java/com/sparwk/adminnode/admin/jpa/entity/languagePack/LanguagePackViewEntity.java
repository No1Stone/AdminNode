package com.sparwk.adminnode.admin.jpa.entity.languagePack;

import com.sparwk.adminnode.admin.jpa.entity.BaseEntity;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name="view_language_pack")
public class LanguagePackViewEntity extends BaseEntity implements Serializable {

    @Id
    @Column(name = "pack_id")
    private Long packId;

    @Column(name = "language_cd", length = 9)
    private String languageCd;

    @Column(name = "language_name", length = 9)
    private String languageName;

    @Column(name = "native_language", length = 9)
    private String nativeLanguage;

    @Column(name = "national_flag_url", length = 200)
    private String nationalFlagUrl;

    @Column(name = "json_upload_url", length = 200)
    private String jsonUploadUrl;

    @Column(name = "pack_version")
    private int packVersion;

    @Enumerated(EnumType.STRING)
    @Column(name = "use_yn", length = 1)
    private YnTypeEnum useType;


    @Builder
    public LanguagePackViewEntity(
                            Long packId,
                            String languageCd,
                            String languageName,
                            String nativeLanguage,
                            String nationalFlagUrl,
                            String jsonUploadUrl,
                            int packVersion,
                            YnTypeEnum useType
                          ) {
        this.packId = packId;
        this.languageCd = languageCd;
        this.languageName = languageName;
        this.nativeLanguage = nativeLanguage;
        this.nationalFlagUrl = nationalFlagUrl;
        this.jsonUploadUrl = jsonUploadUrl;
        this.packVersion = packVersion;
        this.useType = useType;
    }

}
