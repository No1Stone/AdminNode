package com.sparwk.adminnode.admin.jpa.entity.projects;

import com.sparwk.adminnode.admin.jpa.entity.BaseEntity;
import com.sparwk.adminnode.admin.jpa.entity.projects.id.ProjectServiceCountryId;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor
@IdClass(ProjectServiceCountryId.class)
@Table(name = "tb_project_service_country")
public class ProjectServiceCountry extends BaseEntity {

    @Id
    @Column(name = "proj_id", nullable = false)
    private long projId;
    @Id
    @Column(name = "service_cntr_cd", nullable = false)
    private String serviceCntrCd;

    @Builder
    ProjectServiceCountry(
            long projId,
            String serviceCntrCd
    ) {
            this.projId =   projId;
            this.serviceCntrCd = serviceCntrCd;
    }

//    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
//    @JoinColumn(name = "proj_id", referencedColumnName = "proj_id",insertable = false,updatable = false)
//    private Project project;

}
