package com.sparwk.adminnode.admin.jpa.entity.account;

import lombok.Getter;

@Getter
public enum VerificationIndividualAccountFilterEnum {

    All("All"),
    PassportVerify("PassportVerify"),
    PassportUnconfirmed("PassportUnconfirmed"),
    PassportReject("PassportReject"),
    IpiVerify("IPIVerify"),
    IpiUnconfirmed("IPIUnconfirmed"),
    IpiReject("IPIReject");

    private String filter;

    VerificationIndividualAccountFilterEnum(String filter) {
        this.filter = filter;
    }
}
