package com.sparwk.adminnode.admin.jpa.repository.song;

import com.sparwk.adminnode.admin.jpa.entity.song.SongFileEntity;
import com.sparwk.adminnode.admin.jpa.entity.song.SongTimelineEntity;
import com.sparwk.adminnode.admin.jpa.repository.song.dsl.SongFileCustomRepository;
import com.sparwk.adminnode.admin.jpa.repository.song.dsl.SongTimelineCustomRepository;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface SongFileRepository extends JpaRepository<SongFileEntity, Long>, SongFileCustomRepository {

    Optional<SongFileEntity> findBySongIdAndSongFileSeq(Long id, Long seq);

}
