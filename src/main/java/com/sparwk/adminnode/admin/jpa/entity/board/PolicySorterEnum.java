package com.sparwk.adminnode.admin.jpa.entity.board;

import lombok.Getter;

@Getter
public enum PolicySorterEnum {

    CategoryAsc("CategoryAsc"), CategoryDesc("CategoryDesc"),
    TitleAsc("TitleAsc"), TitleDesc("TitleDesc"),
    UseAsc("UseAsc"), UseDesc("UseDesc"),
    CreatedAsc("CreatedAsc"), CreatedDesc("CreatedDesc"),
    CreaterNameAsc("CreaterNameAsc"), CreaterNameDesc("CreaterNameDesc"),
    ModifierNameAsc("ModifierNameAsc"), ModifierNameDesc("ModifierNameDesc")
    ;

    private String sorter;

    PolicySorterEnum(String sorter) {
        this.sorter = sorter;
    }

}
