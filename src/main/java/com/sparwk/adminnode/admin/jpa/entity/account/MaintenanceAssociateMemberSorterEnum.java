package com.sparwk.adminnode.admin.jpa.entity.account;

import lombok.Getter;

@Getter
public enum MaintenanceAssociateMemberSorterEnum {

    EmailAsc("EmailAsc"),
    EmailDesc("EmailDesc"),
    FullNameAsc("FullNameAsc"),
    FullNameDesc("FullNameDesc"),
    BirthAsc("BirthAsc"),
    BirthDesc("BirthDesc"),
    GenderAsc("GenderAsc"),
    GenderDesc("GenderDesc"),
    CreatedAsc("CreatedAsc"),
    CreatedDesc("CreatedDesc");

    private String sorter;

    MaintenanceAssociateMemberSorterEnum(String sorter) {
        this.sorter = sorter;
    }
}
