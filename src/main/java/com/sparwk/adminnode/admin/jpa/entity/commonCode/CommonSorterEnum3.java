package com.sparwk.adminnode.admin.jpa.entity.commonCode;

import lombok.Getter;

@Getter
public enum CommonSorterEnum3 {

    MenuAsc("MenuAsc"), MenuDesc("MenuDesc"),
    CategoryAsc("CategoryAsc"), CategoryDesc("CategoryDesc"),
    UseYnAsc("UseYnAsc"), UseYnDesc("UseYnDesc"),
    DescriptionAsc("DescriptionAsc"), DescriptionDesc("DescriptionDesc"),
    LastModifiedAsc("LastModifiedAsc"), LastModifiedDesc("LastModifiedDesc"),
    CreatedAsc("CreatedAsc"), CreatedDesc("CreatedDesc"),
    CreaterNameAsc("CreaterNameAsc"), CreaterNameDesc("CreaterNameDesc"),
    ModifierNameAsc("ModifierNameAsc"), ModifierNameDesc("ModifierNameDesc")
    ;

    private String sorter;

    CommonSorterEnum3(String sorter) {
        this.sorter = sorter;
    }

}
