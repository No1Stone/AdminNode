package com.sparwk.adminnode.admin.jpa.repository.song.dsl;

import com.sparwk.adminnode.admin.biz.v1.song.dto.SongFileDto;
import com.sparwk.adminnode.admin.biz.v1.song.dto.SongTimelineDto;

import java.util.List;
import java.util.Optional;

public interface SongFileCustomRepository {

    SongFileDto findQrySongFile(Long id);

}
