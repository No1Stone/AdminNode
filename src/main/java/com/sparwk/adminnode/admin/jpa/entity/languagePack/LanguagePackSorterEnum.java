package com.sparwk.adminnode.admin.jpa.entity.languagePack;

import lombok.Getter;

@Getter
public enum LanguagePackSorterEnum {

    LanguagePackAsc("LanguagePackAsc"), LanguagePackDesc("LanguagePackDesc"),
    NativeLanguageAsc("NativeLanguageAsc"), NativeLanguageDesc("NativeLanguageDesc"),
    UseAsc("UseAsc"), UseDesc("UseDesc"),
    LastModifideAsc("LastModifideAsc"), LastModifideDesc("LastModifideDesc"),
    CreatedAsc("CreatedAsc"), CreatedDesc("CreatedDesc"),
    CreaterNameAsc("CreaterNameAsc"), CreaterNameDesc("CreaterNameDesc"),
    ModifierNameAsc("ModifierNameAsc"), ModifierNameDesc("ModifierNameDesc")
    ;

    private String sorter;

    LanguagePackSorterEnum(String sorter) {
        this.sorter = sorter;
    }

}
