package com.sparwk.adminnode.admin.jpa.entity.song;

import com.sparwk.adminnode.admin.jpa.entity.BaseEntity;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_song")
public class SongEntity extends BaseEntity {
    @Id
    @Column(name = "song_id", nullable = true)
    private long songId;
    @Column(name = "original_song_id", nullable = true)
    private long originalSongId;
    @Column(name = "song_version_cd", nullable = true)
    private String songVersionCd;
    @Column(name = "song_owner", nullable = true)
    private long songOwner;
    @Column(name = "lang_cd", nullable = true)
    private String langCd;
    @Column(name = "song_title", nullable = true)
    private String songTitle;
    @Column(name = "song_sub_title", nullable = true)
    private String songSubTitle;
    @Column(name = "description", nullable = true)
    private String description;
    @Column(name = "avatar_img_use_yn", nullable = true)
    private String avatarImgUseYn;
    @Column(name = "avatar_file_url", nullable = true)
    private String avatarFileUrl;
    @Enumerated(EnumType.STRING)
    @Column(name = "song_avail_yn", nullable = true)
    private YnTypeEnum songAvailYn;
    @Column(name = "song_status_cd", nullable = true)
    private String songStatusCd;
    @Column(name = "song_status_mod_dt", nullable = true)
    private LocalDateTime songStatusModDt;
    @Column(name = "user_define_version", nullable = true)
    private String userDefineVersion;
    @Column(name = "iswc", nullable = true)
    private String iswc;
    @Column(name = "isrc", nullable = true)
    private String isrc;


    @Builder
    SongEntity(
            long songId,
            long originalSongId,
            String songVersionCd,
            long songOwner,
            String langCd,
            String songTitle,
            String songSubTitle,
            String description,
            String avatarImgUseYn,
            String avatarFileUrl,
            YnTypeEnum songAvailYn,
            String songStatusCd,
            LocalDateTime songStatusModDt,
            String userDefineVersion,
            String iswc,
            String isrc
    ) {
        this.songId = songId;
        this.originalSongId = originalSongId;
        this.songVersionCd = songVersionCd;
        this.songOwner = songOwner;
        this.langCd = langCd;
        this.songTitle = songTitle;
        this.songSubTitle = songSubTitle;
        this.description = description;
        this.avatarImgUseYn = avatarImgUseYn;
        this.avatarFileUrl = avatarFileUrl;
        this.songAvailYn = songAvailYn;
        this.songStatusCd = songStatusCd;
        this.songStatusModDt = songStatusModDt;
        this.userDefineVersion = userDefineVersion;
        this.iswc = iswc;
        this.isrc = isrc;
    }

//    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy="project")
//    private List<ProjectMetadata> projectMetadata = new ArrayList<>();
//
//    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy="project")
//    private List<ProjectReferenceFile> projectReferenceFiles = new ArrayList<>();
//
//    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy="project")
//    private ProjectInviteCompany projectInviteCompany;
//
//    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy="project")
//    private List<ProjectServiceCountry> projectServiceCountries = new ArrayList<>();

//    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy="project")
//    private List<ProjectSong> projectSongs = new ArrayList<>();

//    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy="project")
//    private List<ProjectSongCowriter> projectSongCowriters = new ArrayList<>();

//    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy="project")
//    private ProjectMemb projectMemb;

    //
//    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = ProjectMemb.class)
//    @JoinColumn(name = "proj_id", referencedColumnName = "proj_id")
//    private ProjectMemb projectMemb;
//
//    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = ProjectSong.class)
//    @JoinColumn(name = "proj_id", referencedColumnName = "proj_id")
//    private ProjectSong projectSong;

    public void update(
            String songTitle,
            String songSubTitle,
            String description,
            String iswc,
            String isrc
    ) {
        this.songTitle = songTitle;
        this.songSubTitle = songSubTitle;
        this.description = description;
        this.iswc = iswc;
        this.isrc = isrc;
    }

}
