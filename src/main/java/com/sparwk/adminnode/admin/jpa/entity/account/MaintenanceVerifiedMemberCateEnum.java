package com.sparwk.adminnode.admin.jpa.entity.account;

import lombok.Getter;

@Getter
public enum MaintenanceVerifiedMemberCateEnum {

    All("All"),
    Email("Email"),
    FullName("FullName"),
    StageName("StageName");

    private String cate;

    MaintenanceVerifiedMemberCateEnum(String cate) {
        this.cate = cate;
    }
}
