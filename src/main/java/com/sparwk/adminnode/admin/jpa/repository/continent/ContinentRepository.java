package com.sparwk.adminnode.admin.jpa.repository.continent;

import com.sparwk.adminnode.admin.jpa.entity.continent.ContinentEntity;
import com.sparwk.adminnode.admin.jpa.repository.codeSeq.CodeSeqRepository;
import com.sparwk.adminnode.admin.jpa.repository.continent.dsl.ContinentCustomRepository;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ContinentRepository extends JpaRepository<ContinentEntity, Long>, ContinentCustomRepository, CodeSeqRepository {
    ContinentEntity findByCode(String code);
    ContinentEntity findByContinent(String continent);
}
