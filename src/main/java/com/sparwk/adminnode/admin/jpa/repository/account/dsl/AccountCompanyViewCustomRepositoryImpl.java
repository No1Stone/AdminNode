package com.sparwk.adminnode.admin.jpa.repository.account.dsl;

import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.sparwk.adminnode.admin.biz.v1.account.dto.CompanyMemberViewListDto;
import com.sparwk.adminnode.admin.jpa.entity.PeriodTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.account.*;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import static com.sparwk.adminnode.admin.jpa.entity.account.QAccountCompanyViewEntity.accountCompanyViewEntity;


@Repository
public class AccountCompanyViewCustomRepositoryImpl implements AccountCompanyViewCustomRepository {

    private final JPAQueryFactory jpaQueryFactory;

    public AccountCompanyViewCustomRepositoryImpl(JPAQueryFactory jpaQueryFactory) {
        this.jpaQueryFactory = jpaQueryFactory;
    }

    @Override
    public List<CompanyMemberViewListDto> findQryVerificationCompany(VerificationCompanyAccountCateEnum cate,
                                                                   String val,
                                                                   PeriodTypeEnum periodType,
                                                                   String sdate,
                                                                   String edate,
                                                                   VerificationCompanyAccountFilterEnum filter,
                                                                   VerificationCompanyAccountSorterEnum sorter,
                                                                   PageRequest pageRequest) {
        return jpaQueryFactory.select(Projections.bean(CompanyMemberViewListDto.class,
                                accountCompanyViewEntity.accntId,
                                accountCompanyViewEntity.profileId,
                                accountCompanyViewEntity.accntEmail,
                                accountCompanyViewEntity.companyName,
                                accountCompanyViewEntity.companyType,
                                accountCompanyViewEntity.bussinessLocation,
                                accountCompanyViewEntity.companyLicenseVerifyYn,
                                accountCompanyViewEntity.ipiNumberVarifyYn,
                                accountCompanyViewEntity.ipiNumber,
                                accountCompanyViewEntity.vatNumber,
                                accountCompanyViewEntity.regDt
                        )
                )
                .from(accountCompanyViewEntity)
                .where(valLike1(cate, val), filterLike1(filter), termBetween(periodType, sdate, edate))
                .limit(pageRequest.getPageSize())
                .offset(pageRequest.getPageSize() * (pageRequest.getPageNumber() - 1))
                .orderBy(sorterOrder1(sorter).stream().toArray(OrderSpecifier[]::new))
                .fetch();
    }

    @Override
    public long countQryVerificationCompany(VerificationCompanyAccountCateEnum cate,
                                          String val,
                                          PeriodTypeEnum periodType,
                                          String sdate,
                                          String edate,
                                          VerificationCompanyAccountFilterEnum filter) {
        return jpaQueryFactory.select(
                        accountCompanyViewEntity.accntId.count()
                )
                .from(accountCompanyViewEntity)
                .where(valLike1(cate, val), filterLike1(filter), termBetween(periodType, sdate, edate))
                .fetchOne();
    }

    @Override
    public List<CompanyMemberViewListDto> findQryOfficeMember(MaintenanceOfficeMemberCateEnum cate,
                                                               String val,
                                                               PeriodTypeEnum periodType,
                                                               String sdate,
                                                               String edate,
                                                              MaintenanceOfficeMemberSorterEnum sorter,
                                                               PageRequest pageRequest) {
        return jpaQueryFactory.select(Projections.bean(CompanyMemberViewListDto.class,
                                accountCompanyViewEntity.accntId,
                                accountCompanyViewEntity.profileId,
                                accountCompanyViewEntity.accntEmail,
                                accountCompanyViewEntity.companyName,
                                accountCompanyViewEntity.companyType,
                                accountCompanyViewEntity.bussinessLocation,
                                accountCompanyViewEntity.companyLicenseVerifyYn,
                                accountCompanyViewEntity.ipiNumberVarifyYn,
                                accountCompanyViewEntity.ipiNumber,
                                accountCompanyViewEntity.vatNumber,
                                accountCompanyViewEntity.regDt
                        )
                )
                .from(accountCompanyViewEntity)
                .where(accountCompanyViewEntity.companyLicenseVerifyYn.eq(YnTypeEnum.Y), valLike2(cate, val), termBetween(periodType, sdate, edate))
                .limit(pageRequest.getPageSize())
                .offset(pageRequest.getPageSize() * (pageRequest.getPageNumber() - 1))
                .orderBy(sorterOrder2(sorter).stream().toArray(OrderSpecifier[]::new))
                .fetch();
    }

    @Override
    public long countQryOfficeMember(MaintenanceOfficeMemberCateEnum cate,
                                                               String val,
                                                               PeriodTypeEnum periodType,
                                                               String sdate,
                                                               String edate) {
        return jpaQueryFactory.select(
                        accountCompanyViewEntity.accntId.count()
                )
                .from(accountCompanyViewEntity)
                .where(accountCompanyViewEntity.companyLicenseVerifyYn.eq(YnTypeEnum.Y), valLike2(cate, val), termBetween(periodType, sdate, edate))
                .fetchOne();
    }

    private BooleanExpression valLike1(VerificationCompanyAccountCateEnum cate, String val) {
        if (cate != null && val != null && val.length() > 0) {
            switch (cate) {
                case All:
                    return accountCompanyViewEntity.companyName.toLowerCase().contains(val.toLowerCase())
                            .or(accountCompanyViewEntity.accntEmail.toLowerCase().contains(val.toLowerCase()))
                            .or(accountCompanyViewEntity.companyType.toLowerCase().contains(val.toLowerCase()));
                case Email:
                    return accountCompanyViewEntity.accntEmail.toLowerCase().contains(val.toLowerCase());
                case CompanyName:
                    return accountCompanyViewEntity.companyName.toLowerCase().contains(val.toLowerCase());
                case CompanyType:
                    return accountCompanyViewEntity.companyType.toLowerCase().contains(val.toLowerCase());
                case BusinessLocation:
                    return accountCompanyViewEntity.bussinessLocation.toLowerCase().contains(val.toLowerCase());
            }
        }
        return null;
    }

    private BooleanExpression valLike2(MaintenanceOfficeMemberCateEnum cate, String val) {
        if (cate != null && val != null && val.length() > 0) {
            switch (cate) {
                case All:
                    return accountCompanyViewEntity.companyName.toLowerCase().contains(val.toLowerCase())
                            .or(accountCompanyViewEntity.accntEmail.toLowerCase().contains(val.toLowerCase()))
                            .or(accountCompanyViewEntity.companyType.toLowerCase().contains(val.toLowerCase()))
                            .or(accountCompanyViewEntity.ipiNumber.toLowerCase().contains(val.toLowerCase()))
                            .or(accountCompanyViewEntity.vatNumber.toLowerCase().contains(val.toLowerCase()));
                case Email:
                    return accountCompanyViewEntity.accntEmail.toLowerCase().contains(val.toLowerCase());
                case CompanyName:
                    return accountCompanyViewEntity.companyName.toLowerCase().contains(val.toLowerCase());
                case CompanyType:
                    return accountCompanyViewEntity.companyType.toLowerCase().contains(val.toLowerCase());
                case Location:
                    return accountCompanyViewEntity.bussinessLocation.toLowerCase().contains(val.toLowerCase());
                case Ipi:
                    return accountCompanyViewEntity.ipiNumber.toLowerCase().contains(val.toLowerCase());
                case VatNo:
                    return accountCompanyViewEntity.vatNumber.toLowerCase().contains(val.toLowerCase());
            }
        }
        return null;
    }

    private BooleanExpression filterLike1(VerificationCompanyAccountFilterEnum filter) {
        if (filter != null) {
            switch (filter) {
                case All:
                    return null;
                case AccountVerify:
                    return accountCompanyViewEntity.companyLicenseVerifyYn.eq(YnTypeEnum.Y);
                case AccountUnconfirmed:
                    return accountCompanyViewEntity.companyLicenseVerifyYn.isNull();
                case AccountReject:
                    return accountCompanyViewEntity.companyLicenseVerifyYn.eq(YnTypeEnum.N);
                case IpiBaseNumberVerify:
                    return accountCompanyViewEntity.ipiNumberVarifyYn.eq(YnTypeEnum.Y);
                case IpiBaseNumberUnconfirmed:
                    return accountCompanyViewEntity.ipiNumberVarifyYn.isNull();
                case IpiBaseNumberReject:
                    return accountCompanyViewEntity.ipiNumberVarifyYn.eq(YnTypeEnum.N);
            }
        }
        return null;
    }

    private BooleanExpression termBetween(PeriodTypeEnum periodType, String  sdate, String edate) {

        if (sdate != null && edate != null && periodType != null) {

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
            LocalDateTime searchStartDate = LocalDateTime.parse(sdate + " 00:00:00.000", formatter);
            LocalDateTime searchEndDate = LocalDateTime.parse(edate + " 23:59:59.999", formatter);

            switch (periodType) {
                case Created:
                    return accountCompanyViewEntity.regDt.between(searchStartDate, searchEndDate);
                default:
                    return null;
            }
        }
        return null;
    }

    private List<OrderSpecifier> sorterOrder1(VerificationCompanyAccountSorterEnum sorter) {

        List<OrderSpecifier> orders = new ArrayList<>();

        if (sorter == null) {
            orders.add(accountCompanyViewEntity.regDt.desc());
            return orders;
        }

        if (sorter == VerificationCompanyAccountSorterEnum.CompanyNameAsc) {

            orders.add(accountCompanyViewEntity.companyName.asc());
            return orders;
        }

        if (sorter == VerificationCompanyAccountSorterEnum.CompanyNameDesc) {

            orders.add(accountCompanyViewEntity.companyName.desc());
            return orders;
        }

        if (sorter == VerificationCompanyAccountSorterEnum.CompanyTypeAsc) {

            orders.add(accountCompanyViewEntity.companyType.asc());
            return orders;
        }

        if (sorter == VerificationCompanyAccountSorterEnum.CompanyTypeDesc) {

            orders.add(accountCompanyViewEntity.companyType.desc());
            return orders;
        }

        if (sorter == VerificationCompanyAccountSorterEnum.CreatedAsc) {

            orders.add(accountCompanyViewEntity.regDt.asc());
            return orders;
        }

        if (sorter == VerificationCompanyAccountSorterEnum.CreatedDesc) {

            orders.add(accountCompanyViewEntity.regDt.desc());
            return orders;
        }

        if (sorter == VerificationCompanyAccountSorterEnum.EmailAsc) {

            orders.add(accountCompanyViewEntity.accntEmail.asc());

            return orders;
        }

        if (sorter == VerificationCompanyAccountSorterEnum.EmailDesc) {

            orders.add(accountCompanyViewEntity.accntEmail.desc());
            return orders;
        }

        if (sorter == VerificationCompanyAccountSorterEnum.BusinessLocationAsc) {

            orders.add(accountCompanyViewEntity.bussinessLocation.asc());
            return orders;
        }

        if (sorter == VerificationCompanyAccountSorterEnum.BusinessLocationDesc) {

            orders.add(accountCompanyViewEntity.bussinessLocation.desc());
            return orders;
        }

        orders.add(accountCompanyViewEntity.regDt.desc());
        return orders;
    }

    private List<OrderSpecifier> sorterOrder2(MaintenanceOfficeMemberSorterEnum sorter) {

        List<OrderSpecifier> orders = new ArrayList<>();

        if (sorter == null) {

            orders.add(accountCompanyViewEntity.regDt.desc());
            return orders;
        }

        if (sorter == MaintenanceOfficeMemberSorterEnum.CompanyNameAsc) {

            orders.add(accountCompanyViewEntity.companyName.asc());
            return orders;
        }

        if (sorter == MaintenanceOfficeMemberSorterEnum.CompanyNameDesc) {

            orders.add(accountCompanyViewEntity.companyName.desc());
            return orders;
        }

        if (sorter == MaintenanceOfficeMemberSorterEnum.CompanyTypeAsc) {

            orders.add(accountCompanyViewEntity.companyType.asc());
            return orders;
        }

        if (sorter == MaintenanceOfficeMemberSorterEnum.CompanyTypeDesc) {

            orders.add(accountCompanyViewEntity.companyType.desc());
            return orders;
        }

        if (sorter == MaintenanceOfficeMemberSorterEnum.CreatedAsc) {

            orders.add(accountCompanyViewEntity.regDt.asc());
            return orders;
        }

        if (sorter == MaintenanceOfficeMemberSorterEnum.CreatedDesc) {

            orders.add(accountCompanyViewEntity.regDt.desc());
            return orders;
        }

        if (sorter == MaintenanceOfficeMemberSorterEnum.EmailAsc) {

            orders.add(accountCompanyViewEntity.accntEmail.asc());
            return orders;
        }

        if (sorter == MaintenanceOfficeMemberSorterEnum.EmailDesc) {

            orders.add(accountCompanyViewEntity.accntEmail.desc());
            return orders;
        }

        if (sorter == MaintenanceOfficeMemberSorterEnum.LocationAsc) {

            orders.add(accountCompanyViewEntity.bussinessLocation.asc());
            return orders;
        }

        if (sorter == MaintenanceOfficeMemberSorterEnum.LocationDesc) {

            orders.add(accountCompanyViewEntity.bussinessLocation.desc());
            return orders;
        }

        orders.add(accountCompanyViewEntity.regDt.desc());
        return orders;
    }

}
