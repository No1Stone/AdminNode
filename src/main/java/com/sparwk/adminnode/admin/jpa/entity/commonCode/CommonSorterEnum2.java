package com.sparwk.adminnode.admin.jpa.entity.commonCode;

import lombok.Getter;

@Getter
public enum CommonSorterEnum2 {

    NameAsc("NameAsc"), NameDesc("NameDesc"),
    CountryAsc("CountryAsc"), CountryDesc("CountryDesc"),
    UseYnAsc("UseYnAsc"), UseYnDesc("UseYnDesc"),
    LastModifiedAsc("LastModifiedAsc"), LastModifiedDesc("LastModifiedDesc"),
    CreatedAsc("CreatedAsc"), CreatedDesc("CreatedDesc"),
    DescriptionAsc("DescriptionAsc"), DescriptionDesc("DescriptionDesc"),
    DcodeAsc("DcodeAsc"), DcodeDesc("DcodeDesc"),
    CreaterNameAsc("CreaterNameAsc"), CreaterNameDesc("CreaterNameDesc"),
    ModifierNameAsc("ModifierNameAsc"), ModifierNameDesc("ModifierNameDesc")
    ;

    private String sorter;

    CommonSorterEnum2(String sorter) {
        this.sorter = sorter;
    }

}
