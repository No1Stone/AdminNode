package com.sparwk.adminnode.admin.jpa.entity.currency;

import lombok.Getter;

@Getter
public enum CurrencySorterEnum {

    CurrencyAsc("CurrencyAsc"), CurrencyDesc("CurrencyNameDesc"),
    UseYnAsc("UseYnAsc"), UseYnDesc("UseYnDesc"),
    LastModifiedAsc("LastModifiedAsc"), LastModifiedDesc("LastModifiedDesc"),
    CreatedAsc("CreatedAsc"), CreatedDesc("CreatedDesc"),
    IsoAsc("IsoAsc"), IsoDesc("IsoDesc"),
    SymbolAsc("SymbolAsc"), SymbolDesc("SymbolDesc"),
    CountryAsc("CountryAsc"), CountryDesc("CountryDesc"),
    CreaterNameAsc("CreaterNameAsc"), CreaterNameDesc("CreaterNameDesc"),
    ModifierNameAsc("ModifierNameAsc"), ModifierNameDesc("ModifierNameDesc")
    ;

    private String sorter;

    CurrencySorterEnum(String sorter) {
        this.sorter = sorter;
    }

}
