package com.sparwk.adminnode.admin.jpa.entity.admin;

import lombok.Getter;

@Getter
public enum AdminActiveSorterEnum {

    AdminIdAsc("AdminIdAsc"), AdminIdDesc("AdminIdDesc"),
    FullNameAsc("FullNameAsc"), FullNameDesc("FullNameDesc"),
    MenuNameAsc("MenuNameAsc"), MenuNameDesc("MenuNameDesc"),
    CreatedAsc("CreatedAsc"), CreatedDesc("CreatedDesc")
    ;

    private String sorter;

    AdminActiveSorterEnum(String sorter) {
        this.sorter = sorter;
    }

}
