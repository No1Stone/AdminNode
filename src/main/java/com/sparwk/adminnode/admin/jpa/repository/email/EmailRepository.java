package com.sparwk.adminnode.admin.jpa.repository.email;

import com.sparwk.adminnode.admin.jpa.entity.email.EmailEntity;
import com.sparwk.adminnode.admin.jpa.repository.email.dsl.EmailCustomRepository;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmailRepository extends JpaRepository<EmailEntity, Long>, EmailCustomRepository {
}
