package com.sparwk.adminnode.admin.jpa.entity.commonCode;

import lombok.Getter;

@Getter
public enum CommonCateEnum3 {

    All("All"), Value("Value"), Country("Country"), Description("Description"), RegUsrName("RegUsrName"), ModUsrName("ModUsrName");

    private String commonCateEnum;

    CommonCateEnum3(String commonCateEnum) {
        this.commonCateEnum = commonCateEnum;
    }
}
