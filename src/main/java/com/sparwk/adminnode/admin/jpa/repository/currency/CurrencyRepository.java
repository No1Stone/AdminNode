package com.sparwk.adminnode.admin.jpa.repository.currency;

import com.sparwk.adminnode.admin.jpa.entity.currency.CurrencyEntity;
import com.sparwk.adminnode.admin.jpa.repository.codeSeq.CodeSeqRepository;
import com.sparwk.adminnode.admin.jpa.repository.currency.dsl.CurrencyCustomRepository;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CurrencyRepository extends JpaRepository<CurrencyEntity, Long>, CurrencyCustomRepository, CodeSeqRepository {
    Long countByCurrency(String val);
}
