package com.sparwk.adminnode.admin.jpa.repository.role;

import com.sparwk.adminnode.admin.jpa.entity.role.RoleCodeEntity;
import com.sparwk.adminnode.admin.jpa.repository.codeSeq.CodeSeqRepository;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleCodeRepository extends JpaRepository<RoleCodeEntity, Long>, CodeSeqRepository {

    RoleCodeEntity findByCode(String val);
}
