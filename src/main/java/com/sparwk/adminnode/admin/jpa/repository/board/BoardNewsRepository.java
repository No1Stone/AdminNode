package com.sparwk.adminnode.admin.jpa.repository.board;

import com.sparwk.adminnode.admin.jpa.entity.board.BoardNewsEntity;
import com.sparwk.adminnode.admin.jpa.repository.board.dsl.BoardNewsCustomRepository;
import com.sparwk.adminnode.admin.jpa.repository.codeSeq.CodeSeqRepository;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BoardNewsRepository extends JpaRepository<BoardNewsEntity, Long>, BoardNewsCustomRepository, CodeSeqRepository {

}
