package com.sparwk.adminnode.admin.jpa.entity;

import lombok.Getter;

@Getter
public enum YnTypeEnum {
    Y("Y"), N("N"), R("R"), P("P"), A("A");

    String ynEnumType;

    YnTypeEnum(String ynEnumType) {
        this.ynEnumType = ynEnumType;
    }

}
