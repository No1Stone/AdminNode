package com.sparwk.adminnode.admin.jpa.entity.song;

import com.sparwk.adminnode.admin.jpa.entity.BaseEntity;
import com.sparwk.adminnode.admin.jpa.entity.song.id.SongMetaId;
import lombok.*;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_song_timeline")
public class SongTimelineEntity extends BaseEntity {
    @Id
    @Column(name = "timeline_seq", nullable = true)
    private long timelineSeq;
    @Column(name = "song_id", nullable = true)
    private long songId;
    @Column(name = "component_use_id", nullable = true)
    private long componentUseId;
    @Column(name = "componet_cd", nullable = true)
    private String componetCd;
    @Column(name = "tlm_cd", nullable = true)
    private String tlmCd;
    @Column(name = "params", nullable = true)
    private String params;
    @Column(name = "result_msg", nullable = true)
    private String resultMsg;

    @Builder
    SongTimelineEntity(
            long timelineSeq,
            long songId,
            long componentUseId,
            String componetCd,
            String tlmCd,
            String params,
            String resultMsg
    ) {
        this.timelineSeq = timelineSeq;
        this.songId = songId;
        this.componentUseId = componentUseId;
        this.componetCd = componetCd;
        this.tlmCd = tlmCd;
        this.params = params;
        this.resultMsg = resultMsg;
    }
}
