package com.sparwk.adminnode.admin.jpa.entity.board;

import lombok.Getter;

@Getter
public enum BoardCateEnum {

    All("All"), Title("Title"), Content("Contents"), RegUsrName("RegUsrName"), ModUsrName("ModUsrName");

    private String boardCateEnum;

    BoardCateEnum(String BoardCateEnum) {
        this.boardCateEnum = boardCateEnum;
    }
}
