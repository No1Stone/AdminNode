package com.sparwk.adminnode.admin.jpa.repository.board;

import com.sparwk.adminnode.admin.jpa.entity.board.BoardSuccessEntity;
import com.sparwk.adminnode.admin.jpa.repository.board.dsl.BoardSuccessCustomRepository;
import com.sparwk.adminnode.admin.jpa.repository.codeSeq.CodeSeqRepository;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BoardSuccessRepository extends JpaRepository<BoardSuccessEntity, Long>, BoardSuccessCustomRepository, CodeSeqRepository {

}
