package com.sparwk.adminnode.admin.jpa.repository.email;

import com.sparwk.adminnode.admin.jpa.entity.email.EmailAttachEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmailAttachRepository extends JpaRepository<EmailAttachEntity, Long> {

}
