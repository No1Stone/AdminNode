package com.sparwk.adminnode.admin.jpa.entity.board;

import lombok.Getter;

@Getter
public enum QnaCateEnum {

    All("All"), Unanswered("Unanswered");

    private String cate;

    QnaCateEnum(String cate) {
        this.cate = cate;
    }
}
