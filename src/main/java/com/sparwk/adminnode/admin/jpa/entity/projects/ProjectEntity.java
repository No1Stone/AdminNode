package com.sparwk.adminnode.admin.jpa.entity.projects;

import com.sparwk.adminnode.admin.jpa.entity.BaseEntity;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import lombok.*;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_project")
public class ProjectEntity extends BaseEntity {
    @Id
    @GeneratedValue(generator = "tb_project_seq")
    @Column(name = "proj_id", nullable = true)
    private long projId;
    @Column(name = "proj_owner", nullable = true)
    private long projOwner;
    @Column(name = "comp_profile_id", nullable = true)
    private long compProfileId;
    @Enumerated(EnumType.STRING)
    @Column(name = "proj_aval_yn", nullable = true)
    private YnTypeEnum projAvalYn;
    @Column(name = "gender_cd", nullable = true)
    private String genderCd;
    @Column(name = "proj_indiv_comp_type", nullable = true)
    private String projIndivCompType;
    @Enumerated(EnumType.STRING)
    @Column(name = "proj_publ_yn", nullable = true)
    private YnTypeEnum projPublYn;
    @Column(name = "proj_pwd", nullable = true)
    private String projPwd;
    @Column(name = "proj_invt_title", nullable = true)
    private String projInvtTitle;
    @Column(name = "proj_invt_desc", nullable = true)
    private String projInvtDesc;
    @Column(name = "proj_what_for", nullable = true)
    private String projWhatFor;
    @Column(name = "pitch_proj_type_cd", nullable = true)
    private String pitchProjTypeCd;
    @Column(name = "proj_work_locat", nullable = true)
    private String projWorkLocat;
    @Column(name = "proj_title", nullable = true)
    private String projTitle;
    @Column(name = "proj_desc", nullable = true)
    private String projDesc;
    @Column(name = "proj_ddl_dt", nullable = true)
    private LocalDate projDdlDt;
    @Column(name = "proj_cond_cd", nullable = true)
    private String projCondCd;
    @Column(name = "min_val", nullable = true)
    private long minVal;
    @Column(name = "max_val", nullable = true)
    private long maxVal;
    @Enumerated(EnumType.STRING)
    @Column(name = "recruit_yn", nullable = true)
    private YnTypeEnum recruitYn;
    @Column(name = "avatar_file_url", nullable = true)
    private String avatarFileUrl;
    @Enumerated(EnumType.STRING)
    @Column(name = "leave_yn", nullable = true)
    private YnTypeEnum leaveYn;
    @Enumerated(EnumType.STRING)
    @Column(name = "complete_yn", nullable = true)
    private YnTypeEnum completeYn;

    @Column(name = "reg_usr", updatable = false)
    //first registrant
    @CreatedBy
    private Long regUsr;

    @Column(name = "reg_dt", updatable = false)
    @CreatedDate//serverTime
    //@CreationTimestamp//dbtime
    private LocalDateTime regDt;

    @Column(name = "mod_usr")
    @LastModifiedBy
    private Long modUsr;

    @Column(name = "mod_dt")
    @LastModifiedDate//serverTime
    //@UpdateTimestamp//dbtime
    private LocalDateTime modDt;
    @Builder
    ProjectEntity(
        long projId,
        long projOwner,
        long compProfileId,
        YnTypeEnum projAvalYn,
        String projIndivCompType,
        YnTypeEnum projPublYn,
        String projPwd,
        String projInvtTitle,
        String projInvtDesc,
        String projWhatFor,
        String pitchProjTypeCd,
        String projWorkLocat,
        String projTitle,
        String projDesc,
        LocalDate projDdlDt,
        String projCondCd,
        long minVal,
        long maxVal,
        YnTypeEnum recruitYn,
        String avatarFileUrl,
        YnTypeEnum leaveYn,
        YnTypeEnum completeYn,
        Long regUsr,
        LocalDateTime regDt,
        Long modUsr,
        LocalDateTime modDt
    ) {
        this.projId                 =     projId;
        this.projOwner              =     projOwner;
        this.compProfileId          =     Long.valueOf(compProfileId) != null ? compProfileId : 0L;
        this.projAvalYn             =     projAvalYn;
        this.projIndivCompType      =     projIndivCompType != null ? projIndivCompType : "IND000001";
        this.projPublYn             =     projPublYn != null ? projPublYn : YnTypeEnum.Y;
        this.projPwd                =     projPwd != null ? projPwd : "";
        this.projInvtTitle          =     projInvtTitle != null ? projInvtTitle : "";
        this.projInvtDesc           =     projInvtDesc != null ? projInvtDesc : "" ;
        this.projWhatFor            =     projWhatFor != null ? projWhatFor : "" ;
        this.pitchProjTypeCd        =     pitchProjTypeCd != null ? pitchProjTypeCd.trim() : "";
        this.projWorkLocat          =     projWorkLocat != null ? projWorkLocat.trim() : "PWL000001" ;
        this.projTitle              =     projTitle != null ? projTitle : "";
        this.projDesc               =     projDesc != null ? projDesc : "";
        this.projDdlDt              =     projDdlDt != null ? projDdlDt : LocalDate.now();
        this.projCondCd             =     projCondCd != null ? projCondCd.trim() : "PJC000001";
        this.minVal                 =     minVal != 0 ? minVal : 0;
        this.maxVal                 =     maxVal != 0 ? maxVal : 0;
        this.recruitYn              =     recruitYn;
        this.avatarFileUrl          =     avatarFileUrl != null ? avatarFileUrl : "";
        this.leaveYn                =     leaveYn;
        this.completeYn             =     completeYn;
        this.regUsr                 =     regUsr;
        this.regDt                  =     regDt;
        this.modUsr                 =     modUsr;
        this.modDt                  =     modDt;
    }

//    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy="project")
//    private List<ProjectMetadata> projectMetadata = new ArrayList<>();
//
//    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy="project")
//    private List<ProjectReferenceFile> projectReferenceFiles = new ArrayList<>();
//
//    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy="project")
//    private ProjectInviteCompany projectInviteCompany;
//
//    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy="project")
//    private List<ProjectServiceCountry> projectServiceCountries = new ArrayList<>();

//    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy="project")
//    private List<ProjectSong> projectSongs = new ArrayList<>();

//    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy="project")
//    private List<ProjectSongCowriter> projectSongCowriters = new ArrayList<>();

//    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy="project")
//    private ProjectMemb projectMemb;

    //
//    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = ProjectMemb.class)
//    @JoinColumn(name = "proj_id", referencedColumnName = "proj_id")
//    private ProjectMemb projectMemb;
//
//    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = ProjectSong.class)
//    @JoinColumn(name = "proj_id", referencedColumnName = "proj_id")
//    private ProjectSong projectSong;

    public void update(
            String projTitle
    ) {
        this.projTitle = projTitle;
    }
}
