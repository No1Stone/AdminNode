package com.sparwk.adminnode.admin.jpa.repository.permissionSetGroups.dsl;

import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.sparwk.adminnode.admin.biz.v1.permissionSetGroups.dto.PermissionSetGroupsDto;
import com.sparwk.adminnode.admin.jpa.entity.PeriodTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.permissionSetGroups.PermissionSetGroupsCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.permissionSetGroups.PermissionSetGroupsSorterEnum;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static com.sparwk.adminnode.admin.jpa.entity.permissionSetGroups.QPermissionSetGroupsEntity.permissionSetGroupsEntity;
import static com.sparwk.adminnode.admin.jpa.entity.songCode.QSongDetailCodeEntity.songDetailCodeEntity;

@Repository
public class PermissionSetGroupsCustomRepositoryImpl implements PermissionSetGroupsCustomRepository {

    private final JPAQueryFactory jpaQueryFactory;

    public PermissionSetGroupsCustomRepositoryImpl(JPAQueryFactory jpaQueryFactory) {
        this.jpaQueryFactory = jpaQueryFactory;
    }


    @Override
    public List<PermissionSetGroupsDto> findQryAll(PermissionSetGroupsCateEnum cate,
                                                   String cdLabelVal,
                                                   YnTypeEnum useType,
                                                   PeriodTypeEnum periodType,
                                                   String sdate,
                                                   String edate,
                                                   PermissionSetGroupsSorterEnum sorter,
                                                   PageRequest pageRequest) {
        return jpaQueryFactory.select(Projections.bean(PermissionSetGroupsDto.class,
                                permissionSetGroupsEntity.adminPermissionGroupId,
                                permissionSetGroupsEntity.labelVal,
                                permissionSetGroupsEntity.cdDesc,
                                permissionSetGroupsEntity.useType,
                                permissionSetGroupsEntity.cdOrd,
                                permissionSetGroupsEntity.regUsr,
                                permissionSetGroupsEntity.regDt,
                                permissionSetGroupsEntity.modUsr,
                                permissionSetGroupsEntity.modDt
                        ))
                .from(permissionSetGroupsEntity)
                .where(cdValLike(cate, cdLabelVal), useTypeEq(useType), termBetween(periodType, sdate, edate))
                .limit(pageRequest.getPageSize())
                .offset(pageRequest.getPageSize() * (pageRequest.getPageNumber() - 1))
                .orderBy(sorterOrder(sorter))
                .fetch();
    }

    @Override
    public List<PermissionSetGroupsDto> findQryUseY(PermissionSetGroupsCateEnum cate, String cdLabelVal) {

        return jpaQueryFactory.select(Projections.bean(PermissionSetGroupsDto.class,
                        permissionSetGroupsEntity.adminPermissionGroupId,
                        permissionSetGroupsEntity.labelVal,
                        permissionSetGroupsEntity.cdDesc,
                        permissionSetGroupsEntity.useType,
                        permissionSetGroupsEntity.cdOrd,
                        permissionSetGroupsEntity.regUsr,
                        permissionSetGroupsEntity.regDt,
                        permissionSetGroupsEntity.modUsr,
                        permissionSetGroupsEntity.modDt
                ))
                .from(permissionSetGroupsEntity)
                .where(cdValLike(cate, cdLabelVal), useTypeEq(YnTypeEnum.Y))
                .orderBy(sorterOrder(PermissionSetGroupsSorterEnum.NameAsc))
                .fetch();
    }

     private BooleanExpression cdValLike(PermissionSetGroupsCateEnum cate, String val) {
         if (cate !=null && val != null && val.length() > 0) {
             switch (cate) {
                 case All:
                     return permissionSetGroupsEntity.labelVal.toLowerCase().contains(val.toLowerCase())
                             .or(permissionSetGroupsEntity.cdDesc.toLowerCase().contains(val.toLowerCase()));
                 case GroupsLabel:
                     return permissionSetGroupsEntity.labelVal.toLowerCase().contains(val.toLowerCase());
                 case Description:
                     return permissionSetGroupsEntity.cdDesc.toLowerCase().contains(val.toLowerCase());
             }
         }
         return null;
    }

    private BooleanExpression useTypeEq(YnTypeEnum useType) {
        if (useType != null)
            return permissionSetGroupsEntity.useType.eq(useType);
        return null;
    }

    private BooleanExpression termBetween(PeriodTypeEnum periodType, String  sdate, String edate) {

        if (sdate != null && edate != null && periodType != null) {

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
            LocalDateTime searchStartDate = LocalDateTime.parse(sdate + " 00:00:00.000", formatter);
            LocalDateTime searchEndDate = LocalDateTime.parse(edate + " 23:59:59.999", formatter);

            switch (periodType) {
                case Created:
                    return songDetailCodeEntity.regDt.between(searchStartDate, searchEndDate);
                case LastModified:
                    return songDetailCodeEntity.modDt.between(searchStartDate, searchEndDate);
                default:
                    return null;
            }
        }
        return null;
    }

    private OrderSpecifier sorterOrder(PermissionSetGroupsSorterEnum sorter) {
        if (sorter == null)
            return permissionSetGroupsEntity.modDt.asc();

        if (sorter == PermissionSetGroupsSorterEnum.NameAsc)
            return permissionSetGroupsEntity.labelVal.asc();

        if (sorter == PermissionSetGroupsSorterEnum.NameDesc)
            return permissionSetGroupsEntity.labelVal.desc();

        if (sorter == PermissionSetGroupsSorterEnum.UseYnAsc)
            return permissionSetGroupsEntity.useType.asc();

        if (sorter == PermissionSetGroupsSorterEnum.UseYnDesc)
            return permissionSetGroupsEntity.useType.desc();

        if (sorter == PermissionSetGroupsSorterEnum.LastModifiedAsc)
            return permissionSetGroupsEntity.modDt.asc();

        if (sorter == PermissionSetGroupsSorterEnum.LastModifiedDesc)
            return permissionSetGroupsEntity.modDt.desc();

        if (sorter == PermissionSetGroupsSorterEnum.CreatedAsc)
            return permissionSetGroupsEntity.regDt.asc();

        if (sorter == PermissionSetGroupsSorterEnum.CreatedDesc)
            return permissionSetGroupsEntity.regDt.desc();

        return permissionSetGroupsEntity.regDt.desc();
    }

}
