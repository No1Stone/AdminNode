package com.sparwk.adminnode.admin.jpa.entity.account;

import lombok.Getter;

@Getter
public enum MaintenanceOfficeMemberSorterEnum {

    CompanyNameAsc("CompanyNameAsc"),
    CompanyNameDesc("CompanyNameDesc"),
    CompanyTypeAsc("CompanyTypeAsc"),
    CompanyTypeDesc("CompanyTypeDesc"),
    EmailAsc("EmailAsc"),
    EmailDesc("EmailDesc"),
    LocationAsc("LocationAsc"),
    LocationDesc("LocationDesc"),
    CreatedAsc("CreatedAsc"),
    CreatedDesc("CreatedDesc");

    private String sorter;

    MaintenanceOfficeMemberSorterEnum(String sorter) {
        this.sorter = sorter;
    }
}
