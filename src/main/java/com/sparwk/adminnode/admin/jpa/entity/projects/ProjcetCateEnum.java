package com.sparwk.adminnode.admin.jpa.entity.projects;

import lombok.Getter;

@Getter
public enum ProjcetCateEnum {

    All("All"),
    PojectTitle("ProjectTitle"),
    PojectType("PojectType"),
    Owner("Owner"),
    Company("Company"),
    MusicGenre("MusicGenre"),
    RegUsrName("RegUsrName"),
    ModUsrName("ModUsrName")
    ;

    private String projcetCateEnum;

    ProjcetCateEnum(String projcetCateEnum) {
        this.projcetCateEnum = projcetCateEnum;
    }
}
