
package com.sparwk.adminnode.admin.jpa.entity.projects;

import com.sparwk.adminnode.admin.jpa.entity.BaseEntity;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.projects.id.ProjectSongCowriterId;
import lombok.*;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@IdClass(ProjectSongCowriterId.class)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_project_song_cowriter")
public class ProjectSongCowriter extends BaseEntity {

    @Id
    @Column(name = "proj_id", nullable = true)
    private long projId;
    @Id
    @Column(name = "song_id", nullable = true)
    private long songId;
    @Id
    @Column(name = "profile_id", nullable = true)
    private long profileId;
    @Column(name = "rate_share", nullable = true)
    private double rateShare;
    @Column(name = "accept_yn", nullable = true)
    private YnTypeEnum acceptYn;

    @Column(name = "reg_usr", updatable = false)
    //first registrant
    @CreatedBy
    private Long regUsr;

    @Column(name = "reg_dt", updatable = false)
    @CreatedDate//serverTime
    //@CreationTimestamp//dbtime
    private LocalDateTime regDt;

    @Column(name = "mod_usr")
    @LastModifiedBy
    private Long modUsr;

    @Column(name = "mod_dt")
    @LastModifiedDate//serverTime
    //@UpdateTimestamp//dbtime
    private LocalDateTime modDt;


    @Builder
    ProjectSongCowriter(
        long projId,
        long songId,
        long profileId,
        double rateShare,
        YnTypeEnum acceptYn,
        Long regUsr,
        LocalDateTime regDt,
        Long modUsr,
        LocalDateTime modDt
    ) {
        this.projId                 =       projId;
        this.songId                 =       songId;
        this.profileId              =       profileId;
        this.rateShare              =       rateShare;
        this.acceptYn               =       acceptYn;
        this.regUsr                 =       regUsr;
        this.regDt                  =       regDt;
        this.modUsr                 =       modUsr;
        this.modDt                  =       modDt;
    }

//    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
//    @JoinColumn(name = "proj_id", referencedColumnName = "proj_id",insertable = false,updatable = false)
//    private Project project;

}
