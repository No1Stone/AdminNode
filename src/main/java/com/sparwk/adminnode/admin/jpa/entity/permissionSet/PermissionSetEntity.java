package com.sparwk.adminnode.admin.jpa.entity.permissionSet;

import com.sparwk.adminnode.admin.jpa.entity.BaseEntity;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name="tb_admin_permission")
public class PermissionSetEntity extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "admin_permission_id")
    private Long adminPermissionId;

    @Column(name = "label_val", nullable = false, length = 100)
    private String labelVal;

    @Column(name = "cd_ord")
    private int cdOrd;

    @Column(name = "cd_desc", nullable = false, length = 1000)
    private String cdDesc;

    @Enumerated(EnumType.STRING)
    @Column(name = "use_yn", length = 1)
    private YnTypeEnum useType;

    @Builder
    public PermissionSetEntity(
                            Long adminPermissionId,
                            String labelVal,
                            int cdOrd,
                            String cdDesc,
                            YnTypeEnum useType
                        ) {
        this.adminPermissionId = adminPermissionId;
        this.labelVal = labelVal;
        this.cdOrd = cdOrd;
        this.cdDesc = cdDesc;
        this.useType = useType;
    }


    public void updateYn(YnTypeEnum useType) {
        this.useType = useType;
    }

    public void update(
                                String labelVal,
                                int cdOrd,
                                String cdDesc,
                                YnTypeEnum useType
                        ) {
        this.labelVal = labelVal;
        this.cdOrd = cdOrd; //현재는 사용계획 없음
        this.cdDesc = cdDesc;
        this.useType = useType;
    }
}
