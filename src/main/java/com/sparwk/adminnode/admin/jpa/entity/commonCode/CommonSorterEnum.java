package com.sparwk.adminnode.admin.jpa.entity.commonCode;

import lombok.Getter;

@Getter
public enum CommonSorterEnum {

    NameAsc("NameAsc"), NameDesc("NameDesc"),
    UseYnAsc("UseYnAsc"), UseYnDesc("UseYnDesc"),
    DescriptionAsc("DescriptionAsc"), DescriptionDesc("DescriptionDesc"),
    LastModifiedAsc("LastModifiedAsc"), LastModifiedDesc("LastModifiedDesc"),
    CreatedAsc("CreatedAsc"), CreatedDesc("CreatedDesc"),
    CreaterNameAsc("CreaterNameAsc"), CreaterNameDesc("CreaterNameDesc"),
    ModifierNameAsc("ModifierNameAsc"), ModifierNameDesc("ModifierNameDesc"),
    DcodeAsc("DcodeAsc"), DcodeDesc("DcodeDesc");

    private String sorter;

    CommonSorterEnum(String sorter) {
        this.sorter = sorter;
    }

}
