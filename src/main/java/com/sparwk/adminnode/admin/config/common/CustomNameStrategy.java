package com.sparwk.adminnode.admin.config.common;

import org.hibernate.boot.model.naming.Identifier;
import org.hibernate.boot.model.naming.PhysicalNamingStrategyStandardImpl;
import org.hibernate.engine.jdbc.env.spi.JdbcEnvironment;

public class CustomNameStrategy extends PhysicalNamingStrategyStandardImpl {

    /**
     * JPA-hibernate 테이블 접미사 붙이는 방법
     * application.properties에서 같이 사용
     */

    private String PREFIX = "";

    @Override
    public Identifier toPhysicalTableName(Identifier name, JdbcEnvironment context) {
        Identifier prefixedTableName = new Identifier(PREFIX + name.getText().toLowerCase(), name.isQuoted());
        return super.toPhysicalTableName(prefixedTableName, context);
    }
}
