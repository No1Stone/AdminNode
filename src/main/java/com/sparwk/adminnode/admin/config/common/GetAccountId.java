package com.sparwk.adminnode.admin.config.common;

import com.sparwk.adminnode.admin.config.filter.token.UserInfoDTO;

import javax.servlet.http.HttpServletRequest;

public class GetAccountId {

    private Long accountId;

    public Long ofId(HttpServletRequest request){
        UserInfoDTO user = (UserInfoDTO) request.getAttribute("UserInfoDTO");
        this.accountId = user.getAccountId();
        return accountId;
    }

}
