package com.sparwk.adminnode.admin.config.util;

import com.sparwk.adminnode.admin.config.filter.token.UserInfoDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;

public class TokenUtil {

    private final Logger logger = LoggerFactory.getLogger(TokenUtil.class);


    /**
     * Token ProfileId Get
     * @param req
     * @return
     */
    public static UserInfoDTO getTokenInfo(HttpServletRequest req) {
        return (UserInfoDTO) req.getAttribute("UserInfoDTO");
    }

    /**
     * Token ProfileId Get
     * @param req
     * @return
     */
    public static Long getToekenProfileId(HttpServletRequest req) {
        UserInfoDTO userInfoDTO = (UserInfoDTO) req.getAttribute("UserInfoDTO");
        return userInfoDTO.getLastUseProfileId();
    }

}
