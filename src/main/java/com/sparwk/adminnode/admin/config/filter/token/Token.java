package com.sparwk.adminnode.admin.config.filter.token;

import lombok.*;

@Getter@Setter@Builder
@NoArgsConstructor@AllArgsConstructor
public class Token {

    private UserInfoDTO userInfoDTO;
}
