package com.sparwk.adminnode.admin.config.common;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@EnableJpaAuditing
@Configuration
public class AuditorAwareConfig {

    @Bean
    public void getPrint(){
        System.out.println("test: test");
    }
}
