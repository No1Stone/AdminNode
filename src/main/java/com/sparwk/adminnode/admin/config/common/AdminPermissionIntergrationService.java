package com.sparwk.adminnode.admin.config.common;

import com.google.gson.Gson;
import com.sparwk.adminnode.admin.jpa.entity.permissionSet.PermissionMenuSetEntity;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Service
@RequiredArgsConstructor
public class AdminPermissionIntergrationService {

    private final HttpServletRequest httpServletRequest;
    private final MessageSourceAccessor messageSourceAccessor;
    private final Logger logger = LoggerFactory.getLogger(AdminPermissionIntergrationService.class);

    public Boolean AdminPermissionIntergrationCheck(Long menuId, String CRUD) {
        Gson gson = new Gson();
        boolean result = false;
        List<PermissionMenuSetEntity> adminPermission = (List<PermissionMenuSetEntity>)
                httpServletRequest.getAttribute("adminPermission");
        logger.info("AdminPermissionIntergrationCheck----{}", gson.toJson(adminPermission, List.class));

        for (PermissionMenuSetEntity e : adminPermission) {
            if (e.getAdminMenuId().equals(menuId)) {
                logger.info("permission For");
                logger.info("e data  - - -{} ", e.getAdminMenuId());

                switch (CRUD) {
                    case "C":
                        if(String.valueOf(e.getCreateYn()).equals("Y")){
                            logger.info("C 권한 있음");
                            return true;
                        }
                        else{
                            logger.info("C 권한 없음");
                            return false;
                        }
                    case "R":
                        if(String.valueOf(e.getReadYn()).equals("Y")){
                            logger.info("R 권한 있음");
                            return true;
                        }
                        else{
                            logger.info("R 권한 없음");
                            return false;
                        }
                    case "U":
                        if(String.valueOf(e.getEditYn()).equals("Y")){
                            logger.info("U 권한 있음");
                            return true;
                        }
                        else{
                            logger.info("U 권한 없음");
                            return false;
                        }
                    case "D":
                        if(String.valueOf(e.getDeleteYn()).equals("Y")){
                            logger.info("D 권한 있음");
                            return true;
                        }
                        else{
                            logger.info("D 권한 없음");
                            return false;
                        }
                }
            }
            continue;
        }

        return result;
    }

}
