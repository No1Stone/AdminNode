package com.sparwk.adminnode.admin.config.common;

import com.fasterxml.classmate.TypeResolver;
import com.sparwk.adminnode.admin.biz.v1.account.dto.AssoiateMemberViewListDto;
import com.sparwk.adminnode.admin.biz.v1.account.dto.CompanyMemberViewListDto;
import com.sparwk.adminnode.admin.biz.v1.account.dto.IndividualMemberViewListDto;
import com.sparwk.adminnode.admin.biz.v1.account.dto.VerifiedMemberListDto;
import com.sparwk.adminnode.admin.biz.v1.admin.dto.AdminActiveLogListDto;
import com.sparwk.adminnode.admin.biz.v1.board.dto.BoardDelete;
import com.sparwk.adminnode.admin.biz.v1.board.dto.BoardFaqDto;
import com.sparwk.adminnode.admin.biz.v1.email.dto.EmailDto;
import com.sparwk.adminnode.admin.biz.v1.inquary.dto.InquaryDto;
import com.sparwk.adminnode.admin.biz.v1.language.dto.LanguageDto;
import com.sparwk.adminnode.admin.biz.v1.projects.dto.ProjectDetailDto;
import com.sparwk.adminnode.admin.biz.v1.projects.dto.ProjectViewListDto;
import com.sparwk.adminnode.admin.biz.v1.song.dto.SongViewListDto;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.http.ResponseEntity;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.AlternateTypeRule;
import springfox.documentation.schema.AlternateTypeRules;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.time.LocalDate;
import java.util.*;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Bean
    public Docket api(TypeResolver typeResolver) {

        //Authentication header 처리를 위해 사용
        List global = new ArrayList();
        global.add(new ParameterBuilder().name("Authorization").
                description("Access Token").parameterType("header").
                required(false).modelRef(new ModelRef("string")).build());

        return new Docket(DocumentationType.SWAGGER_2)
                .useDefaultResponseMessages(false)
                .alternateTypeRules(getAlternateTypeRuleForLocalDate())
                .alternateTypeRules(getAlternateTypeRuleForResponseEntity())
                //.directModelSubstitute(LocalDateTime.class, java.util.Date.class)
                //.directModelSubstitute(LocalDateTime.class, String.class)
                //.directModelSubstitute(AssoiateMemberViewListDto.class, com.sparwk.adminnode.admin.biz.v1.account.dto.AssoiateMemberViewListDto.class)
                .additionalModels(
                        typeResolver.resolve(AssoiateMemberViewListDto.class),
                        typeResolver.resolve(BoardDelete.class),
                        typeResolver.resolve(IndividualMemberViewListDto.class),
                        typeResolver.resolve(CompanyMemberViewListDto.class),
                        typeResolver.resolve(InquaryDto.class),
                        typeResolver.resolve(VerifiedMemberListDto.class),
                        typeResolver.resolve(ProjectViewListDto.class),
                        typeResolver.resolve(ProjectDetailDto.class),
                        typeResolver.resolve(BoardFaqDto.class),
                        typeResolver.resolve(EmailDto.class),
                        typeResolver.resolve(AdminActiveLogListDto.class),
                        typeResolver.resolve(LanguageDto.class),
                        typeResolver.resolve(SongViewListDto.class)
                )
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.sparwk.adminnode.admin"))
                .paths(PathSelectors.any())
                .build()
                .securityContexts(Arrays.asList(securityContext()))
                .securitySchemes(Arrays.asList(apiKey()));
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("SPARWK")
                .version("0.1")
                .description("SPARWK Controller")
                .license("SPARWK")
                .build();
    }

    public AlternateTypeRule getAlternateTypeRuleForLocalDate() {
        TypeResolver typeResolver = new TypeResolver();
        return AlternateTypeRules.newRule(
                typeResolver.resolve(Map.class, String.class, typeResolver.resolve(LocalDate.class)),
                typeResolver.resolve(Map.class, String.class, Date.class),
                Ordered.HIGHEST_PRECEDENCE
        );
    }

    public AlternateTypeRule getAlternateTypeRuleForResponseEntity() {
        TypeResolver typeResolver = new TypeResolver();
        return AlternateTypeRules.newRule(
                typeResolver.resolve(ResponseEntity.class),
                typeResolver.resolve(Void.class)
        );
    }

    private ApiKey apiKey() {
        return new ApiKey("JWT", "Authorization", "header");
    }

    private SecurityContext securityContext() {
        return springfox
                .documentation
                .spi.service
                .contexts
                .SecurityContext
                .builder()
                .securityReferences(defaultAuth()).forPaths(PathSelectors.any()).build();
    }

    List<SecurityReference> defaultAuth() {
        AuthorizationScope authorizationScope = new AuthorizationScope("global", "accessEverything");
        AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
        authorizationScopes[0] = authorizationScope;
        return Arrays.asList(new SecurityReference("JWT", authorizationScopes));
    }




    //http://localhost:8089/swagger-ui/#/
}