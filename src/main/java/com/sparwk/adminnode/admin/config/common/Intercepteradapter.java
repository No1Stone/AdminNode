package com.sparwk.adminnode.admin.config.common;

import com.google.gson.Gson;
import com.sparwk.adminnode.admin.config.filter.token.TokenDecoding;
import com.sparwk.adminnode.admin.config.filter.token.UserInfoDTO;
import com.sparwk.adminnode.admin.config.util.ClientIp;
import com.sparwk.adminnode.admin.jpa.entity.permissionSet.PermissionMenuSetEntity;
import com.sparwk.adminnode.admin.jpa.repository.permissionSet.PermissionMenuSetRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Component
public class Intercepteradapter implements HandlerInterceptor {

    @Autowired
    private ClientIp clientIp;
    @Autowired
    private TokenDecoding tokenDecoding;
    @Autowired
    private PermissionMenuSetRepository permissionMenuSetRepository;

    @Value("${security.jwt.token.secret-key}")
    private String jwt;
    private final Logger logger = LoggerFactory.getLogger(Intercepteradapter.class);

    @Override
    public boolean preHandle(HttpServletRequest request,
                             HttpServletResponse response, Object handler) throws Exception {
        if (request.getMethod().equals("OPTIONS")) {
            //CORS인증 유효 처리를 위해
            return true;
        }
        Gson gson = new Gson();
        String reqAuthorization = request.getHeader("Authorization");
        logger.info("reqAuthorization - {}", reqAuthorization);
        String token = reqAuthorization.trim().substring(6);
        logger.info("token - {}", token);
        String sub = tokenDecoding.getSubject(token);
        UserInfoDTO userInfoDTO = gson.fromJson(sub, UserInfoDTO.class);
        logger.info("userInfoDTO - {}", userInfoDTO);
        request.setAttribute("UserInfoDTO", userInfoDTO);
        List<PermissionMenuSetEntity> adminPermission = permissionMenuSetRepository
                .findByAdminPermissionId(Long.parseLong(userInfoDTO.getPermission().get("assignSeq")));
        request.setAttribute("adminPermission", adminPermission);
        logger.info("permission select - {}", gson.toJson(adminPermission, List.class));
        return HandlerInterceptor.super.preHandle(request, response, handler);
    }

    @Override
    public void postHandle(HttpServletRequest request,
                           HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
    }

    @Override
    public void afterCompletion(HttpServletRequest request,
                                HttpServletResponse response, Object handler, Exception ex) throws Exception {
    }

}
