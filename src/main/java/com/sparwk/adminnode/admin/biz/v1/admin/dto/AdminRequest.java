package com.sparwk.adminnode.admin.biz.v1.admin.dto;

import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class AdminRequest {

    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long adminId;
    @Schema(description = "Admin Email", nullable = false, example = "admin@sparwk.com")
    private String adminEmail;
    @Schema(description = "Full Name", nullable = false, example = "Yoon Jung Sub")
    private String fullName;
    @Schema(description = "국가번호", nullable = false, example = "082")
    private String dial;
    @Schema(description = "전화번호", nullable = false, example = "01012345678")
    private String phoneNumber;
    @Schema(description = "권한 id값 Seq 사용", nullable = false, example = "1")
    private Long permissionAssignSeq;
    @Schema(description = "통합관리자", nullable = false, example = "통합관리자")
    private String permissionAssignName;
    @Schema(description = "설명", nullable = true, example = "설명")
    private String description;
    @Schema(description = "관리자 사용여부 Y/N", nullable = false, defaultValue = "Y", allowableValues = {"Y", "N"})
    private YnTypeEnum useType;
    @Schema(description = "관리자 로그인 잠김여부 Y/N", nullable = false, defaultValue = "N", allowableValues = {"Y", "N"})
    private YnTypeEnum lockType;
    @Schema(description = "등록자", nullable = false, example = "10000000019")
    private Long regUsr;
    @Schema(description = "수정자", nullable = false, example = "10000000019")
    private Long modUsr;
}
