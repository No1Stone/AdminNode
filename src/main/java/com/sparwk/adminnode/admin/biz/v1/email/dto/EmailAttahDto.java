package com.sparwk.adminnode.admin.biz.v1.email.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Setter
@Getter
@NoArgsConstructor
public class EmailAttahDto {

    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long emailSeq;
    @Schema(description = "첨부파일 순번", nullable = false, example = "1")
    private int attachOrder;
    @Schema(description = "Email 첨부파일 제목", nullable = false, example = "demo.txt")
    private String attachFileName;
    @Schema(description = "Email 첨부파일 크기", nullable = true, example = "100000")
    private int attachFileSize;

    @Builder
    public EmailAttahDto(Long emailSeq,
                         int attachOrder,
                         String attachFileName,
                         int attachFileSize
    ) {
        this.emailSeq = emailSeq;
        this.attachOrder = attachOrder;
        this.attachFileName = attachFileName;
        this.attachFileSize = attachFileSize;
    }
}
