package com.sparwk.adminnode.admin.biz.v1.anr.dto;

import lombok.*;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class AnrRequest {

//    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
//    private Long anrSeq;
//    @Schema(description = "A&R 이름", nullable = false, example = "Reject")
//    private String anrName;
//    @Schema(description = "코드 9자리", nullable = false, example = "ANR000001")
//    private String anrGroupCd;
//    @Schema(description = "코드 9자리", nullable = false, example = "ARS000001")
//    private String anrCd;
//    @Schema(description = "코드 URL", nullable = false, example = "http://")
//    private String description;
//    @Schema(description = "코드 사용여부 Y/N", defaultValue = "Y", allowableValues = {"Y", "N"})
//    private YnEnumType useType;
//    @Schema(description = "등록일", pattern = "yyMMdd hh:mm:ss", example = "2021-12-08T05:44:26.627991")
//    private LocalDateTime regDt;
//    @Schema(description = "등록자", nullable = false, example = "10000001")
//    private Long regUsr;
//    @Schema(description = "수정일", pattern = "yyMMdd hh:mm:ss", example = "2021-12-08T05:44:26.627991")
//    private LocalDateTime modDt;
//    @Schema(description = "수정자", nullable = false, example = "10000001")
//    private Long modUsr;
//
//    @Builder
//    public AnrDto(Long anrSeq,
//                  String anrName,
//                  String anrGroupCd,
//                  String anrCd,
//                  String description,
//                  YnEnumType useType,
//                  LocalDateTime regDt,
//                  Long regUsr,
//                  LocalDateTime modDt,
//                  Long modUsr
//    ) {
//        this.anrSeq = anrSeq;
//        this.anrName = anrName;
//        this.anrGroupCd = anrGroupCd;
//        this.anrCd = anrCd;
//        this.description = description;
//        this.useType = useType;
//        this.regDt = regDt;
//        this.regUsr = regUsr;
//        this.modDt = modDt;
//        this.modUsr = modUsr;
//    }

    private AnrDto anrDto;
}
