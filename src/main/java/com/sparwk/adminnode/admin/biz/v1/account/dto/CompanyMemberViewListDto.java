package com.sparwk.adminnode.admin.biz.v1.account.dto;

import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Setter
@Getter
@NoArgsConstructor
public class CompanyMemberViewListDto {

    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long accntId;
    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long profileId;
    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private String accntEmail;
    @Schema(description = "COMPANY NAME", nullable = false, example = "Sparwk")
    private String companyName;
    @Schema(description = "COMPANY TYPE NAME", nullable = false, example = "Music publisher")
    private String companyType;
    @Schema(description = "BUSSINESS_LOCATION", nullable = true, example = "USA")
    private String bussinessLocation;
    @Schema(description = "COMPANY LICENSE VERIFY YN", nullable = true, example = "Y")
    private YnTypeEnum companyLicenseVerifyYn;
    @Schema(description = "IPI NUMBER VARIFY YN", nullable = true, example = "Y")
    private YnTypeEnum ipiNumberVarifyYn;
    @Schema(description = "IPI_NUMBER", nullable = true, example = "W1234567890")
    private String ipiNumber;
    @Schema(description = "VAT_NUMBER", nullable = true, example = "W1234567890")
    private String vatNumber;
    @Schema(description = "등록일", pattern = "yyMMdd hh:mm:ss", example = "2021-12-08T05:44:26.627991")
    private LocalDateTime regDt;


    @Builder
    public CompanyMemberViewListDto(Long accntId,
                                    Long profileId,
                                    String accntEmail,
                                    String companyName,
                                    String companyType,
                                    String bussinessLocation,
                                    YnTypeEnum companyLicenseVerifyYn,
                                    YnTypeEnum ipiNumberVarifyYn,
                                    String ipiNumber,
                                    String vatNumber,
                                    LocalDateTime regDt
    ) {
        this.accntId = accntId;
        this.profileId = profileId;
        this.accntEmail = accntEmail;
        this.companyName = companyName;
        this.companyType = companyType;
        this.bussinessLocation = bussinessLocation;
        this.companyLicenseVerifyYn = companyLicenseVerifyYn;
        this.ipiNumberVarifyYn = ipiNumberVarifyYn;
        this.ipiNumber = ipiNumber;
        this.vatNumber = vatNumber;
        this.regDt = regDt;
    }
}
