package com.sparwk.adminnode.admin.biz.v1.role.service;

import com.sparwk.adminnode.admin.biz.v1.CommonPagingListDto;
import com.sparwk.adminnode.admin.biz.v1.admin.service.AdminActiveLogService;
import com.sparwk.adminnode.admin.biz.v1.role.dto.RoleDetailCodeDto;
import com.sparwk.adminnode.admin.biz.v1.role.dto.RoleDetailCodeExcelDto;
import com.sparwk.adminnode.admin.biz.v1.role.dto.RoleDetailCodeRequest;
import com.sparwk.adminnode.admin.config.common.ActiveCodeConst;
import com.sparwk.adminnode.admin.config.common.GetAccountId;
import com.sparwk.adminnode.admin.config.common.Response;
import com.sparwk.adminnode.admin.config.common.ResultCodeConst;
import com.sparwk.adminnode.admin.config.util.ExcelWriter;
import com.sparwk.adminnode.admin.jpa.entity.FormatTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.PeriodTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.admin.AdminActiveLogEntity;
import com.sparwk.adminnode.admin.jpa.entity.role.RoleCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.role.RoleCodeEntity;
import com.sparwk.adminnode.admin.jpa.entity.role.RoleDetailCodeEntity;
import com.sparwk.adminnode.admin.jpa.entity.role.RoleDetailCodeSorterEnum;
import com.sparwk.adminnode.admin.jpa.repository.role.RoleCodeRepository;
import com.sparwk.adminnode.admin.jpa.repository.role.RoleDetailCodeRepository;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class RoleDetailCodeService {

    private final RoleCodeRepository roleCodeRepository;
    private final RoleDetailCodeRepository roleDetailCodeRepository;
    private final AdminActiveLogService adminActiveLogService;

    private final HttpServletRequest httpServletRequest;
    private final MessageSourceAccessor messageSourceAccessor;

    private final Logger logger = LoggerFactory.getLogger(RoleDetailCodeService.class);


    public Response<CommonPagingListDto> getList(String pcode, RoleCateEnum cate,
                                                     String val,
                                                     FormatTypeEnum cdFormatType,
                                                     YnTypeEnum useType,
                                                     PeriodTypeEnum periodType,
                                                     String sdate,
                                                     String edate,
                                                     RoleDetailCodeSorterEnum sorter,
                                                     PageRequest pageRequest) {

        Response<CommonPagingListDto> res = new Response<>();

        //부모코드 없을 때
        if(pcode == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<RoleCodeEntity> roleCode = Optional.ofNullable(
                roleCodeRepository.findByCode(pcode)
        );

        logger.info("roleCode - {}", roleCode);

        //부모코드 조회내용 없을 때
        if(!roleCode.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }
                
        List<RoleDetailCodeDto> roleDetailCodeList =
                roleDetailCodeRepository.findQryAll(pcode, cate, val, cdFormatType, useType, periodType, sdate, edate, sorter, pageRequest);

        logger.info("roleDetailCodeList - {}", roleDetailCodeList);

        //조회내용 없을 때
        if(roleDetailCodeList == null || roleDetailCodeList.size() < 1){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        CommonPagingListDto list = new CommonPagingListDto();

        logger.info("cdFormatType - {}", cdFormatType);

        //총 카운트
        long totalSize = roleDetailCodeRepository.countQryAll(pcode, cate, val, cdFormatType, useType, periodType, sdate, edate);
        long totalPage = totalSize / pageRequest.getPageSize();
        long mod = totalSize % pageRequest.getPageSize();
        if(mod > 0) {
            totalPage += 1;
        }

        list.setList(roleDetailCodeList);
        list.setSize(pageRequest.getPageSize());
        list.setPage(pageRequest.getPageNumber());
        list.setTotalPage(totalPage);
        list.setTotalSize(totalSize);

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(list);
        return res;
    }

    public Response<CommonPagingListDto> getUseYList(String pcode, RoleCateEnum cate, RoleDetailCodeSorterEnum sorter, String val, FormatTypeEnum cdFormatType) {

        Response<CommonPagingListDto> res = new Response<>();

        //부모코드 없을 때
        if(pcode == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<RoleCodeEntity> roleCode = Optional.ofNullable(
                roleCodeRepository.findByCode(pcode)
        );

        logger.info("roleCode - {}", roleCode);

        //부모코드 조회내용 없을 때
        if(!roleCode.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        List<RoleDetailCodeDto> roleDetailCodeList = roleDetailCodeRepository.findQryUseY(pcode, cate, sorter, val, cdFormatType);

        logger.info("roleDetailCodeList - {}", roleDetailCodeList);

        //조회내용 없을 때
        if(roleDetailCodeList == null || roleDetailCodeList.size() < 1){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        CommonPagingListDto list = new CommonPagingListDto();

        PageRequest pageRequest = PageRequest.of(10, 10);

        //총 카운트
        long totalSize = roleDetailCodeRepository.countQryUseY(pcode, cate, sorter, val, cdFormatType);
        long totalPage = totalSize / pageRequest.getPageSize();
        long mod = totalSize % pageRequest.getPageSize();
        if(mod > 0) {
            totalPage += 1;
        }

        list.setList(roleDetailCodeList);
        list.setSize(pageRequest.getPageSize());
        list.setPage(pageRequest.getPageNumber());
        list.setTotalPage(totalPage);
        list.setTotalSize(totalSize);

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(list);
        return res;
    }

    public Response<CommonPagingListDto> getPopularYList(String pcode, RoleDetailCodeSorterEnum sorter) {

        Response<CommonPagingListDto> res = new Response<>();

        //부모코드 없을 때
        if(pcode == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<RoleCodeEntity> roleCode = Optional.ofNullable(
                roleCodeRepository.findByCode(pcode)
        );

        logger.info("roleCode - {}", roleCode);

        //부모코드 조회내용 없을 때
        if(!roleCode.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        List<RoleDetailCodeDto> roleDetailCodeList = roleDetailCodeRepository.findPopularUseY(pcode, sorter);

        logger.info("roleDetailCodeList - {}", roleDetailCodeList);

        //조회내용 없을 때
        if(roleDetailCodeList == null || roleDetailCodeList.size() < 1){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        CommonPagingListDto list = new CommonPagingListDto();

        PageRequest pageRequest = PageRequest.of(10, 10);

        //총 카운트
        long totalSize = roleDetailCodeRepository.countPopularUseY(pcode, sorter);
        long totalPage = totalSize / pageRequest.getPageSize();
        long mod = totalSize % pageRequest.getPageSize();
        if(mod > 0) {
            totalPage += 1;
        }

        list.setList(roleDetailCodeList);
        list.setSize(pageRequest.getPageSize());
        list.setPage(pageRequest.getPageNumber());
        list.setTotalPage(totalPage);
        list.setTotalSize(totalSize);

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(list);
        return res;
    }

    public Response<CommonPagingListDto> getMatchingYList(String pcode, RoleDetailCodeSorterEnum sorter) {

        Response<CommonPagingListDto> res = new Response<>();

        //부모코드 없을 때
        if(pcode == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<RoleCodeEntity> roleCode = Optional.ofNullable(
                roleCodeRepository.findByCode(pcode)
        );

        logger.info("roleCode - {}", roleCode);

        //부모코드 조회내용 없을 때
        if(!roleCode.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        List<RoleDetailCodeDto> roleDetailCodeList = roleDetailCodeRepository.findMatchingUseY(pcode, sorter);

        logger.info("roleDetailCodeList - {}", roleDetailCodeList);

        //조회내용 없을 때
        if(roleDetailCodeList == null || roleDetailCodeList.size() < 1){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        CommonPagingListDto list = new CommonPagingListDto();

        PageRequest pageRequest = PageRequest.of(10, 10);

        //총 카운트
        long totalSize = roleDetailCodeRepository.countMatchingUseY(pcode, sorter);
        long totalPage = totalSize / pageRequest.getPageSize();
        long mod = totalSize % pageRequest.getPageSize();
        if(mod > 0) {
            totalPage += 1;
        }

        list.setList(roleDetailCodeList);
        list.setSize(pageRequest.getPageSize());
        list.setPage(pageRequest.getPageNumber());
        list.setTotalPage(totalPage);
        list.setTotalSize(totalSize);

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(list);
        return res;
    }

    public Response<CommonPagingListDto> getCreditYList(String pcode, RoleDetailCodeSorterEnum sorter) {

        Response<CommonPagingListDto> res = new Response<>();

        //부모코드 없을 때
        if(pcode == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<RoleCodeEntity> roleCode = Optional.ofNullable(
                roleCodeRepository.findByCode(pcode)
        );

        logger.info("roleCode - {}", roleCode);

        //부모코드 조회내용 없을 때
        if(!roleCode.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        List<RoleDetailCodeDto> roleDetailCodeList = roleDetailCodeRepository.findCreditUseY(pcode, sorter);

        logger.info("roleDetailCodeList - {}", roleDetailCodeList);

        //조회내용 없을 때
        if(roleDetailCodeList == null || roleDetailCodeList.size() < 1){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        CommonPagingListDto list = new CommonPagingListDto();

        PageRequest pageRequest = PageRequest.of(10, 10);

        //총 카운트
        long totalSize = roleDetailCodeRepository.countCreditUseY(pcode, sorter);
        long totalPage = totalSize / pageRequest.getPageSize();
        long mod = totalSize % pageRequest.getPageSize();
        if(mod > 0) {
            totalPage += 1;
        }

        list.setList(roleDetailCodeList);
        list.setSize(pageRequest.getPageSize());
        list.setPage(pageRequest.getPageNumber());
        list.setTotalPage(totalPage);
        list.setTotalSize(totalSize);

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(list);
        return res;
    }

    public Response<CommonPagingListDto> getSplitSheetYList(String pcode, RoleDetailCodeSorterEnum sorter) {

        Response<CommonPagingListDto> res = new Response<>();

        //부모코드 없을 때
        if(pcode == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<RoleCodeEntity> roleCode = Optional.ofNullable(
                roleCodeRepository.findByCode(pcode)
        );

        logger.info("roleCode - {}", roleCode);

        //부모코드 조회내용 없을 때
        if(!roleCode.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        List<RoleDetailCodeDto> roleDetailCodeList = roleDetailCodeRepository.findSplitSheetUseY(pcode, sorter);

        logger.info("roleDetailCodeList - {}", roleDetailCodeList);

        //조회내용 없을 때
        if(roleDetailCodeList == null || roleDetailCodeList.size() < 1){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        CommonPagingListDto list = new CommonPagingListDto();

        PageRequest pageRequest = PageRequest.of(10, 10);

        //총 카운트
        long totalSize = roleDetailCodeRepository.countSplitSheetUseY(pcode, sorter);
        long totalPage = totalSize / pageRequest.getPageSize();
        long mod = totalSize % pageRequest.getPageSize();
        if(mod > 0) {
            totalPage += 1;
        }

        list.setList(roleDetailCodeList);
        list.setSize(pageRequest.getPageSize());
        list.setPage(pageRequest.getPageNumber());
        list.setTotalPage(totalPage);
        list.setTotalSize(totalSize);

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(list);
        return res;
    }

    public Response<Optional<RoleDetailCodeDto>> getOne(String pcode, Long id) {

        Response<Optional<RoleDetailCodeDto>> res = new Response<>();

        //부모코드 없을 때
        if(pcode == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<RoleCodeEntity> roleCode = Optional.ofNullable(
                roleCodeRepository.findByCode(pcode)
        );

        logger.info("roleCode - {}", roleCode);

        //부모코드 조회내용 없을 때
        if(!roleCode.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<RoleDetailCodeEntity> roleDetailCode = roleDetailCodeRepository.findById(id);

        logger.info("roleDetailCode - {}", roleDetailCode);

        //조회내용 없을 때
        if(!roleDetailCode.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }
        

        Optional<RoleDetailCodeDto> dtoPage =

                roleDetailCode
                        .map(p -> RoleDetailCodeDto.builder()
                                .roleDetailCodeSeq(p.getRoleDetailCodeSeq())
                                .pcode(p.getRoleCodeEntity().getCode())
                                .pcodeVal(p.getRoleCodeEntity().getVal())
                                .dcode(p.getDcode())
                                .role(p.getRole())
                                .formatType(p.getFormatType())
                                .useType(p.getUseType())
                                .popularType(p.getPopularType())
                                .matchingRoleYn(p.getMatchingRoleYn())
                                .creditRoleYn(p.getCreditRoleYn())
                                .splitSheetRoleYn(p.getSplitSheetRoleYn())
                                .abbeviation(p.getAbbeviation())
                                .customCode(p.getCustomCode())
                                .description(p.getDescription())
                                .hit(p.getHit())
                                .regUsr(p.getRegUsr())
                                .regDt(p.getRegDt())
                                .modUsr(p.getModUsr())
                                .modDt(p.getModDt())
                                .build()
                        );

        logger.info("RoleDetailCode - {}", dtoPage);

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(dtoPage);
        return res;
    }

    @Transactional
    public Response<Void> saveContent(String reqRoleCode, RoleDetailCodeRequest request, HttpServletRequest req) {

        Response<Void> res = new Response<>();
        String dcode;

        //부모코드 없을 때
        if(request.getRoleDetailCodeDto().getPcode() == null){
            res.setResultCd(ResultCodeConst.NOT_VALID_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<RoleCodeEntity> roleCodeEntity =
                Optional.ofNullable(roleCodeRepository.findByCode(request.getRoleDetailCodeDto().getPcode()));

        logger.info("roleCodeEntity - {} ", roleCodeEntity);

        //부모코드 조회내용 없을 때
        if(!roleCodeEntity.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Format 값 없거나 틀릴 때
        if(request.getRoleDetailCodeDto().getFormatType() == null || (request.getRoleDetailCodeDto().getFormatType() != FormatTypeEnum.DDEX && request.getRoleDetailCodeDto().getFormatType() != FormatTypeEnum.SPARWK)){
            res.setResultCd(ResultCodeConst.NOT_VALID_FORMAT.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //USEYN 값 없거나 틀릴 때
        if(request.getRoleDetailCodeDto().getUseType() == null || (request.getRoleDetailCodeDto().getUseType() != YnTypeEnum.Y && request.getRoleDetailCodeDto().getUseType() != YnTypeEnum.N)){
            res.setResultCd(ResultCodeConst.NOT_VALID_USEYN.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //POPULAR 값 없거나 틀릴 때
        if(request.getRoleDetailCodeDto().getPopularType() == null || (request.getRoleDetailCodeDto().getPopularType() != YnTypeEnum.Y && request.getRoleDetailCodeDto().getPopularType() != YnTypeEnum.N)){
            res.setResultCd(ResultCodeConst.NOT_VALID_POPULAR.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if(request.getRoleDetailCodeDto().getRole() == null){
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        try {

            //seq 번호 불러옴
            int k = roleCodeRepository.getLastSeq(reqRoleCode) + 1;

            if(k < 10)
                dcode = reqRoleCode + "00000" + k;
            else if(k < 100)
                dcode = reqRoleCode + "0000" + k;
            else if(k < 1000)
                dcode = reqRoleCode + "000" + k;
            else if(k < 10000)
                dcode = reqRoleCode + "00" + k;
            else if(k < 100000)
                dcode = reqRoleCode + "0" + k;
            else if(k < 1000000)
                dcode = reqRoleCode + k;
            else
                dcode = reqRoleCode + k;

            logger.info("/////////////////////////////////////// - {}", dcode);

            RoleDetailCodeEntity newRoleDetailCode = RoleDetailCodeEntity.builder()
                    .roleCodeEntity(roleCodeEntity.get())
                    .pcode(roleCodeEntity.get().getCode())
                    .dcode(dcode)
                    .role(request.getRoleDetailCodeDto().getRole())
                    .formatType(request.getRoleDetailCodeDto().getFormatType())
                    .popularType(request.getRoleDetailCodeDto().getPopularType())
                    .matchingRoleYn(request.getRoleDetailCodeDto().getMatchingRoleYn())
                    .creditRoleYn(request.getRoleDetailCodeDto().getCreditRoleYn())
                    .splitSheetRoleYn(request.getRoleDetailCodeDto().getSplitSheetRoleYn())
                    .abbeviation(request.getRoleDetailCodeDto().getAbbeviation())
                    .customCode(request.getRoleDetailCodeDto().getCustomCode())
                    .description(request.getRoleDetailCodeDto().getDescription())
                    .useType(request.getRoleDetailCodeDto().getUseType())
                    .hit(0L)
                    .build();


            RoleDetailCodeEntity saved = roleDetailCodeRepository.save(newRoleDetailCode);

            roleCodeRepository.updateLastSeq(reqRoleCode, k);
//            Optional<RoleDetailCodeEntity> roleDetailCode = roleDetailCodeRepository.findById(saved.getRoleDetailCodeSeq());
//
//            Optional<RoleDetailCodeDto> dtoPage =
//
//                    roleDetailCode
//                            .map(p -> RoleDetailCodeDto.builder()
//                                    .roleDetailCodeSeq(p.getRoleDetailCodeSeq())
//                                    .pcode(p.getRoleCodeEntity().getCode())
//                                    .pcodeVal(p.getRoleCodeEntity().getVal())
//                                    .dcode(p.getDcode())
//                                    .role(p.getRole())
//                                    .formatType(p.getFormat())
//                                    .popularType(p.getPopularType())
//                                    .abbeviation(p.getAbbeviation())
//                                    .customCode(p.getCustomCode())
//                                    .description(p.getDescription())
//                                    .useType(p.getUseType())
//                                    .hit(p.getHit())
//                                    .regUsr(p.getRegUsr())
//                                    .regDt(p.getRegDt())
//                                    .modUsr(p.getModUsr())
//                                    .modDt(p.getModDt())
//                                    .build()
//                            );
//
//            return dtoPage;

            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
//            res.setResult(dtoPage);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));

            String menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_ROLES.getCode());
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_CREATE.getCode());

            //netive query
            String usrName = adminActiveLogService.findQryUsrName(saved.getRegUsr());

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(saved.getRegDt())
                    .adminId(saved.getRegUsr())
                    .menuName(menuName)
                    .activeType("C")
                    .contentsId(saved.getRoleDetailCodeSeq())
                    .activeMsg(usrName + " " + activeMsg + " '" + saved.getRole() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e){
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }

        return res;
    }

    @Transactional
    public Response<Void> updateYn(String pcode, Long id, HttpServletRequest req) {

        Response<Void> res = new Response<>();

        //부모코드 없을 때
        if(pcode == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<RoleCodeEntity> roleCode = Optional.ofNullable(
                roleCodeRepository.findByCode(pcode)
        );

        logger.info("roleCode - {}", roleCode);

        //부모코드 조회내용 없을 때
        if(roleCode == null){
            res.setResultCd(ResultCodeConst.NOT_FOUND_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //id값이 없을 때
        if(id == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        RoleDetailCodeEntity roleDetailCode = roleDetailCodeRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("해당 내용이 없습니다"));

        logger.info("roleDetailCode - {}", roleDetailCode);

        //조회내용 없을 때
        if(roleDetailCode == null){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        YnTypeEnum useType = roleDetailCode.getUseType();
        YnTypeEnum newUseType = (useType == YnTypeEnum.Y) ? YnTypeEnum.N : YnTypeEnum.Y;

        logger.info("newUseType - {} ", newUseType);

        try {
            roleDetailCode.updateYn(newUseType);
            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            //res.setResult(roleDetailCode);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));

            String menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_ROLES.getCode());
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_MODIFY.getCode());

            //netive query
            String usrName = adminActiveLogService.findQryUsrName(roleDetailCode.getModUsr());

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(roleDetailCode.getModDt())
                    .adminId(roleDetailCode.getModUsr())
                    .menuName(menuName)
                    .activeType("U")
                    .contentsId(roleDetailCode.getRoleDetailCodeSeq())
                    .activeMsg(usrName + " " + activeMsg + " '" + roleDetailCode.getRole() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e) {
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }
        return res;

    }

    @Transactional
    public Response<Void> updatePopularYn(String pcode, Long id, HttpServletRequest req) {

        Response<Void> res = new Response<>();

        //부모코드 없을 때
        if(pcode == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<RoleCodeEntity> roleCode = Optional.ofNullable(
                roleCodeRepository.findByCode(pcode)
        );

        logger.info("roleCode - {}", roleCode);

        //부모코드 조회내용 없을 때
        if(roleCode == null){
            res.setResultCd(ResultCodeConst.NOT_FOUND_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //id값이 없을 때
        if(id == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        RoleDetailCodeEntity roleDetailCode = roleDetailCodeRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("해당 내용이 없습니다"));

        logger.info("roleDetailCode - {}", roleDetailCode);

        //조회내용 없을 때
        if(roleDetailCode == null){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        YnTypeEnum popularType = roleDetailCode.getPopularType();
        YnTypeEnum newType = (popularType == YnTypeEnum.Y) ? YnTypeEnum.N : YnTypeEnum.Y;

        logger.info("newUseType - {} ", newType);

        try {
            roleDetailCode.updatePopularYn(newType);
            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            //res.setResult(roleDetailCode);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));

            String menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_ROLES.getCode());
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_MODIFY.getCode());

            //netive query
            String usrName = adminActiveLogService.findQryUsrName(roleDetailCode.getModUsr());

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(roleDetailCode.getModDt())
                    .adminId(roleDetailCode.getModUsr())
                    .menuName(menuName)
                    .activeType("U")
                    .contentsId(roleDetailCode.getRoleDetailCodeSeq())
                    .activeMsg(usrName + " " + activeMsg + " '" + roleDetailCode.getRole() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e) {
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }
        return res;

    }

    @Transactional
    public Response<Void> updateMatchingRoleYn(String pcode, Long id, HttpServletRequest req) {

        Response<Void> res = new Response<>();

        //부모코드 없을 때
        if(pcode == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<RoleCodeEntity> roleCode = Optional.ofNullable(
                roleCodeRepository.findByCode(pcode)
        );

        logger.info("roleCode - {}", roleCode);

        //부모코드 조회내용 없을 때
        if(roleCode == null){
            res.setResultCd(ResultCodeConst.NOT_FOUND_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //id값이 없을 때
        if(id == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        RoleDetailCodeEntity roleDetailCode = roleDetailCodeRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("해당 내용이 없습니다"));

        logger.info("roleDetailCode - {}", roleDetailCode);

        //조회내용 없을 때
        if(roleDetailCode == null){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        YnTypeEnum popularType = roleDetailCode.getMatchingRoleYn();
        YnTypeEnum newType = (popularType == YnTypeEnum.Y) ? YnTypeEnum.N : YnTypeEnum.Y;

        logger.info("newUseType - {} ", newType);

        try {
            roleDetailCode.updateMatchingRoleYn(newType);
            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            //res.setResult(roleDetailCode);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));

            String menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_ROLES.getCode());
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_MODIFY.getCode());

            //netive query
            String usrName = adminActiveLogService.findQryUsrName(roleDetailCode.getModUsr());

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(roleDetailCode.getModDt())
                    .adminId(roleDetailCode.getModUsr())
                    .menuName(menuName)
                    .activeType("U")
                    .contentsId(roleDetailCode.getRoleDetailCodeSeq())
                    .activeMsg(usrName + " " + activeMsg + " '" + roleDetailCode.getRole() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e) {
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }
        return res;

    }

    @Transactional
    public Response<Void> updateCreditRoleYn(String pcode, Long id, HttpServletRequest req) {

        Response<Void> res = new Response<>();

        //부모코드 없을 때
        if(pcode == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<RoleCodeEntity> roleCode = Optional.ofNullable(
                roleCodeRepository.findByCode(pcode)
        );

        logger.info("roleCode - {}", roleCode);

        //부모코드 조회내용 없을 때
        if(roleCode == null){
            res.setResultCd(ResultCodeConst.NOT_FOUND_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //id값이 없을 때
        if(id == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        RoleDetailCodeEntity roleDetailCode = roleDetailCodeRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("해당 내용이 없습니다"));

        logger.info("roleDetailCode - {}", roleDetailCode);

        //조회내용 없을 때
        if(roleDetailCode == null){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        YnTypeEnum popularType = roleDetailCode.getCreditRoleYn();
        YnTypeEnum newType = (popularType == YnTypeEnum.Y) ? YnTypeEnum.N : YnTypeEnum.Y;

        logger.info("newUseType - {} ", newType);

        try {
            roleDetailCode.updateCreditRoleYn(newType);
            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            //res.setResult(roleDetailCode);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));

            String menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_ROLES.getCode());
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_MODIFY.getCode());

            //netive query
            String usrName = adminActiveLogService.findQryUsrName(roleDetailCode.getModUsr());

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(roleDetailCode.getModDt())
                    .adminId(roleDetailCode.getModUsr())
                    .menuName(menuName)
                    .activeType("U")
                    .contentsId(roleDetailCode.getRoleDetailCodeSeq())
                    .activeMsg(usrName + " " + activeMsg + " '" + roleDetailCode.getRole() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e) {
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }
        return res;

    }

    @Transactional
    public Response<Void> updateSplitSheetRoleYn(String pcode, Long id, HttpServletRequest req) {

        Response<Void> res = new Response<>();

        //부모코드 없을 때
        if(pcode == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<RoleCodeEntity> roleCode = Optional.ofNullable(
                roleCodeRepository.findByCode(pcode)
        );

        logger.info("roleCode - {}", roleCode);

        //부모코드 조회내용 없을 때
        if(roleCode == null){
            res.setResultCd(ResultCodeConst.NOT_FOUND_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //id값이 없을 때
        if(id == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        RoleDetailCodeEntity roleDetailCode = roleDetailCodeRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("해당 내용이 없습니다"));

        logger.info("roleDetailCode - {}", roleDetailCode);

        //조회내용 없을 때
        if(roleDetailCode == null){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        YnTypeEnum popularType = roleDetailCode.getSplitSheetRoleYn();
        YnTypeEnum newType = (popularType == YnTypeEnum.Y) ? YnTypeEnum.N : YnTypeEnum.Y;

        logger.info("newUseType - {} ", newType);

        try {
            roleDetailCode.updateSplitSheetRoleYn(newType);
            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            //res.setResult(roleDetailCode);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));

            String menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_ROLES.getCode());
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_MODIFY.getCode());

            //netive query
            String usrName = adminActiveLogService.findQryUsrName(roleDetailCode.getModUsr());

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(roleDetailCode.getModDt())
                    .adminId(roleDetailCode.getModUsr())
                    .menuName(menuName)
                    .activeType("U")
                    .contentsId(roleDetailCode.getRoleDetailCodeSeq())
                    .activeMsg(usrName + " " + activeMsg + " '" + roleDetailCode.getRole() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e) {
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }
        return res;

    }

    @Transactional
    public Response<Optional<RoleDetailCodeDto>> updateContent(String pcode, Long id, RoleDetailCodeRequest request, HttpServletRequest req) {

        Response<Optional<RoleDetailCodeDto>> res = new Response<>();

        //부모코드 없을 때
        if(pcode == null){
            res.setResultCd(ResultCodeConst.NOT_VALID_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<RoleCodeEntity> roleCode = Optional.ofNullable(roleCodeRepository.findByCode(request.getRoleDetailCodeDto().getDcode()));

        logger.info("roleCode - {}", roleCode);

        //부모코드 조회내용 없을 때
        if(roleCode == null){
            res.setResultCd(ResultCodeConst.NOT_FOUND_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //id값이 없을 때
        if(id == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }
        
        RoleDetailCodeEntity roleDetailCode = roleDetailCodeRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("해당 내용이 없습니다"));
    
        logger.info("roleDetailCode - {} ", roleDetailCode);

        //조회내용 없을 때
        if(roleDetailCode == null){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Format 값 없거나 틀릴 때
        if(request.getRoleDetailCodeDto().getFormatType() == null || (request.getRoleDetailCodeDto().getFormatType() != FormatTypeEnum.DDEX && request.getRoleDetailCodeDto().getFormatType() != FormatTypeEnum.SPARWK)){
            res.setResultCd(ResultCodeConst.NOT_VALID_FORMAT.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //USEYN 값 없거나 틀릴 때
        if(request.getRoleDetailCodeDto().getUseType() == null || (request.getRoleDetailCodeDto().getUseType() != YnTypeEnum.Y && request.getRoleDetailCodeDto().getUseType() != YnTypeEnum.N)){
            res.setResultCd(ResultCodeConst.NOT_VALID_USEYN.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //POPULAR 값 없거나 틀릴 때
        if(request.getRoleDetailCodeDto().getPopularType() == null || (request.getRoleDetailCodeDto().getPopularType() != YnTypeEnum.Y && request.getRoleDetailCodeDto().getPopularType() != YnTypeEnum.N)){
            res.setResultCd(ResultCodeConst.NOT_VALID_POPULAR.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if(request.getRoleDetailCodeDto().getRole() == null){
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        try {
            roleDetailCode.update(
                    request.getRoleDetailCodeDto().getPcode(),
                    request.getRoleDetailCodeDto().getDcode(),
                    request.getRoleDetailCodeDto().getFormatType(),
                    request.getRoleDetailCodeDto().getRole(),
                    request.getRoleDetailCodeDto().getAbbeviation(),
                    request.getRoleDetailCodeDto().getCustomCode(),
                    request.getRoleDetailCodeDto().getDescription(),
                    request.getRoleDetailCodeDto().getUseType(),
                    request.getRoleDetailCodeDto().getPopularType(),
                    request.getRoleDetailCodeDto().getMatchingRoleYn(),
                    request.getRoleDetailCodeDto().getCreditRoleYn(),
                    request.getRoleDetailCodeDto().getSplitSheetRoleYn()
            );

            Optional<RoleDetailCodeEntity> roleDetailCodeUpdate = roleDetailCodeRepository.findById(roleDetailCode.getRoleDetailCodeSeq());
    
            Optional<RoleDetailCodeDto> dtoPage =
                                                                                                                                                                                                    
                    roleDetailCodeUpdate
                            .map(p -> RoleDetailCodeDto.builder()
                                    .roleDetailCodeSeq(p.getRoleDetailCodeSeq())
                                    .pcode(p.getRoleCodeEntity().getCode())
                                    .pcodeVal(p.getRoleCodeEntity().getVal())
                                    .dcode(p.getDcode())
                                    .role(p.getRole())
                                    .formatType(p.getFormatType())
                                    .useType(p.getUseType())
                                    .popularType(p.getPopularType())
                                    .matchingRoleYn(p.getMatchingRoleYn())
                                    .creditRoleYn(p.getCreditRoleYn())
                                    .splitSheetRoleYn(p.getSplitSheetRoleYn())
                                    .abbeviation(p.getAbbeviation())
                                    .customCode(p.getCustomCode())
                                    .description(p.getDescription())
                                    .hit(p.getHit())
                                    .regUsr(p.getRegUsr())
                                    .regDt(p.getRegDt())
                                    .modUsr(p.getModUsr())
                                    .modDt(p.getModDt())
                                    .build()
                            );

            logger.info("dtoPage - {} ", dtoPage);

            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            res.setResult(dtoPage);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));

            String menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_ROLES.getCode());
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_MODIFY.getCode());

            //netive query
            String usrName = adminActiveLogService.findQryUsrName(roleDetailCode.getModUsr());

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(roleDetailCode.getModDt())
                    .adminId(roleDetailCode.getModUsr())
                    .menuName(menuName)
                    .activeType("U")
                    .contentsId(roleDetailCode.getRoleDetailCodeSeq())
                    .activeMsg(usrName + " " + activeMsg + " '" + roleDetailCode.getRole() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e){
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }

        return res;
    }

    @Transactional
    public Response<Void> deleteContent(String pcode, Long id, HttpServletRequest req) {

        Response<Void> res = new Response<>();

        //부모코드 없을 때
        if(pcode == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<RoleCodeEntity> roleCode = Optional.ofNullable(
                roleCodeRepository.findByCode(pcode)
        );

        logger.info("roleCode - {} ", roleCode);

        //부모코드 조회내용 없을 때
        if(roleCode == null){
            res.setResultCd(ResultCodeConst.NOT_FOUND_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //id값이 없을 때
        if(id == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        RoleDetailCodeEntity roleDetailCode = roleDetailCodeRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("해당 내용이 없습니다"));

        logger.info("roleDetailCode - {} ", roleDetailCode);

        //조회내용 없을 때
        if(roleDetailCode == null){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        try {
            roleDetailCodeRepository.deleteById(id);
            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            //res.setResult(roleDetailCode);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));
            Long adminId = getAccountId.ofId(httpServletRequest);

            String menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_ROLES.getCode());
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_DELETE.getCode());

            //netive query
            String usrName = adminActiveLogService.findQryUsrName(adminId);

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(LocalDateTime.now())
                    .adminId(adminId)
                    .menuName(menuName)
                    .activeType("D")
                    .contentsId(roleDetailCode.getRoleDetailCodeSeq())
                    .activeMsg(usrName + " " + activeMsg + " '" + roleDetailCode.getRole() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e) {
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }
        return res;

    }

    public List<RoleDetailCodeExcelDto> getExcelList(String pcode) {

        List<RoleDetailCodeExcelDto> getExcelList = roleDetailCodeRepository.findExcelList(pcode);

        logger.info("getExcelList - {} ", getExcelList);

        return getExcelList;
    }


    //엑셀 다운로드 요청
    public Map<String, Object> downExcelList(String roleCode) throws Exception {
        // 데이터 가져오기
        List<RoleDetailCodeExcelDto> excelList = this.getExcelList(roleCode);

        logger.info("excelList - {} ", excelList);

        return ExcelWriter.createExcelData(excelList, RoleDetailCodeExcelDto.class);
    }

    //마지막 코드 번호 확인
    public Integer getLastSeq(String code) {

        return roleDetailCodeRepository.getLastSeq(code);
    }

    //코드 번호 업데이트
    @Transactional
    public void updateLastSeq(String code, Integer k) {

        roleDetailCodeRepository.updateLastSeq(code, k);
    }

    //이름으로 코드검색
    public Long getOneName(String val) {

        return roleDetailCodeRepository.countByRole(val);
    }


    @Transactional
    public Response<Void> saveExcelContent(String pcode, RoleDetailCodeDto roleDetailCodeDto, HttpServletRequest req) {

        Response<Void> res = new Response<>();

        Optional<RoleCodeEntity> roleCode = Optional.ofNullable(roleCodeRepository.findByCode(pcode));

        logger.info("roleCode - {}", roleCode);

        //부모코드 조회내용 없을 때
        if (!roleCode.isPresent() || roleCode.equals("")) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Format 값 없거나 틀릴 때
        if (roleDetailCodeDto.getFormatType() == null || (roleDetailCodeDto.getFormatType() != FormatTypeEnum.DDEX && roleDetailCodeDto.getFormatType() != FormatTypeEnum.SPARWK)) {
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //USEYN 값 없거나 틀릴 때
        if (roleDetailCodeDto.getUseType() == null || (roleDetailCodeDto.getUseType() != YnTypeEnum.Y && roleDetailCodeDto.getUseType() != YnTypeEnum.N)) {
            res.setResultCd(ResultCodeConst.NOT_VALID_USEYN.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //POPULAR 값 없거나 틀릴 때
        if (roleDetailCodeDto.getPopularType() == null || (roleDetailCodeDto.getPopularType() != YnTypeEnum.Y && roleDetailCodeDto.getPopularType() != YnTypeEnum.N)) {
            res.setResultCd(ResultCodeConst.NOT_VALID_POPULAR.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Matching Role 값 없거나 틀릴 때
        if (roleDetailCodeDto.getMatchingRoleYn() == null || (roleDetailCodeDto.getMatchingRoleYn() != YnTypeEnum.Y && roleDetailCodeDto.getMatchingRoleYn() != YnTypeEnum.N)) {
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Credit Role 값 없거나 틀릴 때
        if (roleDetailCodeDto.getCreditRoleYn() == null || (roleDetailCodeDto.getCreditRoleYn() != YnTypeEnum.Y && roleDetailCodeDto.getCreditRoleYn() != YnTypeEnum.N)) {
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Split Sheet Role 값 없거나 틀릴 때
        if (roleDetailCodeDto.getSplitSheetRoleYn() == null || (roleDetailCodeDto.getSplitSheetRoleYn() != YnTypeEnum.Y && roleDetailCodeDto.getSplitSheetRoleYn() != YnTypeEnum.N)) {
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if (roleDetailCodeDto.getRole() == null) {
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        try {

            RoleDetailCodeEntity newRoleDetailCode = RoleDetailCodeEntity.builder()
                    .roleCodeEntity(roleCode.get())
                    .pcode(roleCode.get().getCode())
                    .dcode(roleDetailCodeDto.getDcode())
                    .role(roleDetailCodeDto.getRole())
                    .formatType(roleDetailCodeDto.getFormatType())
                    .description(roleDetailCodeDto.getDescription())
                    .useType(roleDetailCodeDto.getUseType())
                    .popularType(YnTypeEnum.N)
                    .matchingRoleYn(roleDetailCodeDto.getMatchingRoleYn())
                    .creditRoleYn(roleDetailCodeDto.getCreditRoleYn())
                    .splitSheetRoleYn(roleDetailCodeDto.getSplitSheetRoleYn())
                    .hit(0L)
                    .build();

            RoleDetailCodeEntity saved = roleDetailCodeRepository.save(newRoleDetailCode);

            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
//            res.setResult(dtoPage);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));

            String menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_ROLES.getCode());
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_CREATE.getCode());

            //netive query
            String usrName = adminActiveLogService.findQryUsrName(saved.getRegUsr());

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(saved.getRegDt())
                    .adminId(saved.getRegUsr())
                    .menuName(menuName)
                    .activeType("C")
                    .contentsId(saved.getRoleDetailCodeSeq())
                    .activeMsg(usrName + " " + activeMsg + " '" + saved.getRole() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e) {
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }

        return res;
    }

}
