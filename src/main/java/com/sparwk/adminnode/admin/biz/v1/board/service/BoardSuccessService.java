package com.sparwk.adminnode.admin.biz.v1.board.service;

import com.sparwk.adminnode.admin.biz.v1.CommonPagingListDto;
import com.sparwk.adminnode.admin.biz.v1.admin.service.AdminActiveLogService;
import com.sparwk.adminnode.admin.biz.v1.board.dto.BoardDelete;
import com.sparwk.adminnode.admin.biz.v1.board.dto.BoardSuccessDto;
import com.sparwk.adminnode.admin.biz.v1.board.dto.BoardSuccessRequest;
import com.sparwk.adminnode.admin.config.common.ActiveCodeConst;
import com.sparwk.adminnode.admin.config.common.GetAccountId;
import com.sparwk.adminnode.admin.config.common.Response;
import com.sparwk.adminnode.admin.config.common.ResultCodeConst;
import com.sparwk.adminnode.admin.jpa.entity.PeriodTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.admin.AdminActiveLogEntity;
import com.sparwk.adminnode.admin.jpa.entity.board.*;
import com.sparwk.adminnode.admin.jpa.repository.board.BoardSuccessRepository;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class BoardSuccessService {

    private final BoardSuccessRepository boardSuccessRepository;
    private final AdminActiveLogService adminActiveLogService;

    private final HttpServletRequest httpServletRequest;
    private final MessageSourceAccessor messageSourceAccessor;

    private final Logger logger = LoggerFactory.getLogger(BoardSuccessService.class);

    public Response<CommonPagingListDto> getList(SuccessCateEnum cate,
                                                 String val,
                                                 YnTypeEnum useType,
                                                 PeriodTypeEnum periodType,
                                                 String sdate,
                                                 String edate,
                                                 SuccessSorterEnum sorter,
                                                 PageRequest pageRequest) {

        Response<CommonPagingListDto> res = new Response<>();

        List<BoardSuccessDto> boardSuccessList =
                boardSuccessRepository.findQryAll(cate, val, useType, periodType, sdate, edate, sorter, pageRequest);

        logger.info("BoardSuccessList - {}", boardSuccessList);

        //조회내용 없을 때
        if (boardSuccessList == null || boardSuccessList.size() < 1) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        CommonPagingListDto list = new CommonPagingListDto();

        //총 카운트
        long totalSize = boardSuccessRepository.countQryAll(cate, val, useType, periodType, sdate, edate);
        long totalPage = totalSize / pageRequest.getPageSize();
        long mod = totalSize % pageRequest.getPageSize();
        if(mod > 0) {
            totalPage += 1;
        }

        list.setList(boardSuccessList);
        list.setSize(pageRequest.getPageSize());
        list.setPage(pageRequest.getPageNumber());
        list.setTotalPage(totalPage);
        list.setTotalSize(totalSize);

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(list);
        return res;
    }

    public Response<CommonPagingListDto> getUseYList() {

        Response<CommonPagingListDto> res = new Response<>();

        List<BoardSuccessDto> boardSuccessList = boardSuccessRepository.findQryUseY();

        logger.info("BoardSuccessList - {}", boardSuccessList);

        //조회내용 없을 때
        if (boardSuccessList == null || boardSuccessList.size() < 1) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        CommonPagingListDto list = new CommonPagingListDto();

        //총 카운트
        long totalSize = boardSuccessRepository.countQryUseY();
        long totalPage = totalSize ;

        list.setList(boardSuccessList);
        list.setSize(1);
        list.setPage(1);
        list.setTotalPage(totalPage);
        list.setTotalSize(totalSize);

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(list);
        return res;
    }

    public Response<Optional<BoardSuccessDto>> getOne(Long id) {

        Response<Optional<BoardSuccessDto>> res = new Response<>();

        //id값이 없을 때
        if (id == null) {
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<BoardSuccessEntity> boardSuccess = boardSuccessRepository.findById(id);

        logger.info("boardSuccess - {}", boardSuccess);

        //조회내용 없을 때
        if (!boardSuccess.isPresent()) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<BoardSuccessDto> dtoPage =

                boardSuccess
                        .map(p -> BoardSuccessDto.builder()
                                .successId(p.getSuccessId())
                                .name(p.getName())
                                .imageUrl(p.getImageUrl())
                                .role(p.getRole())
                                .genre(p.getGenre())
                                .content(p.getContent())
                                .useType(p.getUseType())
                                .regUsr(p.getRegUsr())
                                .regDt(p.getRegDt())
                                .modUsr(p.getModUsr())
                                .modDt(p.getModDt())
                                .build()
                        );

        logger.info("BoardSuccess - {}", dtoPage);

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(dtoPage);
        return res;
    }



    @Transactional
    public Response<Void> saveContent(@NotNull BoardSuccessRequest request, HttpServletRequest req) {

        Response<Void> res = new Response<>();

        //USEYN 값 없거나 틀릴 때
        if (request.getUseType() == null || (request.getUseType() != YnTypeEnum.Y && request.getUseType() != YnTypeEnum.N)) {
            res.setResultCd(ResultCodeConst.NOT_VALID_USEYN.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if (request.getName() == null) {
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }



        try {

             BoardSuccessEntity newBoardSuccess = BoardSuccessEntity.builder()
                    .name(request.getName())
                     .imageUrl(request.getImageUrl())
                    .role(request.getRole())
                    .genre(request.getGenre())
                    .content(request.getContent())
                    .useType(request.getUseType())
                    .build();

            BoardSuccessEntity saved = boardSuccessRepository.save(newBoardSuccess);

            Optional<BoardSuccessEntity> BoardSuccess = boardSuccessRepository.findById(saved.getSuccessId());

            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
//            res.setResult(dtoPage);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));

            String menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_SUCCESS_STORY.getCode());
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_CREATE.getCode());

            //netive query
            String usrName = adminActiveLogService.findQryUsrName(saved.getRegUsr());

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(saved.getRegDt())
                    .adminId(saved.getRegUsr())
                    .menuName(menuName)
                    .activeType("C")
                    .contentsId(saved.getSuccessId())
                    .activeMsg(usrName + " " + activeMsg + " '" + saved.getRole() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e) {
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }

        return res;
    }

    @Transactional
    public Response<Optional<BoardSuccessDto>> updateContent(Long id, @NotNull BoardSuccessRequest request, HttpServletRequest req) {

        Response<Optional<BoardSuccessDto>> res = new Response<>();

        //id값이 없을 때
        if (id == null) {
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<BoardSuccessEntity> boardSuccess = boardSuccessRepository.findById(id);


        logger.info("boardSuccess - {} ", boardSuccess);

        //조회내용 없을 때
        if (!boardSuccess.isPresent()) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //USEYN 값 없거나 틀릴 때
        if (request.getUseType() == null || (request.getUseType() != YnTypeEnum.Y && request.getUseType() != YnTypeEnum.N)) {
            res.setResultCd(ResultCodeConst.NOT_VALID_USEYN.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if (request.getName() == null) {
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        try {
            logger.info("/////////////////////////////// boardSuccess - asdflkajsdflkja ");


            boardSuccess.get().update(request.getName(),
                    request.getRole(),
                    request.getGenre(),
                    request.getContent(),
                    request.getUseType(),
                    request.getImageUrl());

            logger.info("/////////////////////////////// boardSuccess - {} ", boardSuccess.toString());

            Optional<BoardSuccessEntity> boardSuccessUpdate = boardSuccessRepository.findById(boardSuccess.get().getSuccessId());

            Optional<BoardSuccessDto> dtoPage =

                    boardSuccessUpdate
                            .map(p -> BoardSuccessDto.builder()
                                    .successId(p.getSuccessId())
                                    .name(p.getName())
                                    .imageUrl(p.getImageUrl())
                                    .role(p.getRole())
                                    .genre(p.getGenre())
                                    .content(p.getContent())
                                    .useType(p.getUseType())
                                    .regUsr(p.getRegUsr())
                                    .regDt(p.getRegDt())
                                    .modUsr(p.getModUsr())
                                    .modDt(p.getModDt())
                                    .build()
                            );

            logger.info("dtoPage - {} ", dtoPage);

            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            res.setResult(dtoPage);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));

            String menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_SUCCESS_STORY.getCode());
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_MODIFY.getCode());

            //netive query
            String usrName = adminActiveLogService.findQryUsrName(boardSuccessUpdate.get().getModUsr());

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(boardSuccessUpdate.get().getModDt())
                    .adminId(boardSuccessUpdate.get().getModUsr())
                    .menuName(menuName)
                    .activeType("U")
                    .contentsId(boardSuccessUpdate.get().getSuccessId())
                    .activeMsg(usrName + " " + activeMsg + " '" + boardSuccessUpdate.get().getRole() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e) {
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }

        return res;
    }

    @Transactional
    public Response<Void> deleteContent(Long id, HttpServletRequest req) {

        Response<Void> res = new Response<>();

        //id값이 없을 때
        if (id == null) {
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<BoardSuccessEntity> boardSuccess = boardSuccessRepository.findById(id);

        logger.info("boardSuccess - {} ", boardSuccess);

        //조회내용 없을 때
        if (!boardSuccess.isPresent()) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        try {
            boardSuccessRepository.deleteById(id);
            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
//            res.setResult(dtoPage);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));
            Long adminId = getAccountId.ofId(httpServletRequest);

            String menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_SUCCESS_STORY.getCode());
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_DELETE.getCode());

            //netive query
            String usrName = adminActiveLogService.findQryUsrName(adminId);

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(LocalDateTime.now())
                    .adminId(adminId)
                    .menuName(menuName)
                    .activeType("D")
                    .contentsId(boardSuccess.get().getSuccessId())
                    .activeMsg(usrName + " " + activeMsg + " '" + boardSuccess.get().getRole() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e) {
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }
        return res;

    }

    @Transactional
    public Response<Void> deleteList(@NotNull BoardDelete request) {

        Response<Void> res = new Response<>();

        //id값이 없을 때
        if (request.getId() == null) {
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //조회내용 없을 때
        try {

            Long[] kkk = request.getId();

            for(int i=0; i < kkk.length; i++) {

                Optional<BoardSuccessEntity> board = boardSuccessRepository.findById(request.getId()[i]);

                ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
                GetAccountId getAccountId = new GetAccountId();
                logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));
                Long adminId = getAccountId.ofId(httpServletRequest);


                String menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_SUCCESS_STORY.getCode());
                String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_DELETE.getCode());

                //netive query
                String usrName = adminActiveLogService.findQryUsrName(adminId);

                AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                        .activeTime(LocalDateTime.now())
                        .adminId(adminId)
                        .menuName(menuName)
                        .activeType("D")
                        .contentsId(board.get().getSuccessId())
                        .activeMsg(usrName + " " + activeMsg + " '" + board.get().getRole() + "' in " + menuName)
                        .build();

                adminActiveLogService.saveContent(newLog);
            }

            boardSuccessRepository.deleteQryList(request.getId());
            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
//            res.setResult(dtoPage);
        } catch (Exception e) {
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }
        return res;

    }

}
