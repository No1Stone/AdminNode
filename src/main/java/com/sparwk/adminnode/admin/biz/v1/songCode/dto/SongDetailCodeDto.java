package com.sparwk.adminnode.admin.biz.v1.songCode.dto;

import com.sparwk.adminnode.admin.jpa.entity.FormatTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Setter
@Getter
@NoArgsConstructor
public class SongDetailCodeDto {

    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long songDetailCodeSeq;
    @Schema(description = "부모코드 3자리", nullable = false, example = "SGG")
    private String pcode;
    @Schema(description = "부모코드 값", nullable = true, example = "Genre")
    private String pcodeVal;
    @Schema(description = "코드 9자리", nullable = false, example = "SGG000001")
    private String dcode;
    @Schema(description = "코드 값", nullable = false, example = "Hip-Hop")
    private String val;
    @Schema(description = "포맷 여부 DDEX / SPARWK", nullable = false, defaultValue = "DDEX", allowableValues = {"DDEX", "SPARWK"})
    private FormatTypeEnum formatType;
    @Schema(description = "코드 사용여부 Y/N", nullable = false, defaultValue = "Y", allowableValues = {"Y", "N"})
    private YnTypeEnum useType;
    @Schema(description = "popular 사용 Y/N", nullable = false, defaultValue = "Y", allowableValues = {"Y", "N"})
    private YnTypeEnum popularType;
    @Schema(description = "description 설명", nullable = true, example = "설명")
    private String description;
    @Schema(description = "코드 순번 숫자",  nullable = false, example = "1")
    private int sortIndex;
    @Schema(description = "hit 인기 순",  nullable = false, example = "1")
    private int hit;
    @Schema(description = "등록일", pattern = "yyMMdd hh:mm:ss", example = "2021-12-08T05:44:26.627991")
    private LocalDateTime regDt;
    @Schema(description = "등록자", nullable = false, example = "10000001")
    private Long regUsr;
    @Schema(description = "등록자 이름", nullable = false, example = "Yoon Jung Sun")
    private String regUsrName;
    @Schema(description = "수정일", pattern = "yyMMdd hh:mm:ss", example = "2021-12-08T05:44:26.627991")
    private LocalDateTime modDt;
    @Schema(description = "수정자", nullable = false, example = "10000001")
    private Long modUsr;
    @Schema(description = "수정자 이름", nullable = false, example = "Yoon Jung Sun")
    private String modUsrName;

    @Builder
    public SongDetailCodeDto(Long songDetailCodeSeq,
                             String pcode,
                             String pcodeVal,
                             String dcode,
                             String val,
                             FormatTypeEnum formatType,
                             YnTypeEnum useType,
                             YnTypeEnum popularType,
                             String description,
                             int sortIndex,
                             int hit,
                             LocalDateTime regDt,
                             Long regUsr,
                             String regUsrName,
                             LocalDateTime modDt,
                             Long modUsr,
                             String modUsrName
    ) {
        this.songDetailCodeSeq = songDetailCodeSeq;
        this.pcode = pcode;
        this.pcodeVal = pcodeVal;
        this.dcode = dcode;
        this.val = val;
        this.formatType = formatType;
        this.useType = useType;
        this.popularType = popularType;
        this.description = description;
        this.sortIndex = sortIndex;
        this.hit = hit;
        this.regDt = regDt;
        this.regUsr = regUsr;
        this.regUsrName = regUsrName;
        this.modDt = modDt;
        this.modUsr = modUsr;
        this.modUsrName = modUsrName;
    }
}