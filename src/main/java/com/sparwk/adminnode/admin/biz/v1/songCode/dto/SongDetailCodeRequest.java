package com.sparwk.adminnode.admin.biz.v1.songCode.dto;

import lombok.*;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class SongDetailCodeRequest {

//    @Schema(description = "부모코드 3자리", nullable = false, example = "SGG")
//    @NotBlank(message = "Please enter a code.")
//    private String pcode;
//    @Schema(description = "코드 9자리", nullable = false, example = "SGG000001")
//    @NotBlank(message = "Please enter a code.")
//    private String dcode;
//    @Schema(description = "코드 값", nullable = false, example = "Hip-Hop")
//    @NotBlank(message = "Please enter a value.")
//    private String val;
//    @Schema(description = "포맷 여부 DDEX / SPARWK", defaultValue = "DDEX", allowableValues = {"DDEX", "SPARWK"})
//    @NotNull(message = "Please check use.")
//    private Format format;
//    @Schema(description = "코드 사용여부 Y/N", defaultValue = "Y", allowableValues = {"Y", "N"})
//    @NotNull(message = "Please check use.")
//    private YnEnumType useType;
//    @Schema(description = "popular 사용 Y/N", defaultValue = "Y", allowableValues = {"Y", "N"})
//    @NotNull(message = "Please check popular")
//    private YnEnumType popularType;
//    @Schema(description = "description 설명", nullable = true, example = "설명")
//    private String description;
//    @Schema(description = "코드 순번 숫자",  nullable = false, example = "1")
//    private int sortIndex;

//    @Builder
//    public SongDetailCodeRequest(String pcode,
//                                 String dcode,
//                                 String val,
//                                 Format format,
//                                 UseType useType,
//                                 YnEnumType popularType,
//                                 String description,
//                                 int sortIndex
//
//    ) {
//        this.pcode = pcode;
//        this.dcode = dcode;
//        this.val = val;
//        this.format = format;
//        this.useType = useType;
//        this.popularType = popularType;
//        this.description = description;
//        this.sortIndex = sortIndex;
//
//    }

    private SongDetailCodeDto songDetailCodeDto;
}
