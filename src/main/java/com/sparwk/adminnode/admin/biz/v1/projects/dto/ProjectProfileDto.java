package com.sparwk.adminnode.admin.biz.v1.projects.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class ProjectProfileDto {

    @Schema(description = "project id 번호", nullable = false, example = "1")
    private Long projId;
    @Schema(description = "profile id", nullable = false, example = "1")
    private Long profileId;
    @Schema(description = "profile name", nullable = false, example = "Mr.yoon")
    private String profileFullName;



    @Builder
    public ProjectProfileDto(Long projId,
                             Long profileId,
                             String profileFullName

    ) {
        this.projId = projId;
        this.profileId = profileId;
        this.profileFullName = profileFullName;
    }
}
