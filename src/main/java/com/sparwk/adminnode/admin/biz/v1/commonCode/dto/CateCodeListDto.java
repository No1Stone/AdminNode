package com.sparwk.adminnode.admin.biz.v1.commonCode.dto;

import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Setter
@Getter
@NoArgsConstructor
public class CateCodeListDto {

    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long commonDetailCodeSeq;
    @Schema(description = "부모코드 3자리", nullable = false, example = "ENC")
    private String pcode;
    @Schema(description = "부모코드 값", nullable = true, example = "Encoding")
    private String pcodeVal;
    @Schema(description = "코드 9자리", nullable = false, example = "ENC000001")
    private String dcode;
    @Schema(description = "코드 값", nullable = false, example = "UTF-8")
    private String val;
    @Schema(description = "코드 사용여부 Y/N", nullable = false, defaultValue = "Y", allowableValues = {"Y", "N"})
    private YnTypeEnum useType;
    @Schema(description = "코드 순번 숫자", nullable = false, defaultValue = "1", example = "1")
    private int sortIndex;
    @Schema(description = "등록일", pattern = "yyMMdd hh:mm:ss", example = "2021-12-08T05:44:26.627991")
    private LocalDateTime regDt;
    @Schema(description = "등록자", nullable = false, example = "10000001")
    private Long regUsr;
    @Schema(description = "수정일", pattern = "yyMMdd hh:mm:ss", example = "2021-12-08T05:44:26.627991")
    private LocalDateTime modDt;
    @Schema(description = "수정자", nullable = false, example = "10000001")
    private Long modUsr;

    @Builder
    public CateCodeListDto(Long commonDetailCodeSeq,
                           String pcode,
                           String pcodeVal,
                           String dcode,
                           String val,
                           YnTypeEnum useType,
                           int sortIndex,
                           LocalDateTime regDt,
                           Long regUsr,
                           LocalDateTime modDt,
                           Long modUsr
    ) {
        this.commonDetailCodeSeq = commonDetailCodeSeq;
        this.pcode = pcode;
        this.pcodeVal = pcodeVal;
        this.dcode = dcode;
        this.val = val;
        this.useType = useType;
        this.sortIndex = sortIndex;
        this.regDt = regDt;
        this.regUsr = regUsr;
        this.modDt = modDt;
        this.modUsr = modUsr;
    }
}