package com.sparwk.adminnode.admin.biz.v1.board.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

@Setter
@Getter
@ToString
@NoArgsConstructor
public class BoardQnaEmailDto {

    @Schema(description = "제목", nullable = false, example = "제목")
    private String subject;
    @Schema(description = "내용", nullable = true, example = "내용")
    private String contents;
    @Schema(description = "상대방 Email 배열로 표시", nullable = false, example = "[yoon@gmail.com]")
    private String[] receiver;


    @Builder
    public BoardQnaEmailDto(
                   String subject,
                   String contents,
                   String[] receiver
    ) {
        this.subject = subject;
        this.contents = contents;
        this.receiver = receiver;
    }
}
