package com.sparwk.adminnode.admin.biz.v1.song.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Setter
@Getter
@NoArgsConstructor
public class SongTimelineDto {

    @Schema(description = "timelineSeq id 번호", nullable = false, example = "1")
    private Long timelineSeq;
    @Schema(description = "id 번호", nullable = false, example = "1")
    private Long songId;
    @Schema(description = "id 번호", nullable = false, example = "1")
    private Long componentUseId;
    @Schema(description = "", nullable = false, example = "")
    private String fullName;
    @Schema(description = "", nullable = false, example = "")
    private String componetCd;
    @Schema(description = "", nullable = false, example = "")
    private String componetCdName;
    @Schema(description = "메세지 내용", nullable = true, example = "song has become available.")
    private String resultMsg;
    @Schema(description = "등록일", pattern = "yyMMdd hh:mm:ss", example = "2021-12-08T05:44:26.627991")
    private LocalDateTime regDt;



    @Builder
    public SongTimelineDto(Long timelineSeq,
                           Long songId,
                           Long componentUseId,
                           String fullName,
                           String componetCd,
                           String componetCdName,
                           String resultMsg,
                           LocalDateTime regDt
    ) {
        this.timelineSeq = timelineSeq;
        this.songId = songId;
        this.componentUseId = componentUseId;
        this.fullName = fullName;
        this.componetCd = componetCd;
        this.componetCdName = componetCdName;
        this.resultMsg = resultMsg;
        this.regDt = regDt;
    }
}
