package com.sparwk.adminnode.admin.biz.v1.test.controller;

import com.google.gson.Gson;
import com.sparwk.adminnode.admin.biz.v1.board.dto.BoardQnaEmailDto;
import com.sparwk.adminnode.admin.biz.v1.test.dto.AdminEmailSendRequest;
import com.sparwk.adminnode.admin.biz.v1.test.dto.TestDto;
import com.sparwk.adminnode.admin.biz.v1.test.dto.TestEmailDto;
import com.sparwk.adminnode.admin.biz.v1.test.service.TestService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping(value = "/V1/setting/identifire/")
@RequiredArgsConstructor
@CrossOrigin("*")
@ApiIgnore
public class TestController {

    private final Logger logger = LoggerFactory.getLogger(TestController.class);

    @Autowired
    private TestService testService;

    @Value("${file.path}")
    private String fileRealPath;

    //쓰기처리
    @PostMapping("test/new")
    @Operation(
            summary = "Test List", description = "Test List 입니다.",
            responses = {
                    @ApiResponse(
                            responseCode = "200", description = "TestList"
                            , content = @Content(schema = @Schema(implementation = TestDto.class)))
            })
    public ResponseEntity<Optional<TestDto>> saveContent(
                                                        @RequestPart(value="key", required=false) @Valid TestDto testDto,
                                                        @RequestPart(value="file", required=true) MultipartFile file,
                                                        HttpServletRequest req) {
        return ResponseEntity.ok(testService.saveContent(testDto, req));
    }

    // @ResponseBody 생략가능. class 에 @RestController
    @PostMapping(value="test/uploadFile")
    public ResponseEntity<String> uploadFile(
            @RequestPart(value="key", required=false) @Valid TestDto testDto,
            @RequestParam("file") MultipartFile file,
            HttpServletRequest req
    ) throws IllegalStateException, IOException {

        if( !file.isEmpty() ) {
            logger.info("file org name = {}", file.getOriginalFilename());
            logger.info("file content type = {}", file.getContentType());
            logger.info("file content type = {}", file.getSize());
            logger.info("file content type = {}", file.getName());
            //file.transferTo(new File(file.getOriginalFilename()));
        }

        System.out.println(fileRealPath);

        UUID uuid = UUID.randomUUID();
        String uuidFilename = uuid+"_"+file.getOriginalFilename();


        //이미지 저장 위치: 외부에 잡기/ 나중에 이 경로랑 파일명만 db에 저장하면 된다.
        //nio 로 임포트
        Path filePath = Paths.get(fileRealPath+uuidFilename);

        logger.info("{}", filePath.toString());

        try {
            Files.write(filePath,file.getBytes());
            //얘가 스레드 새로 만들어서 만약 10메가 넘어가면 여기서 파일 넣는동안 메인스레드가 ok 뿌려서 엑박뜸->
            //사진 용량 제한시키기 아니면 메인스레드 기다리게하기(비동기처리)

            // FileReader 생성
            Reader reader = new FileReader(filePath.toString());

            Gson gson = new Gson();
            TestDto testDto1 = gson.fromJson(reader, TestDto.class);

            testService.saveContent(testDto1, req);

            logger.info("{}", testDto1.getCkey());
            logger.info("{}", testDto1.getCval());


        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return new ResponseEntity<>("", HttpStatus.OK);
    }

    //서버간 통신 Get방식 1 -- 보내기
    @GetMapping("/hello")
    public TestEmailDto getHello(TestEmailDto testEmailDto){
        return testService.testSend(testEmailDto);
    }

    //서버간 통신 Get방식 2 -- 보낸거 보여주기
    @GetMapping("/hello2")
    public TestEmailDto getHello2(TestEmailDto testEmailDto){
        logger.info("testEmailDto : {}" , testEmailDto);
        return testEmailDto;
    }

    //서버간 통신 Post방식 1 -- 보내기
    @GetMapping("/hello-post")
    public TestEmailDto getPostHello(){
        return testService.testPostSend();
    }

    //서버간 통신 Post방식 3 -- 보내기
    @PostMapping("/hello-post3")
    public TestEmailDto getPostHello3(@RequestBody TestEmailDto testEmailDto){
        return testService.testPostSend3(testEmailDto);
    }

    //서버간 통신 Post방식 2 -- 받기
    @PostMapping("/hello-post2")
    public TestEmailDto getPostHello2(
            @RequestBody TestEmailDto testEmailDto
    ){
        logger.info("testEmailDto : {}" , testEmailDto);
        return testEmailDto;
    }

    //서버간 통신 Post방식 2 -- 받기
    @PostMapping("/email-post")
    public BoardQnaEmailDto getPostEmailTest(
            @RequestBody BoardQnaEmailDto boardQnaEmailDto
    ){
        logger.info("boardQnaEmailDto : {}" , boardQnaEmailDto);
        return testService.testPostEmailTest(boardQnaEmailDto);
    }

    //서버간 통신 Post방식 3 -- 보내기
    @PostMapping("/hello-post4")
    public void getPostHello4(@RequestBody AdminEmailSendRequest adminEmailSendRequest){
        testService.EmailSend2(adminEmailSendRequest);
    }

    //서버간 통신 Post방식 3 -- 보내기
//    @PostMapping("/hello-post5")
//    public  AdminEmailSendRequest getPostHello5(@RequestBody AdminEmailSendRequest adminEmailSendRequest){
//        return testService.EmailSend3(adminEmailSendRequest);
//    }

    //서버간 통신 Post방식 2 -- 받기
//    @PostMapping("/hello-post6")
//    public AdminEmailSendRequest getPostHello2(
//            @RequestBody AdminEmailSendRequest testEmailDto
//    ){
//        logger.info("AdminEmailSendRequest : {}" , testEmailDto);
//        return testEmailDto;
//    }
}
