package com.sparwk.adminnode.admin.biz.v1.projects.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class ProjectSongDto {

    @Schema(description = "project id 번호", nullable = false, example = "1")
    private Long projId;
    @Schema(description = "SONG ID", nullable = false, example = "1")
    private Long songId;
    @Schema(name = "SONG_TITLE", nullable = false, example = "Mr.Yoon")
    private String songTitle;
    @Schema(name = "SONG_GENRE", nullable = true, example = "K-Pop")
    private String songGenre;
    @Schema(name = "SONG_SUB_GENRE", nullable = true, example = "Dance")
    private String songSubGenre;
    @Schema(name = "Language", nullable = true, example = "sparwk@sparwk.com")
    private String songLanguage;
    @Schema(name = "SONG STATUS", nullable = false, example = "")
    private String songStatusCd;
    @Schema(name = "SONG STATUS NAME", nullable = false, example = "Cut")
    private String songStatusName;
    @Schema(description = "PITCHED SONG COUNT", nullable = false, example = "1")
    private int songPitchedCnt;



    @Builder
    public ProjectSongDto(Long projId,
                          Long songId,
                          String songTitle,
                          String songGenre,
                          String songSubGenre,
                          String songLanguage,
                          String songStatusCd,
                          String songStatusName,
                          int songPitchedCnt
    ) {
        this.projId = projId;
        this.songId = songId;
        this.songTitle = songTitle;
        this.songGenre = songGenre;
        this.songSubGenre = songSubGenre;
        this.songLanguage = songLanguage;
        this.songStatusCd = songStatusCd;
        this.songStatusName = songStatusName;
        this.songPitchedCnt = songPitchedCnt;
    }
}
