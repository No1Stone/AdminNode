package com.sparwk.adminnode.admin.biz.v1.test.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

@Setter
@Getter
@ToString
@NoArgsConstructor
public class TestEmailDto {

    @Schema(description = "상대방 Email")
    private String[] userEmail;
    @Schema(description = "제목")
    private String emailTitle;
    @Schema(description = "내용")
    private String emailBody;

    @Builder
    public TestEmailDto(
                   String[] userEmail,
                   String emailTitle,
                   String emailBody
    ) {
        this.userEmail = userEmail;
        this.emailTitle = emailTitle;
        this.emailBody = emailBody;
    }
}
