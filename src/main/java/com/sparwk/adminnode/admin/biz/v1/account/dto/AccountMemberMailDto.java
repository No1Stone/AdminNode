package com.sparwk.adminnode.admin.biz.v1.account.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class AccountMemberMailDto {

    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long accntId;
    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long profileId;
    @Schema(description = "ACCNT FULL NAME", nullable = false, example = "Mr.Yoon")
    private String profileFullName;
    @Schema(description = "CURRENT_COUNTRY_CD_NAME", nullable = false, example = "USA")
    private String currentCityCountryCdName;
    @Schema(description = "CURRENT CITY", nullable = false, example = "LA")
    private String currentCityNm;
    @Schema(description = "PROFILE IMG URL", nullable = true, example = "http://...")
    private String profileImgUrl;


    @Builder
    public AccountMemberMailDto(Long accntId,
                                Long profileId,
                                String profileFullName,
                                String currentCityCountryCdName,
                                String currentCityNm,
                                String profileImgUrl
    ) {
        this.accntId=accntId;
        this.profileId=profileId;
        this.profileFullName=profileFullName;
        this.currentCityCountryCdName=currentCityCountryCdName;
        this.currentCityNm=currentCityNm;
        this.profileImgUrl=profileImgUrl;
    }
}
