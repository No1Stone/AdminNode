package com.sparwk.adminnode.admin.biz.v1.account.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Setter
@Getter
@NoArgsConstructor
public class AccountSnsTokenDto {

    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long accntId;
    @Schema(description = "sns_type_cd", nullable = false, example = "face")
    private String snsTypeCd;
    @Schema(description = "sns_token", nullable = true, example = "")
    private String snsToken;

    @Builder
    public AccountSnsTokenDto(Long accntId,
                              String snsTypeCd,
                              String snsToken
    ) {
        this.accntId = accntId;
        this.snsTypeCd = snsTypeCd;
        this.snsToken = snsToken;
    }
}
