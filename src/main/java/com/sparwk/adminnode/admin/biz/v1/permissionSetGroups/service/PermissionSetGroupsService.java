package com.sparwk.adminnode.admin.biz.v1.permissionSetGroups.service;

import com.sparwk.adminnode.admin.biz.v1.permissionSetGroups.dto.PermissionSetGroupsDto;
import com.sparwk.adminnode.admin.biz.v1.permissionSetGroups.dto.PermissionSetGroupsRequest;
import com.sparwk.adminnode.admin.config.common.Response;
import com.sparwk.adminnode.admin.config.common.ResultCodeConst;
import com.sparwk.adminnode.admin.jpa.entity.PeriodTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.permissionSetGroups.PermissionSetGroupsCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.permissionSetGroups.PermissionSetGroupsEntity;
import com.sparwk.adminnode.admin.jpa.entity.permissionSetGroups.PermissionSetGroupsSorterEnum;
import com.sparwk.adminnode.admin.jpa.repository.permissionSetGroups.PermissionSetGroupsRepository;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class PermissionSetGroupsService {

    private final PermissionSetGroupsRepository permissionSetGroupsRepository;

    private final MessageSourceAccessor messageSourceAccessor;

    private final Logger logger = LoggerFactory.getLogger(PermissionSetGroupsService.class);
    

    public Response<List<PermissionSetGroupsDto>> getList(PermissionSetGroupsCateEnum cate,
                                                          String cdLabelVal,
                                                          YnTypeEnum useType,
                                                          PeriodTypeEnum periodType,
                                                          String sdate,
                                                          String edate,
                                                          PermissionSetGroupsSorterEnum sorter,
                                                          PageRequest pageRequest) {

        Response<List<PermissionSetGroupsDto>> res = new Response<>();

        List<PermissionSetGroupsDto> permissionSetGroups =
                permissionSetGroupsRepository.findQryAll(cate, cdLabelVal, useType, periodType, sdate, edate, sorter, pageRequest);

        logger.info("permissionSetGroups - {}", permissionSetGroups);

        //조회내용 없을 때
        if(permissionSetGroups == null || permissionSetGroups.size() < 1){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(permissionSetGroups);
        return res;
    }

    public Response<List<PermissionSetGroupsDto>> getUseYList(PermissionSetGroupsCateEnum cate, String  cdVal) {

        Response<List<PermissionSetGroupsDto>> res = new Response<>();
        
        List<PermissionSetGroupsDto> permissionSetGroups = permissionSetGroupsRepository.findQryUseY(cate, cdVal);

        logger.info("permissionSetGroups - {}", permissionSetGroups);

        //조회내용 없을 때
        if(permissionSetGroups == null || permissionSetGroups.size() < 1){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(permissionSetGroups);
        return res;
    }

    public Response<Optional<PermissionSetGroupsDto>> getOne(Long id) {

        Response<Optional<PermissionSetGroupsDto>> res = new Response<>();

        //id값이 없을 때
        if(id == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<PermissionSetGroupsEntity> permissionSetGroups = permissionSetGroupsRepository.findById(id);

        logger.info("permissionSetGroups - {}", permissionSetGroups);

        //조회내용 없을 때
        if(!permissionSetGroups.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<PermissionSetGroupsDto> dtoPage =

                permissionSetGroups
                        .map(p -> PermissionSetGroupsDto.builder()
                                .adminPermissionGroupId(p.getAdminPermissionGroupId())
                                .labelVal(p.getLabelVal())
                                .cdOrd(p.getCdOrd())
                                .cdDesc(p.getCdDesc())
                                .useType(p.getUseType())
                                .regUsr(p.getRegUsr())
                                .regDt(p.getRegDt())
                                .modUsr(p.getModUsr())
                                .modDt(p.getModDt())
                                .build()
                        );

        logger.info("CommonDetailCode - {}", dtoPage);

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(dtoPage);
        return res;
    }

    @Transactional
    public Response<Void> saveContent(PermissionSetGroupsRequest request, HttpServletRequest req) {

        Response<Void> res = new Response<>();

        //USEYN 값 없거나 틀릴 때
        if(request.getPermissionSetGroupsDto().getUseType() == null || (request.getPermissionSetGroupsDto().getUseType() != YnTypeEnum.Y && request.getPermissionSetGroupsDto().getUseType() != YnTypeEnum.N)){
            res.setResultCd(ResultCodeConst.NOT_VALID_USEYN.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if(request.getPermissionSetGroupsDto().getLabelVal() == null){
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        try {
            PermissionSetGroupsEntity newPermissionSetGroups = PermissionSetGroupsEntity.builder()
                    .labelVal(request.getPermissionSetGroupsDto().getLabelVal())
                    .cdDesc(request.getPermissionSetGroupsDto().getCdDesc())
                    .useType(request.getPermissionSetGroupsDto().getUseType())
                    .cdOrd(1)
                    .build();
            PermissionSetGroupsEntity saved = permissionSetGroupsRepository.save(newPermissionSetGroups);

//            Optional<PermissionSetGroupsEntity> permissionSetGroups = permissionSetGroupsRepository.findById(saved.getAdminPermissionGroupId());
//
//            Optional<PermissionSetGroupsDto> dtoPage =
//
//                    permissionSetGroups
//                            .map(p -> PermissionSetGroupsDto.builder()
//                                    .adminPermissionGroupId(p.getAdminPermissionGroupId())
//                                    .labelVal(p.getLabelVal())
//                                    .cdOrd(p.getCdOrd())
//                                    .cdDesc(p.getCdDesc())
//                                    .useType(p.getUseType())
//                                    .regUsr(p.getRegUsr())
//                                    .regDt(p.getRegDt())
//                                    .modUsr(p.getModUsr())
//                                    .modDt(p.getModDt())
//                                    .build()
//                            );
//
//            System.out.println(">>>>>>>>>>>>>>>>>" + dtoPage);
//
//        return dtoPage;

            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
//            res.setResult(dtoPage);

        } catch (Exception e){
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }

        return res;
    }

    @Transactional
    public Response<Void> updateYn(Long id, HttpServletRequest req) {

        Response<Void> res = new Response<>();

        //id값이 없을 때
        if(id == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<PermissionSetGroupsEntity> permissionSetGroups = permissionSetGroupsRepository.findById(id);

        logger.info("permissionSetGroups - {}", permissionSetGroups);

        //조회내용 없을 때
        if(!permissionSetGroups.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        YnTypeEnum useType = permissionSetGroups.get().getUseType();
        YnTypeEnum newUseType = (useType== YnTypeEnum.Y) ? YnTypeEnum.N : YnTypeEnum.Y;

        logger.info("newUseType - {} " , newUseType);

        try {
            permissionSetGroups.get().updateYn(newUseType);
            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            //res.setResult(songDetailCode);
        } catch (Exception e) {
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }
        return res;
    }

    @Transactional
    public Response<Optional<PermissionSetGroupsDto>> updateContent(Long id, PermissionSetGroupsRequest request, HttpServletRequest req) {

        Response<Optional<PermissionSetGroupsDto>> res = new Response<>();

        //id값이 없을 때
        if(id == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<PermissionSetGroupsEntity> permissionSetGroups = permissionSetGroupsRepository.findById(id);

        logger.info("permissionSetGroups - {}", permissionSetGroups);

        //조회내용 없을 때
        if(!permissionSetGroups.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //USEYN 값 없거나 틀릴 때
        if(request.getPermissionSetGroupsDto().getUseType() == null || (request.getPermissionSetGroupsDto().getUseType() != YnTypeEnum.Y && request.getPermissionSetGroupsDto().getUseType() != YnTypeEnum.N)){
            res.setResultCd(ResultCodeConst.NOT_VALID_USEYN.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if(request.getPermissionSetGroupsDto().getLabelVal() == null){
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }


        try {
            permissionSetGroups.get().update(request.getPermissionSetGroupsDto().getLabelVal(), request.getPermissionSetGroupsDto().getCdOrd(), request.getPermissionSetGroupsDto().getCdDesc(), request.getPermissionSetGroupsDto().getUseType());

            Optional<PermissionSetGroupsEntity> permissionSetGroupsUpdate = permissionSetGroupsRepository.findById(permissionSetGroups.get().getAdminPermissionGroupId());

            Optional<PermissionSetGroupsDto> dtoPage =

                    permissionSetGroupsUpdate
                            .map(p -> PermissionSetGroupsDto.builder()
                                    .adminPermissionGroupId(p.getAdminPermissionGroupId())
                                    .labelVal(p.getLabelVal())
                                    .cdOrd(p.getCdOrd())
                                    .cdDesc(p.getCdDesc())
                                    .useType(p.getUseType())
                                    .regUsr(p.getRegUsr())
                                    .regDt(p.getRegDt())
                                    .modUsr(p.getModUsr())
                                    .modDt(p.getModDt())
                                    .build()
                            );

            logger.info("dtoPage - {} ", dtoPage);

            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            res.setResult(dtoPage);

        } catch (Exception e){
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }

        return res;
    }

    @Transactional
    public Response<Void> deleteContent(Long id, HttpServletRequest req) {

        Response<Void> res = new Response<>();

        //id값이 없을 때
        if(id == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<PermissionSetGroupsEntity> permissionSetGroups = permissionSetGroupsRepository.findById(id);

        logger.info("permissionSetGroups - {} ", permissionSetGroups);

        //조회내용 없을 때
        if(!permissionSetGroups.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        try {
            permissionSetGroupsRepository.delete(permissionSetGroups.get());
            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
//            res.setResult(dtoPage);
        } catch (Exception e) {
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }
        return res;

    }

}
