package com.sparwk.adminnode.admin.biz.v1.email.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Setter
@Getter
@NoArgsConstructor
public class EmailListDto {

    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long emailSeq;
    @Schema(description = "Email 제목", nullable = false, example = "메일 발송 제목입니다.")
    private String subject;
    @Schema(description = "첨부파일 개수", nullable = false, example = "0")
    private int maxAttachCnt;
    @Schema(description = "발송인원대상 수", nullable = false, example = "100")
    private int memberCnt;
    @Schema(description = "발송일시", pattern = "yyMMdd hh:mm:ss", example = "2021-12-08T05:44:26.627991")
    private LocalDateTime lastSendDt;
    @Schema(description = "등록일", pattern = "yyMMdd hh:mm:ss", example = "2021-12-08T05:44:26.627991")
    private LocalDateTime regDt;
    @Schema(description = "등록자", nullable = false, example = "10000001")
    private Long regUsr;
    @Schema(description = "등록자 이름", nullable = false, example = "Yoon Jung Sun")
    private String regUsrName;


    @Builder
    public EmailListDto(Long emailSeq,
                        String subject,
                        int maxAttachCnt,
                        int memberCnt,
                        LocalDateTime lastSendDt,
                        LocalDateTime regDt,
                        Long regUsr,
                        String regUsrName
    ) {
        this.emailSeq = emailSeq;
        this.subject = subject;
        this.maxAttachCnt = maxAttachCnt;
        this.memberCnt = memberCnt;
        this.lastSendDt = lastSendDt;
        this.regDt = regDt;
        this.regUsr = regUsr;
        this.regUsrName = regUsrName;
    }
}
