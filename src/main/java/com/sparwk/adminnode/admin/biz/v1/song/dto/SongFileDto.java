package com.sparwk.adminnode.admin.biz.v1.song.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class SongFileDto {

    @Schema(description = "id 번호", nullable = false, example = "1")
    private Long songId;
    @Schema(description = "id 번호", nullable = false, example = "1")
    private Long songFileSeq;
    @Schema(description = "", nullable = false, example = "")
    private String songFileName;
    @Schema(description = "", nullable = false, example = "")
    private String songFilePath;
    @Schema(description = "", nullable = false, example = "")
    private String soundType;
    @Schema(description = "", nullable = false, example = "")
    private String bitDepth;
    @Schema(description = "", nullable = false, example = "")
    private String samplingRate;
    @Schema(description = "", nullable = false, example = "")
    private int songFileSize;
    @Schema(description = "파일에 대한 코멘트", nullable = true, example = "코멘트")
    private String duration;
    @Schema(description = "파일에 대한 코멘트", nullable = true, example = "코멘트")
    private String songFileComt;
    @Schema(description = "bpm", nullable = false, example = "144")
    private int bpm;


    @Builder
    public SongFileDto(Long songId,
                       Long songFileSeq,
                       String songFileName,
                       String songFilePath,
                       String soundType,
                       String bitDepth,
                       String samplingRate,
                       int songFileSize,
                       String duration,
                       String songFileComt,
                       int bpm
    ) {
        this.songId = songId;
        this.songFileSeq = songFileSeq;
        this.songFileName = songFileName;
        this.songFilePath = songFilePath;
        this.soundType = soundType;
        this.bitDepth = bitDepth;
        this.samplingRate = samplingRate;
        this.songFileSize = songFileSize;
        this.songFileComt = songFileComt;
        this.duration = duration;
        this.bpm = bpm;
    }
}
