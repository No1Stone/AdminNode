
package com.sparwk.adminnode.admin.biz.v1.test.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

@Setter
@Getter
@ToString
@NoArgsConstructor
public class TestExcelDto {

    @Schema(description = "코드 9자리")
    private String code;
    @Schema(description = "부모코드 3자리")
    private String ckey;
    @Schema(description = "부모코드 값")
    private String cval;
    @Schema(description = "description")
    private String cdesc;

    @Builder
    public TestExcelDto(
                   String code,
                   String ckey,
                   String cval,
                   String cdesc
    ) {
        this.code = code;
        this.ckey = ckey;
        this.cval = cval;
        this.cdesc = cdesc;
    }
}
