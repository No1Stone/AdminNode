package com.sparwk.adminnode.admin.biz.v1.board.dto;

import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

@Setter
@Getter
@NoArgsConstructor
public class BoardPolicyDto {

    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long policyId;
    @Schema(description = "카테고리 코드 9자리", nullable = false, example = "TAP000001")
    private String cateCd;
    @Schema(description = "카테고리 코드 9자리", nullable = false, example = "TERM")
    private String cateCdName;
    @Schema(description = "제목", nullable = false, example = "Terms and Policies 제목입니다.")
    private String title;
    @Schema(description = "content 설명", nullable = true, example = "Terms and Policies 내용입니다.")
    private String content;
    @Schema(description = "노출여부 Y/N", nullable = false, defaultValue = "Y", allowableValues = {"Y", "N"})
    private YnTypeEnum useType;
    @Schema(description = "hit 인기 순", nullable = false, example = "1")
    private Long hit;
    @Schema(description = "등록일", pattern = "yyMMdd hh:mm:ss", example = "2021-12-08T05:44:26.627991")
    private LocalDateTime regDt;
    @Schema(description = "등록자", nullable = false, example = "10000001")
    private Long regUsr;
    @Schema(description = "등록자", nullable = false, example = "Sparwk")
    private String regUsrName;
    @Schema(description = "수정일", pattern = "yyMMdd hh:mm:ss", example = "2021-12-08T05:44:26.627991")
    private LocalDateTime modDt;
    @Schema(description = "수정자", nullable = false, example = "10000001")
    private Long modUsr;
    @Schema(description = "수정자", nullable = false, example = "Sparwk")
    private String modUsrName;


    @Builder
    public BoardPolicyDto(Long policyId,
                          String cateCd,
                          String cateCdName,
                          String title,
                          String content,
                          YnTypeEnum useType,
                          Long hit,
                          LocalDateTime regDt,
                          Long regUsr,
                          String regUsrName,
                          LocalDateTime modDt,
                          Long modUsr,
                          String modUsrName
    ) {
        this.policyId = policyId;
        this.cateCd = cateCd;
        this.cateCdName = cateCdName;
        this.title = title;
        this.content = content;
        this.useType = useType;
        this.hit = hit;
        this.regDt = regDt;
        this.regUsr = regUsr;
        this.regUsrName = regUsrName;
        this.modDt = modDt;
        this.modUsr = modUsr;
        this.modUsrName = modUsrName;
    }
}