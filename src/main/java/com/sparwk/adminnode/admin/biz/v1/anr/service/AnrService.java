package com.sparwk.adminnode.admin.biz.v1.anr.service;

import com.sparwk.adminnode.admin.biz.v1.CommonPagingListDto;
import com.sparwk.adminnode.admin.biz.v1.admin.service.AdminActiveLogService;
import com.sparwk.adminnode.admin.biz.v1.anr.dto.AnrDto;
import com.sparwk.adminnode.admin.biz.v1.anr.dto.AnrExcelDto;
import com.sparwk.adminnode.admin.biz.v1.anr.dto.AnrRequest;
import com.sparwk.adminnode.admin.config.common.ActiveCodeConst;
import com.sparwk.adminnode.admin.config.common.GetAccountId;
import com.sparwk.adminnode.admin.config.common.Response;
import com.sparwk.adminnode.admin.config.common.ResultCodeConst;
import com.sparwk.adminnode.admin.config.util.ExcelWriter;
import com.sparwk.adminnode.admin.jpa.entity.PeriodTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.admin.AdminActiveLogEntity;
import com.sparwk.adminnode.admin.jpa.entity.anr.AnrCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.anr.AnrEntity;
import com.sparwk.adminnode.admin.jpa.entity.anr.AnrSorterEnum;
import com.sparwk.adminnode.admin.jpa.entity.commonCode.CommonDetailCodeEntity;
import com.sparwk.adminnode.admin.jpa.repository.anr.AnrRepository;
import com.sparwk.adminnode.admin.jpa.repository.commonCode.CommonDetailCodeRepository;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class AnrService {

    private final AnrRepository anrRepository;
    private final CommonDetailCodeRepository commonDetailCodeRepository;
    private final AdminActiveLogService adminActiveLogService;

    private final HttpServletRequest httpServletRequest;
    private final MessageSourceAccessor messageSourceAccessor;

    private final Logger logger = LoggerFactory.getLogger(AnrService.class);


    public Response<CommonPagingListDto> getList(String pcode,
                                                 AnrCateEnum cate,
                                                 String val,
                                                 YnTypeEnum useType,
                                                 PeriodTypeEnum periodType,
                                                 String sdate,
                                                 String edate,
                                                 AnrSorterEnum sorter,
                                                 PageRequest pageRequest) {

        Response<CommonPagingListDto> res = new Response<>();
        
        List<AnrDto> anrList =
                anrRepository.findQryAll(pcode, cate, val, useType, periodType, sdate, edate, sorter, pageRequest);

        logger.info("anrList - {}", anrList);

        //조회내용 없을 때
        if(anrList == null || anrList.size() < 1){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        CommonPagingListDto list = new CommonPagingListDto();

        //총 카운트
        long totalSize = anrRepository.countQryAll(pcode, cate, val, useType, periodType, sdate, edate);
        long totalPage = totalSize / pageRequest.getPageSize();
        long mod = totalSize % pageRequest.getPageSize();
        if(mod > 0) {
            totalPage += 1;
        }

        list.setList(anrList);
        list.setSize(pageRequest.getPageSize());
        list.setPage(pageRequest.getPageNumber());
        list.setTotalPage(totalPage);
        list.setTotalSize(totalSize);

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(list);
        return res;
    }

    public Response<Optional<AnrDto>> getOne(Long id) {

        Response<Optional<AnrDto>> res = new Response<>();

        //id값이 없을 때
        if(id == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<AnrEntity> anr = anrRepository.findById(id);

        logger.info("anr - {}", anr);

        //조회내용 없을 때
        if(!anr.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //A&R Group 명
        Optional<CommonDetailCodeEntity> commonDetailCode = 
                Optional.ofNullable(commonDetailCodeRepository.findByDcode(anr.get().getAnrGroupCd()));

        logger.info("commonDetailCode - {}", commonDetailCode);

        //조회내용 없을 때
        if(!commonDetailCode.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }
        
        Optional<AnrDto> dtoPage =

                anr.map(p -> AnrDto.builder()
                        .anrSeq(p.getAnrSeq())
                        .anrServiceName(p.getAnrServiceName())
                        .anrCd(p.getAnrCd())
                        .anrGroupCd(p.getAnrGroupCd())
                        .anrGroupCdName(commonDetailCode.get().getVal())
                        .description(p.getDescription())
                        .useType(p.getUseType())
                        .regUsr(p.getRegUsr())
                        .regDt(p.getRegDt())
                        .modUsr(p.getModUsr())
                        .modDt(p.getModDt())
                        .build()
                );

        logger.info("Anr - {}", dtoPage);

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(dtoPage);
        return res;
    }

    @Transactional
    public Response<Void> saveContent(AnrRequest request, HttpServletRequest req) {

        Response<Void> res = new Response<>();
        String anrCd;

        //A&R Group 명
        Optional<CommonDetailCodeEntity> commonDetailCode =
                Optional.ofNullable(commonDetailCodeRepository.findByDcode(request.getAnrDto().getAnrGroupCd()));

        logger.info("commonDetailCode - {}", commonDetailCode);

        //부모코드 조회내용 없을 때
        if(!commonDetailCode.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //USEYN 값 없거나 틀릴 때
        if(request.getAnrDto().getUseType() == null || (request.getAnrDto().getUseType() != YnTypeEnum.Y && request.getAnrDto().getUseType() != YnTypeEnum.N)){
            res.setResultCd(ResultCodeConst.NOT_VALID_USEYN.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if(request.getAnrDto().getAnrServiceName() == null){
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        try {

            //seq 번호 불러옴
            int k = anrRepository.getLastSeq("ANR") + 1;

            if(k < 10)
                anrCd = "ANR00000" + k;
            else if(k < 100)
                anrCd = "ANR0000" + k;
            else if(k < 1000)
                anrCd = "ANR000" + k;
            else if(k < 10000)
                anrCd = "ANR00" + k;
            else if(k < 100000)
                anrCd = "ANR0" + k;
            else if(k < 1000000)
                anrCd = "ANR" + k;
            else
                anrCd = "ANR" + k;

            AnrEntity newAnr = AnrEntity.builder()
                    .anrServiceName(request.getAnrDto().getAnrServiceName())
                    .anrCd(anrCd)
                    .anrGroupCd(request.getAnrDto().getAnrGroupCd())
                    .description(request.getAnrDto().getDescription())
                    .useType(request.getAnrDto().getUseType())
                    .build();

            AnrEntity saved = anrRepository.save(newAnr);

            //seq 번호 수동으로 업데이트 시킨다
            anrRepository.updateLastSeq("ANR", k);



//            Optional<AnrEntity> anr = anrRepository.findById(saved.getAnrSeq());
//
//            Optional<AnrDto> dtoPage =
//
//                    anr.map(p -> AnrDto.builder()
//                            .anrSeq(p.getAnrSeq())
//                            .anr(p.getAnr())
//                            .anrCd(p.getAnrCd())
//                            .continentCode(p.getContinentCode())
//                            .continentCodeVal(p.getContinentEntity().getContinent())
//                            .iso2(p.getIso2())
//                            .iso3(p.getIso3())
//                            .description(p.getDescription())
//                            .dial(p.getDial())
//                            .nmr(p.getNmr())
//                            .useType(p.getUseType())
//                            .regUsr(p.getRegUsr())
//                            .regDt(p.getRegDt())
//                            .modUsr(p.getModUsr())
//                            .modDt(p.getModDt())
//                            .build()
//                    );
//
//            return dtoPage;
            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
//            res.setResult(dtoPage);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));

            String menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_ANR_SERVICE.getCode());
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_CREATE.getCode());

            //netive query
            String usrName = adminActiveLogService.findQryUsrName(saved.getRegUsr());

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(saved.getRegDt())
                    .adminId(saved.getRegUsr())
                    .menuName(menuName)
                    .activeType("C")
                    .contentsId(saved.getAnrSeq())
                    .activeMsg(usrName + " " + activeMsg + " '" + saved.getAnrServiceName() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e){
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }

        return res;
    }

    @Transactional
    public Response<Void> updateYn(Long id, HttpServletRequest req) {

        Response<Void> res = new Response<>();

        //id값이 없을 때
        if(id == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<AnrEntity> anr = anrRepository.findById(id);


        //조회내용 없을 때
        if(!anr.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        YnTypeEnum useType = anr.get().getUseType();
        YnTypeEnum newUseType = (useType == YnTypeEnum.Y) ? YnTypeEnum.N : YnTypeEnum.Y;

        logger.info("newUseType - {} ", newUseType);

        try {
            anr.get().updateYn(newUseType);
            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
//            res.setResult(dtoPage);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));

            String menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_ANR_SERVICE.getCode());
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_MODIFY.getCode());

            //netive query
            String usrName = adminActiveLogService.findQryUsrName(anr.get().getModUsr());

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(anr.get().getModDt())
                    .adminId(anr.get().getModUsr())
                    .menuName(menuName)
                    .activeType("U")
                    .contentsId(anr.get().getAnrSeq())
                    .activeMsg(usrName + " " + activeMsg + " '" + anr.get().getAnrServiceName() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e) {
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }

        return res;
    }

    @Transactional
    public Response<Optional<AnrDto>> updateContent(Long id, AnrRequest request, HttpServletRequest req) {

        Response<Optional<AnrDto>> res = new Response<>();

        //id값이 없을 때
        if(id == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<CommonDetailCodeEntity> commonDetailCode =
                Optional.ofNullable(commonDetailCodeRepository.findByDcode(request.getAnrDto().getAnrGroupCd()));

        logger.info("commonDetailCode - {}", commonDetailCode);

        //부모코드 조회내용 없을 때
        if (!commonDetailCode.isPresent()) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<AnrEntity> anrUpdate = anrRepository.findById(id);

        //조회내용 없을 때
        if(!anrUpdate.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //USEYN 값 없거나 틀릴 때
        if(request.getAnrDto().getUseType() == null || (request.getAnrDto().getUseType() != YnTypeEnum.Y && request.getAnrDto().getUseType() != YnTypeEnum.N)){
            res.setResultCd(ResultCodeConst.NOT_VALID_USEYN.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if(request.getAnrDto().getAnrServiceName() == null){
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        try {

        anrUpdate.get()
                .update(request.getAnrDto().getAnrServiceName(),
                        request.getAnrDto().getAnrCd(),
                        request.getAnrDto().getAnrGroupCd(),
                        request.getAnrDto().getDescription(),
                        request.getAnrDto().getUseType()
                );

        Optional<AnrEntity> anr = anrRepository.findById(anrUpdate.get().getAnrSeq());

        Optional<AnrDto> dtoPage =

                anr.map(p -> AnrDto.builder()
                        .anrSeq(p.getAnrSeq())
                        .anrServiceName(p.getAnrServiceName())
                        .anrCd(p.getAnrCd())
                        .anrGroupCd(p.getAnrGroupCd())
                        .anrGroupCdName(commonDetailCode.get().getVal())
                        .description(p.getDescription())
                        .useType(p.getUseType())
                        .regUsr(p.getRegUsr())
                        .regDt(p.getRegDt())
                        .modUsr(p.getModUsr())
                        .modDt(p.getModDt())
                        .build()
                );

            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            res.setResult(dtoPage);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));

            String menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_ANR_SERVICE.getCode());
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_MODIFY.getCode());

            //netive query
            String usrName = adminActiveLogService.findQryUsrName(anr.get().getModUsr());

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(anr.get().getModDt())
                    .adminId(anr.get().getModUsr())
                    .menuName(menuName)
                    .activeType("U")
                    .contentsId(anr.get().getAnrSeq())
                    .activeMsg(usrName + " " + activeMsg + " '" + anr.get().getAnrServiceName() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e){
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }

        return res;
    }

    @Transactional
    public Response<Void> deleteContent(Long id, HttpServletRequest req) {

        Response<Void> res = new Response<>();

        //id값이 없을 때
        if(id == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<AnrEntity> anr = anrRepository.findById(id);

        //조회내용 없을 때
        if(!anr.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        try {
            anrRepository.deleteById(id);
            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
//            res.setResult(dtoPage);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));
            Long adminId = getAccountId.ofId(httpServletRequest);

            String menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_ANR_SERVICE.getCode());
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_DELETE.getCode());

            //netive query
            String usrName = adminActiveLogService.findQryUsrName(adminId);

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(LocalDateTime.now())
                    .adminId(adminId)
                    .menuName(menuName)
                    .activeType("D")
                    .contentsId(anr.get().getAnrSeq())
                    .activeMsg(usrName + " " + activeMsg + " '" + anr.get().getAnrServiceName() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e) {
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }
        return res;
    }

    public List<AnrExcelDto> getExcelList() {

        List<AnrExcelDto> getExcelList = anrRepository.findExcelList();

        logger.info("getExcelList - {} ", getExcelList);

        return getExcelList;
    }


    //엑셀 다운로드 요청
    public Map<String, Object> downExcelList() throws Exception {
        // 데이터 가져오기
        List<AnrExcelDto> excelList = this.getExcelList();

        logger.info("excelList - {} ", excelList);

        return ExcelWriter.createExcelData(excelList, AnrExcelDto.class);
    }

    //마지막 코드 번호 확인
    public Integer getLastSeq(String code) {

        return anrRepository.getLastSeq(code);
    }

    //코드 번호 업데이트
    @Transactional
    public void updateLastSeq(String code, Integer k) {

        anrRepository.updateLastSeq(code, k);
    }

    @Transactional
    public Response<Void> saveExcelContent(AnrDto anrDto, HttpServletRequest req) {

        Response<Void> res = new Response<>();

        Optional<CommonDetailCodeEntity> commonDetailCode =
                Optional.ofNullable(commonDetailCodeRepository.findByVal(anrDto.getAnrGroupCdName()));

        logger.info("commonDetailCode - {}", commonDetailCode);

        //부모코드 조회내용 없을 때
        if (!commonDetailCode.isPresent()) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //USEYN 값 없거나 틀릴 때
        if (anrDto.getUseType() == null || (anrDto.getUseType() != YnTypeEnum.Y && anrDto.getUseType() != YnTypeEnum.N)) {
            res.setResultCd(ResultCodeConst.NOT_VALID_USEYN.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if(anrDto.getAnrServiceName() == null){
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        try {

            AnrEntity newAnr = AnrEntity.builder()
                    .anrServiceName(anrDto.getAnrServiceName())
                    .anrCd(anrDto.getAnrCd())
                    .anrGroupCd(commonDetailCode.get().getDcode())
                    .description(anrDto.getDescription())
                    .useType(anrDto.getUseType())
                    .build();

            AnrEntity saved = anrRepository.save(newAnr);

            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
//            res.setResult(dtoPage);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));

            String menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_ANR_SERVICE.getCode());
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_CREATE.getCode());

            //netive query
            String usrName = adminActiveLogService.findQryUsrName(saved.getRegUsr());

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(saved.getRegDt())
                    .adminId(saved.getRegUsr())
                    .menuName(menuName)
                    .activeType("C")
                    .contentsId(saved.getAnrSeq())
                    .activeMsg(usrName + " " + activeMsg + " '" + saved.getAnrServiceName() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e) {
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }

        return res;
    }

}
