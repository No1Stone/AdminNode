package com.sparwk.adminnode.admin.biz.v1.continent.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@Schema(description = "Request DTO")
public class ContinentRequest {

//    @Schema(description = "대륙명", nullable = false, example = "Africa")
//    @NotBlank(message = "Please enter a continent.")
//    private String continent;
//    @Schema(description = "ISO 대륙코드", nullable = false, example = "AF")
//    @NotBlank(message = "Please enter a code.")
//    private String code;
//    @Schema(description = "description", nullable = true, example = "설명")
//    private String description;
//    @Schema(description = "코드 사용여부 Y/N",  defaultValue = "Y", allowableValues = {"Y", "N"})
//    @NotNull(message = "Please check use.")
//    private UseType useType;
//
//    @Builder
//    public ContinentRequest(String continent,
//                            String code,
//                            String description,
//                            UseType useType
//    ) {
//        this.continent = continent;
//        this.code = code;
//        this.description = description;
//        this.useType = useType;
//    }

    private ContinentDto continentDto;
}
