package com.sparwk.adminnode.admin.biz.v1.song.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class SongMetadataCustomDto {

    @Schema(description = "song id 번호", nullable = false, example = "1")
    private Long songId;
    @Schema(description = "metadata seq id 번호", nullable = false, example = "1")
    private Long metadataSeq;
    @Schema(description = "커스텀 키", nullable = false, example = "key")
    private String metadataName;
    @Schema(description = "커스텀 값", nullable = false, example = "value")
    private String metadataValue;


    @Builder
    public SongMetadataCustomDto(Long songId,
                                 Long metadataSeq,
                                 String metadataName,
                                 String metadataValue
    ) {
        this.songId = songId;
        this.metadataSeq = metadataSeq;
        this.metadataName = metadataName;
        this.metadataValue = metadataValue;
    }
}
