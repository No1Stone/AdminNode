package com.sparwk.adminnode.admin.biz.v1.account.dto;

import com.sparwk.adminnode.admin.biz.v1.profile.dto.ProfileMetaDataDto;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

@Setter
@Getter
@NoArgsConstructor
public class IndividualMemberDetailDto {

    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long accntId;
    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long profileId;
    @Schema(description = "ACCNT_TYPE_CD", nullable = false, example = "COMPANY / GROUP / PERSON")
    private String accntTypeCd;
    @Schema(description = "ACCNT_Email", nullable = false, example = "yoon@gmail.com")
    private String accntEmail;
    @Schema(description = "ACCNT FULL NAME", nullable = false, example = "Mr.Yoon")
    private String profileFullName;
    @Schema(description = "STAGE_NAME_YN", nullable = false, defaultValue = "Y", allowableValues = {"Y", "N"})
    private YnTypeEnum stageNameYn;
    @Schema(description = "Date a Birth", nullable = false, example = "2001")
    private String bthYear;
    @Schema(description = "Date a Middle", nullable = false, example = "1")
    private String bthMonth;
    @Schema(description = "Date a Day", nullable = false, example = "1")
    private String bthDay;
    @Schema(description = "Role", nullable = true)
    private List<ProfileMetaDataDto> roleList;
    @Schema(description = "Gender", nullable = true)
    private List<ProfileMetaDataDto> genderList;
    @Schema(description = "Language", nullable = true)
    private List<ProfileMetaDataDto> languageList;

    @Schema(description = "Current country", nullable = false, example = "CNT000158")
    private String currentCityCountryCd;
    @Schema(description = "Current country Name", nullable = false, example = "USA")
    private String currentCityCountryCdName;
    @Schema(description = "Current city", nullable = false, example = "구로구")
    private String currentCityNm;
    @Schema(description = "Hometown Country", nullable = false, example = "CNT000158")
    private String hometownCountryCd;
    @Schema(description = "Hometown country Name", nullable = false, example = "USA")
    private String hometownCountryCdName;
    @Schema(description = "Hometown city", nullable = false, example = "구로구")
    private String hometownCityNm;

    @Schema(description = "Passport Country", nullable = false, example = "CNT000158")
    private String passportCountryCd;
    @Schema(description = "Passport country Name", nullable = false, example = "USA")
    private String passportCountryCdName;
    @Schema(description = "ACCNT First NAME", nullable = false, example = "Yoon")
    private String accntFirstName;
    @Schema(description = "ACCNT Middle NAME", nullable = false, example = "Jung")
    private String accntMiddleName;
    @Schema(description = "ACCNT Last NAME", nullable = false, example = "Hun")
    private String accntLastName;
    @Schema(description = "Passport photocopy", nullable = false, example = "http://...")
    private String passportPhotocopy;
    @Schema(description = "Passport photocopy file", nullable = false, example = "filename.jpg")
    private String passportfile;
    @Schema(description = "ACCOUNT PASSPORT VERIFY YN", nullable = false, defaultValue = "N", allowableValues = {"Y", "N"})
    private YnTypeEnum passportVerifyYn;
    @Schema(description = "ACCOUNT VERIFY YN", nullable = false, defaultValue = "N", allowableValues = {"Y", "N"})
    private YnTypeEnum accountVerifyYn;
    @Schema(description = "COUNTRY_CD", nullable = false, example = "CNT000158")
    private String phoneCountryCd;
    @Schema(description = "COUNTRY_CD_NAME", nullable = true, example = "USA")
    private String phoneCountryCdName;
    @Schema(description = "PHONE_NUMBER", nullable = false, example = "0821012344321")
    private String phoneNumber;
    @Schema(description = "accountVerifyPhoneYn", nullable = false, example = "Y")
    private YnTypeEnum accountVerifyPhoneYn;
    @Schema(description = "IPI_NUMBER", nullable = true, example = "12344321")
    private String ipiInfo;
    @Schema(description = "IPI VERIFY YN",  nullable = false, defaultValue = "N", allowableValues = {"Y", "N"})
    private YnTypeEnum ipiVerifyYn;

    @Schema(description = "등록일", pattern = "yyMMdd hh:mm:ss", example = "2021-12-08 05:44:26.627991")
    private LocalDateTime regDt;

    @Builder
    public IndividualMemberDetailDto(Long accntId,
                                     Long profileId,
                                     String accntTypeCd,
                                     String accntFirstName,
                                     String accntMiddleName,
                                     String accntLastName,
                                     String profileFullName,
                                     YnTypeEnum stageNameYn,
                                     String accntEmail,
                                     String bthYear,
                                     String bthMonth,
                                     String bthDay,
                                     List<ProfileMetaDataDto> genderList,
                                     List<ProfileMetaDataDto> languageList,
                                     List<ProfileMetaDataDto> roleList,
                                     String currentCityCountryCd,
                                     String currentCityCountryCdName,
                                     String currentCityNm,
                                     String hometownCountryCd,
                                     String hometownCountryCdName,
                                     String hometownCityNm,
                                     String passportCountryCd,
                                     String passportCountryCdName,
                                     String passportPhotocopy,
                                     String passportfile,
                                     YnTypeEnum passportVerifyYn,
                                     YnTypeEnum accountVerifyYn,
                                     String phoneCountryCd,
                                     String phoneCountryCdName,
                                     String phoneNumber,
                                     YnTypeEnum accountVerifyPhoneYn,
                                     String ipiInfo,
                                     YnTypeEnum ipiVerifyYn,
                                     LocalDateTime regDt
    ) {
        this.accntId = accntId;
        this.profileId = profileId;
        this.accntTypeCd = accntTypeCd;
        this.accntFirstName = accntFirstName;
        this.accntMiddleName = accntMiddleName;
        this.accntLastName = accntLastName;
        this.profileFullName = profileFullName;
        this.stageNameYn = stageNameYn;
        this.accntEmail = accntEmail;
        this.bthYear = bthYear;
        this.bthMonth = bthMonth;
        this.bthDay = bthDay;
        this.genderList = genderList;
        this.languageList = languageList;
        this.roleList = roleList;
        this.currentCityCountryCd = currentCityCountryCd;
        this.currentCityCountryCdName = currentCityCountryCdName;
        this.currentCityNm = currentCityNm;
        this.hometownCountryCd = hometownCountryCd;
        this.hometownCountryCdName = hometownCountryCdName;
        this.hometownCityNm = hometownCityNm;
        this.passportCountryCd = passportCountryCd;
        this.passportCountryCdName = passportCountryCdName;
        this.passportPhotocopy = passportPhotocopy;
        this.passportfile = passportfile;
        this.passportVerifyYn = passportVerifyYn;
        this.accountVerifyYn = accountVerifyYn;
        this.phoneCountryCd = phoneCountryCd;
        this.phoneCountryCdName = phoneCountryCdName;
        this.phoneNumber = phoneNumber;
        this.accountVerifyPhoneYn = accountVerifyPhoneYn;
        this.ipiInfo = ipiInfo;
        this.ipiVerifyYn = ipiVerifyYn;
        this.regDt = regDt;

    }
}
