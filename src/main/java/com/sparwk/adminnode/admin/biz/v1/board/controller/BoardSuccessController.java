package com.sparwk.adminnode.admin.biz.v1.board.controller;

import com.sparwk.adminnode.admin.biz.v1.CommonPagingListDto;
import com.sparwk.adminnode.admin.biz.v1.board.dto.BoardDelete;
import com.sparwk.adminnode.admin.biz.v1.board.dto.BoardSuccessDto;
import com.sparwk.adminnode.admin.biz.v1.board.dto.BoardSuccessRequest;
import com.sparwk.adminnode.admin.biz.v1.board.service.BoardSuccessService;
import com.sparwk.adminnode.admin.config.common.AdminPermissionIntergrationService;
import com.sparwk.adminnode.admin.config.common.Response;
import com.sparwk.adminnode.admin.config.common.ResultCodeConst;
import com.sparwk.adminnode.admin.config.common.Webconfig;
import com.sparwk.adminnode.admin.jpa.entity.PeriodTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.board.BoardCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.board.BoardSorterEnum;
import com.sparwk.adminnode.admin.jpa.entity.board.SuccessCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.board.SuccessSorterEnum;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/V1/setting/")
@RequiredArgsConstructor
@CrossOrigin("*")
public class BoardSuccessController {

    private final Logger logger = LoggerFactory.getLogger(BoardSuccessController.class);

    @Autowired
    private BoardSuccessService boardSuccessService;
    private final MessageSourceAccessor messageSourceAccessor;
    @Autowired
    private AdminPermissionIntergrationService adminPermissionIntergrationService;

    @Value("${file.path}")
    private String fileRealPath;

    @GetMapping("success")
    @Operation(
            summary = "Main Success Board List", description = "Main Success Board List입니다.",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = BoardSuccessDto.class))),
                    @ApiResponse(
                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
                            , content = @Content(schema = @Schema(hidden = true )))
            })
    public Response<CommonPagingListDto> getList(
            @Parameter(name = "cate", description = "검색 종류", in = ParameterIn.PATH) @RequestParam(value = "cate", required = false) SuccessCateEnum cate,
            @Parameter(name = "val", description = "검색어 포함", in = ParameterIn.PATH) @RequestParam(value = "val", required = false) String val,
            @Parameter(name = "useType", description = "사용여부 값 Y/N", in = ParameterIn.PATH) @RequestParam(value = "useType", required = false) YnTypeEnum useType,
            @Parameter(name = "periodType", description = "기간 종류", in = ParameterIn.PATH) @RequestParam(value = "periodType", required = false) PeriodTypeEnum periodType,
            @Parameter(name = "sdate", description = "시작일", in = ParameterIn.PATH) @RequestParam(value = "sdate", required = false) String sdate,
            @Parameter(name = "edate", description = "종료일", in = ParameterIn.PATH) @RequestParam(value = "edate", required = false) String edate,
            @Parameter(name = "sorterType", description = "순서", in = ParameterIn.PATH) @RequestParam(value = "sorterType", required = false) SuccessSorterEnum sorterType,
            @Parameter(name = "page", description = "페이지 번호", in = ParameterIn.PATH) @RequestParam(value = "page", required = false, defaultValue = "1") int page,
            @Parameter(name = "size", description = "페이지 사이즈", in = ParameterIn.PATH) @RequestParam(value = "size", required = false, defaultValue = "10") int size
    ) {
        PageRequest pageRequest = PageRequest.of(page, size);

        Response res = new Response();
        if(!adminPermissionIntergrationService.AdminPermissionIntergrationCheck(3L, "R")){
            res.setResultCd(ResultCodeConst.NOT_VALID_READ.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        return boardSuccessService.getList(cate, val, useType, periodType, sdate, edate, sorterType, pageRequest);
    }

    //사용중만 보기
    @GetMapping("success/use")
    @Operation(
            summary = "Main Success Board Use List", description = "Main Success Board Use List 입니다.",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = BoardSuccessDto.class))),
                    @ApiResponse(
                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
                            , content = @Content(schema = @Schema(hidden = true )))
            })
    public Response<CommonPagingListDto> getUseYList(
//            @Parameter(name = "pcode", description = "categry 종류", in = ParameterIn.PATH) @RequestParam(value = "pcode", required = false) String pcode,
//            @Parameter(name = "cate", description = "검색 종류", in = ParameterIn.PATH) @RequestParam(value = "cate", required = false) BoardCateEnum cate,
//            @Parameter(name = "val", description = "검색어 포함", in = ParameterIn.PATH) @RequestParam(value = "val", required = false) String val,
//            @Parameter(name = "page", description = "페이지 번호", in = ParameterIn.PATH) @RequestParam(value = "page", required = false, defaultValue = "1") int page,
//            @Parameter(name = "size", description = "페이지 사이즈", in = ParameterIn.PATH) @RequestParam(value = "size", required = false, defaultValue = "10") int size
    ) {
//        PageRequest pageRequest = PageRequest.of(page, size);

        Response res = new Response();
        if(!adminPermissionIntergrationService.AdminPermissionIntergrationCheck(3L, "R")){
            res.setResultCd(ResultCodeConst.NOT_VALID_READ.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        return boardSuccessService.getUseYList();
    }

    //리스트 삭제처리
    @PostMapping("success/delete")
    @Operation(
            summary = "FAQ LIST DELETE", description = "FAQ List 삭제합니다.",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = BoardDelete.class))),
                    @ApiResponse(
                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
                            , content = @Content(schema = @Schema(hidden = true)))
            })
    public Response<Void> deleteList(
            @Parameter(name = "request", description = "Request Dto", in = ParameterIn.PATH) @RequestBody @Valid BoardDelete request,
            HttpServletRequest req
    ) {
        Response res = new Response();
        if(!adminPermissionIntergrationService.AdminPermissionIntergrationCheck(3L, "D")){
            res.setResultCd(ResultCodeConst.NOT_VALID_DELETE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        return boardSuccessService.deleteList(request);
    }

    //상세페이지
    @GetMapping("success/{id}")
    @Operation(
            summary = "Main Success Board View", description = "Main Success Board View 입니다.",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = BoardSuccessDto.class))),
                    @ApiResponse(
                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
                            , content = @Content(schema = @Schema(hidden = true ))),
                    @ApiResponse(
                            responseCode = "1003", description = "id가 존재하지 않습니다.."
                            , content = @Content(schema = @Schema(hidden = true )))
            })
    public Response<Optional<BoardSuccessDto>> getOne(
            @Parameter(name = "id", description = "id 값", in = ParameterIn.PATH) @PathVariable(value = "id") Long id
    ) {
        Response res = new Response();
        if(!adminPermissionIntergrationService.AdminPermissionIntergrationCheck(3L, "R")){
            res.setResultCd(ResultCodeConst.NOT_VALID_READ.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        return boardSuccessService.getOne(id);
    }

    //쓰기처리
    @PostMapping("success/new")
    @Operation(
            summary = "Main Success Board", description = "Main Success Board 추가합니다.",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = BoardSuccessDto.class))),
                    @ApiResponse(
                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
                            , content = @Content(schema = @Schema(hidden = true ))),
                    @ApiResponse(
                            responseCode = "1203", description = "Use Type 값 형식이 잘못되었습니다."
                            , content = @Content(schema = @Schema(hidden = true ))),
                    @ApiResponse(
                            responseCode = "1206", description = "Value 형식이 잘못되었습니다."
                            , content = @Content(schema = @Schema(hidden = true )))
            })
    public Response<Void> saveContent(
            @Parameter(name = "request", description = "Request Dto", in = ParameterIn.PATH) @RequestBody @Valid BoardSuccessRequest request,
            HttpServletRequest req
    ) {
        Response res = new Response();
        if(!adminPermissionIntergrationService.AdminPermissionIntergrationCheck(3L, "C")){
            res.setResultCd(ResultCodeConst.NOT_VALID_CREATE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        return boardSuccessService.saveContent(request, req);
    }


    //업데이트처리
    @PostMapping("success/{id}/modify")
    @Operation(
            summary = "Main Success Board MODIFY", description = "Main Success Board 수정합니다.",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = BoardSuccessDto.class))),
                    @ApiResponse(
                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
                            , content = @Content(schema = @Schema(hidden = true ))),
                    @ApiResponse(
                            responseCode = "1003", description = "id가 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true ))),
                    @ApiResponse(
                            responseCode = "1203", description = "Use Type 값 형식이 잘못되었습니다."
                            , content = @Content(schema = @Schema(hidden = true ))),
                    @ApiResponse(
                            responseCode = "1206", description = "Value 형식이 잘못되었습니다."
                            , content = @Content(schema = @Schema(hidden = true )))
            })
    public Response<Optional<BoardSuccessDto>> updateContent(
            @Parameter(name = "id", description = "id 값", in = ParameterIn.PATH) @PathVariable(value = "id") Long id,
            @Parameter(name = "request", description = "Request Dto", in = ParameterIn.PATH) @RequestBody @Valid BoardSuccessRequest request,
            HttpServletRequest req
    ) {
        Response res = new Response();
        if(!adminPermissionIntergrationService.AdminPermissionIntergrationCheck(3L, "U")){
            res.setResultCd(ResultCodeConst.NOT_VALID_UPDATE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        return boardSuccessService.updateContent(id, request, req);
    }

    //삭제처리
    @DeleteMapping("success/{id}/delete")
    @Operation(
            summary = "Main Success Board DELETE", description = "Main Success Board 삭제합니다.",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = BoardSuccessDto.class))),
                    @ApiResponse(
                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
                            , content = @Content(schema = @Schema(hidden = true ))),
                    @ApiResponse(
                            responseCode = "1003", description = "id가 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true )))
            })
    public Response<Void> delete(
            @Parameter(name = "id", description = "id 값", in = ParameterIn.PATH) @PathVariable(value = "id") Long id,
            HttpServletRequest req) {
        Response res = new Response();
        if(!adminPermissionIntergrationService.AdminPermissionIntergrationCheck(3L, "D")){
            res.setResultCd(ResultCodeConst.NOT_VALID_DELETE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        return boardSuccessService.deleteContent(id, req);
    }

}
