package com.sparwk.adminnode.admin.biz.v1.board.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class BoardAttachRequest {

    @Schema(description = "사용 Board", nullable = false, example = "1")
    private Long attacId;
    @Schema(description = "게시판 타입", nullable = false, example = "Question / Answer / FAQ / NEWS")
    private String boardType;
    @Schema(description = "사용 Board", nullable = false, example = "1")
    private Long boardId;
    @Schema(description = "파일 순번", nullable = true, example = "1")
    private Long fileNum;
    @Schema(description = "첨부파일 URL", nullable = false, example = "http://...")
    private String fileUrl;
    @Schema(description = "파일명", nullable = false, example = "fileName.txt")
    private String fileName;
    @Schema(description = "file Size", nullable = true, example = "100000")
    private Long fileSize;

}
