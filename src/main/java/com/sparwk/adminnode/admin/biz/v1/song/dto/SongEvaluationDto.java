package com.sparwk.adminnode.admin.biz.v1.song.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class SongEvaluationDto {

    @Schema(description = "id 번호", nullable = false, example = "1")
    private Long evalSeq;
    @Schema(description = "id 번호", nullable = false, example = "1")
    private Long songId;
    @Schema(description = "", nullable = false, example = "")
    private String memberType;
    @Schema(description = "id 번호", nullable = false, example = "1")
    private Long evalAnrId;
    @Schema(description = "id 번호", nullable = false, example = "1")
    private Long evalAnrSeq;
    @Schema(description = "", nullable = false, example = "")
    private String fullName;
    @Schema(description = "id 번호", nullable = false, example = "1")
    private Long anrProfileCompanyId;
    @Schema(description = "", nullable = false, example = "")
    private String profileCompanyName;
    @Schema(description = "eval_status_name", nullable = false, example = "")
    private String evalStatusName;
    @Schema(description = "", nullable = false, example = "")
    private int totalRate;

    @Builder
    public SongEvaluationDto(Long evalSeq,
                             Long songId,
                             String memberType,
                             Long evalAnrId,
                             Long evalAnrSeq,
                             String fullName,
                             Long anrProfileCompanyId,
                             String profileCompanyName,
                             String evalStatusName,
                             int totalRate
    ) {
        this.evalSeq = evalSeq;
        this.songId = songId;
        this.memberType = memberType;
        this.evalAnrId = evalAnrId;
        this.evalAnrSeq = evalAnrSeq;
        this.fullName = fullName;
        this.anrProfileCompanyId = anrProfileCompanyId;
        this.profileCompanyName = profileCompanyName;
        this.evalStatusName = evalStatusName;
        this.totalRate = totalRate;
    }
}
