package com.sparwk.adminnode.admin.biz.v1.email.controller;

import com.sparwk.adminnode.admin.biz.v1.CommonPagingListDto;
import com.sparwk.adminnode.admin.biz.v1.email.dto.EmailDto;
import com.sparwk.adminnode.admin.biz.v1.email.dto.EmailRequest;
import com.sparwk.adminnode.admin.biz.v1.email.service.EmailService;
import com.sparwk.adminnode.admin.config.common.AdminPermissionIntergrationService;
import com.sparwk.adminnode.admin.config.common.Response;
import com.sparwk.adminnode.admin.config.common.ResultCodeConst;
import com.sparwk.adminnode.admin.jpa.entity.email.EmailCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.email.EmailMemberCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.email.EmailSorterEnum;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@RequestMapping(value = "/V1/operation/maintenance/")
@RequiredArgsConstructor
@CrossOrigin("*")
public class EmailController {

    private final Logger logger = LoggerFactory.getLogger(EmailController.class);

    @Autowired
    private EmailService emailService;

    @Autowired
    private AdminPermissionIntergrationService adminPermissionIntergrationService;
    private final MessageSourceAccessor messageSourceAccessor;

    @Value("${file.path}")
    private String fileRealPath;

    @GetMapping("email")
    @Operation(
            summary = "SNS List", description = "SNS All List - Use NO Include(사용안함도 포함한 전체 리스트입니다.)",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = EmailDto.class))),
                    @ApiResponse(
                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "404", description = "존재하지 않는 리소스 접근"
                            , content = @Content(schema = @Schema(hidden = true)))
            })
    public Response<CommonPagingListDto> getList(
            @Parameter(name = "cate", description = "검색 종류", in = ParameterIn.PATH) @RequestParam(value = "cate", required = false) EmailCateEnum cate,
            @Parameter(name = "val", description = "코드 값 포함", in = ParameterIn.PATH) @RequestParam(value = "val", required = false) String val,
            @Parameter(name = "sorter", description = "순서", in = ParameterIn.PATH) @RequestParam(value = "sorter", required = false) EmailSorterEnum sorter,
            @Parameter(name = "page", description = "페이지 번호", in = ParameterIn.PATH) @RequestParam(value = "page", required = false, defaultValue = "1") int page,
            @Parameter(name = "size", description = "페이지 사이즈", in = ParameterIn.PATH) @RequestParam(value = "size", required = false, defaultValue = "10") int size

    ) {
        PageRequest pageRequest = PageRequest.of(page, size);

        Response res = new Response();
        if(!adminPermissionIntergrationService.AdminPermissionIntergrationCheck(7L, "R") &&
                !adminPermissionIntergrationService.AdminPermissionIntergrationCheck(81L, "R") ){
            res.setResultCd(ResultCodeConst.NOT_VALID_READ.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        return emailService.getList(cate, val, sorter, pageRequest);
    }

    //쓰기처리
    @PostMapping("email/new")
    @Operation(
            summary = "New Email", description = "Email 데이터 추가합니다.",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = EmailDto.class))),
                   @ApiResponse(
                            responseCode = "1206", description = "Value 형식이 잘못되었습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "2000", description = "서비스 실행에 실패하였습니다."
                            , content = @Content(schema = @Schema(hidden = true)))
            })
    public Response<Void> saveContent(
            @Parameter(name = "request", description = "Request Dto", in = ParameterIn.PATH) @RequestBody @Valid EmailRequest request,
            HttpServletRequest req
    ) {
        Response res = new Response();
        if(!adminPermissionIntergrationService.AdminPermissionIntergrationCheck(7L, "C") &&
                !adminPermissionIntergrationService.AdminPermissionIntergrationCheck(81L, "C") ){
            res.setResultCd(ResultCodeConst.NOT_VALID_CREATE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        return emailService.saveContent(request, req);
    }

    @GetMapping("email/member-settings")
    @Operation(
            summary = "Member Search Setting", description = "Member Search Setting  ",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(hidden = true)))
            })
    public Response<CommonPagingListDto> getMemberCheckList(
    ) {
        return emailService.getMemberSettings();
    }

    @GetMapping("email/members")
    @Operation(
            summary = "Member List", description = "Member List 입니다",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = EmailDto.class))),
                    @ApiResponse(
                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "404", description = "존재하지 않는 리소스 접근"
                            , content = @Content(schema = @Schema(hidden = true)))
            })
    public Response<CommonPagingListDto> getMemberList(
            @Parameter(name = "cate", description = "검색 종류", in = ParameterIn.PATH) @RequestParam(value = "cate", required = false) EmailMemberCateEnum cate,
            @Parameter(name = "val", description = "검색값", in = ParameterIn.PATH) @RequestParam(value = "val", required = false) String val,
            @Parameter(name = "indiYn", description = "individual 체크", in = ParameterIn.PATH) @RequestParam(value = "indiYn", required = false) String indiYn,
            @Parameter(name = "comYn", description = "company 체크", in = ParameterIn.PATH) @RequestParam(value = "comYn", required = false) String comYn,
            @Parameter(name = "etcYn", description = "etc 체크", in = ParameterIn.PATH) @RequestParam(value = "etcYn", required = false) String etcYn,
            @Parameter(name = "page", description = "페이지 번호", in = ParameterIn.PATH) @RequestParam(value = "page", required = false, defaultValue = "1") int page,
            @Parameter(name = "size", description = "페이지 사이즈", in = ParameterIn.PATH) @RequestParam(value = "size", required = false, defaultValue = "10") int size

    ) {
        PageRequest pageRequest = PageRequest.of(page, size);

        return emailService.getMemberList(cate, val, indiYn, comYn, etcYn, pageRequest);
    }

}
