package com.sparwk.adminnode.admin.biz.v1;

import com.sparwk.adminnode.admin.biz.v1.account.dto.CompanyMemberViewListDto;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.util.List;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class CommonPagingListDto {

    @Schema(description = "Paging size")
    private int size;

    @Schema(description = "Paging Number")
    private int page;

    @Schema(description = "Total Paging Size")
    private long totalSize;

    @Schema(description = "Total Page")
    private long totalPage;

    @Schema(description = "List DTO")
    private Object list;

}
