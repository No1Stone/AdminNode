package com.sparwk.adminnode.admin.biz.v1.continent.service;

import com.sparwk.adminnode.admin.biz.v1.CommonPagingListDto;
import com.sparwk.adminnode.admin.biz.v1.admin.service.AdminActiveLogService;
import com.sparwk.adminnode.admin.biz.v1.continent.dto.ContinentDto;
import com.sparwk.adminnode.admin.biz.v1.continent.dto.ContinentExcelDto;
import com.sparwk.adminnode.admin.biz.v1.continent.dto.ContinentRequest;
import com.sparwk.adminnode.admin.config.common.ActiveCodeConst;
import com.sparwk.adminnode.admin.config.common.GetAccountId;
import com.sparwk.adminnode.admin.config.common.Response;
import com.sparwk.adminnode.admin.config.common.ResultCodeConst;
import com.sparwk.adminnode.admin.config.util.ExcelWriter;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.admin.AdminActiveLogEntity;
import com.sparwk.adminnode.admin.jpa.entity.continent.ContinentCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.continent.ContinentEntity;
import com.sparwk.adminnode.admin.jpa.entity.continent.ContinentSorterEnum;
import com.sparwk.adminnode.admin.jpa.repository.continent.ContinentRepository;
import com.sparwk.adminnode.admin.jpa.repository.country.CountryRepository;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class ContinentService {

    private final ContinentRepository continentRepository;
    private final CountryRepository countryRepository;
    private final AdminActiveLogService adminActiveLogService;

    private final HttpServletRequest httpServletRequest;
    private final MessageSourceAccessor messageSourceAccessor;

    private final Logger logger = LoggerFactory.getLogger(ContinentService.class);


    public Response<CommonPagingListDto> getList(ContinentCateEnum cate, String val, YnTypeEnum useType, ContinentSorterEnum sorter, PageRequest pageRequest) {

        Response<CommonPagingListDto> res = new Response<>();

        List<ContinentDto> continentList = continentRepository.findQryAll(cate, val, useType, sorter, pageRequest);

        logger.info("continentList - {}", continentList);

        //조회내용 없을 때
        if(continentList == null || continentList.size() < 1){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        CommonPagingListDto list = new CommonPagingListDto();

        //총 카운트
        long totalSize = continentRepository.countQryAll(cate, val, useType);
        long totalPage = totalSize / pageRequest.getPageSize();
        long mod = totalSize % pageRequest.getPageSize();
        if(mod > 0) {
            totalPage += 1;
        }

        list.setList(continentList);
        list.setSize(pageRequest.getPageSize());
        list.setPage(pageRequest.getPageNumber());
        list.setTotalPage(totalPage);
        list.setTotalSize(totalSize);

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(list);
        return res;
    }

    public Response<CommonPagingListDto> getUseYList() {

        Response<CommonPagingListDto> res = new Response<>();

        List<ContinentDto> continentList = continentRepository.findQryUseY();

        logger.info("continentList - {}", continentList);

        //WorldWide  추가
        ContinentDto continentWorld = new ContinentDto();
        Long cnt = countryRepository.countByUseType(YnTypeEnum.Y);
        continentWorld.setCnt(cnt);
        continentWorld.setContinent("WorldWide");
        continentList.add(continentWorld);

        //조회내용 없을 때
        if(continentList == null || continentList.size() < 1){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        CommonPagingListDto list = new CommonPagingListDto();

        PageRequest pageRequest = PageRequest.of(10, 10);

        //총 카운트
        long totalSize = continentRepository.countQryUseY();
        long totalPage = totalSize / pageRequest.getPageSize();
        long mod = totalSize % pageRequest.getPageSize();
        if(mod > 0) {
            totalPage += 1;
        }

        list.setList(continentWorld);
        list.setSize(pageRequest.getPageSize());
        list.setPage(pageRequest.getPageNumber());
        list.setTotalPage(totalPage);
        list.setTotalSize(totalSize);

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(list);
        return res;
    }

    public Response<Optional<ContinentDto>> getOne(Long id) {

        Response<Optional<ContinentDto>> res = new Response<>();

        //id값이 없을 때
        if(id == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<ContinentEntity> continent = continentRepository.findById(id);

        logger.info("continent - {}", continent);

        //조회내용 없을 때
        if(!continent.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<ContinentDto> dtoPage =

                continent.map(p -> ContinentDto.builder()
                        .continentSeq(p.getContinentSeq())
                        .continent(p.getContinent())
                        .continentCd(p.getContinentCd())
                        .code(p.getCode())
                        .description(p.getDescription())
                        .useType(p.getUseType())
                        .regUsr(p.getRegUsr())
                        .regDt(p.getRegDt())
                        .modUsr(p.getModUsr())
                        .modDt(p.getModDt())
                        .build()
                );

        logger.info("Continent - {}", dtoPage);

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(dtoPage);
        return res;
    }

    @Transactional
    public Response<Void> saveContent(ContinentRequest request, HttpServletRequest req) {

        Response<Void> res = new Response<>();
        String pcode = "COT";
        String dcode;

        //USEYN 값 없거나 틀릴 때
        if(request.getContinentDto().getUseType() == null || (request.getContinentDto().getUseType() != YnTypeEnum.Y && request.getContinentDto().getUseType() != YnTypeEnum.N)){
            res.setResultCd(ResultCodeConst.NOT_VALID_USEYN.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if(request.getContinentDto().getContinent() == null){
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        try {

            //seq 번호 불러옴
            int k = continentRepository.getLastSeq(pcode) + 1;

            if(k < 10)
                dcode = pcode + "00000" + k;
            else if(k < 100)
                dcode = pcode + "0000" + k;
            else if(k < 1000)
                dcode = pcode + "000" + k;
            else if(k < 10000)
                dcode = pcode + "00" + k;
            else if(k < 100000)
                dcode = pcode + "0" + k;
            else if(k < 1000000)
                dcode = pcode + k;
            else
                dcode = pcode + k;

            ContinentEntity newContinent = ContinentEntity.builder()
                    .continent(request.getContinentDto().getContinent())
                    .code(request.getContinentDto().getCode())
                    .description(request.getContinentDto().getDescription())
                    .continentCd(dcode)
                    .useType(request.getContinentDto().getUseType())
                    .build();

            ContinentEntity saved = continentRepository.save(newContinent);

            //seq 번호 수동으로 업데이트 시킨다
            continentRepository.updateLastSeq(pcode, k);

//            Optional<ContinentEntity> continent = continentRepository.findById(saved.getContinentSeq());
//
//            Optional<ContinentDto> dtoPage =
//
//                    continent.map(p -> ContinentDto.builder()
//                            .continentSeq(p.getContinentSeq())
//                            .continent(p.getContinent())
//                            .code(p.getCode())
//                            .description(p.getDescription())
//                            .useType(p.getUseType())
//                            .regUsr(p.getRegUsr())
//                            .regDt(p.getRegDt())
//                            .modUsr(p.getModUsr())
//                            .modDt(p.getModDt())
//                            .build()
//                    );
//
//            return dtoPage;
            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
//            res.setResult(dtoPage);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));

            String menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_CONTINENTS.getCode());
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_CREATE.getCode());

            //netive query
            String usrName = adminActiveLogService.findQryUsrName(saved.getRegUsr());

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(saved.getRegDt())
                    .adminId(saved.getRegUsr())
                    .menuName(menuName)
                    .activeType("C")
                    .contentsId(saved.getContinentSeq())
                    .activeMsg(usrName + " " + activeMsg + " '" + saved.getContinent() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e){
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }

        return res;
    }

    @Transactional
    public Response<Void> updateYn(Long id, HttpServletRequest req) {

        Response<Void> res = new Response<>();

        //id값이 없을 때
        if(id == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<ContinentEntity> continent = continentRepository.findById(id);

        logger.info("continent - {}", continent);

        //조회내용 없을 때
        if(!continent.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        YnTypeEnum useType = continent.get().getUseType();
        YnTypeEnum newUseType = (useType == YnTypeEnum.Y) ? YnTypeEnum.N : YnTypeEnum.Y;

        logger.info("newUseType - {} ", newUseType);

        try {
            continent.get().updateYn(newUseType);
            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            //res.setResult(songDetailCode);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));

            String menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_CONTINENTS.getCode());
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_MODIFY.getCode());

            //netive query
            String usrName = adminActiveLogService.findQryUsrName(continent.get().getModUsr());

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(continent.get().getModDt())
                    .adminId(continent.get().getModUsr())
                    .menuName(menuName)
                    .activeType("U")
                    .contentsId(continent.get().getContinentSeq())
                    .activeMsg(usrName + " " + activeMsg + " '" + continent.get().getContinent() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e) {
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }
        return res;
    }

    @Transactional
    public Response<Optional<ContinentDto>> updateContent(Long id, ContinentRequest request, HttpServletRequest req) {

        Response<Optional<ContinentDto>> res = new Response<>();

        //id값이 없을 때
        if(id == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<ContinentEntity> continentUpdate = continentRepository.findById(id);

        logger.info("continentUpdate - {} ", continentUpdate);

        //조회내용 없을 때
        if(!continentUpdate.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //USEYN 값 없거나 틀릴 때
        if(request.getContinentDto().getUseType() == null || (request.getContinentDto().getUseType() != YnTypeEnum.Y && request.getContinentDto().getUseType() != YnTypeEnum.N)){
            res.setResultCd(ResultCodeConst.NOT_VALID_USEYN.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if(request.getContinentDto().getContinent() == null){
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        try {


            continentUpdate.get()
                    .update(request.getContinentDto().getContinent(),
                            request.getContinentDto().getCode(),
                            request.getContinentDto().getDescription(),
                            request.getContinentDto().getUseType()
                    );

            Optional<ContinentEntity> continent = continentRepository.findById(continentUpdate.get().getContinentSeq());

            Optional<ContinentDto> dtoPage =

                    continent.map(p -> ContinentDto.builder()
                            .continentSeq(p.getContinentSeq())
                            .continent(p.getContinent())
                            .continentCd(p.getContinentCd())
                            .code(p.getCode())
                            .description(p.getDescription())
                            .useType(p.getUseType())
                            .regUsr(p.getRegUsr())
                            .regDt(p.getRegDt())
                            .modUsr(p.getModUsr())
                            .modDt(p.getModDt())
                            .build()
                    );

            logger.info("dtoPage - {}", dtoPage);

            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
//            res.setResult(dtoPage);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));

            String menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_CONTINENTS.getCode());
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_MODIFY.getCode());

            //netive query
            String usrName = adminActiveLogService.findQryUsrName(continent.get().getModUsr());

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(continent.get().getModDt())
                    .adminId(continent.get().getModUsr())
                    .menuName(menuName)
                    .activeType("U")
                    .contentsId(continent.get().getContinentSeq())
                    .activeMsg(usrName + " " + activeMsg + " '" + continent.get().getContinent() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e){
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }

        return res;
    }

    @Transactional
    public Response<Void> deleteContent(Long id, HttpServletRequest req) {

        Response<Void> res = new Response<>();

        //id값이 없을 때
        if(id == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<ContinentEntity> continent = continentRepository.findById(id);

        logger.info("continent - {} ", continent);

        //조회내용 없을 때
        if(!continent.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        try {
            continentRepository.deleteById(id);
            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
//            res.setResult(dtoPage);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));
            Long adminId = getAccountId.ofId(httpServletRequest);

            String menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_CONTINENTS.getCode());
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_DELETE.getCode());

            //netive query
            String usrName = adminActiveLogService.findQryUsrName(adminId);

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(LocalDateTime.now())
                    .adminId(adminId)
                    .menuName(menuName)
                    .activeType("D")
                    .contentsId(continent.get().getContinentSeq())
                    .activeMsg(usrName + " " + activeMsg + " '" + continent.get().getContinent() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e) {
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }
        return res;
    }

    public List<ContinentExcelDto> getExcelList() {

        List<ContinentExcelDto> getExcelList = continentRepository.findExcelList();

        logger.info("getExcelList - {} ", getExcelList);

        return getExcelList;
    }


    //엑셀 다운로드 요청
    public Map<String, Object> downExcelList() throws Exception {
        // 데이터 가져오기
        List<ContinentExcelDto> excelList = this.getExcelList();

        logger.info("excelList - {} ", excelList);

        return ExcelWriter.createExcelData(excelList, ContinentExcelDto.class);
    }

    //마지막 코드 번호 확인
    public Integer getLastSeq(String code) {

        return continentRepository.getLastSeq(code);
    }

    //코드 번호 업데이트
    @Transactional
    public void updateLastSeq(String code, Integer k) {

        continentRepository.updateLastSeq(code, k);
    }

    @Transactional
    public Response<Void> saveExcelContent(ContinentDto continentDto, HttpServletRequest req) {

        Response<Void> res = new Response<>();

        //USEYN 값 없거나 틀릴 때
        if (continentDto.getUseType() == null || (continentDto.getUseType() != YnTypeEnum.Y && continentDto.getUseType() != YnTypeEnum.N)) {
            res.setResultCd(ResultCodeConst.NOT_VALID_USEYN.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if (continentDto.getContinent() == null) {
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        try {

            ContinentEntity newContinent = ContinentEntity.builder()
                    .continentSeq(continentDto.getContinentSeq())
                    .continent(continentDto.getContinent())
                    .code(continentDto.getCode())
                    .description(continentDto.getDescription())
                    .useType(continentDto.getUseType())
                    .build();

            ContinentEntity saved = continentRepository.save(newContinent);

            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
//            res.setResult(dtoPage);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));

            String menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_CONTINENTS.getCode());
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_CREATE.getCode());

            //netive query
            String usrName = adminActiveLogService.findQryUsrName(saved.getRegUsr());

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(saved.getRegDt())
                    .adminId(saved.getRegUsr())
                    .menuName(menuName)
                    .activeType("C")
                    .contentsId(saved.getContinentSeq())
                    .activeMsg(usrName + " " + activeMsg + " '" + saved.getContinent() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e) {
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }

        return res;
    }

}
