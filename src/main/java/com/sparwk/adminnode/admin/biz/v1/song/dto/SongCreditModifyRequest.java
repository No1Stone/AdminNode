package com.sparwk.adminnode.admin.biz.v1.song.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class SongCreditModifyRequest {

    @Schema(description = "song id 번호", nullable = false, example = "1")
    private Long songId;
    @Schema(description = "profile id 번호", nullable = false, example = "1")
    private Long profileId;
    @Schema(name = "rate share", nullable = false, example = "%")
    private String ipn;
    @Schema(name = "song lyrics Comment", nullable = false, example = "comment")
    private String nro;


    @Builder
    public SongCreditModifyRequest(Long songId,
                                   Long profileId,
                                   String ipn,
                                   String nro
    ) {

        this.songId = songId;
        this.profileId = profileId;
        this.ipn = ipn;
        this.nro = nro;
    }
}
