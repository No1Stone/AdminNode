package com.sparwk.adminnode.admin.biz.v1.song.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class SongMetadataDto {

    @Schema(description = "project id 번호", nullable = false, example = "1")
    private Long songId;
    @Schema(description = "부모코드", nullable = false, example = "SGG")
    private String attrTypeCd;
    @Schema(description = "자식코드", nullable = false, example = "SGG000001")
    private String attrDtlCd;
    @Schema(description = "자식코드값", nullable = true, example = "Blues")
    private String attrDtlCdVal;


    @Builder
    public SongMetadataDto(Long songId,
                           String attrTypeCd,
                           String attrDtlCd,
                           String attrDtlCdVal
    ) {
        this.songId = songId;
        this.attrTypeCd = attrTypeCd;
        this.attrDtlCd = attrDtlCd;
        this.attrDtlCdVal = attrDtlCdVal;
    }
}
