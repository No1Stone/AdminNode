package com.sparwk.adminnode.admin.biz.v1.language.dto;

import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Setter
@Getter
@NoArgsConstructor
public class LanguageDto {

    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long languageSeq;
    @Schema(description = "언어명", nullable = false, example = "English")
    private String language;
    @Schema(description = "유사언어", nullable = true, example = "Indo-European")
    private String languageFamily;
    @Schema(description = "현지어", nullable = false, example = "English")
    private String nativeLanguage;
    @Schema(description = "iso639_1 값", nullable = false, example = "en")
    private String iso639_1;
    @Schema(description = "iso639_2t 값", nullable = true, example = "eng")
    private String iso639_2t;
    @Schema(description = "iso639_2b 값", nullable = true, example = "eng")
    private String iso639_2b;
    @Schema(description = "iso639_3 값", nullable = true, example = "eng")
    private String iso639_3;
    @Schema(description = "코드 사용여부 Y/N", nullable = false, defaultValue = "Y", allowableValues = {"Y", "N"})
    private YnTypeEnum useType;
    @Schema(description = "언어코드 9자리", nullable = false, example = "LAN000001")
    private String languageCd;
    @Schema(description = "등록일", pattern = "yyMMdd hh:mm:ss", example = "2021-12-08T05:44:26.627991")
    private LocalDateTime regDt;
    @Schema(description = "등록자", nullable = false, example = "10000001")
    private Long regUsr;
    @Schema(description = "등록자 이름", nullable = false, example = "Yoon Jung Sun")
    private String regUsrName;
    @Schema(description = "수정일", pattern = "yyMMdd hh:mm:ss", example = "2021-12-08T05:44:26.627991")
    private LocalDateTime modDt;
    @Schema(description = "수정자", nullable = false, example = "10000001")
    private Long modUsr;
    @Schema(description = "수정자 이름", nullable = false, example = "Yoon Jung Sun")
    private String modUsrName;

    @Builder
    public LanguageDto(Long languageSeq,
                       String language,
                       String languageFamily,
                       String nativeLanguage,
                       String iso639_1,
                       String iso639_2t,
                       String iso639_2b,
                       String iso639_3,
                       YnTypeEnum useType,
                       String languageCd,
                       LocalDateTime regDt,
                       Long regUsr,
                       String regUsrName,
                       LocalDateTime modDt,
                       Long modUsr,
                       String modUsrName
    ) {
        this.languageSeq = languageSeq;
        this.language = language;
        this.languageFamily = languageFamily;
        this.nativeLanguage = nativeLanguage;
        this.iso639_1 = iso639_1;
        this.iso639_2t = iso639_2t;
        this.iso639_2b = iso639_2b;
        this.iso639_3 = iso639_3;
        this.useType = useType;
        this.languageCd = languageCd;
        this.regDt = regDt;
        this.regUsr = regUsr;
        this.regUsrName = regUsrName;
        this.modDt = modDt;
        this.modUsr = modUsr;
        this.modUsrName = modUsrName;
    }
}
