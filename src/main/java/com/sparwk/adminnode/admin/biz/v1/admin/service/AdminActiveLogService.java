package com.sparwk.adminnode.admin.biz.v1.admin.service;

import com.sparwk.adminnode.admin.biz.v1.CommonPagingListDto;
import com.sparwk.adminnode.admin.biz.v1.admin.dto.AdminActiveLogListDto;
import com.sparwk.adminnode.admin.config.common.Response;
import com.sparwk.adminnode.admin.config.common.ResultCodeConst;
import com.sparwk.adminnode.admin.jpa.entity.admin.AdminActiveCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.admin.AdminActiveLogEntity;
import com.sparwk.adminnode.admin.jpa.entity.admin.AdminActiveSorterEnum;
import com.sparwk.adminnode.admin.jpa.repository.admin.AdminActiveLogRepository;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class AdminActiveLogService {

    private final AdminActiveLogRepository adminActiveLogRepository;
    private final MessageSourceAccessor messageSourceAccessor;

    private final Logger logger = LoggerFactory.getLogger(AdminActiveLogService.class);

    public Response<CommonPagingListDto> getList(AdminActiveCateEnum cate,
                                                 String val,
                                                 String sdate,
                                                 String edate,
                                                 AdminActiveSorterEnum sorter,
                                                 PageRequest pageRequest) {

        Response<CommonPagingListDto> res = new Response<>();

        try{
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
            LocalDateTime searchStartDate = LocalDateTime.parse(sdate + " 00:00:00.000", formatter);
            LocalDateTime searchEndDate = LocalDateTime.parse(edate + " 23:59:59.999", formatter);
        } catch (Exception e){
            sdate = null;
            edate = null;
        }

        List<AdminActiveLogListDto> adminActiveLogList = adminActiveLogRepository.findQryAll(cate, val, sdate, edate, sorter, pageRequest);

        logger.info("adminActiveLogList - {}", adminActiveLogList);

        //조회내용 없을 때
        if(adminActiveLogList == null || adminActiveLogList.size() < 1){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        CommonPagingListDto list = new CommonPagingListDto();

        //총 카운트
        long totalSize = adminActiveLogRepository.countQryAll(cate, val, sdate, edate);
        long totalPage = totalSize / pageRequest.getPageSize();
        long mod = totalSize % pageRequest.getPageSize();
        if(mod > 0) {
            totalPage += 1;
        }

        list.setList(adminActiveLogList);
        list.setSize(pageRequest.getPageSize());
        list.setPage(pageRequest.getPageNumber());
        list.setTotalPage(totalPage);
        list.setTotalSize(totalSize);

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(list);
        return res;
    }


    @Transactional
    public Response<Void> saveContent(AdminActiveLogEntity request) {

        Response<Void> res = new Response<>();

        //Value 값 없을 때
        if(request.getAdminId() == null){
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if(request.getMenuName() == null){
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if(request.getContentsId() == null){
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        try {

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(request.getActiveTime())
                    .adminId(request.getAdminId())
                    .menuName(request.getMenuName())
                    .activeType(request.getActiveType())
                    .contentsId(request.getContentsId())
                    .activeMsg(request.getActiveMsg())
                    .build();

            logger.info("newLog - {}", newLog);

            AdminActiveLogEntity saved = adminActiveLogRepository.save(newLog);

            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
//            res.setResult(dtoPage);
            return res;

        } catch (Exception e){
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }

        return res;
    }

    public String findQryUsrName(Long usr){
        return adminActiveLogRepository.findQryUsrName(usr);
    }

}
