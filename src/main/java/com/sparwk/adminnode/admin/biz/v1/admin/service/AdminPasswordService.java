package com.sparwk.adminnode.admin.biz.v1.admin.service;

import com.sparwk.adminnode.admin.biz.v1.admin.dto.AdminDto;
import com.sparwk.adminnode.admin.biz.v1.admin.dto.AdminRequest;
import com.sparwk.adminnode.admin.config.common.Response;
import com.sparwk.adminnode.admin.config.common.ResultCodeConst;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.admin.AdminEntity;
import com.sparwk.adminnode.admin.jpa.entity.admin.AdminPasswordEntity;
import com.sparwk.adminnode.admin.jpa.entity.permissionSet.PermissionSetEntity;
import com.sparwk.adminnode.admin.jpa.repository.admin.AdminPasswordRepository;
import com.sparwk.adminnode.admin.jpa.repository.admin.AdminRepository;
import com.sparwk.adminnode.admin.jpa.repository.permissionSet.PermissionSetRepository;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class AdminPasswordService {

    private final AdminRepository adminRepository;
    private final AdminPasswordRepository adminPasswordRepository;
    private final PermissionSetRepository permissionSetRepository;

    private final MessageSourceAccessor messageSourceAccessor;

    private final Logger logger = LoggerFactory.getLogger(AdminPasswordService.class);

    @Transactional
    public Response<Void> passwordChange(Long id, AdminRequest adminRequest, HttpServletRequest req) {

        Response<Void> res = new Response<>();

        //id값이 없을 때
        if(id == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<AdminPasswordEntity> adminpassword = adminPasswordRepository.findById(id);

        //조회내용 없을 때
        if(!adminpassword.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //admin 잠김상태 확인용 조회
        Optional<AdminEntity> admin = adminRepository.findById(id);
        //조회내용 없을 때
        if(!adminpassword.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }
        //admin 잠김상태 풀림
        if(admin.get().getLockType() == YnTypeEnum.Y){
            admin.get().updateLockYn(YnTypeEnum.N);
        }

        //admin 기존 error 횟수 확인용 조회







        YnTypeEnum useType = admin.get().getUseType();
        YnTypeEnum newUseType = (useType == YnTypeEnum.Y) ? YnTypeEnum.N : YnTypeEnum.Y;

        logger.info("newUseType - {} ", newUseType);

        try {
            admin.get().updateYn(newUseType);
            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
//            res.setResult(dtoPage);
        } catch (Exception e) {
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }

        return res;
    }



    @Transactional
    public Response<Optional<AdminDto>> updateContent(Long id, AdminRequest request, HttpServletRequest req) {

        Response<Optional<AdminDto>> res = new Response<>();

        //id값이 없을 때
        if(id == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<PermissionSetEntity> permissionSetEntity =
                permissionSetRepository.findById(request.getPermissionAssignSeq());

        //부모코드 조회내용 없을 때
        if(!permissionSetEntity.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //USEYN 값 없거나 틀릴 때
        if(request.getUseType() == null || (request.getUseType() != YnTypeEnum.Y && request.getUseType() != YnTypeEnum.N)){
            res.setResultCd(ResultCodeConst.NOT_VALID_USEYN.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if(request.getAdminEmail() == null){
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if(request.getFullName() == null){
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if(request.getDial() == null){
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if(request.getPhoneNumber() == null){
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<AdminEntity> adminUpdate = adminRepository.findById(id);

        //조회내용 없을 때
        if(!adminUpdate.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        try {

        adminUpdate.get()
                .update(request.getAdminEmail(),
                        request.getFullName(),
                        request.getDescription(),
                        request.getUseType(),
                        request.getDial(),
                        request.getPermissionAssignSeq(),
                        request.getPhoneNumber(),
                        request.getLockType()
                );

        Optional<AdminEntity> admin = adminRepository.findById(adminUpdate.get().getAdminId());

        Optional<PermissionSetEntity> permissionSet = permissionSetRepository.findById(admin.get().getPermissionAssignSeq());

        Optional<AdminDto> dtoPage =

                admin.map(p -> AdminDto.builder()
                        .adminId(p.getAdminId())
                        .adminEmail(p.getAdminEmail())
                        .fullName(p.getFullName())
                        .dial(p.getDial())
                        .phoneNumber(p.getPhoneNumber())
                        .permissionAssignSeq(p.getPermissionAssignSeq())
                        .permissionAssignName(permissionSet.get().getLabelVal())
                        .description(p.getDescription())
                        .useType(p.getUseType())
                        .lockType(p.getLockType())
                        .regUsr(p.getRegUsr())
                        .regDt(p.getRegDt())
                        .modUsr(p.getModUsr())
                        .modDt(p.getModDt())
                        .build()
                );

            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            res.setResult(dtoPage);

        } catch (Exception e){
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }

        return res;
    }

    @Transactional
    public Response<Void> deleteContent(Long id, HttpServletRequest req) {

        Response<Void> res = new Response<>();

        //id값이 없을 때
        if(id == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<AdminEntity> admin = adminRepository.findById(id);

        //조회내용 없을 때
        if(!admin.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        try {
            adminRepository.deleteById(id);
            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
//            res.setResult(dtoPage);
        } catch (Exception e) {
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }
        return res;
    }

}
