package com.sparwk.adminnode.admin.biz.v1.role.dto;

import com.sparwk.adminnode.admin.jpa.entity.FormatTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Setter
@Getter
@NoArgsConstructor
public class RoleDetailCodeDto {

    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long roleDetailCodeSeq;
    @Schema(description = "부모코드 3자리", nullable = false, example = "DEX")
    private String pcode;
    @Schema(description = "부모코드 값", nullable = true, example = "DDEX")
    private String pcodeVal;
    @Schema(description = "코드 9자리", nullable = false, example = "DEX000001")
    private String dcode;
    @Schema(description = "코드 값", nullable = false, example = "Artist")
    private String role;
    @Schema(description = "포맷 여부 DDEX / SPARWK", nullable = false, defaultValue = "DDEX", allowableValues = {"DDEX", "SPARWK"})
    private FormatTypeEnum formatType;
    @Schema(description = "코드 사용여부 Y/N", nullable = false, defaultValue = "Y", allowableValues = {"Y", "N"})
    private YnTypeEnum useType;
    @Schema(description = "popular 사용 Y/N", nullable = false, defaultValue = "Y", allowableValues = {"Y", "N"})
    private YnTypeEnum popularType;
    @Schema(description = "Matching Role 사용 Y/N", nullable = false, defaultValue = "N", allowableValues = {"Y", "N"})
    private YnTypeEnum matchingRoleYn;
    @Schema(description = "Credit Role 사용 Y/N", nullable = false, defaultValue = "N", allowableValues = {"Y", "N"})
    private YnTypeEnum creditRoleYn;
    @Schema(description = "Split Sheet Role 사용 Y/N", nullable = false, defaultValue = "N", allowableValues = {"Y", "N"})
    private YnTypeEnum splitSheetRoleYn;
    @Schema(description = "약어 값", nullable = true, example = "약어")
    private String abbeviation;
    @Schema(description = "자체사용코드", nullable = true, example = "")
    private String customCode;
    @Schema(description = "description 설명", nullable = true, example = "설명")
    private String description;
    @Schema(description = "코드 순번 숫자",  nullable = false, example = "1")
    private Long hit;
    @Schema(description = "등록일", pattern = "yyMMdd hh:mm:ss", example = "2021-12-08T05:44:26.627991")
    private LocalDateTime regDt;
    @Schema(description = "등록자", nullable = false, example = "10000001")
    private Long regUsr;
    @Schema(description = "등록자 이름", nullable = false, example = "Yoon Jung Sun")
    private String regUsrName;
    @Schema(description = "수정일", pattern = "yyMMdd hh:mm:ss", example = "2021-12-08T05:44:26.627991")
    private LocalDateTime modDt;
    @Schema(description = "수정자", nullable = false, example = "10000001")
    private Long modUsr;
    @Schema(description = "수정자 이름", nullable = false, example = "Yoon Jung Sun")
    private String modUsrName;

    @Builder
    public RoleDetailCodeDto(Long roleDetailCodeSeq,
                             String pcode,
                             String pcodeVal,
                             String dcode,
                             String role,
                             FormatTypeEnum formatType,
                             YnTypeEnum useType,
                             YnTypeEnum popularType,
                             YnTypeEnum matchingRoleYn,
                             YnTypeEnum creditRoleYn,
                             YnTypeEnum splitSheetRoleYn,
                             String abbeviation,
                             String customCode,
                             String description,
                             Long hit,
                             //int sortIndex,
                             LocalDateTime regDt,
                             Long regUsr,
                             String regUsrName,
                             LocalDateTime modDt,
                             Long modUsr,
                             String modUsrName
    ) {
        this.roleDetailCodeSeq = roleDetailCodeSeq;
        this.pcode = pcode;
        this.pcodeVal = pcodeVal;
        this.dcode = dcode;
        this.role = role;
        this.formatType = formatType;
        this.useType = useType;
        this.popularType = popularType;
        this.matchingRoleYn = matchingRoleYn;
        this.creditRoleYn = creditRoleYn;
        this.splitSheetRoleYn = splitSheetRoleYn;
        this.abbeviation = abbeviation;
        this.customCode = customCode;
        this.description = description;
        this.hit = hit;
        //this.sortIndex = sortIndex;
        this.regDt = regDt;
        this.regUsr = regUsr;
        this.regUsrName = regUsrName;
        this.modDt = modDt;
        this.modUsr = modUsr;
        this.modUsrName = modUsrName;
    }
}
