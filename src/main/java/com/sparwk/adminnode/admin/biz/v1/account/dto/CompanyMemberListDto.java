package com.sparwk.adminnode.admin.biz.v1.account.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

@Setter
@Getter
@NoArgsConstructor
public class CompanyMemberListDto {

    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long accntId;
    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long profileId;
    @Schema(description = "COMPANY NAME", nullable = false, example = "Spawrk")
    private String companyName;
    @Schema(description = "COMPANY TYPE NAME", nullable = false, example = "Music publisher")
    private String companyTypeName;
    @Schema(description = "Account E-mail", nullable = true, example = "info@ekkomusicrights.com")
    private String companyEmail;
    @Schema(description = "AccountCompanyTypeDto", nullable = true)
    private List<AccountCompanyTypeDto> AccountCompanyTypeDtoList;
    @Schema(description = "IPI_NUMBER", nullable = true, example = "W1234567890")
    private String ipiNumber;
    @Schema(description = "VAT_NUMBER", nullable = true, example = "W1234567890")
    private String vatNumber;
    @Schema(description = "등록일", pattern = "yyMMdd hh:mm:ss", example = "2021-12-08T05:44:26.627991")
    private LocalDateTime regDt;


    @Builder
    public CompanyMemberListDto(Long accntId,
                                Long profileId,
                                String companyName,
                                String companyTypeName,
                                String companyEmail,
                                List<AccountCompanyTypeDto> AccountCompanyTypeDtoList,
                                String ipiNumber,
                                String vatNumber,
                                LocalDateTime regDt
    ) {
        this.accntId = accntId;
        this.profileId = profileId;
        this.companyName = companyName;
        this.companyTypeName = companyTypeName;
        this.companyEmail = companyEmail;
        this.AccountCompanyTypeDtoList = AccountCompanyTypeDtoList;
        this.ipiNumber = ipiNumber;
        this.vatNumber = vatNumber;
        this.regDt = regDt;
    }
}
