package com.sparwk.adminnode.admin.biz.v1.account.dto;

import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
@NoArgsConstructor
public class CompanyOfficeDetailDto {

    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long accntId;
    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long profileId;
    @Schema(description = "Account E-mail", nullable = false, example = "info@ekkomusicrights.com")
    private String companyEmail;
    @Schema(description = "AccountCompanyTypeDto", nullable = true)
    private List<AccountCompanyTypeDto> accountCompanyTypeDtoList;
    @Schema(description = "Company Type VERIFY YN", nullable = false, defaultValue = "Y", allowableValues = {"Y", "N"})
    private YnTypeEnum accountCompanyTypeVerifyYn;
    @Schema(description = "Company Type File Url", nullable = false, example = "http://...")
    private String accountCompanyTypeFileUrl;
    @Schema(description = "Company Type File", nullable = false, example = "filename.pdf")
    private String accountCompanyTypeFile;
    @Schema(description = "COMPANY NAME", nullable = false, example = "Spawrk")
    private String companyName;
    @Schema(description = "AccountCompanyTypeDto", nullable = true)
    private List<AccountCompanyLocationDto> accountCompanyLocationDtoList;
    @Schema(description = "Zip/Postal code", nullable = true, example = "04526")
    private String companyPost;
    @Schema(description = "COMPANY REGION", nullable = true, example = "Seoul")
    private String companyRegion;
    @Schema(description = "COMPANY CITY/TOWN", nullable = true, example = "Cheongdam-dong, Gangna")
    private String companyCity;
    @Schema(description = "COMPANY ADDR1", nullable = true, example = "114 Seolleung-ro 190-gil")
    private String companyAddr1;
    @Schema(description = "COMPANY ADDR2", nullable = true, example = "SM Entertainment Studio Center")
    private String companyAddr2;
    @Schema(description = "CONTCT_PHONE_COUNTRY_CD", nullable = false, example = "CNT000001")
    private String countryCd;
    @Schema(description = "CONTCT_PHONE_COUNTRY_CD_NAME", nullable = true, example = "USA")
    private String countryCdName;
    @Schema(description = "CONTCT_PHONE_NUMBER", nullable = true, example = "0821000001111")
    private String contctPhoneNumber;
    @Schema(description = "CONTCT_FIRST_NAME", nullable = true, example = "YOON")
    private String contctFirstName;
    @Schema(description = "CONTCT_MIDDLE_NAME", nullable = true, example = "JUNG")
    private String contctMiddleName;
    @Schema(description = "CONTCT_LAST_NAME", nullable = false, example = "HUN")
    private String contctLastName;
    @Schema(description = "CONTCT_Email", nullable = false, example = "yoon@email")
    private String contctEmail;
    @Schema(description = "HeadLine", nullable = true, example = "Yoon's")
    private String headline;
    @Schema(description = "ipiInfo", nullable = true, example = "10000000000")
    private String ipiNumber;
    @Schema(description = "IPI VERIFY YN", nullable = false, example = "N")
    private YnTypeEnum ipiNumberVarifyYn;
    @Schema(description = "vatNumber", nullable = true, example = "10000000000")
    private String vatNumber;

    @Builder
    public CompanyOfficeDetailDto(Long accntId,
                                  Long profileId,
                                  String companyEmail,
                                  List<AccountCompanyTypeDto> accountCompanyTypeDtoList,
                                  YnTypeEnum accountCompanyTypeVerifyYn,
                                  String companyName,
                                  String accountCompanyTypeFileUrl,
                                  String accountCompanyTypeFile,
                                  List<AccountCompanyLocationDto> accountCompanyLocationDtoList,
                                  String companyPost,
                                  String companyRegion,
                                  String companyCity,
                                  String companyAddr1,
                                  String companyAddr2,
                                  String countryCd,
                                  String countryCdName,
                                  String contctPhoneNumber,
                                  String contctFirstName,
                                  String contctMiddleName,
                                  String contctLastName,
                                  String contctEmail,
                                  String headline,
                                  String ipiNumber,
                                  YnTypeEnum ipiNumberVarifyYn,
                                  String vatNumber
    ) {
        this.accntId = accntId;
        this.profileId = profileId;
        this.companyEmail = companyEmail;
        this.accountCompanyTypeDtoList = accountCompanyTypeDtoList;
        this.accountCompanyTypeVerifyYn = accountCompanyTypeVerifyYn;
        this.accountCompanyTypeFileUrl = accountCompanyTypeFileUrl;
        this.accountCompanyTypeFile = accountCompanyTypeFile;
        this.companyName = companyName;
        this.accountCompanyLocationDtoList = accountCompanyLocationDtoList;
        this.companyPost = companyPost;
        this.companyRegion = companyRegion;
        this.companyCity = companyCity;
        this.companyAddr1 = companyAddr1;
        this.companyAddr2 = companyAddr2;
        this.countryCd = countryCd;
        this.countryCdName = countryCdName;
        this.contctPhoneNumber = contctPhoneNumber;
        this.contctFirstName = contctFirstName;
        this.contctMiddleName = contctMiddleName;
        this.contctLastName = contctLastName;
        this.contctEmail = contctEmail;
        this.headline = headline;
        this.ipiNumber = ipiNumber;
        this.ipiNumberVarifyYn = ipiNumberVarifyYn;
        this.vatNumber = vatNumber;
    }
}
