package com.sparwk.adminnode.admin.biz.v1.test.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

@Setter
@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class AdminEmailSendRequest {

    @Schema(description = "상대방 Email")
    private String[] receiver;
    @Schema(description = "질문자 이름")
    private String profileFullName;
    @Schema(description = "질문자 나라 이름")
    private String currentCityCountryCdName;
    @Schema(description = "질문자 도시 이름")
    private String currentCityNm;
    @Schema(description = "프로필 사진 URL")
    private String profileImgUrl;
    @Schema(description = "질문 카테고리")
    private String category;
    @Schema(description = "제목")
    private String subject;
    @Schema(description = "질문내용")
    private String questionContent;
    @Schema(description = "답변내용")
    private String answerContent;
    @Schema(description = "내용")
    private String contents;
}
