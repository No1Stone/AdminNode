package com.sparwk.adminnode.admin.biz.v1.account.dto;

import com.sparwk.adminnode.admin.biz.v1.profile.dto.ProfileMetaDataDto;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

@Setter
@Getter
@NoArgsConstructor
public class AssoiateMemberListDto {

    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long accntId;
    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long profileId;
    @Schema(description = "ACCNT_TYPE_CD", nullable = false, example = "COMPANY / GROUP / PERSON")
    private String accntTypeCd;
    @Schema(description = "ACCNT EMAIL", nullable = false, example = "sparwk@sparwk.com")
    private String accntEmail;
    @Schema(description = "PASSPORT_FIRST_NAME", nullable = false, example = "Yoon")
    private String passportFirstName;
    @Schema(description = "PASSPORT_Middle_NAME", nullable = true, example = "Jung")
    private String passportMiddleName;
    @Schema(description = "PASSPORT_LAST_NAME", nullable = false, example = "Hun")
    private String passportLastName;
    @Schema(description = "ACCNT FULL NAME", nullable = false, example = "Mr.Yoon")
    private String profileFullName;
    @Schema(description = "STAGE_NAME_YN", nullable = false, defaultValue = "N", allowableValues = {"Y", "N"})
    private YnTypeEnum stageNameYn;
    @Schema(description = "Year a Birth", nullable = true, example = "2001")
    private String bthYear;
    @Schema(description = "Month a Birth", nullable = true, example = "1")
    private String bthMonth;
    @Schema(description = "Date a Birth", nullable = true, example = "1")
    private String bthDay;
    @Schema(description = "Gender String", nullable = true, example = "Femail")
    private String gender;
    @Schema(description = "Gender", nullable = false)
    private List<ProfileMetaDataDto> genderList;
    @Schema(description = "등록일", pattern = "yyMMdd hh:mm:ss", example = "2021-12-08T05:44:26.627991")
    private LocalDateTime regDt;

    @Schema(description = "verifyYn", nullable = false, defaultValue = "Y", allowableValues = {"Y", "N"})
    private YnTypeEnum verifyYn;


    @Builder
    public AssoiateMemberListDto(Long accntId,
                                 Long profileId,
                                 String accntTypeCd,
                                 String accntEmail,
                                 String passportFirstName,
                                 String passportMiddleName,
                                 String passportLastName,
                                 String profileFullName,
                                 YnTypeEnum stageNameYn,
                                 String bthDay,
                                 List<ProfileMetaDataDto> genderList,
                                 LocalDateTime regDt,
                                 YnTypeEnum verifyYn
    ) {
        this.accntId=accntId;
        this.profileId=profileId;
        this.accntTypeCd=accntTypeCd;
        this.accntEmail=accntEmail;
        this.passportFirstName=passportFirstName;
        this.passportMiddleName=passportMiddleName;
        this.passportLastName=passportLastName;
        this.profileFullName=profileFullName;
        this.stageNameYn=stageNameYn;
        this.bthDay=bthDay;
        this.genderList=genderList;
        this.regDt=regDt;
        this.verifyYn = verifyYn;

    }
}
