package com.sparwk.adminnode.admin.biz.v1.role.dto;

import lombok.*;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class RoleDetailCodeRequest {

//    @Schema(description = "부모코드 3자리", nullable = false, example = "DEX")
//    @NotBlank(message = "Please enter a code.")
//    private String pcode;
//    @Schema(description = "코드 9자리", nullable = false, example = "DEX000001")
//    @NotBlank(message = "Please enter a code.")
//    private String dcode;
//    @Schema(description = "코드 값", nullable = false, example = "Artist")
//    @NotBlank(message = "Please enter a value.")
//    private String role;
//    @Schema(description = "포맷 여부 DDEX / SPARWK", defaultValue = "DDEX", allowableValues = {"DDEX", "SPARWK"})
//    @NotNull(message = "Please format use.")
//    private Format format;
//    @Schema(description = "코드 사용여부 Y/N", defaultValue = "Y", allowableValues = {"Y", "N"})
//    @NotNull(message = "Please check use.")
//    private UseType useType;
//    @Schema(description = "popular 사용 Y/N", defaultValue = "Y", allowableValues = {"Y", "N"})
//    @NotNull(message = "Please check popular")
//    private YnEnumType popularType;
//    @Schema(description = "약어 값", nullable = true, example = "약어")
//    private String abbeviation;
//    @Schema(description = "자체사용코드", nullable = true, example = "")
//    private String customCode;
//    @Schema(description = "description 설명", nullable = true, example = "설명")
//    private String description;
//
//
//
//    @Builder
//    public RoleDetailCodeRequest(String pcode,
//                                 String dcode,
//                                 String role,
//                                 Format format,
//                                 YnEnumType popularType,
//                                 String abbeviation,
//                                 String customCode,
//                                 UseType useType,
//                                 String description
//    ) {
//        this.pcode = pcode;
//        this.dcode = dcode;
//        this.role = role;
//        this.format = format;
//        this.abbeviation = abbeviation;
//        this.customCode = customCode;
//        this.useType = useType;
//        this.description = description;
//        this.popularType = popularType;
//    }

    private RoleDetailCodeDto roleDetailCodeDto;
}
