package com.sparwk.adminnode.admin.biz.v1.sns.service;

import com.sparwk.adminnode.admin.biz.v1.CommonPagingListDto;
import com.sparwk.adminnode.admin.biz.v1.admin.service.AdminActiveLogService;
import com.sparwk.adminnode.admin.biz.v1.sns.dto.SnsDto;
import com.sparwk.adminnode.admin.biz.v1.sns.dto.SnsRequest;
import com.sparwk.adminnode.admin.config.common.ActiveCodeConst;
import com.sparwk.adminnode.admin.config.common.GetAccountId;
import com.sparwk.adminnode.admin.config.common.Response;
import com.sparwk.adminnode.admin.config.common.ResultCodeConst;
import com.sparwk.adminnode.admin.jpa.entity.PeriodTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.admin.AdminActiveLogEntity;
import com.sparwk.adminnode.admin.jpa.entity.sns.SnsCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.sns.SnsEntity;
import com.sparwk.adminnode.admin.jpa.entity.sns.SnsSorterEnum;
import com.sparwk.adminnode.admin.jpa.repository.sns.SnsRepository;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class SnsService {

    private final SnsRepository snsRepository;
    private final AdminActiveLogService adminActiveLogService;

    private final HttpServletRequest httpServletRequest;
    private final MessageSourceAccessor messageSourceAccessor;

    private final Logger logger = LoggerFactory.getLogger(SnsService.class);


    public Response<CommonPagingListDto> getList(SnsCateEnum cate,
                                                 String val,
                                                 YnTypeEnum useType,
                                                 PeriodTypeEnum periodType,
                                                 String sdate,
                                                 String edate,
                                                 SnsSorterEnum sorter,
                                                 PageRequest pageRequest) {

        Response<CommonPagingListDto> res = new Response<>();

        List<SnsDto> snsList =
                snsRepository.findQryAll(cate, val, useType, periodType, sdate, edate, sorter, pageRequest);

        logger.info("snsList - {}", snsList);

        //조회내용 없을 때
        if(snsList == null || snsList.size() < 1){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        CommonPagingListDto list = new CommonPagingListDto();

        //총 카운트
        long totalSize = snsRepository.countQryAll(cate, val, useType, periodType, sdate, edate);
        long totalPage = totalSize / pageRequest.getPageSize();
        long mod = totalSize % pageRequest.getPageSize();
        if(mod > 0) {
            totalPage += 1;
        }

        list.setList(snsList);
        list.setSize(pageRequest.getPageSize());
        list.setPage(pageRequest.getPageNumber());
        list.setTotalPage(totalPage);
        list.setTotalSize(totalSize);

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(list);
        return res;
    }

    public Response<List<SnsDto>> getUseYList(SnsCateEnum cate, String val) {

        Response<List<SnsDto>> res = new Response<>();

        List<SnsDto> snsList = snsRepository.findQryUseY(cate, val);

        logger.info("snsList - {}", snsList);

        //조회내용 없을 때
        if(snsList == null || snsList.size() < 1){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(snsList);
        return res;
    }

    public Response<Optional<SnsDto>> getOne(Long id) {

        Response<Optional<SnsDto>> res = new Response<>();

        //id값이 없을 때
        if(id == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<SnsEntity> sns = snsRepository.findById(id);

        logger.info("sns - {}", sns);

        //조회내용 없을 때
        if(!sns.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<SnsDto> dtoPage =

                sns.map(p -> SnsDto.builder()
                        .snsSeq(p.getSnsSeq())
                        .snsName(p.getSnsName())
                        .snsCd(p.getSnsCd())
                        .snsIconUrl(p.getSnsIconUrl())
                        .useType(p.getUseType())
                        .regUsr(p.getRegUsr())
                        .regDt(p.getRegDt())
                        .modUsr(p.getModUsr())
                        .modDt(p.getModDt())
                        .build()
                );

        logger.info("Sns - {}", dtoPage);

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(dtoPage);
        return res;
    }

    @Transactional
    public Response<Void> saveContent(SnsRequest request, HttpServletRequest req) {

        Response<Void> res = new Response<>();
        String snsCd;

        //USEYN 값 없거나 틀릴 때
        if(request.getSnsDto().getUseType() == null || (request.getSnsDto().getUseType() != YnTypeEnum.Y && request.getSnsDto().getUseType() != YnTypeEnum.N)){
            res.setResultCd(ResultCodeConst.NOT_VALID_USEYN.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if(request.getSnsDto().getSnsName() == null){
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        try {

            //seq 번호 불러옴
            int k = snsRepository.getLastSeq("SNS") + 1;

            if(k < 10)
                snsCd = "SNS00000" + k;
            else if(k < 100)
                snsCd = "SNS0000" + k;
            else if(k < 1000)
                snsCd = "SNS000" + k;
            else if(k < 10000)
                snsCd = "SNS00" + k;
            else if(k < 100000)
                snsCd = "SNS0" + k;
            else if(k < 1000000)
                snsCd = "SNS" + k;
            else
                snsCd = "SNS" + k;

            SnsEntity newSns = SnsEntity.builder()
                    .snsName(request.getSnsDto().getSnsName())
                    .snsCd(snsCd)
                    .snsIconUrl(request.getSnsDto().getSnsIconUrl())
                    .useType(request.getSnsDto().getUseType())
                    .build();

            SnsEntity saved = snsRepository.save(newSns);

            //seq 번호 수동으로 업데이트 시킨다
            snsRepository.updateLastSeq("SNS", k);



//            Optional<SnsEntity> sns = snsRepository.findById(saved.getSnsSeq());
//
//            Optional<SnsDto> dtoPage =
//
//                    sns.map(p -> SnsDto.builder()
//                            .snsSeq(p.getSnsSeq())
//                            .sns(p.getSns())
//                            .snsCd(p.getSnsCd())
//                            .commonDetailCodeCode(p.getContinentCode())
//                            .commonDetailCodeCodeVal(p.getContinentEntity().getContinent())
//                            .iso2(p.getIso2())
//                            .iso3(p.getIso3())
//                            .description(p.getDescription())
//                            .dial(p.getDial())
//                            .nmr(p.getNmr())
//                            .useType(p.getUseType())
//                            .regUsr(p.getRegUsr())
//                            .regDt(p.getRegDt())
//                            .modUsr(p.getModUsr())
//                            .modDt(p.getModDt())
//                            .build()
//                    );
//
//            return dtoPage;
            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
//            res.setResult(dtoPage);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));

            String menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_SNS_MEDIA.getCode());
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_CREATE.getCode());

            //netive query
            String usrName = adminActiveLogService.findQryUsrName(saved.getRegUsr());

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(saved.getRegDt())
                    .adminId(saved.getRegUsr())
                    .menuName(menuName)
                    .activeType("C")
                    .contentsId(saved.getSnsSeq())
                    .activeMsg(usrName + " " + activeMsg + " '" + saved.getSnsName() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e){
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }

        return res;
    }

    @Transactional
    public Response<Void> updateYn(Long id, HttpServletRequest req) {

        Response<Void> res = new Response<>();

        //id값이 없을 때
        if(id == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<SnsEntity> sns = snsRepository.findById(id);


        //조회내용 없을 때
        if(!sns.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        YnTypeEnum useType = sns.get().getUseType();
        YnTypeEnum newUseType = (useType == YnTypeEnum.Y) ? YnTypeEnum.N : YnTypeEnum.Y;

        logger.info("newUseType - {} ", newUseType);

        try {
            sns.get().updateYn(newUseType);
            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
//            res.setResult(dtoPage);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));

            String menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_SNS_MEDIA.getCode());
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_MODIFY.getCode());

            //netive query
            String usrName = adminActiveLogService.findQryUsrName(sns.get().getModUsr());

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(sns.get().getModDt())
                    .adminId(sns.get().getModUsr())
                    .menuName(menuName)
                    .activeType("U")
                    .contentsId(sns.get().getSnsSeq())
                    .activeMsg(usrName + " " + activeMsg + " '" + sns.get().getSnsName() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e) {
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }

        return res;
    }

    @Transactional
    public Response<Optional<SnsDto>> updateContent(Long id, SnsRequest request, HttpServletRequest req) {

        Response<Optional<SnsDto>> res = new Response<>();

        //id값이 없을 때
        if(id == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<SnsEntity> snsUpdate = snsRepository.findById(id);

        //조회내용 없을 때
        if(!snsUpdate.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //USEYN 값 없거나 틀릴 때
        if(request.getSnsDto().getUseType() == null || (request.getSnsDto().getUseType() != YnTypeEnum.Y && request.getSnsDto().getUseType() != YnTypeEnum.N)){
            res.setResultCd(ResultCodeConst.NOT_VALID_USEYN.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if(request.getSnsDto().getSnsName() == null){
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        try {

            snsUpdate.get()
                    .update(request.getSnsDto().getSnsName(),
                            request.getSnsDto().getSnsIconUrl(),
                            request.getSnsDto().getUseType()
                    );

            Optional<SnsEntity> sns = snsRepository.findById(snsUpdate.get().getSnsSeq());

            Optional<SnsDto> dtoPage =

                    sns.map(p -> SnsDto.builder()
                            .snsSeq(p.getSnsSeq())
                            .snsCd(p.getSnsCd())
                            .snsName(p.getSnsName())
                            .snsIconUrl(p.getSnsIconUrl())
                            .useType(p.getUseType())
                            .regUsr(p.getRegUsr())
                            .regDt(p.getRegDt())
                            .modUsr(p.getModUsr())
                            .modDt(p.getModDt())
                            .build()
                    );

            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            res.setResult(dtoPage);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));

            String menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_SNS_MEDIA.getCode());
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_MODIFY.getCode());

            //netive query
            String usrName = adminActiveLogService.findQryUsrName(sns.get().getModUsr());

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(sns.get().getModDt())
                    .adminId(sns.get().getModUsr())
                    .menuName(menuName)
                    .activeType("U")
                    .contentsId(sns.get().getSnsSeq())
                    .activeMsg(usrName + " " + activeMsg + " '" + sns.get().getSnsName() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e){
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }

        return res;
    }

    @Transactional
    public Response<Void> deleteContent(Long id, HttpServletRequest req) {

        Response<Void> res = new Response<>();

        //id값이 없을 때
        if(id == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<SnsEntity> sns = snsRepository.findById(id);

        //조회내용 없을 때
        if(!sns.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        try {
            snsRepository.deleteById(id);
            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
//            res.setResult(dtoPage);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));

            String menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_SNS_MEDIA.getCode());
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_DELETE.getCode());
            Long adminId = getAccountId.ofId(httpServletRequest);

            //netive query
            String usrName = adminActiveLogService.findQryUsrName(adminId);

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(LocalDateTime.now())
                    .adminId(adminId)
                    .menuName(menuName)
                    .activeType("D")
                    .contentsId(sns.get().getSnsSeq())
                    .activeMsg(usrName + " " + activeMsg + " '" + sns.get().getSnsName() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e) {
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }
        return res;
    }

}
