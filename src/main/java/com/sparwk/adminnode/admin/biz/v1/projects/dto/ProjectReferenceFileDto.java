package com.sparwk.adminnode.admin.biz.v1.projects.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class ProjectReferenceFileDto {

    @Schema(description = "project id 번호", nullable = false, example = "1")
    private Long projId;
    @Schema(description = "순번", nullable = false, example = "1")
    private long fileFefSeq;
    @Schema(description = "첨부파일타입코드", nullable = false, example = "UPL000001")
    private String uploadTypeCd;
    @Schema(description = "첨부파일타입코드명", nullable = false, example = "MP3")
    private String uploadTypeCdName;
    @Schema(description = "첨부파일명", nullable = false, example = "MP3")
    private String fileName;
    @Schema(description = "첨부파일경로", nullable = false, example = "http://...")
    private String filePath;
    @Schema(description = "첨부파일크기", nullable = false, example = "180")
    private int fileSize;
    @Schema(description = "첨부파일참조URL", nullable = false, example = "http://...")
    private String refUrl;

    @Builder
    public ProjectReferenceFileDto(Long projId,
                                   long fileFefSeq,
                                   String uploadTypeCd,
                                   String uploadTypeCdName,
                                   String fileName,
                                   String filePath,
                                   int fileSize,
                                   String refUrl

    ) {
        this.projId = projId;
        this.fileFefSeq = fileFefSeq;
        this.uploadTypeCd = uploadTypeCd;
        this.uploadTypeCdName = uploadTypeCdName;
        this.fileName = fileName;
        this.filePath = filePath;
        this.fileSize = fileSize;
        this.refUrl = refUrl;
    }
}
