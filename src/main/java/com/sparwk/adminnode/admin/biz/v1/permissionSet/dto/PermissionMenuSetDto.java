package com.sparwk.adminnode.admin.biz.v1.permissionSet.dto;

import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Setter
@Getter
@NoArgsConstructor
public class PermissionMenuSetDto {

    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long menuId;
    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long adminPermissionId;
    @Schema(description = "메뉴명", nullable = false, example = "SECURITY/IDENTITY")
    private String menuLabel;
    @Schema(description = "메뉴 뎁스, 레벨", nullable = false, example = "1")
    int menuDepth;
    @Schema(description = "부모Seq", nullable = true, example = "1L, 없으면 null 또는 0")
    private Long uppMenuSeq;
    @Schema(description = "메뉴 순서", nullable = false, example = "1")
    int sortIndex;
    @Schema(description = "데이터 등록권한여부 Y/N", nullable = false, example = "Y")
    private String createYn;
    @Schema(description = "데이터 조회권한여부 Y/N", nullable = false, example = "Y")
    private String readYn;
    @Schema(description = "데이터 수정권한여부 Y/N", nullable = false, example = "Y")
    private String editYn;
    @Schema(description = "데이터 삭제권한여부 Y/N", nullable = false, example = "N")
    private String deleteYn;
    @Schema(description = "등록일", pattern = "yyMMdd hh:mm:ss", example = "2021-12-08T05:44:26.627991")
    private LocalDateTime regDt;
    @Schema(description = "등록자", nullable = false, example = "10000001")
    private Long regUsr;
    @Schema(description = "수정일", pattern = "yyMMdd hh:mm:ss", example = "2021-12-08T05:44:26.627991")
    private LocalDateTime modDt;
    @Schema(description = "수정자", nullable = false, example = "10000001")
    private Long modUsr;

    @Builder
    public PermissionMenuSetDto(Long menuId,
                                Long adminPermissionId,
                                String menuLabel,
                                int menuDepth,
                                Long uppMenuSeq,
                                int sortIndex,
                                String createYn,
                                String readYn,
                                String  editYn,
                                String deleteYn,
                                LocalDateTime regDt,
                                Long regUsr,
                                LocalDateTime modDt,
                                Long modUsr
    ) {
        this.menuId = menuId;
        this.adminPermissionId = adminPermissionId;
        this.menuLabel = menuLabel;
        this.menuDepth = menuDepth;
        this.uppMenuSeq = uppMenuSeq;
        this.sortIndex = sortIndex;
        this.createYn = createYn;
        this.readYn = readYn;
        this.editYn = editYn;
        this.deleteYn = deleteYn;
        this.regDt = regDt;
        this.regUsr = regUsr;
        this.modDt = modDt;
        this.modUsr = modUsr;
    }

}
