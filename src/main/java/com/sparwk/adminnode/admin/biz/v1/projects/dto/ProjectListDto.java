package com.sparwk.adminnode.admin.biz.v1.projects.dto;

import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Setter
@Getter
@NoArgsConstructor
public class ProjectListDto {

    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long projId;
    @Schema(description = "PROJECT TITLE", nullable = false, example = "PROJ_TITLE")
    private String projectTitle;
    @Schema(description = "AVATAR_FILE_URL", nullable = false, example = "http://")
    private String projectAvatarUrl;
    @Schema(description = "PROJECT TITLE TYPE", nullable = false, example = "Collaboration / Cowriter")
    private String projectType;
    @Schema(description = "PROJECT TITLE TYPE NAME", nullable = false, example = "Collaboration / Cowriter")
    private String projectTypeName;
    @Schema(description = "PROJECT OWNER", nullable = false, example = "OWNER PROFILE ID")
    private Long projOwner;
    @Schema(description = "PROJECT OWNER NAME", nullable = false, example = "OWNER PROFILE ID")
    private String projOwnerName;
    @Schema(description = "PROJECT INVITING COMPANY TYPE", nullable = false, example = "IND000001 / IND000002")
    private String projIndivCompType;
    @Schema(description = "PROJECT INVITING COMPANY TYPE Name", nullable = true, example = "Individual / Company")
    private String projIndivCompTypeName;

    @Schema(description = "BUDGET", nullable = false, example = "분배 PJC000001/PJC000002/PJC000003")
    private String budget;
    @Schema(description = "BUDGET NAME", nullable = false, example = "분배")
    private String budgetName;
    @Schema(description = "PROJECT COMPLETE YN", nullable = false, defaultValue = "Y", allowableValues = {"Y", "N"})
    private YnTypeEnum completeYn;
    @Schema(description = "PROJ DDL DATE", nullable = false, example = "2022-01-01")
    private LocalDate projDdlDt;
    @Schema(description = "등록일", pattern = "yyMMdd hh:mm:ss", example = "2021-12-08T05:44:26.627991")
    private LocalDateTime regDt;

    @Schema(description = "project Music Genre", nullable = true)
    private List<ProjectMetadataDto> projectGenres;
    @Schema(description = "project Invite Company", nullable = true)
    private List<ProjectProfileDto> projectInviteCompany;

    @Builder
    public ProjectListDto(Long projId,
                          String projectTitle,
                          String projectType,
                          String projectTypeName,
                          Long projOwner,
                          String projOwnerName,
                          String projIndivCompType,
                          String projIndivCompTypeName,
                          String budget,
                          String budgetName,
                          YnTypeEnum completeYn,
                          LocalDate projDdlDt,
                          LocalDateTime regDt,
                          List<ProjectMetadataDto> projectGenres,
                          List<ProjectProfileDto> projectInviteCompany
    ) {
        this.projId = projId;
        this.projectTitle = projectTitle;
        this.projectType = projectType;
        this.projectTypeName = projectTypeName;
        this.projOwner = projOwner;
        this.projOwnerName = projOwnerName;
        this.projIndivCompType = projIndivCompType;
        this.projIndivCompTypeName = projIndivCompTypeName;
        this.budget = budget;
        this.budgetName = budgetName;
        this.completeYn = completeYn;
        this.projDdlDt = projDdlDt;
        this.regDt = regDt;
        this.projectGenres = projectGenres;
        this.projectInviteCompany = projectInviteCompany;
    }
}
