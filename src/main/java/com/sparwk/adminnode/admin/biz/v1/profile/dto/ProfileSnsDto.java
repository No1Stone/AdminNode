package com.sparwk.adminnode.admin.biz.v1.profile.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class ProfileSnsDto {

    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long profileId;
    @Schema(description = "SNS Type Code", nullable = false, example = "SNS000002")
    private String snsTypeCd;
    @Schema(description = "SNS Type Code Name", nullable = false, example = "Official")
    private String snsTypeCdName;
    @Schema(description = "SNS_URL", nullable = true, example = "http://...")
    private String snsUrl;

    @Builder
    public ProfileSnsDto(Long profileId,
                         String snsTypeCd,
                         String snsTypeCdName,
                         String snsUrl
    ) {
        this.profileId = profileId;
        this.snsTypeCd = snsTypeCd;
        this.snsTypeCdName = snsTypeCdName;
        this.snsUrl = snsUrl;
    }
}
