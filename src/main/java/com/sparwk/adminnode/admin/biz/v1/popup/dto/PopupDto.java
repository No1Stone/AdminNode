package com.sparwk.adminnode.admin.biz.v1.popup.dto;

import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Setter
@Getter
@NoArgsConstructor
public class PopupDto {

    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long popupSeq;
    @Schema(description = "코드 9자리", nullable = false, example = "PPS000001")
    private String popupStatusCd;
    @Schema(description = "코드 이름", nullable = false, example = "Wating")
    private String popupStatusCdName;
    @Schema(description = "POPUP 이름", nullable = false, example = "공지사항")
    private String popupTitle;
    @Schema(description = "POPUP 시작일시", nullable = false, example = "팝업오픈 시작일시")
    private String sdate;
    @Schema(description = "POPUP 종료일시", nullable = false, example = "팝업오픈 종료일시")
    private String edate;
    @Schema(description = "POPUP 가로 크기", nullable = false, example = "300")
    private int popupSizeWidth;
    @Schema(description = "POPUP 세로 크기", nullable = false, example = "500")
    private int popupSizeHeight;
    @Schema(description = "POPUP 가로 위치", nullable = false, example = "300")
    private int popupLocationWidth;
    @Schema(description = "POPUP 세로 위치", nullable = false, example = "500")
    private int popupLocationHeight;
    @Schema(description = "POPUP 내용", nullable = true, example = "팝업 내용")
    private String content;
    @Schema(description = "등록일", pattern = "yyMMdd hh:mm:ss", example = "2021-12-08T05:44:26.627991")
    private LocalDateTime regDt;
    @Schema(description = "등록자", nullable = false, example = "10000001")
    private Long regUsr;
    @Schema(description = "등록자 이름", nullable = false, example = "Yoon Jung Sun")
    private String regUsrName;
    @Schema(description = "수정일", pattern = "yyMMdd hh:mm:ss", example = "2021-12-08T05:44:26.627991")
    private LocalDateTime modDt;
    @Schema(description = "수정자", nullable = false, example = "10000001")
    private Long modUsr;
    @Schema(description = "수정자 이름", nullable = false, example = "Yoon Jung Sun")
    private String modUsrName;

    @Builder
    public PopupDto(Long popupSeq,
                    String popupStatusCd,
                    String popupStatusCdName,
                    String popupTitle,
                    String sdate,
                    String edate,
                    int popupSizeWidth,
                    int popupSizeHeight,
                    int popupLocationWidth,
                    int popupLocationHeight,
                    String content,
                    LocalDateTime regDt,
                    Long regUsr,
                    String regUsrName,
                    LocalDateTime modDt,
                    Long modUsr,
                    String modUsrName
    ) {
        this.popupSeq = popupSeq;
        this.popupStatusCd = popupStatusCd;
        this.popupStatusCdName = popupStatusCdName;
        this.popupTitle = popupTitle;
        this.sdate = sdate;
        this.edate = edate;
        this.popupSizeWidth = popupSizeWidth;
        this.popupSizeHeight = popupSizeHeight;
        this.popupLocationWidth = popupLocationWidth;
        this.popupLocationHeight = popupLocationHeight;
        this.content = content;
        this.regDt = regDt;
        this.regUsr = regUsr;
        this.regUsrName = regUsrName;
        this.modDt = modDt;
        this.modUsr = modUsr;
        this.modUsrName = modUsrName;
    }
}