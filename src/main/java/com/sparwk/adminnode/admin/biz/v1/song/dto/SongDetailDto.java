package com.sparwk.adminnode.admin.biz.v1.song.dto;

import com.sparwk.adminnode.admin.jpa.entity.song.SongCreditsViewEntity;
import com.sparwk.adminnode.admin.jpa.entity.song.SongMetaCustomEntity;
import com.sparwk.adminnode.admin.jpa.entity.song.SongSplitsheetViewEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.Optional;

@Setter
@Getter
@NoArgsConstructor
public class SongDetailDto {

    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long songId;
    @Schema(description = "SONG TITLE", nullable = false, example = "SONG_TITLE")
    private String songTitle;
    @Schema(description = "AVATAR_FILE_URL", nullable = false, example = "http://")
    private String avatarFileUrl;
    @Schema(description = "SONG DESCRIPTION", nullable = false, example = "DESCRIPTION")
    private String description;

    @Schema(description = "SONG Status CODE", nullable = true, example = "SST000006")
    private String songStatusCd;
    @Schema(description = "SONG Status CODE Name", nullable = true, example = "Released")
    private String songStatusCdName;
    @Schema(description = "SONG_SUB_TITLE", nullable = false, example = "SONG_SUB_TITLE")
    private String songSubTitle;
    @Schema(description = "SONG VERSION CODE", nullable = true, example = "SGV000001")
    private String songVersionCd;
    @Schema(description = "SONG VERSION CODE Name", nullable = true, example = "OrigialVersion")
    private String songVersionCdName;
    @Schema(description = "USER DEFINE VERSION", nullable = true, example = "USER DEFINE VERSION")
    private String userDefineVersion;
    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long songOwnerId;
    @Schema(description = "SONG OWNER NAME", nullable = true, example = "Owner Name")
    private String songOwnerName;
    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long projId;
    @Schema(description = "Project Title", nullable = true, example = "PROJECT NOEASY")
    private String projTitle;
    @Schema(description = "Project Owner", nullable = true, example = "Jin Suk Choi")
    private String projOwner;
    @Schema(description = "Project Type", nullable = true, example = "Collaboration/Cowrite")
    private String projType;
    @Schema(description = "ISRC", nullable = true, example = "ISRC Number")
    private String ISRC;
    @Schema(description = "ISWC", nullable = true, example = "ISWC Number")
    private String ISWC;


    @Schema(description = "Lyrics")
    private Optional<SongFileDto> songFile;
    @Schema(description = "Lyrics", nullable = true, example = "Lyrics")
    private String lyrics;
    @Schema(description = "Lyrics Comment", nullable = true, example = "Owner's Comment")
    private String lyricsComment;
    @Schema(description = "Lyrics Language")
    private List<SongLyricsLangDto> songLyricsLang;


    @Schema(description = "Metadata Genre")
    private List<SongMetadataDto> songGenreDataList;
    @Schema(description = "Metadata SubGenre")
    private List<SongMetadataDto> songSubGenreDataList;
    @Schema(description = "Metadata Theme")
    private List<SongMetadataDto> songThemeDataList;
    @Schema(description = "Metadata Mood")
    private List<SongMetadataDto> songMoodDataList;
    @Schema(description = "Metadata Vocal")
    private List<SongMetadataDto> songVocalDataList;
    @Schema(description = "Metadata Era")
    private List<SongMetadataDto> songEraDataList;
    @Schema(description = "Metadata Tempo")
    private List<SongMetadataDto> songTempoDataList;
    @Schema(description = "Metadata Instrument")
    private List<SongMetadataDto> songInstrumentDataList;
    @Schema(description = "Custom meta")
    private List<SongMetaCustomEntity> songMetaCustomList;

    @Schema(description = "Song Split Sheet")
    private List<SongSplitsheetViewEntity> songSplitsheetViewEntityList;
    @Schema(description = "Song Cerdits")
    private List<SongCreditsViewEntity> songCreditsViewEntityList;
    @Schema(description = "Song Evaluation")
    List<SongEvaluationDto> songEvaluationList;
    @Schema(description = "Song Split Sheet")
    private List<SongTimelineDto> songTimelineDtoList;



    @Builder
    public SongDetailDto(Long songId,
                         String songTitle,
                         String avatarFileUrl,
                         String description,
                         String songStatusCd,
                         String songStatusCdName,
                         String userDefineVersion,
                         String songSubTitle,
                         String songVersionCd,
                         String songVersionCdName,
                         Long songOwnerId,
                         String songOwnerName,
                         Long projId,
                         String projTitle,
                         String projOwner,
                         String projType,
                         String ISRC,
                         String ISWC,
                         Optional<SongFileDto> songFile,
                         String lyrics,
                         String lyricsComment,
                         List<SongLyricsLangDto> songLyricsLang,
                         List<SongMetadataDto> songGenreDataList,
                         List<SongMetadataDto> songSubGenreDataList,
                         List<SongMetadataDto> songThemeDataList,
                         List<SongMetadataDto> songMoodDataList,
                         List<SongMetadataDto> songVocalDataList,
                         List<SongMetadataDto> songEraDataList,
                         List<SongMetadataDto> songTempoDataList,
                         List<SongMetadataDto> songInstrumentDataList,
                         List<SongMetaCustomEntity> songMetaCustomList,
                         List<SongSplitsheetViewEntity> songSplitsheetViewEntityList,
                         List<SongCreditsViewEntity> songCreditsViewEntityList,
                         List<SongEvaluationDto> songEvaluationList,
                         List<SongTimelineDto> songTimelineDtoList
    ) {
        this.songId = songId;
        this.songTitle = songTitle;
        this.avatarFileUrl = avatarFileUrl;
        this.description = description;
        this.songStatusCd = songStatusCd;
        this.songStatusCdName = songStatusCdName;
        this.songSubTitle = songSubTitle;
        this.songVersionCd = songVersionCd;
        this.songVersionCdName = songVersionCdName;
        this.userDefineVersion = userDefineVersion;
        this.songOwnerId = songOwnerId;
        this.songOwnerName = songOwnerName;
        this.projId = projId;
        this.projTitle = projTitle;
        this.projOwner = projOwner;
        this.projType = projType;
        this.ISRC = ISRC;
        this.ISWC = ISWC;
        this.songFile = songFile;
        this.lyrics = lyrics;
        this.lyricsComment = lyricsComment;
        this.songLyricsLang = songLyricsLang;
        this.songGenreDataList = songGenreDataList;
        this.songSubGenreDataList = songSubGenreDataList;
        this.songThemeDataList = songThemeDataList;
        this.songMoodDataList = songMoodDataList;
        this.songVocalDataList = songVocalDataList;
        this.songEraDataList = songEraDataList;
        this.songTempoDataList = songTempoDataList;
        this.songInstrumentDataList = songInstrumentDataList;
        this.songMetaCustomList = songMetaCustomList;
        this.songSplitsheetViewEntityList = songSplitsheetViewEntityList;
        this.songCreditsViewEntityList = songCreditsViewEntityList;
        this.songEvaluationList = songEvaluationList;
        this.songTimelineDtoList = songTimelineDtoList;
    }
}
