package com.sparwk.adminnode.admin.biz.v1.sns.dto;

import lombok.*;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class SnsRequest {

//    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
//    private Long snsSeq;
//    @Schema(description = "SNS 이름", nullable = false, example = "Facebook")
//    private String snsName;
//    @Schema(description = "코드 9자리", nullable = false, example = "SNS000001")
//    private String snsCode;
//    @Schema(description = "코드 URL", nullable = false, example = "http://")
//    private String snsIconUrl;
//    @Schema(description = "코드 사용여부 Y/N", defaultValue = "Y", allowableValues = {"Y", "N"})
//    private YnEnumType useType;
//    @Schema(description = "등록일", pattern = "yyMMdd hh:mm:ss", example = "2021-12-08 05:44:26.627991")
//    private LocalDateTime regDt;
//    @Schema(description = "등록자", nullable = false, example = "10000001")
//    private Long regUsr;
//    @Schema(description = "수정일", pattern = "yyMMdd hh:mm:ss", example = "2021-12-08 05:44:26.627991")
//    private LocalDateTime modDt;
//    @Schema(description = "수정자", nullable = false, example = "10000001")
//    private Long modUsr;
//
//    @Builder
//    public SnsDto(Long snsSeq,
//                  String snsName,
//                  String snsCode,
//                  String snsIconUrl,
//                  YnEnumType useType,
//                  LocalDateTime regDt,
//                  Long regUsr,
//                  LocalDateTime modDt,
//                  Long modUsr
//    ) {
//        this.snsSeq = snsSeq;
//        this.snsName = snsName;
//        this.snsCode = snsCode;
//        this.snsIconUrl = snsIconUrl;
//        this.useType = useType;
//        this.regDt = regDt;
//        this.regUsr = regUsr;
//        this.modDt = modDt;
//        this.modUsr = modUsr;
//    }

    private SnsDto snsDto;
}
