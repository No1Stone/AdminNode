package com.sparwk.adminnode.admin.biz.v1.country.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sparwk.adminnode.admin.jpa.entity.ExcelColumnName;
import com.sparwk.adminnode.admin.jpa.entity.ExcelDto;
import com.sparwk.adminnode.admin.jpa.entity.ExcelFileName;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Arrays;
import java.util.List;

@Data
@NoArgsConstructor
@ExcelFileName(filename = "Country_Code")
public class CountryExcelDto implements ExcelDto {

    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    @ExcelColumnName(headerName = "Country Seq")
    private Long countrySeq;
    @Schema(description = "국가명", nullable = false, example = "Africa")
    @ExcelColumnName
    @JsonProperty("country")
    private String country;
    @Schema(description = "국가코드 9자리", nullable = false, example = "CNT000216")
    @ExcelColumnName
    @JsonProperty("countryCd")
    private String countryCd;
    @Schema(description = "ISO 국가코드 2자리", nullable = true, example = "ZM")
    @ExcelColumnName
    @JsonProperty("iso2")
    private String iso2;
    @Schema(description = "ISO 국가코드 3자리", nullable = true, example = "ZMB")
    @ExcelColumnName
    @JsonProperty("iso3")
    private String iso3;
    @Schema(description = "대륙코드", nullable = false, example = "AS")
    @ExcelColumnName
    @JsonProperty("continentCode")
    private String continentCode;
    @Schema(description = "대륙", nullable = false, example = "Antarctica")
    @ExcelColumnName
    @JsonProperty("continentCodeVal")
    private String continentCodeVal;
    @Schema(description = "nmr 값", nullable = true, example = "887")
    @ExcelColumnName
    @JsonProperty("nmr")
    private String nmr;
    @Schema(description = "국가 전화번호", nullable = true, example = "82")
    @ExcelColumnName
    @JsonProperty("dial")
    private String dial;
    @Schema(description = "코드 사용여부 Y/N", nullable = false, defaultValue = "Y", allowableValues = {"Y", "N"})
    @ExcelColumnName
    @JsonProperty("useType")
    private YnTypeEnum useType;
    @Schema(description = "description", nullable = true, example = "설명")
    @ExcelColumnName
    @JsonProperty("description")
    private String description;

    @Override
    public List<String> mapToList() {
        return Arrays.asList(
                String.valueOf(countrySeq),
                country,
                countryCd,
                iso2,
                iso3,
                continentCode,
                continentCodeVal,
                nmr,
                dial,
                String.valueOf(useType),
                description

        );
    }
}
