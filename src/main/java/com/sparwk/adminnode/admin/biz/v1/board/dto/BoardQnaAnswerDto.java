package com.sparwk.adminnode.admin.biz.v1.board.dto;

import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

@Setter
@Getter
@NoArgsConstructor
public class BoardQnaAnswerDto {

    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long qnaId;
    @Schema(description = "답변 사용여부 Y/N", nullable = false, defaultValue = "N", allowableValues = {"Y", "N"})
    private YnTypeEnum answerYn;
    @Schema(description = "질문내용", nullable = true, example = "질문내용")
    private String answerContent;
    @Schema(description = "첨부파일 사용여부 Y/N", nullable = false, defaultValue = "N", allowableValues = {"Y", "N"})
    private YnTypeEnum answerAttachYn;
    @Schema(description = "답변 이메일 발송 성공여부 Y/N", defaultValue = "N", allowableValues = {"Y", "N"})
    private YnTypeEnum answerEmailYn;
    @Schema(description = "등록일", pattern = "yyMMdd hh:mm:ss", example = "2021-12-08T05:44:26.627991")
    private LocalDateTime answerRegDt;
    @Schema(description = "등록자", nullable = false, example = "10000001")
    private Long answerRegUsr;
    @Schema(description = "수정일", pattern = "yyMMdd hh:mm:ss", example = "2021-12-08T05:44:26.627991")
    private LocalDateTime answerModDt;
    @Schema(description = "수정자", nullable = false, example = "10000001")
    private Long answerModUsr;
    @Schema(description = "첨부파일", nullable = true)
    private List<BoardAttachDto> boardAttachDtoList;


    @Builder
    public BoardQnaAnswerDto(Long qnaId,
                             YnTypeEnum answerYn,
                             String answerContent,
                             YnTypeEnum answerAttachYn,
                             YnTypeEnum answerEmailYn,
                             LocalDateTime answerRegDt,
                             Long answerRegUsr,
                             LocalDateTime answerModDt,
                             Long answerModUsr,
                             List<BoardAttachDto> boardAttachDtoList
    ) {
        this.qnaId = qnaId;
        this.answerYn = answerYn;
        this.answerContent = answerContent;
        this.answerAttachYn = answerAttachYn;
        this.answerEmailYn = answerEmailYn;
        this.answerRegDt = answerRegDt;
        this.answerRegUsr = answerRegUsr;
        this.answerModDt = answerModDt;
        this.answerModUsr = answerModUsr;
        this.boardAttachDtoList = boardAttachDtoList;
    }
}