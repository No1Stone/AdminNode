package com.sparwk.adminnode.admin.biz.v1.currency.service;

import com.sparwk.adminnode.admin.biz.v1.CommonPagingListDto;
import com.sparwk.adminnode.admin.biz.v1.admin.service.AdminActiveLogService;
import com.sparwk.adminnode.admin.biz.v1.currency.dto.CurrencyDto;
import com.sparwk.adminnode.admin.biz.v1.currency.dto.CurrencyExcelDto;
import com.sparwk.adminnode.admin.biz.v1.currency.dto.CurrencyRequest;
import com.sparwk.adminnode.admin.config.common.ActiveCodeConst;
import com.sparwk.adminnode.admin.config.common.GetAccountId;
import com.sparwk.adminnode.admin.config.common.Response;
import com.sparwk.adminnode.admin.config.common.ResultCodeConst;
import com.sparwk.adminnode.admin.config.util.ExcelWriter;
import com.sparwk.adminnode.admin.jpa.entity.PeriodTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.admin.AdminActiveLogEntity;
import com.sparwk.adminnode.admin.jpa.entity.currency.CurrencyCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.currency.CurrencyEntity;
import com.sparwk.adminnode.admin.jpa.entity.currency.CurrencySorterEnum;
import com.sparwk.adminnode.admin.jpa.repository.currency.CurrencyRepository;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class CurrencyService {

    private final CurrencyRepository currencyRepository;
    private final AdminActiveLogService adminActiveLogService;

    private final HttpServletRequest httpServletRequest;
    private final MessageSourceAccessor messageSourceAccessor;

    private final Logger logger = LoggerFactory.getLogger(CurrencyService.class);


    public Response<CommonPagingListDto> getList(CurrencyCateEnum cate,
                                               String val,
                                               YnTypeEnum useType,
                                               PeriodTypeEnum periodType,
                                               String sdate,
                                               String edate,
                                               CurrencySorterEnum sorter,
                                               PageRequest pageRequest) {

        Response<CommonPagingListDto> res = new Response<>();

        List<CurrencyDto> currencyList =
                currencyRepository.findQryAll(cate, val, useType, periodType, sdate, edate, sorter, pageRequest);

        logger.info("currencyList - {}", currencyList);

        //조회내용 없을 때
        if(currencyList == null || currencyList.size() < 1){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        CommonPagingListDto list = new CommonPagingListDto();

        //총 카운트
        long totalSize = currencyRepository.countQryAll(cate, val, useType, periodType, sdate, edate);
        long totalPage = totalSize / pageRequest.getPageSize();
        long mod = totalSize % pageRequest.getPageSize();
        if(mod > 0) {
            totalPage += 1;
        }

        list.setList(currencyList);
        list.setSize(pageRequest.getPageSize());
        list.setPage(pageRequest.getPageNumber());
        list.setTotalPage(totalPage);
        list.setTotalSize(totalSize);

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(list);
        return res;
    }

    public Response<CommonPagingListDto> getUseYList(CurrencyCateEnum cate, String val) {

        Response<CommonPagingListDto> res = new Response<>();

        List<CurrencyDto> currencyList = currencyRepository.findQryUseY(cate, val);

        logger.info("currencyList - {}", currencyList);

        //조회내용 없을 때
        if(currencyList == null || currencyList.size() < 1){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        CommonPagingListDto list = new CommonPagingListDto();

        PageRequest pageRequest = PageRequest.of(10, 10);

        //총 카운트
        long totalSize = currencyRepository.countQryUseY(cate, val);
        long totalPage = totalSize / pageRequest.getPageSize();
        long mod = totalSize % pageRequest.getPageSize();
        if(mod > 0) {
            totalPage += 1;
        }

        list.setList(currencyList);
        list.setSize(pageRequest.getPageSize());
        list.setPage(pageRequest.getPageNumber());
        list.setTotalPage(totalPage);
        list.setTotalSize(totalSize);

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(list);
        return res;
    }

    public Response<Optional<CurrencyDto>> getOne(Long id) {

        Response<Optional<CurrencyDto>> res = new Response<>();

        //id값이 없을 때
        if(id == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<CurrencyEntity> currency = currencyRepository.findById(id);

        logger.info("currencyList - {}", currency);

        //조회내용 없을 때
        if(!currency.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<CurrencyDto> dtoPage =

                currency.map(p -> CurrencyDto.builder()
                        .currencySeq(p.getCurrencySeq())
                        .currency(p.getCurrency())
                        .currencyCd(p.getCurrencyCd())
                        .isoCode(p.getIsoCode())
                        .symbol(p.getSymbol())
                        .country(p.getCountry())
                        .useType(p.getUseType())
                        .regUsr(p.getRegUsr())
                        .regDt(p.getRegDt())
                        .modUsr(p.getModUsr())
                        .modDt(p.getModDt())
                        .build()
                );

        logger.info("dtoPage - {}", dtoPage);

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(dtoPage);
        return res;
    }

    @Transactional
    public Response<Void> saveContent(CurrencyRequest request, HttpServletRequest req) {

        Response<Void> res = new Response<>();
        String currencyCd;

        //USEYN 값 없거나 틀릴 때
        if(request.getCurrencyDto().getUseType() == null || (request.getCurrencyDto().getUseType() != YnTypeEnum.Y && request.getCurrencyDto().getUseType() != YnTypeEnum.N)){
            res.setResultCd(ResultCodeConst.NOT_VALID_USEYN.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Code 값 없을 때
        if(request.getCurrencyDto().getIsoCode() == null){
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if(request.getCurrencyDto().getCurrency() == null){
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        try {

            //seq 번호 불러옴
            int k = currencyRepository.getLastSeq("CUR") + 1;

            if(k < 10)
                currencyCd = "CUR00000" + k;
            else if(k < 100)
                currencyCd = "CUR0000" + k;
            else if(k < 1000)
                currencyCd = "CUR000" + k;
            else if(k < 10000)
                currencyCd = "CUR00" + k;
            else if(k < 100000)
                currencyCd = "CUR0" + k;
            else if(k < 1000000)
                currencyCd = "CUR" + k;
            else
                currencyCd = "CUR" + k;

            CurrencyEntity newCurrency = CurrencyEntity.builder()
                    .currency(request.getCurrencyDto().getCurrency())
                    .currencyCd(currencyCd)
                    .isoCode(request.getCurrencyDto().getIsoCode())
                    .country(request.getCurrencyDto().getCountry())
                    .useType(request.getCurrencyDto().getUseType())
                    .symbol(request.getCurrencyDto().getSymbol())
                    .build();

            CurrencyEntity saved = currencyRepository.save(newCurrency);

            //seq 번호 수동으로 업데이트 시킨다
            currencyRepository.updateLastSeq("CUR", k);

//            Optional<CurrencyEntity> currency = currencyRepository.findById(saved.getCurrencySeq());
//
//            Optional<CurrencyDto> dtoPage =
//
//                    currency.map(p -> CurrencyDto.builder()
//                            .currencySeq(p.getCurrencySeq())
//                            .currency(p.getCurrency())
//                            .code(p.getCode())
//                            .country(p.getCountry())
//                            .useType(p.getUseType())
//                            .regUsr(p.getRegUsr())
//                            .regDt(p.getRegDt())
//                            .modUsr(p.getModUsr())
//                            .modDt(p.getModDt())
//                            .build()
//                    );
//
//            return dtoPage;
            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
//            res.setResult(dtoPage);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));

            String menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_CURRENCY.getCode());
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_CREATE.getCode());

            //netive query
            String usrName = adminActiveLogService.findQryUsrName(saved.getRegUsr());

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(saved.getRegDt())
                    .adminId(saved.getRegUsr())
                    .menuName(menuName)
                    .activeType("C")
                    .contentsId(saved.getCurrencySeq())
                    .activeMsg(usrName + " " + activeMsg + " '" + saved.getCountry() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);


        } catch (Exception e){
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }

        return res;
    }

    @Transactional
    public Response<Void> updateYn(Long id, HttpServletRequest req) {

        Response<Void> res = new Response<>();

        //id값이 없을 때
        if(id == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<CurrencyEntity> currency = currencyRepository.findById(id);

        logger.info("currencyList - {}", currency);

        //조회내용 없을 때
        if(!currency.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        YnTypeEnum useType = currency.get().getUseType();
        YnTypeEnum newUseType = (useType == YnTypeEnum.Y) ? YnTypeEnum.N : YnTypeEnum.Y;

        logger.info("newUseType - {} ", newUseType);

        try {
            currency.get().updateYn(newUseType);
            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            //res.setResult(songDetailCode);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));

            String menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_CURRENCY.getCode());
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_MODIFY.getCode());

            //netive query
            String usrName = adminActiveLogService.findQryUsrName(currency.get().getModUsr());

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(currency.get().getModDt())
                    .adminId(currency.get().getModUsr())
                    .menuName(menuName)
                    .activeType("U")
                    .contentsId(currency.get().getCurrencySeq())
                    .activeMsg(usrName + " " + activeMsg + " '" + currency.get().getCurrency() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e) {
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }
        return res;

    }

    @Transactional
    public Response<Optional<CurrencyDto>> updateContent(Long id, CurrencyRequest request, HttpServletRequest req) {

        Response<Optional<CurrencyDto>> res = new Response<>();

        //id값이 없을 때
        if(id == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<CurrencyEntity> currencyUpdate = currencyRepository.findById(id);

        logger.info("currencyUpdate - {} ", currencyUpdate);

        //조회내용 없을 때
        if(!currencyUpdate.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //USEYN 값 없거나 틀릴 때
        if(request.getCurrencyDto().getUseType() == null || (request.getCurrencyDto().getUseType() != YnTypeEnum.Y && request.getCurrencyDto().getUseType() != YnTypeEnum.N)){
            res.setResultCd(ResultCodeConst.NOT_VALID_USEYN.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Code 값 없을 때
        if(request.getCurrencyDto().getIsoCode() == null){
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if(request.getCurrencyDto().getCurrency() == null){
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        try {

            currencyUpdate.get()
                    .update(request.getCurrencyDto().getCurrency(),
                            request.getCurrencyDto().getCurrencyCd(),
                            request.getCurrencyDto().getIsoCode(),
                            request.getCurrencyDto().getSymbol(),
                            request.getCurrencyDto().getCountry(),
                            request.getCurrencyDto().getUseType()
                    );

            Optional<CurrencyEntity> currency = currencyRepository.findById(currencyUpdate.get().getCurrencySeq());

            Optional<CurrencyDto> dtoPage =

                    currency.map(p -> CurrencyDto.builder()
                            .currencySeq(p.getCurrencySeq())
                            .currency(p.getCurrency())
                            .currencyCd(p.getCurrencyCd())
                            .isoCode(p.getIsoCode())
                            .country(p.getCountry())
                            .useType(p.getUseType())
                            .regUsr(p.getRegUsr())
                            .regDt(p.getRegDt())
                            .modUsr(p.getModUsr())
                            .modDt(p.getModDt())
                            .build()
                    );

            logger.info("dtoPage - {} ", dtoPage);

            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            res.setResult(dtoPage);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));

            String menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_CURRENCY.getCode());
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_MODIFY.getCode());

            //netive query
            String usrName = adminActiveLogService.findQryUsrName(currency.get().getModUsr());

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(currency.get().getModDt())
                    .adminId(currency.get().getModUsr())
                    .menuName(menuName)
                    .activeType("U")
                    .contentsId(currency.get().getCurrencySeq())
                    .activeMsg(usrName + " " + activeMsg + " '" + currency.get().getCurrency() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e){
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }

        return res;
    }

    @Transactional
    public Response<Void> deleteContent(Long id, HttpServletRequest req) {

        Response<Void> res = new Response<>();

        //id값이 없을 때
        if(id == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<CurrencyEntity> currency = currencyRepository.findById(id);

        logger.info("currency - {} ", currency);

        //조회내용 없을 때
        if(!currency.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        try {
            currencyRepository.deleteById(id);
            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
//            res.setResult(dtoPage);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));
            Long adminId = getAccountId.ofId(httpServletRequest);

            String menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_CURRENCY.getCode());
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_DELETE.getCode());

            //netive query
            String usrName = adminActiveLogService.findQryUsrName(adminId);

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(LocalDateTime.now())
                    .adminId(adminId)
                    .menuName(menuName)
                    .activeType("D")
                    .contentsId(currency.get().getCurrencySeq())
                    .activeMsg(usrName + " " + activeMsg + " '" + currency.get().getCurrency() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e) {
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }
        return res;

    }

    public List<CurrencyExcelDto> getExcelList() {

        List<CurrencyExcelDto> getExcelList = currencyRepository.findExcelList();

        logger.info("getExcelList - {} ", getExcelList);

        return getExcelList;
    }


    //엑셀 다운로드 요청
    public Map<String, Object> downExcelList() throws Exception {
        // 데이터 가져오기
        List<CurrencyExcelDto> excelList = this.getExcelList();

        logger.info("excelList - {} ", excelList);

        return ExcelWriter.createExcelData(excelList, CurrencyExcelDto.class);
    }

    //마지막 코드 번호 확인
    public Integer getLastSeq(String code) {

        return currencyRepository.getLastSeq(code);
    }

    //코드 번호 업데이트
    @Transactional
    public void updateLastSeq(String code, Integer k) {

        currencyRepository.updateLastSeq(code, k);
    }

    //이름으로 코드검색
    public Long getOneName(String val) {

        return currencyRepository.countByCurrency(val);
    }


    @Transactional
    public Response<Void> saveExcelContent(CurrencyDto currencyDto, HttpServletRequest req) {

        Response<Void> res = new Response<>();

        //USEYN 값 없거나 틀릴 때
        if (currencyDto.getUseType() == null || (currencyDto.getUseType() != YnTypeEnum.Y && currencyDto.getUseType() != YnTypeEnum.N)) {
            res.setResultCd(ResultCodeConst.NOT_VALID_USEYN.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if (currencyDto.getCurrency() == null) {
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if (currencyDto.getIsoCode() == null) {
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if (currencyDto.getSymbol() == null) {
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        try {

            CurrencyEntity newCurrency = CurrencyEntity.builder()
                    .currency(currencyDto.getCurrency())
                    .currencyCd(currencyDto.getCurrencyCd())
                    .isoCode(currencyDto.getIsoCode())
                    .symbol(currencyDto.getSymbol())
                    .country(currencyDto.getCountry())
                    .useType(currencyDto.getUseType())
                    .build();

            CurrencyEntity saved = currencyRepository.save(newCurrency);

            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
//            res.setResult(dtoPage);
            
            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));

            String menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_CURRENCY.getCode());
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_CREATE.getCode());

            //netive query
            String usrName = adminActiveLogService.findQryUsrName(saved.getRegUsr());

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(saved.getRegDt())
                    .adminId(saved.getRegUsr())
                    .menuName(menuName)
                    .activeType("C")
                    .contentsId(saved.getCurrencySeq())
                    .activeMsg(usrName + " " + activeMsg + " '" + saved.getCountry() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e) {
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }

        return res;
    }

}
