package com.sparwk.adminnode.admin.biz.v1.account.controller;

import com.sparwk.adminnode.admin.biz.v1.CommonPagingListDto;
import com.sparwk.adminnode.admin.biz.v1.account.dto.*;
import com.sparwk.adminnode.admin.biz.v1.account.service.AccountService;
import com.sparwk.adminnode.admin.biz.v1.anr.dto.AnrDto;
import com.sparwk.adminnode.admin.config.common.AdminPermissionIntergrationService;
import com.sparwk.adminnode.admin.config.common.Response;
import com.sparwk.adminnode.admin.config.common.ResultCodeConst;
import com.sparwk.adminnode.admin.jpa.entity.PeriodTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.account.*;
import com.sparwk.adminnode.admin.jpa.entity.anr.AnrCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.anr.AnrSorterEnum;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping(value = "/V1/operation/maintenance/")
@RequiredArgsConstructor
@CrossOrigin("*")
public class AccountController {

    private final Logger logger = LoggerFactory.getLogger(AccountController.class);

    @Autowired
    private AccountService accountService;

    @Autowired
    private AdminPermissionIntergrationService adminPermissionIntergrationService;
    private final MessageSourceAccessor messageSourceAccessor;

    @Value("${file.path}")
    private String fileRealPath;


    @GetMapping("assoiate")
    @Operation(
            summary = "Assoiate Member List", description = "Assoiate Member List - 준회원 리스트입니다.",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = AssoiateMemberViewListDto.class))),
                    @ApiResponse(
                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
                            , content = @Content(schema = @Schema(hidden = true)))
            })
    public Response<CommonPagingListDto> getAssoiateUserList(
            @Parameter(name = "cate", description = "검색 종류", in = ParameterIn.PATH) @RequestParam(value = "cate", required = false) MaintenanceAssociateMemberCateEnum cate,
            @Parameter(name = "val", description = "코드 값 포함", in = ParameterIn.PATH) @RequestParam(value = "val", required = false) String val,
            @Parameter(name = "periodType", description = "기간 종류", in = ParameterIn.PATH) @RequestParam(value = "periodType", required = false) PeriodTypeEnum periodType,
            @Parameter(name = "sdate", description = "시작일", in = ParameterIn.PATH) @RequestParam(value = "sdate", required = false) String sdate,
            @Parameter(name = "edate", description = "종료일", in = ParameterIn.PATH) @RequestParam(value = "edate", required = false) String edate,
            @Parameter(name = "sorter", description = "정렬", in = ParameterIn.PATH) @RequestParam(value = "sorter", required = false) MaintenanceAssociateMemberSorterEnum sorter,
            @Parameter(name = "page", description = "페이지 번호", in = ParameterIn.PATH) @RequestParam(value = "page", required = false, defaultValue = "1") int page,
            @Parameter(name = "size", description = "페이지 사이즈", in = ParameterIn.PATH) @RequestParam(value = "size", required = false, defaultValue = "10") int size
    ) {
        PageRequest pageRequest = PageRequest.of(page, size);

        Response res = new Response();
        if(!adminPermissionIntergrationService.AdminPermissionIntergrationCheck(7L, "C") &&
                !adminPermissionIntergrationService.AdminPermissionIntergrationCheck(67L, "C") ){
            res.setResultCd(ResultCodeConst.NOT_VALID_CREATE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        return accountService.getAssoiateUserList(cate, val, periodType, sdate, edate, sorter, pageRequest);
    }

    //상세페이지
    @GetMapping("assoiate/{id}")
    @Operation(
            summary = "Assoiate Member Detail View", description = "Assoiate Member Detail View 입니다.",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = AccountMemberDetailDto.class))),
                    @ApiResponse(
                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1003", description = "id가 존재하지 않습니다.."
                            , content = @Content(schema = @Schema(hidden = true)))
            })
    public Response<Optional<AccountMemberDetailDto>> getAssoiateUserOne(
            @Parameter(name = "id", description = "id 값", in = ParameterIn.PATH) @PathVariable(value = "id") Long id
    ) {
        Response res = new Response();
        if(!adminPermissionIntergrationService.AdminPermissionIntergrationCheck(7L, "C") &&
                !adminPermissionIntergrationService.AdminPermissionIntergrationCheck(67L, "C") ){
            res.setResultCd(ResultCodeConst.NOT_VALID_CREATE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        return accountService.getUserOne(id);
    }

    @GetMapping("verified")
    @Operation(
            summary = "Verified Member List", description = "Verified Member List - 회원 리스트입니다.",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = VerifiedMemberListDto.class))),
                    @ApiResponse(
                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
                            , content = @Content(schema = @Schema(hidden = true)))
            })
    public Response<CommonPagingListDto> getVerifiedUserList(
            @Parameter(name = "cate", description = "검색 종류", in = ParameterIn.PATH) @RequestParam(value = "cate", required = false) MaintenanceVerifiedMemberCateEnum cate,
            @Parameter(name = "val", description = "코드 값 포함", in = ParameterIn.PATH) @RequestParam(value = "val", required = false) String val,
            @Parameter(name = "periodType", description = "기간 종류", in = ParameterIn.PATH) @RequestParam(value = "periodType", required = false) PeriodTypeEnum periodType,
            @Parameter(name = "sdate", description = "시작일", in = ParameterIn.PATH) @RequestParam(value = "sdate", required = false) String sdate,
            @Parameter(name = "edate", description = "종료일", in = ParameterIn.PATH) @RequestParam(value = "edate", required = false) String edate,
            @Parameter(name = "sorter", description = "정렬", in = ParameterIn.PATH) @RequestParam(value = "sorter", required = false) MaintenanceVerifiedMemberSorterEnum sorter,
            @Parameter(name = "page", description = "페이지 번호", in = ParameterIn.PATH) @RequestParam(value = "page", required = false, defaultValue = "1") int page,
            @Parameter(name = "size", description = "페이지 사이즈", in = ParameterIn.PATH) @RequestParam(value = "size", required = false, defaultValue = "10") int size
    ) {
        PageRequest pageRequest = PageRequest.of(page, size);

        Response res = new Response();
        if(!adminPermissionIntergrationService.AdminPermissionIntergrationCheck(7L, "C") &&
                !adminPermissionIntergrationService.AdminPermissionIntergrationCheck(68L, "C") ){
            res.setResultCd(ResultCodeConst.NOT_VALID_CREATE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        return accountService.getVerifiedUserList(cate, val, periodType, sdate, edate, sorter, pageRequest);
    }

    //상세페이지
    @GetMapping("verified/{id}")
    @Operation(
            summary = "Assoiate Member Detail View", description = "Assoiate Member Detail View 입니다.",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = AccountMemberDetailDto.class))),
                    @ApiResponse(
                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1003", description = "id가 존재하지 않습니다.."
                            , content = @Content(schema = @Schema(hidden = true)))
            })
    public Response<Optional<AccountMemberDetailDto>> getVerifiedUserOne(
            @Parameter(name = "id", description = "id 값", in = ParameterIn.PATH) @PathVariable(value = "id") Long id
    ) {
        Response res = new Response();
        if(!adminPermissionIntergrationService.AdminPermissionIntergrationCheck(7L, "C") &&
                !adminPermissionIntergrationService.AdminPermissionIntergrationCheck(68L, "C") ){
            res.setResultCd(ResultCodeConst.NOT_VALID_CREATE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        return accountService.getUserOne(id);
    }


    @GetMapping("company")
    @Operation(
            summary = "Company Member list", description = "Company Member list - 회사리스트입니다",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = CompanyMemberViewListDto.class))),
                    @ApiResponse(
                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
                            , content = @Content(schema = @Schema(hidden = true)))
            })
    public Response<CommonPagingListDto> getVerifiedComList(
            @Parameter(name = "cate", description = "검색 종류", in = ParameterIn.PATH) @RequestParam(value = "cate", required = false) MaintenanceOfficeMemberCateEnum cate,
            @Parameter(name = "val", description = "코드 값 포함", in = ParameterIn.PATH) @RequestParam(value = "val", required = false) String val,
            @Parameter(name = "periodType", description = "기간 종류", in = ParameterIn.PATH) @RequestParam(value = "periodType", required = false) PeriodTypeEnum periodType,
            @Parameter(name = "sdate", description = "시작일", in = ParameterIn.PATH) @RequestParam(value = "sdate", required = false) String sdate,
            @Parameter(name = "edate", description = "종료일", in = ParameterIn.PATH) @RequestParam(value = "edate", required = false) String edate,
            @Parameter(name = "sorter", description = "정렬", in = ParameterIn.PATH) @RequestParam(value = "sorter", required = false) MaintenanceOfficeMemberSorterEnum sorter,
            @Parameter(name = "page", description = "페이지 번호", in = ParameterIn.PATH) @RequestParam(value = "page", required = false, defaultValue = "1") int page,
            @Parameter(name = "size", description = "페이지 사이즈", in = ParameterIn.PATH) @RequestParam(value = "size", required = false, defaultValue = "10") int size
    ) {
        PageRequest pageRequest = PageRequest.of(page, size);

        Response res = new Response();
        if(!adminPermissionIntergrationService.AdminPermissionIntergrationCheck(7L, "C") &&
                !adminPermissionIntergrationService.AdminPermissionIntergrationCheck(69L, "C") ){
            res.setResultCd(ResultCodeConst.NOT_VALID_CREATE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        return accountService.getVerifiedComList(cate, val, periodType, sdate, edate, sorter, pageRequest);
    }

    //상세페이지
    @GetMapping("company/{id}")
    @Operation(
            summary = "Company Member Detail View", description = "Company Member Detail View 입니다.",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = CompanyMemberDetailDto.class))),
                    @ApiResponse(
                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1003", description = "id가 존재하지 않습니다.."
                            , content = @Content(schema = @Schema(hidden = true)))
            })
    public Response<Optional<CompanyMemberDetailDto>> getComOne(
            @Parameter(name = "id", description = "id 값", in = ParameterIn.PATH) @PathVariable(value = "id") Long id
    ) {
        Response res = new Response();
        if(!adminPermissionIntergrationService.AdminPermissionIntergrationCheck(7L, "C") &&
                !adminPermissionIntergrationService.AdminPermissionIntergrationCheck(69L, "C") ){
            res.setResultCd(ResultCodeConst.NOT_VALID_CREATE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        return accountService.getComOne(id);
    }
}
