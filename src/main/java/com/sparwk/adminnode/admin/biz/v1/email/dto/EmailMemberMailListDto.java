package com.sparwk.adminnode.admin.biz.v1.email.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class EmailMemberMailListDto {

    @Schema(description = "email 주소", nullable = false, example = "yoon@sparwk.com")
    private String email;

    @Builder
    public EmailMemberMailListDto(
            String email
    ) {
        this.email = email;
    }
}
