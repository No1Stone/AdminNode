package com.sparwk.adminnode.admin.biz.v1.board.dto;

import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

@Setter
@Getter
@NoArgsConstructor
public class BoardSuccessDto {

    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long successId;
    @Schema(description = "name", nullable = false, example = "Mr.Yoon")
    private String name;
    @Schema(description = "imageUrl", nullable = false, example = "http://")
    private String imageUrl;
    @Schema(description = "role text", nullable = true, example = "artist, creator")
    private String role;
    @Schema(description = "genre text", nullable = true, example = "hip hop, dance")
    private String genre;
    @Schema(description = "content 설명", nullable = true, example = "내용")
    private String content;
    @Schema(description = "노출여부 Y/N", nullable = false, defaultValue = "Y", allowableValues = {"Y", "N"})
    private YnTypeEnum useType;
    @Schema(description = "등록일", pattern = "yyMMdd hh:mm:ss", example = "2021-12-08T05:44:26.627991")
    private LocalDateTime regDt;
    @Schema(description = "등록자", nullable = false, example = "10000001")
    private Long regUsr;
    @Schema(description = "등록자 이름", nullable = false, example = "Yoon Jung Sun")
    private String regUsrName;
    @Schema(description = "수정일", pattern = "yyMMdd hh:mm:ss", example = "2021-12-08T05:44:26.627991")
    private LocalDateTime modDt;
    @Schema(description = "수정자", nullable = false, example = "10000001")
    private Long modUsr;
    @Schema(description = "수정자 이름", nullable = false, example = "Yoon Jung Sun")
    private String modUsrName;


    @Builder
    public BoardSuccessDto(Long successId,
                           String name,
                           String imageUrl,
                           String role,
                           String genre,
                           String content,
                           YnTypeEnum useType,
                           LocalDateTime regDt,
                           Long regUsr,
                           String regUsrName,
                           LocalDateTime modDt,
                           Long modUsr,
                           String modUsrName
    ) {
        this.successId = successId;
        this.name = name;
        this.imageUrl = imageUrl;
        this.role = role;
        this.genre = genre;
        this.content = content;
        this.useType = useType;
        this.regDt = regDt;
        this.regUsr = regUsr;
        this.regUsrName = regUsrName;
        this.modDt = modDt;
        this.modUsr = modUsr;
        this.modUsrName = modUsrName;
    }
}