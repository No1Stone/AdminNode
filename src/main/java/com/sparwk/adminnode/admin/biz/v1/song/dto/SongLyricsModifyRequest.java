package com.sparwk.adminnode.admin.biz.v1.song.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class SongLyricsModifyRequest {

    @Schema(description = "song id 번호", nullable = false, example = "1")
    private Long songId;
    @Schema(description = "song seq 번호", nullable = false, example = "1")
    private Long songSeq;
    @Schema(name = "song lyrics", nullable = false, example = "lyrics")
    private String lyrics;
    @Schema(name = "song lyrics Comment", nullable = false, example = "comment")
    private String lyricsComt;


    @Builder
    public SongLyricsModifyRequest(Long songId,
                                   Long songSeq,
                                   String lyrics,
                                   String lyricsComt
    ) {

        this.songId = songId;
        this.songSeq = songSeq;
        this.lyrics = lyrics;
        this.lyricsComt = lyricsComt;
    }
}
