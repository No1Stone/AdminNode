package com.sparwk.adminnode.admin.biz.v1.continent.dto;

import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Setter
@Getter
@NoArgsConstructor
public class ContinentDto {

    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long continentSeq;
    @Schema(description = "대륙명", nullable = false, example = "Africa")
    private String continent;
    @Schema(description = "대륙코드", nullable = false, example = "Africa")
    private String continentCd;
    @Schema(description = "ISO 대륙코드", nullable = false, example = "AF")
    private String code;
    @Schema(description = "description", nullable = true, example = "설명")
    private String description;
    @Schema(description = "코드 사용여부 Y/N", nullable = false, defaultValue = "Y", allowableValues = {"Y", "N"})
    private YnTypeEnum useType;
    @Schema(description = "소속 국가 개수", nullable = false, example = "1")
    private Long cnt;
    @Schema(description = "등록일", pattern = "yyMMdd hh:mm:ss", example = "2021-12-08T05:44:26.627991")
    private LocalDateTime regDt;
    @Schema(description = "등록자", nullable = false, example = "10000001")
    private Long regUsr;
    @Schema(description = "등록자 이름", nullable = false, example = "Yoon Jung Sun")
    private String regUsrName;
    @Schema(description = "수정일", pattern = "yyMMdd hh:mm:ss", example = "2021-12-08T05:44:26.627991")
    private LocalDateTime modDt;
    @Schema(description = "수정자", nullable = false, example = "10000001")
    private Long modUsr;
    @Schema(description = "수정자 이름", nullable = false, example = "Yoon Jung Sun")
    private String modUsrName;

    @Builder
    public ContinentDto(Long continentSeq,
                        String continent,
                        String continentCd,
                        String code,
                        String description,
                        YnTypeEnum useType,
                        Long cnt,
                        LocalDateTime regDt,
                        Long regUsr,
                        String regUsrName,
                        LocalDateTime modDt,
                        Long modUsr,
                        String modUsrName
    ) {
        this.continentSeq = continentSeq;
        this.continent = continent;
        this.continentCd = continentCd;
        this.code = code;
        this.description = description;
        this.useType = useType;
        this.cnt = cnt;
        this.regDt = regDt;
        this.regUsr = regUsr;
        this.regUsrName = regUsrName;
        this.modDt = modDt;
        this.modUsr = modUsr;
        this.modUsrName = modUsrName;
    }
}
