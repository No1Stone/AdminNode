package com.sparwk.adminnode.admin.biz.v1.inquary.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Setter
@Getter
@NoArgsConstructor
public class InquaryDto {

    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long adminLoginLogSeq;
    @Schema(description = "등록일", pattern = "yyMMdd hh:mm:ss", example = "2021-12-08T05:44:26.627991")
    private LocalDateTime connectTime;
    @Schema(description = "접속 결과", nullable = false, example = "Y")
    private String connectResult;
    @Schema(description = "관리자 아이디 번호", nullable = false, example = "1")
    private Long adminId;
    @Schema(description = "관리자 이름", nullable = false, example = "yoon")
    private String fullName;
    @Schema(description = "관리자 이메일", nullable = false, example = "yoon@sparwk.com")
    private String adminEmail;
    @Schema(description = "접속 브라우저", nullable = false, example = "safari")
    private String connectBrowser;
    @Schema(description = "접속 OS", nullable = false, example = "window 10")
    private String connectOs;
    @Schema(description = "접속 기기", nullable = false, example = "computer")
    private String connectDevice;
    @Schema(description = "접속 IP", nullable = false, example = "1.1.1.1")
    private String connectIp;

    @Builder
    public InquaryDto(
            Long adminLoginLogSeq,
            LocalDateTime connectTime,
            String connectResult,
            Long adminId,
            String fullName,
            String adminEmail,
            String connectBrowser,
            String connectOs,
            String connectDevice,
            String connectIp
    ) {
        this.adminLoginLogSeq = adminLoginLogSeq;
        this.connectTime = connectTime;
        this.connectResult = connectResult;
        this.adminId = adminId;
        this.fullName = fullName;
        this.adminEmail = adminEmail;
        this.connectBrowser = connectBrowser;
        this.connectOs = connectOs;
        this.connectDevice = connectDevice;
        this.connectIp = connectIp;
    }
}