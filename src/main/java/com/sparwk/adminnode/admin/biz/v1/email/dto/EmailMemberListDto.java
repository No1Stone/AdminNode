package com.sparwk.adminnode.admin.biz.v1.email.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Setter
@Getter
@NoArgsConstructor
public class EmailMemberListDto {

    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long profileId;
    @Schema(description = "full Name", nullable = false, example = "yoon jung sub")
    private String fullName;
    @Schema(description = "email 주소", nullable = false, example = "yoon@sparwk.com")
    private String accntEmail;
    @Schema(description = "회원타입", nullable = false, example = "i")
    private String profileType;

    @Builder
    public EmailMemberListDto(Long profileId,
                              String fullName,
                              String accntEmail,
                              String profileType
    ) {
        this.profileId = profileId;
        this.fullName = fullName;
        this.accntEmail = accntEmail;
        this.profileType = profileType;
    }
}
