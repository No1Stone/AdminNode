package com.sparwk.adminnode.admin.biz.v1.passwordPolicy.dto;

import lombok.*;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class PasswordPolicyRequest {

//    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
//    private Long passwordPolicySeq;
//    @Schema(description = "패스워드 변경 기간 설정", nullable = false, example = "90")
//    private Integer passwordsExpire;
//    @Schema(description = "이전과 동일한 패스워드를 최소 몇번까지 사용을 못하게 할 것인지 설정", nullable = false, example = "3")
//    private Integer passwordHistory;
//    @Schema(description = "패스워드 최소 길이", nullable = false, example = "8")
//    private Integer passwordMinimumLength;
//    @Schema(description = "패스워드 복잡성을 요구 코드 9자리", nullable = false, example = "PWE000001")
//    private String passwordComplexityRequirement;
//    @Schema(description = "최대 로그인 잘못 시도 휫수 코드 9자리", nullable = false, example = "PWM000001")
//    private String maximumInvalidLogin;
//    @Schema(description = "잠금 유효 시간 코드 9자리", nullable = false, example = "PWP000001")
//    private String lockoutEffectivePeriod;
//    @Schema(description = "수정일", pattern = "yyMMdd hh:mm:ss", example = "2021-12-08T05:44:26.627991")
//    private LocalDateTime modDt;
//    @Schema(description = "수정자", nullable = false, example = "10000001")
//    private Long modUsr;
//
//    @Builder
//    public PasswordPolicyDto(Long passwordPolicySeq,
//                             Integer passwordsExpire,
//                             Integer passwordHistory,
//                             Integer passwordMinimumLength,
//                             String passwordComplexityRequirement,
//                             String maximumInvalidLogin,
//                             String lockoutEffectivePeriod,
//                             LocalDateTime modDt,
//                             Long modUsr
//    ) {
//        this.passwordPolicySeq = passwordPolicySeq;
//        this.passwordsExpire = passwordsExpire;
//        this.passwordHistory = passwordHistory;
//        this.passwordMinimumLength = passwordMinimumLength;
//        this.passwordComplexityRequirement = passwordComplexityRequirement;
//        this.maximumInvalidLogin = maximumInvalidLogin;
//        this.lockoutEffectivePeriod = lockoutEffectivePeriod;
//        this.modDt = modDt;
//        this.modUsr = modUsr;
//    }

    private PasswordPolicyDto passwordPolicyDto;
}
