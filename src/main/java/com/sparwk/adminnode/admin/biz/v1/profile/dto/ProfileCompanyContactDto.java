package com.sparwk.adminnode.admin.biz.v1.profile.dto;

import com.sparwk.adminnode.admin.biz.v1.account.dto.AccountCompanyTypeDto;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
@NoArgsConstructor
public class ProfileCompanyContactDto {

    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long profileId;
    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long partnerId;
    @Schema(description = "PROFILE CONTACT IMG URL", nullable = true, example = "http://...")
    private String profileContactImgUrl;
    @Schema(description = "PROFILE_CONTACT_DESCRIPTION", nullable = false, example = "description")
    private String profileContactDescription;
    @Schema(description = "CONTCT_FIRST_NAME", nullable = false, example = "Yoon")
    private String contctFirstName;
    @Schema(description = "CONTCT_MIDDLE_NAME", nullable = true, example = "JUNG")
    private String contctMiddleName;
    @Schema(description = "CONTCT_Last_NAME", nullable = false, example = "HUN")
    private String contctLastName;
    @Schema(description = "CONTCT_EMAIL", nullable = true, example = "yoon@gmail.com")
    private String contctEmail;
    @Schema(description = "CONTCT_PHONE_NUMBER", nullable = true, example = "01000001234")
    private String contctPhoneNumber;
    @Schema(description = "VERIFY_PHONE Y/N", nullable = false, defaultValue = "N", allowableValues = {"Y", "N"})
    private YnTypeEnum verifyPhoneYn;
    @Schema(description = "company cd", nullable = false, example = "CNT000001")
    private String countryCd;
    @Schema(description = "company Country Name", nullable = false, example = "USA")
    private String countryCdName;



    @Builder
    public ProfileCompanyContactDto(Long profileId,
                                    Long partnerId,
                                    String profileContactImgUrl,
                                    String profileContactDescription,
                                    String contctFirstName,
                                    String contctMiddleName,
                                    String contctLastName,
                                    String contctEmail,
                                    String contctPhoneNumber,
                                    YnTypeEnum verifyPhoneYn,
                                    String countryCd,
                                    String countryCdName
    ) {
        this.profileId=profileId;
        this.partnerId=partnerId;
        this.profileContactImgUrl=profileContactImgUrl;
        this.profileContactDescription=profileContactDescription;
        this.contctFirstName=contctFirstName;
        this.contctMiddleName=contctMiddleName;
        this.contctLastName=contctLastName;
        this.contctEmail=contctEmail;
        this.contctPhoneNumber=contctPhoneNumber;
        this.verifyPhoneYn=verifyPhoneYn;
        this.countryCd=countryCd;
        this.countryCdName=countryCdName;
    }
}
