package com.sparwk.adminnode.admin.biz.v1.timezone.dto;

import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Setter
@Getter
@NoArgsConstructor
public class TimezoneDto {

    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long timezoneSeq;
    @Schema(description = "타임존명", nullable = false, example = "Africa/Maseru")
    private String timezoneName;
    @Schema(description = "국가명", nullable = false, example = "Africa")
    private String continent;
    @Schema(description = "도시명", nullable = false, example = "Maseru")
    private String city;
    @Schema(description = "UTC 시간", nullable = false, example = "3:00")
    private String utcHour;
    @Schema(description = "UTC 시간 숫자형", nullable = false, example = "3")
    private int diffTime;
    @Schema(description = "코드 사용여부 Y/N", nullable = false, defaultValue = "Y", allowableValues = {"Y", "N"})
    private YnTypeEnum useType;
    @Schema(description = "등록일", pattern = "yyMMdd hh:mm:ss", example = "2021-12-08T05:44:26.627991")
    private LocalDateTime regDt;
    @Schema(description = "등록자", nullable = false, example = "10000001")
    private Long regUsr;
    @Schema(description = "등록자 이름", nullable = false, example = "Yoon Jung Sun")
    private String regUsrName;
    @Schema(description = "수정일", pattern = "yyMMdd hh:mm:ss", example = "2021-12-08T05:44:26.627991")
    private LocalDateTime modDt;
    @Schema(description = "수정자", nullable = false, example = "10000001")
    private Long modUsr;
    @Schema(description = "수정자 이름", nullable = false, example = "Yoon Jung Sun")
    private String modUsrName;

    @Builder
    public TimezoneDto(
                        Long timezoneSeq,
                        String timezoneName,
                        String continent,
                        String city,
                        String utcHour,
                        int diffTime,
                        YnTypeEnum useType,
                        LocalDateTime regDt,
                        Long regUsr,
                        String regUsrName,
                        LocalDateTime modDt,
                        Long modUsr,
                        String modUsrName
    ) {
        this.timezoneSeq = timezoneSeq;
        this.timezoneName = timezoneName;
        this.continent = continent;
        this.city = city;
        this.utcHour = utcHour;
        this.diffTime = diffTime;
        this.useType = useType;
        this.regDt = regDt;
        this.regUsr = regUsr;
        this.regUsrName = regUsrName;
        this.modDt = modDt;
        this.modUsr = modUsr;
        this.modUsrName = modUsrName;
    }
}
