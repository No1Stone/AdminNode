package com.sparwk.adminnode.admin.biz.v1.song.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class SongModifyRequest {

    @Schema(description = "song id 번호", nullable = false, example = "1")
    private Long songId;
    @Schema(name = "song TITLE", nullable = false, example = "Title")
    private String songTitle;
    @Schema(name = "song sub TITLE", nullable = false, example = "SubTitle")
    private String songSubTitle;
    @Schema(name = "Description", nullable = false, example = "Description")
    private String description;
    @Schema(name = "isrc", nullable = false, example = "isrc")
    private String isrc;
    @Schema(name = "iswc", nullable = false, example = "iswc")
    private String iswc;


    @Builder
    public SongModifyRequest(Long songId,
                             String songTitle,
                             String songSubTitle,
                             String description,
                             String isrc,
                             String iswc
    ) {

        this.songId = songId;
        this.songTitle = songTitle;
        this.songSubTitle = songSubTitle;
        this.description = description;
        this.isrc = isrc;
        this.iswc = iswc;
    }
}
