package com.sparwk.adminnode.admin.biz.v1.commonCode.dto;

import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.time.LocalDateTime;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class CateCodeRequest {

    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long commonDetailCodeSeq;
    @Schema(description = "부모코드 3자리", nullable = false, example = "ENC")
    private String pcode;
    @Schema(description = "코드 값", nullable = false, example = "UTF-8")
    private String val;
    @Schema(description = "코드 사용여부 Y/N", nullable = false, defaultValue = "Y", allowableValues = {"Y", "N"})
    private YnTypeEnum useType;

}
