package com.sparwk.adminnode.admin.biz.v1.country.dto;

import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Setter
@Getter
@NoArgsConstructor
public class CountryDto {

    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long countrySeq;
    @Schema(description = "국가명", nullable = false, example = "Africa")
    private String country;
    @Schema(description = "국가코드 9자리", nullable = false, example = "CNT000216")
    private String countryCd;
    @Schema(description = "ISO 국가코드 2자리", nullable = true, example = "ZM")
    private String iso2;
    @Schema(description = "ISO 국가코드 3자리", nullable = true, example = "ZMB")
    private String iso3;
    @Schema(description = "대륙코드", nullable = false, example = "AS")
    private String continentCode;
    @Schema(description = "대륙", nullable = false, example = "Antarctica")
    private String continentCodeVal;
    @Schema(description = "nmr 값", nullable = true, example = "887")
    private String nmr;
    @Schema(description = "국가 전화번호", nullable = true, example = "82")
    private String dial;
    @Schema(description = "코드 사용여부 Y/N", nullable = false, defaultValue = "Y", allowableValues = {"Y", "N"})
    private YnTypeEnum useType;
    @Schema(description = "description", nullable = true, example = "설명")
    private String description;
    @Schema(description = "등록일", pattern = "yyMMdd hh:mm:ss", example = "2021-12-08T05:44:26.627991")
    private LocalDateTime regDt;
    @Schema(description = "등록자", nullable = false, example = "10000001")
    private Long regUsr;
    @Schema(description = "등록자 이름", nullable = false, example = "Yoon Jung Sun")
    private String regUsrName;
    @Schema(description = "수정일", pattern = "yyMMdd hh:mm:ss", example = "2021-12-08T05:44:26.627991")
    private LocalDateTime modDt;
    @Schema(description = "수정자", nullable = false, example = "10000001")
    private Long modUsr;
    @Schema(description = "수정자 이름", nullable = false, example = "Yoon Jung Sun")
    private String modUsrName;

    @Builder
    public CountryDto(Long countrySeq,
                      String country,
                      String countryCd,
                      String iso2,
                      String iso3,
                      String continentCode,
                      String continentCodeVal,
                      String nmr,
                      YnTypeEnum useType,
                      String description,
                      String dial,
                      LocalDateTime regDt,
                      Long regUsr,
                      String regUsrName,
                      LocalDateTime modDt,
                      Long modUsr,
                      String modUsrName
    ) {
        this.countrySeq = countrySeq;
        this.country = country;
        this.countryCd = countryCd;
        this.iso2 = iso2;
        this.iso3 = iso3;
        this.continentCode = continentCode;
        this.continentCodeVal = continentCodeVal;
        this.nmr = nmr;
        this.dial = dial;
        this.useType = useType;
        this.description = description;
        this.regDt = regDt;
        this.regUsr = regUsr;
        this.regUsrName = regUsrName;
        this.modDt = modDt;
        this.modUsr = modUsr;
        this.modUsrName = modUsrName;
    }
}
