package com.sparwk.adminnode.admin.biz.v1.board.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class BoardDelete {

    @Schema(description = "id값 Seq 사용", nullable = false, example = "[1, 2]")
    private Long[] id;

}