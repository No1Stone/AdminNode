package com.sparwk.adminnode.admin.biz.v1.test.controller;

import com.sparwk.adminnode.admin.biz.v1.test.dto.TestDto;
import com.sparwk.adminnode.admin.biz.v1.test.dto.TestExcelDto;
import com.sparwk.adminnode.admin.biz.v1.test.service.TestExcelService;
import lombok.RequiredArgsConstructor;
import org.apache.commons.io.FilenameUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(value = "/V1/setting/identifire/")
@RequiredArgsConstructor
@CrossOrigin("*")
@ApiIgnore
public class TestExcelController {

    private final Logger logger = LoggerFactory.getLogger(TestExcelController.class);

    @Autowired
    private TestExcelService testExcelService;

    @Value("${file.path}")
    private String fileRealPath;

    // @ResponseBody 생략가능. class 에 @RestController
    @PostMapping(value="test/uploadExcel")
    public ResponseEntity<String> uploadFile(
            @RequestPart(value="key", required=false) @Valid TestDto testDto,
            @RequestParam("file") MultipartFile file,
            Model model,
            HttpServletRequest req
    ) throws IllegalStateException, IOException {

        if( !file.isEmpty() ) {
            logger.info("file org name = {}", file.getOriginalFilename());
            logger.info("file content type = {}", file.getContentType());
            logger.info("file content type = {}", file.getSize());
            logger.info("file content type = {}", file.getName());
            //file.transferTo(new File(file.getOriginalFilename()));
        }

        List<TestExcelDto> dataList = new ArrayList<>();

        String extension = FilenameUtils.getExtension(file.getOriginalFilename()); // 3

        if (!extension.equals("xlsx") && !extension.equals("xls")) {
            throw new IOException("엑셀파일만 업로드 해주세요.");
        }


        System.out.println(fileRealPath);

        UUID uuid = UUID.randomUUID();
        String uuidFilename = uuid+"_"+file.getOriginalFilename();


        //이미지 저장 위치: 외부에 잡기/ 나중에 이 경로랑 파일명만 db에 저장하면 된다.
        //nio 로 임포트
        Path filePath = Paths.get(fileRealPath+uuidFilename);

        logger.info("{}", filePath.toString());

        try {
            Files.write(filePath,file.getBytes());
            //얘가 스레드 새로 만들어서 만약 10메가 넘어가면 여기서 파일 넣는동안 메인스레드가 ok 뿌려서 엑박뜸->
            //사진 용량 제한시키기 아니면 메인스레드 기다리게하기(비동기처리)

            Workbook workbook = null;

            if (extension.equals("xlsx")) {
                workbook = new XSSFWorkbook(file.getInputStream());
            } else if (extension.equals("xls")) {
                workbook = new HSSFWorkbook(file.getInputStream());
            }

            Sheet worksheet = workbook.getSheetAt(0);

            for (int i = 1; i < worksheet.getPhysicalNumberOfRows(); i++) { // 4

                Row row = worksheet.getRow(i);

                TestExcelDto testExcelDto = new TestExcelDto();

                testExcelDto.setCode(row.getCell(0).getStringCellValue());
                testExcelDto.setCkey(row.getCell(1).getStringCellValue());
                testExcelDto.setCval(row.getCell(2).getStringCellValue());
                testExcelDto.setCdesc(row.getCell(3).getStringCellValue());
                //숫자일경우
                //testExcelDto.setCdesc(row.getCell(3).getNumericCellValue());

                dataList.add(testExcelDto);

                logger.info("testExcelDto - {}", testExcelDto.toString());
                testExcelService.saveContent(testExcelDto, req);
            }



        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return new ResponseEntity<>("", HttpStatus.OK);
    }

}
