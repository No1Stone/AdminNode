package com.sparwk.adminnode.admin.biz.v1.song.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class SongSplitSheetModifyRequest {

    @Schema(description = "song id 번호", nullable = false, example = "1")
    private Long songId;
    @Schema(description = "profile id 번호", nullable = false, example = "1")
    private Long profileId;
    @Schema(name = "rate share", nullable = false, example = "%")
    private Float rateShare;
    @Schema(name = "song lyrics Comment", nullable = false, example = "comment")
    private String isni;


    @Builder
    public SongSplitSheetModifyRequest(Long songId,
                                        Long profileId,
                                       Float rateShare,
                                       String isni
    ) {

        this.songId = songId;
        this.profileId = profileId;
        this.rateShare = rateShare;
        this.isni = isni;
    }
}
