package com.sparwk.adminnode.admin.biz.v1.commonCode.dto;

import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Setter
@Getter
@NoArgsConstructor
public class CommonCodeDto {

    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long commonCodeSeq;
    @Schema(description = "부모코드 3자리", nullable = false, example = "ENC")
    private String code;
    @Schema(description = "부모코드 값", nullable = false, example = "Encoding")
    private String val;
    @Schema(description = "부모코드 사용여부 Y/N", nullable = false, defaultValue = "Y", allowableValues = {"Y", "N"})
    private YnTypeEnum useType;
    @Schema(description = "등록일", pattern = "yyMMdd hh:mm:ss", example = "2021-12-08 05:44:26.627991")
    private LocalDateTime regDt;
    @Schema(description = "등록자", nullable = false, example = "10000001")
    private Long regUsr;
    @Schema(description = "수정일", pattern = "yyMMdd hh:mm:ss", example = "2021-12-08 05:44:26.627991")
    private LocalDateTime modDt;
    @Schema(description = "수정자", nullable = false, example = "10000001")
    private Long modUsr;

    @Builder
    public CommonCodeDto(Long commonCodeSeq,
                         String code,
                         String val,
                         YnTypeEnum useType,
                         LocalDateTime regDt,
                         Long regUsr,
                         LocalDateTime modDt,
                         Long modUsr
    ) {
        this.commonCodeSeq = commonCodeSeq;
        this.code = code;
        this.val = val;
        this.useType = useType;
        this.regDt = regDt;
        this.regUsr = regUsr;
        this.modDt = modDt;
        this.modUsr = modUsr;
    }
}
