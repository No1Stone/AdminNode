package com.sparwk.adminnode.admin.biz.v1.timezone.service;

import com.sparwk.adminnode.admin.biz.v1.CommonPagingListDto;
import com.sparwk.adminnode.admin.biz.v1.admin.service.AdminActiveLogService;
import com.sparwk.adminnode.admin.biz.v1.timezone.dto.TimezoneDto;
import com.sparwk.adminnode.admin.biz.v1.timezone.dto.TimezoneRequest;
import com.sparwk.adminnode.admin.config.common.ActiveCodeConst;
import com.sparwk.adminnode.admin.config.common.GetAccountId;
import com.sparwk.adminnode.admin.config.common.Response;
import com.sparwk.adminnode.admin.config.common.ResultCodeConst;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.admin.AdminActiveLogEntity;
import com.sparwk.adminnode.admin.jpa.entity.timezone.TimezoneCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.timezone.TimezoneEntity;
import com.sparwk.adminnode.admin.jpa.entity.timezone.TimezoneSorterEnum;
import com.sparwk.adminnode.admin.jpa.repository.timezone.TimezoneRepository;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class TimezoneService {

    private final TimezoneRepository timezoneRepository;
    private final AdminActiveLogService adminActiveLogService;

    private final HttpServletRequest httpServletRequest;
    private final MessageSourceAccessor messageSourceAccessor;
    
    private final Logger logger = LoggerFactory.getLogger(TimezoneService.class);


    public Response<CommonPagingListDto> getList(TimezoneCateEnum cate, String val, YnTypeEnum useType, TimezoneSorterEnum sorter, PageRequest pageRequest) {

        Response<CommonPagingListDto> res = new Response<>();

        List<TimezoneDto> timezoneList = timezoneRepository.findQryAll(cate, val, useType, sorter, pageRequest);

        logger.info("timezoneList - {}", timezoneList);

        //조회내용 없을 때
        if(timezoneList == null || timezoneList.size() < 1){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        CommonPagingListDto list = new CommonPagingListDto();

        //총 카운트
        long totalSize = timezoneRepository.countQryAll(cate, val, useType);
        long totalPage = totalSize / pageRequest.getPageSize();
        long mod = totalSize % pageRequest.getPageSize();
        if(mod > 0) {
            totalPage += 1;
        }

        list.setList(timezoneList);
        list.setSize(pageRequest.getPageSize());
        list.setPage(pageRequest.getPageNumber());
        list.setTotalPage(totalPage);
        list.setTotalSize(totalSize);

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(list);
        return res;
    }

    public Response<CommonPagingListDto> getUseYList() {

        Response<CommonPagingListDto> res = new Response<>();

        List<TimezoneDto> timezoneList = timezoneRepository.findQryUseY();

        logger.info("timezoneList - {}", timezoneList);

        //조회내용 없을 때
        if(timezoneList == null || timezoneList.size() < 1){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        CommonPagingListDto list = new CommonPagingListDto();

        PageRequest pageRequest = PageRequest.of(10, 10);

        //총 카운트
        long totalSize = timezoneRepository.countQryUseY();
        long totalPage = totalSize / pageRequest.getPageSize();
        long mod = totalSize % pageRequest.getPageSize();
        if(mod > 0) {
            totalPage += 1;
        }

        list.setList(timezoneList);
        list.setSize(pageRequest.getPageSize());
        list.setPage(pageRequest.getPageNumber());
        list.setTotalPage(totalPage);
        list.setTotalSize(totalSize);

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(list);
        return res;
    }

    public Response<Optional<TimezoneDto>> getOne(Long id) {

        Response<Optional<TimezoneDto>> res = new Response<>();

        //id값이 없을 때
        if(id == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<TimezoneEntity> timezone = timezoneRepository.findById(id);

        logger.info("timezone - {}", timezone);

        //조회내용 없을 때
        if(!timezone.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<TimezoneDto> dtoPage =

                timezone.map(p -> TimezoneDto.builder()
                                .timezoneSeq(p.getTimezoneSeq())
                                .timezoneName(p.getTimezoneName())
                                .continent(p.getContinent())
                                .city(p.getCity())
                                .utcHour(p.getUtcHour())
                                .diffTime(p.getDiffTime())
                                .useType(p.getUseType())
                                .regUsr(p.getRegUsr())
                                .regDt(p.getRegDt())
                                .modUsr(p.getModUsr())
                                .modDt(p.getModDt())
                                .build()
                        );

        logger.info("dtoPage - {}", dtoPage);

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(dtoPage);
        return res;
    }

    @Transactional
    public Response<Void> saveContent(TimezoneRequest request, HttpServletRequest req) {

        Response<Void> res = new Response<>();

        /*
         내용 없어서 그냥 놔둠
         */

        try {
            TimezoneEntity newTimezone = TimezoneEntity.builder()
                    .timezoneName(request.getTimezoneDto().getTimezoneName())
                    .continent(request.getTimezoneDto().getContinent())
                    .city(request.getTimezoneDto().getCity())
                    .utcHour(request.getTimezoneDto().getUtcHour())
                    .diffTime(request.getTimezoneDto().getDiffTime())
                    .useType(request.getTimezoneDto().getUseType())
                    .build();

            TimezoneEntity saved = timezoneRepository.save(newTimezone);

//            Optional<TimezoneEntity> timezone = timezoneRepository.findById(saved.getTimezoneSeq());
//
//            Optional<TimezoneDto> dtoPage =
//
//                    timezone.map(p -> TimezoneDto.builder()
//                                    .timezoneSeq(p.getTimezoneSeq())
//                                    .timezoneName(p.getTimezoneName())
//                                    .continent(p.getContinent())
//                                    .city(p.getCity())
//                                    .utcHour(p.getUtcHour())
//                                    .diffTime(p.getDiffTime())
//                                    .useType(p.getUseType())
//                                    .regUsr(p.getRegUsr())
//                                    .regDt(p.getRegDt())
//                                    .modUsr(p.getModUsr())
//                                    .modDt(p.getModDt())
//                                    .build()
//                            );
//
            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
//            res.setResult(dtoPage);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));

            String menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_TIMEZONE.getCode());
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_CREATE.getCode());

            //netive query
            String usrName = adminActiveLogService.findQryUsrName(saved.getRegUsr());

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(saved.getRegDt())
                    .adminId(saved.getRegUsr())
                    .menuName(menuName)
                    .activeType("C")
                    .contentsId(saved.getTimezoneSeq())
                    .activeMsg(usrName + " " + activeMsg + " '" + saved.getContinent() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e){
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }

        return res;
    }

    @Transactional
    public Response<Void> updateYn(Long id, HttpServletRequest req) {

        Response<Void> res = new Response<>();

        //id값이 없을 때
        if(id == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<TimezoneEntity> timezone = timezoneRepository.findById(id);

        YnTypeEnum useType = timezone.get().getUseType();
        YnTypeEnum newUseType = (useType == YnTypeEnum.Y) ? YnTypeEnum.N : YnTypeEnum.Y;

        logger.info("timezone - {}", timezone);

        //조회내용 없을 때
        if(timezone == null){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        logger.info("newUseType - {} ", newUseType);

        try {
            timezone.get().updateYn(newUseType);
            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            //res.setResult(songDetailCode);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));

            String menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_TIMEZONE.getCode());
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_MODIFY.getCode());

            //netive query
            String usrName = adminActiveLogService.findQryUsrName(timezone.get().getModUsr());

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(timezone.get().getModDt())
                    .adminId(timezone.get().getModUsr())
                    .menuName(menuName)
                    .activeType("U")
                    .contentsId(timezone.get().getTimezoneSeq())
                    .activeMsg(usrName + " " + activeMsg + " '" + timezone.get().getContinent() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e) {
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }
        return res;

    }

    @Transactional
    public Response<Optional<TimezoneDto>> updateContent(Long id, TimezoneRequest request, HttpServletRequest req) {

        Response<Optional<TimezoneDto>> res = new Response<>();

        //id값이 없을 때
        if(id == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<TimezoneEntity> timezone = timezoneRepository.findById(id);

        logger.info("timezoneUpdate - {} ", timezone);

        //조회내용 없을 때
        if(timezone == null){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        try{

            timezone.get()
                    .update(request.getTimezoneDto().getTimezoneName(),
                            request.getTimezoneDto().getContinent(),
                            request.getTimezoneDto().getCity(),
                            request.getTimezoneDto().getUtcHour(),
                            request.getTimezoneDto().getDiffTime(),
                            request.getTimezoneDto().getUseType()
                        );

            Optional<TimezoneEntity> timezoneUpdate = timezoneRepository.findById(timezone.get().getTimezoneSeq());

            Optional<TimezoneDto> dtoPage =

                    timezoneUpdate.map(p -> TimezoneDto.builder()
                            .timezoneSeq(p.getTimezoneSeq())
                            .timezoneName(p.getTimezoneName())
                            .continent(p.getContinent())
                            .city(p.getCity())
                            .utcHour(p.getUtcHour())
                            .diffTime(p.getDiffTime())
                            .useType(p.getUseType())
                            .regUsr(p.getRegUsr())
                            .regDt(p.getRegDt())
                            .modUsr(p.getModUsr())
                            .modDt(p.getModDt())
                            .build()
                    );

            logger.info("dtoPage - {} ", dtoPage);

            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            res.setResult(dtoPage);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));

            String menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_TIMEZONE.getCode());
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_MODIFY.getCode());

            //netive query
            String usrName = adminActiveLogService.findQryUsrName(timezone.get().getModUsr());

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(timezone.get().getModDt())
                    .adminId(timezone.get().getModUsr())
                    .menuName(menuName)
                    .activeType("U")
                    .contentsId(timezone.get().getTimezoneSeq())
                    .activeMsg(usrName + " " + activeMsg + " '" + timezone.get().getContinent() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e){
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }

        return res;
    }

    @Transactional
    public Response<Void> deleteContent(Long id, HttpServletRequest req) {

        Response<Void> res = new Response<>();

        //id값이 없을 때
        if(id == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Map<String, Object> result = new HashMap<>();
        Optional<TimezoneEntity> timezone = timezoneRepository.findById(id);

        try {
            timezoneRepository.deleteById(id);
            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
//            res.setResult(dtoPage);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));
            Long adminId = getAccountId.ofId(httpServletRequest);

            String menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_TIMEZONE.getCode());
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_DELETE.getCode());

            //netive query
            String usrName = adminActiveLogService.findQryUsrName(adminId);

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(LocalDateTime.now())
                    .adminId(adminId)
                    .menuName(menuName)
                    .activeType("D")
                    .contentsId(timezone.get().getTimezoneSeq())
                    .activeMsg(usrName + " " + activeMsg + " '" + timezone.get().getContinent() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e) {
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }
        return res;
    }

}
