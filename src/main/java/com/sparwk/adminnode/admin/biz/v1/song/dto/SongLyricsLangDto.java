package com.sparwk.adminnode.admin.biz.v1.song.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class SongLyricsLangDto {

    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long songId;
    @Schema(description = "LANGUAGE CODE", nullable = false, example = "LAN000001")
    private String langCd;
    @Schema(description = "LANGUAGE", nullable = false, example = "English")
    private String language;

    @Builder
    public SongLyricsLangDto(Long songId,
                             String langCd,
                             String language
    ) {
        this.songId = songId;
        this.langCd = langCd;
        this.language = language;
    }
}
