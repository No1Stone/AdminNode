package com.sparwk.adminnode.admin.biz.v1.projects.dto;

import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Setter
@Getter
@NoArgsConstructor
public class ProjectViewListDto {

    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long projId;
    @Schema(description = "PROJECT TITLE", nullable = false, example = "PROJ_TITLE")
    private String projTitle;
    @Schema(description = "AVATAR_FILE_URL", nullable = false, example = "http://")
    private String avatarFileUrl;
    @Schema(description = "PROJECT TITLE TYPE", nullable = false, example = "PJT000002")
    private String pitchProjTypeCd;
    @Schema(description = "PROJECT TITLE TYPE NAME", nullable = false, example = "Collaboration / Cowriter")
    private String pitchProjTypeCdName;
    @Schema(description = "PROJECT OWNER", nullable = true, example = "1")
    private Long projOwner;
    @Schema(description = "PROJECT OWNER NAME", nullable = true, example = "OWNER PROFILE ID")
    private String projOwnerName;
    @Schema(description = "PROJECT INVITING COMPANY TYPE", nullable = true, example = "1")
    private Long compProfileId;
    @Schema(description = "PROJECT INVITING COMPANY TYPE Name", nullable = true, example = "kakao")
    private String compProfileName;
    @Schema(description = "PROJECT GENRE TYPE", nullable = true, example = "SGG000008,SGG000009")
    private String projGenreCd;
    @Schema(description = "PROJECT INVITING COMPANY TYPE Name", nullable = true, example = "HipHop,Jazz")
    private String projGenreName;
    @Schema(description = "BUDGET", nullable = false, example = "분배 PJC000001/PJC000002/PJC000003")
    private String projCondCd;
    @Schema(description = "BUDGET NAME", nullable = false, example = "Terms")
    private String projCondCdName;
    @Schema(description = "PROJECT COMPLETE YN", nullable = false, defaultValue = "Y", allowableValues = {"Y", "N"})
    private YnTypeEnum completeYn;
    @Schema(description = "등록일", pattern = "yyMMdd hh:mm:ss", example = "2021-12-08T05:44:26.627991")
    private LocalDateTime regDt;
    @Schema(description = "등록자", nullable = false, example = "10000001")
    private Long regUsr;
    @Schema(description = "등록자 이름", nullable = false, example = "Yoon Jung Sun")
    private String regUsrName;
    @Schema(description = "수정일", pattern = "yyMMdd hh:mm:ss", example = "2021-12-08T05:44:26.627991")
    private LocalDateTime modDt;
    @Schema(description = "수정자", nullable = false, example = "10000001")
    private Long modUsr;
    @Schema(description = "수정자 이름", nullable = false, example = "Yoon Jung Sun")
    private String modUsrName;

    @Builder
    public ProjectViewListDto(Long projId,
                              String projTitle,
                              String avatarFileUrl,
                              String pitchProjTypeCd,
                              String pitchProjTypeCdName,
                              Long projOwner,
                              String projOwnerName,
                              Long compProfileId,
                              String compProfileName,
                              String projGenreCd,
                              String projGenreName,
                              String projCondCd,
                              String projCondCdName,
                              LocalDateTime regDt,
                              Long regUsr,
                              String regUsrName,
                              LocalDateTime modDt,
                              Long modUsr,
                              String modUsrName,
                              YnTypeEnum completeYn
    ) {
        this.projId = projId;
        this.projTitle = projTitle;
        this.avatarFileUrl = avatarFileUrl;
        this.pitchProjTypeCd = pitchProjTypeCd;
        this.pitchProjTypeCdName = pitchProjTypeCdName;
        this.projOwner = projOwner;
        this.projOwnerName = projOwnerName;
        this.compProfileId = compProfileId;
        this.compProfileName = compProfileName;
        this.projGenreCd = projGenreCd;
        this.projGenreName = projGenreName;
        this.projCondCd = projCondCd;
        this.projCondCdName = projCondCdName;
        this.regDt = regDt;
        this.regUsr = regUsr;
        this.regUsrName = regUsrName;
        this.modDt = modDt;
        this.modUsr = modUsr;
        this.modUsrName = modUsrName;
        this.completeYn = completeYn;
    }
}
