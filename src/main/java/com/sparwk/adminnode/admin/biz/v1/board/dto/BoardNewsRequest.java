package com.sparwk.adminnode.admin.biz.v1.board.dto;

import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.util.List;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class BoardNewsRequest {

    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long newsId;
    @Schema(description = "카테고리 코드 9자리", nullable = false, example = "HSN000001")
    private String cateCd;
    @Schema(description = "카테고리 코드 9자리", nullable = false, example = "Music Industry")
    private String cateCdName;
    @Schema(description = "제목", nullable = false, example = "News & Post 제목입니다.")
    private String title;
    @Schema(description = "content 설명", nullable = true, example = "내용")
    private String content;
    @Schema(description = "첨부파일 사용여부 Y/N", nullable = false, defaultValue = "N", allowableValues = {"Y", "N"})
    private YnTypeEnum attachYn;
    @Schema(description = "노출여부 Y/N", nullable = false, defaultValue = "Y", allowableValues = {"Y", "N"})
    private YnTypeEnum useType;
    @Schema(description = "등록자", nullable = false, example = "10000001")
    private Long regUsr;
    @Schema(description = "수정자", nullable = false, example = "10000001")
    private Long modUsr;
    @Schema(description = "첨부파일", nullable = true)
    private List<BoardAttachDto> boardAttachDtoList;
}
