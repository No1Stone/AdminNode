package com.sparwk.adminnode.admin.biz.v1.account.dto;

import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Setter
@Getter
@NoArgsConstructor
public class VerifiedMemberListDto {

    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long accntId;
    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long profileId;
    @Schema(description = "ACCNT_TYPE_CD", nullable = false, example = "COMPANY / GROUP / PERSON")
    private String accntTypeCd;
    @Schema(description = "ACCNT EMAIL", nullable = false, example = "sparwk@sparwk.com")
    private String accntEmail;
    @Schema(description = "PASSPORT_FIRST_NAME", nullable = false, example = "Yoon")
    private String passportFirstName;
    @Schema(description = "PASSPORT_Middle_NAME", nullable = true, example = "Jung")
    private String passportMiddleName;
    @Schema(description = "PASSPORT_LAST_NAME", nullable = false, example = "Hun")
    private String passportLastName;
    @Schema(description = "ACCNT FULL NAME", nullable = false, example = "Mr.Yoon")
    private String profileFullName;
    @Schema(description = "STAGE_NAME_YN", nullable = false, defaultValue = "N", allowableValues = {"Y", "N"})
    private YnTypeEnum stageNameYn;
    @Schema(description = "countryCd", nullable = true, example = "CNT000001")
    private String countryCd;
    @Schema(description = "country Name", nullable = true, example = "USA")
    private String countryCdName;
    @Schema(description = "ipiInfo", nullable = true, example = "W123456789")
    private String ipiInfo;
    @Schema(description = "ipiInfo", nullable = true, example = "W123456789")
    private String caeInfo;
    @Schema(description = "isniInfo", nullable = true, example = "W123456789")
    private String isniInfo;
    @Schema(description = "등록일", pattern = "yyMMdd hh:mm:ss", example = "2021-12-08T05:44:26.627991")
    private LocalDateTime regDt;

    @Schema(description = "verifyYn", nullable = false, defaultValue = "Y", allowableValues = {"Y", "N"})
    private YnTypeEnum verifyYn;


    @Builder
    public VerifiedMemberListDto(Long accntId,
                                 Long profileId,
                                 String accntTypeCd,
                                 String accntEmail,
                                 String passportFirstName,
                                 String passportMiddleName,
                                 String passportLastName,
                                 String profileFullName,
                                 YnTypeEnum stageNameYn,
                                 String countryCd,
                                 String countryCdName,
                                 String ipiInfo,
                                 String caeInfo,
                                 String isniInfo,
                                 LocalDateTime regDt,
                                 YnTypeEnum verifyYn
    ) {
        this.accntId=accntId;
        this.profileId=profileId;
        this.accntTypeCd=accntTypeCd;
        this.accntEmail=accntEmail;
        this.passportFirstName=passportFirstName;
        this.passportMiddleName=passportMiddleName;
        this.passportLastName=passportLastName;
        this.profileFullName=profileFullName;
        this.stageNameYn=stageNameYn;
        this.countryCd=countryCd;
        this.countryCdName=countryCdName;
        this.ipiInfo=ipiInfo;
        this.caeInfo=caeInfo;
        this.isniInfo=isniInfo;
        this.regDt=regDt;
        this.verifyYn = verifyYn;

    }
}
