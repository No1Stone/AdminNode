package com.sparwk.adminnode.admin.biz.v1.languagePack.dto;

import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Setter
@Getter
@NoArgsConstructor
public class LanguagePackDto {

    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long packId;
    @Schema(description = "언어명", nullable = false, example = "LAN0000001")
    private String languageCd;
    @Schema(description = "언어명", nullable = false, example = "Korean")
    private String languageName;
    @Schema(description = "네이티브 언어명", nullable = false, example = "한국어")
    private String nativeLanguage;
    @Schema(description = "국기 이미지 url", nullable = false, example = "http://")
    private String nationalFlagUrl;
    @Schema(description = "json 언어팩 url", nullable = false, example = "http://")
    private String jsonUploadUrl;
    @Schema(description = "버전", nullable = false, example = "1")
    private int packVersion;
    @Schema(description = "코드 사용여부 Y/N", nullable = false, defaultValue = "Y", allowableValues = {"Y", "N"})
    private YnTypeEnum useType;
    @Schema(description = "등록일", pattern = "yyMMdd hh:mm:ss", example = "2021-12-08T05:44:26.627991")
    private LocalDateTime regDt;
    @Schema(description = "등록자", nullable = false, example = "10000001")
    private Long regUsr;
    @Schema(description = "등록자 이름", nullable = false, example = "Yoon Jung Sun")
    private String regUsrName;
    @Schema(description = "수정일", pattern = "yyMMdd hh:mm:ss", example = "2021-12-08T05:44:26.627991")
    private LocalDateTime modDt;
    @Schema(description = "수정자", nullable = false, example = "10000001")
    private Long modUsr;
    @Schema(description = "수정자 이름", nullable = false, example = "Yoon Jung Sun")
    private String modUsrName;

    @Builder
    public LanguagePackDto(Long packId,
                           String languageCd,
                           String languageName,
                           String nativeLanguage,
                           String nationalFlagUrl,
                           String jsonUploadUrl,
                           int packVersion,
                           YnTypeEnum useType,
                           LocalDateTime regDt,
                           Long regUsr,
                           String regUsrName,
                           LocalDateTime modDt,
                           Long modUsr,
                           String modUsrName
    ) {
        this.packId = packId;
        this.languageCd = languageCd;
        this.languageName = languageName;
        this.nativeLanguage = nativeLanguage;
        this.nationalFlagUrl = nationalFlagUrl;
        this.jsonUploadUrl = jsonUploadUrl;
        this.packVersion = packVersion;
        this.useType = useType;
        this.regDt = regDt;
        this.regUsr = regUsr;
        this.regUsrName = regUsrName;
        this.modDt = modDt;
        this.modUsr = modUsr;
        this.modUsrName = modUsrName;
    }
}
