package com.sparwk.adminnode.admin.biz.v1.permissionSet.dto;

import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class PermissionSetRequest {

    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long adminPermissionId;
    @Schema(description = "코드 값", nullable = false, example = "admin")
    private String labelVal;
    @Schema(description = "코드 사용여부 Y/N", nullable = false, defaultValue = "Y", allowableValues = {"Y", "N"})
    private YnTypeEnum useType;
    @Schema(description = "description 설명", nullable = true, example = "설명")
    private String cdDesc;
    @Schema(description = "코드 순번 숫자", nullable = true, example = "1")
    private int cdOrd;

    @Schema(description = "수정자")
    List<PermissionMenuSetDto> permissionMenuSetDto;

}