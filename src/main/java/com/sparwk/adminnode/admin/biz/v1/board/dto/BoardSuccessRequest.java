package com.sparwk.adminnode.admin.biz.v1.board.dto;

import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

@Setter
@Getter
@NoArgsConstructor
public class BoardSuccessRequest {

    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long successId;
    @Schema(description = "name", nullable = false, example = "Mr.Yoon")
    private String name;
    @Schema(description = "imageUrl", nullable = false, example = "http://")
    private String imageUrl;
    @Schema(description = "role text", nullable = true, example = "artist, creator")
    private String role;
    @Schema(description = "genre text", nullable = true, example = "hip hop, dance")
    private String genre;
    @Schema(description = "content 설명", nullable = true, example = "내용")
    private String content;
    @Schema(description = "노출여부 Y/N", nullable = false, defaultValue = "Y", allowableValues = {"Y", "N"})
    private YnTypeEnum useType;
    @Schema(description = "등록자", nullable = false, example = "10000001")
    private Long regUsr;
    @Schema(description = "수정자", nullable = false, example = "10000001")
    private Long modUsr;

}