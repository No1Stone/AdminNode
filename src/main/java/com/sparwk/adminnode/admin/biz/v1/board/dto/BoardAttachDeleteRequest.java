package com.sparwk.adminnode.admin.biz.v1.board.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class BoardAttachDeleteRequest {

    @Schema(description = "사용 Board", nullable = false, example = "1")
    private Long attacId;
    @Schema(description = "게시판 타입", nullable = false, example = "Question / Answer / FAQ / NEWS")
    private String boardType;
    @Schema(description = "사용 Board", nullable = false, example = "1")
    private Long boardId;

}
