package com.sparwk.adminnode.admin.biz.v1.song.service;

import com.sparwk.adminnode.admin.biz.v1.CommonPagingListDto;
import com.sparwk.adminnode.admin.biz.v1.admin.service.AdminActiveLogService;
import com.sparwk.adminnode.admin.biz.v1.song.dto.*;
import com.sparwk.adminnode.admin.config.common.ActiveCodeConst;
import com.sparwk.adminnode.admin.config.common.GetAccountId;
import com.sparwk.adminnode.admin.config.common.Response;
import com.sparwk.adminnode.admin.config.common.ResultCodeConst;
import com.sparwk.adminnode.admin.jpa.entity.PeriodTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.admin.AdminActiveLogEntity;
import com.sparwk.adminnode.admin.jpa.entity.anr.AnrEntity;
import com.sparwk.adminnode.admin.jpa.entity.commonCode.CommonDetailCodeEntity;
import com.sparwk.adminnode.admin.jpa.entity.profile.ProfileEntity;
import com.sparwk.adminnode.admin.jpa.entity.projects.ProjectEntity;
import com.sparwk.adminnode.admin.jpa.entity.projects.ProjectSong;
import com.sparwk.adminnode.admin.jpa.entity.song.*;
import com.sparwk.adminnode.admin.jpa.entity.songCode.SongDetailCodeEntity;
import com.sparwk.adminnode.admin.jpa.repository.anr.AnrRepository;
import com.sparwk.adminnode.admin.jpa.repository.commonCode.CommonDetailCodeRepository;
import com.sparwk.adminnode.admin.jpa.repository.profile.ProfileRepository;
import com.sparwk.adminnode.admin.jpa.repository.projects.ProjectRepository;
import com.sparwk.adminnode.admin.jpa.repository.projects.ProjectSongRepository;
import com.sparwk.adminnode.admin.jpa.repository.song.*;
import com.sparwk.adminnode.admin.jpa.repository.songCode.SongDetailCodeRepository;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class SongService {

    private final SongViewRepository songViewRepository;
    private final SongRepository songRepository;
    private final SongFileRepository songFileRepository;
    private final SongMetaCustomRepository songMetaCustomRepository;
    private final SongLyricsRepository songLyricsRepository;
    private final SongSplitsheetViewRepository songSplitsheetViewRepository;
    private final SongCreditsViewRepository songCreditsViewRepository;
    private final SongCowriterRepository songCowriterRepository;
    private final SongEvaluationRepository songEvaluationRepository;
    private final SongTimelineRepository songTimelineRepository;
    private final AnrRepository anrRepository;
    private final ProjectRepository projectRepository;
    private final ProjectSongRepository projectSongRepository;
    private final SongDetailCodeRepository songDetailCodeRepository;
    private final CommonDetailCodeRepository commonDetailCodeRepository;
    private final ProfileRepository profileRepository;
    private final AdminActiveLogService adminActiveLogService;

    private final HttpServletRequest httpServletRequest;
    private final MessageSourceAccessor messageSourceAccessor;
    
    private final Logger logger = LoggerFactory.getLogger(SongService.class);


    public Response<CommonPagingListDto> getList(SongCateEnum cate,
                                                 String val,
                                                 YnTypeEnum completeVal,
                                                 PeriodTypeEnum periodType,
                                                 String sdate,
                                                 String edate,
                                                 SongSorterEnum sorter,
                                                 PageRequest pageRequest) {

        Response<CommonPagingListDto> res = new Response<>();

        List<SongViewListDto> songList = songViewRepository.findQryAll(cate,
                val,
                completeVal,
                periodType,
                sdate,
                edate,
                sorter,
                pageRequest);

        logger.info("songList - {}", songList);

        //조회내용 없을 때
        if(songList == null || songList.size() < 1){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        for(SongViewListDto song : songList){
            List<SongMetaCustomEntity> songMetadataCustomList = songMetaCustomRepository.findBySongIdOrderByMetadataSeq(song.getSongId());

            song.setSongMetadataCustomList(songMetadataCustomList);
        }


        CommonPagingListDto list = new CommonPagingListDto();

        //총 카운트
        long totalSize = songViewRepository.countQryAll(cate,
                val,
                completeVal,
                periodType,
                sdate,
                edate);
        long totalPage = totalSize / pageRequest.getPageSize();
        long mod = totalSize % pageRequest.getPageSize();
        if(mod > 0) {
            totalPage += 1;
        }

        list.setList(songList);
        list.setSize(pageRequest.getPageSize());
        list.setPage(pageRequest.getPageNumber());
        list.setTotalPage(totalPage);
        list.setTotalSize(totalSize);
        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(list);
        return res;
    }

    //상세페이지
    public Response<Optional<SongDetailDto>> getOne(Long id) {

        Response<Optional<SongDetailDto>> res = new Response<>();

        //id값이 없을 때
        if(id == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<SongEntity> song = songRepository.findById(id);

        logger.info("song - {}", song);

        //조회내용 없을 때
        if(!song.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Song 오너 이름
        Optional<ProfileEntity> owner = profileRepository.findById(song.get().getSongOwner());

        logger.info("owner - {}", owner);

        //조회내용 없을 때
        if(!owner.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            logger.info("/////////////////owner");
            return res;
        }
        logger.info("//////////////////////////////----{}", song.get().getSongStatusCd());

        //Song Status 확인
        Optional<AnrEntity> statusCode = anrRepository.findByAnrCd(song.get().getSongStatusCd());
        String statusCodeName = (!statusCode.isPresent()) ? "" : statusCode.get().getAnrServiceName();

        //Song Detail 확인
        Optional<SongDetailCodeEntity> songDetailCode = songDetailCodeRepository.findByDcode(song.get().getSongVersionCd());

        logger.info("songDetailCode - {}", songDetailCode);

        //조회내용 없을 때
        if(!songDetailCode.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            logger.info("///////////////////songDetailCode");
            return res;
        }

        Optional<ProjectSong> projectSong = projectSongRepository.findBySongId(id);

        //조회내용 없을 때
        Long projId;
        String projTitle;
        String projOwner;
        String projType;
        if(!projectSong.isPresent()){
            projId = 0L;
            projTitle = "";
            projOwner = "";
            projType = "";
        } else {
            projId = projectSong.get().getProjId();

            //project 유무 확인
            Optional<ProjectEntity> project = projectRepository.findById(projectSong.get().getProjId());
            projTitle = project.get().getProjTitle();

            //Project 오너 이름
            Optional<ProfileEntity> projectOwner = profileRepository.findById(project.get().getProjOwner());
            projOwner = (!owner.isPresent()) ? "" : projectOwner.get().getFullName();


            Optional<CommonDetailCodeEntity> commonDetailCode = Optional.ofNullable(commonDetailCodeRepository.findByDcode(project.get().getPitchProjTypeCd()));
            projType = (!commonDetailCode.isPresent()) ? "" : commonDetailCode.get().getVal();
        }

        //Song File 자료
        Optional<SongFileDto> getSongFile = Optional.ofNullable(songFileRepository.findQrySongFile(id));
        getSongFile.get().setSoundType("Sound type");
        getSongFile.get().setBitDepth("Bit Depth");
        getSongFile.get().setSamplingRate("Sampling Rate");
        getSongFile.get().setDuration("Duration");
      //가사확인, 언어 포함
        Optional < SongLyricsEntity > songLyricsEntity = songLyricsRepository.findBySongId(id);
        String lyrics = (!songLyricsEntity.isPresent()) ? "" : songLyricsEntity.get().getLyrics();
        String lyricsComment = (!songLyricsEntity.isPresent()) ? "" : songLyricsEntity.get().getLyricsComt();
        List<SongLyricsLangDto> songLyricsLang = songLyricsRepository.findQryLyricsLang(id);


        //장르 리스트 String으로 추가
        List<SongMetadataDto> getSongGenreDataList = songRepository.findQrySongMeta("SGG", id);

        //서브장르 리스트 String으로 추가
        List<SongMetadataDto> getSongSubGenreDataList = songRepository.findQrySongMeta("SGS", id);

        //Theme 리스트 String으로 추가
        List<SongMetadataDto> getSongThemeDataList = songRepository.findQrySongMeta("SGT", id);

        //Mood 리스트 String으로 추가
        List<SongMetadataDto> getSongMoodDataList = songRepository.findQrySongMeta("SGM", id);

        //Vocal 리스트 String으로 추가
        List<SongMetadataDto> getSongVocalDataList = songRepository.findQrySongMeta("SGC", id);

        //Era 리스트 String으로 추가
        List<SongMetadataDto> getSongEraDataList = songRepository.findQrySongMeta("SGE", id);

        //Tempo 리스트 String으로 추가
        List<SongMetadataDto> getSongTempoDataList = songRepository.findQrySongMeta("SGP", id);

        //Instrument 리스트 String으로 추가
        List<SongMetadataDto> getSongInstrumentDataList = songRepository.findQrySongMeta("SGI", id);

        //custom meta
        List<SongMetaCustomEntity> getSongMetaCustomList = songMetaCustomRepository.findBySongIdOrderByMetadataSeq(id);

        //Split Sheet 자료
        List<SongSplitsheetViewEntity> getSongSplitsheetViewEntityList = songSplitsheetViewRepository.findBySongId(id);

        //Creaits 자료
        List<SongCreditsViewEntity> getSongCreditsViewEntityList = songCreditsViewRepository.findBySongId(id);
        Optional<List<SongCreditsViewEntity>> getSongCreditsViewEntityList2 = Optional.of(getSongCreditsViewEntityList);
        logger.info("<<<<<<<<<<<<<<<<<<<<<< getSongCreditsViewEntityList : {}",getSongCreditsViewEntityList );

        //Song Evaluation 자료
        List<SongEvaluationDto> getSongEvaluationList = songEvaluationRepository.findQryEvaluation(id);

        //Timeline 자료
        List<SongTimelineDto> getSongTimelineDtoList = songTimelineRepository.findQrySongTimeline(id);

               Optional<SongDetailDto> dtoPage =

                song.map(p -> SongDetailDto.builder()
                                .songId(p.getSongId())
                                .songTitle(p.getSongTitle())
                                .avatarFileUrl(p.getAvatarFileUrl())
                                .description(p.getDescription())
                                .songStatusCd(p.getSongStatusCd())
                                .songStatusCdName(statusCodeName)
                                .songSubTitle(p.getSongSubTitle())
                                .songVersionCd(p.getSongVersionCd())
                                .songVersionCdName(songDetailCode.get().getVal())
                                .userDefineVersion(p.getUserDefineVersion())
                                .songOwnerId(p.getSongOwner())
                                .songOwnerName(owner.get().getFullName())
                                .projId(projId)
                                .projTitle(projTitle)
                                .projOwner(projOwner)
                                .projType(projType)
                                .ISRC("ISRC")
                                .ISWC("ISWC")
                                .songFile(getSongFile)
                                .lyrics(lyrics)
                                .lyricsComment(lyricsComment)
                                .songLyricsLang(songLyricsLang)
                                .songGenreDataList(getSongGenreDataList)
                                .songSubGenreDataList(getSongSubGenreDataList)
                                .songThemeDataList(getSongThemeDataList)
                                .songMoodDataList(getSongMoodDataList)
                                .songVocalDataList(getSongVocalDataList)
                                .songEraDataList(getSongEraDataList)
                                .songTempoDataList(getSongTempoDataList)
                                .songInstrumentDataList(getSongInstrumentDataList)
                                .songMetaCustomList(getSongMetaCustomList)
                                .songSplitsheetViewEntityList(getSongSplitsheetViewEntityList)
                                .songCreditsViewEntityList(getSongCreditsViewEntityList)
                                .songEvaluationList(getSongEvaluationList)
                                .songTimelineDtoList(getSongTimelineDtoList)
                                .build()
                        );

        logger.info("SongDetailCode - {}", dtoPage);

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(dtoPage);
        return res;
    }




    public Response<List<SongEvaluationDtailListDto>> getEvaluationDetailList(Long id, Long evalAnrSeq) {

        Response<List<SongEvaluationDtailListDto>> res = new Response<>();

        List<SongEvaluationDtailListDto> songEvaluationList = songEvaluationRepository.findQryEvaluationDetailList(id, evalAnrSeq);

        logger.info("songEvaluationList - {}", songEvaluationList);

        //조회내용 없을 때
        if(songEvaluationList == null || songEvaluationList.size() < 1){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(songEvaluationList);
        return res;
    }


    //수정처리
    @Transactional
    public Response<Void> updateContent(Long id, SongModifyRequest request, HttpServletRequest req) {

        Response<Void> res = new Response<>();

        //id값이 없을 때
        if(id == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<SongEntity> song = songRepository.findById(id);

        logger.info("song - {} ", song);

        //조회내용 없을 때
        if(!song.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Song Title 값 없거나 틀릴 때
        if(request.getSongTitle() == null){
            res.setResultCd(ResultCodeConst.NOT_VALID_FORMAT.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        try {

            //수정
            song.get().update(
                    request.getSongTitle(),
                    request.getSongSubTitle(),
                    request.getDescription(),
                    request.getIswc(),
                    request.getIsrc()
            );

            //수정 재조회
            Optional<SongEntity> songUpdate =
                    songRepository.findById(song.get().getSongId());



            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));

            String menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_SONGS_LIST.getCode());
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_MODIFY.getCode());
            //netive query
            String usrName = adminActiveLogService.findQryUsrName(songUpdate.get().getModUsr());

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(songUpdate.get().getModDt())
                    .adminId(songUpdate.get().getModUsr())
                    .menuName(menuName)
                    .activeType("U")
                    .contentsId(songUpdate.get().getSongId())
                    .activeMsg(usrName + " " + activeMsg + " '" + songUpdate.get().getSongTitle() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e){
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }

        return res;
    }

    //수정처리
    @Transactional
    public Response<Void> updateSongfileContent(Long id, SongfileModifyRequest request, HttpServletRequest req) {

        Response<Void> res = new Response<>();

        //id값이 없을 때
        if(id == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        logger.info("!00000000000000");

        Optional<SongFileEntity> songfile = songFileRepository.findBySongIdAndSongFileSeq(id, request.getSongFileSeq());

        logger.info("!00000000000011111111");

        logger.info("songfile - {} ", songfile);



        //조회내용 없을 때
        if(!songfile.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        try {

            logger.info("!1111111111111");

            //수정
            songfile.get().update(
                    request.getSongFileComt()
            );

            logger.info("!2222222222222");

        } catch (Exception e){
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }

        return res;
    }

    //수정처리
    @Transactional
    public Response<Void> updateLyricsContent(Long id, SongLyricsModifyRequest request, HttpServletRequest req) {

        Response<Void> res = new Response<>();

        //id값이 없을 때
        if(id == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<SongLyricsEntity> songLyrics = songLyricsRepository.findBySongIdAndSongFileSeq(id, request.getSongSeq());

        logger.info("songLyrics - {} ", songLyrics);

        //조회내용 없을 때
        if(!songLyrics.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        try {

            //수정
            songLyrics.get().update(
                    request.getLyrics(),
                    request.getLyricsComt()
            );

            //수정 재조회
            Optional<SongLyricsEntity> songLyricsUpdate =
                    songLyricsRepository.findBySongIdAndSongFileSeq(songLyrics.get().getSongId(), songLyrics.get().getSongFileSeq());

        } catch (Exception e){
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }

        return res;
    }

    //수정처리
    @Transactional
    public Response<Void> updateSplitSheetContent(Long id, List<SongSplitSheetModifyRequest> request, HttpServletRequest req) {

        Response<Void> res = new Response<>();

        //id값이 없을 때
        if(id == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<SongEntity> song = songRepository.findById(id);

        logger.info("song - {} ", song);

        //조회내용 없을 때
        if(!song.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Song Title 값 없거나 틀릴 때
        if(request.size() == 0){
            res.setResultCd(ResultCodeConst.NOT_VALID_FORMAT.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }


        try {

            for(int i = 0; i < request.size(); i++ ){


                //수정 전 profile id 조회
                Optional<ProfileEntity> profile = profileRepository.findById(request.get(i).getProfileId());

                logger.info("profile - {} ", profile);

                //조회내용 없을 때
                if(!profile.isPresent()){
                    res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
                    res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
                    return res;
                }

                // isni 업데이트
                songRepository.updateQryIsniById(profile.get().getProfileId(), request.get(i).getIsni());

                // isni 업데이트
                songRepository.updateQryshareById(profile.get().getProfileId(), request.get(i).getRateShare());

            }

        } catch (Exception e){
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }

        return res;
    }

    //수정처리
    @Transactional
    public Response<Void> updateCreditContent(Long id, List<SongCreditModifyRequest> request, HttpServletRequest req) {

        Response<Void> res = new Response<>();

        //id값이 없을 때
        if(id == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<SongEntity> song = songRepository.findById(id);

        logger.info("song >>>>>>- {} ", song);

        //조회내용 없을 때
        if(!song.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Song Title 값 없거나 틀릴 때
        if(request.size() == 0){
            res.setResultCd(ResultCodeConst.NOT_VALID_FORMAT.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }


        try {

            for(int i = 0; i < request.size(); i++ ){


                //수정 전 profile id 조회
                Optional<ProfileEntity> profile = profileRepository.findById(request.get(i).getProfileId());

                logger.info("song - {} ", song);

                //조회내용 없을 때
                if(!profile.isPresent()){
                    res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
                    res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
                    return res;
                }

                // ipn 업데이트
                songRepository.updateQryIpnById(profile.get().getProfileId(), request.get(i).getIpn());

                // nro 업데이트
                songRepository.updateQryNroById(profile.get().getProfileId(), request.get(i).getNro());

            }

        } catch (Exception e){
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }

        return res;
    }

}
