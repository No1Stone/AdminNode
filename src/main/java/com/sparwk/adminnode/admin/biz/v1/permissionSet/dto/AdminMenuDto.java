package com.sparwk.adminnode.admin.biz.v1.permissionSet.dto;

import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Setter
@Getter
@NoArgsConstructor
public class AdminMenuDto {

    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long menuSeq;
    @Schema(description = "메뉴명", nullable = false, example = "SECURITY/IDENTITY")
    private String menuLabel;
    @Schema(description = "메뉴 뎁스, 레벨", nullable = false, example = "1")
    int menuDepth;
    @Schema(description = "부모Seq", nullable = true, example = "1L, 없으면 null 또는 0")
    private Long uppMenuSeq;
    @Schema(description = "메뉴 순서", nullable = false, example = "1")
    int sortIndex;
    @Schema(description = "description 설명", nullable = true, example = "설명")
    private String description;
    @Schema(description = "코드 사용여부 Y/N", nullable = false, defaultValue = "Y", allowableValues = {"Y", "N"})
    private YnTypeEnum useType;
    @Schema(description = "등록일", pattern = "yyMMdd hh:mm:ss", example = "2021-12-08T05:44:26.627991")
    private LocalDateTime regDt;
    @Schema(description = "등록자", nullable = false, example = "10000001")
    private Long regUsr;
    @Schema(description = "수정일", pattern = "yyMMdd hh:mm:ss", example = "2021-12-08T05:44:26.627991")
    private LocalDateTime modDt;
    @Schema(description = "수정자", nullable = false, example = "10000001")
    private Long modUsr;

    @Builder
    public AdminMenuDto(Long menuSeq,
                        String menuLabel,
                        int menuDepth,
                        Long uppMenuSeq,
                        int sortIndex,
                        String description,
                        YnTypeEnum useType,
                        LocalDateTime regDt,
                        Long regUsr,
                        LocalDateTime modDt,
                        Long modUsr
    ) {
        this.menuSeq = menuSeq;
        this.menuLabel = menuLabel;
        this.menuDepth = menuDepth;
        this.uppMenuSeq = uppMenuSeq;
        this.sortIndex = sortIndex;
        this.description = description;
        this.useType = (useType == YnTypeEnum.Y) ? YnTypeEnum.Y : YnTypeEnum.N;
        this.regDt = regDt;
        this.regUsr = regUsr;
        this.modDt = modDt;
        this.modUsr = modUsr;
    }

}
