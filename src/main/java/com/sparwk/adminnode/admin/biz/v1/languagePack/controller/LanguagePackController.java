package com.sparwk.adminnode.admin.biz.v1.languagePack.controller;

import com.sparwk.adminnode.admin.biz.v1.CommonPagingListDto;
import com.sparwk.adminnode.admin.biz.v1.language.dto.LanguageDto;
import com.sparwk.adminnode.admin.biz.v1.language.dto.LanguageRequest;
import com.sparwk.adminnode.admin.biz.v1.language.service.LanguageService;
import com.sparwk.adminnode.admin.biz.v1.languagePack.dto.LanguagePackDto;
import com.sparwk.adminnode.admin.biz.v1.languagePack.dto.LanguagePackRequest;
import com.sparwk.adminnode.admin.biz.v1.languagePack.service.LanguagePackService;
import com.sparwk.adminnode.admin.config.common.AdminPermissionIntergrationService;
import com.sparwk.adminnode.admin.config.common.Response;
import com.sparwk.adminnode.admin.config.common.ResultCodeConst;
import com.sparwk.adminnode.admin.config.util.ExcelXlsView;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.language.LanguageCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.language.LanguageEntity;
import com.sparwk.adminnode.admin.jpa.entity.language.LanguageSorterEnum;
import com.sparwk.adminnode.admin.jpa.entity.languagePack.LanguagePackCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.languagePack.LanguagePackSorterEnum;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.RequiredArgsConstructor;
import org.apache.commons.io.FilenameUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.data.domain.PageRequest;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

@RestController
@RequestMapping(value = "/V1/setting/administrator/")
@RequiredArgsConstructor
@CrossOrigin("*")
public class LanguagePackController {

    private final Logger logger = LoggerFactory.getLogger(LanguagePackController.class);

    @Autowired
    private LanguagePackService languagePackService;

    @Autowired
    private AdminPermissionIntergrationService adminPermissionIntergrationService;
    private final MessageSourceAccessor messageSourceAccessor;

    @Value("${file.path}")
    private String fileRealPath;

    @GetMapping("language-packs/language")
    @Operation(
            summary = "Language List", description = "Language List",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = LanguageDto.class))),
                    @ApiResponse(
                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
                            , content = @Content(schema = @Schema(hidden = true)))
            })
    public Response<List<LanguageEntity>> getLanguageList(
            @Parameter(name = "val", description = "코드 값 포함", in = ParameterIn.PATH) @RequestParam(value = "val", required = false) String val
    ) {
        return languagePackService.getLanguageList(YnTypeEnum.Y, val);
    }
    
    @GetMapping("language-packs")
    @Operation(
            summary = "Language Packs List", description = "Language Pack List",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = LanguageDto.class))),
                    @ApiResponse(
                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
                            , content = @Content(schema = @Schema(hidden = true)))
            })
    public Response<CommonPagingListDto> getList(
            @Parameter(name = "cate", description = "검색 종류", in = ParameterIn.PATH) @RequestParam(value = "cate", required = false) LanguagePackCateEnum cate,
            @Parameter(name = "val", description = "코드 값 포함", in = ParameterIn.PATH) @RequestParam(value = "val", required = false) String val,
            @Parameter(name = "useType", description = "사용여부 값 Y/N", in = ParameterIn.PATH) @RequestParam(value = "useType", required = false) YnTypeEnum useType,
            @Parameter(name = "sorter", description = "순서", in = ParameterIn.PATH) @RequestParam(value = "sorter", required = false) LanguagePackSorterEnum sorter,
            @Parameter(name = "page", description = "페이지 번호", in = ParameterIn.PATH) @RequestParam(value = "page", required = false, defaultValue = "1") int page,
            @Parameter(name = "size", description = "페이지 사이즈", in = ParameterIn.PATH) @RequestParam(value = "size", required = false, defaultValue = "10") int size

    ) {
        PageRequest pageRequest = PageRequest.of(page, size);

        Response res = new Response();
        if(!adminPermissionIntergrationService.AdminPermissionIntergrationCheck(3L, "R")){
            res.setResultCd(ResultCodeConst.NOT_VALID_READ.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        return languagePackService.getList(cate, val, useType, sorter, pageRequest);
    }

    //상세페이지
    @GetMapping("language-packs/{id}")
    @Operation(
            summary = "Language Detail View", description = "Common Detail Code View 입니다.",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = LanguageDto.class))),
                    @ApiResponse(
                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1003", description = "id가 존재하지 않습니다.."
                            , content = @Content(schema = @Schema(hidden = true)))
            })
    public Response<Optional<LanguagePackDto>> getOne(
            @Parameter(name = "id", description = "id 값", in = ParameterIn.PATH) @PathVariable(value = "id") Long id
    ) {
        Response res = new Response();
        if(!adminPermissionIntergrationService.AdminPermissionIntergrationCheck(3L, "R")){
            res.setResultCd(ResultCodeConst.NOT_VALID_READ.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        return languagePackService.getOne(id);
    }

    //쓰기처리
    @PostMapping("language-packs/new")
    @Operation(
            summary = "New Language", description = "Language 데이터 추가합니다.",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = LanguageDto.class))),
                    @ApiResponse(
                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1203", description = "Use Type 값 형식이 잘못되었습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1206", description = "Value 형식이 잘못되었습니다."
                            , content = @Content(schema = @Schema(hidden = true)))
            })
    public Response<Void> saveContent(
            @Parameter(name = "request", description = "Request Dto", in = ParameterIn.PATH) @RequestBody @Valid LanguagePackRequest request,
            HttpServletRequest req
    ) {
        Response res = new Response();
        if(!adminPermissionIntergrationService.AdminPermissionIntergrationCheck(3L, "C")){
            res.setResultCd(ResultCodeConst.NOT_VALID_CREATE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        return languagePackService.saveContent(request, req);
    }

    //사용여부 토글처리
    @PostMapping("language-packs/{id}/use")
    @Operation(
            summary = "Use Y/N", description = "Language 데이터 사용/미사용을 처리합니다.",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = LanguageDto.class))),
                    @ApiResponse(
                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1003", description = "id가 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true)))
            })
    public Response<Void> updateYn(
            @Parameter(name = "id", description = "id 값", in = ParameterIn.PATH) @PathVariable(value = "id") Long id,
            HttpServletRequest req
    ) {
        Response res = new Response();
        if(!adminPermissionIntergrationService.AdminPermissionIntergrationCheck(3L, "U")){
            res.setResultCd(ResultCodeConst.NOT_VALID_UPDATE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        return languagePackService.updateYn(id, req);
    }

    //삭제처리
    @DeleteMapping("language-packs/{id}/delete")
    @Operation(
            summary = "delete Language", description = "Language 데이터 삭제합니다.",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = LanguageDto.class))),
                    @ApiResponse(
                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1003", description = "id가 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true)))
            })
    public Response<Void> delete(
            @Parameter(name = "id", description = "id 값", in = ParameterIn.PATH) @PathVariable(value = "id") Long id,
            HttpServletRequest req
    ) {
        Response res = new Response();
        if(!adminPermissionIntergrationService.AdminPermissionIntergrationCheck(3L, "D")){
            res.setResultCd(ResultCodeConst.NOT_VALID_DELETE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        return languagePackService.deleteContent(id, req);
    }

}
