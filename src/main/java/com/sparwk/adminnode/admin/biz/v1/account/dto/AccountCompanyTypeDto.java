package com.sparwk.adminnode.admin.biz.v1.account.dto;

import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class AccountCompanyTypeDto {

    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long accntId;
    @Schema(description = "COMPANY_CD", nullable = false, example = "COP000001")
    private String companyTypeCd;
    @Schema(description = "COMPANY_CD NAME", nullable = false, example = "Music Publisher")
    private String companyTypeCdName;
    @Schema(description = "company_license_file_url", nullable = false, example = "http://")
    private String companyLicenseFileUrl;
    @Schema(description = "company_license_verify_yn", nullable = false, defaultValue = "N", allowableValues = {"Y", "N"})
    private YnTypeEnum companyLicenseVerifyYn;
    @Schema(description = "company_license_file_name", nullable = false, example = "http.zip")
    private String companyLicenseFileName;

    @Builder
    public AccountCompanyTypeDto(Long accntId,
                                 String companyTypeCd,
                                 String companyTypeCdName,
                                 String companyLicenseFileUrl,
                                 YnTypeEnum companyLicenseVerifyYn,
                                 String companyLicenseFileName
    ) {
        this.accntId = accntId;
        this.companyTypeCd = companyTypeCd;
        this.companyTypeCdName = companyTypeCdName;
        this.companyLicenseFileUrl = companyLicenseFileUrl;
        this.companyLicenseVerifyYn = companyLicenseVerifyYn;
        this.companyLicenseFileName = companyLicenseFileName;
    }
}
