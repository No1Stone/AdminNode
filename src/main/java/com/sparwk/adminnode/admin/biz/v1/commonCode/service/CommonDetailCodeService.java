package com.sparwk.adminnode.admin.biz.v1.commonCode.service;

import com.sparwk.adminnode.admin.biz.v1.CommonPagingListDto;
import com.sparwk.adminnode.admin.biz.v1.admin.service.AdminActiveLogService;
import com.sparwk.adminnode.admin.biz.v1.commonCode.dto.*;
import com.sparwk.adminnode.admin.config.common.*;
import com.sparwk.adminnode.admin.config.util.ExcelWriter;
import com.sparwk.adminnode.admin.jpa.entity.PeriodTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.admin.AdminActiveLogEntity;
import com.sparwk.adminnode.admin.jpa.entity.commonCode.*;
import com.sparwk.adminnode.admin.jpa.repository.commonCode.CommonCodeRepository;
import com.sparwk.adminnode.admin.jpa.repository.commonCode.CommonDetailCodeRepository;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class CommonDetailCodeService {

    private final CommonCodeRepository commonCodeRepository;
    private final CommonDetailCodeRepository commonDetailCodeRepository;
    private final AdminActiveLogService adminActiveLogService;

    private final HttpServletRequest httpServletRequest;
    private final MessageSourceAccessor messageSourceAccessor;

    private final Logger logger = LoggerFactory.getLogger(CommonDetailCodeService.class);


    public Response<CommonPagingListDto> getList(String pcode,
                                                 String code,
                                                 CommonCateEnum cate,
                                                 String val,
                                                 YnTypeEnum useType,
                                                 PeriodTypeEnum periodType,
                                                 String sdate,
                                                 String edate,
                                                 CommonSorterEnum sorter,
                                                 PageRequest pageRequest) {

        Response<CommonPagingListDto> res = new Response<>();

        //부모코드 없을 때
        if (pcode == null) {
            res.setResultCd(ResultCodeConst.NOT_EXIST_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<CommonCodeEntity> commonCode = Optional.ofNullable(
                commonCodeRepository.findByCode(pcode)
        );

        logger.info("commonCode - {}", commonCode);

        //부모코드 조회내용 없을 때
        if (!commonCode.isPresent()) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        List<CommonDetailCodeDto> commonDetailCodeList =
                commonDetailCodeRepository.findQryAll(pcode, code, cate, val, useType, periodType, sdate, edate, sorter, pageRequest);

        logger.info("CommonDetailCodeList - {}", commonDetailCodeList);

        //조회내용 없을 때
        if (commonDetailCodeList == null || commonDetailCodeList.size() < 1) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        CommonPagingListDto list = new CommonPagingListDto();

        //총 카운트
        long totalSize = commonDetailCodeRepository.countQryAll(pcode, code, cate, val, useType, periodType, sdate, edate);
        long totalPage = totalSize / pageRequest.getPageSize();
        long mod = totalSize % pageRequest.getPageSize();
        if(mod > 0) {
            totalPage += 1;
        }

        list.setList(commonDetailCodeList);
        list.setSize(pageRequest.getPageSize());
        list.setPage(pageRequest.getPageNumber());
        list.setTotalPage(totalPage);
        list.setTotalSize(totalSize);

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(list);
        return res;
    }

    public Response<CommonPagingListDto> getUseYList(String pcode, String code, CommonCateEnum cate, String val) {

        Response<CommonPagingListDto> res = new Response<>();

        //부모코드 없을 때
        if (pcode == null) {
            res.setResultCd(ResultCodeConst.NOT_EXIST_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<CommonCodeEntity> commonCode = Optional.ofNullable(
                commonCodeRepository.findByCode(pcode)
        );

        logger.info("commonCode - {}", commonCode);

        //부모코드 조회내용 없을 때
        if (!commonCode.isPresent()) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        List<CommonDetailCodeDto> commonDetailCodeList = commonDetailCodeRepository.findQryUseY(pcode, code, cate, val);

        logger.info("CommonDetailCodeList - {}", commonDetailCodeList);

        //조회내용 없을 때
        if (commonDetailCodeList == null || commonDetailCodeList.size() < 1) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        CommonPagingListDto list = new CommonPagingListDto();

        PageRequest pageRequest = PageRequest.of(10, 10);


        //총 카운트
        long totalSize = commonDetailCodeRepository.countQryUseY(pcode, code, cate, val);
        long totalPage = totalSize / pageRequest.getPageSize();
        long mod = totalSize % pageRequest.getPageSize();
        if(mod > 0) {
            totalPage += 1;
        }

        list.setList(commonDetailCodeList);
        list.setSize(pageRequest.getPageSize());
        list.setPage(pageRequest.getPageNumber());
        list.setTotalPage(totalPage);
        list.setTotalSize(totalSize);

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(list);
        return res;
    }

    public Response<CommonPagingListDto> getPopularYList(String pcode, String code, CommonCateEnum cate, String val) {

        Response<CommonPagingListDto> res = new Response<>();

        //부모코드 없을 때
        if (pcode == null) {
            res.setResultCd(ResultCodeConst.NOT_EXIST_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<CommonCodeEntity> commonCode = Optional.ofNullable(
                commonCodeRepository.findByCode(pcode)
        );

        logger.info("commonCode - {}", commonCode);

        //부모코드 조회내용 없을 때
        if (!commonCode.isPresent()) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        List<CommonDetailCodeDto> commonDetailCodeList = commonDetailCodeRepository.findPopularUseY(pcode, code, cate, val);

        logger.info("CommonDetailCodeList - {}", commonDetailCodeList);

        //조회내용 없을 때
        if (commonDetailCodeList == null || commonDetailCodeList.size() < 1) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        CommonPagingListDto list = new CommonPagingListDto();

        PageRequest pageRequest = PageRequest.of(10, 10);


        //총 카운트
        long totalSize = commonDetailCodeRepository.countPopularUseY(pcode, code, cate, val);
        long totalPage = totalSize / pageRequest.getPageSize();
        long mod = totalSize % pageRequest.getPageSize();
        if(mod > 0) {
            totalPage += 1;
        }

        list.setList(commonDetailCodeList);
        list.setSize(pageRequest.getPageSize());
        list.setPage(pageRequest.getPageNumber());
        list.setTotalPage(totalPage);
        list.setTotalSize(totalSize);

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(list);
        return res;
    }

    public Response<Optional<CommonDetailCodeDto>> getOne(String pcode, Long id) {

        Response<Optional<CommonDetailCodeDto>> res = new Response<>();

        //부모코드 없을 때
        if (pcode == null) {
            res.setResultCd(ResultCodeConst.NOT_EXIST_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //id값이 없을 때
        if (id == null) {
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<CommonCodeEntity> commonCode = Optional.ofNullable(
                commonCodeRepository.findByCode(pcode)
        );

        logger.info("commonCode - {}", commonCode);

        //부모코드 조회내용 없을 때
        if (!commonCode.isPresent()) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<CommonDetailCodeEntity> commonDetailCode = commonDetailCodeRepository.findById(id);

        logger.info("commonDetailCode - {}", commonDetailCode);

        //조회내용 없을 때
        if (!commonDetailCode.isPresent()) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<CommonDetailCodeDto> dtoPage =

                commonDetailCode
                        .map(p -> CommonDetailCodeDto.builder()
                                .commonDetailCodeSeq(p.getCommonDetailCodeSeq())
                                .pcode(p.getCommonCodeEntity().getCode())
                                .pcodeVal(p.getCommonCodeEntity().getVal())
                                .dcode(p.getDcode())
                                .val(p.getVal())
                                .useType(p.getUseType())
                                .popularType(p.getPopularType())
                                .description(p.getDescription())
                                .sortIndex(p.getSortIndex())
                                .hit(p.getHit())
                                .regUsr(p.getRegUsr())
                                .regDt(p.getRegDt())
                                .modUsr(p.getModUsr())
                                .modDt(p.getModDt())
                                .build()
                        );

        logger.info("CommonDetailCode - {}", dtoPage);

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(dtoPage);
        return res;
    }

    @Transactional
    public Response<Void> saveContent(String pcode, CommonDetailCodeRequest request, HttpServletRequest req) {

        Response<Void> res = new Response<>();
        String dcode;

        //부모코드 없을 때
        if (request.getCommonDetailCodeDto().getPcode() == null) {
            res.setResultCd(ResultCodeConst.NOT_VALID_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<CommonCodeEntity> commonCode = Optional.ofNullable(commonCodeRepository.findByCode(pcode));

        logger.info("commonCode - {}", commonCode);

        //부모코드 조회내용 없을 때
        if (!commonCode.isPresent()) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //USEYN 값 없거나 틀릴 때
        if (request.getCommonDetailCodeDto().getUseType() == null || (request.getCommonDetailCodeDto().getUseType() != YnTypeEnum.Y && request.getCommonDetailCodeDto().getUseType() != YnTypeEnum.N)) {
            res.setResultCd(ResultCodeConst.NOT_VALID_USEYN.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //POPULAR 값 없거나 틀릴 때
        if (request.getCommonDetailCodeDto().getPopularType() == null || (request.getCommonDetailCodeDto().getPopularType() != YnTypeEnum.Y && request.getCommonDetailCodeDto().getPopularType() != YnTypeEnum.N)) {
            res.setResultCd(ResultCodeConst.NOT_VALID_POPULAR.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if (request.getCommonDetailCodeDto().getVal() == null) {
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        try {

            //seq 번호 불러옴
            int k = commonDetailCodeRepository.getLastSeq(pcode) + 1;

            if(k < 10)
                dcode = pcode + "00000" + k;
            else if(k < 100)
                dcode = pcode + "0000" + k;
            else if(k < 1000)
                dcode = pcode + "000" + k;
            else if(k < 10000)
                dcode = pcode + "00" + k;
            else if(k < 100000)
                dcode = pcode + "0" + k;
            else if(k < 1000000)
                dcode = pcode + k;
            else
                dcode = pcode + k;

            CommonDetailCodeEntity newCommonDetailCode = CommonDetailCodeEntity.builder()
                    .commonCodeEntity(commonCode.get())
                    .pcode(commonCode.get().getCode())
                    .dcode(dcode)
                    .val(request.getCommonDetailCodeDto().getVal())
                    .description(request.getCommonDetailCodeDto().getDescription())
                    .sortIndex(k)
                    .useType(request.getCommonDetailCodeDto().getUseType())
                    .popularType(request.getCommonDetailCodeDto().getPopularType())
                    .hit(0)
                    .build();

            CommonDetailCodeEntity saved = commonDetailCodeRepository.saveAndFlush(newCommonDetailCode);

            //seq 번호 수동으로 업데이트 시킨다
            commonDetailCodeRepository.updateLastSeq(pcode, k);

//            Optional<CommonDetailCodeEntity> CommonDetailCode = commonDetailCodeRepository.findById(saved.getCommonDetailCodeSeq());
//
//            Optional<CommonDetailCodeDto> dtoPage =
//
//                    CommonDetailCode
//                            .map(p -> CommonDetailCodeDto.builder()
//                                    .commonDetailCodeSeq(p.getCommonDetailCodeSeq())
//                                    .pcode(p.getCommonCodeEntity().getCode())
//                                    .pcodeVal(p.getCommonCodeEntity().getVal())
//                                    .dcode(p.getDcode())
//                                    .val(p.getVal())
//                                    .description(p.getDescription())
//                                    .useType(p.getUseType())
//                                    .popularType(p.getPopularType())
//                                    .hit(p.getHit())
//                                    .regUsr(p.getRegUsr())
//                                    .regDt(p.getRegDt())
//                                    .modUsr(p.getModUsr())
//                                    .modDt(p.getModDt())
//                                    .build()
//                            );
//
//            return dtoPage;

            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
//            res.setResult(dtoPage);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));

            String menuName = "";
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_CREATE.getCode());
            switch(saved.getPcode()){
                case "SGN" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_CONNECTION_STATUS.getCode());
                    break;
                case "SSK" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_STUDIO_SKILL.getCode());
                    break;
                case "SGK" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_GENRE_SKILL.getCode());
                    break;
                case "SMK" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_MUSIC_SKILL.getCode());
                    break;
                case "SSF" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_SOFTWARE_SKILL.getCode());
                    break;
                case "SAV" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_AVAILABILITY.getCode());
                    break;
                case "IND" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_INDIVIDUAL_TYPE.getCode());
                    break;
                case "COP" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_COMPANY_TYPE.getCode());
                    break;
                case "GOV" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_GOVERMENT_TYPE.getCode());
                    break;
                case "UNI" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_UNION_TYPE.getCode());
                    break;
                case "ISO" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_ISO_CODE.getCode());
                    break;
                case "SIC" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_SIC_CODE.getCode());
                    break;
                case "RYL" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_ROYALTIES.getCode());
                    break;
                case "PJT" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_PROJECT_TYPE.getCode());
                    break;
                case "GND" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_GENDER.getCode());
                    break;
                default:
                    menuName = "ETC CODE " + saved.getPcode();
                    break;
            }

            //netive query
            String usrName = adminActiveLogService.findQryUsrName(saved.getRegUsr());

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(saved.getRegDt())
                    .adminId(saved.getRegUsr())
                    .menuName(menuName)
                    .activeType("C")
                    .contentsId(saved.getCommonDetailCodeSeq())
                    .activeMsg(usrName + " " + activeMsg + " '" + saved.getVal() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e) {
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }

        return res;
    }

    @Transactional
    public Response<Void> updateYn(String pcode, Long id, HttpServletRequest req) {

        Response<Void> res = new Response<>();

        //부모코드 없을 때
        if (pcode == null) {
            res.setResultCd(ResultCodeConst.NOT_EXIST_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<CommonCodeEntity> commonCode = Optional.ofNullable(
                commonCodeRepository.findByCode(pcode)
        );

        logger.info("commonCode - {}", commonCode);

        //부모코드 조회내용 없을 때
        if (!commonCode.isPresent()) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //id값이 없을 때
        if (id == null) {
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<CommonDetailCodeEntity> commonDetailCode = commonDetailCodeRepository.findById(id);

        logger.info("commonDetailCode - {}", commonDetailCode);

        //조회내용 없을 때
        if (!commonDetailCode.isPresent()) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        YnTypeEnum useType = commonDetailCode.get().getUseType();
        YnTypeEnum newUseType = (useType == YnTypeEnum.Y) ? YnTypeEnum.N : YnTypeEnum.Y;

        logger.info("newUseType - {} ", newUseType);

        try {
            commonDetailCode.get().updateYn(newUseType);
            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            //res.setResult(songDetailCode);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));

            String menuName = "";
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_MODIFY.getCode());
            switch(commonDetailCode.get().getPcode()){
                case "SGN" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_CONNECTION_STATUS.getCode());
                    break;
                case "SSK" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_STUDIO_SKILL.getCode());
                    break;
                case "SGK" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_GENRE_SKILL.getCode());
                    break;
                case "SMK" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_MUSIC_SKILL.getCode());
                    break;
                case "SSF" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_SOFTWARE_SKILL.getCode());
                    break;
                case "SAV" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_AVAILABILITY.getCode());
                    break;
                case "IND" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_INDIVIDUAL_TYPE.getCode());
                    break;
                case "COP" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_COMPANY_TYPE.getCode());
                    break;
                case "GOV" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_GOVERMENT_TYPE.getCode());
                    break;
                case "UNI" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_UNION_TYPE.getCode());
                    break;
                case "ISO" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_ISO_CODE.getCode());
                    break;
                case "SIC" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_SIC_CODE.getCode());
                    break;
                case "RYL" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_ROYALTIES.getCode());
                    break;
                case "PJT" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_PROJECT_TYPE.getCode());
                    break;
                case "GND" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_GENDER.getCode());
                    break;
                default:
                    menuName = "ETC CODE " + commonDetailCode.get().getPcode();
                    break;
            }


            String usrName = adminActiveLogService.findQryUsrName(commonDetailCode.get().getModUsr());

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(commonDetailCode.get().getModDt())
                    .adminId(commonDetailCode.get().getModUsr())
                    .menuName(menuName)
                    .activeType("U")
                    .contentsId(commonDetailCode.get().getCommonDetailCodeSeq())
                    .activeMsg(usrName + " " + activeMsg + " '" + commonDetailCode.get().getVal() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e) {
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }
        return res;
    }

    @Transactional
    public Response<Void> updatePopularYn(String pcode, Long id, HttpServletRequest req) {

        Response<Void> res = new Response<>();

        //부모코드 없을 때
        if (pcode == null) {
            res.setResultCd(ResultCodeConst.NOT_EXIST_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<CommonCodeEntity> commonCode = Optional.ofNullable(
                commonCodeRepository.findByCode(pcode)
        );

        logger.info("commonCode - {}", commonCode);

        //부모코드 조회내용 없을 때
        if (!commonCode.isPresent()) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //id값이 없을 때
        if (id == null) {
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<CommonDetailCodeEntity> commonDetailCode = commonDetailCodeRepository.findById(id);

        logger.info("commonDetailCode - {}", commonDetailCode);

        //조회내용 없을 때
        if (!commonDetailCode.isPresent()) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        YnTypeEnum popularType = commonDetailCode.get().getPopularType();
        YnTypeEnum newType = (popularType == YnTypeEnum.Y) ? YnTypeEnum.N : YnTypeEnum.Y;

        logger.info("newUseType - {} ", newType);

        try {
            commonDetailCode.get().updatePopularYn(newType);
            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            //res.setResult(songDetailCode);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));

            String menuName = "";
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_MODIFY.getCode());
            switch(commonDetailCode.get().getPcode()){
                case "SGN" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_CONNECTION_STATUS.getCode());
                    break;
                case "SSK" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_STUDIO_SKILL.getCode());
                    break;
                case "SGK" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_GENRE_SKILL.getCode());
                    break;
                case "SMK" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_MUSIC_SKILL.getCode());
                    break;
                case "SSF" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_SOFTWARE_SKILL.getCode());
                    break;
                case "SAV" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_AVAILABILITY.getCode());
                    break;
                case "IND" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_INDIVIDUAL_TYPE.getCode());
                    break;
                case "COP" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_COMPANY_TYPE.getCode());
                    break;
                case "GOV" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_GOVERMENT_TYPE.getCode());
                    break;
                case "UNI" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_UNION_TYPE.getCode());
                    break;
                case "ISO" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_ISO_CODE.getCode());
                    break;
                case "SIC" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_SIC_CODE.getCode());
                    break;
                case "RYL" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_ROYALTIES.getCode());
                    break;
                case "PJT" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_PROJECT_TYPE.getCode());
                    break;
                case "GND" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_GENDER.getCode());
                    break;
                default:
                    menuName = "ETC CODE " + commonDetailCode.get().getPcode();
                    break;
            }


            String usrName = adminActiveLogService.findQryUsrName(commonDetailCode.get().getModUsr());

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(commonDetailCode.get().getModDt())
                    .adminId(commonDetailCode.get().getModUsr())
                    .menuName(menuName)
                    .activeType("U")
                    .contentsId(commonDetailCode.get().getCommonDetailCodeSeq())
                    .activeMsg(usrName + " " + activeMsg + " '" + commonDetailCode.get().getVal() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e) {
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }
        return res;

    }

    @Transactional
    public Response<Optional<CommonDetailCodeDto>> updateContent(String pcode, Long id, CommonDetailCodeRequest request, HttpServletRequest req) {

        Response<Optional<CommonDetailCodeDto>> res = new Response<>();

        //부모코드 없을 때
        if (pcode == null) {
            res.setResultCd(ResultCodeConst.NOT_VALID_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<CommonCodeEntity> commonCode = Optional.ofNullable(commonCodeRepository.findByCode(pcode));

        logger.info("commonCode - {}", commonCode);

        //부모코드 조회내용 없을 때
        if (!commonCode.isPresent()) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //id값이 없을 때
        if (id == null) {
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<CommonDetailCodeEntity> commonDetailCode = commonDetailCodeRepository.findById(id);


        logger.info("commonDetailCode - {} ", commonDetailCode);

        //조회내용 없을 때
        if (!commonDetailCode.isPresent()) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //USEYN 값 없거나 틀릴 때
        if (request.getCommonDetailCodeDto().getUseType() == null || (request.getCommonDetailCodeDto().getUseType() != YnTypeEnum.Y && request.getCommonDetailCodeDto().getUseType() != YnTypeEnum.N)) {
            res.setResultCd(ResultCodeConst.NOT_VALID_USEYN.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //POPULAR 값 없거나 틀릴 때
        if (request.getCommonDetailCodeDto().getPopularType() == null || (request.getCommonDetailCodeDto().getPopularType() != YnTypeEnum.Y && request.getCommonDetailCodeDto().getPopularType() != YnTypeEnum.N)) {
            res.setResultCd(ResultCodeConst.NOT_VALID_POPULAR.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if (request.getCommonDetailCodeDto().getVal() == null) {
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        try {
            logger.info("/////////////////////////////// commonDetailCode - asdflkajsdflkja ");


            commonDetailCode.get().update(request.getCommonDetailCodeDto().getPcode(),
                    request.getCommonDetailCodeDto().getDcode(),
                    request.getCommonDetailCodeDto().getVal(),
                    request.getCommonDetailCodeDto().getDescription(),
                    request.getCommonDetailCodeDto().getSortIndex(),
                    request.getCommonDetailCodeDto().getUseType(),
                    request.getCommonDetailCodeDto().getPopularType());

            logger.info("/////////////////////////////// commonDetailCode - {} ", commonDetailCode.toString());

            Optional<CommonDetailCodeEntity> commonDetailCodeUpdate = commonDetailCodeRepository.findById(commonDetailCode.get().getCommonDetailCodeSeq());

            Optional<CommonDetailCodeDto> dtoPage =

                    commonDetailCodeUpdate
                            .map(p -> CommonDetailCodeDto.builder()
                                    .commonDetailCodeSeq(p.getCommonDetailCodeSeq())
                                    .pcode(p.getCommonCodeEntity().getCode())
                                    .pcodeVal(p.getCommonCodeEntity().getVal())
                                    .dcode(p.getDcode())
                                    .val(p.getVal())
                                    .useType(p.getUseType())
                                    .popularType(p.getPopularType())
                                    .description(p.getDescription())
                                    .sortIndex(p.getSortIndex())
                                    .hit(p.getHit())
                                    .regUsr(p.getRegUsr())
                                    .regDt(p.getRegDt())
                                    .modUsr(p.getModUsr())
                                    .modDt(p.getModDt())
                                    .build()
                            );

            logger.info("dtoPage - {} ", dtoPage);

            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            res.setResult(dtoPage);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));

            String menuName = "";
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_MODIFY.getCode());
            switch(commonDetailCode.get().getPcode()){
                case "SGN" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_CONNECTION_STATUS.getCode());
                    break;
                case "SSK" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_STUDIO_SKILL.getCode());
                    break;
                case "SGK" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_GENRE_SKILL.getCode());
                    break;
                case "SMK" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_MUSIC_SKILL.getCode());
                    break;
                case "SSF" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_SOFTWARE_SKILL.getCode());
                    break;
                case "SAV" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_AVAILABILITY.getCode());
                    break;
                case "IND" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_INDIVIDUAL_TYPE.getCode());
                    break;
                case "COP" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_COMPANY_TYPE.getCode());
                    break;
                case "GOV" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_GOVERMENT_TYPE.getCode());
                    break;
                case "UNI" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_UNION_TYPE.getCode());
                    break;
                case "ISO" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_ISO_CODE.getCode());
                    break;
                case "SIC" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_SIC_CODE.getCode());
                    break;
                case "RYL" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_ROYALTIES.getCode());
                    break;
                case "PJT" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_PROJECT_TYPE.getCode());
                    break;
                case "GND" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_GENDER.getCode());
                    break;
                default:
                    menuName = "ETC CODE " + commonDetailCode.get().getPcode();
                    break;
            }


            String usrName = adminActiveLogService.findQryUsrName(commonDetailCode.get().getModUsr());

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(commonDetailCode.get().getModDt())
                    .adminId(commonDetailCode.get().getModUsr())
                    .menuName(menuName)
                    .activeType("U")
                    .contentsId(commonDetailCode.get().getCommonDetailCodeSeq())
                    .activeMsg(usrName + " " + activeMsg + " '" + commonDetailCode.get().getVal() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e) {
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }

        return res;
    }

    @Transactional
    public Response<Void> deleteContent(Long id, HttpServletRequest req) {

        Response<Void> res = new Response<>();

        //id값이 없을 때
        if (id == null) {
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<CommonDetailCodeEntity> commonDetailCode = commonDetailCodeRepository.findById(id);

        logger.info("commonDetailCode - {} ", commonDetailCode);

        //조회내용 없을 때
        if (!commonDetailCode.isPresent()) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        try {
            commonDetailCodeRepository.deleteById(id);
            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
//            res.setResult(dtoPage);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));
            Long adminId = getAccountId.ofId(httpServletRequest);

            String menuName = "";
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_DELETE.getCode());
            switch(commonDetailCode.get().getPcode()){
                case "SGN" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_CONNECTION_STATUS.getCode());
                    break;
                case "SSK" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_STUDIO_SKILL.getCode());
                    break;
                case "SGK" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_GENRE_SKILL.getCode());
                    break;
                case "SMK" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_MUSIC_SKILL.getCode());
                    break;
                case "SSF" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_SOFTWARE_SKILL.getCode());
                    break;
                case "SAV" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_AVAILABILITY.getCode());
                    break;
                case "IND" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_INDIVIDUAL_TYPE.getCode());
                    break;
                case "COP" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_COMPANY_TYPE.getCode());
                    break;
                case "GOV" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_GOVERMENT_TYPE.getCode());
                    break;
                case "UNI" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_UNION_TYPE.getCode());
                    break;
                case "ISO" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_ISO_CODE.getCode());
                    break;
                case "SIC" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_SIC_CODE.getCode());
                    break;
                case "RYL" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_ROYALTIES.getCode());
                    break;
                case "PJT" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_PROJECT_TYPE.getCode());
                    break;
                case "GND" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_GENDER.getCode());
                    break;
                default:
                    menuName = "ETC CODE " + commonDetailCode.get().getPcode();
                    break;
            }


            String usrName = adminActiveLogService.findQryUsrName(adminId);

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(LocalDateTime.now())
                    .adminId(adminId)
                    .menuName(menuName)
                    .activeType("D")
                    .contentsId(commonDetailCode.get().getCommonDetailCodeSeq())
                    .activeMsg(usrName + " " + activeMsg + " '" + commonDetailCode.get().getVal() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e) {
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }
        return res;

    }

    public List<CommonDetailCodeExcelDto> getExcelList(String pcode) {

        List<CommonDetailCodeExcelDto> getExcelList = commonDetailCodeRepository.findExcelList(pcode);

        logger.info("getExcelList - {} ", getExcelList);

        return getExcelList;
    }


    //엑셀 다운로드 요청
    public Map<String, Object> downExcelList(String commonCode) throws Exception {
        // 데이터 가져오기
        List<CommonDetailCodeExcelDto> excelList = this.getExcelList(commonCode);

        logger.info("excelList - {} ", excelList);

        return ExcelWriter.createExcelData(excelList, CommonDetailCodeExcelDto.class);
    }

    //마지막 코드 번호 확인
    public Integer getLastSeq(String code) {

        return commonDetailCodeRepository.getLastSeq(code);
    }

    //코드 번호 업데이트
    @Transactional
    public void updateLastSeq(String code, Integer k) {

        commonDetailCodeRepository.updateLastSeq(code, k);
    }

    //이름으로 코드검색
    public Long getOneName(String val) {

        return commonDetailCodeRepository.countByVal(val);
    }

    @Transactional
    public Response<Void> saveExcelContent(String pcode, CommonDetailCodeDto commonDetailCodeDto, HttpServletRequest req) {

        Response<Void> res = new Response<>();

        Optional<CommonCodeEntity> commonCode = Optional.ofNullable(commonCodeRepository.findByCode(pcode));

        logger.info("commonCode - {}", commonCode);

        //부모코드 조회내용 없을 때
        if (!commonCode.isPresent() || commonCode.equals("")) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //USEYN 값 없거나 틀릴 때
        if (commonDetailCodeDto.getUseType() == null || (commonDetailCodeDto.getUseType() != YnTypeEnum.Y && commonDetailCodeDto.getUseType() != YnTypeEnum.N)) {
            res.setResultCd(ResultCodeConst.NOT_VALID_USEYN.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //POPULAR 값 없거나 틀릴 때
        if (commonDetailCodeDto.getPopularType() == null || (commonDetailCodeDto.getPopularType() != YnTypeEnum.Y && commonDetailCodeDto.getPopularType() != YnTypeEnum.N)) {
            res.setResultCd(ResultCodeConst.NOT_VALID_POPULAR.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if (commonDetailCodeDto.getVal() == null || commonDetailCodeDto.getVal().equals("")) {
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        try {

            CommonDetailCodeEntity newCommonDetailCode = CommonDetailCodeEntity.builder()
                    .commonCodeEntity(commonCode.get())
                    .pcode(commonCode.get().getCode())
                    .dcode(commonDetailCodeDto.getDcode())
                    .val(commonDetailCodeDto.getVal())
                    .description(commonDetailCodeDto.getDescription())
                    .sortIndex(commonDetailCodeDto.getSortIndex())
                    .useType(commonDetailCodeDto.getUseType())
                    .popularType(YnTypeEnum.N)
                    .hit(0)
                    .build();

            CommonDetailCodeEntity saved = commonDetailCodeRepository.save(newCommonDetailCode);

            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
//            res.setResult(dtoPage);

        } catch (Exception e) {
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }

        return res;
    }

    //카테고리 보기
    public Response<CommonPagingListDto> getCateList(CommonCateEnum2 cate,
                                                     String val,
                                                     YnTypeEnum useType,
                                                     PeriodTypeEnum periodType,
                                                     String sdate,
                                                     String edate,
                                                     CommonSorterEnum sorter,
                                                     PageRequest pageRequest) {

        Response<CommonPagingListDto> res = new Response<>();

        List<CommonDetailCodeDto> commonDetailCodeList =
                commonDetailCodeRepository.findQryCateUseY(cate, val, useType,sorter, pageRequest);

        logger.info("CommonDetailCodeList - {}", commonDetailCodeList);

        //조회내용 없을 때
        if (commonDetailCodeList == null || commonDetailCodeList.size() < 1) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        CommonPagingListDto list = new CommonPagingListDto();

        //총 카운트
        long totalSize = commonDetailCodeRepository.countQryCateUseY(cate, val, useType);
        long totalPage = totalSize / pageRequest.getPageSize();
        long mod = totalSize % pageRequest.getPageSize();
        if(mod > 0) {
            totalPage += 1;
        }

        list.setList(commonDetailCodeList);
        list.setSize(pageRequest.getPageSize());
        list.setPage(pageRequest.getPageNumber());
        list.setTotalPage(totalPage);
        list.setTotalSize(totalSize);

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(list);
        return res;
    }

    ///////////////////// 카테고리 셋팅 /////////////////////////////////////////////////////////////////////////////
    //카테고리 셋팅
    public Response<CommonPagingListDto> getCateCodeList(CommonCateEnum2 cate,
                                                     String val,
                                                     YnTypeEnum useType,
                                                     CommonSorterEnum3 sorter,
                                                     PageRequest pageRequest) {

        Response<CommonPagingListDto> res = new Response<>();

        List<CateCodeListDto> commonDetailCodeList =
                commonDetailCodeRepository.findQryCateCodeList(cate, val, useType,sorter, pageRequest);

        logger.info("CommonDetailCodeList - {}", commonDetailCodeList);

        //조회내용 없을 때
        if (commonDetailCodeList == null || commonDetailCodeList.size() < 1) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        CommonPagingListDto list = new CommonPagingListDto();

        //총 카운트
        long totalSize = commonDetailCodeRepository.countQryCateCodeList(cate, val, useType);
        long totalPage = totalSize / pageRequest.getPageSize();
        long mod = totalSize % pageRequest.getPageSize();
        if(mod > 0) {
            totalPage += 1;
        }

        list.setList(commonDetailCodeList);
        list.setSize(pageRequest.getPageSize());
        list.setPage(pageRequest.getPageNumber());
        list.setTotalPage(totalPage);
        list.setTotalSize(totalSize);

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(list);
        return res;
    }

    public Response<Optional<CommonDetailCodeDto>> getCateOne(Long id) {

        Response<Optional<CommonDetailCodeDto>> res = new Response<>();

        //id값이 없을 때
        if (id == null) {
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<CommonDetailCodeEntity> commonDetailCode = commonDetailCodeRepository.findById(id);

        logger.info("commonDetailCode - {}", commonDetailCode);

        //조회내용 없을 때
        if (!commonDetailCode.isPresent()) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<CommonDetailCodeDto> dtoPage =

                commonDetailCode
                        .map(p -> CommonDetailCodeDto.builder()
                                .commonDetailCodeSeq(p.getCommonDetailCodeSeq())
                                .pcode(p.getCommonCodeEntity().getCode())
                                .pcodeVal(p.getCommonCodeEntity().getVal())
                                .dcode(p.getDcode())
                                .val(p.getVal())
                                .useType(p.getUseType())
                                .popularType(p.getPopularType())
                                .description(p.getDescription())
                                .sortIndex(p.getSortIndex())
                                .hit(p.getHit())
                                .regUsr(p.getRegUsr())
                                .regDt(p.getRegDt())
                                .modUsr(p.getModUsr())
                                .modDt(p.getModDt())
                                .build()
                        );

        logger.info("CommonDetailCode - {}", dtoPage);

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(dtoPage);
        return res;
    }

    @Transactional
    public Response<Void> saveCateContent(CateCodeRequest request, HttpServletRequest req) {

        Response<Void> res = new Response<>();
        String dcode;

        logger.info(">>>>>>>>>>>>>>>>>>>>> request.getPcode() : ",request.getPcode());

        //부모코드 조회값 없을 때
        if (request.getPcode() == null) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<CommonCodeEntity> commonCode = Optional.ofNullable(commonCodeRepository.findByCode(request.getPcode()));

        logger.info("commonCode - {}", commonCode);

        //부모코드 조회내용 없을 때
        if (!commonCode.isPresent() || commonCode.equals("")) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if (request.getVal() == null) {
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        try {

            //seq 번호 불러옴
            String pcode = request.getPcode();
            int k = commonDetailCodeRepository.getLastSeq(pcode) + 1;

            if(k < 10)
                dcode = pcode + "00000" + k;
            else if(k < 100)
                dcode = pcode + "0000" + k;
            else if(k < 1000)
                dcode = pcode + "000" + k;
            else if(k < 10000)
                dcode = pcode + "00" + k;
            else if(k < 100000)
                dcode = pcode + "0" + k;
            else if(k < 1000000)
                dcode = pcode + k;
            else
                dcode = pcode + k;

            logger.info("dcode - {}", dcode);

            CommonDetailCodeEntity newCommonDetailCode = CommonDetailCodeEntity.builder()
                    .pcode(request.getPcode())
                    .dcode(dcode)
                    .val(request.getVal())
                    .description("")
                    .sortIndex(k)
                    .useType(request.getUseType())
                    .popularType(YnTypeEnum.N)
                    .hit(0)
                    .build();

            CommonDetailCodeEntity saved = commonDetailCodeRepository.save(newCommonDetailCode);

            logger.info("saved - {}", saved.getVal());

            //seq 번호 수동으로 업데이트 시킨다
            commonDetailCodeRepository.updateLastSeq(pcode, k);

//            Optional<CommonDetailCodeEntity> CommonDetailCode = commonDetailCodeRepository.findById(saved.getCommonDetailCodeSeq());
//
//            Optional<CommonDetailCodeDto> dtoPage =
//
//                    CommonDetailCode
//                            .map(p -> CommonDetailCodeDto.builder()
//                                    .commonDetailCodeSeq(p.getCommonDetailCodeSeq())
//                                    .pcode(p.getCommonCodeEntity().getCode())
//                                    .pcodeVal(p.getCommonCodeEntity().getVal())
//                                    .dcode(p.getDcode())
//                                    .val(p.getVal())
//                                    .description(p.getDescription())
//                                    .useType(p.getUseType())
//                                    .popularType(p.getPopularType())
//                                    .hit(p.getHit())
//                                    .regUsr(p.getRegUsr())
//                                    .regDt(p.getRegDt())
//                                    .modUsr(p.getModUsr())
//                                    .modDt(p.getModDt())
//                                    .build()
//                            );
//
//            return dtoPage;

            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
//            res.setResult(dtoPage);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));

            String menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_CATEGORY_SETTING.getCode());
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_CREATE.getCode());

            String usrName = adminActiveLogService.findQryUsrName(saved.getRegUsr());

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(saved.getRegDt())
                    .adminId(saved.getRegUsr())
                    .menuName(menuName)
                    .activeType("C")
                    .contentsId(saved.getCommonDetailCodeSeq())
                    .activeMsg(usrName + " " + activeMsg + " '" + saved.getVal() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e) {
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }

        return res;
    }



    @Transactional
    public Response<Optional<CateCodeListDto>> updateCateContent(Long id, CateCodeRequest request, HttpServletRequest req) {

        Response<Optional<CateCodeListDto>> res = new Response<>();

        //부모코드 없을 때
        if (request.getPcode() == null) {
            res.setResultCd(ResultCodeConst.NOT_VALID_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<CommonCodeEntity> commonCode = Optional.ofNullable(commonCodeRepository.findByCode(request.getPcode()));

        logger.info("commonCode - {}", commonCode);

        //부모코드 조회내용 없을 때
        if (!commonCode.isPresent()) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //id값이 없을 때
        if (id == null) {
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<CommonDetailCodeEntity> commonDetailCode = commonDetailCodeRepository.findById(id);


        logger.info("commonDetailCode - {} ", commonDetailCode);

        //조회내용 없을 때
        if (!commonDetailCode.isPresent()) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //USEYN 값 없거나 틀릴 때
        if (request.getUseType() == null || (request.getUseType() != YnTypeEnum.Y && request.getUseType() != YnTypeEnum.N)) {
            res.setResultCd(ResultCodeConst.NOT_VALID_USEYN.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if (request.getVal() == null) {
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        try {
            logger.info("/////////////////////////////// commonDetailCode - asdflkajsdflkja ");


            commonDetailCode.get().cateUpdate(request.getPcode(),
                    request.getVal(),
                    request.getUseType()
            );

            logger.info("/////////////////////////////// commonDetailCode - {} ", commonDetailCode.toString());

            Optional<CommonDetailCodeEntity> commonDetailCodeUpdate = commonDetailCodeRepository.findById(commonDetailCode.get().getCommonDetailCodeSeq());


            Optional<CateCodeListDto> dtoPage =

                    commonDetailCodeUpdate
                            .map(p -> CateCodeListDto.builder()
                                    .commonDetailCodeSeq(p.getCommonDetailCodeSeq())
                                    .pcode(p.getCommonCodeEntity().getCode())
                                    .pcodeVal(p.getCommonCodeEntity().getVal())
                                    .dcode(p.getDcode())
                                    .val(p.getVal())
                                    .useType(p.getUseType())
                                    .sortIndex(p.getSortIndex())
                                    .regUsr(p.getRegUsr())
                                    .regDt(p.getRegDt())
                                    .modUsr(p.getModUsr())
                                    .modDt(p.getModDt())
                                    .build()
                            );

            logger.info("dtoPage - {} ", dtoPage);

            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            res.setResult(dtoPage);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));

            String menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_CATEGORY_SETTING.getCode());
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_MODIFY.getCode());

            String usrName = adminActiveLogService.findQryUsrName(commonDetailCode.get().getModUsr());

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(commonDetailCode.get().getModDt())
                    .adminId(commonDetailCode.get().getModUsr())
                    .menuName(menuName)
                    .activeType("U")
                    .contentsId(commonDetailCode.get().getCommonDetailCodeSeq())
                    .activeMsg(usrName + " " + activeMsg + " '" + commonDetailCode.get().getVal() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e) {
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }

        return res;
    }

    @Transactional
    public Response<Void> deleteCateContent(Long id, HttpServletRequest req) {

        Response<Void> res = new Response<>();

        //id값이 없을 때
        if (id == null) {
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<CommonDetailCodeEntity> commonDetailCode = commonDetailCodeRepository.findById(id);

        logger.info("commonDetailCode - {} ", commonDetailCode);

        //조회내용 없을 때
        if (!commonDetailCode.isPresent()) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        try {
            commonDetailCodeRepository.deleteById(id);
            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
//            res.setResult(dtoPage);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));
            Long adminId = getAccountId.ofId(httpServletRequest);

            String menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_CATEGORY_SETTING.getCode());
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_DELETE.getCode());

            String usrName = adminActiveLogService.findQryUsrName(adminId);

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(LocalDateTime.now())
                    .adminId(adminId)
                    .menuName(menuName)
                    .activeType("D")
                    .contentsId(commonDetailCode.get().getCommonDetailCodeSeq())
                    .activeMsg(usrName + " " + activeMsg + " '" + commonDetailCode.get().getVal() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e) {
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }
        return res;

    }


    public Response<List<CommonCodeDto>> getCatePCodeList() {

        Response<List<CommonCodeDto>> res = new Response<>();

        List<CommonCodeDto> list = commonDetailCodeRepository.findQryCatePCodeList();

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(list);
        return res;
    }
}
