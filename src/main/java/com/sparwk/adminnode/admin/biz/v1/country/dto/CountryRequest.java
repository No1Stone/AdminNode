package com.sparwk.adminnode.admin.biz.v1.country.dto;

import lombok.*;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class CountryRequest {

//    private String country;
//    private String countryCd;
//    private String iso2;
//    private String iso3;
//    private String continentCode;
//    private String nmr;
//    private String dial;
//    private UseType useType;
//    private String description;
//
//    @Builder
//    public CountryRequest(
//                          String country,
//                          String countryCd,
//                          String iso2,
//                          String iso3,
//                          String continentCode,
//                          String nmr,
//                          UseType useType,
//                          String description,
//                          String dial
//    ) {
//        this.country = country;
//        this.countryCd = countryCd;
//        this.iso2 = iso2;
//        this.iso3 = iso3;
//        this.continentCode = continentCode;
//        this.nmr = nmr;
//        this.dial = dial;
//        this.useType = useType;
//        this.description = description;
//    }

    private CountryDto countryDto;
}
