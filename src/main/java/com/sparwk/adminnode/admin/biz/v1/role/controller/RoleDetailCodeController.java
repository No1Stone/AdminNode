package com.sparwk.adminnode.admin.biz.v1.role.controller;

import com.sparwk.adminnode.admin.biz.v1.CommonPagingListDto;
import com.sparwk.adminnode.admin.biz.v1.role.dto.RoleDetailCodeDto;
import com.sparwk.adminnode.admin.biz.v1.role.dto.RoleDetailCodeRequest;
import com.sparwk.adminnode.admin.biz.v1.role.service.RoleDetailCodeService;
import com.sparwk.adminnode.admin.config.common.AdminPermissionIntergrationService;
import com.sparwk.adminnode.admin.config.common.Response;
import com.sparwk.adminnode.admin.config.common.ResultCodeConst;
import com.sparwk.adminnode.admin.config.util.ExcelXlsView;
import com.sparwk.adminnode.admin.jpa.entity.FormatTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.PeriodTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.role.RoleCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.role.RoleDetailCodeSorterEnum;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.RequiredArgsConstructor;
import org.apache.commons.io.FilenameUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.data.domain.PageRequest;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

@RestController
@RequestMapping(value = "/V1/setting/identifire/")
@RequiredArgsConstructor
@CrossOrigin("*")
public class RoleDetailCodeController {

    private final Logger logger = LoggerFactory.getLogger(RoleDetailCodeController.class);

    @Autowired
    private RoleDetailCodeService roleDetailCodeService;
    @Autowired
    private AdminPermissionIntergrationService adminPermissionIntergrationService;
    private final MessageSourceAccessor messageSourceAccessor;

    @Value("${file.path}")
    private String fileRealPath;

    @GetMapping("role-codes/{roleCode}")
    @Operation(
            summary = "Role Detail Code All List", description = "Role Detail Code All List 입니다.",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = RoleDetailCodeDto.class))),
                    @ApiResponse(
                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1001", description = "부모코드가 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1101", description = "부모코드 값이 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true)))
            })
    public Response<CommonPagingListDto> getList(
            @Parameter(name = "roleCode", description = "부모코드", in = ParameterIn.PATH) @PathVariable("roleCode") String roleCode,
            @Parameter(name = "cate", description = "검색 종류", in = ParameterIn.PATH) @RequestParam(value = "cate", required = false) RoleCateEnum cate,
            @Parameter(name = "val", description = "검색어 포함", in = ParameterIn.PATH) @RequestParam(value = "val", required = false) String val,
            @Parameter(name = "formatType", description = "format 값 DDEX/SPARWK", in = ParameterIn.PATH) @RequestParam(value = "formatType", required = false) FormatTypeEnum formatType,
            @Parameter(name = "useType", description = "사용여부 값 Y/N", in = ParameterIn.PATH) @RequestParam(value = "useType", required = false) YnTypeEnum useType,
            @Parameter(name = "periodType", description = "기간 종류", in = ParameterIn.PATH) @RequestParam(value = "periodType", required = false) PeriodTypeEnum periodType,
            @Parameter(name = "sdate", description = "시작일", in = ParameterIn.PATH) @RequestParam(value = "sdate", required = false) String sdate,
            @Parameter(name = "edate", description = "종료일", in = ParameterIn.PATH) @RequestParam(value = "edate", required = false) String edate,
            @Parameter(name = "sorter", description = "순서", in = ParameterIn.PATH) @RequestParam(value = "sorter", required = false) RoleDetailCodeSorterEnum sorter,
            @Parameter(name = "page", description = "페이지 번호", in = ParameterIn.PATH) @RequestParam(value = "page", required = false, defaultValue = "1") int page,
            @Parameter(name = "size", description = "페이지 사이즈", in = ParameterIn.PATH) @RequestParam(value = "size", required = false, defaultValue = "10") int size
    ) {
        PageRequest pageRequest = PageRequest.of(page, size);

        Response res = new Response();
        if(!adminPermissionIntergrationService.AdminPermissionIntergrationCheck(4L, "R")){
            res.setResultCd(ResultCodeConst.NOT_VALID_READ.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        return roleDetailCodeService.getList(roleCode, cate, val, formatType, useType, periodType, sdate, edate, sorter, pageRequest);
    }


    @GetMapping("role-codes/{roleCode}/use")
    @Operation(
            summary = "Role Detail Code Use List", description = "Role Detail Code Use List 입니다.",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = RoleDetailCodeDto.class))),
                    @ApiResponse(
                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1001", description = "부모코드가 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1101", description = "부모코드 값이 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true)))
            })
    public Response<CommonPagingListDto> getUseYList(
            @Parameter(name = "roleCode", description = "부모코드", in = ParameterIn.PATH) @PathVariable("roleCode") String roleCode,
            @Parameter(name = "cate", description = "검색 종류", in = ParameterIn.PATH) @RequestParam(value = "cate", required = false) RoleCateEnum cate,
            @Parameter(name = "val", description = "검색어 포함", in = ParameterIn.PATH) @RequestParam(value = "val", required = false) String val,
            @Parameter(name = "formatType", description = "format 값 DDEX/SPARWK", in = ParameterIn.PATH) @RequestParam(value = "formatType", required = false) FormatTypeEnum formatType,
            @Parameter(name = "sorter", description = "순번확인", in = ParameterIn.PATH) @RequestParam(value = "sorter", required = false) RoleDetailCodeSorterEnum sorter
    ) {
        Response res = new Response();
        if(!adminPermissionIntergrationService.AdminPermissionIntergrationCheck(4L, "R")){
            res.setResultCd(ResultCodeConst.NOT_VALID_READ.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        return roleDetailCodeService.getUseYList(roleCode, cate, sorter, val, formatType);
    }

    @GetMapping("role-codes/{roleCode}/popular")
    @Operation(
            summary = "Popular Role Code List", description = "Role Detail Code Popular List 입니다.",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = RoleDetailCodeDto.class))),
                    @ApiResponse(
                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1001", description = "부모코드가 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1101", description = "부모코드 값이 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true)))
            })
    public Response<CommonPagingListDto> getPopularYist(
            @Parameter(name = "roleCode", description = "부모코드", in = ParameterIn.PATH) @PathVariable("roleCode") String roleCode,
            @Parameter(name = "sorter", description = "순서", in = ParameterIn.PATH) @RequestParam(value = "sorter", required = false) RoleDetailCodeSorterEnum sorter
    ) {
        Response res = new Response();
        if(!adminPermissionIntergrationService.AdminPermissionIntergrationCheck(4L, "R")){
            res.setResultCd(ResultCodeConst.NOT_VALID_READ.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        return roleDetailCodeService.getPopularYList(roleCode, sorter);
    }

    @GetMapping("role-codes/{roleCode}/matching")
    @Operation(
            summary = "Matching Role Code List", description = "Matching Role Code List 입니다.",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = RoleDetailCodeDto.class))),
                    @ApiResponse(
                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1001", description = "부모코드가 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1101", description = "부모코드 값이 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true)))
            })
    public Response<CommonPagingListDto> getMatchingYist(
            @Parameter(name = "roleCode", description = "부모코드", in = ParameterIn.PATH) @PathVariable("roleCode") String roleCode,
            @Parameter(name = "sorter", description = "순서", in = ParameterIn.PATH) @RequestParam(value = "sorter", required = false) RoleDetailCodeSorterEnum sorter
    ) {
        Response res = new Response();
        if(!adminPermissionIntergrationService.AdminPermissionIntergrationCheck(4L, "R")){
            res.setResultCd(ResultCodeConst.NOT_VALID_READ.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        return roleDetailCodeService.getMatchingYList(roleCode, sorter);
    }

    @GetMapping("role-codes/{roleCode}/credit")
    @Operation(
            summary = "Credit Role Code List", description = "Credit Role Code List 입니다.",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = RoleDetailCodeDto.class))),
                    @ApiResponse(
                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1001", description = "부모코드가 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1101", description = "부모코드 값이 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true)))
            })
    public Response<CommonPagingListDto> getCreditYist(
            @Parameter(name = "roleCode", description = "부모코드", in = ParameterIn.PATH) @PathVariable("roleCode") String roleCode,
            @Parameter(name = "sorter", description = "순서", in = ParameterIn.PATH) @RequestParam(value = "sorter", required = false) RoleDetailCodeSorterEnum sorter
    ) {
        Response res = new Response();
        if(!adminPermissionIntergrationService.AdminPermissionIntergrationCheck(4L, "R")){
            res.setResultCd(ResultCodeConst.NOT_VALID_READ.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        return roleDetailCodeService.getCreditYList(roleCode, sorter);
    }

    @GetMapping("role-codes/{roleCode}/split")
    @Operation(
            summary = "SplitSheet Role Code List", description = "SplitSheet Role Code List 입니다.",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = RoleDetailCodeDto.class))),
                    @ApiResponse(
                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1001", description = "부모코드가 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1101", description = "부모코드 값이 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true)))
            })
    public Response<CommonPagingListDto> getSplitSheetYist(
            @Parameter(name = "roleCode", description = "부모코드", in = ParameterIn.PATH) @PathVariable("roleCode") String roleCode,
            @Parameter(name = "sorter", description = "순서", in = ParameterIn.PATH) @RequestParam(value = "sorter", required = false) RoleDetailCodeSorterEnum sorter
    ) {
        Response res = new Response();
        if(!adminPermissionIntergrationService.AdminPermissionIntergrationCheck(4L, "R")){
            res.setResultCd(ResultCodeConst.NOT_VALID_READ.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        return roleDetailCodeService.getSplitSheetYList(roleCode, sorter);
    }

    //상세페이지
    @GetMapping("role-codes/{roleCode}/{id}")
    @Operation(
            summary = "Role Detail Code View", description = "Role Detail Code View 입니다.",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = RoleDetailCodeDto.class))),
                    @ApiResponse(
                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1001", description = "부모코드가 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1101", description = "부모코드 값이 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1003", description = "id가 존재하지 않습니다.."
                            , content = @Content(schema = @Schema(hidden = true)))
            })
    public Response<Optional<RoleDetailCodeDto>> getOne(
            @Parameter(name = "roleCode", description = "부모코드", in = ParameterIn.PATH) @PathVariable(value = "roleCode") String roleCode,
            @Parameter(name = "id", description = "id 값", in = ParameterIn.PATH) @PathVariable(value = "id") Long id
    ) {
        Response res = new Response();
        if(!adminPermissionIntergrationService.AdminPermissionIntergrationCheck(4L, "R")){
            res.setResultCd(ResultCodeConst.NOT_VALID_READ.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        return roleDetailCodeService.getOne(roleCode, id);
    }

    //쓰기처리
    @PostMapping("role-codes/{roleCode}/new")
    @Operation(
            summary = "New Role Code", description = "Role Detail Code 추가합니다.",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = RoleDetailCodeDto.class))),
                    @ApiResponse(
                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1001", description = "부모코드가 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1101", description = "부모코드 값이 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1204", description = "Format Type 값 형식이 잘못되었습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1203", description = "Use Type 값 형식이 잘못되었습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1205", description = "Popular Type 값 형식이 잘못되었습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1206", description = "Value 형식이 잘못되었습니다."
                            , content = @Content(schema = @Schema(hidden = true)))
            })
    public Response<Void> saveContent(
            @Parameter(name = "roleCode", description = "부모코드", in = ParameterIn.PATH) @PathVariable("roleCode") String roleCode,
            @Parameter(name = "request", description = "Request Dto", in = ParameterIn.PATH) @RequestBody @Valid RoleDetailCodeRequest request,
            HttpServletRequest req
    ) {
        Response res = new Response();
        if(!adminPermissionIntergrationService.AdminPermissionIntergrationCheck(4L, "C")){
            res.setResultCd(ResultCodeConst.NOT_VALID_CREATE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        return roleDetailCodeService.saveContent(roleCode, request, req);
    }

    //사용여부 토글처리
    @PostMapping("role-codes/{roleCode}/{id}/use")
    @Operation(
            summary = "Use Y/N", description = "Role Detail Code 사용/미사용을 처리합니다.",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = RoleDetailCodeDto.class))),
                    @ApiResponse(
                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1001", description = "부모코드가 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1101", description = "부모코드 값이 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1003", description = "id가 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true)))
            })
    public Response<Void> updateYn(
            @Parameter(name = "roleCode", description = "부모코드", in = ParameterIn.PATH) @PathVariable(value = "roleCode") String roleCode,
            @Parameter(name = "id", description = "id 값", in = ParameterIn.PATH) @PathVariable(value = "id") Long id,
            HttpServletRequest req
    ) {
        Response res = new Response();
        if(!adminPermissionIntergrationService.AdminPermissionIntergrationCheck(4L, "U")){
            res.setResultCd(ResultCodeConst.NOT_VALID_UPDATE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        return roleDetailCodeService.updateYn(roleCode, id, req);
    }

    //popular 토글처리
    @PostMapping("role-codes/{roleCode}/{id}/popular")
    @Operation(
            summary = "Popular Y/N", description = "Role Code Popular 체크합니다.",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = RoleDetailCodeDto.class))),
                    @ApiResponse(
                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1001", description = "부모코드가 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1101", description = "부모코드 값이 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1003", description = "id가 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true)))
            })
    public Response<Void> updatePopularYn(
            @Parameter(name = "roleCode", description = "부모코드", in = ParameterIn.PATH) @PathVariable(value = "roleCode") String roleCode,
            @Parameter(name = "id", description = "id 값", in = ParameterIn.PATH) @PathVariable(value = "id") Long id,
            HttpServletRequest req
    ) {
        Response res = new Response();
        if(!adminPermissionIntergrationService.AdminPermissionIntergrationCheck(4L, "U")){
            res.setResultCd(ResultCodeConst.NOT_VALID_UPDATE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        return roleDetailCodeService.updatePopularYn(roleCode, id, req);
    }

    //Matching Role 여부 토글처리
    @PostMapping("role-codes/{roleCode}/{id}/matching")
    @Operation(
            summary = "Matching Role Y/N", description = "Matching Role 사용/미사용을 처리합니다.",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = RoleDetailCodeDto.class))),
                    @ApiResponse(
                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1001", description = "부모코드가 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1101", description = "부모코드 값이 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1003", description = "id가 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true)))
            })
    public Response<Void> updateMatchingRoleYn(
            @Parameter(name = "roleCode", description = "부모코드", in = ParameterIn.PATH) @PathVariable(value = "roleCode") String roleCode,
            @Parameter(name = "id", description = "id 값", in = ParameterIn.PATH) @PathVariable(value = "id") Long id,
            HttpServletRequest req
    ) {
        Response res = new Response();
        if(!adminPermissionIntergrationService.AdminPermissionIntergrationCheck(4L, "U")){
            res.setResultCd(ResultCodeConst.NOT_VALID_UPDATE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        return roleDetailCodeService.updateMatchingRoleYn(roleCode, id, req);
    }

    //Matching Role 여부 토글처리
    @PostMapping("role-codes/{roleCode}/{id}/credit")
    @Operation(
            summary = "Credit Role Y/N", description = "Credit Role 사용/미사용을 처리합니다.",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = RoleDetailCodeDto.class))),
                    @ApiResponse(
                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1001", description = "부모코드가 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1101", description = "부모코드 값이 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1003", description = "id가 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true)))
            })
    public Response<Void> updateCreditRoleYn(
            @Parameter(name = "roleCode", description = "부모코드", in = ParameterIn.PATH) @PathVariable(value = "roleCode") String roleCode,
            @Parameter(name = "id", description = "id 값", in = ParameterIn.PATH) @PathVariable(value = "id") Long id,
            HttpServletRequest req
    ) {
        Response res = new Response();
        if(!adminPermissionIntergrationService.AdminPermissionIntergrationCheck(4L, "U")){
            res.setResultCd(ResultCodeConst.NOT_VALID_UPDATE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        return roleDetailCodeService.updateCreditRoleYn(roleCode, id, req);
    }

    //Matching Role 여부 토글처리
    @PostMapping("role-codes/{roleCode}/{id}/split")
    @Operation(
            summary = "SplitSheet Role Y/N", description = "SplitSheet Role 사용/미사용을 처리합니다.",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = RoleDetailCodeDto.class))),
                    @ApiResponse(
                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1001", description = "부모코드가 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1101", description = "부모코드 값이 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1003", description = "id가 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true)))
            })
    public Response<Void> updateSplitSheetRoleYn(
            @Parameter(name = "roleCode", description = "부모코드", in = ParameterIn.PATH) @PathVariable(value = "roleCode") String roleCode,
            @Parameter(name = "id", description = "id 값", in = ParameterIn.PATH) @PathVariable(value = "id") Long id,
            HttpServletRequest req
    ) {
        Response res = new Response();
        if(!adminPermissionIntergrationService.AdminPermissionIntergrationCheck(4L, "U")){
            res.setResultCd(ResultCodeConst.NOT_VALID_UPDATE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        return roleDetailCodeService.updateSplitSheetRoleYn(roleCode, id, req);
    }

    //업데이트처리
    @PostMapping("role-codes/{roleCode}/{id}/modify")
    @Operation(
            summary = "Modify Role Code", description = "Role Detail Code 수정합니다.",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = RoleDetailCodeDto.class))),
                    @ApiResponse(
                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1001", description = "부모코드가 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1101", description = "부모코드 값이 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1003", description = "id가 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1204", description = "Format Type 값 형식이 잘못되었습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1203", description = "Use Type 값 형식이 잘못되었습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1205", description = "Popular Type 값 형식이 잘못되었습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1206", description = "Value 형식이 잘못되었습니다."
                            , content = @Content(schema = @Schema(hidden = true)))
            })
    public Response<Optional<RoleDetailCodeDto>> updateContent(
            @Parameter(name = "roleCode", description = "부모코드", in = ParameterIn.PATH) @PathVariable(value = "roleCode") String roleCode,
            @Parameter(name = "id", description = "id 값", in = ParameterIn.PATH) @PathVariable(value = "id") Long id,
            @Parameter(name = "request", description = "Request Dto", in = ParameterIn.PATH) @RequestBody @Valid RoleDetailCodeRequest request,
            HttpServletRequest req
    ) {
        Response res = new Response();
        if(!adminPermissionIntergrationService.AdminPermissionIntergrationCheck(4L, "U")){
            res.setResultCd(ResultCodeConst.NOT_VALID_UPDATE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        return roleDetailCodeService.updateContent(roleCode, id, request, req);
    }

    //삭제처리
    @DeleteMapping("role-codes/{roleCode}/{id}/delete")
    @Operation(
            summary = "delete Role Code", description = "Role Detail Code 삭제합니다.",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = RoleDetailCodeDto.class))),
                    @ApiResponse(
                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1001", description = "부모코드가 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1101", description = "부모코드 값이 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1003", description = "id가 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true)))
            })
    public Response<Void> delete(
            @Parameter(name = "roleCode", description = "부모코드", in = ParameterIn.PATH) @PathVariable(value = "roleCode") String roleCode,
            @Parameter(name = "id", description = "id 값", in = ParameterIn.PATH) @PathVariable(value = "id") Long id,
            HttpServletRequest req
    ) {
        Response res = new Response();
        if(!adminPermissionIntergrationService.AdminPermissionIntergrationCheck(4L, "D")){
            res.setResultCd(ResultCodeConst.NOT_VALID_DELETE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        return roleDetailCodeService.deleteContent(roleCode, id, req);
    }

    //EXCEL DOWN
    @GetMapping("role-codes/down-excel")
    @Operation(
            summary = "Role Code List Excel Down", description = "Role Detail Code Excel Down 입니다.",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "Role Detail Code List Excel Down"
                            , content = @Content(schema = @Schema(implementation = RoleDetailCodeDto.class)))
            })
    public ModelAndView getExcelDownList(
            @Parameter(name = "roleCode", description = "부모코드", in = ParameterIn.PATH) @RequestParam(value = "roleCode", required = true) String roleCode,
            HttpServletRequest request
    ) {
        Map<String, Object> excelData = null;
        try {
            excelData = roleDetailCodeService.downExcelList(roleCode);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ModelAndView(new ExcelXlsView(), excelData);
    }

    // @ResponseBody 생략가능. class 에 @RestController
    @PostMapping(value="role-codes/{roleCode}/upload-excel")
    public Response uploadFile(
            @Parameter(name = "roleCode", description = "부모코드", in = ParameterIn.PATH) @PathVariable(value = "roleCode") String roleCode,
            @RequestPart(value="key", required=false) @Valid RoleDetailCodeRequest request,
            @RequestParam("file") MultipartFile file,
            Model model,
            HttpServletRequest req
    ) throws IllegalStateException, IOException {

        Response res = new Response();
        if(!adminPermissionIntergrationService.AdminPermissionIntergrationCheck(4L, "C")){
            res.setResultCd(ResultCodeConst.NOT_VALID_CREATE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        if( !file.isEmpty() ) {
            logger.info("file org name = {}", file.getOriginalFilename());
            logger.info("file content type = {}", file.getContentType());
            logger.info("file content type = {}", file.getSize());
            logger.info("file content type = {}", file.getName());
            //file.transferTo(new File(file.getOriginalFilename()));
        }

        List<RoleDetailCodeDto> dataList = new ArrayList<>();

        String extension = FilenameUtils.getExtension(file.getOriginalFilename()); // 3

        if (!extension.equals("xlsx") && !extension.equals("xls")) {
            throw new IOException("엑셀파일만 업로드 해주세요.");
        }


        System.out.println(fileRealPath);

        UUID uuid = UUID.randomUUID();
        String uuidFilename = uuid+"_"+file.getOriginalFilename();


        //이미지 저장 위치: 외부에 잡기/ 나중에 이 경로랑 파일명만 db에 저장하면 된다.
        //nio 로 임포트
        Path filePath = Paths.get(fileRealPath+uuidFilename);

        logger.info("{}", filePath.toString());

        try {
            Files.write(filePath,file.getBytes());
            //얘가 스레드 새로 만들어서 만약 10메가 넘어가면 여기서 파일 넣는동안 메인스레드가 ok 뿌려서 엑박뜸->
            //사진 용량 제한시키기 아니면 메인스레드 기다리게하기(비동기처리)

            Workbook workbook = null;

            if (extension.equals("xlsx")) {
                workbook = new XSSFWorkbook(file.getInputStream());
            } else if (extension.equals("xls")) {
                workbook = new HSSFWorkbook(file.getInputStream());
            }

            Sheet worksheet = workbook.getSheetAt(0);

            Integer k = roleDetailCodeService.getLastSeq(roleCode);
            Integer kk = 0;
            String dcode;
            String format;
            String role;
            String desc;
            String matchingYn;
            String createYn;
            String splitSheetYn;
            FormatTypeEnum newFormat;
            YnTypeEnum newMatchingYn;
            YnTypeEnum newCreateYn;
            YnTypeEnum newSplitSheetYn;


            for (int i = 2; i < worksheet.getPhysicalNumberOfRows(); i++) { // 4

                Row row = worksheet.getRow(i);

                k++;

                if(k < 10)
                    dcode = roleCode + "00000" + k;
                else if(k < 100)
                    dcode = roleCode + "0000" + k;
                else if(k < 1000)
                    dcode = roleCode + "000" + k;
                else if(k < 10000)
                    dcode = roleCode + "00" + k;
                else if(k < 100000)
                    dcode = roleCode + "0" + k;
                else if(k < 1000000)
                    dcode = roleCode + k;
                else
                    dcode = roleCode + k;

                format = (row.getCell(1) == null) ? "" : row.getCell(1).getStringCellValue();
                role = (row.getCell(2) == null) ? "" : row.getCell(2).getStringCellValue();
                desc = (row.getCell(3) == null) ? "" : row.getCell(3).getStringCellValue();
                matchingYn = (row.getCell(4) == null) ? "" : row.getCell(4).getStringCellValue();
                createYn = (row.getCell(5) == null) ? "" : row.getCell(5).getStringCellValue();
                splitSheetYn = (row.getCell(6) == null) ? "" : row.getCell(6).getStringCellValue();

                newFormat = (format.equals(String.valueOf(FormatTypeEnum.DDEX))) ? FormatTypeEnum.DDEX : FormatTypeEnum.SPARWK;
                newMatchingYn = (matchingYn.equals(String.valueOf(YnTypeEnum.Y))) ? YnTypeEnum.Y : YnTypeEnum.N;
                newCreateYn = (createYn.equals(String.valueOf(YnTypeEnum.Y))) ? YnTypeEnum.Y : YnTypeEnum.N;
                newSplitSheetYn = (splitSheetYn.equals(String.valueOf(YnTypeEnum.Y))) ? YnTypeEnum.Y : YnTypeEnum.N;

                RoleDetailCodeDto roleDetailCodeDtoDto = new RoleDetailCodeDto();

                roleDetailCodeDtoDto.setPcode(roleCode);
                roleDetailCodeDtoDto.setDcode(dcode);
                roleDetailCodeDtoDto.setFormatType(newFormat);
                roleDetailCodeDtoDto.setRole(role);
                roleDetailCodeDtoDto.setDescription(desc);
                roleDetailCodeDtoDto.setUseType(YnTypeEnum.Y);
                roleDetailCodeDtoDto.setPopularType(YnTypeEnum.N);
                roleDetailCodeDtoDto.setMatchingRoleYn(newMatchingYn);
                roleDetailCodeDtoDto.setCreditRoleYn(newCreateYn);
                roleDetailCodeDtoDto.setSplitSheetRoleYn(newSplitSheetYn);
                //숫자일경우
                //testExcelDto.setCdesc(row.getCell(3).getNumericCellValue());

//                logger.info("--------------------------------------------------------");
//                logger.info("val - {} : {}", val, row.getCell(1) );
//                logger.info("country - {} : {}", country, row.getCell(2) );
//                logger.info("desc - {} : {}", desc, row.getCell(3) );
//                logger.info("--------------------------------------------------------");

                //logger.info("val - {}", val);
                Long countVal = roleDetailCodeService.getOneName(role);
                //logger.info("countVal - {}", countVal);
                if(countVal > 0){
                    res.setResultCd(ResultCodeConst.NOT_VALID_OVERLAP.getCode());
                    res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
                    return res;
                }

                dataList.add(roleDetailCodeDtoDto);

                logger.info("commonOrganizationCodeDto - {}", roleDetailCodeDtoDto.toString());
                Response<Void> result = roleDetailCodeService.saveExcelContent(roleCode, roleDetailCodeDtoDto, req);
                if (result.getResultCd().equals("0000")) {
                    kk++;
                }

            }

            //성공해야만 seq 업데이트
            if(kk > 0) {
                roleDetailCodeService.updateLastSeq(roleCode, k);
            }

            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }

        return res;
    }
}
