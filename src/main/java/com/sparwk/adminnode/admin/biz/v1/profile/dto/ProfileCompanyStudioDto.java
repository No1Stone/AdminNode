package com.sparwk.adminnode.admin.biz.v1.profile.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class ProfileCompanyStudioDto {

    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long profileId;
    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long studioId;
    @Schema(description = "studio Name", nullable = false, example = "SPARWK")
    private String studioName;
    @Schema(description = "business_location_cd", nullable = false, example = "CNT000001")
    private String businessLocationCd;
    @Schema(description = "business_location_cd", nullable = true, example = "CNT000001")
    private String businessLocationCdName;
    @Schema(description = "post_cd", nullable = true, example = "12345")
    private String postCd;
    @Schema(description = "region", nullable = true, example = "korea ")
    private String region;
    @Schema(description = "city", nullable = true, example = "seoul")
    private String city;
    @Schema(description = "addr1", nullable = true, example = "Jung-gu, Seoul, Republic of Korea")
    private String addr1;
    @Schema(description = "addr2", nullable = true, example = "72, Sejong-daero")
    private String addr2;


    @Builder
    public ProfileCompanyStudioDto(Long profileId,
                                   Long studioId,
                                   String studioName,
                                   String businessLocationCd,
                                   String businessLocationCdName,
                                   String postCd,
                                   String region,
                                   String city,
                                   String addr1,
                                   String addr2
    ) {
        this.profileId = profileId;
        this.studioId = studioId;
        this.studioName = studioName;
        this.businessLocationCdName = businessLocationCdName;
        this.businessLocationCd = businessLocationCd;
        this.postCd = postCd;
        this.region = region;
        this.city = city;
        this.addr1 = addr1;
        this.addr2 = addr2;
    }
}
