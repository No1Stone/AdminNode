package com.sparwk.adminnode.admin.biz.v1.projects.dto;

import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Setter
@Getter
@NoArgsConstructor
public class ProjectDetailDto {

    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long projId;
    @Schema(description = "PROJECT TITLE", nullable = false, example = "PROJ_TITLE")
    private String projectTitle;
    @Schema(description = "PROJECT DESCRIPTION", nullable = false, example = "PROJ PROJDESC")
    private String projDesc;
    @Schema(name = "AVATAR FILE URL", nullable = true, example = "http://...")
    private String avatarFileUrl;

    @Schema(description = "PROJECT STATUS", nullable = false, example = "PROJ STATUS")
    private String projStatus;
    @Schema(description = "PITCH PROJ TYPE CD", nullable = false, example = "PWL000001")
    private String pitchProjTypeCd;
    @Schema(description = "PITCH PROJ TYPE NAME", nullable = false, example = "Collaboration / Cowrite")
    private String pitchProjTypeCdName;
    @Schema(description = "PROJECT OWNER", nullable = true, example = "OWNER PROFILE ID")
    private Long projOwner;
    @Schema(description = "PROJECT OWNER Name", nullable = true, example = "OWNER PROFILE ID")
    private String projOwnerName;
    @Schema(description = "PROJECT COMPANY ID", nullable = true, example = "1")
    private Long compProfileId;
    @Schema(description = "PROJECT COMPANY NAME", nullable = true, example = "ECKO")
    private String compProfileName;

    @Schema(description = "project Invite Company", nullable = true)
    private List<ProjectProfileDto> projectInviteCompanyList;
    @Schema(description = "등록일", pattern = "yyMMdd hh:mm:ss", example = "2021-12-08T05:44:26.627991")
    private LocalDateTime regDt;
    @Schema(description = "PROJ DDL DATE", nullable = false, example = "2022-01-01")
    private LocalDate projDdlDt;
    @Schema(description = "BUDGET", nullable = false, example = "분배 PJC000001/PJC000002/PJC000003")
    private String budget;
    @Schema(description = "BUDGET NAME", nullable = false, example = "분배 PJC000001/PJC000002/PJC000003")
    private String budgetName;
    @Schema(description = "MIN_VAL", nullable = false, example = "10")
    private Long minVal;
    @Schema(description = "MAX_VAL", nullable = false, example = "50")
    private Long maxVal;
    @Schema(description = "PROJECT PUBL YN", nullable = false, example = "Y")
    private YnTypeEnum projPublYn;

    @Schema(description = "project Roles", nullable = true)
    private List<ProjectMetadataDto> projectRolesList;
    @Schema(description = "project Music Genre", nullable = true)
    private List<ProjectMetadataDto> projectGenresList;
    @Schema(description = "project Music Mood", nullable = true)
    private List<ProjectMetadataDto> projectMoodsList;
    @Schema(description = "project Music Theme", nullable = true)
    private List<ProjectMetadataDto> projectThemesList;
    @Schema(description = "PROJECT WORK LOCATION", nullable = false, example = "PWL000001")
    private String projWorkLocat;
    @Schema(description = "PROJECT WORK LOCATION NAME", nullable = true, example = "DIGITAL")
    private String projWorkLocatName;
    @Schema(description = "project Music Mood", nullable = true)
    private List<ProjectMetadataDto> projectServiceCountriesList;

    @Schema(name = "첨부파일 리스트", nullable = true)
    private List<ProjectReferenceFileDto> projectReferenceFileList;
    @Schema(name = "song 리스트", nullable = true)
    private List<ProjectSongDto> projectSongList;
    @Schema(name = "cowriters 리스트", nullable = true)
    private List<ProjectCowritersViewListDto> projectCowritersList;


    @Builder
    public ProjectDetailDto(Long projId,
                            String projectTitle,
                            String projDesc,
                            String avatarFileUrl,
                            String projStatus,
                            String pitchProjTypeCd,
                            String pitchProjTypeCdName,
                            Long projOwner,
                            String projOwnerName,
                            Long compProfileId,
                            String compProfileName,
                            List<ProjectProfileDto> projectInviteCompanyList,
                            LocalDateTime regDt,
                            LocalDate projDdlDt,
                            String budget,
                            String budgetName,
                            Long minVal,
                            Long maxVal,
                            YnTypeEnum projPublYn,
                            List<ProjectMetadataDto> projectRolesList,
                            List<ProjectMetadataDto> projectGenresList,
                            List<ProjectMetadataDto> projectMoodsList,
                            List<ProjectMetadataDto> projectThemesList,
                            String projWorkLocat,
                            String projWorkLocatName,
                            List<ProjectMetadataDto> projectServiceCountriesList,
                            List<ProjectReferenceFileDto> projectReferenceFileList,
                            List<ProjectSongDto> projectSongList,
                            List<ProjectCowritersViewListDto> projectCowritersList
    ) {
        this.projId = projId;
        this.projectTitle = projectTitle;
        this.projDesc = projDesc;
        this.avatarFileUrl = avatarFileUrl;
        this.projStatus = projStatus;
        this.pitchProjTypeCd = pitchProjTypeCd;
        this.pitchProjTypeCdName = pitchProjTypeCdName;
        this.projOwner = projOwner;
        this.projOwnerName = projOwnerName;
        this.compProfileId = compProfileId;
        this.compProfileName = compProfileName;
        this.projectInviteCompanyList = projectInviteCompanyList;
        this.regDt = regDt;
        this.projDdlDt = projDdlDt;
        this.budget = budget;
        this.budgetName = budgetName;
        this.minVal = minVal;
        this.maxVal = maxVal;
        this.projPublYn = projPublYn;
        this.projectRolesList = projectRolesList;
        this.projectGenresList = projectGenresList;
        this.projectMoodsList = projectMoodsList;
        this.projectThemesList = projectThemesList;
        this.projWorkLocat = projWorkLocat;
        this.projWorkLocatName = projWorkLocatName;
        this.projectServiceCountriesList = projectServiceCountriesList;
        this.projectReferenceFileList = projectReferenceFileList;
        this.projectSongList = projectSongList;
        this.projectCowritersList = projectCowritersList;
    }
}
