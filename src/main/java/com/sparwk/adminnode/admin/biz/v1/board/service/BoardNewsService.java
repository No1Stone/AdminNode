package com.sparwk.adminnode.admin.biz.v1.board.service;

import com.sparwk.adminnode.admin.biz.v1.CommonPagingListDto;
import com.sparwk.adminnode.admin.biz.v1.admin.service.AdminActiveLogService;
import com.sparwk.adminnode.admin.biz.v1.board.dto.BoardAttachDto;
import com.sparwk.adminnode.admin.biz.v1.board.dto.BoardNewsDto;
import com.sparwk.adminnode.admin.biz.v1.board.dto.BoardNewsRequest;
import com.sparwk.adminnode.admin.biz.v1.board.dto.BoardDelete;
import com.sparwk.adminnode.admin.biz.v1.commonCode.dto.CommonCateListDto;
import com.sparwk.adminnode.admin.config.common.ActiveCodeConst;
import com.sparwk.adminnode.admin.config.common.GetAccountId;
import com.sparwk.adminnode.admin.config.common.Response;
import com.sparwk.adminnode.admin.config.common.ResultCodeConst;
import com.sparwk.adminnode.admin.jpa.entity.PeriodTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.admin.AdminActiveLogEntity;
import com.sparwk.adminnode.admin.jpa.entity.board.*;
import com.sparwk.adminnode.admin.jpa.entity.commonCode.CommonCodeEntity;
import com.sparwk.adminnode.admin.jpa.entity.commonCode.CommonDetailCodeEntity;
import com.sparwk.adminnode.admin.jpa.repository.board.BoardAttachRepository;
import com.sparwk.adminnode.admin.jpa.repository.board.BoardNewsRepository;
import com.sparwk.adminnode.admin.jpa.repository.commonCode.CommonCodeRepository;
import com.sparwk.adminnode.admin.jpa.repository.commonCode.CommonDetailCodeRepository;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class BoardNewsService {

    private final CommonCodeRepository commonCodeRepository;
    private final CommonDetailCodeRepository commonDetailCodeRepository;

    private final BoardAttachRepository boardAttachRepository;
    private final BoardNewsRepository boardNewsRepository;
    private final AdminActiveLogService adminActiveLogService;

    private final HttpServletRequest httpServletRequest;
    private final MessageSourceAccessor messageSourceAccessor;

    private final Logger logger = LoggerFactory.getLogger(BoardNewsService.class);

    public Response<List<CommonCateListDto>> getCateList(String pcode) {

        Response<List<CommonCateListDto>> res = new Response<>();

        //부모코드 없을 때
        if (pcode == null) {
            res.setResultCd(ResultCodeConst.NOT_EXIST_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<CommonCodeEntity> commonCode = Optional.ofNullable(
                commonCodeRepository.findByCode(pcode)
        );

        logger.info("commonCode - {}", commonCode);

        //부모코드 조회내용 없을 때
        if (!commonCode.isPresent()) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        List<CommonCateListDto> cateList = commonDetailCodeRepository.findQryCateList(pcode);

        logger.info("cateList - {}", cateList);

        //조회내용 없을 때
        if (cateList == null || cateList.size() < 1) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(cateList);
        return res;
    }



    public Response<CommonPagingListDto> getList(String type,
                                                 String pcode,
                                                 BoardCateEnum cate,
                                                 String val,
                                                 YnTypeEnum useType,
                                                 PeriodTypeEnum periodType,
                                                 String sdate,
                                                 String edate,
                                                 BoardSorterEnum sorter,
                                                 PageRequest pageRequest) {

        Response<CommonPagingListDto> res = new Response<>();

        //부모코드 없을 때, 실제 코드가 아니고 타입이라 조회는 생략함
        if (type == null) {
            res.setResultCd(ResultCodeConst.NOT_EXIST_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        List<BoardNewsDto> boardList =
                boardNewsRepository.findQryAll(type, pcode, cate, val, useType, periodType, sdate, edate, sorter, pageRequest);

        logger.info("BoardNewsList - {}", boardList);

        //조회내용 없을 때
        if (boardList == null || boardList.size() < 1) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        CommonPagingListDto list = new CommonPagingListDto();

        //총 카운트
        long totalSize = boardNewsRepository.countQryAll(type, pcode, cate, val, useType, periodType, sdate, edate);
        long totalPage = totalSize / pageRequest.getPageSize();
        long mod = totalSize % pageRequest.getPageSize();
        if(mod > 0) {
            totalPage += 1;
        }

        list.setList(boardList);
        list.setSize(pageRequest.getPageSize());
        list.setPage(pageRequest.getPageNumber());
        list.setTotalPage(totalPage);
        list.setTotalSize(totalSize);

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(list);
        return res;
    }

    public Response<CommonPagingListDto> getUseYList(String type, String pcode, BoardCateEnum cate, String val, PageRequest pageRequest) {

        Response<CommonPagingListDto> res = new Response<>();

        //부모코드 없을 때, 실제 코드가 아니고 타입이라 조회는 생략함
        if (type == null) {
            res.setResultCd(ResultCodeConst.NOT_EXIST_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        List<BoardNewsDto> boardList = boardNewsRepository.findQryUseY(type, pcode, cate, val, pageRequest);

        logger.info("BoardNewsList - {}", boardList);

        //조회내용 없을 때
        if (boardList == null || boardList.size() < 1) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        CommonPagingListDto list = new CommonPagingListDto();

        //총 카운트
        long totalSize = boardNewsRepository.countQryUseY(type, pcode, cate, val);

        long totalPage = totalSize / pageRequest.getPageSize();
        long mod = totalSize % pageRequest.getPageSize();
        if(mod > 0) {
            totalPage += 1;
        }

        list.setList(boardList);
        list.setSize(pageRequest.getPageSize());
        list.setPage(pageRequest.getPageNumber());
        list.setTotalPage(totalPage);
        list.setTotalSize(totalSize);

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(list);
        return res;
    }

    public Response<Optional<BoardNewsDto>> getOne(Long id) {

        Response<Optional<BoardNewsDto>> res = new Response<>();

        //id값이 없을 때
        if (id == null) {
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<BoardNewsEntity> boardNews = boardNewsRepository.findById(id);

        logger.info("boardNews - {}", boardNews);

        //조회내용 없을 때
        if (!boardNews.isPresent()) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //내용 조회 후 카테고리를 검색
        Optional<CommonDetailCodeEntity> commonDetailCode = Optional.ofNullable(
                commonDetailCodeRepository.findByDcode(boardNews.get().getCateCd())
        );

        logger.info("commonDetailCode - {}", commonDetailCode);

        //부모코드 조회내용 없을 때
        if (!commonDetailCode.isPresent()) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        List<BoardAttachDto> boardAttachDtoList = new ArrayList<>();
        //첨부파일이 있으면 리스트를 불러온다
        if(boardNews.get().getAttachYn() == YnTypeEnum.Y){
            boardAttachDtoList = boardAttachRepository.findQryAttach(id, "News");
        }

        List<BoardAttachDto> finalBoardAttachDtoList = boardAttachDtoList;
        Optional<BoardNewsDto> dtoPage =

                boardNews
                        .map(p -> BoardNewsDto.builder()
                                .newsId(p.getNewsId())
                                .cateCd(p.getCateCd())
                                .cateCdName(commonDetailCode.get().getVal())
                                .title(p.getTitle())
                                .content(p.getContent())
                                .useType(p.getUseYn())
                                .attachYn(p.getAttachYn())
                                .hit(p.getHit())
                                .regUsr(p.getRegUsr())
                                .regDt(p.getRegDt())
                                .modUsr(p.getModUsr())
                                .modDt(p.getModDt())
                                .boardAttachDtoList(finalBoardAttachDtoList)
                                .build()
                        );

        logger.info("BoardNews - {}", dtoPage);

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(dtoPage);
        return res;
    }



    @Transactional
    public Response<Void> saveContent(@NotNull BoardNewsRequest request, HttpServletRequest req) {

        Response<Void> res = new Response<>();

        //카테고리코드 없을 때
        if (request.getCateCd() == null) {
            res.setResultCd(ResultCodeConst.NOT_VALID_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<CommonDetailCodeEntity> commonDetailCode =
                Optional.ofNullable(commonDetailCodeRepository.findByDcode(request.getCateCd()));

        logger.info("commonDetailCode - {}", commonDetailCode);

        //카테고리코드 조회내용 없을 때
        if (!commonDetailCode.isPresent()) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //USEYN 값 없거나 틀릴 때
        if (request.getUseType() == null || (request.getUseType() != YnTypeEnum.Y && request.getUseType() != YnTypeEnum.N)) {
            res.setResultCd(ResultCodeConst.NOT_VALID_USEYN.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if (request.getTitle() == null) {
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }



        try {

            if(request.getBoardAttachDtoList() != null && request.getBoardAttachDtoList().size() > 0) {
                request.setAttachYn(YnTypeEnum.Y);
                //logger.info("====================== 11111111111111 {}", request.getBoardAttachDtoList());
            } else {
                request.setAttachYn(YnTypeEnum.N);
                //'logger.info("====================== 222222222 {}", request.getBoardAttachDtoList());
            }



            BoardNewsEntity newBoardNews = BoardNewsEntity.builder()
                    .cateCd(request.getCateCd())
                    .title(request.getTitle())
                    .content(request.getContent())
                    .useYn(request.getUseType())
                    .attachYn(request.getAttachYn())
                    .hit(0L)
                    .build();

            BoardNewsEntity saved = boardNewsRepository.save(newBoardNews);

            Optional<BoardNewsEntity> BoardNews = boardNewsRepository.findById(saved.getNewsId());

            //첨부파일이 있으면 추가로 등록한다
            if(saved.getAttachYn() == YnTypeEnum.Y){

                for(int i=0; i < request.getBoardAttachDtoList().size(); i++){

                    BoardAttachEntity newAttach = BoardAttachEntity.builder()
                            .boardType("News")
                            .boardId(saved.getNewsId())
                            .fileNum((long) (i+1))
                            .fileUrl(request.getBoardAttachDtoList().get(i).getFileUrl())
                            .fileName(request.getBoardAttachDtoList().get(i).getFileName())
                            .fileSize(request.getBoardAttachDtoList().get(i).getFileSize())
                            .build();

                    BoardAttachEntity saved2 = boardAttachRepository.save(newAttach);

                }

            }

            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
//            res.setResult(dtoPage);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));

            String menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_NEWS.getCode());
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_CREATE.getCode());

            //netive query
            String usrName = adminActiveLogService.findQryUsrName(saved.getRegUsr());

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(saved.getRegDt())
                    .adminId(saved.getRegUsr())
                    .menuName(menuName)
                    .activeType("C")
                    .contentsId(saved.getNewsId())
                    .activeMsg(usrName + " " + activeMsg + " '" + saved.getTitle() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e) {
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }

        return res;
    }

    @Transactional
    public Response<Optional<BoardNewsDto>> updateContent(String type, Long id, @NotNull BoardNewsRequest request, HttpServletRequest req) {

        Response<Optional<BoardNewsDto>> res = new Response<>();

        //부모코드 없을 때
        if (type == null) {
            res.setResultCd(ResultCodeConst.NOT_VALID_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<CommonDetailCodeEntity> commonDetailCode =
                Optional.ofNullable(commonDetailCodeRepository.findByDcode(request.getCateCd()));

        logger.info("commonCode - {}", commonDetailCode);

        //부모코드 조회내용 없을 때
        if (!commonDetailCode.isPresent()) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //id값이 없을 때
        if (id == null) {
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<BoardNewsEntity> boardNews = boardNewsRepository.findById(id);


        logger.info("boardNews - {} ", boardNews);

        //조회내용 없을 때
        if (!boardNews.isPresent()) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //USEYN 값 없거나 틀릴 때
        if (request.getUseType() == null || (request.getUseType() != YnTypeEnum.Y && request.getUseType() != YnTypeEnum.N)) {
            res.setResultCd(ResultCodeConst.NOT_VALID_USEYN.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if (request.getTitle() == null) {
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        try {

            boardNews.get().update(request.getCateCd(),
                    request.getTitle(),
                    request.getContent(),
                    request.getAttachYn(),
                    request.getUseType());

            logger.info("/////////////////////////////// boardNews - {} ", boardNews.toString());

            Optional<BoardNewsEntity> boardNewsUpdate = boardNewsRepository.findById(boardNews.get().getNewsId());

            List<BoardAttachDto> boardAttachDtoList = new ArrayList<>();
            //첨부파일이 있으면 리스트를 불러온다
            if(boardNews.get().getAttachYn()== YnTypeEnum.Y){
                for(int i=0; i < request.getBoardAttachDtoList().size(); i++){

                    //먼저 검사
                    Optional<BoardAttachEntity> isBoardAttachEntity = boardAttachRepository.findById(request.getBoardAttachDtoList().get(i).getAttachId());

                    //없으면 저장
                    if(!isBoardAttachEntity.isPresent()) {

                        BoardAttachEntity newAttach = BoardAttachEntity.builder()
                                .boardType("News")
                                .boardId(id)
                                .fileNum((long) (i + 1))
                                .fileUrl(request.getBoardAttachDtoList().get(i).getFileUrl())
                                .fileName(request.getBoardAttachDtoList().get(i).getFileName())
                                .fileSize(request.getBoardAttachDtoList().get(i).getFileSize())
                                .build();

                        BoardAttachEntity saved2 = boardAttachRepository.save(newAttach);
                    }
                    // 있으면 업데이트
                    else {
                        isBoardAttachEntity.get().update(
                                request.getBoardAttachDtoList().get(i).getFileUrl(),
                                request.getBoardAttachDtoList().get(i).getFileName(),
                                request.getBoardAttachDtoList().get(i).getFileSize()
                        );
                    }

                }

                //다시 리스트 불러옴
                boardAttachDtoList = boardAttachRepository.findQryAttach(boardNews.get().getNewsId(), "News");
            }

            List<BoardAttachDto> finalBoardAttachDtoList = boardAttachDtoList;
            Optional<BoardNewsDto> dtoPage =

                    boardNewsUpdate
                            .map(p -> BoardNewsDto.builder()
                                    .newsId(p.getNewsId())
                                    .cateCd(p.getCateCd())
                                    .cateCdName(commonDetailCode.get().getVal())
                                    .title(p.getTitle())
                                    .content(p.getContent())
                                    .useType(p.getUseYn())
                                    .attachYn(p.getAttachYn())
                                    .hit(p.getHit())
                                    .regUsr(p.getRegUsr())
                                    .regDt(p.getRegDt())
                                    .modUsr(p.getModUsr())
                                    .modDt(p.getModDt())
                                    .boardAttachDtoList(finalBoardAttachDtoList)
                                    .build()
                            );

            logger.info("dtoPage - {} ", dtoPage);

            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            res.setResult(dtoPage);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));

            String menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_NEWS.getCode());
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_MODIFY.getCode());

            //netive query
            String usrName = adminActiveLogService.findQryUsrName(boardNews.get().getModUsr());

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(boardNews.get().getModDt())
                    .adminId(boardNews.get().getModUsr())
                    .menuName(menuName)
                    .activeType("U")
                    .contentsId(boardNews.get().getNewsId())
                    .activeMsg(usrName + " " + activeMsg + " '" + boardNews.get().getTitle() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e) {
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }

        return res;
    }

    @Transactional
    public Response<Void> deleteContent(Long id, HttpServletRequest req) {

        Response<Void> res = new Response<>();

        //id값이 없을 때
        if (id == null) {
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<BoardNewsEntity> boardNews = boardNewsRepository.findById(id);

        logger.info("boardNews - {} ", boardNews);

        //조회내용 없을 때
        if (!boardNews.isPresent()) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        try {
            boardNewsRepository.deleteById(id);
            boardAttachRepository.deleteByBoardId(id);
            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
//            res.setResult(dtoPage);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));
            Long adminId = getAccountId.ofId(httpServletRequest);

            String menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_NEWS.getCode());
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_DELETE.getCode());

            //netive query
            String usrName = adminActiveLogService.findQryUsrName(adminId);

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(LocalDateTime.now())
                    .adminId(adminId)
                    .menuName(menuName)
                    .activeType("D")
                    .contentsId(boardNews.get().getNewsId())
                    .activeMsg(usrName + " " + activeMsg + " '" + boardNews.get().getTitle() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e) {
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }
        return res;

    }

    @Transactional
    public Response<Void> deleteList(@NotNull BoardDelete request) {

        Response<Void> res = new Response<>();

        //id값이 없을 때
        if (request.getId() == null) {
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //조회내용 없을 때
        try {

            Long[] kkk = request.getId();

            for(int i=0; i < kkk.length; i++) {

                Optional<BoardNewsEntity> boardNews = boardNewsRepository.findById(request.getId()[i]);

                ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
                GetAccountId getAccountId = new GetAccountId();
                logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));
                Long adminId = getAccountId.ofId(httpServletRequest);


                String menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_NEWS.getCode());
                String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_DELETE.getCode());

                //netive query
                String usrName = adminActiveLogService.findQryUsrName(adminId);

                AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                        .activeTime(LocalDateTime.now())
                        .adminId(adminId)
                        .menuName(menuName)
                        .activeType("D")
                        .contentsId(boardNews.get().getNewsId())
                        .activeMsg(usrName + " " + activeMsg + " '" + boardNews.get().getTitle() + "' in " + menuName)
                        .build();

                adminActiveLogService.saveContent(newLog);
            }

            boardNewsRepository.deleteQryList(request.getId());
            boardAttachRepository.deleteQryAttahList(request.getId());
            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
//            res.setResult(dtoPage);

        } catch (Exception e) {
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }
        return res;

    }

    @Transactional
    public Response<Void> deleteAttach(Long id, Long attahId, HttpServletRequest req) {

        Response<Void> res = new Response<>();

        //id값이 없을 때
        if (id == null) {
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<BoardNewsEntity> boardNews = boardNewsRepository.findById(id);

        logger.info("boardNews - {} ", boardNews);

        //조회내용 없을 때
        if (!boardNews.isPresent()) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //attahId값이 없을 때
        if (attahId == null) {
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<BoardAttachEntity> boardAttach = boardAttachRepository.findById(attahId);

        logger.info("boardNews - {} ", boardNews);

        //조회내용 없을 때
        if (!boardNews.isPresent()) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        try {
            boardAttachRepository.deleteById(attahId);

            int cnt = boardAttachRepository.countByBoardId(id);

            if (cnt == 0) {
                boardNews.get().updateZeroAttach(YnTypeEnum.N);
            }

            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
//            res.setResult(dtoPage);
        } catch (Exception e) {
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }
        return res;

    }

}
