package com.sparwk.adminnode.admin.biz.v1.songCode.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sparwk.adminnode.admin.jpa.entity.*;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Arrays;
import java.util.List;

@Data
@NoArgsConstructor
@ExcelFileName
public class SongDetailCodeExcelDto implements ExcelDto {
    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    @ExcelColumnName(headerName = "Song Detail Code Seq")
    private Long songDetailCodeSeq;
    @Schema(description = "부모코드 3자리", nullable = false, example = "SGG")
    @ExcelColumnName
    @JsonProperty("pcode")
    private String pcode;
    @Schema(description = "부모코드 값", nullable = true, example = "Genre")
    @ExcelColumnName
    @JsonProperty("pcodeVal")
    private String pcodeVal;
    @Schema(description = "코드 9자리", nullable = false, example = "SGG000001")
    @ExcelColumnName
    @JsonProperty("dcode")
    private String dcode;
    @Schema(description = "코드 값", nullable = false, example = "Hip-Hop")
    @ExcelColumnName
    @JsonProperty("val")
    private String val;
    @Schema(description = "포맷 여부 DDEX / SPARWK", defaultValue = "DDEX", allowableValues = {"DDEX", "SPARWK"})
    @ExcelColumnName
    @JsonProperty("format")
    private FormatTypeEnum formatType;
    @Schema(description = "코드 사용여부 Y/N", defaultValue = "Y", allowableValues = {"Y", "N"})
    @ExcelColumnName
    @JsonProperty("useType")
    private YnTypeEnum useType;
    @Schema(description = "popular 사용 Y/N", defaultValue = "Y", allowableValues = {"Y", "N"})
    @ExcelColumnName
    @JsonProperty("popularType")
    private YnTypeEnum popularType;
    @Schema(description = "description 설명", nullable = true, example = "설명")
    @ExcelColumnName
    @JsonProperty("description")
    private String description;

    @Override
    public List<String> mapToList() {
        return Arrays.asList(
                String.valueOf(songDetailCodeSeq),
                pcode,
                pcodeVal,
                dcode,
                val,
                String.valueOf(formatType),
                String.valueOf(useType),
                String.valueOf(popularType),
                description
        );
    }
}
