package com.sparwk.adminnode.admin.biz.v1.language.service;

import com.sparwk.adminnode.admin.biz.v1.CommonPagingListDto;
import com.sparwk.adminnode.admin.biz.v1.admin.service.AdminActiveLogService;
import com.sparwk.adminnode.admin.biz.v1.language.dto.LanguageDto;
import com.sparwk.adminnode.admin.biz.v1.language.dto.LanguageExcelDto;
import com.sparwk.adminnode.admin.biz.v1.language.dto.LanguageRequest;
import com.sparwk.adminnode.admin.config.common.ActiveCodeConst;
import com.sparwk.adminnode.admin.config.common.GetAccountId;
import com.sparwk.adminnode.admin.config.common.Response;
import com.sparwk.adminnode.admin.config.common.ResultCodeConst;
import com.sparwk.adminnode.admin.config.util.ExcelWriter;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.admin.AdminActiveLogEntity;
import com.sparwk.adminnode.admin.jpa.entity.language.LanguageCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.language.LanguageEntity;
import com.sparwk.adminnode.admin.jpa.entity.language.LanguageSorterEnum;
import com.sparwk.adminnode.admin.jpa.repository.language.LanguageRepository;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class LanguageService {

    private final LanguageRepository languageRepository;
    private final AdminActiveLogService adminActiveLogService;

    private final HttpServletRequest httpServletRequest;
    private final MessageSourceAccessor messageSourceAccessor;
    
    private final Logger logger = LoggerFactory.getLogger(LanguageService.class);


    public Response<CommonPagingListDto> getList(LanguageCateEnum languageCateEnum,
                                               String val,
                                               YnTypeEnum useType,
                                               LanguageSorterEnum sorter,
                                               PageRequest pageRequest) {

        Response<CommonPagingListDto> res = new Response<>();

        List<LanguageDto> languageList = languageRepository.findQryAll(languageCateEnum, val, useType, sorter, pageRequest);

        logger.info("languageList - {}", languageList);

        //조회내용 없을 때
        if(languageList == null || languageList.size() < 1){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        CommonPagingListDto list = new CommonPagingListDto();

        //총 카운트
        long totalSize = languageRepository.countQryAll(languageCateEnum, val, useType);
        long totalPage = totalSize / pageRequest.getPageSize();
        long mod = totalSize % pageRequest.getPageSize();
        if(mod > 0) {
            totalPage += 1;
        }

        list.setList(languageList);
        list.setSize(pageRequest.getPageSize());
        list.setPage(pageRequest.getPageNumber());
        list.setTotalPage(totalPage);
        list.setTotalSize(totalSize);

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(list);
        return res;
    }

    public Response<CommonPagingListDto> getUseYList() {

        Response<CommonPagingListDto> res = new Response<>();

        List<LanguageDto> languageList = languageRepository.findQryUseY();

        logger.info("languageList - {}", languageList);

        //조회내용 없을 때
        if(languageList == null || languageList.size() < 1){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        CommonPagingListDto list = new CommonPagingListDto();

        PageRequest pageRequest = PageRequest.of(10, 10);

        //총 카운트
        long totalSize = languageRepository.countQryUseY();
        long totalPage = totalSize / pageRequest.getPageSize();
        long mod = totalSize % pageRequest.getPageSize();
        if(mod > 0) {
            totalPage += 1;
        }

        list.setList(languageList);
        list.setSize(pageRequest.getPageSize());
        list.setPage(pageRequest.getPageNumber());
        list.setTotalPage(totalPage);
        list.setTotalSize(totalSize);

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(list);
        return res;
    }

    public Response<Optional<LanguageDto>> getOne(Long id) {

        Response<Optional<LanguageDto>> res = new Response<>();

        //id값이 없을 때
        if(id == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<LanguageEntity> language = languageRepository.findById(id);

        logger.info("language - {}", language);

        //조회내용 없을 때
        if(!language.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }
        
        Optional<LanguageDto> dtoPage =

                language.map(p -> LanguageDto.builder()
                                .languageSeq(p.getLanguageSeq())
                                .language(p.getLanguage())
                                .languageFamily(p.getLanguageFamily())
                                .nativeLanguage(p.getNativeLanguage())
                                .iso639_1(p.getIso639_1())
                                .iso639_2b(p.getIso639_2b())
                                .iso639_2t(p.getIso639_2t())
                                .useType(p.getUseType())
                                .languageCd(p.getLanguageCd())
                                .regUsr(p.getRegUsr())
                                .regDt(p.getRegDt())
                                .modUsr(p.getModUsr())
                                .modDt(p.getModDt())
                                .build()
                        );

        logger.info("dtoPage - {}", dtoPage);

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(dtoPage);
        return res;
    }

    @Transactional
    public Response<Void> saveContent(LanguageRequest request, HttpServletRequest req) {

        Response<Void> res = new Response<>();
        String languageCd;

        //USEYN 값 없거나 틀릴 때
        if(request.getLanguageDto().getUseType() == null || (request.getLanguageDto().getUseType() != YnTypeEnum.Y && request.getLanguageDto().getUseType() != YnTypeEnum.N)){
            res.setResultCd(ResultCodeConst.NOT_VALID_USEYN.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if(request.getLanguageDto().getLanguage() == null){
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if(request.getLanguageDto().getLanguageCd() == null){
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if(request.getLanguageDto().getNativeLanguage() == null){
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if(request.getLanguageDto().getIso639_1() == null){
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        try {

            //seq 번호 불러옴
            int k = languageRepository.getLastSeq("LAN") + 1;

            if(k < 10)
                languageCd = "LAN00000" + k;
            else if(k < 100)
                languageCd = "LAN0000" + k;
            else if(k < 1000)
                languageCd = "LAN000" + k;
            else if(k < 10000)
                languageCd = "LAN00" + k;
            else if(k < 100000)
                languageCd = "LAN0" + k;
            else if(k < 1000000)
                languageCd = "LAN" + k;
            else
                languageCd = "LAN" + k;
            

            LanguageEntity newLanguage = LanguageEntity.builder()
                    .language(request.getLanguageDto().getLanguage())
                    .languageFamily(request.getLanguageDto().getLanguageFamily())
                    .nativeLanguage(request.getLanguageDto().getNativeLanguage())
                    .iso639_1(request.getLanguageDto().getIso639_1())
                    .iso639_2t(request.getLanguageDto().getIso639_2t())
                    .iso639_2b(request.getLanguageDto().getIso639_2b())
                    .languageCd(languageCd)
                    .useType(request.getLanguageDto().getUseType())
                    .build();

            LanguageEntity saved = languageRepository.save(newLanguage);

            //seq 번호 수동으로 업데이트 시킨다
            languageRepository.updateLastSeq("LAN", k);

//            Optional<LanguageEntity> language = languageRepository.findById(saved.getLanguageSeq());
//
//            Optional<LanguageDto> dtoPage =
//
//                    language.map(p -> LanguageDto.builder()
//                                    .languageSeq(p.getLanguageSeq())
//                                    .language(p.getLanguage())
//                                    .languageFamily(p.getLanguageFamily())
//                                    .nativeLanguage(p.getNativeLanguage())
//                                    .iso639_1(p.getIso639_1())
//                                    .iso639_2b(p.getIso639_2b())
//                                    .iso639_2t(p.getIso639_2t())
//                                    .iso639_3(p.getIso639_3())
//                                    .useType(p.getUseType())
//                                    .languageCd(p.getLanguageCd())
//                                    .regUsr(p.getRegUsr())
//                                    .regDt(p.getRegDt())
//                                    .modUsr(p.getModUsr())
//                                    .modDt(p.getModDt())
//                                    .build()
//                            );
//
//            return dtoPage;
            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
//            res.setResult(dtoPage);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));

            String menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_LANGUAGES.getCode());
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_CREATE.getCode());

            //netive query
            String usrName = adminActiveLogService.findQryUsrName(saved.getRegUsr());

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(saved.getRegDt())
                    .adminId(saved.getRegUsr())
                    .menuName(menuName)
                    .activeType("C")
                    .contentsId(saved.getLanguageSeq())
                    .activeMsg(usrName + " " + activeMsg + " '" + saved.getLanguage() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e){
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }

        return res;
    }

    @Transactional
    public Response<Void> updateYn(Long id, HttpServletRequest req) {

        Response<Void> res = new Response<>();

        Optional<LanguageEntity> language = languageRepository.findById(id);

        logger.info("language - {}", language);

        //조회내용 없을 때
        if(!language.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        YnTypeEnum useType = language.get().getUseType();
        YnTypeEnum newUseType = (useType == YnTypeEnum.Y) ? YnTypeEnum.N : YnTypeEnum.Y;

        logger.info("newUseType - {} ", newUseType);

        try {
            language.get().updateYn(newUseType);
            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            //res.setResult(songDetailCode);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));

            String menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_LANGUAGES.getCode());
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_MODIFY.getCode());

            //netive query
            String usrName = adminActiveLogService.findQryUsrName(language.get().getModUsr());

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(language.get().getModDt())
                    .adminId(language.get().getModUsr())
                    .menuName(menuName)
                    .activeType("U")
                    .contentsId(language.get().getLanguageSeq())
                    .activeMsg(usrName + " " + activeMsg + " '" + language.get().getLanguage() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e) {
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }
        return res;

    }

    @Transactional
    public Response<Optional<LanguageDto>> updateContent(Long id, LanguageRequest request, HttpServletRequest req) {

        Response<Optional<LanguageDto>> res = new Response<>();

        //id값이 없을 때
        if(id == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }
        Optional<LanguageEntity> languageUpdate = languageRepository.findById(id);

        logger.info("languageUpdate - {} ", languageUpdate);

        //조회내용 없을 때
        if(!languageUpdate.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //USEYN 값 없거나 틀릴 때
        if(request.getLanguageDto().getUseType() == null || (request.getLanguageDto().getUseType() != YnTypeEnum.Y && request.getLanguageDto().getUseType() != YnTypeEnum.N)){
            res.setResultCd(ResultCodeConst.NOT_VALID_USEYN.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if(request.getLanguageDto().getLanguage() == null){
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if(request.getLanguageDto().getLanguageCd() == null){
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if(request.getLanguageDto().getNativeLanguage() == null){
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if(request.getLanguageDto().getIso639_1() == null){
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        try {

            languageUpdate.get()
                    .update(request.getLanguageDto().getLanguage(),
                            request.getLanguageDto().getLanguageFamily(),
                            request.getLanguageDto().getNativeLanguage(),
                            request.getLanguageDto().getIso639_1(),
                            request.getLanguageDto().getIso639_2t(),
                            request.getLanguageDto().getIso639_2b(),
                            request.getLanguageDto().getUseType()
                        );

            Optional<LanguageEntity> language = languageRepository.findById(languageUpdate.get().getLanguageSeq());

            Optional<LanguageDto> dtoPage =

                    language.map(p -> LanguageDto.builder()
                            .languageSeq(p.getLanguageSeq())
                            .language(p.getLanguage())
                            .languageFamily(p.getLanguageFamily())
                            .nativeLanguage(p.getNativeLanguage())
                            .iso639_1(p.getIso639_1())
                            .iso639_2b(p.getIso639_2b())
                            .iso639_2t(p.getIso639_2t())
                            .useType(p.getUseType())
                            .languageCd(p.getLanguageCd())
                            .regUsr(p.getRegUsr())
                            .regDt(p.getRegDt())
                            .modUsr(p.getModUsr())
                            .modDt(p.getModDt())
                            .build()
                    );

            logger.info("dtoPage - {} ", dtoPage);

            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            res.setResult(dtoPage);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));

            String menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_LANGUAGES.getCode());
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_MODIFY.getCode());

            //netive query
            String usrName = adminActiveLogService.findQryUsrName(language.get().getModUsr());

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(language.get().getModDt())
                    .adminId(language.get().getModUsr())
                    .menuName(menuName)
                    .activeType("U")
                    .contentsId(language.get().getLanguageSeq())
                    .activeMsg(usrName + " " + activeMsg + " '" + language.get().getLanguage() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e){
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }

        return res;
    }

    @Transactional
    public Response<Void> deleteContent(Long id, HttpServletRequest req) {

        Response<Void> res = new Response<>();

        //id값이 없을 때
        if(id == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<LanguageEntity> language = languageRepository.findById(id);

        logger.info("language - {} ", language);

        //조회내용 없을 때
        if(!language.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        try {
            languageRepository.deleteById(id);
            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
//            res.setResult(dtoPage);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));
            Long adminId = getAccountId.ofId(httpServletRequest);

            String menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_LANGUAGES.getCode());
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_DELETE.getCode());

            //netive query
            String usrName = adminActiveLogService.findQryUsrName(adminId);

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(LocalDateTime.now())
                    .adminId(adminId)
                    .menuName(menuName)
                    .activeType("D")
                    .contentsId(language.get().getLanguageSeq())
                    .activeMsg(usrName + " " + activeMsg + " '" + language.get().getLanguage() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e) {
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }
        return res;

    }

    public List<LanguageExcelDto> getExcelList() {

        List<LanguageExcelDto> getExcelList = languageRepository.findExcelList();

        logger.info("getExcelList - {} ", getExcelList);

        return getExcelList;
    }


    //엑셀 다운로드 요청
    public Map<String, Object> downExcelList() throws Exception {
        // 데이터 가져오기
        List<LanguageExcelDto> excelList = this.getExcelList();

        logger.info("excelList - {} ", excelList);

        return ExcelWriter.createExcelData(excelList, LanguageExcelDto.class);
    }

    //마지막 코드 번호 확인
    public Integer getLastSeq(String code) {

        return languageRepository.getLastSeq(code);
    }

    //코드 번호 업데이트
    @Transactional
    public void updateLastSeq(String code, Integer k) {

        languageRepository.updateLastSeq(code, k);
    }

    //이름으로 코드검색
    public Long getOneName(String val) {

        return languageRepository.countByLanguage(val);
    }


    @Transactional
    public Response<Void> saveExcelContent(LanguageDto languageDto, HttpServletRequest req) {

        Response<Void> res = new Response<>();

        //USEYN 값 없거나 틀릴 때
        if (languageDto.getUseType() == null || (languageDto.getUseType() != YnTypeEnum.Y && languageDto.getUseType() != YnTypeEnum.N)) {
            res.setResultCd(ResultCodeConst.NOT_VALID_USEYN.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if (languageDto.getLanguage() == null) {
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if (languageDto.getNativeLanguage() == null) {
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if (languageDto.getIso639_1() == null) {
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        try {

            LanguageEntity newLanguage = LanguageEntity.builder()
                    .languageSeq(languageDto.getLanguageSeq())
                    .language(languageDto.getLanguage())
                    .languageCd(languageDto.getLanguageCd())
                    .languageFamily(languageDto.getLanguageFamily())
                    .nativeLanguage(languageDto.getNativeLanguage())
                    .iso639_1(languageDto.getIso639_1())
                    .iso639_2t(languageDto.getIso639_2t())
                    .iso639_2b(languageDto.getIso639_2b())
                    .useType(languageDto.getUseType())
                    .build();

            LanguageEntity saved = languageRepository.save(newLanguage);

            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
//            res.setResult(dtoPage);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));

            String menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_LANGUAGES.getCode());
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_CREATE.getCode());

            //netive query
            String usrName = adminActiveLogService.findQryUsrName(saved.getRegUsr());

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(saved.getRegDt())
                    .adminId(saved.getRegUsr())
                    .menuName(menuName)
                    .activeType("C")
                    .contentsId(saved.getLanguageSeq())
                    .activeMsg(usrName + " " + activeMsg + " '" + saved.getLanguage() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e) {
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }

        return res;
    }

}
