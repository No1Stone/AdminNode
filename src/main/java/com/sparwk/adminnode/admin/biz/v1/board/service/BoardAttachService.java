package com.sparwk.adminnode.admin.biz.v1.board.service;

import com.sparwk.adminnode.admin.biz.v1.board.dto.BoardAttachDto;
import com.sparwk.adminnode.admin.biz.v1.board.dto.BoardAttachRequest;
import com.sparwk.adminnode.admin.config.common.Response;
import com.sparwk.adminnode.admin.config.common.ResultCodeConst;
import com.sparwk.adminnode.admin.jpa.entity.board.BoardAttachEntity;
import com.sparwk.adminnode.admin.jpa.entity.board.BoardNewsEntity;
import com.sparwk.adminnode.admin.jpa.repository.board.BoardAttachRepository;
import com.sparwk.adminnode.admin.jpa.repository.board.BoardNewsRepository;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class BoardAttachService {

    private final BoardAttachRepository boardAttachRepository;
    private final BoardNewsRepository boardNewsRepository;

    private final MessageSourceAccessor messageSourceAccessor;

    private final Logger logger = LoggerFactory.getLogger(BoardAttachService.class);

    public Response<List<BoardAttachDto>> getList(String boardType, Long id) {

        Response<List<BoardAttachDto>> res = new Response<>();

        //id 없을 때
        if (id == null) {
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        List<BoardAttachDto> boardAttachList = boardAttachRepository.findQryAttach(id, boardType);

        logger.info("BoardNewsList - {}", boardAttachList);

        //조회내용 없을 때
        if (boardAttachList == null || boardAttachList.size() < 1) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

                //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(boardAttachList);
        return res;
    }


    @Transactional
    public Response<Void> saveContent(String type, Long id, @NotNull BoardAttachRequest request, HttpServletRequest req) {

        Response<Void> res = new Response<>();

        //Type 없을 때
        if (type == null || !type.equals("News")) {
            res.setResultCd(ResultCodeConst.NOT_EXIST_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //ID 없을 때
        if (id == null) {
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        if(type.equals("News")) {

            Optional<BoardNewsEntity> news = boardNewsRepository.findById(id);

            logger.info("news - {}", news);

            //부모코드 조회내용 없을 때
            if (!news.isPresent()) {
                res.setResultCd(ResultCodeConst.NOT_FOUND_PCODE.getCode());
                res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
                return res;
            }
        }


        //BoardId 값 없거나 틀릴 때
        if (request.getBoardId() == null) {
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //FileNum 값 없거나 틀릴 때
        if (request.getFileNum() == null) {
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //fileUrl 값 없을 때
        if (request.getFileUrl() == null) {
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //fileName 값 없을 때
        if (request.getFileName() == null) {
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        try {

            BoardAttachEntity newAttach = BoardAttachEntity.builder()
                    .boardType(request.getBoardType())
                    .boardId(request.getBoardId())
                    .fileNum(request.getFileNum())
                    .fileUrl(request.getFileUrl())
                    .fileName(request.getFileName())
                    .fileSize(request.getFileSize())
                    .build();

            BoardAttachEntity saved = boardAttachRepository.save(newAttach);

//            Optional<BoardNewsEntity> BoardNews = boardNewsRepository.findById(saved.getBoardNewsSeq());
//
//            Optional<BoardNewsDto> dtoPage =
//
//                    BoardNews
//                            .map(p -> BoardNewsDto.builder()
//                                    .boardNewsSeq(p.getBoardNewsSeq())
//                                    .pcode(p.getCommonCodeEntity().getCode())
//                                    .pcodeVal(p.getCommonCodeEntity().getVal())
//                                    .dcode(p.getDcode())
//                                    .val(p.getVal())
//                                    .description(p.getDescription())
//                                    .useType(p.getUseType())
//                                    .popularType(p.getPopularType())
//                                    .hit(p.getHit())
//                                    .regUsr(p.getRegUsr())
//                                    .regDt(p.getRegDt())
//                                    .modUsr(p.getModUsr())
//                                    .modDt(p.getModDt())
//                                    .build()
//                            );
//
//            return dtoPage;

            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
//            res.setResult(dtoPage);

        } catch (Exception e) {
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }

        return res;
    }

    @Transactional
    public Response<Void> deleteContent(String type, Long boardId, Long attahId, HttpServletRequest req) {

        Response<Void> res = new Response<>();

        //부모코드 없을 때
        if (boardId == null) {
            res.setResultCd(ResultCodeConst.NOT_EXIST_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        if(type.equals("News")) {

            Optional<BoardNewsEntity> news = boardNewsRepository.findById(boardId);

            logger.info("news - {}", news);

            //부모컨텐츠 조회내용 없을 때
            if (!news.isPresent()) {
                res.setResultCd(ResultCodeConst.NOT_FOUND_PCODE.getCode());
                res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
                return res;
            }
        }

        //id값이 없을 때
        if (attahId == null) {
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<BoardAttachEntity> boardAttach = boardAttachRepository.findById(attahId);

        logger.info("boardAttach - {} ", boardAttach);

        //조회내용 없을 때
        if (!boardAttach.isPresent()) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        try {
            boardNewsRepository.deleteById(attahId);
            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
//            res.setResult(dtoPage);
        } catch (Exception e) {
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }
        return res;

    }

}
