package com.sparwk.adminnode.admin.biz.v1.sns.dto;

import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Setter
@Getter
@NoArgsConstructor
public class SnsDto {

    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long snsSeq;
    @Schema(description = "SNS 이름", nullable = false, example = "Facebook")
    private String snsName;
    @Schema(description = "코드 9자리", nullable = false, example = "SNS000001")
    private String snsCd;
    @Schema(description = "코드 URL", nullable = false, example = "http://")
    private String snsIconUrl;
    @Schema(description = "코드 사용여부 Y/N", nullable = false, defaultValue = "Y", allowableValues = {"Y", "N"})
    private YnTypeEnum useType;
    @Schema(description = "등록일", pattern = "yyMMdd hh:mm:ss", example = "2021-12-08T05:44:26.627991")
    private LocalDateTime regDt;
    @Schema(description = "등록자", nullable = false, example = "10000001")
    private Long regUsr;
    @Schema(description = "등록자 이름", nullable = false, example = "Yoon Jung Sun")
    private String regUsrName;
    @Schema(description = "수정일", pattern = "yyMMdd hh:mm:ss", example = "2021-12-08T05:44:26.627991")
    private LocalDateTime modDt;
    @Schema(description = "수정자", nullable = false, example = "10000001")
    private Long modUsr;
    @Schema(description = "수정자 이름", nullable = false, example = "Yoon Jung Sun")
    private String modUsrName;

    @Builder
    public SnsDto(Long snsSeq,
                  String snsName,
                  String snsCd,
                  String snsIconUrl,
                  YnTypeEnum useType,
                  LocalDateTime regDt,
                  Long regUsr,
                  String regUsrName,
                  LocalDateTime modDt,
                  Long modUsr,
                  String modUsrName
    ) {
        this.snsSeq = snsSeq;
        this.snsName = snsName;
        this.snsCd = snsCd;
        this.snsIconUrl = snsIconUrl;
        this.useType = useType;
        this.regDt = regDt;
        this.regUsr = regUsr;
        this.regUsrName = regUsrName;
        this.modDt = modDt;
        this.modUsr = modUsr;
        this.modUsrName = modUsrName;
    }
}