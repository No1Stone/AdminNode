package com.sparwk.adminnode.admin.biz.v1.languagePack.dto;

import com.sparwk.adminnode.admin.biz.v1.language.dto.LanguageDto;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.time.LocalDateTime;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@Schema(description = "Request DTO")
public class LanguagePackRequest {

    @Schema(description = "언어명", nullable = false, example = "LAN000001")
    private String languageCd;
    @Schema(description = "국기 업로드 url", nullable = false, example = "http://")
    private String nationalFlagUrl;
    @Schema(description = "Json 업로드 url", nullable = false, example = "http://")
    private String jsonUploadUrl;
    @Schema(description = "version", nullable = true, example = "1")
    private String version;
    @Schema(description = "코드 사용여부 Y/N", nullable = false, defaultValue = "Y", allowableValues = {"Y", "N"})
    private YnTypeEnum useType;
}
