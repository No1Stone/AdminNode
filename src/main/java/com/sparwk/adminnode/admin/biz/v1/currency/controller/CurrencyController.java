package com.sparwk.adminnode.admin.biz.v1.currency.controller;

import com.sparwk.adminnode.admin.biz.v1.CommonPagingListDto;
import com.sparwk.adminnode.admin.biz.v1.currency.dto.CurrencyDto;
import com.sparwk.adminnode.admin.biz.v1.currency.dto.CurrencyRequest;
import com.sparwk.adminnode.admin.biz.v1.currency.service.CurrencyService;
import com.sparwk.adminnode.admin.config.common.AdminPermissionIntergrationService;
import com.sparwk.adminnode.admin.config.common.Response;
import com.sparwk.adminnode.admin.config.common.ResultCodeConst;
import com.sparwk.adminnode.admin.config.util.ExcelXlsView;
import com.sparwk.adminnode.admin.jpa.entity.PeriodTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.currency.CurrencyCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.currency.CurrencySorterEnum;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.RequiredArgsConstructor;
import org.apache.commons.io.FilenameUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

@RestController
@RequestMapping(value = "/V1/setting/identifire/")
@RequiredArgsConstructor
@CrossOrigin("*")
public class
CurrencyController {

    private final Logger logger = LoggerFactory.getLogger(CurrencyController.class);

    @Autowired
    private CurrencyService currencyService;

    @Autowired
    private AdminPermissionIntergrationService adminPermissionIntergrationService;
    private final MessageSourceAccessor messageSourceAccessor;

    @Value("${file.path}")
    private String fileRealPath;

    @GetMapping("currency")
    @Operation(
            summary = "Currency List", description = "Currency 데이터 All List - Use NO Include(사용안함도 포함한 전체 리스트입니다.)",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = CurrencyDto.class))),
                    @ApiResponse(
                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
                            , content = @Content(schema = @Schema(hidden = true)))
            })
    public Response<CommonPagingListDto> getList(
            @Parameter(name = "cate", description = "검색 종류", in = ParameterIn.PATH) @RequestParam(value = "cate", required = false) CurrencyCateEnum cate,
            @Parameter(name = "val", description = "코드 값 포함", in = ParameterIn.PATH) @RequestParam(value = "val", required = false) String val,
            @Parameter(name = "useType", description = "사용여부 값 Y/N", in = ParameterIn.PATH) @RequestParam(value = "useType", required = false) YnTypeEnum useType,
            @Parameter(name = "periodType", description = "기간 종류", in = ParameterIn.PATH) @RequestParam(value = "periodType", required = false) PeriodTypeEnum periodType,
            @Parameter(name = "sdate", description = "시작일", in = ParameterIn.PATH) @RequestParam(value = "sdate", required = false) String sdate,
            @Parameter(name = "edate", description = "종료일", in = ParameterIn.PATH) @RequestParam(value = "edate", required = false) String edate,
            @Parameter(name = "sorter", description = "순서", in = ParameterIn.PATH) @RequestParam(value = "sorter", required = false) CurrencySorterEnum sorter,
            @Parameter(name = "page", description = "페이지 번호", in = ParameterIn.PATH) @RequestParam(value = "page", required = false, defaultValue = "1") int page,
            @Parameter(name = "size", description = "페이지 사이즈", in = ParameterIn.PATH) @RequestParam(value = "size", required = false, defaultValue = "10") int size

    ) {
        PageRequest pageRequest = PageRequest.of(page, size);

        Response res = new Response();
        if(!adminPermissionIntergrationService.AdminPermissionIntergrationCheck(4L, "R")){
            res.setResultCd(ResultCodeConst.NOT_VALID_READ.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        return currencyService.getList(cate, val, useType, periodType, sdate, edate, sorter, pageRequest);
    }

    //사용중만 보기
    @GetMapping("currency/use")
    @Operation(
            summary = "Use Currency List", description = "Currency 데이터 Use Y List - Use NO Include(사용하는 코드 리스트입니다.)",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = CurrencyDto.class))),
                    @ApiResponse(
                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
                            , content = @Content(schema = @Schema(hidden = true)))
            })
    public Response<CommonPagingListDto> getUseYList(
            @Parameter(name = "cate", description = "검색 종류", in = ParameterIn.PATH) @RequestParam(value = "cate", required = false) CurrencyCateEnum cate,
            @Parameter(name = "val", description = "검색어 포함", in = ParameterIn.PATH) @RequestParam(value = "val", required = false) String val
    ) {
        Response res = new Response();
        if(!adminPermissionIntergrationService.AdminPermissionIntergrationCheck(4L, "R")){
            res.setResultCd(ResultCodeConst.NOT_VALID_READ.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        return currencyService.getUseYList(cate, val);
    }

    //상세페이지
    @GetMapping("currency/{id}")
    @Operation(
            summary = "Currency Detail View", description = "Currency Detail Code View 입니다.",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = CurrencyDto.class))),
                    @ApiResponse(
                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1003", description = "id가 존재하지 않습니다.."
                            , content = @Content(schema = @Schema(hidden = true)))
            })
    public Response<Optional<CurrencyDto>> getOne(
            @Parameter(name = "id", description = "id 값", in = ParameterIn.PATH) @PathVariable(value = "id") Long id
    ) {
        Response res = new Response();
        if(!adminPermissionIntergrationService.AdminPermissionIntergrationCheck(4L, "R")){
            res.setResultCd(ResultCodeConst.NOT_VALID_READ.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        return currencyService.getOne(id);
    }

    //쓰기처리
    @PostMapping("currency/new")
    @Operation(
            summary = "New Currency", description = "Currency 데이터 추가합니다.",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = CurrencyDto.class))),
                    @ApiResponse(
                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1203", description = "Use Type 값 형식이 잘못되었습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1206", description = "Value 형식이 잘못되었습니다."
                            , content = @Content(schema = @Schema(hidden = true)))
            })
    public Response<Void> saveContent(
            @Parameter(name = "request", description = "Request Dto", in = ParameterIn.PATH) @RequestBody @Valid CurrencyRequest request,
            HttpServletRequest req
    ) {
        Response res = new Response();
        if(!adminPermissionIntergrationService.AdminPermissionIntergrationCheck(4L, "C")){
            res.setResultCd(ResultCodeConst.NOT_VALID_CREATE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        return currencyService.saveContent(request, req);
    }

    //사용여부 토글처리
    @PostMapping("currency/{id}/use")
    @Operation(
            summary = "Use Y/N", description = "Currency 데이터 사용/미사용을 처리합니다.",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = CurrencyDto.class))),
                    @ApiResponse(
                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1003", description = "id가 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true)))
            })
    public Response<Void> updateYn(
            @Parameter(name = "id", description = "id 값", in = ParameterIn.PATH) @PathVariable(value = "id") Long id,
            HttpServletRequest req
    ) {
        Response res = new Response();
        if(!adminPermissionIntergrationService.AdminPermissionIntergrationCheck(4L, "U")){
            res.setResultCd(ResultCodeConst.NOT_VALID_UPDATE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        return currencyService.updateYn(id, req);
    }

    //업데이트처리
    @PostMapping("currency/{id}/modify")
    @Operation(
            summary = "Modify Currency", description = "Currency 데이터 수정합니다.",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = CurrencyDto.class))),
                    @ApiResponse(
                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1003", description = "id가 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1203", description = "Use Type 값 형식이 잘못되었습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1206", description = "Value 형식이 잘못되었습니다."
                            , content = @Content(schema = @Schema(hidden = true)))
            })
    public Response<Optional<CurrencyDto>> updateContent(
            @Parameter(name = "id", description = "id 값", in = ParameterIn.PATH) @PathVariable(value = "id") Long id,
            @Parameter(name = "request", description = "Request Dto", in = ParameterIn.PATH) @RequestBody @Valid CurrencyRequest request,
            HttpServletRequest req
    ) {
        Response res = new Response();
        if(!adminPermissionIntergrationService.AdminPermissionIntergrationCheck(4L, "U")){
            res.setResultCd(ResultCodeConst.NOT_VALID_UPDATE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        return currencyService.updateContent(id, request, req);
    }

    //삭제처리
    @DeleteMapping("currency/{id}/delete")
    @Operation(
            summary = "delete Currency", description = "Currency 데이터 삭제합니다.",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = CurrencyDto.class))),
                    @ApiResponse(
                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1003", description = "id가 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true)))
            })
    public Response<Void> delete(
            @Parameter(name = "id", description = "id 값", in = ParameterIn.PATH) @PathVariable(value = "id") Long id,
            HttpServletRequest req
    ) {
        Response res = new Response();
        if(!adminPermissionIntergrationService.AdminPermissionIntergrationCheck(4L, "D")){
            res.setResultCd(ResultCodeConst.NOT_VALID_DELETE
                    .getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        return currencyService.deleteContent(id, req);
    }

    //EXCEL DOWN
    @GetMapping("currency/down-excel")
    @Operation(
            summary = "currency Code List Excel Down", description = "Song Detail Code Excel Down 입니다.",
            responses = {
                    @ApiResponse(
                            responseCode = "200", description = "Song Detail Code List Excel Down"
                            , content = @Content(schema = @Schema(implementation = CurrencyDto.class)))
            })
    public ModelAndView getExcelDownList(
            HttpServletRequest request
    ) {
        Map<String, Object> excelData = null;
        try {
            excelData = currencyService.downExcelList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ModelAndView(new ExcelXlsView(), excelData);
    }

    // @ResponseBody 생략가능. class 에 @RestController
    @PostMapping(value="currency/upload-excel")
    public Response uploadFile(
            @RequestPart(value="key", required=false) @Valid CurrencyRequest request,
            @RequestParam("file") MultipartFile file,
            Model model,
            HttpServletRequest req
    ) throws IllegalStateException, IOException {

        Response res = new Response();
        if(!adminPermissionIntergrationService.AdminPermissionIntergrationCheck(4L, "C")){
            res.setResultCd(ResultCodeConst.NOT_VALID_CREATE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        if( !file.isEmpty() ) {
            logger.info("file org name = {}", file.getOriginalFilename());
            logger.info("file content type = {}", file.getContentType());
            logger.info("file content type = {}", file.getSize());
            logger.info("file content type = {}", file.getName());
            //file.transferTo(new File(file.getOriginalFilename()));
        }

        List<CurrencyDto> dataList = new ArrayList<>();

        String extension = FilenameUtils.getExtension(file.getOriginalFilename()); // 3

        if (!extension.equals("xlsx") && !extension.equals("xls")) {
            throw new IOException("엑셀파일만 업로드 해주세요.");
        }


        System.out.println(fileRealPath);

        UUID uuid = UUID.randomUUID();
        String uuidFilename = uuid+"_"+file.getOriginalFilename();


        //이미지 저장 위치: 외부에 잡기/ 나중에 이 경로랑 파일명만 db에 저장하면 된다.
        //nio 로 임포트
        Path filePath = Paths.get(fileRealPath+uuidFilename);

        logger.info("{}", filePath.toString());

        try {
            Files.write(filePath,file.getBytes());
            //얘가 스레드 새로 만들어서 만약 10메가 넘어가면 여기서 파일 넣는동안 메인스레드가 ok 뿌려서 엑박뜸->
            //사진 용량 제한시키기 아니면 메인스레드 기다리게하기(비동기처리)

            Workbook workbook = null;

            if (extension.equals("xlsx")) {
                workbook = new XSSFWorkbook(file.getInputStream());
            } else if (extension.equals("xls")) {
                workbook = new HSSFWorkbook(file.getInputStream());
            }

            Sheet worksheet = workbook.getSheetAt(0);

            String commonCode = "CUR";
            Integer k = currencyService.getLastSeq(commonCode);
            Integer kk = 0;
            String currency;
            String currencyCd;
            String isoCode;
            String country;
            String symbol;


            for (int i = 2; i < worksheet.getPhysicalNumberOfRows(); i++) { // 4

                Row row = worksheet.getRow(i);

                k++;

                if(k < 10)
                    currencyCd = commonCode + "00000" + k;
                else if(k < 100)
                    currencyCd = commonCode + "0000" + k;
                else if(k < 1000)
                    currencyCd = commonCode + "000" + k;
                else if(k < 10000)
                    currencyCd = commonCode + "00" + k;
                else if(k < 100000)
                    currencyCd = commonCode + "0" + k;
                else if(k < 1000000)
                    currencyCd = commonCode + k;
                else
                    currencyCd = commonCode + k;


                currency = (row.getCell(1) == null) ? "" : row.getCell(1).getStringCellValue();
                isoCode = (row.getCell(2) == null) ? "" : row.getCell(2).getStringCellValue();
                symbol = (row.getCell(3) == null) ? "" : row.getCell(3).getStringCellValue();
                country = (row.getCell(4) == null) ? "" : row.getCell(4).getStringCellValue();

                CurrencyDto currencyDto = new CurrencyDto();

                currencyDto.setCurrency(currency);
                currencyDto.setCurrencyCd(currencyCd);
                currencyDto.setIsoCode(isoCode);
                currencyDto.setCountry(country);
                currencyDto.setSymbol(symbol);
                currencyDto.setUseType(YnTypeEnum.Y);
                //숫자일경우
                //testExcelDto.setCdesc(row.getCell(3).getNumericCellValue());

                //logger.info("val - {}", val);
                Long countVal = currencyService.getOneName(currency);
                //logger.info("countVal - {}", countVal);
                if(countVal > 0){
                    res.setResultCd(ResultCodeConst.NOT_VALID_OVERLAP.getCode());
                    res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
                    return res;
                }

                dataList.add(currencyDto);

                logger.info("currencyDto - {}", currencyDto.toString());
                Response<Void> result = currencyService.saveExcelContent(currencyDto, req);
                if (result.getResultCd().equals("0000")) {
                    kk++;
                }
            }

            //성공해야만 seq 업데이트
            if(kk > 0) {
                currencyService.updateLastSeq(commonCode, k);
            }

            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }

        return res;
    }

}
