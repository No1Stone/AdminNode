package com.sparwk.adminnode.admin.biz.v1.commonCode.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class CommonCateListDto {

    @Schema(description = "카테고리 코드 9자리", nullable = false, example = "ENC000001")
    private String cateCd;
    @Schema(description = "카테고리 코드 이름", nullable = false, example = "Best Music")
    private String cateCdName;

    @Builder
    public CommonCateListDto(String cateCd,
                             String cateCdName
    ) {
        this.cateCd = cateCd;
        this.cateCdName = cateCdName;
    }
}