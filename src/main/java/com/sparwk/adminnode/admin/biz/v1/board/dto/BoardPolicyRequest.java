package com.sparwk.adminnode.admin.biz.v1.board.dto;

import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Setter
@Getter
@NoArgsConstructor
public class BoardPolicyRequest {

    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long policyId;
    @Schema(description = "카테고리 코드 9자리", nullable = false, example = "TAP000001")
    private String cateCd;
    @Schema(description = "제목", nullable = false, example = "Terms and Policies 제목입니다.")
    private String title;
    @Schema(description = "content 설명", nullable = true, example = "Terms and Policies 내용입니다.")
    private String content;
    @Schema(description = "노출여부 Y/N", nullable = false, defaultValue = "Y", allowableValues = {"Y", "N"})
    private YnTypeEnum useType;
    @Schema(description = "등록자", nullable = false, example = "10000001")
    private Long regUsr;
    @Schema(description = "수정자", nullable = false, example = "10000001")
    private Long modUsr;

}