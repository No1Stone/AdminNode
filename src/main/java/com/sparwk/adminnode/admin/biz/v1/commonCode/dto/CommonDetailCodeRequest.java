package com.sparwk.adminnode.admin.biz.v1.commonCode.dto;

import lombok.*;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class CommonDetailCodeRequest {

//    @Schema(description = "부모코드 3자리", nullable = false, example = "ENC")
//    @NotBlank(message = "Please enter a code.")
//    private String pcode;
//    @Schema(description = "코드 9자리", nullable = true, example = "ENC000001")
//    @NotBlank(message = "Please enter a code.")
//    private String dcode;
//    @Schema(description = "코드 값", nullable = false, example = "UTF-8")
//    @NotBlank(message = "Please enter a value.")
//    private String val;
//    @Schema(description = "코드 사용여부 Y/N", defaultValue = "Y", allowableValues = {"Y", "N"})
//    @NotNull(message = "Please check use.")
//    private UseType useType;
//    @Schema(description = "popular 사용 Y/N", defaultValue = "Y", allowableValues = {"Y", "N"})
//    @NotNull(message = "Please check popular")
//    private YnEnumType popularType;
//    @Schema(description = "description 설명", nullable = true, example = "설명")
//    private String description;
//    @Schema(description = "코드 순번 숫자",  nullable = false, example = "1")
//    private int sortIndex;
//    @Schema(description = "hit 인기 순",  nullable = false, example = "1")
//    private int hit;
//
//
//    @Builder
//    public CommonDetailRequest(String pcode,
//                               String dcode,
//                               String val,
//                               UseType useType,
//                               YnEnumType popularType,
//                               String description,
//                               int sortIndex,
//                               int hit
//    ) {
//        this.pcode = pcode;
//        this.dcode = dcode;
//        this.val = val;
//        this.useType = useType;
//        this.popularType = popularType;
//        this.description = description;
//        this.sortIndex = sortIndex;
//        this.hit = hit;
//    }

    private CommonDetailCodeDto commonDetailCodeDto;



}
