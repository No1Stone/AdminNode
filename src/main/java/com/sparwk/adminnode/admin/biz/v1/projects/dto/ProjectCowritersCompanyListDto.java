package com.sparwk.adminnode.admin.biz.v1.projects.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
@NoArgsConstructor
public class ProjectCowritersCompanyListDto {

    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long companyProfileId;
    @Schema(description = "FullName", nullable = false, example = "Mr.Yoon")
    private String fullName;


    @Builder
    public ProjectCowritersCompanyListDto(Long companyProfileId,
                                          String fullName

    ) {
        this.companyProfileId = companyProfileId;
        this.fullName = fullName;
    }
}
