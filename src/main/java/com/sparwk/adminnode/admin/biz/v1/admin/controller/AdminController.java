package com.sparwk.adminnode.admin.biz.v1.admin.controller;

import com.sparwk.adminnode.admin.biz.v1.CommonPagingListDto;
import com.sparwk.adminnode.admin.biz.v1.admin.dto.AdminDto;
import com.sparwk.adminnode.admin.biz.v1.admin.dto.AdminRequest;
import com.sparwk.adminnode.admin.biz.v1.admin.service.AdminService;
import com.sparwk.adminnode.admin.config.common.Response;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.admin.AdminCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.admin.AdminSorterEnum;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Enumeration;
import java.util.Optional;

@RestController
@RequestMapping(value = "/V1/setting/administrator/")
@RequiredArgsConstructor
@CrossOrigin("*")
public class AdminController {

    private final Logger logger = LoggerFactory.getLogger(AdminController.class);

    @Autowired
    private AdminService adminService;

    @Value("${file.path}")
    private String fileRealPath;


    @GetMapping("administrator-management")
    @Operation(
            summary = "Admin List", description = "Admin 데이터 All List - Use NO Include(사용안함도 포함한 전체 리스트입니다.)",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = AdminDto.class))),
                    @ApiResponse(
                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
                            , content = @Content(schema = @Schema(hidden = true)))
            })
    public Response<CommonPagingListDto> getList(
            @Parameter(name = "cate", description = "검색 종류", in = ParameterIn.PATH) @RequestParam(value = "cate", required = false) AdminCateEnum cate,
            @Parameter(name = "val", description = "검색어 포함", in = ParameterIn.PATH) @RequestParam(value = "val", required = false) String val,
            @Parameter(name = "useType", description = "사용여부 값 Y/N", in = ParameterIn.PATH) @RequestParam(value = "useType", required = false) YnTypeEnum useType,
            @Parameter(name = "sorter", description = "순서", in = ParameterIn.PATH) @RequestParam(value = "sorter", required = false) AdminSorterEnum sorter,
            @Parameter(name = "page", description = "페이지 번호", in = ParameterIn.PATH) @RequestParam(value = "page", required = false, defaultValue = "1") int page,
            @Parameter(name = "size", description = "페이지 사이즈", in = ParameterIn.PATH) @RequestParam(value = "size", required = false, defaultValue = "10") int size
    ) {
        PageRequest pageRequest = PageRequest.of(page, size);

        return adminService.getList(cate, val, useType, sorter, pageRequest);
    }

    //상세페이지
    @GetMapping("administrator-management/{id}")
    @Operation(
            summary = "Admin Detail View", description = "Admin View 입니다.",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = AdminDto.class))),
                    @ApiResponse(
                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1003", description = "id가 존재하지 않습니다.."
                            , content = @Content(schema = @Schema(hidden = true)))
            })
    public Response<Optional<AdminDto>> getOne(
            @Parameter(name = "id", description = "id 값", in = ParameterIn.PATH) @PathVariable(value = "id") Long id
    ) {
        return adminService.getOne(id);
    }

    //쓰기처리 - create 장원석 소스 참고해야함 -오류남
    @ApiIgnore
    @PostMapping("administrator-management/new")
    @Operation(
            summary = "New Admin", description = "Admin 데이터 추가합니다.",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = AdminDto.class))),
                    @ApiResponse(
                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1001", description = "부모코드가 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1101", description = "부모코드 값이 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1203", description = "Use Type 값 형식이 잘못되었습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1206", description = "Value 형식이 잘못되었습니다."
                            , content = @Content(schema = @Schema(hidden = true)))
            })
    public Response<Void> saveContent(
            @Parameter(name = "request", description = "Request Dto", in = ParameterIn.PATH) @RequestBody @Valid AdminRequest request,
            HttpServletRequest req
    ) {
        return adminService.saveContent(request, req);
    }

    //사용여부 토글처리
    @PostMapping("administrator-management/{id}/use")
    @Operation(
            summary = "Use Y/N", description = "Admin 데이터 사용/미사용을 처리합니다.",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = AdminDto.class))),
                    @ApiResponse(
                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1003", description = "id가 존재하지 않습니다.."
                            , content = @Content(schema = @Schema(hidden = true)))
            })
    public Response<Void> updateYn(
            @Parameter(name = "id", description = "id 값", in = ParameterIn.PATH) @PathVariable(value = "id") Long id,
            HttpServletRequest req
    ) {
        return adminService.updateYn(id, req);
    }

    //업데이트처리
    @PostMapping("administrator-management/{id}/modify")
    @Operation(
            summary = "Modify Admin", description = "Admin 데이터 수정합니다.",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = AdminDto.class))),
                    @ApiResponse(
                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1003", description = "id가 존재하지 않습니다.."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1001", description = "부모코드가 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1101", description = "부모코드 값이 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                   @ApiResponse(
                            responseCode = "1203", description = "Use Type 값 형식이 잘못되었습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                   @ApiResponse(
                            responseCode = "1206", description = "Value 형식이 잘못되었습니다."
                            , content = @Content(schema = @Schema(hidden = true)))
            })
    public Response<Optional<AdminDto>> updateContent(
            @Parameter(name = "id", description = "id 값", in = ParameterIn.PATH) @PathVariable(value = "id") Long id,
            @Parameter(name = "request", description = "Request Dto", in = ParameterIn.PATH) @RequestBody @Valid AdminRequest request,
            HttpServletRequest req
    ) {
        return adminService.updateContent(id, request, req);
    }

    //삭제처리
    @DeleteMapping("administrator-management/{id}/delete")
    @Operation(
            summary = "delete Admin", description = "Admin 데이터 삭제합니다.",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = AdminDto.class))),
                    @ApiResponse(
                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1003", description = "id가 존재하지 않습니다.."
                            , content = @Content(schema = @Schema(hidden = true)))
            })
    public Response<Void> delete(
            @Parameter(name = "id", description = "id 값", in = ParameterIn.PATH) @PathVariable(value = "id") Long id,
            HttpServletRequest req
    ) {
        return adminService.deleteContent(id, req);
    }

}
