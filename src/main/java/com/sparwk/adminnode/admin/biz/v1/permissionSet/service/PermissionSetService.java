package com.sparwk.adminnode.admin.biz.v1.permissionSet.service;

import com.sparwk.adminnode.admin.biz.v1.CommonPagingListDto;
import com.sparwk.adminnode.admin.biz.v1.admin.service.AdminActiveLogService;
import com.sparwk.adminnode.admin.biz.v1.permissionSet.dto.AdminMenuDto;
import com.sparwk.adminnode.admin.biz.v1.permissionSet.dto.PermissionMenuSetDto;
import com.sparwk.adminnode.admin.biz.v1.permissionSet.dto.PermissionSetDto;
import com.sparwk.adminnode.admin.biz.v1.permissionSet.dto.PermissionSetRequest;
import com.sparwk.adminnode.admin.config.common.ActiveCodeConst;
import com.sparwk.adminnode.admin.config.common.GetAccountId;
import com.sparwk.adminnode.admin.config.common.Response;
import com.sparwk.adminnode.admin.config.common.ResultCodeConst;
import com.sparwk.adminnode.admin.jpa.entity.PeriodTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.admin.AdminActiveLogEntity;
import com.sparwk.adminnode.admin.jpa.entity.permissionSet.PermissionMenuSetEntity;
import com.sparwk.adminnode.admin.jpa.entity.permissionSet.PermissionSetCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.permissionSet.PermissionSetEntity;
import com.sparwk.adminnode.admin.jpa.entity.permissionSet.PermissionSetSorterEnum;
import com.sparwk.adminnode.admin.jpa.repository.permissionSet.PermissionMenuSetRepository;
import com.sparwk.adminnode.admin.jpa.repository.permissionSet.PermissionSetRepository;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class PermissionSetService {

    private final PermissionSetRepository permissionSetRepository;
    private final PermissionMenuSetRepository permissionMenuSetRepository;
    private final AdminActiveLogService adminActiveLogService;

    private final HttpServletRequest httpServletRequest;
    private final MessageSourceAccessor messageSourceAccessor;

    private final Logger logger = LoggerFactory.getLogger(PermissionSetService.class);

    public Response<CommonPagingListDto> getList(PermissionSetCateEnum cate,
                                                 String labelVal,
                                                 YnTypeEnum useType,
                                                 PeriodTypeEnum periodType,
                                                 String sdate,
                                                 String edate,
                                                 PermissionSetSorterEnum sorter,
                                                 PageRequest pageRequest) {

        Response<CommonPagingListDto> res = new Response<>();
        
        List<PermissionSetDto> permissionSetList = permissionSetRepository.findQryAll(cate,
                                                            labelVal,
                                                            useType,
                                                            periodType,
                                                            sdate,
                                                            edate,
                                                            sorter,
                                                            pageRequest);

        logger.info("permissionSetList - {}", permissionSetList);

        //조회내용 없을 때
        if(permissionSetList == null || permissionSetList.size() < 1){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        CommonPagingListDto list = new CommonPagingListDto();

        //총 카운트
        long totalSize = permissionSetRepository.countQryAll(cate,
                labelVal,
                useType,
                periodType,
                sdate,
                edate);
        long totalPage = totalSize / pageRequest.getPageSize();
        long mod = totalSize % pageRequest.getPageSize();
        if(mod > 0) {
            totalPage += 1;
        }

        list.setList(permissionSetList);
        list.setSize(pageRequest.getPageSize());
        list.setPage(pageRequest.getPageNumber());
        list.setTotalPage(totalPage);
        list.setTotalSize(totalSize);

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(list);
        return res;
    }

    public Response<List<PermissionSetDto>> getUseYList(PermissionSetCateEnum cate, String  labelVal) {

        Response<List<PermissionSetDto>> res = new Response<>();

        List<PermissionSetDto> permissionSetList = permissionSetRepository.findQryUseY(cate, labelVal);

        logger.info("permissionSetList - {}", permissionSetList);

        //조회내용 없을 때
        if(permissionSetList == null || permissionSetList.size() < 1){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(permissionSetList);
        return res;
    }

    public Response<Optional<PermissionSetDto>> getOne(Long id) {

        Response<Optional<PermissionSetDto>> res = new Response<>();

        //id값이 없을 때
        if(id == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<PermissionSetEntity> permissionSet = permissionSetRepository.findById(id);

        logger.info("permissionSet - {}", permissionSet);

        //조회내용 없을 때
        if(!permissionSet.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }
        
        Optional<PermissionSetDto> dtoPage =

                permissionSet
                        .map(p -> PermissionSetDto.builder()
                                .adminPermissionId(p.getAdminPermissionId())
                                .labelVal(p.getLabelVal())
                                .cdOrd(p.getCdOrd())
                                .cdDesc(p.getCdDesc())
                                .useType(p.getUseType())
                                .regUsr(p.getRegUsr())
                                .regDt(p.getRegDt())
                                .modUsr(p.getModUsr())
                                .modDt(p.getModDt())
                                .build()
                        );

        logger.info("CommonDetailCode - {}", dtoPage);

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(dtoPage);
        return res;
    }

    @Transactional
    public Response<Void> saveContent(PermissionSetRequest request, HttpServletRequest req) {

        Response<Void> res = new Response<>();

        //USEYN 값 없거나 틀릴 때
        if(request.getUseType() == null || (request.getUseType() != YnTypeEnum.Y && request.getUseType() != YnTypeEnum.N)){
            res.setResultCd(ResultCodeConst.NOT_VALID_USEYN.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if(request.getLabelVal() == null){
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        try {

            logger.info("11111111");

            PermissionSetEntity newPermissionSet = PermissionSetEntity.builder()
                    .labelVal(request.getLabelVal())
                    .cdDesc(request.getCdDesc())
                    .useType(request.getUseType())
                    .build();

            PermissionSetEntity saved = permissionSetRepository.save(newPermissionSet);

            for (PermissionMenuSetDto permissionMenuSetDto : request.getPermissionMenuSetDto()) {

                YnTypeEnum createYn;
                YnTypeEnum readYn;
                YnTypeEnum editYn;
                YnTypeEnum deleteYn;

                createYn = (permissionMenuSetDto.getCreateYn().equals("Y")) ? YnTypeEnum.Y : YnTypeEnum.N;
                readYn = (permissionMenuSetDto.getReadYn().equals("Y")) ? YnTypeEnum.Y : YnTypeEnum.N;
                editYn = (permissionMenuSetDto.getEditYn().equals("Y")) ? YnTypeEnum.Y : YnTypeEnum.N;
                deleteYn = (permissionMenuSetDto.getDeleteYn().equals("Y")) ? YnTypeEnum.Y : YnTypeEnum.N;


                //Value 값 없을 때
                if (permissionMenuSetDto.getMenuId() == null) {
                    res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
                    res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
                    return res;
                }

                //Value 값 없을 때
                if (permissionMenuSetDto.getAdminPermissionId() == null) {
                    res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
                    res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
                    return res;
                }

                Optional<PermissionMenuSetEntity> isPermissionMenuSet =
                        permissionMenuSetRepository.findByAdminMenuIdAndAdminPermissionId(permissionMenuSetDto.getMenuId(), newPermissionSet.getAdminPermissionId());

                //조회내용 없을 때 새로 입력, 있으면 수정
                if (!isPermissionMenuSet.isPresent()) {

                    PermissionMenuSetEntity newPermissionMenuSet = PermissionMenuSetEntity.builder()
                            .adminMenuId(permissionMenuSetDto.getMenuId())
                            .adminPermissionId(newPermissionSet.getAdminPermissionId())
                            .createYn(createYn)
                            .readYn(readYn)
                            .editYn(editYn)
                            .deleteYn(deleteYn)
                            .build();

                    PermissionMenuSetEntity saved2 = permissionMenuSetRepository.save(newPermissionMenuSet);

                    logger.info("newPermissionMenuSet - {} ", newPermissionMenuSet);
                } else {

                    logger.info("update!!!!!!!!!!!!!!!!!!!!");

                    isPermissionMenuSet.get().update(createYn, readYn, editYn, deleteYn);
                }
            }

//            Optional<PermissionSetEntity> permissionSet = permissionSetRepository.findById(saved.getAdminPermissionId());
//
//            Optional<PermissionSetDto> dtoPage =
//
//                    permissionSet
//                            .map(p -> PermissionSetDto.builder()
//                                    .adminPermissionId(p.getAdminPermissionId())
//                                    .adminPermissionGroupId(p.getPermissionSetGroupsEntity().getAdminPermissionGroupId())
//                                    .adminPermissionGroupVal(p.getPermissionSetGroupsEntity().getLabelVal())
//                                    .labelVal(p.getLabelVal())
//                                    .cdOrd(p.getCdOrd())
//                                    .cdDesc(p.getCdDesc())
//                                    .useType(p.getUseType())
//                                    .regUsr(p.getRegUsr())
//                                    .regDt(p.getRegDt())
//                                    .modUsr(p.getModUsr())
//                                    .modDt(p.getModDt())
//                                    .build()
//                            );

            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
//            res.setResult(dtoPage);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));

            String menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_PERMISSION_SETS.getCode());
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_CREATE.getCode());

            //netive query
            String usrName = adminActiveLogService.findQryUsrName(saved.getRegUsr());

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(saved.getRegDt())
                    .adminId(saved.getRegUsr())
                    .menuName(menuName)
                    .activeType("C")
                    .contentsId(saved.getAdminPermissionId())
                    .activeMsg(usrName + " " + activeMsg + " '" + saved.getLabelVal() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e){
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }
        return res;
    }

    @Transactional
    public Response<Void> updateYn(Long id, HttpServletRequest req) {

        Response<Void> res = new Response<>();

        //id값이 없을 때
        if(id == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<PermissionSetEntity> permissionSet = permissionSetRepository.findById(id);

        logger.info("permissionSet - {}", permissionSet);

        //조회내용 없을 때
        if(!permissionSet.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        YnTypeEnum useType = permissionSet.get().getUseType();
        YnTypeEnum newUseType = (useType== YnTypeEnum.Y) ? YnTypeEnum.N : YnTypeEnum.Y;

        logger.info("newUseType - {} " , newUseType);

        try {
            permissionSet.get().updateYn(newUseType);
            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            //res.setResult(songDetailCode);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));

            String menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_PERMISSION_SETS.getCode());
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_MODIFY.getCode());

            //netive query
            String usrName = adminActiveLogService.findQryUsrName(permissionSet.get().getModUsr());

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(permissionSet.get().getModDt())
                    .adminId(permissionSet.get().getModUsr())
                    .menuName(menuName)
                    .activeType("U")
                    .contentsId(permissionSet.get().getAdminPermissionId())
                    .activeMsg(usrName + " " + activeMsg + " '" + permissionSet.get().getLabelVal() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e) {
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }
        return res;

    }

    @Transactional
    public Response<Optional<PermissionSetDto>> updateContent(Long id, PermissionSetRequest request, HttpServletRequest req) {

        Response<Optional<PermissionSetDto>> res = new Response<>();

        //id값이 없을 때
        if(id == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<PermissionSetEntity> permissionSet = permissionSetRepository.findById(id);

        logger.info("permissionSet - {}", permissionSet);

        //조회내용 없을 때
        if(!permissionSet.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //USEYN 값 없거나 틀릴 때
        if(request.getUseType() == null || (request.getUseType() != YnTypeEnum.Y && request.getUseType() != YnTypeEnum.N)){
            res.setResultCd(ResultCodeConst.NOT_VALID_USEYN.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if(request.getLabelVal() == null){
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        try {
            permissionSet.get().update(request.getLabelVal(),
                    request.getCdOrd(),
                    request.getCdDesc(),
                    request.getUseType()
                    );

            Optional<PermissionSetEntity> permissionSetUpdate =
                    permissionSetRepository.findById(permissionSet.get().getAdminPermissionId());

            Optional<PermissionSetDto> dtoPage =

                    permissionSetUpdate
                            .map(p -> PermissionSetDto.builder()
                                    .adminPermissionId(p.getAdminPermissionId())
                                    .labelVal(p.getLabelVal())
                                    .cdOrd(p.getCdOrd())
                                    .cdDesc(p.getCdDesc())
                                    .useType(p.getUseType())
                                    .regUsr(p.getRegUsr())
                                    .regDt(p.getRegDt())
                                    .modUsr(p.getModUsr())
                                    .modDt(p.getModDt())
                                    .build()
                            );

            logger.info("dtoPage - {} ", dtoPage);

            for (PermissionMenuSetDto permissionMenuSetDto : request.getPermissionMenuSetDto()) {

                YnTypeEnum createYn;
                YnTypeEnum readYn;
                YnTypeEnum editYn;
                YnTypeEnum deleteYn;

                createYn = (permissionMenuSetDto.getCreateYn().equals("Y")) ? YnTypeEnum.Y : YnTypeEnum.N;
                readYn = (permissionMenuSetDto.getReadYn().equals("Y")) ? YnTypeEnum.Y : YnTypeEnum.N;
                editYn = (permissionMenuSetDto.getEditYn().equals("Y")) ? YnTypeEnum.Y : YnTypeEnum.N;
                deleteYn = (permissionMenuSetDto.getDeleteYn().equals("Y")) ? YnTypeEnum.Y : YnTypeEnum.N;


                //Value 값 없을 때
                if (permissionMenuSetDto.getMenuId() == null) {
                    res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
                    res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
                    return res;
                }

                //Value 값 없을 때
                if (permissionMenuSetDto.getAdminPermissionId() == null) {
                    res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
                    res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
                    return res;
                }

                Optional<PermissionMenuSetEntity> isPermissionMenuSet =
                        permissionMenuSetRepository.findByAdminMenuIdAndAdminPermissionId(permissionMenuSetDto.getMenuId(), permissionMenuSetDto.getAdminPermissionId());

                //조회내용 없을 때 새로 입력, 있으면 수정
                if (!isPermissionMenuSet.isPresent()) {

                    PermissionMenuSetEntity newPermissionMenuSet = PermissionMenuSetEntity.builder()
                            .adminMenuId(permissionMenuSetDto.getMenuId())
                            .adminPermissionId(permissionMenuSetDto.getAdminPermissionId())
                            .createYn(createYn)
                            .readYn(readYn)
                            .editYn(editYn)
                            .deleteYn(deleteYn)
                            .build();

                    PermissionMenuSetEntity saved2 = permissionMenuSetRepository.save(newPermissionMenuSet);

                    logger.info("newPermissionMenuSet - {} ", newPermissionMenuSet);
                } else {
                    isPermissionMenuSet.get().update(createYn, readYn, editYn, deleteYn);
                }
            }

            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            res.setResult(dtoPage);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));

            String menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_PERMISSION_SETS.getCode());
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_MODIFY.getCode());

            //netive query
            String usrName = adminActiveLogService.findQryUsrName(permissionSet.get().getModUsr());

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(permissionSet.get().getModDt())
                    .adminId(permissionSet.get().getModUsr())
                    .menuName(menuName)
                    .activeType("U")
                    .contentsId(permissionSet.get().getAdminPermissionId())
                    .activeMsg(usrName + " " + activeMsg + " '" + permissionSet.get().getLabelVal() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e){
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }

        return res;
    }

    @Transactional
    public Response<Void> deleteContent(Long id, HttpServletRequest req) {

        Response<Void> res = new Response<>();

        //id값이 없을 때
        if(id == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        PermissionSetEntity permissionSet = permissionSetRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("해당 내용이 없습니다"));

        logger.info("permissionSet - {} ", permissionSet);

        //조회내용 없을 때
        if(permissionSet == null){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        try {
            //fk때문에 먼저 삭제
            permissionMenuSetRepository.deleteByAdminPermissionId(id);
            permissionSetRepository.deleteById(id);

            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
//            res.setResult(dtoPage);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));
            Long adminId = getAccountId.ofId(httpServletRequest);

            String menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_PERMISSION_SETS.getCode());
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_DELETE.getCode());

            //netive query
            String usrName = adminActiveLogService.findQryUsrName(adminId);

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(LocalDateTime.now())
                    .adminId(adminId)
                    .menuName(menuName)
                    .activeType("D")
                    .contentsId(permissionSet.getAdminPermissionId())
                    .activeMsg(usrName + " " + activeMsg + " '" + permissionSet.getLabelVal() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e) {
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }
        return res;

    }

}
