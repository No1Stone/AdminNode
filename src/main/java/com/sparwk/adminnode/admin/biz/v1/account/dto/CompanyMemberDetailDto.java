package com.sparwk.adminnode.admin.biz.v1.account.dto;

import com.sparwk.adminnode.admin.biz.v1.profile.dto.ProfileCompanyPartnerDto;
import com.sparwk.adminnode.admin.biz.v1.profile.dto.ProfileCompanyRosteredDto;
import com.sparwk.adminnode.admin.biz.v1.profile.dto.ProfileCompanySnsDto;
import com.sparwk.adminnode.admin.biz.v1.profile.dto.ProfileCompanyStudioDto;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
@NoArgsConstructor
public class CompanyMemberDetailDto {

    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long accntId;
    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long profileId;
    @Schema(description = "COMPANY NAME", nullable = false, example = "EKKO Music Rights")
    private String companyName;

    @Schema(description = "COMPANT INFO EMAIL", nullable = false, example = "info@ekkomusicrights.com")
    private String companyEmail;
    @Schema(description = "COMPANY TYPE", nullable = true)
    private List<AccountCompanyTypeDto> accountCompanyTypeDtoList;
    @Schema(description = "COMPANY TYPE", nullable = true)
    private List<AccountCompanyLocationDto> accountCompanyLocationDtoList;
    @Schema(description = "COMPANY POST", nullable = true, example = "06012")
    private String companyPost;
    @Schema(description = "COMPANY REGION", nullable = true, example = "Seoul")
    private String companyRegion;
    @Schema(description = "COMPANY CITY/TOWN", nullable = true, example = "Cheongdam-dong, Gangna")
    private String companyCity;
    @Schema(description = "COMPANY ADDR1", nullable = true, example = "114 Seolleung-ro 190-gil")
    private String companyAddr1;
    @Schema(description = "COMPANY ADDR2", nullable = true, example = "SM Entertainment Studio Center")
    private String companyAddr2;
    @Schema(description = "COMPANY HeadLine", nullable = true, example = "SM Entertainment Studio Center")
    private String headline;


    @Schema(description = "BUSINESS LICENSE DOC", nullable = true)
    private String companyLicenseFileUrl;
    @Schema(description = "BUSINESS LICENSE DOC FILE NAME", nullable = true)
    private String companyLicenseFile;
    @Schema(description = "SUBSIDIARY COMPANIES", nullable = true)
    private String subsidiaryCompanies;
    @Schema(description = "AFFILIATED COMPANIES", nullable = true)
    private String affiliatedCompanies;

    @Schema(description = "PRIMARY CONTACT NAME", nullable = false, example = "Yoon")
    private String primaryContactFirstName;
    @Schema(description = "PRIMARY CONTACT NAME", nullable = true, example = "Jung")
    private String primaryContactMiddleName;
    @Schema(description = "PRIMARY CONTACT NAME", nullable = false, example = "Min")
    private String primaryContactLastName;
    @Schema(description = "PRIMARY CONTACT EMAIL", nullable = false, example = "yoon@sparwk.com")
    private String primaryContactEmail;
    @Schema(description = "PRIMARY CONTACT Phone", nullable = true, example = "0821012341234")
    private String primaryContactPhone;

    @Schema(description = "COMPANY LEGAL NAME", nullable = true, example = "EKKO Music Rights")
    private String companyLegalName;
    @Schema(description = "COMPANY ON THE WEB", nullable = true)
    private List<ProfileCompanySnsDto> profileCompanyOnthewebDtoList;

    @Schema(description = "COMPANY IPI No.", nullable = true, example = "W123456789")
    private String companyIpiNo;
    @Schema(description = "COMPANY VAT No.", nullable = true, example = "W123456789")
    private String companyVatNo;

    @Schema(description = "COMPANY ON THE WEB", nullable = true)
    private List<ProfileCompanyPartnerDto> profileCompanyPartnerDto;

    @Schema(description = "COMPANY ON THE WEB", nullable = true)
    private List<ProfileCompanyRosteredDto> profileCompanyRosteredDto;

    @Schema(description = "COMPANY ON THE WEB", nullable = true)
    private List<ProfileCompanyStudioDto> profileCompanyStudioDto;


    @Builder
    public CompanyMemberDetailDto(Long accntId,
                                  Long profileId,
                                  String companyName,
                                  String companyEmail,
                                  List<AccountCompanyTypeDto> accountCompanyTypeDtoList,
                                  List<AccountCompanyLocationDto> accountCompanyLocationDtoList,
                                  String companyPost,
                                  String companyRegion,
                                  String companyCity,
                                  String companyAddr1,
                                  String companyAddr2,
                                  String headline,
                                  String companyLicenseFileUrl,
                                  String companyLicenseFile,
                                  String subsidiaryCompanies,
                                  String affiliatedCompanies,
                                  String primaryContactFirstName,
                                  String primaryContactMiddleName,
                                  String primaryContactLastName,
                                  String primaryContactEmail,
                                  String primaryContactPhone,
                                  String companyLegalName,
                                  List<ProfileCompanySnsDto> profileCompanyOnthewebDtoList,
                                  String companyIpiNo,
                                  String companyVatNo,
                                  List<ProfileCompanyPartnerDto> profileCompanyPartnerDto,
                                  List<ProfileCompanyRosteredDto> profileCompanyRosteredDto,
                                  List<ProfileCompanyStudioDto> profileCompanyStudioDto
    ) {
        this.accntId = accntId;
        this.profileId = profileId;
        this.companyName = companyName;
        this.companyEmail = companyEmail;
        this.accountCompanyTypeDtoList = accountCompanyTypeDtoList;
        this.accountCompanyLocationDtoList = accountCompanyLocationDtoList;
        this.companyPost = companyPost;
        this.companyRegion = companyRegion;
        this.companyCity = companyCity;
        this.companyAddr1 = companyAddr1;
        this.companyAddr2 = companyAddr2;
        this.headline = headline;
        this.companyLicenseFileUrl = companyLicenseFileUrl;
        this.companyLicenseFile = companyLicenseFile;
        this.subsidiaryCompanies = subsidiaryCompanies;
        this.affiliatedCompanies = affiliatedCompanies;
        this.primaryContactFirstName = primaryContactFirstName;
        this.primaryContactMiddleName = primaryContactMiddleName;
        this.primaryContactLastName = primaryContactLastName;
        this.primaryContactEmail = primaryContactEmail;
        this.primaryContactPhone = primaryContactPhone;
        this.companyLegalName = companyLegalName;
        this.profileCompanyOnthewebDtoList = profileCompanyOnthewebDtoList;
        this.companyIpiNo = companyIpiNo;
        this.companyVatNo = companyVatNo;
        this.profileCompanyPartnerDto = profileCompanyPartnerDto;
        this.profileCompanyRosteredDto = profileCompanyRosteredDto;
        this.profileCompanyStudioDto = profileCompanyStudioDto;

    }
}
