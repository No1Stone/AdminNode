package com.sparwk.adminnode.admin.biz.v1.email.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class EmailMemberSettingsDto {

    @Schema(description = "항목", nullable = false, example = "individual")
    private String colName;
    @Schema(description = "email 주소", nullable = true, example = "Y")
    private String val;

    @Builder
    public EmailMemberSettingsDto(String colName,
                                  String val
    ) {
        this.colName = colName;
        this.val = val;
    }
}
