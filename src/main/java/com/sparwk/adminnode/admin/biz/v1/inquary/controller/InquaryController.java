package com.sparwk.adminnode.admin.biz.v1.inquary.controller;

import com.sparwk.adminnode.admin.biz.v1.CommonPagingListDto;
import com.sparwk.adminnode.admin.biz.v1.inquary.dto.InquaryDto;
import com.sparwk.adminnode.admin.biz.v1.inquary.service.InquaryService;
import com.sparwk.adminnode.admin.config.common.AdminPermissionIntergrationService;
import com.sparwk.adminnode.admin.config.common.Response;
import com.sparwk.adminnode.admin.config.common.ResultCodeConst;
import com.sparwk.adminnode.admin.jpa.entity.inquary.InquaryCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.inquary.InquarySorterEnum;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/V1/configration/inquary/")
@RequiredArgsConstructor
@CrossOrigin("*")
public class InquaryController {

    private final Logger logger = LoggerFactory.getLogger(InquaryController.class);

    @Autowired
    private InquaryService inquaryService;

    @Autowired
    private AdminPermissionIntergrationService adminPermissionIntergrationService;
    private final MessageSourceAccessor messageSourceAccessor;

    @Value("${file.path}")
    private String fileRealPath;

    @GetMapping("admin-activities")
    @Operation(
            summary = "Active List", description = "Active All List ",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = InquaryDto.class))),
                    @ApiResponse(
                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "404", description = "존재하지 않는 리소스 접근"
                            , content = @Content(schema = @Schema(hidden = true)))
            })
    public Response<CommonPagingListDto> getList(
            @Parameter(name = "cate", description = "검색 종류", in = ParameterIn.PATH) @RequestParam(value = "cate", required = false) InquaryCateEnum cate,
            @Parameter(name = "val", description = "코드 값 포함", in = ParameterIn.PATH) @RequestParam(value = "val", required = false) String val,
            @Parameter(name = "sorter", description = "순서", in = ParameterIn.PATH) @RequestParam(value = "sorter", required = false) InquarySorterEnum sorter,
            @Parameter(name = "page", description = "페이지 번호", in = ParameterIn.PATH) @RequestParam(value = "page", required = false, defaultValue = "1") int page,
            @Parameter(name = "size", description = "페이지 사이즈", in = ParameterIn.PATH) @RequestParam(value = "size", required = false, defaultValue = "10") int size

    ) {

        PageRequest pageRequest = PageRequest.of(page, size);

        Response res = new Response();
        if(!adminPermissionIntergrationService.AdminPermissionIntergrationCheck(2L, "R")){
            res.setResultCd(ResultCodeConst.NOT_VALID_READ.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        return inquaryService.getList(cate, val, sorter, pageRequest);
    }

}
