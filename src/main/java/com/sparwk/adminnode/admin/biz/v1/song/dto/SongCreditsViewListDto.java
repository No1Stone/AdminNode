package com.sparwk.adminnode.admin.biz.v1.song.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class SongCreditsViewListDto {

    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long songId;
    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long profileId;
    @Schema(description = "SONG TITLE", nullable = false, example = "SONG_TITLE")
    private String fullName;
    @Schema(description = "AVATAR_FILE_URL", nullable = false, example = "http://")
    private String ipiInfo;
    @Schema(description = "SONG OWNER NAME", nullable = true, example = "OWNER PROFILE ID")
    private String caeInfo;
    @Schema(description = "PROJECT TITLE", nullable = false, example = "PROJ_TITLE")
    private String isniInfo;
    @Schema(description = "ipn", nullable = false, example = "11111111")
    private String ipnInfo;
    @Schema(description = "SONG GENRE TYPE", nullable = true, example = "SGG000008,SGG000009")
    private String nroTypeName;
    @Schema(description = "SONG GENRE TYPE", nullable = true, example = "SGG000008,SGG000009")
    private String nroInfo;
    @Schema(description = "SONG GENRE TYPE Name", nullable = true, example = "HipHop,Jazz")
    private String roleName;


    @Builder
    public SongCreditsViewListDto(Long songId,
                                  Long profileId,
                                  String fullName,
                                  String ipiInfo,
                                  String caeInfo,
                                  String isniInfo,
                                  String ipnInfo,
                                  String nroTypeName,
                                  String nroInfo,
                                  String roleName
    ) {
        this.songId = songId;
        this.profileId = profileId;
        this.fullName = fullName;
        this.ipiInfo = ipiInfo;
        this.caeInfo = caeInfo;
        this.isniInfo = isniInfo;
        this.ipnInfo = ipnInfo;
        this.nroTypeName = nroTypeName;
        this.nroInfo = nroInfo;
        this.roleName = roleName;
    }
}
