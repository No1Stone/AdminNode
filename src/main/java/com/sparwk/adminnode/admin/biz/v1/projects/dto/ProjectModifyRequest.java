package com.sparwk.adminnode.admin.biz.v1.projects.dto;

import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class ProjectModifyRequest {

    @Schema(description = "project id 번호", nullable = false, example = "1")
    private Long projId;
    @Schema(name = "PROJECT TITLE", nullable = false, example = "Title")
    private String projTitle;


    @Builder
    public ProjectModifyRequest(Long projId,
                                String projTitle
    ) {

        this.projId = projId;
        this.projTitle = projTitle;
    }
}
