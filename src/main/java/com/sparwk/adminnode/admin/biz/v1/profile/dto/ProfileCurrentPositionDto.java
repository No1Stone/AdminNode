package com.sparwk.adminnode.admin.biz.v1.profile.dto;

import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class ProfileCurrentPositionDto {

    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long profilePositionSeq;
    @Schema(description = "프로필 아이디", nullable = false, example = "10000000068")
    private Long profileId;
    @Schema(description = "회사프로필 아이디", nullable = false, example = "10000000068")
    private Long profileCompanyId;
    @Schema(description = "회사명", nullable = false, example = "회사명")
    private String profileCompanyName;
    @Schema(description = "아티스트여부", nullable = false, defaultValue = "N", allowableValues = {"Y", "N"})
    private YnTypeEnum artistYn;
    @Schema(description = "a&r여부", nullable = false, defaultValue = "N", allowableValues = {"Y", "N"})
    private YnTypeEnum anrYn;
    @Schema(description = "부서 및 역할", nullable = false, example = "부서 및 역할 설명")
    private String deptRoleInfo;
    @Schema(description = "primaryCompanyYn 여부", nullable = false, defaultValue = "N", allowableValues = {"Y", "N"})
    private YnTypeEnum primaryCompanyYn;
    @Schema(description = "회사인증여부", nullable = false, defaultValue = "N", allowableValues = {"Y", "N"})
    private YnTypeEnum companyVerifyYn;
    @Schema(description = "creator 여부", nullable = false, defaultValue = "Y", allowableValues = {"Y", "N"})
    private YnTypeEnum creatorYn;

    @Builder
    public ProfileCurrentPositionDto(
            Long profilePositionSeq,
            Long profileId,
            Long profileCompanyId,
            String profileCompanyName,
            YnTypeEnum anrYn,
            YnTypeEnum artistYn,
            String deptRoleInfo,
            YnTypeEnum primaryCompanyYn,
            YnTypeEnum companyVerifyYn,
            YnTypeEnum creatorYn
    ){
        this.profilePositionSeq = profilePositionSeq;
        this.profileId=profileId;
        this.profileCompanyId=profileCompanyId;
        this.profileCompanyName=profileCompanyName;
        this.anrYn=anrYn;
        this.artistYn=artistYn;
        this.deptRoleInfo=deptRoleInfo;
        this.primaryCompanyYn = primaryCompanyYn;
        this.companyVerifyYn = companyVerifyYn;
        this.creatorYn = creatorYn;
    }
}
