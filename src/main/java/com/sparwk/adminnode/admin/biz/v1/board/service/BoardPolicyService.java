package com.sparwk.adminnode.admin.biz.v1.board.service;

import com.sparwk.adminnode.admin.biz.v1.CommonPagingListDto;
import com.sparwk.adminnode.admin.biz.v1.admin.service.AdminActiveLogService;
import com.sparwk.adminnode.admin.biz.v1.board.dto.BoardDelete;
import com.sparwk.adminnode.admin.biz.v1.board.dto.BoardPolicyDto;
import com.sparwk.adminnode.admin.biz.v1.board.dto.BoardPolicyRequest;
import com.sparwk.adminnode.admin.biz.v1.commonCode.dto.CommonCateListDto;
import com.sparwk.adminnode.admin.config.common.ActiveCodeConst;
import com.sparwk.adminnode.admin.config.common.GetAccountId;
import com.sparwk.adminnode.admin.config.common.Response;
import com.sparwk.adminnode.admin.config.common.ResultCodeConst;
import com.sparwk.adminnode.admin.jpa.entity.PeriodTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.admin.AdminActiveLogEntity;
import com.sparwk.adminnode.admin.jpa.entity.admin.AdminEntity;
import com.sparwk.adminnode.admin.jpa.entity.board.BoardFaqEntity;
import com.sparwk.adminnode.admin.jpa.entity.board.PolicyCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.board.BoardPolicyEntity;
import com.sparwk.adminnode.admin.jpa.entity.board.PolicySorterEnum;
import com.sparwk.adminnode.admin.jpa.entity.commonCode.CommonCodeEntity;
import com.sparwk.adminnode.admin.jpa.entity.commonCode.CommonDetailCodeEntity;
import com.sparwk.adminnode.admin.jpa.repository.admin.AdminRepository;
import com.sparwk.adminnode.admin.jpa.repository.board.BoardPolicyRepository;
import com.sparwk.adminnode.admin.jpa.repository.commonCode.CommonCodeRepository;
import com.sparwk.adminnode.admin.jpa.repository.commonCode.CommonDetailCodeRepository;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class BoardPolicyService {

    private final CommonCodeRepository commonCodeRepository;
    private final CommonDetailCodeRepository commonDetailCodeRepository;

    private final BoardPolicyRepository boardPolicyRepository;
    private final AdminRepository adminRepository;
    private final AdminActiveLogService adminActiveLogService;

    private final HttpServletRequest httpServletRequest;
    private final MessageSourceAccessor messageSourceAccessor;

    private final Logger logger = LoggerFactory.getLogger(BoardPolicyService.class);

    public Response<List<CommonCateListDto>> getCateList(String pcode) {

        Response<List<CommonCateListDto>> res = new Response<>();

        //부모코드 없을 때
        if (pcode == null) {
            res.setResultCd(ResultCodeConst.NOT_EXIST_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<CommonCodeEntity> commonCode = Optional.ofNullable(
                commonCodeRepository.findByCode(pcode)
        );

        logger.info("commonCode - {}", commonCode);

        //부모코드 조회내용 없을 때
        if (!commonCode.isPresent()) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        List<CommonCateListDto> cateList = commonDetailCodeRepository.findQryCateList(pcode);

        logger.info("cateList - {}", cateList);

        //조회내용 없을 때
        if (cateList == null || cateList.size() < 1) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(cateList);
        return res;
    }



    public Response<CommonPagingListDto> getList(String pcode,
                                                 PolicyCateEnum cate,
                                                 String val,
                                                 YnTypeEnum useType,
                                                 PeriodTypeEnum periodType,
                                                 String sdate,
                                                 String edate,
                                                 PolicySorterEnum sorter,
                                                 PageRequest pageRequest) {

        Response<CommonPagingListDto> res = new Response<>();

        if(pcode != null) {

            Optional<CommonCodeEntity> commonCode = Optional.ofNullable(
                    commonCodeRepository.findByCode(pcode)
            );

            logger.info("commonCode - {}", commonCode);

            //부모코드 조회내용 없을 때
            if (!commonCode.isPresent()) {
                res.setResultCd(ResultCodeConst.NOT_FOUND_PCODE.getCode());
                res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
                return res;
            }
        }

        List<BoardPolicyDto> boardList =
                boardPolicyRepository.findQryAll(pcode, cate, val, useType, periodType, sdate, edate, sorter, pageRequest);

        logger.info("BoardPolicyList - {}", boardList);

        //조회내용 없을 때
        if (boardList == null || boardList.size() < 1) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        CommonPagingListDto list = new CommonPagingListDto();

        //총 카운트
        long totalSize = boardPolicyRepository.countQryAll(pcode, cate, val, useType, periodType, sdate, edate);
        long totalPage = totalSize / pageRequest.getPageSize();
        long mod = totalSize % pageRequest.getPageSize();
        if(mod > 0) {
            totalPage += 1;
        }

        list.setList(boardList);
        list.setSize(pageRequest.getPageSize());
        list.setPage(pageRequest.getPageNumber());
        list.setTotalPage(totalPage);
        list.setTotalSize(totalSize);

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(list);
        return res;
    }

    public Response<CommonPagingListDto> getUseYList(String pcode, PolicyCateEnum cate, String val, PageRequest pageRequest) {

        Response<CommonPagingListDto> res = new Response<>();

        if(pcode != null) {

            Optional<CommonCodeEntity> commonCode = Optional.ofNullable(
                    commonCodeRepository.findByCode(pcode)
            );

            logger.info("commonCode - {}", commonCode);

            //부모코드 조회내용 없을 때
            if (!commonCode.isPresent()) {
                res.setResultCd(ResultCodeConst.NOT_FOUND_PCODE.getCode());
                res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
                return res;
            }
        }

        List<BoardPolicyDto> boardList = boardPolicyRepository.findQryUseY(pcode, cate, val, pageRequest);

        logger.info("BoardPolicyList - {}", boardList);

        //조회내용 없을 때
        if (boardList == null || boardList.size() < 1) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        CommonPagingListDto list = new CommonPagingListDto();

        //총 카운트
        long totalSize = boardPolicyRepository.countQryUseY(pcode, cate, val);
        long totalPage = totalSize / pageRequest.getPageSize();
        long mod = totalSize % pageRequest.getPageSize();
        if(mod > 0) {
            totalPage += 1;
        }

        list.setList(boardList);
        list.setSize(pageRequest.getPageSize());
        list.setPage(pageRequest.getPageNumber());
        list.setTotalPage(totalPage);
        list.setTotalSize(totalSize);

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(list);
        return res;
    }

    public Response<Optional<BoardPolicyDto>> getOne(Long id) {

        Response<Optional<BoardPolicyDto>> res = new Response<>();

        //id값이 없을 때
        if (id == null) {
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<BoardPolicyEntity> boardNews = boardPolicyRepository.findById(id);

        logger.info("boardNews - {}", boardNews);

        //조회내용 없을 때
        if (!boardNews.isPresent()) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //내용 조회 후 카테고리를 검색
        Optional<CommonDetailCodeEntity> commonDetailCode = Optional.ofNullable(
                commonDetailCodeRepository.findByDcode(boardNews.get().getCateCd())
        );

        logger.info("commonDetailCode - {}", commonDetailCode);

        //부모코드 조회내용 없을 때
        if (!commonDetailCode.isPresent()) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //사용자 이름 검색
        Optional<AdminEntity> regUsr = adminRepository.findById(boardNews.get().getRegUsr());
        String regName = (!regUsr.isPresent()) ? "Sparwk Manager" : regUsr.get().getFullName();
        Optional<AdminEntity> modUsr = adminRepository.findById(boardNews.get().getModUsr());
        String modName = (!modUsr.isPresent()) ? "Sparwk Manager" : modUsr.get().getFullName();

        Optional<BoardPolicyDto> dtoPage =

                boardNews
                        .map(p -> BoardPolicyDto.builder()
                                .policyId(p.getPolicyId())
                                .cateCd(p.getCateCd())
                                .cateCdName(commonDetailCode.get().getVal())
                                .title(p.getTitle())
                                .content(p.getContent())
                                .useType(p.getUseYn())
                                .hit(p.getHit())
                                .regUsr(p.getRegUsr())
                                .regUsrName(regName)
                                .regDt(p.getRegDt())
                                .modUsr(p.getModUsr())
                                .modDt(p.getModDt())
                                .modUsrName(modName)
                                .build()
                        );

        logger.info("BoardPolicy - {}", dtoPage);

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(dtoPage);
        return res;
    }



    @Transactional
    public Response<Void> saveContent(@NotNull BoardPolicyRequest request, HttpServletRequest req) {

        Response<Void> res = new Response<>();

        //카테고리코드 없을 때
        if (request.getCateCd() == null) {
            res.setResultCd(ResultCodeConst.NOT_VALID_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<CommonDetailCodeEntity> commonDetailCode =
                Optional.ofNullable(commonDetailCodeRepository.findByDcode(request.getCateCd()));

        logger.info("commonDetailCode - {}", commonDetailCode);

        //카테고리코드 조회내용 없을 때
        if (!commonDetailCode.isPresent()) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //USEYN 값 없거나 틀릴 때
        if (request.getUseType() == null || (request.getUseType() != YnTypeEnum.Y && request.getUseType() != YnTypeEnum.N)) {
            res.setResultCd(ResultCodeConst.NOT_VALID_USEYN.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if (request.getTitle() == null) {
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }



        try {

            BoardPolicyEntity newBoardPolicy = BoardPolicyEntity.builder()
                    .cateCd(request.getCateCd())
                    .title(request.getTitle())
                    .content(request.getContent())
                    .useYn(request.getUseType())
                    .hit(0L)
                    .build();

            BoardPolicyEntity saved = boardPolicyRepository.save(newBoardPolicy);

            Optional<BoardPolicyEntity> BoardPolicy = boardPolicyRepository.findById(saved.getPolicyId());

            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
//            res.setResult(dtoPage);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));

            String menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_TERMS_POLICIES.getCode());
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_CREATE.getCode());

            //netive query
            String usrName = adminActiveLogService.findQryUsrName(saved.getRegUsr());

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(saved.getRegDt())
                    .adminId(saved.getRegUsr())
                    .menuName(menuName)
                    .activeType("C")
                    .contentsId(saved.getPolicyId())
                    .activeMsg(usrName + " " + activeMsg + " '" + saved.getTitle() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e) {
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }

        return res;
    }

    @Transactional
    public Response<Optional<BoardPolicyDto>> updateContent(String type, Long id, @NotNull BoardPolicyRequest request, HttpServletRequest req) {

        Response<Optional<BoardPolicyDto>> res = new Response<>();

        //부모코드 없을 때
        if (type == null) {
            res.setResultCd(ResultCodeConst.NOT_VALID_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<CommonDetailCodeEntity> commonDetailCode =
                Optional.ofNullable(commonDetailCodeRepository.findByDcode(request.getCateCd()));

        logger.info("commonCode - {}", commonDetailCode);

        //부모코드 조회내용 없을 때
        if (!commonDetailCode.isPresent()) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //id값이 없을 때
        if (id == null) {
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<BoardPolicyEntity> board = boardPolicyRepository.findById(id);


        logger.info("boardNews - {} ", board);

        //조회내용 없을 때
        if (!board.isPresent()) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //USEYN 값 없거나 틀릴 때
        if (request.getUseType() == null || (request.getUseType() != YnTypeEnum.Y && request.getUseType() != YnTypeEnum.N)) {
            res.setResultCd(ResultCodeConst.NOT_VALID_USEYN.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if (request.getTitle() == null) {
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        try {
            logger.info("/////////////////////////////// boardNews - asdflkajsdflkja ");


            board.get().update(request.getCateCd(),
                    request.getTitle(),
                    request.getContent(),
                    request.getUseType());

            logger.info("/////////////////////////////// boardNews - {} ", board.toString());

            Optional<BoardPolicyEntity> boardNewsUpdate = boardPolicyRepository.findById(board.get().getPolicyId());

            Optional<BoardPolicyDto> dtoPage =

                    boardNewsUpdate
                            .map(p -> BoardPolicyDto.builder()
                                    .policyId(p.getPolicyId())
                                    .cateCd(p.getCateCd())
                                    .cateCdName(commonDetailCode.get().getVal())
                                    .title(p.getTitle())
                                    .content(p.getContent())
                                    .useType(p.getUseYn())
                                    .hit(p.getHit())
                                    .regUsr(p.getRegUsr())
                                    .regDt(p.getRegDt())
                                    .modUsr(p.getModUsr())
                                    .modDt(p.getModDt())
                                    .build()
                            );

            logger.info("dtoPage - {} ", dtoPage);

            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            res.setResult(dtoPage);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));

            String menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_TERMS_POLICIES.getCode());
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_MODIFY.getCode());

            //netive query
            String usrName = adminActiveLogService.findQryUsrName(boardNewsUpdate.get().getModUsr());

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(boardNewsUpdate.get().getModDt())
                    .adminId(boardNewsUpdate.get().getModUsr())
                    .menuName(menuName)
                    .activeType("U")
                    .contentsId(boardNewsUpdate.get().getPolicyId())
                    .activeMsg(usrName + " " + activeMsg + " '" + boardNewsUpdate.get().getTitle() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e) {
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }

        return res;
    }

    @Transactional
    public Response<Void> deleteContent(Long id, HttpServletRequest req) {

        Response<Void> res = new Response<>();

        //id값이 없을 때
        if (id == null) {
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<BoardPolicyEntity> boardNews = boardPolicyRepository.findById(id);

        logger.info("boardNews - {} ", boardNews);

        //조회내용 없을 때
        if (!boardNews.isPresent()) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        try {
            boardPolicyRepository.deleteById(id);
            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
//            res.setResult(dtoPage);

             ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));
            Long adminId = getAccountId.ofId(httpServletRequest);

            String menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_TERMS_POLICIES.getCode());
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_DELETE.getCode());

            //netive query
            String usrName = adminActiveLogService.findQryUsrName(adminId);

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(LocalDateTime.now())
                    .adminId(adminId)
                    .menuName(menuName)
                    .activeType("D")
                    .contentsId(boardNews.get().getPolicyId())
                    .activeMsg(usrName + " " + activeMsg + " '" + boardNews.get().getTitle() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e) {
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }
        return res;

    }

    @Transactional
    public Response<Void> deleteList(@NotNull BoardDelete request) {

        Response<Void> res = new Response<>();

        //id값이 없을 때
        if (request.getId() == null) {
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //조회내용 없을 때
        try {

            Long[] kkk = request.getId();

            for(int i=0; i < kkk.length; i++) {

                Optional<BoardPolicyEntity> board = boardPolicyRepository.findById(request.getId()[i]);

                ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
                GetAccountId getAccountId = new GetAccountId();
                logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));
                Long adminId = getAccountId.ofId(httpServletRequest);


                String menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_TERMS_POLICIES.getCode());
                String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_DELETE.getCode());

                //netive query
                String usrName = adminActiveLogService.findQryUsrName(adminId);

                AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                        .activeTime(LocalDateTime.now())
                        .adminId(adminId)
                        .menuName(menuName)
                        .activeType("D")
                        .contentsId(board.get().getPolicyId())
                        .activeMsg(usrName + " " + activeMsg + " '" + board.get().getTitle() + "' in " + menuName)
                        .build();

                adminActiveLogService.saveContent(newLog);
            }

            boardPolicyRepository.deleteQryList(request.getId());
            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
//            res.setResult(dtoPage);
        } catch (Exception e) {
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }
        return res;

    }

}
