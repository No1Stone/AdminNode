package com.sparwk.adminnode.admin.biz.v1.profile.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class ProfileMetaDataDto {

    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long profileId;
    @Schema(description = "부모코드", nullable = false, example = "GND")
    private String attrTypeCd;
    @Schema(description = "자식코드", nullable = false, example = "GND000001")
    private String attrDtlCd;
    @Schema(description = "자식코드값", nullable = true, example = "MALE")
    private String attrDtlCdVal;

    @Builder
    public ProfileMetaDataDto(Long profileId,
                              String attrTypeCd,
                              String attrDtlCd,
                              String attrDtlCdVal
    ) {
        this.profileId = profileId;
        this.attrTypeCd = attrTypeCd;
        this.attrDtlCd = attrDtlCd;
        this.attrDtlCdVal = attrDtlCdVal;
    }
}
