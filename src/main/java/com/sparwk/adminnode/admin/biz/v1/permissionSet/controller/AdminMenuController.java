package com.sparwk.adminnode.admin.biz.v1.permissionSet.controller;

import com.sparwk.adminnode.admin.biz.v1.permissionSet.dto.AdminMenuDto;
import com.sparwk.adminnode.admin.biz.v1.permissionSet.dto.PermissionMenuSetDto;
import com.sparwk.adminnode.admin.biz.v1.permissionSet.dto.PermissionSetDto;
import com.sparwk.adminnode.admin.biz.v1.permissionSet.dto.PermissionSetRequest;
import com.sparwk.adminnode.admin.biz.v1.permissionSet.service.AdminMenuService;
import com.sparwk.adminnode.admin.config.common.Response;
import com.sparwk.adminnode.admin.jpa.entity.PeriodTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.permissionSet.PermissionSetSorterEnum;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/V1/setting/administrator/")
@RequiredArgsConstructor
@CrossOrigin("*")
public class AdminMenuController {

    private final Logger logger = LoggerFactory.getLogger(AdminMenuController.class);

    @Autowired
    private AdminMenuService adminMenuService;

    @GetMapping("admin-menu/all")
    @Operation(
            summary = "Admin Menu List", description = "Admin Menu 입니다.",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = AdminMenuDto.class))),
                    @ApiResponse(
                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
                            , content = @Content(schema = @Schema(hidden = true)))
            })
    public Response<List<AdminMenuDto>> getUseYList(
            //기본으로 사용
    ) {

        return adminMenuService.getUseYList();
    }

    @GetMapping("admin-menu/first")
    @Operation(
            summary = "Admin Menu List", description = "Admin Menu 입니다.",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = AdminMenuDto.class))),
                    @ApiResponse(
                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
                            , content = @Content(schema = @Schema(hidden = true)))
            })
    public Response<List<AdminMenuDto>> getUseYFirstList(
            //기본으로 사용
    ) {

        return adminMenuService.getUseYFirstList();
    }

    @GetMapping("admin-menu/children")
    @Operation(
            summary = "Admin Menu List", description = "Admin Menu 입니다.",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = AdminMenuDto.class))),
                    @ApiResponse(
                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
                            , content = @Content(schema = @Schema(hidden = true)))
            })
    public Response<List<AdminMenuDto>> getUseYChildrenList
            (
             @Parameter(name = "depth", description = "뎁스, 레벨", in = ParameterIn.PATH) @RequestParam(value = "depth", required = true) int depth,
             @Parameter(name = "uppMenuSeq", description = "부모 id", in = ParameterIn.PATH) @RequestParam(value = "uppMenuSeq", required = true) Long uppMenuSeq
    ) {

        return adminMenuService.getUseYChildrenList(depth, uppMenuSeq);
    }

    @GetMapping("admin-menu/setting/{id}")
    @Operation(
            summary = "Setting Admin Menu List", description = "Admin Menu 입니다.",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = AdminMenuDto.class))),
                    @ApiResponse(
                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
                            , content = @Content(schema = @Schema(hidden = true)))
            })
    public Response<List<PermissionMenuSetDto>> getMenuSettingList
            (
                    @Parameter(name = "id", description = "id 값", in = ParameterIn.PATH) @PathVariable("id") Long id
            ) {

        return adminMenuService.getMenuSettingList(id);
    }

    //저장 처리
    @PostMapping("admin-menu/setting/{id}")
    @Operation(
            summary = "Save Permission Menu Set", description = "PermissionSet을 저장 또는 수정합니다.",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = PermissionMenuSetDto.class))),
                    @ApiResponse(
                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1003", description = "id가 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1203", description = "Use Type 값 형식이 잘못되었습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1206", description = "Value 형식이 잘못되었습니다."
                            , content = @Content(schema = @Schema(hidden = true)))
            })
    public Response<Void> saveContent(
            @Parameter(name = "request", description = "Request Dto", in = ParameterIn.PATH) @RequestBody @Valid List<PermissionMenuSetDto> request,
            HttpServletRequest req
    ) {
        return adminMenuService.saveContent(request, req);
    }

}
