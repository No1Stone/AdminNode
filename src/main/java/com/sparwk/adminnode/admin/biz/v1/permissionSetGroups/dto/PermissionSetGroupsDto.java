package com.sparwk.adminnode.admin.biz.v1.permissionSetGroups.dto;

import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Setter
@Getter
@NoArgsConstructor
public class PermissionSetGroupsDto {

    @Schema(description = "부모 id값 Seq 사용", nullable = false, example = "1")
    private Long adminPermissionGroupId;
    @Schema(description = "코드 값", nullable = false, example = "admin")
    private String labelVal;
    @Schema(description = "코드 사용여부 Y/N", nullable = false, defaultValue = "Y", allowableValues = {"Y", "N"})
    private YnTypeEnum useType;
    @Schema(description = "description 설명", nullable = true, example = "설명")
    private String cdDesc;
    @Schema(description = "코드 순번 숫자", nullable = true, example = "1")
    private int cdOrd;
    @Schema(description = "등록일", pattern = "yyMMdd hh:mm:ss", example = "2021-12-08T05:44:26.627991")
    private LocalDateTime regDt;
    @Schema(description = "등록자", nullable = false, example = "10000001")
    private Long regUsr;
    @Schema(description = "수정일", pattern = "yyMMdd hh:mm:ss", example = "2021-12-08T05:44:26.627991")
    private LocalDateTime modDt;
    @Schema(description = "수정자", nullable = false, example = "10000001")
    private Long modUsr;

    @Builder
    public PermissionSetGroupsDto(Long adminPermissionGroupId,
                                  String labelVal,
                                  YnTypeEnum useType,
                                  String cdDesc,
                                  int cdOrd,
                                  LocalDateTime regDt,
                                  Long regUsr,
                                  LocalDateTime modDt,
                                  Long modUsr
    ) {
        this.adminPermissionGroupId = adminPermissionGroupId;
        this.labelVal = labelVal;
        this.useType = useType;
        this.cdDesc = cdDesc;
        this.cdOrd = cdOrd;
        this.regDt = regDt;
        this.regUsr = regUsr;
        this.modDt = modDt;
        this.modUsr = modUsr;
    }
}
