package com.sparwk.adminnode.admin.biz.v1.board.service;

import com.google.gson.Gson;
import com.sparwk.adminnode.admin.biz.v1.CommonPagingListDto;
import com.sparwk.adminnode.admin.biz.v1.account.dto.AccountMemberMailDto;
import com.sparwk.adminnode.admin.biz.v1.admin.service.AdminActiveLogService;
import com.sparwk.adminnode.admin.biz.v1.board.dto.BoardAttachDto;
import com.sparwk.adminnode.admin.biz.v1.board.dto.BoardQnaAnswerRequest;
import com.sparwk.adminnode.admin.biz.v1.board.dto.BoardQnaDto;
import com.sparwk.adminnode.admin.biz.v1.board.dto.BoardQnaQuestionRequest;
import com.sparwk.adminnode.admin.biz.v1.test.dto.AdminEmailSendRequest;
import com.sparwk.adminnode.admin.config.common.ActiveCodeConst;
import com.sparwk.adminnode.admin.config.common.GetAccountId;
import com.sparwk.adminnode.admin.config.common.Response;
import com.sparwk.adminnode.admin.config.common.ResultCodeConst;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.admin.AdminActiveLogEntity;
import com.sparwk.adminnode.admin.jpa.entity.board.BoardAttachEntity;
import com.sparwk.adminnode.admin.jpa.entity.board.BoardQnaEntity;
import com.sparwk.adminnode.admin.jpa.entity.board.QnaCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.commonCode.CommonDetailCodeEntity;
import com.sparwk.adminnode.admin.jpa.entity.profile.ProfilePositionEntity;
import com.sparwk.adminnode.admin.jpa.repository.account.AccountRepository;
import com.sparwk.adminnode.admin.jpa.repository.board.BoardAttachRepository;
import com.sparwk.adminnode.admin.jpa.repository.board.BoardQnaRepository;
import com.sparwk.adminnode.admin.jpa.repository.commonCode.CommonCodeRepository;
import com.sparwk.adminnode.admin.jpa.repository.commonCode.CommonDetailCodeRepository;
import com.sparwk.adminnode.admin.jpa.repository.profile.ProfilePositionRepository;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static java.time.LocalDateTime.now;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class BoardQnaService {

    private final CommonCodeRepository commonCodeRepository;
    private final CommonDetailCodeRepository commonDetailCodeRepository;
    private final BoardAttachRepository boardAttachRepository;
    private final BoardQnaRepository boardQnaRepository;
    private final ProfilePositionRepository profilePositionRepository;
    private final AccountRepository accountRepository;
    private final RestTemplate restTemplate;
    private final AdminActiveLogService adminActiveLogService;

    private final HttpServletRequest httpServletRequest;
    private final MessageSourceAccessor messageSourceAccessor;

    @Value("${mail.schema}")
    private String mailSchema;
    @Value("${mail.host}")
    private String mailHost;
    @Value("${mail.path}")
    private String mailPath;

    private final Logger logger = LoggerFactory.getLogger(BoardQnaService.class);



    public Response<CommonPagingListDto> getList(String type,
                                                 String val,
                                                 QnaCateEnum cate,
                                                 PageRequest pageRequest) {

        Response<CommonPagingListDto> res = new Response<>();

        //부모코드 없을 때, 실제 코드가 아니고 타입이라 조회는 생략함
        if (type == null) {
            res.setResultCd(ResultCodeConst.NOT_EXIST_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        List<BoardQnaDto> boardQnaList =
                boardQnaRepository.findQryQna(type, val, cate, pageRequest);

        logger.info("BoardQnaList - {}", boardQnaList);

        //for문 예제
        for(BoardQnaDto boardQna : boardQnaList){

            List<BoardAttachDto> boardAttachDtoQuestionList = new ArrayList<>();
            //첨부파일이 있으면 리스트를 불러온다
            if(boardQna.getQuestionAttachYn() == YnTypeEnum.Y){
                boardAttachDtoQuestionList = boardAttachRepository.findQryAttach(boardQna.getQnaId(),"Question");
                boardQna.setBoardAttachDtoQuestionList(boardAttachDtoQuestionList);
            }

            List<BoardAttachDto> boardAttachDtoAnswerList = new ArrayList<>();
            //첨부파일이 있으면 리스트를 불러온다
            if(boardQna.getAnswerAttachYn() == YnTypeEnum.Y){
                boardAttachDtoAnswerList = boardAttachRepository.findQryAttach(boardQna.getQnaId(),"Answer");
                boardQna.setBoardAttachDtoAnswerList(boardAttachDtoAnswerList);
            }

            List<ProfilePositionEntity> profilePositionList = profilePositionRepository.findByProfileId(boardQna.getRegUsr());
            boardQna.setProfilePositionList(profilePositionList);

        }


        //조회내용 없을 때
        if (boardQnaList == null || boardQnaList.size() < 1) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        CommonPagingListDto list = new CommonPagingListDto();

        //총 카운트
        long totalSize = boardQnaRepository.countQryAll(type, val, cate);
        long totalPage = totalSize / pageRequest.getPageSize();
        long mod = totalSize % pageRequest.getPageSize();
        if(mod > 0) {
            totalPage += 1;
        }

        list.setList(boardQnaList);
        list.setSize(pageRequest.getPageSize());
        list.setPage(pageRequest.getPageNumber());
        list.setTotalPage(totalPage);
        list.setTotalSize(totalSize);

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(list);
        return res;
    }

    public Response<Optional<BoardQnaDto>> getOne(Long id) {

        Response<Optional<BoardQnaDto>> res = new Response<>();

        //id값이 없을 때
        if (id == null) {
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<BoardQnaEntity> boardQna = boardQnaRepository.findById(id);

        logger.info("boardQna - {}", boardQna);

        //조회내용 없을 때
        if (!boardQna.isPresent()) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //내용 조회 후 카테고리를 검색
        Optional<CommonDetailCodeEntity> commonDetailCode = Optional.ofNullable(
                commonDetailCodeRepository.findByDcode(boardQna.get().getCateCd())
        );

        logger.info("commonDetailCode - {}", commonDetailCode);

        //부모코드 조회내용 없을 때
        if (!commonDetailCode.isPresent()) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        List<BoardAttachDto> boardAttachDtoQuestionList = new ArrayList<>();
        //첨부파일이 있으면 리스트를 불러온다
        if(boardQna.get().getQuestionAttachYn() == YnTypeEnum.Y){
            boardAttachDtoQuestionList = boardAttachRepository.findQryAttach(id,"Question");
        }

        List<BoardAttachDto> finalBoardAttachDtoQuestionList = boardAttachDtoQuestionList;

        List<BoardAttachDto> boardAttachDtoAnswerList = new ArrayList<>();
        //첨부파일이 있으면 리스트를 불러온다
        if(boardQna.get().getAnswerAttachYn() == YnTypeEnum.Y){
            boardAttachDtoAnswerList = boardAttachRepository.findQryAttach(id,"Answer");
        }

        List<BoardAttachDto> finalBoardAttachDtoAnswerList = boardAttachDtoAnswerList;
        Optional<BoardQnaDto> dtoPage =

                boardQna
                        .map(p -> BoardQnaDto.builder()
                                .qnaId(p.getQnaId())
                                .cateCd(p.getCateCd())
                                .cateCdName(commonDetailCode.get().getVal())
                                .reciveEmail(p.getReciveEmail())
                                .reciveEmailYn(p.getReciveEmailYn())
                                .title(p.getTitle())
                                .questionContent(p.getQuestionContent())
                                .questionAttachYn(p.getQuestionAttachYn())
                                .regUsr(p.getRegUsr())
                                .regDt(p.getRegDt())
                                .modUsr(p.getModUsr())
                                .modDt(p.getModDt())
                                .boardAttachDtoQuestionList(finalBoardAttachDtoQuestionList)
                                .answerYn(p.getAnswerYn())
                                .answerContent(p.getAnswerContent())
                                .answerAttachYn(p.getAnswerAttachYn())
                                .answerEmailYn(p.getAnswerEmailYn())
                                .answerRegUsr(p.getAnswerRegUsr())
                                .answerRegDt(p.getRegDt())
                                .answerModUsr(p.getAnswerRegUsr())
                                .answerModDt(p.getAnswerModDt())
                                .boardAttachDtoAnswerList(finalBoardAttachDtoAnswerList)
                                .build()
                        );

        logger.info("BoardQna - {}", dtoPage);

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(dtoPage);
        return res;
    }



    @Transactional
    public Response<Void> saveQuestionContent(@NotNull BoardQnaQuestionRequest request, HttpServletRequest req) {

        Response<Void> res = new Response<>();

        //카테고리코드 없을 때
        if (request.getBoardQnaQuestionDto().getCateCd() == null) {
            res.setResultCd(ResultCodeConst.NOT_VALID_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<CommonDetailCodeEntity> commonDetailCode =
                Optional.ofNullable(commonDetailCodeRepository.findByDcode(request.getBoardQnaQuestionDto().getCateCd()));

        logger.info("commonDetailCode - {}", commonDetailCode);

        //카테고리코드 조회내용 없을 때
        if (!commonDetailCode.isPresent()) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if (request.getBoardQnaQuestionDto().getTitle() == null) {
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }



        try {

            if(request.getBoardQnaQuestionDto().getBoardAttachDtoList() != null && request.getBoardQnaQuestionDto().getBoardAttachDtoList().size() > 0) {
                request.getBoardQnaQuestionDto().setQuestionAttachYn(YnTypeEnum.Y);
                //logger.info("====================== 11111111111111 {}", request.getBoardQnaQuestionDto().getBoardAttachDtoList());
            } else {
                request.getBoardQnaQuestionDto().setQuestionAttachYn(YnTypeEnum.N);
                //logger.info("====================== 222222222 {}", request.getBoardQnaQuestionDto().getBoardAttachDtoList());
            }



            BoardQnaEntity newBoardQna = BoardQnaEntity.builder()
                    .cateCd(request.getBoardQnaQuestionDto().getCateCd())
                    .title(request.getBoardQnaQuestionDto().getTitle())
                    .reciveEmail(request.getBoardQnaQuestionDto().getReciveEmail())
                    .reciveEmailYn(request.getBoardQnaQuestionDto().getReciveEmailYn())
                    .questionContent(request.getBoardQnaQuestionDto().getQuestionContent())
                    .questionAttachYn(request.getBoardQnaQuestionDto().getQuestionAttachYn())
                    .answerYn(YnTypeEnum.N)
                    .regUsr(request.getBoardQnaQuestionDto().getRegUsr())
                    .regDt(now())
                    .modUsr(request.getBoardQnaQuestionDto().getModUsr())
                    .modDt(now())
                    .build();

            BoardQnaEntity saved = boardQnaRepository.save(newBoardQna);

            Optional<BoardQnaEntity> BoardQna = boardQnaRepository.findById(saved.getQnaId());

            //첨부파일이 있으면 추가로 등록한다
            if(saved.getQuestionAttachYn() == YnTypeEnum.Y){

                for(int i=0; i < request.getBoardQnaQuestionDto().getBoardAttachDtoList().size(); i++){

                    BoardAttachEntity newAttach = BoardAttachEntity.builder()
                            .boardType("Question")
                            .boardId(saved.getQnaId())
                            .fileNum((long) (i+1))
                            .fileUrl(request.getBoardQnaQuestionDto().getBoardAttachDtoList().get(i).getFileUrl())
                            .fileName(request.getBoardQnaQuestionDto().getBoardAttachDtoList().get(i).getFileName())
                            .fileSize(request.getBoardQnaQuestionDto().getBoardAttachDtoList().get(i).getFileSize())
                            .build();

                    BoardAttachEntity saved2 = boardAttachRepository.save(newAttach);

                }

            }

            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
//            res.setResult(dtoPage);

        } catch (Exception e) {
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }

        return res;
    }

    @Transactional
    public Response<Void> saveAnswerContent(Long id, @NotNull BoardQnaAnswerRequest request, HttpServletRequest req) {

        Response<Void> res = new Response<>();

        //ID 값 없을 때
        if (id == null) {
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<BoardQnaEntity> boardQna = boardQnaRepository.findById(request.getQnaId());

        //조회내용 없을 때
        if (!boardQna.isPresent()) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if (request.getAnswerContent() == null) {
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        try {

            if (request.getBoardAttachDtoList() != null && request.getBoardAttachDtoList().size() > 0) {
                request.setAnswerAttachYn(YnTypeEnum.Y);
            } else {
                request.setAnswerAttachYn(YnTypeEnum.N);
            }

            boardQna.get().answerReg(YnTypeEnum.Y,
                    request.getAnswerContent(),
                    request.getAnswerAttachYn(),
                    YnTypeEnum.Y,
                    request.getAnswerRegUsr(),
                    now(),
                    request.getAnswerModUsr(),
                    now());


            //첨부파일이 있으면 추가로 등록한다
            if (request.getAnswerAttachYn() == YnTypeEnum.Y) {

                for (int i = 0; i < request.getBoardAttachDtoList().size(); i++) {

                    BoardAttachEntity newAttach = BoardAttachEntity.builder()
                            .boardType("Answer")
                            .boardId(request.getQnaId())
                            .fileNum((long) (i + 1))
                            .fileUrl(request.getBoardAttachDtoList().get(i).getFileUrl())
                            .fileName(request.getBoardAttachDtoList().get(i).getFileName())
                            .fileSize(request.getBoardAttachDtoList().get(i).getFileSize())
                            .build();

                    BoardAttachEntity saved2 = boardAttachRepository.save(newAttach);

                }

            }

            //메일 받는다고 체크하면 메일 보냄
            if(boardQna.get().getReciveEmailYn() == YnTypeEnum.Y){

                //메일 보내기 자료 모음
                Optional<CommonDetailCodeEntity> commonDetailCode = Optional.ofNullable(commonDetailCodeRepository.findByDcode(boardQna.get().getCateCd()));
                //카테고리코드 조회내용 없을 때
                if (!commonDetailCode.isPresent()) {
                    res.setResultCd(ResultCodeConst.NOT_FOUND_PCODE.getCode());
                    res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
                    return res;
                }

                Optional<AccountMemberMailDto> queryMember = accountRepository.findQryMemberMail(boardQna.get().getRegUsr());
                //카테고리코드 조회내용 없을 때
                if (!queryMember.isPresent()) {
                    //아무것도 안함
                } else {

                    //메일 보내기 셋팅
                    AdminEmailSendRequest adminEmailSendRequest = new AdminEmailSendRequest();

                    String[] reciverArr = {boardQna.get().getReciveEmail()};
                    adminEmailSendRequest.setReceiver(reciverArr);
                    adminEmailSendRequest.setProfileFullName(queryMember.get().getProfileFullName());
                    adminEmailSendRequest.setCurrentCityCountryCdName(queryMember.get().getCurrentCityCountryCdName());
                    adminEmailSendRequest.setCurrentCityNm(queryMember.get().getCurrentCityNm());
                    adminEmailSendRequest.setProfileImgUrl(queryMember.get().getProfileImgUrl());
                    adminEmailSendRequest.setCategory(commonDetailCode.get().getVal());
                    adminEmailSendRequest.setSubject(boardQna.get().getTitle());
                    adminEmailSendRequest.setQuestionContent(boardQna.get().getQuestionContent());
                    adminEmailSendRequest.setAnswerContent(request.getAnswerContent());

                    //답변 메일 보내기
                    this.EmailSend2(adminEmailSendRequest);

                    logger.info("email -------------------- ");
                }
            }

            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
//            res.setResult(dtoPage);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));

            String menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_INQUERY.getCode());
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_CREATE.getCode());

            //netive query
            String usrName = adminActiveLogService.findQryUsrName(boardQna.get().getAnswerRegUsr());

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(boardQna.get().getAnswerRegDt())
                    .adminId(boardQna.get().getAnswerRegUsr())
                    .menuName(menuName)
                    .activeType("C")
                    .contentsId(boardQna.get().getQnaId())
                    .activeMsg(usrName + " " + activeMsg + " '" + boardQna.get().getTitle() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e) {
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }

        return res;
    }

    @Transactional
    public Response<Optional<BoardQnaDto>> updateQuestionContent(Long id, @NotNull BoardQnaQuestionRequest request, HttpServletRequest req) {

        Response<Optional<BoardQnaDto>> res = new Response<>();

        //id값이 없을 때
        if (id == null) {
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<BoardQnaEntity> boardQna = boardQnaRepository.findById(id);


        logger.info("boardQna - {} ", boardQna);

        //조회내용 없을 때
        if (!boardQna.isPresent()) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //내용 조회 후 카테고리를 검색
        Optional<CommonDetailCodeEntity> commonDetailCode = Optional.ofNullable(
                commonDetailCodeRepository.findByDcode(boardQna.get().getCateCd())
        );

        //카테고리코드 조회내용 없을 때
        if (!commonDetailCode.isPresent()) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }



        //Value 값 없을 때
        if (request.getBoardQnaQuestionDto().getTitle() == null) {
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        try {
            logger.info("/////////////////////////////// boardQna - asdflkajsdflkja ");

            boardQna.get().questionUpdate(
                    request.getBoardQnaQuestionDto().getAnswerYn(),
                    request.getBoardQnaQuestionDto().getCateCd(),
                    request.getBoardQnaQuestionDto().getReciveEmail(),
                    request.getBoardQnaQuestionDto().getReciveEmailYn(),
                    request.getBoardQnaQuestionDto().getTitle(),
                    request.getBoardQnaQuestionDto().getQuestionContent(),
                    request.getBoardQnaQuestionDto().getQuestionAttachYn(),
                    request.getBoardQnaQuestionDto().getModUsr(),
                    request.getBoardQnaQuestionDto().getModDt()
            );

            logger.info("/////////////////////////////// boardQna - {} ", boardQna.toString());

            Optional<BoardQnaEntity> boardQnaUpdate = boardQnaRepository.findById(boardQna.get().getQnaId());

            List<BoardAttachDto> boardAttachDtoQuestionList = new ArrayList<>();
            //첨부파일이 있으면 리스트를 불러온다
            if(boardQna.get().getQuestionAttachYn() == YnTypeEnum.Y){
                boardAttachDtoQuestionList = boardAttachRepository.findQryAttach(id,"Question");
            }

            List<BoardAttachDto> finalBoardAttachDtoQuestionList = boardAttachDtoQuestionList;

            List<BoardAttachDto> boardAttachDtoAnswerList = new ArrayList<>();
            //첨부파일이 있으면 리스트를 불러온다
            if(boardQna.get().getAnswerAttachYn() == YnTypeEnum.Y){
                boardAttachDtoAnswerList = boardAttachRepository.findQryAttach(id,"Answer");
            }

            List<BoardAttachDto> finalBoardAttachDtoAnswerList = boardAttachDtoAnswerList;
            Optional<BoardQnaDto> dtoPage =

                    boardQnaUpdate
                            .map(p -> BoardQnaDto.builder()
                                    .qnaId(p.getQnaId())
                                    .cateCd(p.getCateCd())
                                    .cateCdName(commonDetailCode.get().getVal())
                                    .reciveEmail(p.getReciveEmail())
                                    .reciveEmailYn(p.getReciveEmailYn())
                                    .title(p.getTitle())
                                    .questionContent(p.getQuestionContent())
                                    .questionAttachYn(p.getQuestionAttachYn())
                                    .regUsr(p.getRegUsr())
                                    .regDt(p.getRegDt())
                                    .modUsr(p.getModUsr())
                                    .modDt(p.getModDt())
                                    .boardAttachDtoQuestionList(finalBoardAttachDtoQuestionList)
                                    .answerYn(p.getAnswerYn())
                                    .answerContent(p.getAnswerContent())
                                    .answerAttachYn(p.getAnswerAttachYn())
                                    .answerEmailYn(p.getAnswerEmailYn())
                                    .answerRegUsr(p.getAnswerRegUsr())
                                    .answerRegDt(p.getRegDt())
                                    .answerModUsr(p.getAnswerRegUsr())
                                    .answerModDt(p.getAnswerModDt())
                                    .boardAttachDtoAnswerList(finalBoardAttachDtoAnswerList)
                                    .build()
                            );

            logger.info("dtoPage - {} ", dtoPage);

            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            res.setResult(dtoPage);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));

            String menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_INQUERY.getCode());
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_MODIFY.getCode());

            //netive query
            String usrName = adminActiveLogService.findQryUsrName(boardQna.get().getAnswerModUsr());

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(boardQna.get().getAnswerModDt())
                    .adminId(boardQna.get().getAnswerModUsr())
                    .menuName(menuName)
                    .activeType("U")
                    .contentsId(boardQna.get().getQnaId())
                    .activeMsg(usrName + " " + activeMsg + " '" + boardQna.get().getTitle() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e) {
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }

        return res;
    }

    @Transactional
    public Response<Optional<BoardQnaDto>> updateAnswerContent(Long id, @NotNull BoardQnaAnswerRequest request, HttpServletRequest req) {

        Response<Optional<BoardQnaDto>> res = new Response<>();

        //id값이 없을 때
        if (id == null) {
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<BoardQnaEntity> boardQna = boardQnaRepository.findById(id);


        logger.info("boardQna - {} ", boardQna);

        //조회내용 없을 때
        if (!boardQna.isPresent()) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if (request.getAnswerContent() == null) {
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        try {
            logger.info("/////////////////////////////// boardQna - asdflkajsdflkja ");

            LocalDateTime now = LocalDateTime.now();

            boardQna.get().answerUpdate(
                    request.getAnswerYn(),
                    request.getAnswerContent(),
                    request.getAnswerEmailYn(),
                    request.getAnswerAttachYn(),
                    request.getAnswerModUsr(),
                    now
            );

            logger.info("/////////////////////////////// boardQna - {} ", boardQna.toString());

            Optional<BoardQnaEntity> boardQnaUpdate = boardQnaRepository.findById(boardQna.get().getQnaId());

            List<BoardAttachDto> boardAttachDtoQuestionList = new ArrayList<>();
            //첨부파일이 있으면 리스트를 불러온다
            if(boardQna.get().getQuestionAttachYn() == YnTypeEnum.Y){
                boardAttachDtoQuestionList = boardAttachRepository.findQryAttach(id,"Question");
            }

            List<BoardAttachDto> finalBoardAttachDtoQuestionList = boardAttachDtoQuestionList;

            List<BoardAttachDto> boardAttachDtoAnswerList = new ArrayList<>();
            //첨부파일이 있으면 리스트를 불러온다
            if(boardQna.get().getAnswerAttachYn() == YnTypeEnum.Y){
                boardAttachDtoAnswerList = boardAttachRepository.findQryAttach(id,"Answer");
            }

            //내용 조회 후 카테고리를 검색
            Optional<CommonDetailCodeEntity> commonDetailCode = Optional.ofNullable(
                    commonDetailCodeRepository.findByDcode(boardQnaUpdate.get().getCateCd())
            );

            //카테고리코드 조회내용 없을 때
            if (!commonDetailCode.isPresent()) {
                res.setResultCd(ResultCodeConst.NOT_FOUND_PCODE.getCode());
                res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
                return res;
            }

            List<BoardAttachDto> finalBoardAttachDtoAnswerList = boardAttachDtoAnswerList;
            Optional<BoardQnaDto> dtoPage =

                    boardQnaUpdate
                            .map(p -> BoardQnaDto.builder()
                                    .qnaId(p.getQnaId())
                                    .cateCd(p.getCateCd())
                                    .cateCdName(commonDetailCode.get().getVal())
                                    .reciveEmail(p.getReciveEmail())
                                    .reciveEmailYn(p.getReciveEmailYn())
                                    .title(p.getTitle())
                                    .questionContent(p.getQuestionContent())
                                    .questionAttachYn(p.getQuestionAttachYn())
                                    .regUsr(p.getRegUsr())
                                    .regDt(p.getRegDt())
                                    .modUsr(p.getModUsr())
                                    .modDt(p.getModDt())
                                    .boardAttachDtoQuestionList(finalBoardAttachDtoQuestionList)
                                    .answerYn(p.getAnswerYn())
                                    .answerContent(p.getAnswerContent())
                                    .answerAttachYn(p.getAnswerAttachYn())
                                    .answerEmailYn(p.getAnswerEmailYn())
                                    .answerRegUsr(p.getAnswerRegUsr())
                                    .answerRegDt(p.getRegDt())
                                    .answerModUsr(p.getAnswerRegUsr())
                                    .answerModDt(p.getAnswerModDt())
                                    .boardAttachDtoAnswerList(finalBoardAttachDtoAnswerList)
                                    .build()
                            );

            logger.info("dtoPage - {} ", dtoPage);

            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            res.setResult(dtoPage);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));

            String menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_INQUERY.getCode());
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_MODIFY.getCode());

            //netive query
            String usrName = adminActiveLogService.findQryUsrName(boardQna.get().getAnswerModUsr());

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(boardQna.get().getAnswerModDt())
                    .adminId(boardQna.get().getAnswerModUsr())
                    .menuName(menuName)
                    .activeType("U")
                    .contentsId(boardQna.get().getQnaId())
                    .activeMsg(usrName + " " + activeMsg + " '" + boardQna.get().getTitle() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e) {
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }

        return res;
    }

    @Transactional
    public Response<Void> deleteContent(Long id, HttpServletRequest req) {

        Response<Void> res = new Response<>();

        //id값이 없을 때
        if (id == null) {
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<BoardQnaEntity> boardQna = boardQnaRepository.findById(id);

        logger.info("boardQna - {} ", boardQna);

        //조회내용 없을 때
        if (!boardQna.isPresent()) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        try {
            boardQnaRepository.deleteById(id);
            boardAttachRepository.deleteByBoardId(id);
            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
//            res.setResult(dtoPage);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));
            Long adminId = getAccountId.ofId(httpServletRequest);

            String menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_TERMS_POLICIES.getCode());
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_DELETE.getCode());

            //netive query
            String usrName = adminActiveLogService.findQryUsrName(adminId);

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(LocalDateTime.now())
                    .adminId(adminId)
                    .menuName(menuName)
                    .activeType("D")
                    .contentsId(boardQna.get().getQnaId())
                    .activeMsg(usrName + " " + activeMsg + " '" + boardQna.get().getTitle() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e) {
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }
        return res;

    }

    public void EmailSend2(AdminEmailSendRequest entity) {

        HttpHeaders headers = new HttpHeaders();
        headers.set("Auth","test");
        UriComponentsBuilder builder =
                UriComponentsBuilder
                        .newInstance()
                        .scheme(mailSchema)// http & https
                        .host(mailHost) //localhost:8080
                        .path(mailPath); //V1/profile/info
        UriComponents uriComponents = builder.build();

        if(entity.getProfileImgUrl() == null || entity.getProfileImgUrl().equals("") || entity.getProfileImgUrl().isEmpty()){
            entity.setProfileImgUrl("https://sparwk-profile.s3.ap-northeast-2.amazonaws.com/trans_sparwk80.png");
        }

        String html = "<html>";
        html += "";
        html += "<head>";
        html += "    <title>SPARWK-Email</title>";
        html += "    <meta http-equiv='Content-Type' content='text/html; charset=utf-8'>";
        html += "</head>";
        html += "";
        html += "<body style='margin:0;padding: 0;'>";
        html += "";
        html += "    <div";
        html += "        style='width:900px; margin: 0 auto; box-sizing: border-box; font-family:\"Malgun Gothic\",\"맑은 고딕\", sans-serif; letter-spacing:0px;'>";
        html += "";
        html += "        <div style='width:100%;'>";
        html += "            <div style='width:100%; position: relative; text-align: center; background:#182566; margin:0; box-sizing: border-box; padding: 30px;'>";
        html += "            </div>";
        html += "";
        html += "            <div style='width:100%; box-sizing: border-box; background:#0c1333;'>";
        html += "                <div style='font-size:15px; color: #fff; text-align: center; background-color: #0c1333; padding:45px 25px 50px;'>";
        html += "";
        html += "                    <div style='margin:0px; margin-bottom: 40px;'>";
        html += "                        <img src='https://sparwk-profile.s3.ap-northeast-2.amazonaws.com/logo_big.png' alt='logo' />";
        html += "                        <p style='margin: 0px; margin-top: 15px; margin-bottom: 10px; letter-spacing: -1px; font-size: 22px;'>";
        html += "                            Hi. "+entity.getProfileFullName()+" will answer the questions you asked in 1:1 inquiry.";
        html += "                        </p>";
        html += "";
        html += "                        <div style='width: 700px; margin:35px auto 0px;'>";
        html += "                            <div style='text-align: left; border-top: 1px solid #555; padding: 30px 0px;'>";
        html += "                                <div style='float: left; width: 140px; padding-top: 5px; text-align: center;'>";
        html += "                                    <div style='width: 80px; height: 80px; margin: 0px auto; background: #0c1333; border: 3px solid #778eff; border-radius: 100%; text-align: center; overflow: hidden; position: relative;'>";
        html += "                                        <img style='width: 100%;' src='"+entity.getProfileImgUrl()+"' alt='photo'>";
        html += "                                    </div>";
        html += "                                    <h5 style='font-size: 16px; font-weight: bold; color: #778eff; letter-spacing: 0px; margin: 7px 0px 3px;'>";
        html += "                                        "+entity.getProfileFullName();
        html += "                                    </h5>";
        html += "                                    <p style='margin: 0px; font-size: 11px; color: #fff;'>";
        html += "                                        "+entity.getCurrentCityNm() + ", " + entity.getCurrentCityCountryCdName();
        html += "                                    </p>";
        html += "                                </div>";
        html += "                                <div style='margin-left: 155px;'>";
        html += "                                    <p style='font-size: 16px; letter-spacing: -0.5px; margin:0px 0px 10px;'>";
        html += "                                        <span style='background-color: #ff994a; padding: 2px 10px 3px;'>"+entity.getCategory()+"</span>";
        html += "                                        <span style='font-size: 17px; font-weight: bold; padding: 3px 0px 2px 5px;'>";
        html += "						                        "+entity.getSubject();
        html += "                                    </p>";
        html += "                                    <p style='font-size: 12px; letter-spacing: 0px; margin:0px 0px 10px; min-height:100px;'>";
        html += "                                        " + entity.getQuestionContent();
        html += "                                    </p>";
        html += "                                </div>";
        html += "                            </div>";
        html += "";
        html += "                            <div style='text-align: left; border-top: 1px solid #555; padding: 30px 0px;'>";
        html += "                                <div style='float: left; width: 140px; padding-top: 15px; text-align: center;'>";
        html += "                                    <div style='width: 100px; margin: 0px auto;'>";
        html += "                                        <img style='width: 100%;' src='https://sparwk-profile.s3.ap-northeast-2.amazonaws.com/logo_mini.png' alt='logo'>";
        html += "                                    </div>";
        html += "                                    <h5 style='font-size: 16px; font-weight: bold; color: #778eff; letter-spacing: 0px; margin: 7px 0px 3px;'>";
        html += "                                        CS Team";
        html += "                                    </h5>";
        html += "                                </div>";
        html += "                                <div style='margin-left: 155px;'>";
        html += "                                    <p style='font-size: 12px; letter-spacing: 0px; margin:0px 0px 10px; color: #ff994a;'>";
        html += "                                        " + entity.getAnswerContent();
        html += "                                    </p>";
        html += "                                </div>";
        html += "                            </div>";
        html += "                        </div>";
        html += "";
        html += "                    </div>";
        html += "";
        html += "                </div>";
        html += "            </div>";
        html += "";
        html += "        </div>";
        html += "";
        html += "    </div>";
        html += "";
        html += "</body>";
        html += "";
        html += "</html>";


        entity.setContents(html);

        logger.info("URI COMPONENT tostring - {}", uriComponents.toString());
        logger.info(" - - - - - - 1 : {}", entity);
        HttpEntity<AdminEmailSendRequest> responsEntity = new HttpEntity<>(entity, headers);
        logger.info(" - - - - - - 2 : {}", responsEntity.toString());
        logger.info(" - - - - - - 3 : {}", uriComponents.toUriString());
//        ResponseEntity<String> resultResponse =
//                restTemplate.postForEntity(uriComponents.toUriString(), responsEntity, String.class);
        ResponseEntity<String> resultResponse =
                restTemplate.postForEntity(uriComponents.toUriString(), responsEntity, String.class);
        logger.info("asdasdasd - 4 : {}", resultResponse.toString());
//        JSONObject obj = new JSONObject(resultResponse.getBody());
//        logger.info("object - 5 : {}", obj.toString());
        Gson gson = new Gson();
//        Response response = gson.fromJson(String.valueOf(obj), Response.class);
//        logger.info("--------Respoonse {}", response.getResultCd());
    }


}
