package com.sparwk.adminnode.admin.biz.v1.currency.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sparwk.adminnode.admin.jpa.entity.*;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Arrays;
import java.util.List;

@Data
@NoArgsConstructor
@ExcelFileName
public class CurrencyExcelDto implements ExcelDto {
    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    @ExcelColumnName(headerName = "currency Detail Code Seq")
    private Long currencySeq;

    @Schema(description = "화폐명", nullable = false, example = "United States dollar")
    @ExcelColumnName
    @JsonProperty("currency")
    private String currency;

    @Schema(description = "코드", nullable = false, example = "CUR000001")
    @ExcelColumnName
    @JsonProperty("currencyCd")
    private String currencyCd;

    @Schema(description = "화폐코드", nullable = false, example = "USD")
    @ExcelColumnName
    @JsonProperty("iso_code")
    private String isoCode;

    @Schema(description = "화폐표식", nullable = false, example = "$")
    @ExcelColumnName
    @JsonProperty("symbol")
    private String symbol;

    @Schema(description = "국가", nullable = false, example = "United States")
    @ExcelColumnName
    @JsonProperty("country")
    private String country;

    @Schema(description = "화폐 사용여부 Y/N", nullable = false, defaultValue = "Y", allowableValues = {"Y", "N"})
    @ExcelColumnName
    @JsonProperty("useType")
    private YnTypeEnum useType;

    @Override
    public List<String> mapToList() {
        return Arrays.asList(String.valueOf(currencySeq), currency, isoCode, symbol, country, String.valueOf(useType));
    }

}
