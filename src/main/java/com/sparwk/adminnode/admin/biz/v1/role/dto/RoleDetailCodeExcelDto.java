package com.sparwk.adminnode.admin.biz.v1.role.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sparwk.adminnode.admin.jpa.entity.*;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Arrays;
import java.util.List;

@Data
@NoArgsConstructor
@ExcelFileName(filename = "Role")
public class RoleDetailCodeExcelDto implements ExcelDto {

    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    @ExcelColumnName(headerName = "Role Detail Code Seq")
    private Long roleDetailCodeSeq;
    @Schema(description = "부모코드 3자리", nullable = false, example = "DEX")
    @ExcelColumnName
    @JsonProperty("pcode")
    private String pcode;
    @Schema(description = "부모코드 값", nullable = true, example = "DDEX")
    @ExcelColumnName
    @JsonProperty("pcodeVal")
    private String pcodeVal;
    @Schema(description = "코드 9자리", nullable = false, example = "DEX000001")
    @ExcelColumnName
    @JsonProperty("dcode")
    private String dcode;
    @Schema(description = "코드 값", nullable = false, example = "Artist")
    @ExcelColumnName
    @JsonProperty("role")
    private String role;
    @Schema(description = "포맷 여부 DDEX / SPARWK", defaultValue = "DDEX", allowableValues = {"DDEX", "SPARWK"})
    @ExcelColumnName
    @JsonProperty("format")
    private FormatTypeEnum formatType;
    @Schema(description = "코드 사용여부 Y/N", defaultValue = "Y", allowableValues = {"Y", "N"})
    @ExcelColumnName
    @JsonProperty("useType")
    private YnTypeEnum useType;
    @Schema(description = "popular 사용 Y/N", defaultValue = "Y", allowableValues = {"Y", "N"})
    @ExcelColumnName
    @JsonProperty("popularType")
    private YnTypeEnum popularType;
    @Schema(description = "Matching Role 사용 Y/N", defaultValue = "Y", allowableValues = {"Y", "N"})
    @ExcelColumnName
    @JsonProperty("matchingRoleYn")
    private YnTypeEnum matchingRoleYn;
    @Schema(description = "Credit Role 사용 Y/N", defaultValue = "Y", allowableValues = {"Y", "N"})
    @ExcelColumnName
    @JsonProperty("creditRoleYn")
    private YnTypeEnum creditRoleYn;
    @Schema(description = "Split Sheet Role 사용 Y/N", defaultValue = "Y", allowableValues = {"Y", "N"})
    @ExcelColumnName
    @JsonProperty("splitSheetRoleYn")
    private YnTypeEnum splitSheetRoleYn;
    @Schema(description = "약어 값", nullable = true, example = "약어")
    @ExcelColumnName
    @JsonProperty("abbeviation")
    private String abbeviation;
    @Schema(description = "자체사용코드", nullable = true, example = "")
    @ExcelColumnName
    @JsonProperty("customCode")
    private String customCode;
    @Schema(description = "description 설명", nullable = true, example = "설명")
    @ExcelColumnName
    @JsonProperty("description")
    private String description;
    @Schema(description = "코드 순번 숫자",  nullable = false, example = "1")
    @ExcelColumnName
    @JsonProperty("hit")
    private Long hit;

    @Override
    public List<String> mapToList() {
        return Arrays.asList(
                String.valueOf(roleDetailCodeSeq),
                pcode,
                pcodeVal,
                dcode,
                role,
                String.valueOf(formatType),
                String.valueOf(useType),
                String.valueOf(popularType),
                String.valueOf(matchingRoleYn),
                String.valueOf(creditRoleYn),
                String.valueOf(creditRoleYn),
                abbeviation,
                customCode,
                description,
                String.valueOf(hit)
        );
    }
}
