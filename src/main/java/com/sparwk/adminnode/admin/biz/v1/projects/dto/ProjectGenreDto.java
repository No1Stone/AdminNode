package com.sparwk.adminnode.admin.biz.v1.projects.dto;

import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class ProjectGenreDto {

    @Schema(description = "project id 번호", nullable = false, example = "1")
    private Long projId;
    @Schema(description = "PROFILE ID", nullable = false, example = "1")
    private long profileId;
    @Schema(name = "FULL_NAME", nullable = false, example = "Mr.Yoon")
    private String fullName;
    @Schema(name = "COMPANY_NAME", nullable = false, example = "")
    private String companyName;
    @Schema(name = "PHONE NUMBER", nullable = true, example = "0821011112222")
    private String phoneNumber;
    @Schema(name = "ACCNT EMAIL", nullable = true, example = "sparwk@sparwk.com")
    private String accntEmail;
    @Schema(name = "PARTICIPATION SONG", nullable = false, example = "8")
    private int participationSong;
    @Schema(description = "활동여부", nullable = false, defaultValue = "Y", allowableValues = {"Y", "N"})
    private YnTypeEnum activeYn;



    @Builder
    public ProjectGenreDto(Long projId,
                           long profileId,
                           String fullName,
                           String companyName,
                           String phoneNumber,
                           String accntEmail,
                           int participationSong,
                           YnTypeEnum activeYn
    ) {

        this.projId = projId;
        this.profileId = profileId;
        this.fullName = fullName;
        this.companyName = companyName;
        this.phoneNumber = phoneNumber;
        this.accntEmail = accntEmail;
        this.participationSong = participationSong;
        this.activeYn = activeYn;
    }
}
