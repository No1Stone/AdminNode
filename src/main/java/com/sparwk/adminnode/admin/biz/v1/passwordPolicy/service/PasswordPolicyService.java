package com.sparwk.adminnode.admin.biz.v1.passwordPolicy.service;

import com.sparwk.adminnode.admin.biz.v1.passwordPolicy.dto.PasswordPolicyDto;
import com.sparwk.adminnode.admin.biz.v1.passwordPolicy.dto.PasswordPolicyRequest;
import com.sparwk.adminnode.admin.config.common.Response;
import com.sparwk.adminnode.admin.config.common.ResultCodeConst;
import com.sparwk.adminnode.admin.jpa.entity.passwordPolicy.PasswordPolicyEntity;
import com.sparwk.adminnode.admin.jpa.repository.passwordPolicy.PasswordPolicyRepository;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class PasswordPolicyService {

    private final PasswordPolicyRepository passwordPolicyRepository;

    private final MessageSourceAccessor messageSourceAccessor;

    private final Logger logger = LoggerFactory.getLogger(PasswordPolicyService.class);


    public Response<Optional<PasswordPolicyDto>> getOne(Long id) {

        Response<Optional<PasswordPolicyDto>> res = new Response<>();

        //id값이 없을 때
        if(id == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<PasswordPolicyEntity> passwordPolicy = passwordPolicyRepository.findById(id);

        logger.info("passwordPolicy - {}", passwordPolicy);

        //조회내용 없을 때
        if(!passwordPolicy.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<PasswordPolicyDto> dtoPage =

                passwordPolicy.map(p -> PasswordPolicyDto.builder()
                        .passwordPolicySeq(p.getPasswordPolicySeq())
                        .passwordExpire(p.getPasswordExpire())
                        .passwordHistory(p.getPasswordHistory())
                        .passwordMinimumLength(p.getPasswordMiNinimumLength())
                        .passwordComplexityRequirement(p.getPasswordComplexityRequirement())
                        .maximumInvalidLogin(p.getMaximumInvalidLogin())
                        .lockoutEffectivePeriod(p.getLockoutEffectivePeriod())
                        .modUsr(p.getModUsr())
                        .modDt(p.getModDt())
                        .build()
                );

        logger.info("Sns - {}", dtoPage);

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(dtoPage);
        return res;
    }

    @Transactional
    public Response<Optional<PasswordPolicyDto>> updateContent(Long id, PasswordPolicyRequest request, HttpServletRequest req) {

        Response<Optional<PasswordPolicyDto>> res = new Response<>();

        //id값이 없을 때
        if(id == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<PasswordPolicyEntity> passwordPolicyUpdate = passwordPolicyRepository.findById(id);

        //조회내용 없을 때
        if(!passwordPolicyUpdate.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if(request.getPasswordPolicyDto().getPasswordExpire() < 0){
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if(request.getPasswordPolicyDto().getPasswordHistory() < 0){
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if(request.getPasswordPolicyDto().getPasswordMinimumLength() < 0){
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if(request.getPasswordPolicyDto().getPasswordComplexityRequirement() == null){
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if(request.getPasswordPolicyDto().getMaximumInvalidLogin() == null){
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if(request.getPasswordPolicyDto().getLockoutEffectivePeriod() == null){
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        try {

            passwordPolicyUpdate.get()
                    .update(request.getPasswordPolicyDto().getPasswordExpire(),
                            request.getPasswordPolicyDto().getPasswordHistory(),
                            request.getPasswordPolicyDto().getPasswordMinimumLength(),
                            request.getPasswordPolicyDto().getPasswordComplexityRequirement(),
                            request.getPasswordPolicyDto().getMaximumInvalidLogin(),
                            request.getPasswordPolicyDto().getLockoutEffectivePeriod()
                    );

            Optional<PasswordPolicyEntity> passwordPolicy = passwordPolicyRepository.findById(passwordPolicyUpdate.get().getPasswordPolicySeq());

            Optional<PasswordPolicyDto> dtoPage =

                    passwordPolicy.map(p -> PasswordPolicyDto.builder()
                            .passwordPolicySeq(p.getPasswordPolicySeq())
                            .passwordExpire(p.getPasswordExpire())
                            .passwordHistory(p.getPasswordHistory())
                            .passwordMinimumLength(p.getPasswordMiNinimumLength())
                            .passwordComplexityRequirement(p.getPasswordComplexityRequirement())
                            .maximumInvalidLogin(p.getMaximumInvalidLogin())
                            .lockoutEffectivePeriod(p.getLockoutEffectivePeriod())
                            .modUsr(p.getModUsr())
                            .modDt(p.getModDt())
                            .build()
                    );

            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            res.setResult(dtoPage);

        } catch (Exception e){
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }

        return res;
    }

}
