package com.sparwk.adminnode.admin.biz.v1.projects.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class ProjectServiceCountryDto {

    @Schema(description = "project id 번호", nullable = false, example = "1")
    private Long projId;
    @Schema(description = "자식코드", nullable = false, example = "CNT000001")
    private String serviceCntrCd;
    @Schema(description = "자식코드값", nullable = false, example = "USA")
    private String serviceCntrCdVal;


    @Builder
    public ProjectServiceCountryDto(Long projId,
                                    String serviceCntrCd,
                                    String attrDtlCd,
                                    String attrDtlCdVal
    ) {
        this.projId = projId;
        this.serviceCntrCd = serviceCntrCd;
        this.serviceCntrCdVal = serviceCntrCdVal;
    }
}
