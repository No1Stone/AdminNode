package com.sparwk.adminnode.admin.biz.v1.inquary.service;

import com.sparwk.adminnode.admin.biz.v1.CommonPagingListDto;
import com.sparwk.adminnode.admin.biz.v1.inquary.dto.InquaryDto;
import com.sparwk.adminnode.admin.config.common.Response;
import com.sparwk.adminnode.admin.config.common.ResultCodeConst;
import com.sparwk.adminnode.admin.jpa.entity.inquary.InquaryCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.inquary.InquarySorterEnum;
import com.sparwk.adminnode.admin.jpa.repository.inquary.InquaryRepository;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class InquaryService {

    private final InquaryRepository inquaryRepository;

    private final MessageSourceAccessor messageSourceAccessor;

    private final Logger logger = LoggerFactory.getLogger(InquaryService.class);


    public Response<CommonPagingListDto> getList(InquaryCateEnum cate,
                                                  String val,
                                                  InquarySorterEnum sorter,
                                                  PageRequest pageRequest) {

        Response<CommonPagingListDto> res = new Response<>();

        List<InquaryDto> inquaryList = inquaryRepository.findQryAll(cate, val, sorter, pageRequest);

        logger.info("inquaryList - {}", inquaryList);

        //조회내용 없을 때
        if(inquaryList == null || inquaryList.size() < 1){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        CommonPagingListDto list = new CommonPagingListDto();

        //총 카운트
        long totalSize = inquaryRepository.countQryAll(cate, val, sorter);
        long totalPage = totalSize / pageRequest.getPageSize();
        long mod = totalSize % pageRequest.getPageSize();
        if(mod > 0) {
            totalPage += 1;
        }

        list.setList(inquaryList);
        list.setSize(pageRequest.getPageSize());
        list.setPage(pageRequest.getPageNumber());
        list.setTotalPage(totalPage);
        list.setTotalSize(totalSize);

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(list);
        return res;
    }

}
