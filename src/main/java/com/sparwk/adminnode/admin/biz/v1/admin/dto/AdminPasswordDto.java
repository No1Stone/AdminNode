package com.sparwk.adminnode.admin.biz.v1.admin.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Setter
@Getter
@NoArgsConstructor
public class AdminPasswordDto {

    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long adminId;
    @Schema(description = "password", nullable = false, example = "password")
    private String adminPassword;
    @Schema(description = "preview password", nullable = false, example = "이전 비밀번호")
    private String adminPrevPassword;
    @Schema(description = "수정일", pattern = "yyMMdd hh:mm:ss", example = "2021-12-08T05:44:26.627991")
    private LocalDateTime lastConnect;

    @Builder
    public AdminPasswordDto(Long adminId,
                            String adminPassword,
                            String adminPrevPassword,
                            LocalDateTime lastConnect
                    ) {
        this.adminId = adminId;
        this.adminPassword = adminPassword;
        this.adminPrevPassword = adminPrevPassword;
        this.lastConnect = lastConnect;
    }
}