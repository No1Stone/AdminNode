package com.sparwk.adminnode.admin.biz.v1.song.dto;

import com.sparwk.adminnode.admin.jpa.entity.song.SongMetaCustomEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

@Setter
@Getter
@NoArgsConstructor
public class SongViewListDto {

    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long songId;
    @Schema(description = "SONG TITLE", nullable = false, example = "SONG_TITLE")
    private String songTitle;
    @Schema(description = "AVATAR_FILE_URL", nullable = false, example = "http://")
    private String avatarFileUrl;
    @Schema(description = "SONG_FILE_PATH", nullable = false, example = "http://")
    private String songFilePath;
    @Schema(description = "SONG OWNER", nullable = true, example = "1")
    private Long songOwner;
    @Schema(description = "SONG OWNER NAME", nullable = true, example = "OWNER PROFILE NAME")
    private String songOwnerName;
    @Schema(description = "SONG COWRITERS NAME", nullable = true, example = "COWRITERS PROFILE NAME")
    private String songCowritersName;
    @Schema(description = "PROJECT TITLE", nullable = false, example = "PROJ_TITLE")
    private String projTitle;
    @Schema(description = "SONG GENRE TYPE", nullable = true, example = "SGG000008,SGG000009")
    private String songGenreCd;
    @Schema(description = "SONG GENRE TYPE Name", nullable = true, example = "HipHop,Jazz")
    private String songGenreName;
    @Schema(description = "SONG SUB GENRE TYPE", nullable = true, example = "SGS000001")
    private String songSubgenreCd;
    @Schema(description = "SONG SUB GENRE TYPE Name", nullable = true, example = "K-POP")
    private String songSubgenreName;
    @Schema(description = "SONG VERSION CODE", nullable = true, example = "SGV000001")
    private String songVersionCd;
    @Schema(description = "SONG VERSION CODE Name", nullable = true, example = "OrigialVersion")
    private String songVersionCdName;
    @Schema(description = "SONG Status CODE", nullable = true, example = "SST000006")
    private String songStatusCd;
    @Schema(description = "SONG Status CODE Name", nullable = true, example = "Released")
    private String songStatusCdName;
    @Schema(description = "SONG Language CODE", nullable = true, example = "LAN000006")
    private String songLangCd;
    @Schema(description = "SONG Language CODE Name", nullable = true, example = "KOREA")
    private String songLangCdName;
    @Schema(description = "SONG Mood CODE", nullable = true, example = "SGM000006")
    private String songMoodCd;
    @Schema(description = "SONG Mood CODE Name", nullable = true, example = "Disgust")
    private String songMoodCdName;
    @Schema(description = "SONG Mood CODE", nullable = true, example = "SGT000006")
    private String songThemeCd;
    @Schema(description = "SONG Mood CODE Name", nullable = true, example = "Actor")
    private String songThemeCdName;
    @Schema(description = "SONG PLAY TIME", nullable = true, example = "4:00")
    private String songPlayTime;
    @Schema(description = "SONG PLAY BPM", nullable = true, example = "120")
    private long songPlayBpm;
    @Schema(description = "SONG TEMPO", nullable = true, example = "High")
    private String songTempo;
    @Schema(description = "SONG ISRC", nullable = true, example = "ISRC")
    private String songISRC;
    @Schema(description = "SONG KEY SIGNATURE", nullable = true, example = "key Signature")
    private String songKeySignature;
    @Schema(description = "SONG TIME SIGNATURE", nullable = true, example = "Time Signature")
    private String songTimeSignature;
    @Schema(description = "LYRICS", nullable = true, example = "가사")
    private String lyrics;
    @Schema(description = "LYRICS COMMENT", nullable = true, example = "가사 코멘트")
    private String lyricsComt;
    @Schema(description = "Writer", nullable = true, example = "1L")
    private Long regUsr;
    @Schema(description = "Writer NAME", nullable = true, example = "Yoon")
    private String regUsrName;
    @Schema(description = "등록일", pattern = "yyMMdd hh:mm:ss", example = "2021-12-08T05:44:26.627991")
    private LocalDateTime regDt;
    @Schema(description = "Modify", nullable = true, example = "1L")
    private Long modUsr;
    @Schema(description = "Modify NAME", nullable = true, example = "Yoon")
    private String modUsrName;
    @Schema(description = "등록일", pattern = "yyMMdd hh:mm:ss", example = "2021-12-08T05:44:26.627991")
    private LocalDateTime modDt;

    @Schema(description = "custom meta, voice family")
    private List<SongMetaCustomEntity> songMetadataCustomList;

    @Builder
    public SongViewListDto(Long songId,
                           String songTitle,
                           String avatarFileUrl,
                           String songFilePath,
                           Long songOwner,
                           String songOwnerName,
                           String songCowritersName,
                           String projTitle,
                           String songGenreCd,
                           String songGenreName,
                           String songSubgenreCd,
                           String songSubgenreName,
                           String songVersionCd,
                           String songVersionCdName,
                           String songStatusCd,
                           String songStatusCdName,
                           String songLangCd,
                           String songLangCdName,
                           String songMoodCd,
                           String songMoodCdName,
                           String songThemeCd,
                           String songThemeCdName,
                           String songPlayTime,
                           long songPlayBpm,
                           String songTempo,
                           String songISRC,
                           String songKeySignature,
                           String songTimeSignature,
                           String lyrics,
                           String lyricsComt,
                           Long regUsr,
                           String regUsrName,
                           LocalDateTime regDt,
                           Long modUsr,
                           String modUsrName,
                           LocalDateTime modDt,
                           List<SongMetaCustomEntity> songMetadataCustomList
    ) {
        this.songId = songId;
        this.songTitle = songTitle;
        this.avatarFileUrl = avatarFileUrl;
        this.songFilePath = songFilePath;
        this.songOwner = songOwner;
        this.songOwnerName = songOwnerName;
        this.songCowritersName = songCowritersName;
        this.projTitle = projTitle;
        this.songGenreCd = songGenreCd;
        this.songGenreName = songGenreName;
        this.songSubgenreCd = songSubgenreCd;
        this.songSubgenreName = songSubgenreName;
        this.songVersionCd = songVersionCd;
        this.songVersionCdName = songVersionCdName;
        this.songStatusCd = songStatusCd;
        this.songStatusCdName = songStatusCdName;
        this.songLangCd = songLangCd;
        this.songLangCdName = songLangCdName;
        this.songMoodCd = songMoodCd;
        this.songMoodCdName = songMoodCdName;
        this.songThemeCd = songThemeCd;
        this.songThemeCdName = songThemeCdName;
        this.songPlayTime = songPlayTime;
        this.songPlayBpm = songPlayBpm;
        this.songTempo = songTempo;
        this.songISRC = songISRC;
        this.songKeySignature = songKeySignature;
        this.songTimeSignature = songTimeSignature;
        this.lyrics = lyrics;
        this.lyricsComt = lyricsComt;
        this.regUsr = regUsr;
        this.regUsrName = regUsrName;
        this.regDt = regDt;
        this.modUsr = modUsr;
        this.modUsrName = modUsrName;
        this.modDt = modDt;
        this.songMetadataCustomList = songMetadataCustomList;
    }
}
