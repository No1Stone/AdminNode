package com.sparwk.adminnode.admin.biz.v1.board.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Setter
@Getter
@NoArgsConstructor
public class BoardAttachDto {

    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long attachId;
    @Schema(description = "Board 타입", nullable = true, example = "FAQ / Question / Answer / News")
    private String boardType;
    @Schema(description = "각 게시판 id값 공통 Seq 사용", nullable = true, example = "1")
    private Long boardId;
    @Schema(description = "파일 순번", nullable = true, example = "1")
    private Long fileNum;
    @Schema(description = "첨부파일 URL", nullable = false, example = "http://...")
    private String fileUrl;
    @Schema(description = "파일명", nullable = false, example = "fileName.txt")
    private String fileName;
    @Schema(description = "file Size", nullable = true, example = "100000")
    private Long fileSize;
    @Schema(description = "등록일", pattern = "yyMMdd hh:mm:ss", example = "2021-12-08T05:44:26.627991")
    private LocalDateTime regDt;
    @Schema(description = "등록자", nullable = false, example = "10000001")
    private Long regUsr;
    @Schema(description = "수정일", pattern = "yyMMdd hh:mm:ss", example = "2021-12-08T05:44:26.627991")
    private LocalDateTime modDt;
    @Schema(description = "수정자", nullable = false, example = "10000001")
    private Long modUsr;

    @Builder
    public BoardAttachDto(Long attachId,
                          String boardType,
                          Long boardId,
                          Long fileNum,
                          String fileUrl,
                          String fileName,
                          Long fileSize,
                          LocalDateTime regDt,
                          Long regUsr,
                          LocalDateTime modDt,
                          Long modUsr
    ) {
        this.attachId=attachId;
        this.boardType=boardType;
        this.boardId=boardId;
        this.fileNum=fileNum;
        this.fileUrl=fileUrl;
        this.fileName=fileName;
        this.fileSize=fileSize;
        this.regDt=regDt;
        this.regUsr=regUsr;
        this.modDt=modDt;
        this.modUsr=modUsr;
    }


}