package com.sparwk.adminnode.admin.biz.v1.board.dto;

import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

@Setter
@Getter
@NoArgsConstructor
public class BoardQnaQuestionDto {

    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long qnaId;
    @Schema(description = "카테고리 코드 9자리", nullable = false, example = "HSC000001")
    private String cateCd;
    @Schema(description = "카테고리 코드 9자리", nullable = false, example = "Account")
    private String cateCdName;
    @Schema(description = "답변 받을 이메일 주소", nullable = true, example = "yoon@naver.com")
    private String reciveEmail;
    @Schema(description = "답변 사용여부 Y/N", nullable = false, defaultValue = "N", allowableValues = {"Y", "N"})
    private YnTypeEnum reciveEmailYn;
    @Schema(description = "제목", nullable = false, example = "News & Post 제목입니다.")
    private String title;
    @Schema(description = "질문내용", nullable = true, example = "질문내용")
    private String questionContent;
    @Schema(description = "첨부파일 사용여부 Y/N", nullable = false, defaultValue = "N", allowableValues = {"Y", "N"})
    private YnTypeEnum questionAttachYn;
    @Schema(description = "답변 사용여부 Y/N", nullable = false, defaultValue = "N", allowableValues = {"Y", "N"})
    private YnTypeEnum answerYn;
    @Schema(description = "등록일", pattern = "yyMMdd hh:mm:ss", example = "2021-12-08T05:44:26.627991")
    private LocalDateTime regDt;
    @Schema(description = "등록자", nullable = false, example = "10000001")
    private Long regUsr;
    @Schema(description = "수정일", pattern = "yyMMdd hh:mm:ss", example = "2021-12-08T05:44:26.627991")
    private LocalDateTime modDt;
    @Schema(description = "수정자", nullable = false, example = "10000001")
    private Long modUsr;
    @Schema(description = "첨부파일", nullable = true)
    private List<BoardAttachDto> boardAttachDtoList;


    @Builder
    public BoardQnaQuestionDto(Long qnaId,
                               String cateCd,
                               String cateCdName,
                               String reciveEmail,
                               YnTypeEnum reciveEmailYn,
                               String title,
                               String questionContent,
                               YnTypeEnum questionAttachYn,
                               YnTypeEnum answerYn,
                               LocalDateTime regDt,
                               Long regUsr,
                               LocalDateTime modDt,
                               Long modUsr,
                               List<BoardAttachDto> boardAttachDtoList
    ) {
        this.qnaId = qnaId;
        this.cateCd = cateCd;
        this.cateCdName = cateCdName;
        this.reciveEmail = reciveEmail;
        this.reciveEmailYn = reciveEmailYn;
        this.title = title;
        this.questionContent = questionContent;
        this.questionAttachYn = questionAttachYn;
        this.answerYn = answerYn;
        this.regDt = regDt;
        this.regUsr = regUsr;
        this.modDt = modDt;
        this.modUsr = modUsr;
        this.boardAttachDtoList = boardAttachDtoList;
    }
}