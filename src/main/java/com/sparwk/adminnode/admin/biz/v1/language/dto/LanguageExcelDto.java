package com.sparwk.adminnode.admin.biz.v1.language.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sparwk.adminnode.admin.jpa.entity.ExcelColumnName;
import com.sparwk.adminnode.admin.jpa.entity.ExcelDto;
import com.sparwk.adminnode.admin.jpa.entity.ExcelFileName;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Arrays;
import java.util.List;

@Data
@NoArgsConstructor
@ExcelFileName
public class LanguageExcelDto implements ExcelDto {

    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    @ExcelColumnName(headerName = "Language Detail Code Seq")
    private Long languageSeq;

    @Schema(description = "언어명", nullable = false, example = "English")
    @ExcelColumnName
    @JsonProperty("language")
    private String language;

    @Schema(description = "언어그룹", nullable = false, example = "Indo-European")
    @ExcelColumnName
    @JsonProperty("language_family")
    private String languageFamily;

    @Schema(description = "현지어", nullable = false, example = "English")
    @ExcelColumnName
    @JsonProperty("native_language")
    private String nativeLanguage;

    @Schema(description = "현지어", nullable = false, example = "en")
    @ExcelColumnName
    @JsonProperty("iso639_1")
    private String iso639_1;

    @ExcelColumnName
    @JsonProperty("iso639_2t")
    private String iso639_2t;

    @ExcelColumnName
    @JsonProperty("iso639_2b")
    private String iso639_2b;

    @ExcelColumnName
    @JsonProperty("iso639_3")
    private String iso639_3;

    @ExcelColumnName
    @JsonProperty("language_cd")
    private String languageCd;

    @ExcelColumnName
    @JsonProperty("useType")
    private YnTypeEnum useType;

    @Override
    public List<String> mapToList() {
        return Arrays.asList(String.valueOf(languageSeq),
                                language,
                                languageFamily,
                                nativeLanguage,
                                iso639_1,
                                iso639_2t,
                                iso639_2b,
                                iso639_3,
                                languageCd,
                                String.valueOf(useType)
        );
    }

}
