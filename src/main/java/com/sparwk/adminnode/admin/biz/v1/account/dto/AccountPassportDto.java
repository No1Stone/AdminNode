package com.sparwk.adminnode.admin.biz.v1.account.dto;

import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class AccountPassportDto {

    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long accntId;
    @Schema(description = "passport_first_name", nullable = false, example = "alice")
    private String passportFirstName;
    @Schema(description = "passport_middle_name", nullable = true, example = "alice")
    private String passportMiddleName;
    @Schema(description = "passport_last_name", nullable = false, example = "alice")
    private String passportLastName;
    @Schema(description = "country_cd", nullable = false, example = "CNT000146")
    private String passportCountryCd;
    @Schema(description = "passport_img_file_url", nullable = true, example = "http://...")
    private String passportImgFileUrl;
    @Schema(description = "country_cd_name", nullable = true, example = "USA")
    private String passportCountryCdName;
    @Schema(description = "여권인증 Y/N", nullable = false, defaultValue = "N", allowableValues = {"Y", "N"})
    private YnTypeEnum passportVerifyYn;

    @Builder
    public AccountPassportDto(Long accntId,
                              String passportFirstName,
                              String passportMiddleName,
                              String passportLastName,
                              String passportCountryCd,
                              String passportImgFileUrl,
                              String passportCountryCdName,
                              YnTypeEnum passportVerifyYn
    ) {
        this.accntId = accntId;
        this.passportFirstName = passportFirstName;
        this.passportMiddleName = passportMiddleName;
        this.passportLastName = passportLastName;
        this.passportCountryCd = passportCountryCd;
        this.passportImgFileUrl = passportImgFileUrl;
        this.passportCountryCdName = passportCountryCdName;
        this.passportVerifyYn = passportVerifyYn;
    }
}
