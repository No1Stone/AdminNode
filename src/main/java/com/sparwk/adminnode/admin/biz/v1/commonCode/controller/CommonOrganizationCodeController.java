package com.sparwk.adminnode.admin.biz.v1.commonCode.controller;

import com.sparwk.adminnode.admin.biz.v1.CommonPagingListDto;
import com.sparwk.adminnode.admin.biz.v1.commonCode.dto.CommonOrganizationCodeDto;
import com.sparwk.adminnode.admin.biz.v1.commonCode.dto.CommonOrganizationCodeRequest;
import com.sparwk.adminnode.admin.biz.v1.commonCode.service.CommonOrganizationCodeService;
import com.sparwk.adminnode.admin.config.common.AdminPermissionIntergrationService;
import com.sparwk.adminnode.admin.config.common.Response;
import com.sparwk.adminnode.admin.config.common.ResultCodeConst;
import com.sparwk.adminnode.admin.config.util.ExcelXlsView;
import com.sparwk.adminnode.admin.jpa.entity.PeriodTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.commonCode.CommonCateEnum3;
import com.sparwk.adminnode.admin.jpa.entity.commonCode.CommonSorterEnum2;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.RequiredArgsConstructor;
import org.apache.commons.io.FilenameUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

@RestController
@RequestMapping(value = "/V1/setting/identifire/")
@RequiredArgsConstructor
@CrossOrigin("*")
public class CommonOrganizationCodeController {

    private final Logger logger = LoggerFactory.getLogger(CommonOrganizationCodeController.class);

    @Autowired
    private CommonOrganizationCodeService commonOrganizationCodeService;

    @Autowired
    private AdminPermissionIntergrationService adminPermissionIntergrationService;
    private final MessageSourceAccessor messageSourceAccessor;

    @Value("${file.path}")
    private String fileRealPath;

    @GetMapping("organization-codes/{commonCode}")
    @Operation(
            summary = "Common Organization Code All List", description = "Common Organization Code All Code List 입니다.",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = CommonOrganizationCodeDto.class))),
                    @ApiResponse(
                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1001", description = "부모코드가 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1101", description = "부모코드 값이 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true)))
            })
    public Response<CommonPagingListDto> getList(
            @Parameter(name = "commonCode", description = "부모코드", in = ParameterIn.PATH) @PathVariable("commonCode") String commonCode,
            @Parameter(name = "code", description = "코드 값 일치", in = ParameterIn.PATH) @RequestParam(value = "code", required = false) String code,
            @Parameter(name = "cate", description = "검색 종류", in = ParameterIn.PATH) @RequestParam(value = "cate", required = false) CommonCateEnum3 cate,
            @Parameter(name = "val", description = "검색어 포함", in = ParameterIn.PATH) @RequestParam(value = "val", required = false) String val,
            @Parameter(name = "useType", description = "사용여부 값 Y/N", in = ParameterIn.PATH) @RequestParam(value = "useType", required = false) YnTypeEnum useType,
            @Parameter(name = "periodType", description = "기간 종류", in = ParameterIn.PATH) @RequestParam(value = "periodType", required = false) PeriodTypeEnum periodType,
            @Parameter(name = "sdate", description = "시작일", in = ParameterIn.PATH) @RequestParam(value = "sdate", required = false) String sdate,
            @Parameter(name = "edate", description = "종료일", in = ParameterIn.PATH) @RequestParam(value = "edate", required = false) String edate,
            @Parameter(name = "sorter", description = "순서", in = ParameterIn.PATH) @RequestParam(value = "sorter", required = false) CommonSorterEnum2 sorter,
            @Parameter(name = "page", description = "페이지 번호", in = ParameterIn.PATH) @RequestParam(value = "page", required = false, defaultValue = "1") int page,
            @Parameter(name = "size", description = "페이지 사이즈", in = ParameterIn.PATH) @RequestParam(value = "size", required = false, defaultValue = "10") int size
    ) {
        PageRequest pageRequest = PageRequest.of(page, size);

        Response res = new Response();
        if(!adminPermissionIntergrationService.AdminPermissionIntergrationCheck(4L, "R")){
            res.setResultCd(ResultCodeConst.NOT_VALID_READ.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        return commonOrganizationCodeService.getList(commonCode, code, cate, val, useType, periodType, sdate, edate, sorter, pageRequest);
    }

    //사용중만 보기
    @GetMapping("organization-codes/{commonCode}/use")
    @Operation(
            summary = "Common Organization Code Use List", description = "Common Organization Code Use List 입니다.",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = CommonOrganizationCodeDto.class))),
                    @ApiResponse(
                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1001", description = "부모코드가 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1101", description = "부모코드 값이 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true)))
            })
    public Response<CommonPagingListDto> getUseYList(
            @Parameter(name = "commonCode", description = "부모코드", in = ParameterIn.PATH) @PathVariable("commonCode") String commonCode,
            @Parameter(name = "code", description = "코드 값 일치", in = ParameterIn.PATH) @RequestParam(value = "code", required = false) String code,
            @Parameter(name = "cate", description = "검색 종류", in = ParameterIn.PATH) @RequestParam(value = "cate", required = false) CommonCateEnum3 cate,
            @Parameter(name = "val", description = "검색어 포함", in = ParameterIn.PATH) @RequestParam(value = "val", required = false) String val
    ) {
        Response res = new Response();
        if(!adminPermissionIntergrationService.AdminPermissionIntergrationCheck(4L, "R")){
            res.setResultCd(ResultCodeConst.NOT_VALID_READ.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        return commonOrganizationCodeService.getUseYList(commonCode, code, cate, val);
    }

    //인기있는 것만 보기
    @GetMapping("organization-codes/{commonCode}/popular")
    @Operation(
            summary = "Popular Code List", description = "Common Organization Code Popular List 입니다.",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = CommonOrganizationCodeDto.class))),
                    @ApiResponse(
                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1001", description = "부모코드가 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1101", description = "부모코드 값이 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true)))
            })
    public Response<CommonPagingListDto> getPopularYist(
            @Parameter(name = "commonCode", description = "부모코드", in = ParameterIn.PATH) @PathVariable("commonCode") String commonCode,
            @Parameter(name = "code", description = "코드 값 일치", in = ParameterIn.PATH) @RequestParam(value = "code", required = false) String code,
            @Parameter(name = "cate", description = "검색 종류", in = ParameterIn.PATH) @RequestParam(value = "cate", required = false) CommonCateEnum3 cate,
            @Parameter(name = "val", description = "검색어 포함", in = ParameterIn.PATH) @RequestParam(value = "val", required = false) String val
    ) {
        Response res = new Response();
        if(!adminPermissionIntergrationService.AdminPermissionIntergrationCheck(4L, "R")){
            res.setResultCd(ResultCodeConst.NOT_VALID_READ.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        return commonOrganizationCodeService.getPopularYList(commonCode, code, cate, val);
    }

    //상세페이지
    @GetMapping("organization-codes/{commonCode}/{id}")
    @Operation(
            summary = "Common Organization Code View", description = "Common Organization Code View 입니다.",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = CommonOrganizationCodeDto.class))),
                    @ApiResponse(
                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1001", description = "부모코드가 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1101", description = "부모코드 값이 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1003", description = "id가 존재하지 않습니다.."
                            , content = @Content(schema = @Schema(hidden = true)))
            })
    public Response<Optional<CommonOrganizationCodeDto>> getOne(
            @Parameter(name = "commonCode", description = "부모코드", in = ParameterIn.PATH)
            @PathVariable(value = "commonCode") String commonCode,
            @Parameter(name = "id", description = "id 값", in = ParameterIn.PATH)
            @PathVariable(value = "id") Long id
    ) {
        //logger.info("aaa====={}",commonCode);
        //logger.info("bbb====={}",id);

        Response res = new Response();
        if(!adminPermissionIntergrationService.AdminPermissionIntergrationCheck(4L, "R")){
            res.setResultCd(ResultCodeConst.NOT_VALID_READ.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        return commonOrganizationCodeService.getOne(commonCode, id);
    }

    //쓰기처리
    @PostMapping("organization-codes/{commonCode}/new")
    @Operation(
            summary = "New Code", description = "Common Organization Code 추가합니다.",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = CommonOrganizationCodeDto.class))),
                    @ApiResponse(
                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1001", description = "부모코드가 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1101", description = "부모코드 값이 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1204", description = "Format Type 값 형식이 잘못되었습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1203", description = "Use Type 값 형식이 잘못되었습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1205", description = "Popular Type 값 형식이 잘못되었습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1206", description = "Value 형식이 잘못되었습니다."
                            , content = @Content(schema = @Schema(hidden = true)))
            })
    public Response<Void> saveContent(
            @Parameter(name = "commonCode", description = "부모코드", in = ParameterIn.PATH) @PathVariable("commonCode") String commonCode,
            @Parameter(name = "request", description = "Request Dto", in = ParameterIn.PATH) @RequestBody @Valid CommonOrganizationCodeRequest request,
            HttpServletRequest req
    ) {
        Response res = new Response();
        if(!adminPermissionIntergrationService.AdminPermissionIntergrationCheck(4L, "C")){
            res.setResultCd(ResultCodeConst.NOT_VALID_CREATE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        return commonOrganizationCodeService.saveContent(commonCode, request, req);
    }


    //사용여부 토글처리
    @PostMapping("organization-codes/{commonCode}/{id}/use")
    @Operation(
            summary = "Use Y/N", description = "Common Organization Code 사용/미사용을 처리합니다.",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = CommonOrganizationCodeDto.class))),
                    @ApiResponse(
                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1001", description = "부모코드가 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1101", description = "부모코드 값이 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1003", description = "id가 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true)))
            })
    public Response<Void> updateYn(
            @Parameter(name = "commonCode", description = "부모코드", in = ParameterIn.PATH) @PathVariable(value = "commonCode") String commonCode,
            @Parameter(name = "id", description = "id 값", in = ParameterIn.PATH) @PathVariable(value = "id") Long id,
            HttpServletRequest req
    ) {
        Response res = new Response();
        if(!adminPermissionIntergrationService.AdminPermissionIntergrationCheck(4L, "U")){
            res.setResultCd(ResultCodeConst.NOT_VALID_UPDATE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        return commonOrganizationCodeService.updateYn(commonCode, id, req);
    }

    //popular여부 토글처리
    @PostMapping("organization-codes/{commonCode}/{id}/popular")
    @Operation(
            summary = "Popular Y/N", description = "Common Organization Code Popular 체크합니다.",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = CommonOrganizationCodeDto.class))),
                    @ApiResponse(
                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1001", description = "부모코드가 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1101", description = "부모코드 값이 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1003", description = "id가 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true)))
            })
    public Response<Void> updatePopularYn(
            @Parameter(name = "commonCode", description = "부모코드", in = ParameterIn.PATH) @PathVariable(value = "commonCode") String commonCode,
            @Parameter(name = "id", description = "id 값", in = ParameterIn.PATH) @PathVariable(value = "id") Long id,
            HttpServletRequest req
    ) {
        Response res = new Response();
        if(!adminPermissionIntergrationService.AdminPermissionIntergrationCheck(4L, "U")){
            res.setResultCd(ResultCodeConst.NOT_VALID_UPDATE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        return commonOrganizationCodeService.updatePopularYn(commonCode, id, req);
    }

    //업데이트처리
    @PostMapping("organization-codes/{commonCode}/{id}/modify")
    @Operation(
            summary = "Modify Code", description = "Common Organization Code 수정합니다.",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = CommonOrganizationCodeDto.class))),
                    @ApiResponse(
                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1001", description = "부모코드가 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1101", description = "부모코드 값이 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1003", description = "id가 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1204", description = "Format Type 값 형식이 잘못되었습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1203", description = "Use Type 값 형식이 잘못되었습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1205", description = "Popular Type 값 형식이 잘못되었습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1206", description = "Value 형식이 잘못되었습니다."
                            , content = @Content(schema = @Schema(hidden = true)))
            })
    public Response<Optional<CommonOrganizationCodeDto>> updateContent(
            @Parameter(name = "commonCode", description = "부모코드", in = ParameterIn.PATH) @PathVariable(value = "commonCode") String commonCode,
            @Parameter(name = "id", description = "id 값", in = ParameterIn.PATH) @PathVariable(value = "id") Long id,
            @Parameter(name = "request", description = "Request Dto", in = ParameterIn.PATH) @RequestBody @Valid CommonOrganizationCodeRequest request,
            HttpServletRequest req
    ) {
        Response res = new Response();
        if(!adminPermissionIntergrationService.AdminPermissionIntergrationCheck(4L, "U")){
            res.setResultCd(ResultCodeConst.NOT_VALID_UPDATE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        return commonOrganizationCodeService.updateContent(commonCode, id, request, req);
    }

    //삭제처리
    @DeleteMapping("organization-codes/{commonCode}/{id}/delete")
    @Operation(
            summary = "delete Code", description = "Common Organization Code 삭제합니다.",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = CommonOrganizationCodeDto.class))),
                    @ApiResponse(
                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1001", description = "부모코드가 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1101", description = "부모코드 값이 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1003", description = "id가 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true)))
            })
    public Response<Void> delete(
            @Parameter(name = "commonCode", description = "부모코드", in = ParameterIn.PATH) @PathVariable(value = "commonCode") String commonCode,
            @Parameter(name = "id", description = "id 값", in = ParameterIn.PATH) @PathVariable(value = "id") Long id,
            HttpServletRequest req) {
        Response res = new Response();
        if(!adminPermissionIntergrationService.AdminPermissionIntergrationCheck(4L, "D")){
            res.setResultCd(ResultCodeConst.NOT_VALID_DELETE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        return commonOrganizationCodeService.deleteContent(commonCode, id, req);
    }

    //EXCEL DOWN
    @GetMapping("organization-codes/down-excel")
    @Operation(
            summary = "Common Code List Excel Down", description = "Common Organization Code Excel Down 입니다.",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "Common Organization Code List Excel Down"
                            , content = @Content(schema = @Schema(implementation = CommonOrganizationCodeDto.class)))
            })
    public ModelAndView getExcelDownList(
            @Parameter(name = "commonCode", description = "부모코드", in = ParameterIn.PATH) @RequestParam(value = "commonCode", required = true) String commonCode,
            HttpServletRequest request
    ) {
        Map<String, Object> excelData = null;
        try {
            excelData = commonOrganizationCodeService.downExcelList(commonCode);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ModelAndView(new ExcelXlsView(), excelData);
    }

    @GetMapping("organization-codes/{commonCode}/test")
    @Operation(
            summary = "Common Organization Code All List", description = "Common Organization Code All Code List 입니다.",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = CommonOrganizationCodeDto.class))),
                    @ApiResponse(
                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1001", description = "부모코드가 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true))),
                    @ApiResponse(
                            responseCode = "1101", description = "부모코드 값이 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true)))
            })
    public Integer getTest(
            @Parameter(name = "commonCode", description = "부모코드", in = ParameterIn.PATH) @PathVariable("commonCode") String commonCode
    ) {
        return commonOrganizationCodeService.getLastSeq(commonCode);
    }


    // @ResponseBody 생략가능. class 에 @RestController
    @PostMapping(value="organization-codes/{commonCode}/upload-excel")
    public Response uploadFile(
            @Parameter(name = "commonCode", description = "부모코드", in = ParameterIn.PATH) @PathVariable(value = "commonCode") String commonCode,
            @RequestPart(value="key", required=false) @Valid CommonOrganizationCodeRequest request,
            @RequestParam("file") MultipartFile file,
            Model model,
            HttpServletRequest req
    ) throws IllegalStateException, IOException {

        Response res = new Response();
        if(!adminPermissionIntergrationService.AdminPermissionIntergrationCheck(4L, "C")){
            res.setResultCd(ResultCodeConst.NOT_VALID_CREATE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        if( !file.isEmpty() ) {
            logger.info("file org name = {}", file.getOriginalFilename());
            logger.info("file content type = {}", file.getContentType());
            logger.info("file content type = {}", file.getSize());
            logger.info("file content type = {}", file.getName());
            //file.transferTo(new File(file.getOriginalFilename()));
        }

        List<CommonOrganizationCodeDto> dataList = new ArrayList<>();

        String extension = FilenameUtils.getExtension(file.getOriginalFilename()); // 3

        if (!extension.equals("xlsx") && !extension.equals("xls")) {
            throw new IOException("엑셀파일만 업로드 해주세요.");
        }


        System.out.println(fileRealPath);

        UUID uuid = UUID.randomUUID();
        String uuidFilename = uuid+"_"+file.getOriginalFilename();


        //이미지 저장 위치: 외부에 잡기/ 나중에 이 경로랑 파일명만 db에 저장하면 된다.
        //nio 로 임포트
        Path filePath = Paths.get(fileRealPath+uuidFilename);

        logger.info("{}", filePath.toString());

        try {
            Files.write(filePath,file.getBytes());
            //얘가 스레드 새로 만들어서 만약 10메가 넘어가면 여기서 파일 넣는동안 메인스레드가 ok 뿌려서 엑박뜸->
            //사진 용량 제한시키기 아니면 메인스레드 기다리게하기(비동기처리)

            Workbook workbook = null;

            if (extension.equals("xlsx")) {
                workbook = new XSSFWorkbook(file.getInputStream());
            } else if (extension.equals("xls")) {
                workbook = new HSSFWorkbook(file.getInputStream());
            }

            Sheet worksheet = workbook.getSheetAt(0);

            Integer k = commonOrganizationCodeService.getLastSeq(commonCode);
            Integer kk = 0;
            String dcode;
            String val;
            String country;
            String desc;

            for (int i = 2; i < worksheet.getPhysicalNumberOfRows(); i++) { // 4

                Row row = worksheet.getRow(i);

                k++;

                if(k < 10)
                    dcode = commonCode + "00000" + k;
                else if(k < 100)
                    dcode = commonCode + "0000" + k;
                else if(k < 1000)
                    dcode = commonCode + "000" + k;
                else if(k < 10000)
                    dcode = commonCode + "00" + k;
                else if(k < 100000)
                    dcode = commonCode + "0" + k;
                else if(k < 1000000)
                    dcode = commonCode + k;
                else
                    dcode = commonCode + k;

                val = (row.getCell(1) == null) ? "" : row.getCell(1).getStringCellValue();
                country = (row.getCell(2) == null) ? "" : row.getCell(2).getStringCellValue();
                desc = (row.getCell(3) == null) ? "" : row.getCell(3).getStringCellValue();

                CommonOrganizationCodeDto commonOrganizationCodeDto = new CommonOrganizationCodeDto();

                commonOrganizationCodeDto.setPcode(commonCode);
                commonOrganizationCodeDto.setDcode(dcode);
                commonOrganizationCodeDto.setVal(val);
                commonOrganizationCodeDto.setCountry(country);
                commonOrganizationCodeDto.setSortIndex(k);
                commonOrganizationCodeDto.setDescription(desc);
                commonOrganizationCodeDto.setUseType(YnTypeEnum.Y);
                commonOrganizationCodeDto.setPopularType(YnTypeEnum.N);
                //숫자일경우
                //testExcelDto.setCdesc(row.getCell(3).getNumericCellValue());

//                logger.info("--------------------------------------------------------");
//                logger.info("val - {} : {}", val, row.getCell(1) );
//                logger.info("country - {} : {}", country, row.getCell(2) );
//                logger.info("desc - {} : {}", desc, row.getCell(3) );
//                logger.info("--------------------------------------------------------");

                //logger.info("val - {}", val);
                Long countVal = commonOrganizationCodeService.getOneName(val);
                //logger.info("countVal - {}", countVal);
                if(countVal > 0){
                    res.setResultCd(ResultCodeConst.NOT_VALID_OVERLAP.getCode());
                    res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
                    return res;
                }

                dataList.add(commonOrganizationCodeDto);

                logger.info("commonOrganizationCodeDto - {}", commonOrganizationCodeDto.toString());
                Response<Void> result = commonOrganizationCodeService.saveExcelContent(commonCode, commonOrganizationCodeDto, req);
                if (result.getResultCd().equals("0000")) {
                    kk++;
                }

            }

            //성공해야만 seq 업데이트
            if(kk > 0) {
                commonOrganizationCodeService.updateLastSeq(commonCode, k);
            }

            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }

        return res;
    }

}
