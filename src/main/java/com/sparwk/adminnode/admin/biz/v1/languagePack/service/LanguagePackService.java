package com.sparwk.adminnode.admin.biz.v1.languagePack.service;

import com.sparwk.adminnode.admin.biz.v1.CommonPagingListDto;
import com.sparwk.adminnode.admin.biz.v1.admin.service.AdminActiveLogService;
import com.sparwk.adminnode.admin.biz.v1.language.dto.LanguageDto;
import com.sparwk.adminnode.admin.biz.v1.language.dto.LanguageExcelDto;
import com.sparwk.adminnode.admin.biz.v1.language.dto.LanguageRequest;
import com.sparwk.adminnode.admin.biz.v1.languagePack.dto.LanguagePackDto;
import com.sparwk.adminnode.admin.biz.v1.languagePack.dto.LanguagePackRequest;
import com.sparwk.adminnode.admin.config.common.ActiveCodeConst;
import com.sparwk.adminnode.admin.config.common.GetAccountId;
import com.sparwk.adminnode.admin.config.common.Response;
import com.sparwk.adminnode.admin.config.common.ResultCodeConst;
import com.sparwk.adminnode.admin.config.util.ExcelWriter;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.admin.AdminActiveLogEntity;
import com.sparwk.adminnode.admin.jpa.entity.language.LanguageCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.language.LanguageEntity;
import com.sparwk.adminnode.admin.jpa.entity.language.LanguageSorterEnum;
import com.sparwk.adminnode.admin.jpa.entity.languagePack.LanguagePackCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.languagePack.LanguagePackEntity;
import com.sparwk.adminnode.admin.jpa.entity.languagePack.LanguagePackSorterEnum;
import com.sparwk.adminnode.admin.jpa.repository.language.LanguageRepository;
import com.sparwk.adminnode.admin.jpa.repository.languagePack.LanguagePackRepository;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class LanguagePackService {

    private final LanguageRepository languageRepository;
    private final LanguagePackRepository languagePackRepository;
    private final AdminActiveLogService adminActiveLogService;

    private final HttpServletRequest httpServletRequest;
    private final MessageSourceAccessor messageSourceAccessor;
    
    private final Logger logger = LoggerFactory.getLogger(LanguagePackService.class);

    public Response<List<LanguageEntity>> getLanguageList(YnTypeEnum useType,
                                                String val) {

        Response<List<LanguageEntity>> res = new Response<>();

        CommonPagingListDto list = new CommonPagingListDto();

        List<LanguageEntity> languageList = null;

        if(val == null || String.valueOf(val).equals(""))
            languageList = languageRepository.findByUseType(useType);
        else
            languageList = languageRepository.findByUseTypeAndLanguage(useType, val);

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(languageList);

        return res;
    }


    public Response<CommonPagingListDto> getList(LanguagePackCateEnum languagePackCateEnum,
                                                  String val,
                                                  YnTypeEnum useType,
                                                  LanguagePackSorterEnum sorter,
                                                  PageRequest pageRequest) {

        Response<CommonPagingListDto> res = new Response<>();

        List<LanguagePackDto> languagePackList = languagePackRepository.findQryAll(languagePackCateEnum, val, useType, sorter, pageRequest);
        logger.info("languagePackList - {}", languagePackList);

        //조회내용 없을 때
        if(languagePackList == null || languagePackList.size() < 1){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        CommonPagingListDto list = new CommonPagingListDto();

        //총 카운트
        long totalSize = languagePackRepository.countQryAll(languagePackCateEnum, val, useType);
        long totalPage = totalSize / pageRequest.getPageSize();
        long mod = totalSize % pageRequest.getPageSize();
        if(mod > 0) {
            totalPage += 1;
        }

        list.setList(languagePackList);
        list.setSize(pageRequest.getPageSize());
        list.setPage(pageRequest.getPageNumber());
        list.setTotalPage(totalPage);
        list.setTotalSize(totalSize);

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(list);
        return res;
    }

    public Response<Optional<LanguagePackDto>> getOne(Long id) {

        Response<Optional<LanguagePackDto>> res = new Response<>();

        //id값이 없을 때
        if(id == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<LanguagePackEntity> languagePack = languagePackRepository.findById(id);

        logger.info("languagePack - {}", languagePack);

        //조회내용 없을 때
        if(!languagePack.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<LanguageEntity> language = languageRepository.findByLanguageCd(languagePack.get().getLanguageCd());

        logger.info("language - {}", language);

        //조회내용 없을 때
        if(!language.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }
        
        Optional<LanguagePackDto> dtoPage =

                languagePack.map(p -> LanguagePackDto.builder()
                                .packId(p.getPackId())
                                .languageCd(p.getLanguageCd())
                                .languageName(language.get().getLanguage())
                                .nativeLanguage(language.get().getNativeLanguage())
                                .nationalFlagUrl(p.getNationalFlagUrl())
                                .jsonUploadUrl(p.getJsonUploadUrl())
                                .useType(p.getUseType())
                                .languageCd(p.getLanguageCd())
                                .regUsr(p.getRegUsr())
                                .regDt(p.getRegDt())
                                .modUsr(p.getModUsr())
                                .modDt(p.getModDt())
                                .build()
                        );

        logger.info("dtoPage - {}", dtoPage);

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(dtoPage);
        return res;
    }

    @Transactional
    public Response<Void> saveContent(LanguagePackRequest request, HttpServletRequest req) {

        Response<Void> res = new Response<>();
        String languageCd;

        //USEYN 값 없거나 틀릴 때
        if(request.getUseType() == null || (request.getUseType() != YnTypeEnum.Y && request.getUseType() != YnTypeEnum.N)){
            res.setResultCd(ResultCodeConst.NOT_VALID_USEYN.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if(request.getLanguageCd() == null){
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if(request.getNationalFlagUrl() == null){
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if(request.getJsonUploadUrl() == null){
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        int maxVersion = languagePackRepository.findQryMaxVersion(request.getLanguageCd());

        try {

            LanguagePackEntity newLanguagePack = LanguagePackEntity.builder()
                    .languageCd(request.getLanguageCd())
                    .nationalFlagUrl(request.getNationalFlagUrl())
                    .jsonUploadUrl(request.getJsonUploadUrl())
                    .packVersion(maxVersion+1)
                    .useType(request.getUseType())
                    .build();

            LanguagePackEntity saved = languagePackRepository.save(newLanguagePack);

            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
//            res.setResult(dtoPage);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));

            String menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_LANGUAGE_PACK.getCode());
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_CREATE.getCode());

            //netive query
            String usrName = adminActiveLogService.findQryUsrName(saved.getRegUsr());

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(saved.getRegDt())
                    .adminId(saved.getRegUsr())
                    .menuName(menuName)
                    .activeType("C")
                    .contentsId(saved.getPackId())
                    .activeMsg(usrName + " " + activeMsg + " '" + saved.getLanguageCd() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e){
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }

        return res;
    }

    @Transactional
    public Response<Void> updateYn(Long id, HttpServletRequest req) {

        Response<Void> res = new Response<>();

        Optional<LanguagePackEntity> languagePack = languagePackRepository.findById(id);

        logger.info("languagePack - {}", languagePack);

        //조회내용 없을 때
        if(!languagePack.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        YnTypeEnum useType = languagePack.get().getUseType();
        YnTypeEnum newUseType = (useType == YnTypeEnum.Y) ? YnTypeEnum.N : YnTypeEnum.Y;

        logger.info("newUseType - {} ", newUseType);

        try {
            languagePackRepository.updateQryUseYn(languagePack.get().getLanguageCd(), newUseType);
            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            //res.setResult(songDetailCode);
        } catch (Exception e) {
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }
        return res;

    }

    @Transactional
    public Response<Void> deleteContent(Long id, HttpServletRequest req) {

        Response<Void> res = new Response<>();

        //id값이 없을 때
        if(id == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<LanguagePackEntity> languagePack = languagePackRepository.findById(id);

        logger.info("languagePack - {} ", languagePack);

        //조회내용 없을 때
        if(!languagePack.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        try {
            languagePackRepository.deleteById(id);
            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
//            res.setResult(dtoPage);
        } catch (Exception e) {
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }
        return res;

    }

}
