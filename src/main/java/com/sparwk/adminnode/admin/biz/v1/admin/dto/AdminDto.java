package com.sparwk.adminnode.admin.biz.v1.admin.dto;

import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Setter
@Getter
@NoArgsConstructor
public class AdminDto {

    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long adminId;
    @Schema(description = "Admin Email", nullable = false, example = "admin@sparwk.com")
    private String adminEmail;
    @Schema(description = "Full Name", nullable = false, example = "Yoon Jung Sub")
    private String fullName;
    @Schema(description = "국가번호", nullable = false, example = "082")
    private String dial;
    @Schema(description = "전화번호", nullable = false, example = "01012345678")
    private String phoneNumber;
    @Schema(description = "권한 id값 Seq 사용", nullable = false, example = "1")
    private Long permissionAssignSeq;
    @Schema(description = "통합관리자", nullable = false, example = "01012345678")
    private String permissionAssignName;
    @Schema(description = "설명", nullable = true, example = "설명")
    private String description;
    @Schema(description = "관리자 사용여부 Y/N", nullable = false, defaultValue = "Y", allowableValues = {"Y", "N"})
    private YnTypeEnum useType;
    @Schema(description = "관리자 로그인 잠김여부 Y/N", nullable = false, defaultValue = "N", allowableValues = {"Y", "N"})
    private YnTypeEnum lockType;
    @Schema(description = "등록일", pattern = "yyMMdd hh:mm:ss", example = "2021-12-08T05:44:26.627991")
    private LocalDateTime regDt;
    @Schema(description = "등록자", nullable = false, example = "10000001")
    private Long regUsr;
    @Schema(description = "등록자 이름", nullable = false, example = "Yoon Jung Sun")
    private String regUsrName;
    @Schema(description = "수정일", pattern = "yyMMdd hh:mm:ss", example = "2021-12-08T05:44:26.627991")
    private LocalDateTime modDt;
    @Schema(description = "수정자", nullable = false, example = "10000001")
    private Long modUsr;
    @Schema(description = "수정자 이름", nullable = false, example = "Yoon Jung Sun")
    private String modUsrName;


    @Builder
    public AdminDto(Long adminId,
                    String adminEmail,
                    String fullName,
                    String dial,
                    String phoneNumber,
                    Long permissionAssignSeq,
                    String permissionAssignName,
                    String description,
                    YnTypeEnum useType,
                    YnTypeEnum lockType,
                    LocalDateTime regDt,
                    Long regUsr,
                    String regUsrName,
                    LocalDateTime modDt,
                    Long modUsr,
                    String modUsrName
                    ) {
        this.adminId = adminId;
        this.adminEmail = adminEmail;
        this.fullName = fullName;
        this.dial = dial;
        this.phoneNumber = phoneNumber;
        this.permissionAssignSeq = permissionAssignSeq;
        this.permissionAssignName = permissionAssignName;
        this.description = description;
        this.useType = useType;
        this.lockType = lockType;
        this.regDt = regDt;
        this.regUsr = regUsr;
        this.regUsrName = regUsrName;
        this.modDt = modDt;
        this.modUsr = modUsr;
        this.modUsrName = modUsrName;
    }
}