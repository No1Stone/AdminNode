package com.sparwk.adminnode.admin.biz.v1.commonCode.service;

import com.sparwk.adminnode.admin.biz.v1.CommonPagingListDto;
import com.sparwk.adminnode.admin.biz.v1.admin.service.AdminActiveLogService;
import com.sparwk.adminnode.admin.biz.v1.commonCode.dto.CommonOrganizationCodeDto;
import com.sparwk.adminnode.admin.biz.v1.commonCode.dto.CommonOrganizationCodeExcelDto;
import com.sparwk.adminnode.admin.biz.v1.commonCode.dto.CommonOrganizationCodeRequest;
import com.sparwk.adminnode.admin.config.common.ActiveCodeConst;
import com.sparwk.adminnode.admin.config.common.GetAccountId;
import com.sparwk.adminnode.admin.config.common.Response;
import com.sparwk.adminnode.admin.config.common.ResultCodeConst;
import com.sparwk.adminnode.admin.config.util.ExcelWriter;
import com.sparwk.adminnode.admin.jpa.entity.PeriodTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.admin.AdminActiveLogEntity;
import com.sparwk.adminnode.admin.jpa.entity.commonCode.*;
import com.sparwk.adminnode.admin.jpa.entity.country.CountryEntity;
import com.sparwk.adminnode.admin.jpa.repository.commonCode.CommonCodeRepository;
import com.sparwk.adminnode.admin.jpa.repository.commonCode.CommonOrganizationCodeRepository;
import com.sparwk.adminnode.admin.jpa.repository.country.CountryRepository;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class CommonOrganizationCodeService {

    private final CommonCodeRepository commonCodeRepository;
    private final CommonOrganizationCodeRepository commonOrganizationCodeRepository;
    private final CountryRepository countryRepository;
    private final AdminActiveLogService adminActiveLogService;

    private final HttpServletRequest httpServletRequest;
    private final MessageSourceAccessor messageSourceAccessor;

    private final Logger logger = LoggerFactory.getLogger(CommonOrganizationCodeService.class);


    public Response<CommonPagingListDto> getList(String pcode,
                                                 String code,
                                                 CommonCateEnum3 cate,
                                                 String val,
                                                 YnTypeEnum useType,
                                                 PeriodTypeEnum periodType,
                                                 String sdate,
                                                 String edate,
                                                 CommonSorterEnum2 sorter,
                                                 PageRequest pageRequest) {

        Response<CommonPagingListDto> res = new Response<>();

        //부모코드 없을 때
        if (pcode == null) {
            res.setResultCd(ResultCodeConst.NOT_EXIST_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<CommonCodeEntity> commonCode = Optional.ofNullable(
                commonCodeRepository.findByCode(pcode)
        );

        logger.info("commonCode - {}", commonCode);

        //부모코드 조회내용 없을 때
        if (!commonCode.isPresent()) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        List<CommonOrganizationCodeDto> commonOrganizationCodeList =
                commonOrganizationCodeRepository.findQryAll(pcode, code, cate, val, useType, periodType, sdate, edate, sorter, pageRequest);

        logger.info("CommonOrganizationCodeList - {}", commonOrganizationCodeList);

        //조회내용 없을 때
        if (commonOrganizationCodeList == null || commonOrganizationCodeList.size() < 1) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        CommonPagingListDto list = new CommonPagingListDto();

        //총 카운트
        long totalSize = commonOrganizationCodeRepository.countQryAll(pcode, code, cate, val, useType, periodType, sdate, edate);
        long totalPage = totalSize / pageRequest.getPageSize();
        long mod = totalSize % pageRequest.getPageSize();
        if(mod > 0) {
            totalPage += 1;
        }

        list.setList(commonOrganizationCodeList);
        list.setSize(pageRequest.getPageSize());
        list.setPage(pageRequest.getPageNumber());
        list.setTotalPage(totalPage);
        list.setTotalSize(totalSize);

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(list);
        return res;
    }

    public Response<CommonPagingListDto> getUseYList(String pcode, String code, CommonCateEnum3 cate, String val) {

        Response<CommonPagingListDto> res = new Response<>();

        //부모코드 없을 때
        if (pcode == null) {
            res.setResultCd(ResultCodeConst.NOT_EXIST_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<CommonCodeEntity> commonCode = Optional.ofNullable(
                commonCodeRepository.findByCode(pcode)
        );

        logger.info("commonCode - {}", commonCode);

        //부모코드 조회내용 없을 때
        if (!commonCode.isPresent()) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        List<CommonOrganizationCodeDto> commonOrganizationCodeList = commonOrganizationCodeRepository.findQryUseY(pcode, code, cate, val);

        logger.info("CommonOrganizationCodeList - {}", commonOrganizationCodeList);

        //조회내용 없을 때
        if (commonOrganizationCodeList == null || commonOrganizationCodeList.size() < 1) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        CommonPagingListDto list = new CommonPagingListDto();

        PageRequest pageRequest = PageRequest.of(10, 10);

        //총 카운트
        long totalSize = commonOrganizationCodeRepository.countQryUseY(pcode, code, cate, val);
        long totalPage = totalSize / pageRequest.getPageSize();
        long mod = totalSize % pageRequest.getPageSize();
        if(mod > 0) {
            totalPage += 1;
        }

        list.setList(commonOrganizationCodeList);
        list.setSize(pageRequest.getPageSize());
        list.setPage(pageRequest.getPageNumber());
        list.setTotalPage(totalPage);
        list.setTotalSize(totalSize);

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(list);
        return res;
    }

    public Response<CommonPagingListDto> getPopularYList(String pcode, String code, CommonCateEnum3 cate, String val) {

        Response<CommonPagingListDto> res = new Response<>();

        //부모코드 없을 때
        if (pcode == null) {
            res.setResultCd(ResultCodeConst.NOT_EXIST_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<CommonCodeEntity> commonCode = Optional.ofNullable(
                commonCodeRepository.findByCode(pcode)
        );

        logger.info("commonCode - {}", commonCode);

        //부모코드 조회내용 없을 때
        if (!commonCode.isPresent()) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        List<CommonOrganizationCodeDto> commonOrganizationCodeList = commonOrganizationCodeRepository.findPopularUseY(pcode, code, cate, val);

        logger.info("CommonOrganizationCodeList - {}", commonOrganizationCodeList);

        //조회내용 없을 때
        if (commonOrganizationCodeList == null || commonOrganizationCodeList.size() < 1) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        CommonPagingListDto list = new CommonPagingListDto();

        PageRequest pageRequest = PageRequest.of(10, 10);

        //총 카운트
        long totalSize = commonOrganizationCodeRepository.countPopularUseY(pcode, code, cate, val);
        long totalPage = totalSize / pageRequest.getPageSize();
        long mod = totalSize % pageRequest.getPageSize();
        if(mod > 0) {
            totalPage += 1;
        }

        list.setList(commonOrganizationCodeList);
        list.setSize(pageRequest.getPageSize());
        list.setPage(pageRequest.getPageNumber());
        list.setTotalPage(totalPage);
        list.setTotalSize(totalSize);

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(list);
        return res;
    }

    public Response<Optional<CommonOrganizationCodeDto>> getOne(String pcode, Long id) {



        Response<Optional<CommonOrganizationCodeDto>> res = new Response<>();

        //부모코드 없을 때
        if (pcode == null) {
            res.setResultCd(ResultCodeConst.NOT_EXIST_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //id값이 없을 때
        if (id == null) {
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<CommonCodeEntity> commonCode = Optional.ofNullable(
                commonCodeRepository.findByCode(pcode)
        );

        logger.info("commonCode - {}", commonCode);

        //부모코드 조회내용 없을 때
        if (!commonCode.isPresent()) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        logger.info("adfasdfasdfasdfasdfasdf");

        Optional<CommonOrganizationCodeEntity> commonOrganizationCode = commonOrganizationCodeRepository.findById(id);
        //CommonOrganizationCodeEntity commonOrganizationCode = commonOrganizationCodeRepository.findById(id).orElse(null);

        logger.info("commonOrganizationCode - {}", commonOrganizationCode);

        //조회내용 없을 때
        if (!commonOrganizationCode.isPresent()) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<CountryEntity> country = Optional.ofNullable(countryRepository.findByCountryCd(commonOrganizationCode.get().getCountryCd()));

        //조회내용 없을 때
        if (!country.isPresent()) {
            res.setResultCd(ResultCodeConst.NOT_EXIST_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<CommonOrganizationCodeDto> dtoPage =

                commonOrganizationCode
                        .map(p -> CommonOrganizationCodeDto.builder()
                                .commonOrganizationCodeSeq(p.getCommonOrganizationCodeSeq())
                                .pcode(p.getCommonCodeEntity().getCode())
                                .pcodeVal(p.getCommonCodeEntity().getVal())
                                .dcode(p.getDcode())
                                .val(p.getVal())
                                .countryCd(p.getCountryCd())
                                .country(country.get().getCountry())
                                .useType(p.getUseType())
                                .popularType(p.getPopularType())
                                .description(p.getDescription())
                                .sortIndex(p.getSortIndex())
                                .hit(p.getHit())
                                .regUsr(p.getRegUsr())
                                .regDt(p.getRegDt())
                                .modUsr(p.getModUsr())
                                .modDt(p.getModDt())
                                .build()
                        );

        logger.info("CommonOrganizationCode - {}", dtoPage);

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(dtoPage);
        return res;
    }

    @Transactional
    public Response<Void> saveContent(String pcode, CommonOrganizationCodeRequest request, HttpServletRequest req) {

        Response<Void> res = new Response<>();
        String dcode;

        //부모코드 없을 때
        if (request.getCommonOrganizationCodeDto().getPcode() == null) {
            res.setResultCd(ResultCodeConst.NOT_VALID_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<CommonCodeEntity> commonCode = Optional.ofNullable(commonCodeRepository.findByCode(pcode));

        logger.info("commonCode - {}", commonCode);

        //부모코드 조회내용 없을 때
        if (!commonCode.isPresent()) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //USEYN 값 없거나 틀릴 때
        if (request.getCommonOrganizationCodeDto().getUseType() == null || (request.getCommonOrganizationCodeDto().getUseType() != YnTypeEnum.Y && request.getCommonOrganizationCodeDto().getUseType() != YnTypeEnum.N)) {
            res.setResultCd(ResultCodeConst.NOT_VALID_USEYN.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //POPULAR 값 없거나 틀릴 때
        if (request.getCommonOrganizationCodeDto().getPopularType() == null || (request.getCommonOrganizationCodeDto().getPopularType() != YnTypeEnum.Y && request.getCommonOrganizationCodeDto().getPopularType() != YnTypeEnum.N)) {
            res.setResultCd(ResultCodeConst.NOT_VALID_POPULAR.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if (request.getCommonOrganizationCodeDto().getVal() == null) {
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        try {

            //seq 번호 불러옴
            int k = commonOrganizationCodeRepository.getLastSeq(pcode) + 1;

            if(k < 10)
                dcode = pcode + "00000" + k;
            else if(k < 100)
                dcode = pcode + "0000" + k;
            else if(k < 1000)
                dcode = pcode + "000" + k;
            else if(k < 10000)
                dcode = pcode + "00" + k;
            else if(k < 100000)
                dcode = pcode + "0" + k;
            else if(k < 1000000)
                dcode = pcode + k;
            else
                dcode = pcode + k;
            
            CommonOrganizationCodeEntity newCommonOrganizationCode = CommonOrganizationCodeEntity.builder()
                    .commonCodeEntity(commonCode.get())
                    .pcode(commonCode.get().getCode())
                    .dcode(dcode)
                    .val(request.getCommonOrganizationCodeDto().getVal())
                    .countryCd(request.getCommonOrganizationCodeDto().getCountryCd())
                    .description(request.getCommonOrganizationCodeDto().getDescription())
                    .sortIndex(k)
                    .useType(request.getCommonOrganizationCodeDto().getUseType())
                    .popularType(request.getCommonOrganizationCodeDto().getPopularType())
                    .hit(0)
                    .build();

            CommonOrganizationCodeEntity saved = commonOrganizationCodeRepository.save(newCommonOrganizationCode);

            //seq 번호 수동으로 업데이트 시킨다
            commonOrganizationCodeRepository.updateLastSeq(pcode, k);

//            Optional<CommonOrganizationCodeEntity> CommonOrganizationCode = commonOrganizationCodeRepository.findById(saved.getCommonOrganizationCodeSeq());
//
//            Optional<CommonOrganizationCodeDto> dtoPage =
//
//                    CommonOrganizationCode
//                            .map(p -> CommonOrganizationCodeDto.builder()
//                                    .commonOrganizationCodeSeq(p.getCommonOrganizationCodeSeq())
//                                    .pcode(p.getCommonCodeEntity().getCode())
//                                    .pcodeVal(p.getCommonCodeEntity().getVal())
//                                    .dcode(p.getDcode())
//                                    .val(p.getVal())
//                                    .description(p.getDescription())
//                                    .useType(p.getUseType())
//                                    .popularType(p.getPopularType())
//                                    .hit(p.getHit())
//                                    .regUsr(p.getRegUsr())
//                                    .regDt(p.getRegDt())
//                                    .modUsr(p.getModUsr())
//                                    .modDt(p.getModDt())
//                                    .build()
//                            );
//
//            return dtoPage;

            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
//            res.setResult(dtoPage);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));

            String menuName = "";
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_CREATE.getCode());
            switch(saved.getPcode()){
                case "PRO" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_PRO.getCode());
                    break;
                case "NRO" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_NRO.getCode());
                    break;
                default:
                    menuName = "ETC CODE " + saved.getPcode();
                    break;
            }

            //netive query
            String usrName = adminActiveLogService.findQryUsrName(saved.getRegUsr());

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(saved.getRegDt())
                    .adminId(saved.getRegUsr())
                    .menuName(menuName)
                    .activeType("C")
                    .contentsId(saved.getCommonOrganizationCodeSeq())
                    .activeMsg(usrName + " " + activeMsg + " '" + saved.getVal() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e) {
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }

        return res;
    }

    @Transactional
    public Response<Void> updateYn(String pcode, Long id, HttpServletRequest req) {

        Response<Void> res = new Response<>();

        //부모코드 없을 때
        if (pcode == null) {
            res.setResultCd(ResultCodeConst.NOT_EXIST_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<CommonCodeEntity> commonCode = Optional.ofNullable(
                commonCodeRepository.findByCode(pcode)
        );

        logger.info("commonCode - {}", commonCode);

        //부모코드 조회내용 없을 때
        if (!commonCode.isPresent()) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //id값이 없을 때
        if (id == null) {
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<CommonOrganizationCodeEntity> commonOrganizationCode = commonOrganizationCodeRepository.findById(id);

        logger.info("commonOrganizationCode - {}", commonOrganizationCode);

        //조회내용 없을 때
        if (!commonOrganizationCode.isPresent()) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        YnTypeEnum useType = commonOrganizationCode.get().getUseType();
        YnTypeEnum newUseType = (useType == YnTypeEnum.Y) ? YnTypeEnum.N : YnTypeEnum.Y;

        logger.info("newUseType - {} ", newUseType);

        try {
            commonOrganizationCode.get().updateYn(newUseType);
            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            //res.setResult(songOrganizationCode);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));

            String menuName = "";
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_MODIFY.getCode());
            switch(commonOrganizationCode.get().getPcode()){
                case "PRO" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_PRO.getCode());
                    break;
                case "NRO" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_NRO.getCode());
                    break;
                default:
                    menuName = "ETC CODE " + commonOrganizationCode.get().getPcode();
                    break;
            }

            //netive query
            String usrName = adminActiveLogService.findQryUsrName(commonOrganizationCode.get().getModUsr());

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(commonOrganizationCode.get().getModDt())
                    .adminId(commonOrganizationCode.get().getModUsr())
                    .menuName(menuName)
                    .activeType("U")
                    .contentsId(commonOrganizationCode.get().getCommonOrganizationCodeSeq())
                    .activeMsg(usrName + " " + activeMsg + " '" + commonOrganizationCode.get().getVal() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e) {
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }
        return res;
    }

    @Transactional
    public Response<Void> updatePopularYn(String pcode, Long id, HttpServletRequest req) {

        Response<Void> res = new Response<>();

        //부모코드 없을 때
        if (pcode == null) {
            res.setResultCd(ResultCodeConst.NOT_EXIST_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<CommonCodeEntity> commonCode = Optional.ofNullable(
                commonCodeRepository.findByCode(pcode)
        );

        logger.info("commonCode - {}", commonCode);

        //부모코드 조회내용 없을 때
        if (!commonCode.isPresent()) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //id값이 없을 때
        if (id == null) {
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<CommonOrganizationCodeEntity> commonOrganizationCode = commonOrganizationCodeRepository.findById(id);

        logger.info("commonOrganizationCode - {}", commonOrganizationCode);

        //조회내용 없을 때
        if (!commonOrganizationCode.isPresent()) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        YnTypeEnum popularType = commonOrganizationCode.get().getPopularType();
        YnTypeEnum newType = (popularType == YnTypeEnum.Y) ? YnTypeEnum.N : YnTypeEnum.Y;

        logger.info("newUseType - {} ", newType);

        try {
            commonOrganizationCode.get().updatePopularYn(newType);
            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            //res.setResult(songOrganizationCode);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));

            String menuName = "";
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_MODIFY.getCode());
            switch(commonOrganizationCode.get().getPcode()){
                case "PRO" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_PRO.getCode());
                    break;
                case "NRO" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_NRO.getCode());
                    break;
                default:
                    menuName = "ETC CODE " + commonOrganizationCode.get().getPcode();
                    break;
            }

            //netive query
            String usrName = adminActiveLogService.findQryUsrName(commonOrganizationCode.get().getModUsr());

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(commonOrganizationCode.get().getModDt())
                    .adminId(commonOrganizationCode.get().getModUsr())
                    .menuName(menuName)
                    .activeType("U")
                    .contentsId(commonOrganizationCode.get().getCommonOrganizationCodeSeq())
                    .activeMsg(usrName + " " + activeMsg + " '" + commonOrganizationCode.get().getVal() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e) {
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }
        return res;

    }

    @Transactional
    public Response<Optional<CommonOrganizationCodeDto>> updateContent(String pcode, Long id, CommonOrganizationCodeRequest request, HttpServletRequest req) {

        Response<Optional<CommonOrganizationCodeDto>> res = new Response<>();

        //부모코드 없을 때
        if (pcode == null) {
            res.setResultCd(ResultCodeConst.NOT_VALID_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<CommonCodeEntity> commonCode = Optional.ofNullable(commonCodeRepository.findByCode(pcode));

        logger.info("commonCode - {}", commonCode);

        //부모코드 조회내용 없을 때
        if (!commonCode.isPresent()) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //id값이 없을 때
        if (id == null) {
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<CommonOrganizationCodeEntity> commonOrganizationCode = commonOrganizationCodeRepository.findById(id);

        logger.info("commonOrganizationCode - {} ", commonOrganizationCode);

        //조회내용 없을 때
        if (!commonOrganizationCode.isPresent()) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //USEYN 값 없거나 틀릴 때
        if (request.getCommonOrganizationCodeDto().getUseType() == null || (request.getCommonOrganizationCodeDto().getUseType() != YnTypeEnum.Y && request.getCommonOrganizationCodeDto().getUseType() != YnTypeEnum.N)) {
            res.setResultCd(ResultCodeConst.NOT_VALID_USEYN.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //POPULAR 값 없거나 틀릴 때
        if (request.getCommonOrganizationCodeDto().getPopularType() == null || (request.getCommonOrganizationCodeDto().getPopularType() != YnTypeEnum.Y && request.getCommonOrganizationCodeDto().getPopularType() != YnTypeEnum.N)) {
            res.setResultCd(ResultCodeConst.NOT_VALID_POPULAR.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if (request.getCommonOrganizationCodeDto().getVal() == null) {
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        try {
            logger.info("/////////////////////////////// commonOrganizationCode - asdflkajsdflkja ");


            commonOrganizationCode.get().update(request.getCommonOrganizationCodeDto().getPcode(),
                    request.getCommonOrganizationCodeDto().getDcode(),
                    request.getCommonOrganizationCodeDto().getVal(),
                    request.getCommonOrganizationCodeDto().getCountryCd(),
                    request.getCommonOrganizationCodeDto().getDescription(),
                    request.getCommonOrganizationCodeDto().getSortIndex(),
                    request.getCommonOrganizationCodeDto().getUseType(),
                    request.getCommonOrganizationCodeDto().getPopularType());

            logger.info("/////////////////////////////// commonOrganizationCode - {} ", commonOrganizationCode.toString());

            Optional<CommonOrganizationCodeEntity> commonOrganizationCodeUpdate =
                    commonOrganizationCodeRepository.findById(commonOrganizationCode.get().getCommonOrganizationCodeSeq());



            Optional<CommonOrganizationCodeDto> dtoPage =

                    commonOrganizationCodeUpdate
                            .map(p -> CommonOrganizationCodeDto.builder()
                                    .commonOrganizationCodeSeq(p.getCommonOrganizationCodeSeq())
                                    .pcode(p.getCommonCodeEntity().getCode())
                                    .pcodeVal(p.getCommonCodeEntity().getVal())
                                    .dcode(p.getDcode())
                                    .val(p.getVal())
                                    .countryCd(p.getCountryCd())
                                    //.country(p.getCountryEntity().getCountry())
                                    .useType(p.getUseType())
                                    .popularType(p.getPopularType())
                                    .description(p.getDescription())
                                    .sortIndex(p.getSortIndex())
                                    .hit(p.getHit())
                                    .regUsr(p.getRegUsr())
                                    .regDt(p.getRegDt())
                                    .modUsr(p.getModUsr())
                                    .modDt(p.getModDt())
                                    .build()
                            );

            logger.info("dtoPage - {} ", dtoPage);

            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            res.setResult(dtoPage);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));

            String menuName = "";
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_MODIFY.getCode());
            switch(commonOrganizationCode.get().getPcode()){
                case "PRO" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_PRO.getCode());
                    break;
                case "NRO" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_NRO.getCode());
                    break;
                default:
                    menuName = "ETC CODE " + commonOrganizationCode.get().getPcode();
                    break;
            }

            //netive query
            String usrName = adminActiveLogService.findQryUsrName(commonOrganizationCodeUpdate.get().getModUsr());

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(commonOrganizationCodeUpdate.get().getModDt())
                    .adminId(commonOrganizationCodeUpdate.get().getModUsr())
                    .menuName(menuName)
                    .activeType("U")
                    .contentsId(commonOrganizationCodeUpdate.get().getCommonOrganizationCodeSeq())
                    .activeMsg(usrName + " " + activeMsg + " '" + commonOrganizationCodeUpdate.get().getVal() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e) {
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }

        return res;
    }

    @Transactional
    public Response<Void> deleteContent(String pcode, Long id, HttpServletRequest req) {

        Response<Void> res = new Response<>();

        //부모코드 없을 때
        if (pcode == null) {
            res.setResultCd(ResultCodeConst.NOT_EXIST_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<CommonCodeEntity> commonCode = Optional.ofNullable(
                commonCodeRepository.findByCode(pcode)
        );

        logger.info("commonCode - {} ", commonCode);

        //부모코드 조회내용 없을 때
        if (!commonCode.isPresent()) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //id값이 없을 때
        if (id == null) {
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<CommonOrganizationCodeEntity> commonOrganizationCode = commonOrganizationCodeRepository.findById(id);

        logger.info("commonOrganizationCode - {} ", commonOrganizationCode);

        //조회내용 없을 때
        if (!commonOrganizationCode.isPresent()) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        try {
            commonOrganizationCodeRepository.deleteById(id);
            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
//            res.setResult(dtoPage);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));
            Long adminId = getAccountId.ofId(httpServletRequest);

            String menuName = "";
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_DELETE.getCode());
            switch(commonOrganizationCode.get().getPcode()){
                case "PRO" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_PRO.getCode());
                    break;
                case "NRO" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_NRO.getCode());
                    break;
                default:
                    menuName = "ETC CODE " + commonOrganizationCode.get().getPcode();
                    break;
            }

            //netive query
            String usrName = adminActiveLogService.findQryUsrName(adminId);

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(LocalDateTime.now())
                    .adminId(adminId)
                    .menuName(menuName)
                    .activeType("D")
                    .contentsId(commonOrganizationCode.get().getCommonOrganizationCodeSeq())
                    .activeMsg(usrName + " " + activeMsg + " '" + commonOrganizationCode.get().getVal() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e) {
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }
        return res;

    }

    public List<CommonOrganizationCodeExcelDto> getExcelList(String pcode) {

        List<CommonOrganizationCodeExcelDto> getExcelList = commonOrganizationCodeRepository.findExcelList(pcode);

        logger.info("getExcelList - {} ", getExcelList);

        return getExcelList;
    }

    //엑셀 다운로드 요청
    public Map<String, Object> downExcelList(String commonCode) throws Exception {
        // 데이터 가져오기
        List<CommonOrganizationCodeExcelDto> excelList = this.getExcelList(commonCode);

        logger.info("excelList - {} ", excelList);

        return ExcelWriter.createExcelData(excelList, CommonOrganizationCodeExcelDto.class);
    }

    //마지막 코드 번호 확인
    public Integer getLastSeq(String code) {

        return commonOrganizationCodeRepository.getLastSeq(code);
    }

    //코드 번호 업데이트
    @Transactional
    public void updateLastSeq(String code, Integer k) {

        commonOrganizationCodeRepository.updateLastSeq(code, k);
    }

    //이름으로 코드검색
    public Long getOneName(String val) {

        return commonOrganizationCodeRepository.countByVal(val);
    }

    @Transactional
    public Response<Void> saveExcelContent(String pcode, CommonOrganizationCodeDto commonOrganizationCodeDto, HttpServletRequest req) {

        Response<Void> res = new Response<>();

        Optional<CommonCodeEntity> commonCode = Optional.ofNullable(commonCodeRepository.findByCode(pcode));

        logger.info("commonCode - {}", commonCode);

        //부모코드 조회내용 없을 때
        if (!commonCode.isPresent() || commonCode.equals("")) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<CountryEntity> country = Optional.ofNullable(countryRepository.findByCountry(commonOrganizationCodeDto.getCountry()));

        logger.info("country - {}", country);

        //국가코드 조회내용 없을 때
        if (!country.isPresent()) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //USEYN 값 없거나 틀릴 때
        if (commonOrganizationCodeDto.getUseType() == null || (commonOrganizationCodeDto.getUseType() != YnTypeEnum.Y && commonOrganizationCodeDto.getUseType() != YnTypeEnum.N)) {
            res.setResultCd(ResultCodeConst.NOT_VALID_USEYN.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //POPULAR 값 없거나 틀릴 때
        if (commonOrganizationCodeDto.getPopularType() == null || (commonOrganizationCodeDto.getPopularType() != YnTypeEnum.Y && commonOrganizationCodeDto.getPopularType() != YnTypeEnum.N)) {
            res.setResultCd(ResultCodeConst.NOT_VALID_POPULAR.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if (commonOrganizationCodeDto.getVal() == null || commonOrganizationCodeDto.getVal().equals("")) {
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        try {

            CommonOrganizationCodeEntity newCommonOrganizationCode = CommonOrganizationCodeEntity.builder()
                    .commonCodeEntity(commonCode.get())
                    //.countryEntity(country.get())
                    .pcode(commonCode.get().getCode())
                    .dcode(commonOrganizationCodeDto.getDcode())
                    .val(commonOrganizationCodeDto.getVal())
                    .countryCd(country.get().getCountryCd())
                    .description(commonOrganizationCodeDto.getDescription())
                    .sortIndex(commonOrganizationCodeDto.getSortIndex())
                    .useType(commonOrganizationCodeDto.getUseType())
                    .popularType(YnTypeEnum.N)
                    .hit(0)
                    .build();

            CommonOrganizationCodeEntity saved = commonOrganizationCodeRepository.save(newCommonOrganizationCode);

            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
//            res.setResult(dtoPage);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));

            String menuName = "";
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_CREATE.getCode());
            switch(saved.getPcode()){
                case "PRO" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_PRO.getCode());
                    break;
                case "NRO" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_NRO.getCode());
                    break;
                default:
                    menuName = "ETC CODE " + saved.getPcode();
                    break;
            }

            //netive query
            String usrName = adminActiveLogService.findQryUsrName(saved.getRegUsr());

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(saved.getRegDt())
                    .adminId(saved.getRegUsr())
                    .menuName(menuName)
                    .activeType("C")
                    .contentsId(saved.getCommonOrganizationCodeSeq())
                    .activeMsg(usrName + " " + activeMsg + " '" + saved.getVal() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e) {
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }

        return res;
    }
}
