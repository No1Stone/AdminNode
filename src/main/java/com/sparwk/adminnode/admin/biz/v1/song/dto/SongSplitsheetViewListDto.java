package com.sparwk.adminnode.admin.biz.v1.song.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class SongSplitsheetViewListDto {

    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long songId;
    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long profileId;
    @Schema(description = "SONG TITLE", nullable = false, example = "SONG_TITLE")
    private String fullName;
    @Schema(description = "AVATAR_FILE_URL", nullable = false, example = "http://")
    private String ipiInfo;
    @Schema(description = "SONG OWNER NAME", nullable = true, example = "OWNER PROFILE ID")
    private String caeInfo;
    @Schema(description = "PROJECT TITLE", nullable = false, example = "PROJ_TITLE")
    private String isniInfo;
    @Schema(description = "SONG GENRE TYPE", nullable = true, example = "SGG000008,SGG000009")
    private String ipnInfo;
    @Schema(description = "SONG GENRE TYPE Name", nullable = true, example = "HipHop,Jazz")
    private String roleName;
    @Schema(description = "SONG SUB GENRE TYPE", nullable = true, example = "SGS000001")
    private String rateShare;
    @Schema(description = "SONG SUB GENRE TYPE Name", nullable = true, example = "K-POP")
    private String originalPublisher;
    @Schema(description = "SONG VERSION CODE", nullable = true, example = "SGV000001")
    private String subPublisher;
    @Schema(description = "SONG VERSION CODE Name", nullable = true, example = "OrigialVersion")
    private String pro;

    @Builder
    public SongSplitsheetViewListDto(Long songId,
                                     Long profileId,
                                     String fullName,
                                     String ipiInfo,
                                     String caeInfo,
                                     String isniInfo,
                                     String ipnInfo,
                                     String roleName,
                                     String rateShare,
                                     String originalPublisher,
                                     String subPublisher,
                                     String pro
    ) {
        this.songId = songId;
        this.profileId = profileId;
        this.fullName = fullName;
        this.ipiInfo = ipiInfo;
        this.caeInfo = caeInfo;
        this.isniInfo = isniInfo;
        this.ipnInfo = ipnInfo;
        this.roleName = roleName;
        this.rateShare = rateShare;
        this.originalPublisher = originalPublisher;
        this.subPublisher = subPublisher;
        this.pro = pro;
    }
}
