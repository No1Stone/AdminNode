package com.sparwk.adminnode.admin.biz.v1.test.service;

import com.google.gson.Gson;
import com.sparwk.adminnode.admin.biz.v1.board.dto.BoardQnaEmailDto;
import com.sparwk.adminnode.admin.biz.v1.test.dto.AdminEmailSendRequest;
import com.sparwk.adminnode.admin.biz.v1.test.dto.TestDto;
import com.sparwk.adminnode.admin.biz.v1.test.dto.TestEmailDto;
import com.sparwk.adminnode.admin.jpa.entity.test.TestEntity;
import com.sparwk.adminnode.admin.jpa.repository.test.TestRepository;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.nio.charset.Charset;
import java.util.Optional;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class TestService {

    private final TestRepository testRepository;
    private final RestTemplate restTemplate;
    private final Logger logger = LoggerFactory.getLogger(TestService.class);


    @Transactional
    public Optional<TestDto> saveContent(TestDto request, HttpServletRequest req) {

        TestEntity newTest = TestEntity.builder()
                .ckey(request.getCkey())
                .cval(request.getCval())
                .build();

        TestEntity saved = testRepository.save(newTest);

        Optional<TestEntity> test = testRepository.findById(saved.getTestId());

        Optional<TestDto> dtoPage =

                test
                        .map(p -> TestDto.builder()
                                .ckey(p.getCkey())
                                .cval(p.getCval())
                                .build()
                        );

        return dtoPage;
    }


    //param으로 보내는 서버간 통신 테스트
    @Transactional
    public TestEmailDto testSend(TestEmailDto request) {

//        TestEmailDto newTest = TestEmailDto.builder()
//                .userEmail(request.getUserEmail())
//                .emailTitle(request.getEmailTitle())
//                .emailBody(request.getEmailBody())
//                .build();
        System.out.println("-----------------------------------------------------------------------");
        logger.info("request : {}", request);
        logger.info("getUserEmail : {}", request.getUserEmail());
        logger.info("getEmailTitle : {}", request.getEmailTitle());
        logger.info("getEmailBody : {}", request.getEmailBody());
        System.out.println("-----------------------------------------------------------------------");

        //URI를 빌드한다
        URI uri = UriComponentsBuilder
                .fromUriString("http://192.168.0.37:8090")
                .path("/admin/V1/setting/identifire/hello2")
                .queryParam("userEmail", request.getUserEmail())
                .queryParam("emailTitle", request.getEmailTitle())
                .queryParam("emailBody", request.getEmailBody())
                .encode(Charset.defaultCharset())
                .build()
                .toUri();
        System.out.println(uri.toString());

        RestTemplate restTemplate = new RestTemplate();

        //String result = restTemplate.getForObject(uri, String.class);
        //getForEntity는 응답을 ResponseEntity로 받을 수 있도록 해준다 .
        //파라미터 첫번째는 요청 URI 이며 , 2번째는 받을 타입
        ResponseEntity<TestEmailDto> result = restTemplate.getForEntity(uri, TestEmailDto.class);

        System.out.println(result.getStatusCode());
        System.out.println(result.getBody());

        return result.getBody();
    }

    //post로 보내는 서버간 통신 테스트
    @Transactional
    public TestEmailDto testPostSend() {
        // http://192.168.0.37:8090/admin/V1/setting/identifire/hello-post2

        URI uri = UriComponentsBuilder
                .fromUriString("http://192.168.0.37:8090")
                .path("/admin/V1/setting/identifire/hello-post2")
                .encode(Charset.defaultCharset())
                .build()
                // http://localhost:9090/api/server/user/{userId}/name/{username} 일 때
                //.expand("100","ugo")  //pathVariable사용을 위한 메소드 순서대로 들어간다.
                .toUri();

        //아래 순서로 변환
        //http body - object - object mapper -> json - > http body json
        TestEmailDto testEmailDto = new TestEmailDto();
        String[] userEmail = {"jiyaman@naver.com"};
        testEmailDto.setUserEmail(userEmail);
        testEmailDto.setEmailTitle("포스트 테스트 타이틀입니다");
        testEmailDto.setEmailBody("포스트 테스트 바디입니다");

        RestTemplate restTemplate = new RestTemplate();

        //post 의 경우 PostForEntity를 사용한다. 파라미터 1 요청 주소 , 2 요청 바디 , 3 응답 바디 Class
        ResponseEntity<TestEmailDto> response = restTemplate.postForEntity(uri, testEmailDto, TestEmailDto.class);

        System.out.println("-----------------------------------------------------------------------");
        logger.info("getStatusCode : {}", response.getStatusCode());
        logger.info("getHeaders : {}", response.getHeaders());
        logger.info("getBody : {}", response.getBody());
        System.out.println("-----------------------------------------------------------------------");

        return response.getBody();
    }

    //post로 보내는 서버간 통신 테스트
    @Transactional
    public TestEmailDto testPostSend3(TestEmailDto testEmailDto) {
        // http://192.168.0.37:8090/admin/V1/setting/identifire/hello-post2

        URI uri = UriComponentsBuilder
                .fromUriString("http://192.168.0.37:8090")
                .path("/admin/V1/setting/identifire/hello-post2")
                .encode(Charset.defaultCharset())
                .build()
                // http://localhost:9090/api/server/user/{userId}/name/{username} 일 때
                //.expand("100","ugo")  //pathVariable사용을 위한 메소드 순서대로 들어간다.
                .toUri();

        //아래 순서로 변환
        //http body - object - object mapper -> json - > http body json
        RestTemplate restTemplate = new RestTemplate();

        //post 의 경우 PostForEntity를 사용한다. 파라미터 1 요청 주소 , 2 요청 바디 , 3 응답 바디 Class
        ResponseEntity<TestEmailDto> response = restTemplate.postForEntity(uri, testEmailDto, TestEmailDto.class);

        System.out.println("-----------------------------------------------------------------------");
        logger.info("testEmailDto : {}", testEmailDto.getUserEmail());
        logger.info("getStatusCode : {}", response.getStatusCode());
        logger.info("getHeaders : {}", response.getHeaders());
        logger.info("getBody : {}", response.getBody());
        System.out.println("-----------------------------------------------------------------------");

        return response.getBody();
    }

    //post로 보내는 서버간 통신 테스트
    @Transactional
    public BoardQnaEmailDto testPostEmailTest(BoardQnaEmailDto boardQnaEmailDto) {
        // http://192.168.0.37:8090/admin/V1/setting/identifire/hello-post2

        URI uri = UriComponentsBuilder
                .fromUriString("http://externalconnect.sparwkdev.com")
                .path("/mailing/V1/admin/send")
                .encode(Charset.defaultCharset())
                .build()
                // http://localhost:9090/api/server/user/{userId}/name/{username} 일 때
                //.expand("100","ugo")  //pathVariable사용을 위한 메소드 순서대로 들어간다.
                .toUri();

        //아래 순서로 변환
        //http body - object - object mapper -> json - > http body json
        RestTemplate restTemplate = new RestTemplate();

        //post 의 경우 PostForEntity를 사용한다. 파라미터 1 요청 주소 , 2 요청 바디 , 3 응답 바디 Class
        ResponseEntity<BoardQnaEmailDto> response = restTemplate.postForEntity(uri, boardQnaEmailDto, BoardQnaEmailDto.class);

        System.out.println("-----------------------------------------------------------------------");
        logger.info("getStatusCode : {}", response.getStatusCode());
        logger.info("getHeaders : {}", response.getHeaders());
        logger.info("getBody : {}", response.getBody());
        System.out.println("-----------------------------------------------------------------------");

        return response.getBody();
    }


    public void EmailSend2(AdminEmailSendRequest entity) {

        String schema = "http";
        String host = "192.168.0.42:8120/mailing";
        String path = "/V1/admin/send";
        HttpHeaders headers = new HttpHeaders();
        headers.set("Auth","test");
        UriComponentsBuilder builder =
                UriComponentsBuilder
                        .newInstance()
                        .scheme(schema)// http & https
                        .host(host) //localhost:8080
                        .path(path); //V1/profile/info
        UriComponents uriComponents = builder.build();


        String html = "<html>";
        html += "<h1>테스트 제목입니다!<h1>";
        html += "<h2>질문입니다</h2>";
        html += "<h2>" + entity.getContents() + "</h2>";
        html += "</html>";

        entity.setContents(html);


        logger.info("URI COMPONENT tostring - {}", uriComponents.toString());
        logger.info(" - - - - - - 1 : {}", entity);
        HttpEntity<AdminEmailSendRequest> responsEntity = new HttpEntity<>(entity, headers);
        logger.info(" - - - - - - 2 : {}", responsEntity.toString());
        logger.info(" - - - - - - 3 : {}", uriComponents.toUriString());
//        ResponseEntity<String> resultResponse =
//                restTemplate.postForEntity(uriComponents.toUriString(), responsEntity, String.class);
        ResponseEntity<String> resultResponse =
                restTemplate.postForEntity(uriComponents.toUriString(), responsEntity, String.class);
        logger.info("asdasdasd - 4 : {}", resultResponse.toString());
//        JSONObject obj = new JSONObject(resultResponse.getBody());
//        logger.info("object - 5 : {}", obj.toString());
        Gson gson = new Gson();
//        Response response = gson.fromJson(String.valueOf(obj), Response.class);
//        logger.info("--------Respoonse {}", response.getResultCd());
    }

//    public void EmailSend3(AdminEmailSendRequest entity) {
//
//        String schema = "http";
//        String host = "192.168.0.42:8120/mailing";
//        String path = "/V1/admin/send";
//        HttpHeaders headers = new HttpHeaders();
//        headers.set("Auth","test");
//        UriComponentsBuilder builder =
//                UriComponentsBuilder
//                        .newInstance()
//                        .scheme(schema)// http & https
//                        .host(host) //localhost:8080
//                        .path(path); //V1/profile/info
//        UriComponents uriComponents = builder.build();
//        logger.info("URI COMPONENT tostring - {}", uriComponents.toString());
//        HttpEntity<AdminEmailSendRequest> responsEntity = new HttpEntity<>(entity, headers);
//        ResponseEntity<String> resultResponse =
//                restTemplate.postForEntity(uriComponents.toUriString(), responsEntity, String.class);
//        JSONObject obj = new JSONObject(resultResponse.getBody());
//        logger.info("object - {}", obj.toString());
//        Gson gson = new Gson();
//        Response response = gson.fromJson(String.valueOf(obj), Response.class);
//        logger.info("--------Respoonse {}", response.getResultCd());
//    }

}
