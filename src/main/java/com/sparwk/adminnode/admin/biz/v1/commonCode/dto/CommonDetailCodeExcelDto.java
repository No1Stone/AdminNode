package com.sparwk.adminnode.admin.biz.v1.commonCode.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sparwk.adminnode.admin.jpa.entity.ExcelColumnName;
import com.sparwk.adminnode.admin.jpa.entity.ExcelDto;
import com.sparwk.adminnode.admin.jpa.entity.ExcelFileName;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Arrays;
import java.util.List;

@Data
@NoArgsConstructor
@ExcelFileName(filename = "Common_Code")
public class CommonDetailCodeExcelDto implements ExcelDto {

    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    @ExcelColumnName(headerName = "Common Detail Code Seq")
    private Long commonDetailCodeSeq;

    @Schema(description = "부모코드 3자리", nullable = false, example = "ENC")
    @ExcelColumnName
    @JsonProperty("pcode")
    private String pcode;

    @Schema(description = "부모코드 값", nullable = true, example = "Encoding")
    @ExcelColumnName
    @JsonProperty("pcodeVal")
    private String pcodeVal;

    @Schema(description = "코드 9자리", nullable = false, example = "ENC000001")
    @ExcelColumnName
    @JsonProperty("dcode")
    private String dcode;

    @Schema(description = "코드 값", nullable = true, example = "UTF-8")
    @ExcelColumnName
    @JsonProperty("val")
    private String val;

    @Schema(description = "코드 사용여부 Y/N", nullable = false, defaultValue = "Y", allowableValues = {"Y", "N"})
    @ExcelColumnName
    @JsonProperty("useType")
    private YnTypeEnum useType;

    @Schema(description = "popular 사용 Y/N", nullable = false, defaultValue = "Y", allowableValues = {"Y", "N"})
    @ExcelColumnName
    @JsonProperty("popularType")
    private YnTypeEnum popularType;

    @Schema(description = "description 설명", nullable = true, example = "설명")
    @ExcelColumnName
    @JsonProperty("description")
    private String description;

    @Override
    public List<String> mapToList() {
        return Arrays.asList(
                String.valueOf(commonDetailCodeSeq),
                pcode,
                pcodeVal,
                dcode,
                val,
                String.valueOf(useType),
                String.valueOf(popularType),
                description
        );
    }
}
