package com.sparwk.adminnode.admin.biz.v1.currency.dto;

import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Setter
@Getter
@NoArgsConstructor
public class CurrencyDto {

    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long currencySeq;
    @Schema(description = "화폐명", nullable = false, example = "Euro")
    private String currency;
    @Schema(description = "코드", nullable = false, example = "CUR000001")
    private String currencyCd;
    @Schema(description = "ISO 화폐 코드", nullable = false, example = "EUR")
    private String isoCode;
    @Schema(description = "화폐표식", nullable = false, example = "$")
    private String symbol;
    @Schema(description = "사용국가 또는 그룹", nullable = true, example = "Åland Islands (AX), European Union (EU), Andorra (AD), Austria (AT), Belgium (BE), Cyprus (CY), Estonia (EE), Finland (FI), France (FR), French Southern and Antarctic Lands (TF), Germany (DE), Greece (GR), Guadeloupe (GP), Ireland (IE), Italy (IT), Latvia (LV), Lithuania (LT), Luxembourg (LU), Malta (MT), French Guiana (GF), Martinique (MQ), Mayotte (YT), Monaco (MC), Montenegro (ME), Netherlands (NL), Portugal (PT), Réunion (RE), Saint Barthélemy (BL), Saint Martin (MF), Saint Pierre and Miquelon (PM), San Marino (SM), Slovakia (SK), Slovenia (SI), Spain (ES), Vatican City (VA)")
    private String country;
    @Schema(description = "코드 사용여부 Y/N", nullable = false, defaultValue = "Y", allowableValues = {"Y", "N"})
    private YnTypeEnum useType;
    @Schema(description = "등록일", pattern = "yyMMdd hh:mm:ss", example = "2021-12-08T05:44:26.627991")
    private LocalDateTime regDt;
    @Schema(description = "등록자", nullable = false, example = "10000001")
    private Long regUsr;
    @Schema(description = "등록자 이름", nullable = false, example = "Yoon Jung Sun")
    private String regUsrName;
    @Schema(description = "수정일", pattern = "yyMMdd hh:mm:ss", example = "2021-12-08T05:44:26.627991")
    private LocalDateTime modDt;
    @Schema(description = "수정자", nullable = false, example = "10000001")
    private Long modUsr;
    @Schema(description = "수정자 이름", nullable = false, example = "Yoon Jung Sun")
    private String modUsrName;

    @Builder
    public CurrencyDto(Long currencySeq,
                       String currency,
                       String currencyCd,
                       String isoCode,
                       String symbol,
                       String country,
                       YnTypeEnum useType,
                       LocalDateTime regDt,
                       Long regUsr,
                       String regUsrName,
                       LocalDateTime modDt,
                       Long modUsr,
                       String modUsrName
    ) {
        this.currencySeq = currencySeq;
        this.currency = currency;
        this.currencyCd = currencyCd;
        this.isoCode = isoCode;
        this.symbol = symbol;
        this.country = country;
        this.useType = useType;
        this.regDt = regDt;
        this.regUsr = regUsr;
        this.regUsrName = regUsrName;
        this.modDt = modDt;
        this.modUsr = modUsr;
        this.modUsrName = modUsrName;
    }
}
