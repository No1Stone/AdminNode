package com.sparwk.adminnode.admin.biz.v1.notiDateSet.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReceiveNotification {


    private String notiCode;
    private List<String> toUsers;
    private String notiOwner;

    //노티를 보낼때 보여질 메인 이미지를 검색할수 있는 PK
    private Map<String, String> data;
    //data.put("profileId", String.valueOf(repository.get("profileId")));
    //data.put("profileCompanyId", String.valueOf(repository.get("profileCompanyId")));
    //data.put("projId", String.valueOf(repository.get("projId")));
    //data.put("songId", String.valueOf(repository.get("songId")));
}
