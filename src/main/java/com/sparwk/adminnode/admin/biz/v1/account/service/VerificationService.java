package com.sparwk.adminnode.admin.biz.v1.account.service;

import com.sparwk.adminnode.admin.biz.v1.CommonPagingListDto;
import com.sparwk.adminnode.admin.biz.v1.account.dto.*;
import com.sparwk.adminnode.admin.biz.v1.admin.service.AdminActiveLogService;
import com.sparwk.adminnode.admin.biz.v1.notiDateSet.service.NotiDataSetService;
import com.sparwk.adminnode.admin.biz.v1.profile.dto.ProfileCompanyContactDto;
import com.sparwk.adminnode.admin.biz.v1.profile.dto.ProfileCurrentPositionDto;
import com.sparwk.adminnode.admin.biz.v1.profile.dto.ProfileMetaDataDto;
import com.sparwk.adminnode.admin.config.common.ActiveCodeConst;
import com.sparwk.adminnode.admin.config.common.GetAccountId;
import com.sparwk.adminnode.admin.config.common.Response;
import com.sparwk.adminnode.admin.config.common.ResultCodeConst;
import com.sparwk.adminnode.admin.jpa.entity.PeriodTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.account.*;
import com.sparwk.adminnode.admin.jpa.entity.admin.AdminActiveLogEntity;
import com.sparwk.adminnode.admin.jpa.entity.country.CountryEntity;
import com.sparwk.adminnode.admin.jpa.entity.profile.ProfileCompanyEntity;
import com.sparwk.adminnode.admin.jpa.entity.profile.ProfileEntity;
import com.sparwk.adminnode.admin.jpa.repository.account.*;
import com.sparwk.adminnode.admin.jpa.repository.country.CountryRepository;
import com.sparwk.adminnode.admin.jpa.repository.profile.ProfileCompanyRepository;
import com.sparwk.adminnode.admin.jpa.repository.profile.ProfileRepository;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class VerificationService {

    private final AccountRepository accountRepository;
    private final AccountIndividualViewRepository accountIndividualViewRepository;
    private final AccountPassportRepository accountPassportRepository;
    private final AccountCompanyRepository accountCompanyRepository;
    private final AccountCompanyViewRepository accountCompanyViewRepository;
    private final ProfileRepository profileRepository;
    private final ProfileCompanyRepository profileCompanyRepository;
    private final CountryRepository countryRepository;
    private final NotiDataSetService notiDataSetService;
    private final AdminActiveLogService adminActiveLogService;

    private final HttpServletRequest httpServletRequest;
    private final MessageSourceAccessor messageSourceAccessor;
    
    private final Logger logger = LoggerFactory.getLogger(VerificationService.class);

    ///// user ///////////////////////////////////////////////////////////////////////////////////////////
    //인증 리스트
    public Response<CommonPagingListDto> getVerificationUserList(VerificationIndividualAccountCateEnum cate,
                                                               String val,
                                                               PeriodTypeEnum periodType,
                                                               String sdate,
                                                               String edate,
                                                               VerificationIndividualAccountFilterEnum filter,
                                                               VerificationIndividualAccountSorterEnum sorter,
                                                               PageRequest pageRequest) {

        Response<CommonPagingListDto> res = new Response<>();

        logger.info("accoList22222 - {}");

        //검색 시 회사로 조회. 포함 안되면 해당 내용 삭제
        List<IndividualMemberViewListDto> accoList = accountIndividualViewRepository.findQryVerificationMember(cate,
                val,
                periodType,
                sdate,
                edate,
                filter,
                sorter,
                pageRequest);

        logger.info("accoList - {}", accoList);

        //조회내용 없을 때
        if(accoList == null || accoList.size() < 1){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        CommonPagingListDto list = new CommonPagingListDto();

        //총 카운트
        long totalSize = accountIndividualViewRepository.countQryVerificationMember(cate,
                                                                                    val,
                                                                                    periodType,
                                                                                    sdate,
                                                                                    edate,
                                                                                    filter);
        long totalPage = totalSize / pageRequest.getPageSize();
        long mod = totalSize % pageRequest.getPageSize();
        if(mod > 0) {
            totalPage += 1;
        }

        list.setList(accoList);
        list.setSize(pageRequest.getPageSize());
        list.setPage(pageRequest.getPageNumber());
        list.setTotalPage(totalPage);
        list.setTotalSize(totalSize);

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(list);
        return res;
    }

    //인원 상세페이지
    public Response<Optional<IndividualMemberDetailDto>> getUserOne(Long id) {

        Response<Optional<IndividualMemberDetailDto>> res = new Response<>();

        //id값이 없을 때
        if(id == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<ProfileEntity> profile = profileRepository.findById(id);

        logger.info("profile - {}", profile);

        //조회내용 없을 때
        if(!profile.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<AccountEntity> account = accountRepository.findById(profile.get().getAccntId());

        logger.info("account - {}", account);

        //조회내용 없을 때
        if(!account.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<AccountPassportDto> accountPassport = accountRepository.finQryPassport(profile.get().getAccntId());

        logger.info("accountPassport - {}", accountPassport);

        //조회내용 없을 때
        if(!accountPassport.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        String accountPassportArr[] = accountPassport.get().getPassportImgFileUrl().split("/");
        String accountPassportFile = accountPassportArr[accountPassportArr.length - 1];
        //
        Optional<ProfileMetaDataDto> getCurrentCityCountryCdName = accountRepository.findQryCurrentCountry(profile.get().getProfileId());
        //
        Optional<ProfileMetaDataDto> getHometownCityCountryCdName = accountRepository.findQryHomeTownCountry(profile.get().getProfileId());
        //
        Optional<ProfileMetaDataDto> getPhoneCountryCdName = accountRepository.findQryPhoneCountryCdName(profile.get().getAccntId());


        //젠더 리스트
        List<ProfileMetaDataDto> getGenderMetaData = accountRepository.findQryGender(profile.get().getProfileId());
        //랭귀지 리스트
        List<ProfileMetaDataDto> getLanguageMetaData = accountRepository.findQryLanuage(profile.get().getProfileId());
        logger.info("getLanguageMetaData - {}", getLanguageMetaData);
        //roles 리스트
        List<ProfileMetaDataDto> getRoleMetaData = accountRepository.findQryRoles(profile.get().getProfileId());
        logger.info("getRoleMetaData - {}", getRoleMetaData);

        Optional<IndividualMemberDetailDto> dtoPage =

                profile.map(p -> IndividualMemberDetailDto.builder()
                            .accntId(p.getAccntId())
                            .profileId(p.getProfileId())
                            .accntTypeCd(account.get().getAccntTypeCd())
                            .accntFirstName(accountPassport.get().getPassportFirstName())
                            .accntMiddleName(accountPassport.get().getPassportMiddleName())
                            .accntLastName(accountPassport.get().getPassportLastName())
                            .profileFullName(p.getFullName())
                            .stageNameYn(p.getStageNameYn())
                            .accntEmail(account.get().getAccntEmail())
                            .bthYear(p.getBthYear())
                            .bthMonth(p.getBthMonth())
                            .bthDay(p.getBthDay())
                            .roleList(getRoleMetaData)
                            .genderList(getGenderMetaData)
                            .languageList(getLanguageMetaData)
                            .currentCityCountryCd(p.getCurrentCityCountryCd())
                            .currentCityCountryCdName(getCurrentCityCountryCdName.get().getAttrDtlCdVal())
                            .currentCityNm(p.getCurrentCityNm())
                            .hometownCountryCd(p.getHomeTownCountryCd())
                            .hometownCountryCdName(getHometownCityCountryCdName.get().getAttrDtlCdVal())
                            .hometownCityNm(p.getHomeTownNm())
                            .passportCountryCd(accountPassport.get().getPassportCountryCd())
                            .passportCountryCdName(accountPassport.get().getPassportCountryCdName())
                            .passportPhotocopy(accountPassport.get().getPassportImgFileUrl())
                            .passportfile(accountPassportFile)
                            .passportVerifyYn(accountPassport.get().getPassportVerifyYn())
                            .accountVerifyYn(account.get().getVerifyYn())
                            .phoneCountryCd(account.get().getCountryCd())
                            .phoneCountryCdName(getPhoneCountryCdName.get().getAttrDtlCdVal())
                            .phoneNumber(account.get().getPhoneNumber())
                            .accountVerifyPhoneYn(account.get().getVerifyPhoneYn())
                            .ipiInfo(profile.get().getIpiInfo())
                            .ipiVerifyYn(profile.get().getVerifyYn())
                            .regDt(p.getRegDt())
                            .build()
                );

        logger.info("IndividualMemberDetailDto - {}", dtoPage);

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(dtoPage);
        return res;
    }

    //인원 여권 인증
    @Transactional
    public Response<Void> updateUserVerifyYn(Long id, String verifyPassportYn, HttpServletRequest req) {

        Response<Void> res = new Response<>();

        //id값이 없을 때
        if (id == null) {
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<ProfileEntity> profile = profileRepository.findById(id);

        //조회내용 없을 때
        if (!profile.isPresent()) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //account_id 구하기
        Optional<AccountPassportEntity> accountPassport = accountPassportRepository.findById(profile.get().getAccntId());

        logger.info("accountPassport - {}", accountPassport);

        //조회내용 없을 때
        if (!accountPassport.isPresent()) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }


        YnTypeEnum useType = (verifyPassportYn.equals("Y")) ? YnTypeEnum.Y : YnTypeEnum.N;
        //String newUseType = (useType.equals("Y")) ? "Y" : "N";

        logger.info("useType - {} ", useType);

        try {
            accountPassport.get().updatePassportVerifyYn(useType);
            //여권 인증
            notiDataSetService.NotiTestCode001(id);
            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            //res.setResult(songDetailCode);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));

            String menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_VERIFIED_MEMBER.getCode());
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_MODIFY.getCode());

            //netive query
            String usrName = adminActiveLogService.findQryUsrName(profile.get().getModUsr());

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(profile.get().getModDt())
                    .adminId(profile.get().getModUsr())
                    .menuName(menuName)
                    .activeType("U")
                    .contentsId(profile.get().getProfileId())
                    .activeMsg(usrName + " " + activeMsg + " '" + "Passport Verication " + useType.getYnEnumType() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e) {
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }
        return res;
    }

    //인원 Ipi 인증
    @Transactional
    public Response<Void> updateUserIpIYn(Long id, String verifyIpiYn, HttpServletRequest req) {

        Response<Void> res = new Response<>();

        //id값이 없을 때
        if (id == null) {
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<ProfileEntity> profile = profileRepository.findById(id);

        logger.info("profile - {}", profile);

        //조회내용 없을 때
        if (!profile.isPresent()) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }


        //String useType = verifyIpiYn;
        YnTypeEnum newUseType = (verifyIpiYn.equals("Y")) ? YnTypeEnum.Y : YnTypeEnum.N;

        logger.info("newUseType - {} ", newUseType);

        try {
            profile.get().updateVerifyYn(newUseType);
            //IPI Name Number 인증 어드민
            notiDataSetService.NotiTestCode004(id);
            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            //res.setResult(songDetailCode);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));

            String menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_VERIFIED_MEMBER.getCode());
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_MODIFY.getCode());

            //netive query
            String usrName = adminActiveLogService.findQryUsrName(profile.get().getModUsr());

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(profile.get().getModDt())
                    .adminId(profile.get().getModUsr())
                    .menuName(menuName)
                    .activeType("U")
                    .contentsId(profile.get().getProfileId())
                    .activeMsg(usrName + " " + activeMsg + " '" + "IPI Verication " + newUseType.getYnEnumType() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e) {
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }
        return res;
    }

    ///// user ///////////////////////////////////////////////////////////////////////////////////////////

    ///// company ///////////////////////////////////////////////////////////////////////////////////////////
    //인증 리스트


   public Response<CommonPagingListDto> getVerificationCompanyList(VerificationCompanyAccountCateEnum cate,
                                                                     String val, PeriodTypeEnum periodType, String sdate, String edate, VerificationCompanyAccountFilterEnum filter, VerificationCompanyAccountSorterEnum sorter, PageRequest pageRequest) {


        Response<CommonPagingListDto> res = new Response<>();

        //검색 시 회사로 조회. 포함 안되면 해당 내용 삭제
        List<CompanyMemberViewListDto> accoList = accountCompanyViewRepository.findQryVerificationCompany(cate,
                val,
                periodType,
                sdate,
                edate,
                filter,
                sorter,
                pageRequest);

        logger.info("accoList - {}", accoList);

        //조회내용 없을 때
        if(accoList == null || accoList.size() < 1){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }
        CommonPagingListDto list = new CommonPagingListDto();

        //총 카운트
        long totalSize = accountCompanyViewRepository.countQryVerificationCompany(cate,
                                                                                val,
                                                                                periodType,
                                                                                sdate,
                                                                                edate,
                                                                                filter);
        long totalPage = totalSize / pageRequest.getPageSize();
        long mod = totalSize % pageRequest.getPageSize();
        if(mod > 0) {
            totalPage += 1;
        }
        list.setList(accoList);
        list.setSize(pageRequest.getPageSize());
        list.setPage(pageRequest.getPageNumber());
        list.setTotalPage(totalPage);
        list.setTotalSize(totalSize);

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(list);
        return res;
    }

    //회사 상세페이지
    public Response<Optional<CompanyVerifyDetailDto>> getCompanyOne(Long id) {

        Response<Optional<CompanyVerifyDetailDto>> res = new Response<>();

        //id값이 없을 때
        if(id == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<ProfileCompanyEntity> profile =  profileCompanyRepository.findById(id);

        logger.info("profile - {}", profile);

        //조회내용 없을 때
        if(!profile.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<AccountCompanyDetailEntity> account = accountCompanyRepository.findById(profile.get().getAccntId());

        logger.info("account - {}", account);

        //조회내용 없을 때
        if(!account.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //회사 타입 추가
        List<AccountCompanyTypeDto> companyTypeList = accountCompanyRepository.finQryCompanyType(profile.get().getAccntId());
        logger.info("companyTypeList - {}", companyTypeList);

        //조회내용 없을 때
        if(companyTypeList == null || companyTypeList.size() < 1){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Business Location
        List<AccountCompanyLocationDto> companyLocationList = accountCompanyRepository.finQryCompanyLocation(profile.get().getAccntId());
        logger.info("companyLocationList - {}", companyLocationList);

        //조회내용 없을 때
        if(companyLocationList == null || companyLocationList.size() < 1){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //contact person
        Optional<ProfileCompanyContactDto> companyContact  = Optional.ofNullable(accountCompanyRepository.findQryCompanyConcat(profile.get().getProfileId()));
        logger.info("companyContact - {}", companyContact);

        //조회내용 없을 때
        if(!companyContact.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        YnTypeEnum accountCompanyTypeVerifyYn = (companyTypeList.get(0).getCompanyLicenseVerifyYn() == YnTypeEnum.Y) ? YnTypeEnum.Y : YnTypeEnum.N;

        String accountCompanyTypeFileUrl = companyTypeList.get(0).getCompanyLicenseFileUrl();
        String accountCompanyTypeFileArr[] = accountCompanyTypeFileUrl.split("/");
        String accountCompanyTypeFile = accountCompanyTypeFileArr[accountCompanyTypeFileArr.length - 1];

        CountryEntity countryEntity = countryRepository.findByCountryCd(companyContact.get().getCountryCd());
        String countryDial = countryEntity.getDial();
        String countryIso2 = countryEntity.getIso2();
        String companyPhone = "(" + countryIso2 + ")"
                        + "+" + countryDial
                        + " " + companyContact.get().getContctPhoneNumber();



        Optional<CompanyVerifyDetailDto> dtoPage =

                profile.map(p -> CompanyVerifyDetailDto.builder()
                        .accntId(p.getAccntId())
                        .profileId(p.getProfileId())
                        .companyEmail(account.get().getContctEmail())
                        .accountCompanyTypeDtoList(companyTypeList)
                        .accountCompanyTypeVerifyYn(accountCompanyTypeVerifyYn)
                        .accountCompanyTypeFileUrl(accountCompanyTypeFileUrl)
                        .accountCompanyTypeFile(accountCompanyTypeFile)
                        .companyName(p.getProfileCompanyName())
                        .accountCompanyLocationDtoList(companyLocationList)
                        .companyPost(account.get().getPostCd())
                        .companyRegion(account.get().getRegion())
                        .companyCity(account.get().getCity())
                        .companyAddr1(account.get().getAddr1())
                        .companyAddr2(account.get().getAddr2())
                        .countryCd(companyContact.get().getCountryCd())
                        .countryCdName(companyContact.get().getCountryCdName())
                        .contctPhoneNumber(companyPhone)
                        .contctFirstName(companyContact.get().getContctFirstName())
                        .contctMiddleName(companyContact.get().getContctMiddleName())
                        .contctLastName(companyContact.get().getContctLastName())
                        .contctEmail(companyContact.get().getContctEmail())
                        .headline(p.getHeadline())
                        .ipiNumber(p.getIpiNumber())
                        .ipiNumberVarifyYn(p.getIpiNumberVarifyYn())
                        .vatNumber(p.getVatNumber())
                        .build()
                );

        logger.info("IndividualMemberDetailDto - {}", dtoPage);

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(dtoPage);
        return res;
    }

    //컴퍼니 타입 인증
    @Transactional
    public Response<Void> updateCompanyVerifyYn(Long id, String verifyYn, HttpServletRequest req) {

        Response<Void> res = new Response<>();

        //id값이 없을 때
        if (id == null) {
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<ProfileCompanyEntity> profile = profileCompanyRepository.findById(id);

        //조회내용 없을 때
        if (!profile.isPresent()) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //account_company_type 구하기
        List<AccountCompanyTypeDto> accountCompanyTypeDtoList = accountCompanyRepository.finQryCompanyType(profile.get().getAccntId());

        logger.info("accountCompanyTypeDtoList - {}", accountCompanyTypeDtoList);

        //조회내용 없을 때
        if (accountCompanyTypeDtoList == null || accountCompanyTypeDtoList.size() == 0) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }


        YnTypeEnum useType = (verifyYn.equals("Y")) ? YnTypeEnum.Y : YnTypeEnum.N;
        //String newUseType = (useType.equals("Y")) ? "Y" : "N";

        logger.info("useType - {} ", useType);

        try {
            accountCompanyRepository.updateQryCompanyTypeVerifyYn(profile.get().getAccntId(), useType);

            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            //res.setResult(songDetailCode);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));

            String menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_COMPANY_MEMBER.getCode());
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_MODIFY.getCode());

            //netive query
            String usrName = adminActiveLogService.findQryUsrName(profile.get().getModUsr());

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(profile.get().getModDt())
                    .adminId(profile.get().getModUsr())
                    .menuName(menuName)
                    .activeType("U")
                    .contentsId(profile.get().getProfileId())
                    .activeMsg(usrName + " " + activeMsg + " '" + "Company Verication " + useType.getYnEnumType() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e) {
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }
        return res;
    }

    //회사 Ipi 인증
    @Transactional
    public Response<Void> updateCompanyVerifyIpiYn(Long id, String verifyIpiYn, HttpServletRequest req) {

        Response<Void> res = new Response<>();

        //id값이 없을 때
        if (id == null) {
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<ProfileCompanyEntity> profileCompany = profileCompanyRepository.findById(id);

        logger.info("profileCompany - {}", profileCompany);

        //조회내용 없을 때
        if (!profileCompany.isPresent()) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }


        //String useType = verifyIpiYn;
        YnTypeEnum newUseType = (verifyIpiYn.equals("Y")) ? YnTypeEnum.Y : YnTypeEnum.N;

        logger.info("newUseType - {} ", newUseType);

        try {
            profileCompany.get().updateIpiNumberVarifyYn(newUseType);
            //Account 인증 컴퍼니
            notiDataSetService.NotiTestCode006(id);
            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            //res.setResult(songDetailCode);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));

            String menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_COMPANY_MEMBER.getCode());
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_MODIFY.getCode());

            //netive query
            String usrName = adminActiveLogService.findQryUsrName(profileCompany.get().getModUsr());

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(profileCompany.get().getModDt())
                    .adminId(profileCompany.get().getModUsr())
                    .menuName(menuName)
                    .activeType("U")
                    .contentsId(profileCompany.get().getProfileId())
                    .activeMsg(usrName + " " + activeMsg + " '" + "Companuy IPI Verication " + newUseType.getYnEnumType() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e) {
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }
        return res;
    }
    ///// company ///////////////////////////////////////////////////////////////////////////////////////////
}
