package com.sparwk.adminnode.admin.biz.v1.profile.dto;

import com.sparwk.adminnode.admin.biz.v1.account.dto.AccountCompanyTypeDto;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
@NoArgsConstructor
public class ProfileCompanyPartnerDto {

    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long profileId;
    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long partnerId;
    @Schema(description = "partner id값 Seq 사용", nullable = false, example = "1")
    private Long partnerAccntId;
    @Schema(description = "company Name", nullable = false, example = "SPARWK")
    private String partnerCompanyName;
    @Schema(description = "Company Email", nullable = false, example = "sparwk@sparwk.com")
    private String partnerCompanyEmail;
    @Schema(description = "COMPANY TYPE", nullable = false)
    private List<AccountCompanyTypeDto> partnerCompanyTypeDtoList;
    @Schema(description = "company cd", nullable = false, example = "CNT000001")
    private String partnerCompanyCountryCd;
    @Schema(description = "company Country Name", nullable = false, example = "USA")
    private String partnerCompanyCountryName;



    @Builder
    public ProfileCompanyPartnerDto(Long profileId,
                                    Long partnerId,
                                    Long partnerAccntId,
                                    String partnerCompanyName,
                                    String partnerCompanyEmail,
                                    List<AccountCompanyTypeDto> partnerCompanyTypeDtoList,
                                    String partnerCompanyCountryCd,
                                    String partnerCompanyCountryName
    ) {
        this.profileId = profileId;
        this.partnerId = partnerId;
        this.partnerAccntId = partnerAccntId;
        this.partnerCompanyName = partnerCompanyName;
        this.partnerCompanyEmail = partnerCompanyEmail;
        this.partnerCompanyTypeDtoList = partnerCompanyTypeDtoList;
        this.partnerCompanyCountryCd = partnerCompanyCountryCd;
        this.partnerCompanyCountryName = partnerCompanyCountryName;
    }
}
