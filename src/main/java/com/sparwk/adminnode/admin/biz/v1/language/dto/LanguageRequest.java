package com.sparwk.adminnode.admin.biz.v1.language.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@Schema(description = "Request DTO")
public class LanguageRequest {

//    @Schema(description = "언어명", nullable = false, example = "English")
//    @NotBlank(message = "Please enter a language.")
//    private String language;
//    @Schema(description = "유사언어", nullable = true, example = "Indo-European")
//    private String languageFamily;
//    @Schema(description = "현지어", nullable = false, example = "English")
//    @NotBlank(message = "Please enter a native language.")
//    private String nativeLanguage;
//    @Schema(description = "iso639_1 값", nullable = false, example = "en")
//    @NotBlank(message = "Please enter a native ISO 639-1.")
//    private String iso639_1;
//    @Schema(description = "iso639_2t 값", nullable = true, example = "eng")
//    private String iso639_2t;
//    @Schema(description = "iso639_2b 값", nullable = true, example = "eng")
//    private String iso639_2b;
//    @Schema(description = "iso639_3 값", nullable = true, example = "eng")
//    private String iso639_3;
//    @Schema(description = "코드 사용여부 Y/N",  defaultValue = "Y", allowableValues = {"Y", "N"})
//    @NotNull(message = "Please check use.")
//    private UseType useType;
//
//    @Builder
//    public LanguageRequest(String language,
//                          String languageFamily,
//                          String nativeLanguage,
//                          String iso639_1,
//                          String iso639_2t,
//                          String iso639_2b,
//                          String iso639_3,
//                          UseType useType
//    ) {
//        this.language = language;
//        this.languageFamily = languageFamily;
//        this.nativeLanguage = nativeLanguage;
//        this.iso639_1 = iso639_1;
//        this.iso639_2t = iso639_2t;
//        this.iso639_2b = iso639_2b;
//        this.iso639_3 = iso639_3;
//        this.useType = useType;
//    }

    private LanguageDto languageDto;
}
