package com.sparwk.adminnode.admin.biz.v1.currency.dto;

import lombok.*;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class CurrencyRequest {

//    @Schema(description = "언어명", nullable = false, example = "English")
//    @NotBlank(message = "Please enter a currency.")
//    private String currency;
//    @Schema(description = "화폐명", nullable = false, example = "English")
//    @NotBlank(message = "Please enter a code.")
//    private String code;
//    @Schema(description = "사용국가 또는 그룹", nullable = true, example = "Åland Islands (AX), European Union (EU), Andorra (AD), Austria (AT), Belgium (BE), Cyprus (CY), Estonia (EE), Finland (FI), France (FR), French Southern and Antarctic Lands (TF), Germany (DE), Greece (GR), Guadeloupe (GP), Ireland (IE), Italy (IT), Latvia (LV), Lithuania (LT), Luxembourg (LU), Malta (MT), French Guiana (GF), Martinique (MQ), Mayotte (YT), Monaco (MC), Montenegro (ME), Netherlands (NL), Portugal (PT), Réunion (RE), Saint Barthélemy (BL), Saint Martin (MF), Saint Pierre and Miquelon (PM), San Marino (SM), Slovakia (SK), Slovenia (SI), Spain (ES), Vatican City (VA)")
//    private String country;
//    @Schema(description = "코드 사용여부 Y/N",  defaultValue = "Y", allowableValues = {"Y", "N"})
//    @NotNull(message = "Please check use.")
//    private UseType useType;
//
//    @Builder
//    public CurrencyRequest(String currency,
//                           String code,
//                           String country,
//                           UseType useType
//    ) {
//        this.currency = currency;
//        this.code = code;
//        this.country = country;
//        this.useType = useType;
//    }

    private CurrencyDto currencyDto;
}
