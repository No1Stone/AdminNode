package com.sparwk.adminnode.admin.biz.v1.test.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

@Setter
@Getter
@ToString
@NoArgsConstructor
public class TestDto {

    @Schema(description = "부모코드 3자리")
    private String ckey;
    @Schema(description = "부모코드 값")
    private String cval;

    @Builder
    public TestDto(
                   String ckey,
                   String cval
    ) {
        this.ckey = ckey;
        this.cval = cval;
    }
}
