package com.sparwk.adminnode.admin.biz.v1.email.service;

import com.google.gson.Gson;
import com.sparwk.adminnode.admin.biz.v1.CommonPagingListDto;
import com.sparwk.adminnode.admin.biz.v1.admin.service.AdminActiveLogService;
import com.sparwk.adminnode.admin.biz.v1.email.dto.*;
import com.sparwk.adminnode.admin.biz.v1.test.dto.AdminEmailSendRequest;
import com.sparwk.adminnode.admin.config.common.ActiveCodeConst;
import com.sparwk.adminnode.admin.config.common.GetAccountId;
import com.sparwk.adminnode.admin.config.common.Response;
import com.sparwk.adminnode.admin.config.common.ResultCodeConst;
import com.sparwk.adminnode.admin.jpa.entity.admin.AdminActiveLogEntity;
import com.sparwk.adminnode.admin.jpa.entity.email.*;
import com.sparwk.adminnode.admin.jpa.repository.email.EmailAttachRepository;
import com.sparwk.adminnode.admin.jpa.repository.email.EmailRepository;
import com.sparwk.adminnode.admin.jpa.repository.email.MailingRepository;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class EmailService {

    private final EmailRepository emailRepository;
    private final EmailAttachRepository emailAttachRepository;
    private final MailingRepository mailingRepository;
    private final RestTemplate restTemplate;
    private final AdminActiveLogService adminActiveLogService;

    private final HttpServletRequest httpServletRequest;
    private final MessageSourceAccessor messageSourceAccessor;

    private final EntityManager em;

    @Value("${mail.schema}")
    private String mailSchema;
    @Value("${mail.host}")
    private String mailHost;
    @Value("${mail.path}")
    private String mailPath;
    
    private final Logger logger = LoggerFactory.getLogger(EmailService.class);


    public Response<CommonPagingListDto> getList(EmailCateEnum cate,
                                                 String val,
                                                 EmailSorterEnum sorter,
                                                 PageRequest pageRequest) {

        Response<CommonPagingListDto> res = new Response<>();

        List<EmailListDto> emailList = emailRepository.findQryAll(cate, val, sorter, pageRequest);

        logger.info("emailList - {}", emailList);

        //조회내용 없을 때
        if(emailList == null || emailList.size() < 1){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        CommonPagingListDto list = new CommonPagingListDto();

        //총 카운트
        long totalSize = emailRepository.countQryAll(cate, val, sorter);
        long totalPage = totalSize / pageRequest.getPageSize();
        long mod = totalSize % pageRequest.getPageSize();
        if(mod > 0) {
            totalPage += 1;
        }

        list.setList(emailList);
        list.setSize(pageRequest.getPageSize());
        list.setPage(pageRequest.getPageNumber());
        list.setTotalPage(totalPage);
        list.setTotalSize(totalSize);

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(list);
        return res;
    }

    @Transactional
    public Response<Void> saveContent(EmailRequest request, HttpServletRequest req) {

        Response<Void> res = new Response<>();
        List<MailingEntity> mailingEntities = new ArrayList<>();

        //Value 값 없을 때
        if(request.getSubject() == null){
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if(request.getFromEmail() == null){
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if(request.getContent() == null){
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if(request.getToEmail() == null){
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        String toEmailArr[] = request.getToEmail().split(",");



        //첨부파일 수
        //int attachCnt = request.getEmailAttahDtoList().size();

        //보내는 인원 수
        int memberCnt = toEmailArr.length;

        if(memberCnt == 0){
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }



        try {

            EmailEntity newEmail = EmailEntity.builder()
                    .subject(request.getSubject())
                    .fromEmail(request.getFromEmail())
                    .toEmail(request.getToEmail())
                    .content(request.getContent())
                    .maxAttachCnt(0)
                    .memberCnt(memberCnt)
                    .build();

            EmailEntity saved = emailRepository.save(newEmail);

            //다시 불러옴
            Optional<EmailEntity> email = emailRepository.findById(saved.getEmailSeq());
            logger.info("email - {}", email);

            String[] reciverArr = new String[memberCnt];
            String[] awsMailArr = new String[1]; //선언만
            List<String> emailList = new ArrayList<>();
            List<EmailMemberMailListDto> emailList1 = new ArrayList<>();
            List<EmailMemberMailListDto> emailList2 = new ArrayList<>();
            List<EmailMemberMailListDto> emailList3 = new ArrayList<>();
            List<EmailMemberMailListDto> emailList4 = new ArrayList<>();
            if(memberCnt > 0) {

                for(int i=0; i < memberCnt; i++){

                    switch (String.valueOf(toEmailArr[i].replaceAll(" ",""))) {
                        case "@All":
                            //검색
                            logger.info("@All");
                            emailList1 = emailRepository.findQryMemberAll(null,null, "Y", "Y", "Y");
                            break;
                        case "@Individual":
                            logger.info("@Individual");
                            emailList2 = emailRepository.findQryMemberAll(null,null, "Y", "N", "N");
                            break;
                        case "@Company":
                            logger.info("@Company");
                            emailList3 = emailRepository.findQryMemberAll(null,null, "N", "Y", "N");
                            break;
                        case "@Etc":
                            logger.info("@Etc");
                            emailList4 = emailRepository.findQryMemberAll(null,null, "N", "N", "Y");
                            break;
                        default:
                            logger.info("@default");
                            reciverArr[i] = toEmailArr[i];
                            break;
                    }
                }

                //System.out.println("111111111" + Arrays.toString(reciverArr));

                // 배열 -> List 변환
                List<String> list = new ArrayList<>(Arrays.asList(reciverArr));

                // null 삭제
                list.removeAll(Collections.singletonList(null));

//                System.out.println("2222222222222222222222");
//                list.stream().forEach(System.out::println);

                if(emailList1.size() > 0) {
                    //stream 사용
                    List<String> ids = emailList1.stream().map(e -> e.getEmail()).collect(Collectors.toCollection(ArrayList::new));
                    list = Stream.concat(list.stream(), ids.stream()).collect(Collectors.toList());
                }

                if(emailList2.size() > 0) {
                    //stream 사용
                    List<String> ids = emailList2.stream().map(e -> e.getEmail()).collect(Collectors.toCollection(ArrayList::new));
                    list = Stream.concat(list.stream(), ids.stream()).collect(Collectors.toList());
                }

                if(emailList3.size() > 0) {
                    //stream 사용
                    List<String> ids = emailList3.stream().map(e -> e.getEmail()).collect(Collectors.toCollection(ArrayList::new));
                    list = Stream.concat(list.stream(), ids.stream()).collect(Collectors.toList());
                }

                if(emailList4.size() > 0) {
                    //stream 사용
                    List<String> ids = emailList4.stream().map(e -> e.getEmail()).collect(Collectors.toCollection(ArrayList::new));
                    list = Stream.concat(list.stream(), ids.stream()).collect(Collectors.toList());
                }

//                System.out.println("33333333333333333333333");
//                list.stream().forEach(System.out::println);

                // null 삭제 또 삭제
                list.removeAll(Collections.singletonList(null));

                // List -> 배열 변환
                reciverArr = list.toArray(reciverArr);

                //메일 보내기 셋팅
                AdminEmailSendRequest adminEmailSendRequest = new AdminEmailSendRequest();

                //adminEmailSendRequest.setReceiver(reciverArr);
                adminEmailSendRequest.setSubject(request.getSubject());
                adminEmailSendRequest.setContents(request.getContent());

                System.out.println(Arrays.toString(reciverArr));

                if(list.size() > 0) {

                    int k = list.size() / 50;
                    int mod = list.size() % 50;
                    if(mod != 0)
                        k = k + 1;
                    logger.info("list.size() - {}", list.size());
                    logger.info("k - {}", k);

                    for(int i = 0; i < list.size(); i++){

                        //awsMailArr[0] = list.get(i);
                        //logger.info("{} - {}", i, awsMailArr.toString());

                        //AWS 메일 1개씩 짤라서 보내
                        //adminEmailSendRequest.setReceiver(awsMailArr);
                        //메일 보내기
                        //EmailSend2(adminEmailSendRequest);

                        MailingEntity e = MailingEntity.builder()
                                .toEmail(list.get(i))
                                .fromEmail(email.get().getFromEmail())
                                .subject(email.get().getSubject())
                                .content(email.get().getContent())
                                .sendYn("N")
                                .status("100")
                                .build();

                        mailingEntities.add(e);

                        logger.info("------------------------------");
                    }
                    //메일 보내기
                    //EmailSend2(adminEmailSendRequest);
                    logger.info("reciverArr - {}", reciverArr);
//
                    mailingRepository.saveAll(mailingEntities);
                }
            }



            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
//            res.setResult(dtoPage);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));

            String menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_EMAIL.getCode());
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_CREATE.getCode());

            //netive query
            String usrName = adminActiveLogService.findQryUsrName(saved.getRegUsr());

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(saved.getRegDt())
                    .adminId(saved.getRegUsr())
                    .menuName(menuName)
                    .activeType("C")
                    .contentsId(saved.getEmailSeq())
                    .activeMsg(usrName + " " + activeMsg + " '" + saved.getSubject() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e){
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }

        return res;
    }

    public Response<CommonPagingListDto> getMemberList(EmailMemberCateEnum cate,
                                                       String val,
                                                       String indiYn,
                                                       String comYn,
                                                       String etcYn,
                                                       PageRequest pageRequest) {

        Response<CommonPagingListDto> res = new Response<>();

        indiYn = (String.valueOf(indiYn).equals("Y")) ? "Y" : "N";
        comYn = (String.valueOf(comYn).equals("Y")) ? "Y" : "N";
        etcYn = (String.valueOf(etcYn).equals("Y")) ? "Y" : "N";

        List < EmailMemberListDto > emailList = emailRepository.findQryMemberList(cate, val, indiYn, comYn, etcYn, pageRequest);

        logger.info("emailList - {}", emailList);

        //조회내용 없을 때
        if(emailList == null || emailList.size() < 1){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        CommonPagingListDto list = new CommonPagingListDto();

        //총 카운트
        long totalSize = emailRepository.countQryMember(cate, val, indiYn, comYn, etcYn);
        long totalPage = totalSize / pageRequest.getPageSize();
        long mod = totalSize % pageRequest.getPageSize();
        if(mod > 0) {
            totalPage += 1;
        }

        list.setList(emailList);
        list.setSize(pageRequest.getPageSize());
        list.setPage(pageRequest.getPageNumber());
        list.setTotalPage(totalPage);
        list.setTotalSize(totalSize);

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(list);
        return res;
    }

    public Response<CommonPagingListDto> getMemberSettings() {

        Response<CommonPagingListDto> res = new Response<>();

        List<EmailMemberSettingsDto> settingList = new ArrayList<>();

        EmailMemberSettingsDto setting1 = new EmailMemberSettingsDto("indi", "");
        EmailMemberSettingsDto setting2 = new EmailMemberSettingsDto("com", "");
        EmailMemberSettingsDto setting3 = new EmailMemberSettingsDto("etc", "");

        settingList.add(setting1);
        settingList.add(setting2);
        settingList.add(setting3);

        CommonPagingListDto list = new CommonPagingListDto();

        //총 카운트
        long totalSize = 1;
        long totalPage = 1;

        list.setList(settingList);
        list.setSize(1);
        list.setPage(1);
        list.setTotalPage(totalPage);
        list.setTotalSize(totalSize);

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(list);
        return res;
    }


    public void EmailSend2(AdminEmailSendRequest entity) {

        HttpHeaders headers = new HttpHeaders();
        headers.set("Auth","test");
        UriComponentsBuilder builder =
                UriComponentsBuilder
                        .newInstance()
                        .scheme(mailSchema)// http & https
                        .host(mailHost) //localhost:8080
                        .path(mailPath); //V1/profile/info
        UriComponents uriComponents = builder.build();

        //entity.setContents(entity.getContents());

        logger.info("URI COMPONENT tostring - {}", uriComponents.toString());
        logger.info(" - - - - - - 1 : {}", entity);
        HttpEntity<AdminEmailSendRequest> responsEntity = new HttpEntity<>(entity, headers);
        logger.info(" - - - - - - 2 : {}", responsEntity.toString());
        logger.info(" - - - - - - 3 : {}", uriComponents.toUriString());
//        ResponseEntity<String> resultResponse =
//                restTemplate.postForEntity(uriComponents.toUriString(), responsEntity, String.class);
        ResponseEntity<String> resultResponse =
                restTemplate.postForEntity(uriComponents.toUriString(), responsEntity, String.class);
        logger.info("asdasdasd - 4 : {}", resultResponse.toString());
//        JSONObject obj = new JSONObject(resultResponse.getBody());
//        logger.info("object - 5 : {}", obj.toString());
        Gson gson = new Gson();
//        Response response = gson.fromJson(String.valueOf(obj), Response.class);
//        logger.info("--------Respoonse {}", response.getResultCd());
    }

}
