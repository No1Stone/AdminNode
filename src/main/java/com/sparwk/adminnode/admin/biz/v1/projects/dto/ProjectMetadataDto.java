package com.sparwk.adminnode.admin.biz.v1.projects.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Setter
@Getter
@NoArgsConstructor
public class ProjectMetadataDto {

    @Schema(description = "project id 번호", nullable = false, example = "1")
    private Long projId;
    @Schema(description = "부모코드", nullable = false, example = "SGG")
    private String attrTypeCd;
    @Schema(description = "자식코드", nullable = false, example = "SGG000001")
    private String attrDtlCd;
    @Schema(description = "자식코드값", nullable = true, example = "Blues")
    private String attrDtlCdVal;


    @Builder
    public ProjectMetadataDto(Long projId,
                              String attrTypeCd,
                              String attrDtlCd,
                              String attrDtlCdVal
    ) {
        this.projId = projId;
        this.attrTypeCd = attrTypeCd;
        this.attrDtlCd = attrDtlCd;
        this.attrDtlCdVal = attrDtlCdVal;
    }
}
