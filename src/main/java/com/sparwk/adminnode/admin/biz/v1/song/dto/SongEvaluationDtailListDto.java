package com.sparwk.adminnode.admin.biz.v1.song.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class SongEvaluationDtailListDto {

    @Schema(description = "id 번호", nullable = false, example = "1")
    private Long evalSeq;
    @Schema(description = "id 번호", nullable = false, example = "1")
    private Long songId;
    @Schema(description = "id 번호", nullable = false, example = "1")
    private Long evalAnrSeq;
    @Schema(description = "상태코드", nullable = false, example = "TTT000009")
    private String evalTemplDtlCd;
    @Schema(description = "상태코드이름", nullable = false, example = "Concept")
    private String evalTemplDtlCdName;
    @Schema(description = "점수", nullable = false, example = "1")
    private int evalRate;

    @Builder
    public SongEvaluationDtailListDto(Long evalSeq,
                                      Long songId,
                                      Long evalAnrSeq,
                                      String evalTemplDtlCd,
                                      String evalTemplDtlCdName,
                                      int evalRate
    ) {
        this.evalSeq = evalSeq;
        this.songId = songId;
        this.evalAnrSeq = evalAnrSeq;
        this.evalTemplDtlCd = evalTemplDtlCd;
        this.evalTemplDtlCdName = evalTemplDtlCdName;
        this.evalRate = evalRate;
    }
}
