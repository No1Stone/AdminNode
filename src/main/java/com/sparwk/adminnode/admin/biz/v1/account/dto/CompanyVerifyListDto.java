package com.sparwk.adminnode.admin.biz.v1.account.dto;

import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

@Setter
@Getter
@NoArgsConstructor
public class CompanyVerifyListDto {

    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long accntId;
    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long profileId;
    @Schema(description = "Account E-mail", nullable = false, example = "info@ekkomusicrights.com")
    private String companyEmail;
    @Schema(description = "AccountCompanyTypeDto", nullable = true)
    private List<AccountCompanyTypeDto> accountCompanyTypeDtoList;
    @Schema(description = "COMPANY NAME", nullable = false, example = "Spawrk")
    private String companyName;
    @Schema(description = "AccountCompanyTypeDto", nullable = true)
    private List<AccountCompanyLocationDto> accountCompanyLocationDtoList;
    @Schema(description = "IPI_NUMBER_YN", nullable = false, defaultValue = "N", allowableValues = {"Y", "N"})
    private YnTypeEnum ipiNumberVarifyYn;
    @Schema(description = "VAT_NUMBER_YN", nullable = false, defaultValue = "N", allowableValues = {"Y", "N"})
    private YnTypeEnum vatNumberVarifyYn;
    @Schema(description = "등록일", pattern = "yyMMdd hh:mm:ss", example = "2021-12-08T05:44:26.627991")
    private LocalDateTime regDt;


    @Builder
    public CompanyVerifyListDto(Long accntId,
                                Long profileId,
                                String companyEmail,
                                List<AccountCompanyTypeDto> accountCompanyTypeDtoList,
                                String companyName,
                                List<AccountCompanyLocationDto> accountCompanyLocationDtoList,
                                YnTypeEnum ipiNumberVarifyYn,
                                YnTypeEnum vatNumberVarifyYn,
                                LocalDateTime regDt
    ) {
        this.accntId = accntId;
        this.profileId = profileId;
        this.companyEmail = companyEmail;
        this.accountCompanyTypeDtoList = accountCompanyTypeDtoList;
        this.companyName = companyName;
        this.accountCompanyLocationDtoList = accountCompanyLocationDtoList;
        this.ipiNumberVarifyYn = ipiNumberVarifyYn;
        this.vatNumberVarifyYn = vatNumberVarifyYn;
        this.regDt = regDt;
    }
}
