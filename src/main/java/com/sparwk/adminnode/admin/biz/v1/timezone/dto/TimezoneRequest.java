package com.sparwk.adminnode.admin.biz.v1.timezone.dto;

import lombok.*;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class TimezoneRequest {

//    @Schema(description = "타임존명", nullable = false, example = "Africa/Maseru")
//    private String timezoneName;
//    @Schema(description = "국가명", nullable = false, example = "Africa")
//    private String continent;
//    @Schema(description = "도시명", nullable = false, example = "Maseru")
//    private String city;
//    @Schema(description = "UTC 시간", nullable = false, example = "3:00")
//    private String utcHour;
//    @Schema(description = "UTC 시간 숫자형", nullable = false, example = "3")
//    private int diffTime;
//    @Schema(description = "코드 사용여부 Y/N", defaultValue = "Y", allowableValues = {"Y", "N"})
//    private UseType useType;
//
//    @Builder
//    public TimezoneRequest(
//                        String timezoneName,
//                        String continent,
//                        String city,
//                        String utcHour,
//                        int diffTime,
//                        UseType useType
//    ) {
//        this.timezoneName = timezoneName;
//        this.continent = continent;
//        this.city = city;
//        this.utcHour = utcHour;
//        this.diffTime = diffTime;
//        this.useType = useType;
//    }

    private TimezoneDto timezoneDto;
}
