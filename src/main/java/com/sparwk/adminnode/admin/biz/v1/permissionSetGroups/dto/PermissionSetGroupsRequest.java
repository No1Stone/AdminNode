package com.sparwk.adminnode.admin.biz.v1.permissionSetGroups.dto;

import lombok.*;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class PermissionSetGroupsRequest {

//    @Schema(description = "코드 값", nullable = false, example = "admin")
//    @NotBlank(message = "Please enter a label.")
//    private String labelVal;
//    @Schema(description = "코드 사용여부 Y/N", defaultValue = "Y", allowableValues = {"Y", "N"})
//    @NotBlank(message = "Please check use.")
//    private UseType useType;
//    @Schema(description = "description 설명", nullable = true, example = "설명")
//    private String cdDesc;
//    @Schema(description = "코드 순번 숫자", nullable = true, example = "1")
//    private int cdOrd;

    private PermissionSetGroupsDto permissionSetGroupsDto;
}