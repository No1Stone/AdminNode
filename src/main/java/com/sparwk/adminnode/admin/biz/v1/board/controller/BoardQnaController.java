package com.sparwk.adminnode.admin.biz.v1.board.controller;

import com.sparwk.adminnode.admin.biz.v1.CommonPagingListDto;
import com.sparwk.adminnode.admin.biz.v1.board.dto.BoardQnaAnswerRequest;
import com.sparwk.adminnode.admin.biz.v1.board.dto.BoardQnaDto;
import com.sparwk.adminnode.admin.biz.v1.board.dto.BoardQnaQuestionRequest;
import com.sparwk.adminnode.admin.biz.v1.board.service.BoardQnaService;
import com.sparwk.adminnode.admin.config.common.AdminPermissionIntergrationService;
import com.sparwk.adminnode.admin.config.common.Response;
import com.sparwk.adminnode.admin.config.common.ResultCodeConst;
import com.sparwk.adminnode.admin.jpa.entity.PeriodTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.board.BoardCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.board.BoardSorterEnum;
import com.sparwk.adminnode.admin.jpa.entity.board.QnaCateEnum;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/V1/help/")
@RequiredArgsConstructor
@CrossOrigin("*")
public class BoardQnaController {

    private final Logger logger = LoggerFactory.getLogger(BoardQnaController.class);

    @Autowired
    private BoardQnaService boardQnaService;
    private final MessageSourceAccessor messageSourceAccessor;
    @Autowired
    private AdminPermissionIntergrationService adminPermissionIntergrationService;

    @GetMapping("qna")
    @Operation(
            summary = "Q&A List", description = "Q&A List입니다.",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = BoardQnaDto.class))),
                    @ApiResponse(
                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
                            , content = @Content(schema = @Schema(hidden = true ))),
                    @ApiResponse(
                            responseCode = "1001", description = "카테고리 코드가 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true ))),
                    @ApiResponse(
                            responseCode = "1101", description = "카테고리 코드 값이 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true )))
            })
    public Response<CommonPagingListDto> getList(
            @Parameter(name = "val", description = "검색어 포함", in = ParameterIn.PATH) @RequestParam(value = "val", required = false) String val,
            @Parameter(name = "cate", description = "검색 종류", in = ParameterIn.PATH) @RequestParam(value = "cate", required = false) QnaCateEnum cate,
            @Parameter(name = "page", description = "페이지 번호", in = ParameterIn.PATH) @RequestParam(value = "page", required = false, defaultValue = "1") int page,
            @Parameter(name = "size", description = "페이지 사이즈", in = ParameterIn.PATH) @RequestParam(value = "size", required = false, defaultValue = "10") int size
    ) {
        PageRequest pageRequest = PageRequest.of(page, size);

        Response res = new Response();
        if(!adminPermissionIntergrationService.AdminPermissionIntergrationCheck(8L, "R")){
            res.setResultCd(ResultCodeConst.NOT_VALID_READ.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        return boardQnaService.getList("Question", val, cate, pageRequest);
    }

    //상세페이지
    @GetMapping("qna/{id}")
    @Operation(
            summary = "Qna & POST Board View", description = "Qna & POST Board View 입니다.",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = BoardQnaDto.class))),
                    @ApiResponse(
                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
                            , content = @Content(schema = @Schema(hidden = true ))),
                    @ApiResponse(
                            responseCode = "1001", description = "카테고리 코드가 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true ))),
                    @ApiResponse(
                            responseCode = "1101", description = "카테고리 코드 값이 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true ))),
                    @ApiResponse(
                            responseCode = "1003", description = "id가 존재하지 않습니다.."
                            , content = @Content(schema = @Schema(hidden = true )))
            })
    public Response<Optional<BoardQnaDto>> getOne(
            @Parameter(name = "id", description = "id 값", in = ParameterIn.PATH) @PathVariable(value = "id") Long id
    ) {
        Response res = new Response();
        if(!adminPermissionIntergrationService.AdminPermissionIntergrationCheck(8L, "R")){
            res.setResultCd(ResultCodeConst.NOT_VALID_READ.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        return boardQnaService.getOne(id);
    }

    //쓰기처리
    @PostMapping("qna/new")
    @Operation(
            summary = "Qna & POST Board", description = "Qna & POST Board 추가합니다.",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = BoardQnaDto.class))),
                    @ApiResponse(
                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
                            , content = @Content(schema = @Schema(hidden = true ))),
                    @ApiResponse(
                            responseCode = "1001", description = "카테고리 코드가 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true ))),
                    @ApiResponse(
                            responseCode = "1101", description = "카테고리 코드 값이 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true ))),
                    @ApiResponse(
                            responseCode = "1203", description = "Use Type 값 형식이 잘못되었습니다."
                            , content = @Content(schema = @Schema(hidden = true ))),
                    @ApiResponse(
                            responseCode = "1206", description = "Value 형식이 잘못되었습니다."
                            , content = @Content(schema = @Schema(hidden = true )))
            })
    public Response<Void> saveQuestionContent(
            @Parameter(name = "request", description = "Request Dto", in = ParameterIn.PATH) @RequestBody @Valid BoardQnaQuestionRequest request,
            HttpServletRequest req
    ) {
        Response res = new Response();
        if(!adminPermissionIntergrationService.AdminPermissionIntergrationCheck(8L, "C")){
            res.setResultCd(ResultCodeConst.NOT_VALID_CREATE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        return boardQnaService.saveQuestionContent(request, req);
    }

    //답변처리
    @PostMapping("qna/{id}/answer")
    @Operation(
            summary = "Qna & POST Board Answer", description = "Qna & POST Board 답변 추가합니다.",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = BoardQnaDto.class))),
                    @ApiResponse(
                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
                            , content = @Content(schema = @Schema(hidden = true ))),
                    @ApiResponse(
                            responseCode = "1001", description = "카테고리 코드가 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true ))),
                    @ApiResponse(
                            responseCode = "1101", description = "카테고리 코드 값이 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true ))),
                    @ApiResponse(
                            responseCode = "1003", description = "id가 존재하지 않습니다."
                            , content = @Content(schema = @Schema(hidden = true ))),
                    @ApiResponse(
                            responseCode = "1203", description = "Use Type 값 형식이 잘못되었습니다."
                            , content = @Content(schema = @Schema(hidden = true ))),
                    @ApiResponse(
                            responseCode = "1206", description = "Value 형식이 잘못되었습니다."
                            , content = @Content(schema = @Schema(hidden = true )))
            })
    public Response<Void> answerContent(
            @Parameter(name = "id", description = "id 값", in = ParameterIn.PATH) @PathVariable(value = "id") Long id,
            @Parameter(name = "request", description = "Request Dto", in = ParameterIn.PATH) @RequestBody @Valid BoardQnaAnswerRequest request,
            HttpServletRequest req
    ) {
        Response res = new Response();
        if(!adminPermissionIntergrationService.AdminPermissionIntergrationCheck(8L, "C")){
            res.setResultCd(ResultCodeConst.NOT_VALID_CREATE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        return boardQnaService.saveAnswerContent(id, request, req);
    }
//
//
//    //업데이트처리
//    @PostMapping("qna/{id}/modify")
//    @Operation(
//            summary = "Qna & POST Board MODIFY", description = "Qna & POST Board 수정합니다.",
//            responses = {
//                    @ApiResponse(
//                            responseCode = "0000", description = "성공하였습니다."
//                            , content = @Content(schema = @Schema(implementation = BoardQnaDto.class))),
//                    @ApiResponse(
//                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
//                            , content = @Content(schema = @Schema(hidden = true ))),
//                    @ApiResponse(
//                            responseCode = "1001", description = "카테고리 코드가 존재하지 않습니다."
//                            , content = @Content(schema = @Schema(hidden = true ))),
//                    @ApiResponse(
//                            responseCode = "1101", description = "카테고리 코드 값이 존재하지 않습니다."
//                            , content = @Content(schema = @Schema(hidden = true ))),
//                    @ApiResponse(
//                            responseCode = "1003", description = "id가 존재하지 않습니다."
//                            , content = @Content(schema = @Schema(hidden = true ))),
//                    @ApiResponse(
//                            responseCode = "1204", description = "Format Type 값 형식이 잘못되었습니다."
//                            , content = @Content(schema = @Schema(hidden = true ))),
//                    @ApiResponse(
//                            responseCode = "1203", description = "Use Type 값 형식이 잘못되었습니다."
//                            , content = @Content(schema = @Schema(hidden = true ))),
//                    @ApiResponse(
//                            responseCode = "1205", description = "Popular Type 값 형식이 잘못되었습니다."
//                            , content = @Content(schema = @Schema(hidden = true ))),
//                    @ApiResponse(
//                            responseCode = "1206", description = "Value 형식이 잘못되었습니다."
//                            , content = @Content(schema = @Schema(hidden = true )))
//            })
//    public Response<Optional<BoardQnaDto>> updateContent(
//            @Parameter(name = "id", description = "id 값", in = ParameterIn.PATH) @PathVariable(value = "id") Long id,
//            @Parameter(name = "request", description = "Request Dto", in = ParameterIn.PATH) @RequestBody @Valid BoardQnaRequest request,
//            HttpServletRequest req
//    ) {
//        return boardQnaService.updateContent("Qna", id, request, req);
//    }
//
//    //삭제처리
//    @DeleteMapping("qna/{id}/delete")
//    @Operation(
//            summary = "Qna & POST Board DELETE", description = "Qna & POST Board 삭제합니다.",
//            responses = {
//                    @ApiResponse(
//                            responseCode = "0000", description = "성공하였습니다."
//                            , content = @Content(schema = @Schema(implementation = BoardQnaDto.class))),
//                    @ApiResponse(
//                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
//                            , content = @Content(schema = @Schema(hidden = true ))),
//                    @ApiResponse(
//                            responseCode = "1003", description = "id가 존재하지 않습니다."
//                            , content = @Content(schema = @Schema(hidden = true )))
//            })
//    public Response<Void> delete(
//            @Parameter(name = "id", description = "id 값", in = ParameterIn.PATH) @PathVariable(value = "id") Long id,
//            HttpServletRequest req) {
//        return boardQnaService.deleteContent(id, req);
//    }

}
