package com.sparwk.adminnode.admin.biz.v1.songCode.service;

import com.sparwk.adminnode.admin.biz.v1.CommonPagingListDto;
import com.sparwk.adminnode.admin.biz.v1.admin.service.AdminActiveLogService;
import com.sparwk.adminnode.admin.biz.v1.songCode.dto.SongDetailCodeDto;
import com.sparwk.adminnode.admin.biz.v1.songCode.dto.SongDetailCodeExcelDto;
import com.sparwk.adminnode.admin.biz.v1.songCode.dto.SongDetailCodeRequest;
import com.sparwk.adminnode.admin.config.common.ActiveCodeConst;
import com.sparwk.adminnode.admin.config.common.GetAccountId;
import com.sparwk.adminnode.admin.config.common.Response;
import com.sparwk.adminnode.admin.config.common.ResultCodeConst;
import com.sparwk.adminnode.admin.config.util.ExcelWriter;
import com.sparwk.adminnode.admin.jpa.entity.*;
import com.sparwk.adminnode.admin.jpa.entity.admin.AdminActiveLogEntity;
import com.sparwk.adminnode.admin.jpa.entity.songCode.SongCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.songCode.SongCodeEntity;
import com.sparwk.adminnode.admin.jpa.entity.songCode.SongDetailCodeEntity;
import com.sparwk.adminnode.admin.jpa.entity.songCode.SongDetailCodeSorterEnum;
import com.sparwk.adminnode.admin.jpa.repository.songCode.SongCodeRepository;
import com.sparwk.adminnode.admin.jpa.repository.songCode.SongDetailCodeRepository;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class SongDetailCodeService {

    private final SongCodeRepository songCodeRepository;
    private final SongDetailCodeRepository songDetailCodeRepository;
    private final AdminActiveLogService adminActiveLogService;

    private final HttpServletRequest httpServletRequest;
    private final MessageSourceAccessor messageSourceAccessor;

    private final Logger logger = LoggerFactory.getLogger(SongDetailCodeService.class);


    public Response<CommonPagingListDto> getList(String pcode,
                                                 SongCateEnum cate,
                                                 String val,
                                                 FormatTypeEnum cdFormatType,
                                                 YnTypeEnum useType,
                                                 PeriodTypeEnum periodType,
                                                 String sdate,
                                                 String edate,
                                                 SongDetailCodeSorterEnum sorter,
                                                 PageRequest pageRequest
    ) {

        Response<CommonPagingListDto> res = new Response<>();

        //부모코드 없을 때
        if(pcode == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<SongCodeEntity> songCode = Optional.ofNullable(
                songCodeRepository.findByCode(pcode)
        );

        logger.info("songCode - {}", songCode);

        //부모코드 조회내용 없을 때
        if(!songCode.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        List<SongDetailCodeDto> songDetailCodeList =
                songDetailCodeRepository.findQryAll(pcode, cate, val, cdFormatType, useType, periodType, sdate, edate, sorter, pageRequest);

        logger.info("SongDetailCodeList - {}", songDetailCodeList);

        //조회내용 없을 때
        if(songDetailCodeList == null || songDetailCodeList.size() < 1){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        CommonPagingListDto list = new CommonPagingListDto();

        //총 카운트
        long totalSize = songDetailCodeRepository.countQryAll(pcode, cate, val, cdFormatType, useType, periodType, sdate, edate);
        long totalPage = totalSize / pageRequest.getPageSize();
        long mod = totalSize % pageRequest.getPageSize();
        if(mod > 0) {
            totalPage += 1;
        }

        list.setList(songDetailCodeList);
        list.setSize(pageRequest.getPageSize());
        list.setPage(pageRequest.getPageNumber());
        list.setTotalPage(totalPage);
        list.setTotalSize(totalSize);

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(list);
        return res;
    }

    public Response<CommonPagingListDto> getUseYList(String pcode, String val) {

        Response<CommonPagingListDto> res = new Response<>();

        //부모코드 없을 때
        if(pcode == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<SongCodeEntity> songCode = Optional.ofNullable(
                songCodeRepository.findByCode(pcode)
        );

        logger.info("songCode - {}", songCode);

        //부모코드 조회내용 없을 때
        if(!songCode.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        List<SongDetailCodeDto> songDetailCodeList = songDetailCodeRepository.findQryUseY(pcode, val);

        logger.info("songDetailCodeList - {}", songDetailCodeList);

        //조회내용 없을 때
        if(songDetailCodeList == null || songDetailCodeList.size() < 1){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        CommonPagingListDto list = new CommonPagingListDto();

        PageRequest pageRequest = PageRequest.of(10, 10);

        //총 카운트
        long totalSize = songDetailCodeRepository.countQryUseY(pcode, val);
        long totalPage = totalSize / pageRequest.getPageSize();
        long mod = totalSize % pageRequest.getPageSize();
        if(mod > 0) {
            totalPage += 1;
        }

        list.setList(songDetailCodeList);
        list.setSize(pageRequest.getPageSize());
        list.setPage(pageRequest.getPageNumber());
        list.setTotalPage(totalPage);
        list.setTotalSize(totalSize);

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(list);
        return res;
    }

    public  Response<CommonPagingListDto> getPopularYList(String pcode) {

        Response<CommonPagingListDto> res = new Response<>();

        //부모코드 없을 때
        if(pcode == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<SongCodeEntity> songCode = Optional.ofNullable(
                songCodeRepository.findByCode(pcode)
        );

        logger.info("songCode - {}", songCode);

        //부모코드 조회내용 없을 때
        if(!songCode.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        List<SongDetailCodeDto> songDetailCodeList = songDetailCodeRepository.findPopularUseY(pcode);

        logger.info("songDetailCodeList - {}", songDetailCodeList);

        //조회내용 없을 때
        if(songDetailCodeList == null || songDetailCodeList.size() < 1){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        CommonPagingListDto list = new CommonPagingListDto();

        PageRequest pageRequest = PageRequest.of(10, 10);

        //총 카운트
        long totalSize = songDetailCodeRepository.countPopularUseY(pcode);
        long totalPage = totalSize / pageRequest.getPageSize();
        long mod = totalSize % pageRequest.getPageSize();
        if(mod > 0) {
            totalPage += 1;
        }

        list.setList(songDetailCodeList);
        list.setSize(pageRequest.getPageSize());
        list.setPage(pageRequest.getPageNumber());
        list.setTotalPage(totalPage);
        list.setTotalSize(totalSize);

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(list);
        return res;
    }

    public Response<Optional<SongDetailCodeDto>> getOne(String pcode, Long id) {

        Response<Optional<SongDetailCodeDto>> res = new Response<>();

        //부모코드 없을 때
        if(pcode == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<SongCodeEntity> songCode = Optional.ofNullable(
                songCodeRepository.findByCode(pcode)
        );

        logger.info("songCode - {}", songCode);

        //부모코드 조회내용 없을 때
        if(!songCode.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //id값이 없을 때
        if(id == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<SongDetailCodeEntity> songDetailCode = songDetailCodeRepository.findById(id);

        logger.info("songDetailCode - {}", songDetailCode);

        //조회내용 없을 때
        if(!songDetailCode.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<SongDetailCodeDto> dtoPage =

                songDetailCode
                        .map(p -> SongDetailCodeDto.builder()
                                .songDetailCodeSeq(p.getSongDetailCodeSeq())
                                .pcode(p.getSongCodeEntity().getCode())
                                .pcodeVal(p.getSongCodeEntity().getVal())
                                .dcode(p.getDcode())
                                .val(p.getVal())
                                .formatType(p.getFormatType())
                                .useType(p.getUseType())
                                .popularType(p.getPopularType())
                                .description(p.getDescription())
                                .sortIndex(p.getSortIndex())
                                .hit(p.getHit())
                                .regUsr(p.getRegUsr())
                                .regDt(p.getRegDt())
                                .modUsr(p.getModUsr())
                                .modDt(p.getModDt())
                                .build()
                        );

        logger.info("SongDetailCode - {}", dtoPage);

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(dtoPage);
        return res;
    }

    @Transactional
    public Response<Void> saveContent(String pcode, SongDetailCodeRequest request, HttpServletRequest req) {

        Response<Void> res = new Response<>();
        String dcode;

        //부모코드 없을 때
        if(request.getSongDetailCodeDto().getPcode() == null){
            res.setResultCd(ResultCodeConst.NOT_VALID_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<SongCodeEntity> songCode = Optional.ofNullable(songCodeRepository.findByCode(pcode));

        logger.info("songCode - {}", songCode);

        //부모코드 조회내용 없을 때
        if(!songCode.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Format 값 없거나 틀릴 때
        if(request.getSongDetailCodeDto().getFormatType() == null || (request.getSongDetailCodeDto().getFormatType() != FormatTypeEnum.DDEX && request.getSongDetailCodeDto().getFormatType() != FormatTypeEnum.SPARWK)){
            res.setResultCd(ResultCodeConst.NOT_VALID_FORMAT.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //USEYN 값 없거나 틀릴 때
        if(request.getSongDetailCodeDto().getUseType() == null || (request.getSongDetailCodeDto().getUseType() != YnTypeEnum.Y && request.getSongDetailCodeDto().getUseType() != YnTypeEnum.N)){
            res.setResultCd(ResultCodeConst.NOT_VALID_USEYN.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //POPULAR 값 없거나 틀릴 때
        if(request.getSongDetailCodeDto().getPopularType() == null || (request.getSongDetailCodeDto().getPopularType() != YnTypeEnum.Y && request.getSongDetailCodeDto().getPopularType() != YnTypeEnum.N)){
            res.setResultCd(ResultCodeConst.NOT_VALID_POPULAR.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if(request.getSongDetailCodeDto().getVal() == null){
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }


        try {

            //seq 번호 불러옴
            int k = songDetailCodeRepository.getLastSeq(pcode) + 1;

            if(k < 10)
                dcode = pcode + "00000" + k;
            else if(k < 100)
                dcode = pcode + "0000" + k;
            else if(k < 1000)
                dcode = pcode + "000" + k;
            else if(k < 10000)
                dcode = pcode + "00" + k;
            else if(k < 100000)
                dcode = pcode + "0" + k;
            else if(k < 1000000)
                dcode = pcode + k;
            else
                dcode = pcode + k;

            SongDetailCodeEntity newSongDetailCode = SongDetailCodeEntity.builder()
                    .songCodeEntity(songCode.get())
                    .pcode(songCode.get().getCode())
                    .dcode(dcode)
                    .val(request.getSongDetailCodeDto().getVal())
                    .formatType(request.getSongDetailCodeDto().getFormatType())
                    .description(request.getSongDetailCodeDto().getDescription())
                    .sortIndex(request.getSongDetailCodeDto().getSortIndex())
                    .useType(request.getSongDetailCodeDto().getUseType())
                    .hit(0)    //hit는 강제로 0
                    .popularType(request.getSongDetailCodeDto().getPopularType())
                    .build();

            SongDetailCodeEntity saved = songDetailCodeRepository.save(newSongDetailCode);

            //seq 번호 수동으로 업데이트 시킨다
            songDetailCodeRepository.updateLastSeq(pcode, k);

//            Optional<SongDetailCodeEntity> songDetailCode = songDetailCodeRepository.findById(saved.getSongDetailCodeSeq());
//
//            Optional<SongDetailCodeDto> dtoPage =
//
//                    songDetailCode
//                            .map(p -> SongDetailCodeDto.builder()
//                                    .songDetailCodeSeq(p.getSongDetailCodeSeq())
//                                    .pcode(p.getSongCodeEntity().getCode())
//                                    .pcodeVal(p.getSongCodeEntity().getVal())
//                                    .dcode(p.getDcode())
//                                    .val(p.getVal())
//                                    .format(p.getFormat())
//                                    .useType(p.getUseType())
//                                    .popularType(p.getPopularType())
//                                    .description(p.getDescription())
//                                    .sortIndex(p.getSortIndex())
//                                    .hit(p.getHit())
//                                    .regUsr(p.getRegUsr())
//                                    .regDt(p.getRegDt())
//                                    .modUsr(p.getModUsr())
//                                    .modDt(p.getModDt())
//                                    .build()
//                            );

            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
//            res.setResult(dtoPage);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));

            String menuName = "";
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_CREATE.getCode());
            switch(saved.getPcode()){
                case "SGG" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_GENRE.getCode());
                    break;
                case "SGS" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_SUB_GENRE.getCode());
                    break;
                case "SGA" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_ALBUM.getCode());
                    break;
                case "SGT" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_THEME.getCode());
                    break;
                case "SGE" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_ERA.getCode());
                    break;
                case "SGP" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_TEMPO.getCode());
                    break;
                case "SGM" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_MOOD.getCode());
                    break;
                case "SGI" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_INSTRUMENT.getCode());
                    break;
                case "SGV" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_VERSION_TYPE.getCode());
                    break;
                case "SGC" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_VOCALS.getCode());
                    break;
                default:
                    menuName = "ETC CODE " + saved.getPcode();
                    break;
            }

            //netive query
            String usrName = adminActiveLogService.findQryUsrName(saved.getRegUsr());

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(saved.getRegDt())
                    .adminId(saved.getRegUsr())
                    .menuName(menuName)
                    .activeType("C")
                    .contentsId(saved.getSongDetailCodeSeq())
                    .activeMsg(usrName + " " + activeMsg + " '" + saved.getVal() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e){
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }

        return res;

    }

    @Transactional
    public Response<Void> updateYn(String pcode, Long id, HttpServletRequest req) {

        Response<Void> res = new Response<>();

        //부모코드 없을 때
        if(pcode == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<SongCodeEntity> songCode = Optional.ofNullable(
                songCodeRepository.findByCode(pcode)
        );

        logger.info("songCode - {}", songCode);

        //부모코드 조회내용 없을 때
        if(!songCode.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //id값이 없을 때
        if(id == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<SongDetailCodeEntity> songDetailCode = songDetailCodeRepository.findById(id);

        logger.info("songDetailCode - {}", songDetailCode);

        //조회내용 없을 때
        if(!songDetailCode.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        YnTypeEnum useType = songDetailCode.get().getUseType();
        YnTypeEnum newUseType = (useType == YnTypeEnum.Y) ? YnTypeEnum.N : YnTypeEnum.Y;

        logger.info("newUseType - {} ", newUseType);

        try {
            songDetailCode.get().updateYn(newUseType);

            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            //res.setResult(songDetailCode);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));

            String menuName = "";
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_MODIFY.getCode());
            switch(songDetailCode.get().getPcode()){
                case "SGG" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_GENRE.getCode());
                    break;
                case "SGS" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_SUB_GENRE.getCode());
                    break;
                case "SGA" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_ALBUM.getCode());
                    break;
                case "SGT" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_THEME.getCode());
                    break;
                case "SGE" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_ERA.getCode());
                    break;
                case "SGP" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_TEMPO.getCode());
                    break;
                case "SGM" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_MOOD.getCode());
                    break;
                case "SGI" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_INSTRUMENT.getCode());
                    break;
                case "SGV" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_VERSION_TYPE.getCode());
                    break;
                case "SGC" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_VOCALS.getCode());
                    break;
                default:
                    menuName = "ETC CODE " + songDetailCode.get().getPcode();
                    break;
            }

            //netive query
            String usrName = adminActiveLogService.findQryUsrName(songDetailCode.get().getModUsr());

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(songDetailCode.get().getModDt())
                    .adminId(songDetailCode.get().getModUsr())
                    .menuName(menuName)
                    .activeType("U")
                    .contentsId(songDetailCode.get().getSongDetailCodeSeq())
                    .activeMsg(usrName + " " + activeMsg + " '" + songDetailCode.get().getVal() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e) {
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }
        return res;
    }

    @Transactional
    public Response<Void> updatePopularYn(String pcode, Long id, HttpServletRequest req) {

        Response<Void> res = new Response<>();

        //부모코드 없을 때
        if(pcode == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<SongCodeEntity> songCode = Optional.ofNullable(
                songCodeRepository.findByCode(pcode)
        );

        logger.info("songCode - {}", songCode);

        //부모코드 조회내용 없을 때
        if(!songCode.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //id값이 없을 때
        if(id == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<SongDetailCodeEntity> songDetailCode = songDetailCodeRepository.findById(id);

        logger.info("songDetailCode - {}", songDetailCode);

        //조회내용 없을 때
        if(!songDetailCode.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        YnTypeEnum popularType = songDetailCode.get().getPopularType();
        YnTypeEnum newType = (popularType == YnTypeEnum.Y) ? YnTypeEnum.N : YnTypeEnum.Y;

        logger.info("newUseType - {} ", newType);

        try {
            songDetailCode.get().updatePopularYn(newType);
            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
//            res.setResult(dtoPage);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));

            String menuName = "";
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_MODIFY.getCode());
            switch(songDetailCode.get().getPcode()){
                case "SGG" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_GENRE.getCode());
                    break;
                case "SGS" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_SUB_GENRE.getCode());
                    break;
                case "SGA" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_ALBUM.getCode());
                    break;
                case "SGT" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_THEME.getCode());
                    break;
                case "SGE" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_ERA.getCode());
                    break;
                case "SGP" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_TEMPO.getCode());
                    break;
                case "SGM" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_MOOD.getCode());
                    break;
                case "SGI" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_INSTRUMENT.getCode());
                    break;
                case "SGV" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_VERSION_TYPE.getCode());
                    break;
                case "SGC" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_VOCALS.getCode());
                    break;
                default:
                    menuName = "ETC CODE " + songDetailCode.get().getPcode();
                    break;
            }

            //netive query
            String usrName = adminActiveLogService.findQryUsrName(songDetailCode.get().getModUsr());

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(songDetailCode.get().getModDt())
                    .adminId(songDetailCode.get().getModUsr())
                    .menuName(menuName)
                    .activeType("U")
                    .contentsId(songDetailCode.get().getSongDetailCodeSeq())
                    .activeMsg(usrName + " " + activeMsg + " '" + songDetailCode.get().getVal() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e) {
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }

        return res;
    }

    @Transactional
    public Response<Optional<SongDetailCodeDto>> updateContent(String pcode, Long id, SongDetailCodeRequest request, HttpServletRequest req) {

        Response<Optional<SongDetailCodeDto>> res = new Response<>();

        //부모코드 없을 때
        if(pcode == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<SongCodeEntity> songCode = Optional.ofNullable(
                songCodeRepository.findByCode(pcode)
        );

        logger.info("songCode - {} ", songCode);

        //부모코드 조회내용 없을 때
        if(!songCode.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //id값이 없을 때
        if(id == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<SongDetailCodeEntity> songDetailCode = songDetailCodeRepository.findById(id);

        logger.info("songDetailCode - {} ", songDetailCode);

        //조회내용 없을 때
        if(!songDetailCode.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Format 값 없거나 틀릴 때
        if(request.getSongDetailCodeDto().getFormatType() == null || (request.getSongDetailCodeDto().getFormatType() != FormatTypeEnum.DDEX && request.getSongDetailCodeDto().getFormatType() != FormatTypeEnum.SPARWK)){
            res.setResultCd(ResultCodeConst.NOT_VALID_FORMAT.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //USEYN 값 없거나 틀릴 때
        if(request.getSongDetailCodeDto().getUseType() == null || (request.getSongDetailCodeDto().getUseType() != YnTypeEnum.Y && request.getSongDetailCodeDto().getUseType() != YnTypeEnum.N)){
            res.setResultCd(ResultCodeConst.NOT_VALID_USEYN.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //POPULAR 값 없거나 틀릴 때
        if(request.getSongDetailCodeDto().getPopularType() == null || (request.getSongDetailCodeDto().getPopularType() != YnTypeEnum.Y && request.getSongDetailCodeDto().getPopularType() != YnTypeEnum.N)){
            res.setResultCd(ResultCodeConst.NOT_VALID_POPULAR.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if(request.getSongDetailCodeDto().getVal() == null){
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        try {

            //수정
            songDetailCode.get().update(request.getSongDetailCodeDto().getPcode(),
                    request.getSongDetailCodeDto().getDcode(),
                    request.getSongDetailCodeDto().getFormatType(),
                    request.getSongDetailCodeDto().getVal(),
                    request.getSongDetailCodeDto().getDescription(),
                    request.getSongDetailCodeDto().getSortIndex(),
                    request.getSongDetailCodeDto().getUseType(),
                    request.getSongDetailCodeDto().getPopularType()
            );

            //수정 재조회
            Optional<SongDetailCodeEntity> songDetailCodeUpdate =
                    songDetailCodeRepository.findById(songDetailCode.get().getSongDetailCodeSeq());

            //빌드
            Optional<SongDetailCodeDto> dtoPage =

                    songDetailCodeUpdate
                            .map(p -> SongDetailCodeDto.builder()
                                    .songDetailCodeSeq(p.getSongDetailCodeSeq())
                                    .pcode(p.getSongCodeEntity().getCode())
                                    .pcodeVal(p.getSongCodeEntity().getVal())
                                    .dcode(p.getDcode())
                                    .val(p.getVal())
                                    .formatType(p.getFormatType())
                                    .useType(p.getUseType())
                                    .popularType(p.getPopularType())
                                    .description(p.getDescription())
                                    .sortIndex(p.getSortIndex())
                                    .hit(p.getHit())
                                    .regUsr(p.getRegUsr())
                                    .regDt(p.getRegDt())
                                    .modUsr(p.getModUsr())
                                    .modDt(p.getModDt())
                                    .build()
                            );
            logger.info("dtoPage - {} ", dtoPage);

            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            res.setResult(dtoPage);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));

            String menuName = "";
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_MODIFY.getCode());
            switch(songDetailCode.get().getPcode()){
                case "SGG" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_GENRE.getCode());
                    break;
                case "SGS" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_SUB_GENRE.getCode());
                    break;
                case "SGA" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_ALBUM.getCode());
                    break;
                case "SGT" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_THEME.getCode());
                    break;
                case "SGE" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_ERA.getCode());
                    break;
                case "SGP" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_TEMPO.getCode());
                    break;
                case "SGM" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_MOOD.getCode());
                    break;
                case "SGI" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_INSTRUMENT.getCode());
                    break;
                case "SGV" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_VERSION_TYPE.getCode());
                    break;
                case "SGC" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_VOCALS.getCode());
                    break;
                default:
                    menuName = "ETC CODE " + songDetailCode.get().getPcode();
                    break;
            }

            //netive query
            String usrName = adminActiveLogService.findQryUsrName(songDetailCode.get().getModUsr());

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(songDetailCode.get().getModDt())
                    .adminId(songDetailCode.get().getModUsr())
                    .menuName(menuName)
                    .activeType("U")
                    .contentsId(songDetailCode.get().getSongDetailCodeSeq())
                    .activeMsg(usrName + " " + activeMsg + " '" + songDetailCode.get().getVal() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e){
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }

        return res;
    }

    @Transactional
    public Response<Void> deleteContent(String pcode, Long id, HttpServletRequest req) {

        Response<Void> res = new Response<>();

        //부모코드 없을 때
        if(pcode == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<SongCodeEntity> songCode = Optional.ofNullable(
                songCodeRepository.findByCode(pcode)
        );

        logger.info("songCode - {} ", songCode);

        //부모코드 조회내용 없을 때
        if(!songCode.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //id값이 없을 때
        if(id == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<SongDetailCodeEntity> songDetailCode = songDetailCodeRepository.findById(id);

        logger.info("songDetailCode - {} ", songDetailCode);

        //조회내용 없을 때
        if(!songDetailCode.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        try {
            songDetailCodeRepository.deleteById(id);
            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
//            res.setResult(dtoPage);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));
            Long adminId = getAccountId.ofId(httpServletRequest);

            String menuName = "";
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_DELETE.getCode());
            switch(songDetailCode.get().getPcode()){
                case "SGG" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_GENRE.getCode());
                    break;
                case "SGS" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_SUB_GENRE.getCode());
                    break;
                case "SGA" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_ALBUM.getCode());
                    break;
                case "SGT" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_THEME.getCode());
                    break;
                case "SGE" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_ERA.getCode());
                    break;
                case "SGP" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_TEMPO.getCode());
                    break;
                case "SGM" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_MOOD.getCode());
                    break;
                case "SGI" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_INSTRUMENT.getCode());
                    break;
                case "SGV" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_VERSION_TYPE.getCode());
                    break;
                case "SGC" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_VOCALS.getCode());
                    break;
                default:
                    menuName = "ETC CODE " + songDetailCode.get().getPcode();
                    break;
            }

            //netive query
            String usrName = adminActiveLogService.findQryUsrName(adminId);

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(LocalDateTime.now())
                    .adminId(adminId)
                    .menuName(menuName)
                    .activeType("D")
                    .contentsId(songDetailCode.get().getSongDetailCodeSeq())
                    .activeMsg(usrName + " " + activeMsg + " '" + songDetailCode.get().getVal() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e) {
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }
        return res;
    }

    public List<SongDetailCodeExcelDto> getExcelList(String pcode) {

        List<SongDetailCodeExcelDto> getExcelList = songDetailCodeRepository.findExcelList(pcode);

        logger.info("getExcelList - {} ", getExcelList);

        return getExcelList;
    }


    //엑셀 다운로드 요청
    public Map<String, Object> downExcelList(String songCode) throws Exception {
        // 데이터 가져오기
        List<SongDetailCodeExcelDto> excelList = this.getExcelList(songCode);

        logger.info("excelList - {} ", excelList);

        return ExcelWriter.createExcelData(excelList, SongDetailCodeExcelDto.class);
    }

    //마지막 코드 번호 확인
    public Integer getLastSeq(String code) {

        return songDetailCodeRepository.getLastSeq(code);
    }

    //코드 번호 업데이트
    @Transactional
    public void updateLastSeq(String code, Integer k) {

        songDetailCodeRepository.updateLastSeq(code, k);
    }

    //이름으로 코드검색
    public Long getOneName(String val) {

        return songDetailCodeRepository.countByVal(val);
    }


    @Transactional
    public Response<Void> saveExcelContent(String pcode, SongDetailCodeDto songDetailCodeDto, HttpServletRequest req) {

        Response<Void> res = new Response<>();

        Optional<SongCodeEntity> songCode = Optional.ofNullable(songCodeRepository.findByCode(pcode));

        logger.info("songCode - {}", songCode);

        //부모코드 조회내용 없을 때
        if (!songCode.isPresent() || songCode.equals("")) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //USEYN 값 없거나 틀릴 때
        if (songDetailCodeDto.getUseType() == null || (songDetailCodeDto.getUseType() != YnTypeEnum.Y && songDetailCodeDto.getUseType() != YnTypeEnum.N)) {
            res.setResultCd(ResultCodeConst.NOT_VALID_USEYN.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //POPULAR 값 없거나 틀릴 때
        if (songDetailCodeDto.getPopularType() == null || (songDetailCodeDto.getPopularType() != YnTypeEnum.Y && songDetailCodeDto.getPopularType() != YnTypeEnum.N)) {
            res.setResultCd(ResultCodeConst.NOT_VALID_POPULAR.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if (songDetailCodeDto.getVal() == null) {
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        try {

            SongDetailCodeEntity newSongDetailCode = SongDetailCodeEntity.builder()
                    .songCodeEntity(songCode.get())
                    .pcode(songCode.get().getCode())
                    .dcode(songDetailCodeDto.getDcode())
                    .val(songDetailCodeDto.getVal())
                    .formatType(songDetailCodeDto.getFormatType())
                    .description(songDetailCodeDto.getDescription())
                    .sortIndex(songDetailCodeDto.getSortIndex())
                    .useType(songDetailCodeDto.getUseType())
                    .popularType(YnTypeEnum.N)
                    .hit(0)
                    .build();

            SongDetailCodeEntity saved = songDetailCodeRepository.save(newSongDetailCode);

            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
//            res.setResult(dtoPage);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));

            String menuName = "";
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_CREATE.getCode());
            switch(saved.getPcode()){
                case "SGG" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_GENRE.getCode());
                    break;
                case "SGS" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_SUB_GENRE.getCode());
                    break;
                case "SGA" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_ALBUM.getCode());
                    break;
                case "SGT" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_THEME.getCode());
                    break;
                case "SGE" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_ERA.getCode());
                    break;
                case "SGP" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_TEMPO.getCode());
                    break;
                case "SGM" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_MOOD.getCode());
                    break;
                case "SGI" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_INSTRUMENT.getCode());
                    break;
                case "SGV" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_VERSION_TYPE.getCode());
                    break;
                case "SGC" :
                    menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_VOCALS.getCode());
                    break;
                default:
                    menuName = "ETC CODE " + saved.getPcode();
                    break;
            }

            //netive query
            String usrName = adminActiveLogService.findQryUsrName(saved.getRegUsr());

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(saved.getRegDt())
                    .adminId(saved.getRegUsr())
                    .menuName(menuName)
                    .activeType("C")
                    .contentsId(saved.getSongDetailCodeSeq())
                    .activeMsg(usrName + " " + activeMsg + " '" + saved.getVal() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);
            adminActiveLogService.saveContent(newLog);

        } catch (Exception e) {
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }

        return res;
    }
}
