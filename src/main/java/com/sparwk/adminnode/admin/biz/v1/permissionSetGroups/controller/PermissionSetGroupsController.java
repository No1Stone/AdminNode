package com.sparwk.adminnode.admin.biz.v1.permissionSetGroups.controller;

import com.sparwk.adminnode.admin.biz.v1.permissionSetGroups.dto.PermissionSetGroupsDto;
import com.sparwk.adminnode.admin.biz.v1.permissionSetGroups.dto.PermissionSetGroupsRequest;
import com.sparwk.adminnode.admin.biz.v1.permissionSetGroups.service.PermissionSetGroupsService;
import com.sparwk.adminnode.admin.config.common.AdminPermissionIntergrationService;
import com.sparwk.adminnode.admin.config.common.Response;
import com.sparwk.adminnode.admin.config.common.ResultCodeConst;
import com.sparwk.adminnode.admin.jpa.entity.PeriodTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.permissionSetGroups.PermissionSetGroupsCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.permissionSetGroups.PermissionSetGroupsSorterEnum;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/V1/setting/administrator/")
@RequiredArgsConstructor
@CrossOrigin("*")
@ApiIgnore      //스웨거 API 예외처리
public class PermissionSetGroupsController {

    private final Logger logger = LoggerFactory.getLogger(PermissionSetGroupsController.class);

    @Autowired
    private PermissionSetGroupsService permissionSetGroupsService;

    @Autowired
    private AdminPermissionIntergrationService adminPermissionIntergrationService;
    private final MessageSourceAccessor messageSourceAccessor;

    @GetMapping("permission-set-groups")
    @Operation(
            summary = "PermissionSetGroups All List", description = "PermissionSetGroups All List 입니다.",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = PermissionSetGroupsDto.class))),
                    @ApiResponse(
                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
                            , content = @Content(schema = @Schema(implementation = PermissionSetGroupsDto.class)))
            })
    public Response<List<PermissionSetGroupsDto>> getList(
            @Parameter(name = "cate", description = "검색 종류", in = ParameterIn.PATH) @RequestParam(value = "cate", required = false) PermissionSetGroupsCateEnum cate,
            @Parameter(name = "labelVal", description = "코드 값 포함", in = ParameterIn.PATH) @RequestParam(value = "labelVal", required = false) String labelVal,
            @Parameter(name = "useType", description = "사용여부 값 Y/N", in = ParameterIn.PATH) @RequestParam(value = "useType", required = false) YnTypeEnum useType,
            @Parameter(name = "periodType", description = "기간 종류", in = ParameterIn.PATH) @RequestParam(value = "periodType", required = false) PeriodTypeEnum periodType,
            @Parameter(name = "sdate", description = "시작일", in = ParameterIn.PATH) @RequestParam(value = "sdate", required = false) String sdate,
            @Parameter(name = "edate", description = "종료일", in = ParameterIn.PATH) @RequestParam(value = "edate", required = false) String edate,
            @Parameter(name = "sorter", description = "순서", in = ParameterIn.PATH) @RequestParam(value = "sorter", required = false) PermissionSetGroupsSorterEnum sorter,
            @Parameter(name = "page", description = "페이지 번호", in = ParameterIn.PATH) @RequestParam(value = "page", required = false, defaultValue = "1") int page,
            @Parameter(name = "size", description = "페이지 사이즈", in = ParameterIn.PATH) @RequestParam(value = "size", required = false, defaultValue = "10") int size
    ) {
        PageRequest pageRequest = PageRequest.of(page, size);

        Response res = new Response();
        if(!adminPermissionIntergrationService.AdminPermissionIntergrationCheck(3L, "R")){
            res.setResultCd(ResultCodeConst.NOT_VALID_READ.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        return permissionSetGroupsService.getList(cate, labelVal, useType, periodType, sdate, edate, sorter, pageRequest);
    }


    @GetMapping("permission-set-groups/use")
    @Operation(
            summary = "PermissionSetGroups Use List", description = "PermissionSetGroups Use List 입니다.",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = PermissionSetGroupsDto.class))),
                    @ApiResponse(
                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
                            , content = @Content(schema = @Schema(implementation = PermissionSetGroupsDto.class)))
            })
    public Response<List<PermissionSetGroupsDto>> getUseYList(
            @Parameter(name = "cate", description = "검색 종류", in = ParameterIn.PATH) @RequestParam(value = "cate", required = false) PermissionSetGroupsCateEnum cate,
            @Parameter(name = "labelVal", description = "검색어 포함", in = ParameterIn.PATH) @RequestParam(value = "labelVal", required = false) String labelVal
    ) {
        Response res = new Response();
        if(!adminPermissionIntergrationService.AdminPermissionIntergrationCheck(3L, "R")){
            res.setResultCd(ResultCodeConst.NOT_VALID_READ.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        return permissionSetGroupsService.getUseYList(cate, labelVal);
    }

    //상세페이지
    @GetMapping("permission-set-groups/{id}")
    @Operation(
            summary = "PermissionSetGroups Detail View", description = "PermissionSetGroups Detail View 입니다.",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = PermissionSetGroupsDto.class))),
                    @ApiResponse(
                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
                            , content = @Content(schema = @Schema(implementation = PermissionSetGroupsDto.class))),
                    @ApiResponse(
                            responseCode = "1003", description = "id가 존재하지 않습니다.."
                            , content = @Content(schema = @Schema(implementation = PermissionSetGroupsDto.class)))
            })
    public Response<Optional<PermissionSetGroupsDto>> getOne(
            @Parameter(name = "id", description = "id 값", in = ParameterIn.PATH) @PathVariable("id") Long id
    ) {
        Response res = new Response();
        if(!adminPermissionIntergrationService.AdminPermissionIntergrationCheck(3L, "R")){
            res.setResultCd(ResultCodeConst.NOT_VALID_READ.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        return permissionSetGroupsService.getOne(id);
    }

    //쓰기처리
    @PostMapping("permission-set-groups/new")
    @Operation(
            summary = "New PermissionSetGroups", description = "PermissionSetGroups 추가합니다.",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = PermissionSetGroupsDto.class))),
                    @ApiResponse(
                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
                            , content = @Content(schema = @Schema(implementation = PermissionSetGroupsDto.class))),
                    @ApiResponse(
                            responseCode = "1203", description = "Use Type 값 형식이 잘못되었습니다."
                            , content = @Content(schema = @Schema(implementation = PermissionSetGroupsDto.class))),
                    @ApiResponse(
                            responseCode = "1206", description = "Value 형식이 잘못되었습니다."
                            , content = @Content(schema = @Schema(implementation = PermissionSetGroupsDto.class)))
            })
    public Response<Void> saveContent(
            @Parameter(name = "request", description = "Request Dto", in = ParameterIn.PATH) @RequestBody @Valid PermissionSetGroupsRequest request,
            HttpServletRequest req
    ) {
        Response res = new Response();
        if(!adminPermissionIntergrationService.AdminPermissionIntergrationCheck(3L, "C")){
            res.setResultCd(ResultCodeConst.NOT_VALID_CREATE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        return permissionSetGroupsService.saveContent(request, req);
    }

    //사용여부 토글처리
    @PostMapping("permission-set-groups/{id}/use")
    @Operation(
            summary = "Use Y/N", description = "PermissionSetGroups 사용/미사용을 처리합니다.",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = PermissionSetGroupsDto.class))),
                    @ApiResponse(
                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
                            , content = @Content(schema = @Schema(implementation = PermissionSetGroupsDto.class))),
                    @ApiResponse(
                            responseCode = "1003", description = "id가 존재하지 않습니다."
                            , content = @Content(schema = @Schema(implementation = PermissionSetGroupsDto.class)))
            })
    public Response<Void> updateYn(
            @Parameter(name = "id", description = "id 값", in = ParameterIn.PATH) @PathVariable("id") Long id, HttpServletRequest req
    ) {
        Response res = new Response();
        if(!adminPermissionIntergrationService.AdminPermissionIntergrationCheck(3L, "U")){
            res.setResultCd(ResultCodeConst.NOT_VALID_UPDATE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        return permissionSetGroupsService.updateYn(id, req);
    }

    //업데이트처리
    @PostMapping("permission-set-groups/{id}/modify")
    @Operation(
            summary = "Modify PermissionSetGroups", description = "PermissionSetGroups 수정합니다.",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = PermissionSetGroupsDto.class))),
                    @ApiResponse(
                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
                            , content = @Content(schema = @Schema(implementation = PermissionSetGroupsDto.class))),
                    @ApiResponse(
                            responseCode = "1003", description = "id가 존재하지 않습니다."
                            , content = @Content(schema = @Schema(implementation = PermissionSetGroupsDto.class))),
                    @ApiResponse(
                            responseCode = "1203", description = "Use Type 값 형식이 잘못되었습니다."
                            , content = @Content(schema = @Schema(implementation = PermissionSetGroupsDto.class))),
                    @ApiResponse(
                            responseCode = "1206", description = "Value 형식이 잘못되었습니다."
                            , content = @Content(schema = @Schema(implementation = PermissionSetGroupsDto.class)))
            })
    public Response<Optional<PermissionSetGroupsDto>> updateContent(
            @Parameter(name = "id", description = "id 값", in = ParameterIn.PATH) @PathVariable("id") Long id,
            @Parameter(name = "request", description = "Request Dto", in = ParameterIn.PATH) @RequestBody @Valid PermissionSetGroupsRequest request,
            HttpServletRequest req
    ) {
        Response res = new Response();
        if(!adminPermissionIntergrationService.AdminPermissionIntergrationCheck(3L, "U")){
            res.setResultCd(ResultCodeConst.NOT_VALID_UPDATE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        return permissionSetGroupsService.updateContent(id, request, req);
    }

    //삭제처리
    @DeleteMapping("permission-set-groups/{id}/delete")
    @Operation(
            summary = "delete PermissionSetGroups", description = "PermissionSetGroups 삭제합니다.",
            responses = {
                    @ApiResponse(
                            responseCode = "0000", description = "성공하였습니다."
                            , content = @Content(schema = @Schema(implementation = PermissionSetGroupsDto.class))),
                    @ApiResponse(
                            responseCode = "0010", description = "정보를 찾을 수 없습니다."
                            , content = @Content(schema = @Schema(implementation = PermissionSetGroupsDto.class))),
                    @ApiResponse(
                            responseCode = "1003", description = "id가 존재하지 않습니다."
                            , content = @Content(schema = @Schema(implementation = PermissionSetGroupsDto.class)))
            })
    public Response<Void> delete(
            @Parameter(name = "id", description = "id 값", in = ParameterIn.PATH) @PathVariable("id") Long id,
            HttpServletRequest req
    ) {
        Response res = new Response();
        if(!adminPermissionIntergrationService.AdminPermissionIntergrationCheck(3L, "D")){
            res.setResultCd(ResultCodeConst.NOT_VALID_CREATE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        return permissionSetGroupsService.deleteContent(id, req);
    }

}
