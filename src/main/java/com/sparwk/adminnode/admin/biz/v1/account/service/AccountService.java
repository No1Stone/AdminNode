package com.sparwk.adminnode.admin.biz.v1.account.service;

import com.sparwk.adminnode.admin.biz.v1.CommonPagingListDto;
import com.sparwk.adminnode.admin.biz.v1.account.dto.*;
import com.sparwk.adminnode.admin.biz.v1.profile.dto.*;
import com.sparwk.adminnode.admin.config.common.Response;
import com.sparwk.adminnode.admin.config.common.ResultCodeConst;
import com.sparwk.adminnode.admin.jpa.entity.PeriodTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.account.*;
import com.sparwk.adminnode.admin.jpa.entity.country.CountryEntity;
import com.sparwk.adminnode.admin.jpa.entity.profile.ProfileCompanyEntity;
import com.sparwk.adminnode.admin.jpa.entity.profile.ProfileEntity;
import com.sparwk.adminnode.admin.jpa.repository.account.AccountAssoiateViewRepository;
import com.sparwk.adminnode.admin.jpa.repository.account.AccountCompanyRepository;
import com.sparwk.adminnode.admin.jpa.repository.account.AccountCompanyViewRepository;
import com.sparwk.adminnode.admin.jpa.repository.account.AccountRepository;
import com.sparwk.adminnode.admin.jpa.repository.country.CountryRepository;
import com.sparwk.adminnode.admin.jpa.repository.profile.ProfileCompanyRepository;
import com.sparwk.adminnode.admin.jpa.repository.profile.ProfileRepository;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class AccountService {

    private final AccountRepository accountRepository;
    private final AccountAssoiateViewRepository accountAssoiateViewRepository;
    private final AccountCompanyRepository accountCompanyRepository;
    private final AccountCompanyViewRepository accountCompanyViewRepository;
    private final ProfileRepository profileRepository;
    private final ProfileCompanyRepository profileCompanyRepository;
    private final CountryRepository countryRepository;

    private final MessageSourceAccessor messageSourceAccessor;
    
    private final Logger logger = LoggerFactory.getLogger(AccountService.class);

    //비인가인원 리스트
    public Response<CommonPagingListDto> getAssoiateUserList(MaintenanceAssociateMemberCateEnum cate,
                                                             String val, PeriodTypeEnum periodType, String sdate, String edate, MaintenanceAssociateMemberSorterEnum sorter, PageRequest pageRequest) {

        Response<CommonPagingListDto> res = new Response<>();

        //검색 시 회사로 조회. 포함 안되면 해당 내용 삭제
        List<AssoiateMemberViewListDto> accoList = accountAssoiateViewRepository.findQryAssociateMember(cate, val, periodType, sdate, edate, sorter, pageRequest);

        logger.info("accoList - {}", accoList);

        //조회내용 없을 때
        if(accoList == null || accoList.size() < 1){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        CommonPagingListDto list = new CommonPagingListDto();

        //총 카운트
        long totalSize = accountAssoiateViewRepository.countQryAssociateMember(cate,
                val,
                periodType,
                sdate,
                edate);
        long totalPage = totalSize / pageRequest.getPageSize();
        long mod = totalSize % pageRequest.getPageSize();
        if(mod > 0) {
            totalPage += 1;
        }
        list.setList(accoList);
        list.setSize(pageRequest.getPageSize());
        list.setPage(pageRequest.getPageNumber());
        list.setTotalPage(totalPage);
        list.setTotalSize(totalSize);

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(list);
        return res;

    }

    //인가인원 리스트
    public Response<CommonPagingListDto> getVerifiedUserList(MaintenanceVerifiedMemberCateEnum cate,
                                                                     String val, PeriodTypeEnum periodType, String sdate, String edate, MaintenanceVerifiedMemberSorterEnum sorter, PageRequest pageRequest) {

        Response<CommonPagingListDto> res = new Response<>();

        //검색 시 회사로 조회. 포함 안되면 해당 내용 삭제
        List<VerifiedMemberListDto> accoList = accountRepository.findQryVerifiedMember(cate,
                val,
                periodType,
                sdate,
                edate,
                sorter,
                pageRequest);

        logger.info("accoList - {}", accoList);

//
//        for (int i = 0; i < accoList.size(); i++) {
//
//            //젠더 리스트 String으로 추가
//            List<ProfileGenderDto> genderList = accountRepository.findQryGender(accoList.get(i).getProfileId());
//            accoList.get(i).setGenderList(genderList);
//        }

        //조회내용 없을 때
        if(accoList == null || accoList.size() < 1){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        CommonPagingListDto list = new CommonPagingListDto();

        //총 카운트
        long totalSize = accountRepository.countQryVerifiedMember(cate,
                val,
                periodType,
                sdate,
                edate);
        long totalPage = totalSize / pageRequest.getPageSize();
        long mod = totalSize % pageRequest.getPageSize();
        if(mod > 0) {
            totalPage += 1;
        }
        list.setList(accoList);
        list.setSize(pageRequest.getPageSize());
        list.setPage(pageRequest.getPageNumber());
        list.setTotalPage(totalPage);
        list.setTotalSize(totalSize);

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(list);
        return res;
    }

    //인원 상세페이지
    public Response<Optional<AccountMemberDetailDto>> getUserOne(Long id) {

        Response<Optional<AccountMemberDetailDto>> res = new Response<>();

        //id값이 없을 때
        if(id == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<ProfileEntity> profile = profileRepository.findById(id);

        logger.info("profile - {}", profile);

        //조회내용 없을 때
        if(!profile.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<AccountEntity> account = accountRepository.findById(profile.get().getAccntId());

        logger.info("account - {}", account);

        //조회내용 없을 때
        if(!account.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<AccountPassportDto> accountPassport = accountRepository.finQryPassport(profile.get().getAccntId());

        logger.info("accountPassport - {}", accountPassport);

        //조회내용 없을 때
        if(!accountPassport.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        String accountPassportArr[] = accountPassport.get().getPassportImgFileUrl().split("/");
        String accountPassportFile = accountPassportArr[accountPassportArr.length - 1];

        //
        Optional<ProfileMetaDataDto> getCurrentCityCountryCdName = accountRepository.findQryCurrentCountry(profile.get().getProfileId());
        //
        Optional<ProfileMetaDataDto> getHometownCityCountryCdName = accountRepository.findQryHomeTownCountry(profile.get().getProfileId());
        //
        Optional<ProfileMetaDataDto> getPhoneCountryCdName = accountRepository.findQryPhoneCountryCdName(profile.get().getAccntId());


        //SNS 리스트
        List<ProfileSnsDto> getSnsMetaData = accountRepository.findQrySns(account.get().getAccntId());
        //젠더 리스트
        List<ProfileMetaDataDto> getGenderMetaData = accountRepository.findQryGender(profile.get().getProfileId());
        //랭귀지 리스트
        List<ProfileMetaDataDto> getLanguageMetaData = accountRepository.findQryLanuage(profile.get().getProfileId());
        //Current Position 리스트
        List<ProfileCurrentPositionDto> getPositionMetaData = accountRepository.findQryPosition(profile.get().getProfileId());
        //roles 리스트
        List<ProfileMetaDataDto> getRolesMetaData = accountRepository.findQryRoles(profile.get().getProfileId());
        //roles 리스트
        List<ProfileMetaDataDto> getTerritorisCountryMetaData = accountRepository.findQryTerritorisOrCountry(profile.get().getProfileId());
        //Software 리스트
        List<ProfileMetaDataDto> getSoftwareData = accountRepository.findQryCommon("SSF", profile.get().getProfileId());
        //Song Genre Code 리스트
        List<ProfileMetaDataDto> getGenreMetaData = accountRepository.findQrySongs("SGG", profile.get().getProfileId());
        //Song SubGenre Code 리스트
        List<ProfileMetaDataDto> getSubGenreMetaData = accountRepository.findQrySongs("SGS", profile.get().getProfileId());
        //Song Moods Code 리스트
        List<ProfileMetaDataDto> getMoodsMetaData = accountRepository.findQrySongs("SGM", profile.get().getProfileId());
        //Song Theme Code 리스트
        List<ProfileMetaDataDto> getThemeMetaData = accountRepository.findQrySongs("SGT", profile.get().getProfileId());
        //Song Vocals Code 리스트
        List<ProfileMetaDataDto> getVocalsMetaData = accountRepository.findQrySongs("SGC", profile.get().getProfileId());
        //Song Era Code 리스트
        List<ProfileMetaDataDto> getEraMetaData = accountRepository.findQrySongs("SGE", profile.get().getProfileId());
        //Song Instrument Code 리스트
        List<ProfileMetaDataDto> getInstrumentMetaData = accountRepository.findQrySongs("SGI", profile.get().getProfileId());
        //Song Tempo Code 리스트
        List<ProfileMetaDataDto> getTempoMetaData = accountRepository.findQrySongs("SGP", profile.get().getProfileId());

        Optional<AccountMemberDetailDto> dtoPage =

                profile.map(p -> AccountMemberDetailDto.builder()
                            .accntId(p.getAccntId())
                            .profileId(p.getProfileId())
                            .accntTypeCd(account.get().getAccntTypeCd())
                            .accntFirstName(accountPassport.get().getPassportFirstName())
                            .accntMiddleName(accountPassport.get().getPassportMiddleName())
                            .accntLastName(accountPassport.get().getPassportLastName())
                            .profileFullName(p.getFullName())
                            .stageNameYn(p.getStageNameYn())
                            .accntEmail(account.get().getAccntEmail())
                            .bthYear(p.getBthYear())
                            .bthMonth(p.getBthMonth())
                            .bthDay(p.getBthDay())
                            .genderList(getGenderMetaData)
                            .languageList(getLanguageMetaData)
                            .currentCityCountryCd(p.getCurrentCityCountryCd())
                            .currentCityCountryCdName(getCurrentCityCountryCdName.get().getAttrDtlCdVal())
                            .currentCityNm(p.getCurrentCityNm())
                            .hometownCountryCd(p.getHomeTownCountryCd())
                            .hometownCountryCdName(getHometownCityCountryCdName.get().getAttrDtlCdVal())
                            .hometownCityNm(p.getHomeTownNm())
                            .passportCountryCd(accountPassport.get().getPassportCountryCd())
                            .passportCountryCdName(accountPassport.get().getPassportCountryCdName())
                            .passportPhotocopy(accountPassport.get().getPassportImgFileUrl())
                            .passportfile(accountPassportFile)
                            .accountVerifyYn(account.get().getVerifyYn())
                            .phoneCountryCd(account.get().getCountryCd())
                            .phoneCountryCdName(getPhoneCountryCdName.get().getAttrDtlCdVal())
                            .phoneNumber(account.get().getPhoneNumber())
                            .accountVerifyPhoneYn(account.get().getVerifyPhoneYn())
                            .profileCurrentPositionDtoList(getPositionMetaData)
                            .headline(p.getHeadline())
                            .rolesList(getRolesMetaData)
                            .territoriesList(getTerritorisCountryMetaData)
                            .softwareList(getSoftwareData)
                            .genresList(getGenreMetaData)
                            .subGenresList(getSubGenreMetaData)
                            .moodsList(getMoodsMetaData)
                            .themesList(getThemeMetaData)
                            .vocalList(getVocalsMetaData)
                            .eraList(getEraMetaData)
                            .instrumentsList(getInstrumentMetaData)
                            .tempoaList(getTempoMetaData)
                            .snsList(getSnsMetaData)
                            .ipiNumber(p.getIpiInfo())
                            .caeNumber(p.getCaeInfo())
                            .isniNumber(p.getIsniInfo())
                            .ipnNumber(p.getIpnInfo())
                            .nroNumber(p.getNroInfo())
                            .regDt(p.getRegDt())
                            .build()
                );

        logger.info("SongDetailCode - {}", dtoPage);

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(dtoPage);
        return res;
    }



    //인가회사 리스트
    public Response<CommonPagingListDto> getVerifiedComList(MaintenanceOfficeMemberCateEnum cate,
                                                           String val, PeriodTypeEnum periodType, String sdate, String edate, MaintenanceOfficeMemberSorterEnum sorter, PageRequest pageRequest) {

        Response<CommonPagingListDto> res = new Response<>();

        //검색 시 회사로 조회. 포함 안되면 해당 내용 삭제
        List<CompanyMemberViewListDto> companyList = accountCompanyViewRepository.findQryOfficeMember(cate,
                val,
                periodType,
                sdate,
                edate,
                sorter,
                pageRequest);

        logger.info("companyList - {}", companyList);

        //조회내용 없을 때
        if(companyList == null || companyList.size() < 1){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }


        CommonPagingListDto list = new CommonPagingListDto();

        //총 카운트
        long totalSize = accountCompanyViewRepository.countQryOfficeMember(cate,
                val,
                periodType,
                sdate,
                edate);
        long totalPage = totalSize / pageRequest.getPageSize();
        long mod = totalSize % pageRequest.getPageSize();
        if(mod > 0) {
            totalPage += 1;
        }
        list.setList(companyList);
        list.setSize(pageRequest.getPageSize());
        list.setPage(pageRequest.getPageNumber());
        list.setTotalPage(totalPage);
        list.setTotalSize(totalSize);

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(list);
        return res;
    }

    //상세페이지
    public Response<Optional<CompanyMemberDetailDto>> getComOne(Long id) {

        Response<Optional<CompanyMemberDetailDto>> res = new Response<>();

        //id값이 없을 때
        if(id == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<ProfileCompanyEntity> profileCompnay = profileCompanyRepository.findById(id);

        logger.info("profileCompnay - {}", profileCompnay);

        //조회내용 없을 때
        if(!profileCompnay.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<AccountCompanyDetailEntity> accountCompany = accountCompanyRepository.findById(profileCompnay.get().getAccntId());

        logger.info("accountCompany - {}", accountCompany);

        //조회내용 없을 때
        if(!accountCompany.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<AccountEntity> account = accountRepository.findById(profileCompnay.get().getAccntId());

        logger.info("account - {}", account);

        //조회내용 없을 때
        if(!account.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Company Type (Music publisher...)
        List<AccountCompanyTypeDto> getTypeMetaData = accountCompanyRepository.finQryCompanyType(accountCompany.get().getAccntId());
        //Service Location
        List<AccountCompanyLocationDto> getLocationMetaData = accountCompanyRepository.finQryCompanyLocation(accountCompany.get().getAccntId());
        //On the web 리스트
        List<ProfileCompanySnsDto> getSnsMetaData = accountCompanyRepository.findQryCompanySns(profileCompnay.get().getProfileId());
        //Roster 리스트
        List<ProfileCompanyRosteredDto> getRosterMetaData = accountCompanyRepository.findQryCompanyRoster(profileCompnay.get().getProfileId());
        //Partner 리스트
        List<ProfileCompanyPartnerDto> getPartnerMetaData = accountCompanyRepository.findQryCompanyPartner(profileCompnay.get().getProfileId());
        for(ProfileCompanyPartnerDto ProfileCompanyPartner : getPartnerMetaData){

            List<AccountCompanyTypeDto> partnerCompanyTypeList = accountCompanyRepository.finQryCompanyType(ProfileCompanyPartner.getPartnerAccntId());

            ProfileCompanyPartner.setPartnerCompanyTypeDtoList(partnerCompanyTypeList);
        }


        //Studio 리스트
        List<ProfileCompanyStudioDto> getStudioMetaData = accountCompanyRepository.findQryCompanyStudio(profileCompnay.get().getProfileId());

        //contact person
        Optional<ProfileCompanyContactDto> companyContact  = Optional.ofNullable(accountCompanyRepository.findQryCompanyConcat(profileCompnay.get().getProfileId()));
        logger.info("companyContact - {}", companyContact);

        //조회내용 없을 때
        if(!companyContact.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        String accountCompanyTypeFileUrl = getTypeMetaData.get(0).getCompanyLicenseFileUrl();
        String accountCompanyTypeFileArr[] = accountCompanyTypeFileUrl.split("/");
        String accountCompanyTypeFile = accountCompanyTypeFileArr[accountCompanyTypeFileArr.length - 1];

        CountryEntity countryEntity = countryRepository.findByCountryCd(companyContact.get().getCountryCd());
        String countryDial = countryEntity.getDial();
        String countryIso2 = countryEntity.getIso2();
        String companyPhone = "(" + countryIso2 + ")"
                + "+" + countryDial
                + " " + companyContact.get().getContctPhoneNumber();

        Optional<CompanyMemberDetailDto> dtoPage =

                profileCompnay.map(p -> CompanyMemberDetailDto.builder()
                        .accntId(p.getAccntId())
                        .profileId(p.getProfileId())
                        .companyName(accountCompany.get().getCompanyName())
                        .companyEmail(p.getComInfoEmail())
                        .accountCompanyTypeDtoList(getTypeMetaData)
                        .accountCompanyLocationDtoList(getLocationMetaData)
                        .companyPost(accountCompany.get().getPostCd())
                        .companyRegion(accountCompany.get().getRegion())
                        .companyCity(accountCompany.get().getCity())
                        .companyAddr1(accountCompany.get().getAddr1())
                        .companyAddr2(accountCompany.get().getAddr2())
                        .headline(p.getHeadline())
                        .companyLicenseFileUrl(getTypeMetaData.get(0).getCompanyLicenseFileUrl())
                        .companyLicenseFile(accountCompanyTypeFile)
                        .subsidiaryCompanies("")
                        .affiliatedCompanies("")
                        .primaryContactFirstName(accountCompany.get().getContctFirstName())
                        .primaryContactMiddleName(accountCompany.get().getContctMidleName())
                        .primaryContactLastName(accountCompany.get().getContctLastName())
                        .primaryContactEmail(accountCompany.get().getContctEmail())
                        .primaryContactPhone(companyPhone)
                        .companyLegalName(p.getProfileCompanyName())
                        .profileCompanyOnthewebDtoList(getSnsMetaData)
                        .companyIpiNo(p.getIpiNumber())
                        .companyVatNo(p.getVatNumber())
                        .profileCompanyRosteredDto(getRosterMetaData)
                        .profileCompanyPartnerDto(getPartnerMetaData)
                        .profileCompanyStudioDto(getStudioMetaData)
                        .build()
                );

        logger.info("SongDetailCode - {}", dtoPage);

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(dtoPage);
        return res;
    }
}
