package com.sparwk.adminnode.admin.biz.v1.permissionSet.service;

import com.sparwk.adminnode.admin.biz.v1.permissionSet.dto.AdminMenuDto;
import com.sparwk.adminnode.admin.biz.v1.permissionSet.dto.PermissionMenuSetDto;
import com.sparwk.adminnode.admin.config.common.Response;
import com.sparwk.adminnode.admin.config.common.ResultCodeConst;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.permissionSet.AdminMenuEntity;
import com.sparwk.adminnode.admin.jpa.entity.permissionSet.PermissionMenuSetEntity;
import com.sparwk.adminnode.admin.jpa.repository.permissionSet.AdminMenuRepository;
import com.sparwk.adminnode.admin.jpa.repository.permissionSet.PermissionMenuSetRepository;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class AdminMenuService {

    private final AdminMenuRepository adminMenuRepository;
    private final PermissionMenuSetRepository permissionMenuSetRepository;

    private final MessageSourceAccessor messageSourceAccessor;

    private final Logger logger = LoggerFactory.getLogger(AdminMenuService.class);

    public Response<List<AdminMenuDto>> getUseYList() {

        Response<List<AdminMenuDto>> res = new Response<>();
        
        List<AdminMenuEntity> list = adminMenuRepository.findByUseTypeOrderBySortIndex(YnTypeEnum.Y);

        List<AdminMenuDto> list2 = list.stream().map(
                p -> AdminMenuDto.builder()
                        .menuSeq(p.getMenuSeq())
                        .menuLabel(p.getMenuLabel())
                        .menuDepth(p.getMenuDepth())
                        .uppMenuSeq(p.getUppMenuSeq())
                        .sortIndex(p.getSortIndex())
                        .description(p.getDescription())
                        .useType(p.getUseType())
                        .regDt(p.getRegDt())
                        .regUsr(p.getRegUsr())
                        .modDt(p.getModDt())
                        .modUsr(p.getModUsr())
                        .build()
        ).collect(Collectors.toList());



        logger.info("list - {}", list);

        //조회내용 없을 때
        if(list == null || list.size() < 1){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(list2);
        return res;
    }

    public Response<List<AdminMenuDto>> getUseYFirstList() {

        Response<List<AdminMenuDto>> res = new Response<>();

        List<AdminMenuEntity> list = adminMenuRepository.findByUseTypeAndMenuDepthOrderBySortIndex(YnTypeEnum.Y, 1);

        List<AdminMenuDto> list2 = list.stream().map(
                p -> AdminMenuDto.builder()
                        .menuSeq(p.getMenuSeq())
                        .menuLabel(p.getMenuLabel())
                        .menuDepth(p.getMenuDepth())
                        .uppMenuSeq(p.getUppMenuSeq())
                        .sortIndex(p.getSortIndex())
                        .description(p.getDescription())
                        .useType(p.getUseType())
                        .regDt(p.getRegDt())
                        .regUsr(p.getRegUsr())
                        .modDt(p.getModDt())
                        .modUsr(p.getModUsr())
                        .build()
        ).collect(Collectors.toList());



        logger.info("list - {}", list);

        //조회내용 없을 때
        if(list == null || list.size() < 1){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(list2);
        return res;
    }

    public Response<List<AdminMenuDto>> getUseYChildrenList(int depth, Long uppMenuSeq) {

        Response<List<AdminMenuDto>> res = new Response<>();

        List<AdminMenuEntity> list = adminMenuRepository.findByUseTypeAndMenuDepthAndUppMenuSeqOrderBySortIndex(YnTypeEnum.Y, depth, uppMenuSeq);

        List<AdminMenuDto> list2 = list.stream().map(
                p -> AdminMenuDto.builder()
                        .menuSeq(p.getMenuSeq())
                        .menuLabel(p.getMenuLabel())
                        .menuDepth(p.getMenuDepth())
                        .uppMenuSeq(p.getUppMenuSeq())
                        .sortIndex(p.getSortIndex())
                        .description(p.getDescription())
                        .useType(p.getUseType())
                        .regDt(p.getRegDt())
                        .regUsr(p.getRegUsr())
                        .modDt(p.getModDt())
                        .modUsr(p.getModUsr())
                        .build()
        ).collect(Collectors.toList());



        logger.info("list - {}", list);

        //조회내용 없을 때
        if(list == null || list.size() < 1){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(list2);
        return res;
    }

    public Response<List<PermissionMenuSetDto>> getMenuSettingList(Long id) {

        Response<List<PermissionMenuSetDto>> res = new Response<>();

        List<PermissionMenuSetDto> list = permissionMenuSetRepository.findQryMenuSetting(id);

        logger.info("list - {}", list);

        //조회내용 없을 때
        if(list == null || list.size() < 1){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(list);
        return res;
    }

    @Transactional
    public Response<Void> saveContent(List<PermissionMenuSetDto> request, HttpServletRequest req) {

        Response<Void> res = new Response<>();

        for (PermissionMenuSetDto permissionMenuSetDto : request) {

            YnTypeEnum createYn;
            YnTypeEnum readYn;
            YnTypeEnum editYn;
            YnTypeEnum deleteYn;

            //CreateYn 값 없거나 틀릴 때
            if(permissionMenuSetDto.getCreateYn() != null && permissionMenuSetDto.getCreateYn().equals("Y") && permissionMenuSetDto.getCreateYn().equals("N")){
                res.setResultCd(ResultCodeConst.NOT_VALID_USEYN.getCode());
                res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
                return res;
            } else {
                createYn = (permissionMenuSetDto.getCreateYn().equals("Y")) ? YnTypeEnum.Y : YnTypeEnum.N;
            }

            //ReadYn 값 없거나 틀릴 때
            if(permissionMenuSetDto.getReadYn() != null && permissionMenuSetDto.getReadYn().equals("Y") && permissionMenuSetDto.getReadYn().equals("N")){
                res.setResultCd(ResultCodeConst.NOT_VALID_USEYN.getCode());
                res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
                return res;
            } else {
                readYn = (permissionMenuSetDto.getReadYn().equals("Y")) ? YnTypeEnum.Y : YnTypeEnum.N;
            }

            //EditYn 값 없거나 틀릴 때
            if(permissionMenuSetDto.getEditYn() != null && permissionMenuSetDto.getEditYn().equals("Y") && permissionMenuSetDto.getEditYn().equals("N")){
                res.setResultCd(ResultCodeConst.NOT_VALID_USEYN.getCode());
                res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
                return res;
            } else {
                editYn = (permissionMenuSetDto.getEditYn().equals("Y")) ? YnTypeEnum.Y : YnTypeEnum.N;
            }

            //DeleteYn 값 없거나 틀릴 때
            if(permissionMenuSetDto.getDeleteYn() != null && permissionMenuSetDto.getDeleteYn().equals("Y") && permissionMenuSetDto.getDeleteYn().equals("N")){
                res.setResultCd(ResultCodeConst.NOT_VALID_USEYN.getCode());
                res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
                return res;
            } else {
                deleteYn = (permissionMenuSetDto.getDeleteYn().equals("Y")) ? YnTypeEnum.Y : YnTypeEnum.N;
            }

            //Value 값 없을 때
            if(permissionMenuSetDto.getMenuId() == null){
                res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
                res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
                return res;
            }

            //Value 값 없을 때
            if(permissionMenuSetDto.getAdminPermissionId() == null){
                res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
                res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
                return res;
            }


            try {

                Optional<PermissionMenuSetEntity> isPermissionMenuSet =
                        permissionMenuSetRepository.findByAdminMenuIdAndAdminPermissionId(permissionMenuSetDto.getMenuId(), permissionMenuSetDto.getAdminPermissionId());

                //조회내용 없을 때 새로 입력, 있으면 수정
                if(!isPermissionMenuSet.isPresent()){

                    PermissionMenuSetEntity newPermissionMenuSet = PermissionMenuSetEntity.builder()
                            .adminMenuId(permissionMenuSetDto.getMenuId())
                            .adminPermissionId(permissionMenuSetDto.getAdminPermissionId())
                            .createYn(createYn)
                            .readYn(readYn)
                            .editYn(editYn)
                            .deleteYn(deleteYn)
                            .build();

                    PermissionMenuSetEntity saved = permissionMenuSetRepository.save(newPermissionMenuSet);

                    logger.info("newPermissionMenuSet - {} ", newPermissionMenuSet);
                } else {
                    isPermissionMenuSet.get().update(createYn,readYn,editYn,deleteYn);
                }

                //정상작동 할 때
                res.setResultCd(ResultCodeConst.SUCCESS.getCode());
                res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
//            res.setResult(dtoPage);

            } catch (Exception e){
                logger.error("ERROR >>>>> {} ", e.getMessage());
                res.setResultCd(ResultCodeConst.FAIL.getCode());
                res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            }
        }

        return res;
    }

}
