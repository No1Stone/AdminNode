package com.sparwk.adminnode.admin.biz.v1.country.service;

import com.sparwk.adminnode.admin.biz.v1.CommonPagingListDto;
import com.sparwk.adminnode.admin.biz.v1.admin.service.AdminActiveLogService;
import com.sparwk.adminnode.admin.biz.v1.country.dto.CountryDto;
import com.sparwk.adminnode.admin.biz.v1.country.dto.CountryExcelDto;
import com.sparwk.adminnode.admin.biz.v1.country.dto.CountryRequest;
import com.sparwk.adminnode.admin.config.common.ActiveCodeConst;
import com.sparwk.adminnode.admin.config.common.GetAccountId;
import com.sparwk.adminnode.admin.config.common.Response;
import com.sparwk.adminnode.admin.config.common.ResultCodeConst;
import com.sparwk.adminnode.admin.config.util.ExcelWriter;
import com.sparwk.adminnode.admin.jpa.entity.PeriodTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.admin.AdminActiveLogEntity;
import com.sparwk.adminnode.admin.jpa.entity.continent.ContinentEntity;
import com.sparwk.adminnode.admin.jpa.entity.country.CountryCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.country.CountryEntity;
import com.sparwk.adminnode.admin.jpa.entity.country.CountrySorterEnum;
import com.sparwk.adminnode.admin.jpa.repository.continent.ContinentRepository;
import com.sparwk.adminnode.admin.jpa.repository.country.CountryRepository;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class CountryService {

    private final CountryRepository countryRepository;
    private final ContinentRepository continentRepository;
    private final AdminActiveLogService adminActiveLogService;

    private final HttpServletRequest httpServletRequest;
    private final MessageSourceAccessor messageSourceAccessor;

    private final Logger logger = LoggerFactory.getLogger(CountryService.class);


    public Response<CommonPagingListDto> getList(CountryCateEnum cate,
                                                 String val,
                                                 YnTypeEnum useType,
                                                 PeriodTypeEnum periodType,
                                                 String sdate,
                                                 String edate,
                                                 CountrySorterEnum sorter,
                                                 PageRequest pageRequest) {

        Response<CommonPagingListDto> res = new Response<>();

        List<CountryDto> countryList =
                countryRepository.findQryAll(cate, val, useType, periodType, sdate, edate, sorter, pageRequest);

        logger.info("countryList - {}", countryList);

        //조회내용 없을 때
        if(countryList == null || countryList.size() < 1){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        CommonPagingListDto list = new CommonPagingListDto();

        //총 카운트
        long totalSize =
                countryRepository.countQryAll(cate, val, useType, periodType, sdate, edate);
        long totalPage = totalSize / pageRequest.getPageSize();
        long mod = totalSize % pageRequest.getPageSize();
        if(mod > 0) {
            totalPage += 1;
        }

        list.setList(countryList);
        list.setSize(pageRequest.getPageSize());
        list.setPage(pageRequest.getPageNumber());
        list.setTotalPage(totalPage);
        list.setTotalSize(totalSize);

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(list);
        return res;
    }

    public Response<CommonPagingListDto> getUseYList(CountryCateEnum cate, String val) {

        Response<CommonPagingListDto> res = new Response<>();

        List<CountryDto> countryList = countryRepository.findQryUseY(cate, val);

        logger.info("countryList - {}", countryList);

        //조회내용 없을 때
        if(countryList == null || countryList.size() < 1){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        CommonPagingListDto list = new CommonPagingListDto();

        PageRequest pageRequest = PageRequest.of(10, 10);

        //총 카운트
        long totalSize =
                countryRepository.countQryUseY(cate, val);
        long totalPage = totalSize / pageRequest.getPageSize();
        long mod = totalSize % pageRequest.getPageSize();
        if(mod > 0) {
            totalPage += 1;
        }

        list.setList(countryList);
        list.setSize(pageRequest.getPageSize());
        list.setPage(pageRequest.getPageNumber());
        list.setTotalPage(totalPage);
        list.setTotalSize(totalSize);

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(list);
        return res;
    }

    public Response<Optional<CountryDto>> getOne(Long id) {

        Response<Optional<CountryDto>> res = new Response<>();

        //id값이 없을 때
        if(id == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<CountryEntity> country = countryRepository.findById(id);

        logger.info("country - {}", country);

        //조회내용 없을 때
        if(!country.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<CountryDto> dtoPage =

                country.map(p -> CountryDto.builder()
                        .countrySeq(p.getCountrySeq())
                        .country(p.getCountry())
                        .countryCd(p.getCountryCd())
                        .continentCode(p.getContinentCode())
                        .continentCodeVal(p.getContinentEntity().getContinent())
                        .iso2(p.getIso2())
                        .iso3(p.getIso3())
                        .description(p.getDescription())
                        .dial(p.getDial())
                        .nmr(p.getNmr())
                        .useType(p.getUseType())
                        .regUsr(p.getRegUsr())
                        .regDt(p.getRegDt())
                        .modUsr(p.getModUsr())
                        .modDt(p.getModDt())
                        .build()
                );

        logger.info("Country - {}", dtoPage);

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(dtoPage);
        return res;
    }

    @Transactional
    public Response<Void> saveContent(CountryRequest request, HttpServletRequest req) {

        Response<Void> res = new Response<>();
        String cntCd;

        Optional<ContinentEntity> continentEntity =
                Optional.ofNullable(continentRepository.findByCode(request.getCountryDto().getContinentCode()));
        logger.info("continentEntity - {}", continentEntity);


        //부모코드 조회내용 없을 때
        if(!continentEntity.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //USEYN 값 없거나 틀릴 때
        if(request.getCountryDto().getUseType() == null || (request.getCountryDto().getUseType() != YnTypeEnum.Y && request.getCountryDto().getUseType() != YnTypeEnum.N)){
            res.setResultCd(ResultCodeConst.NOT_VALID_USEYN.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if(request.getCountryDto().getCountry() == null){
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if(request.getCountryDto().getContinentCode() == null){
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if(request.getCountryDto().getIso2() == null){
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if(request.getCountryDto().getIso3() == null){
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if(request.getCountryDto().getDial() == null){
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        try {
            //seq 번호 불러옴
            int k = countryRepository.getLastSeq("CNT") + 1;

            if(k < 10)
                cntCd = "CNT00000" + k;
            else if(k < 100)
                cntCd = "CNT0000" + k;
            else if(k < 1000)
                cntCd = "CNT000" + k;
            else if(k < 10000)
                cntCd = "CNT00" + k;
            else if(k < 100000)
                cntCd = "CNT0" + k;
            else if(k < 1000000)
                cntCd = "CNT" + k;
            else
                cntCd = "CNT" + k;

            CountryEntity newCountry = CountryEntity.builder()
                    .country(request.getCountryDto().getCountry())
                    .countryCd(cntCd)
                    .iso2(request.getCountryDto().getIso2())
                    .iso3(request.getCountryDto().getIso3())
                    .nmr(request.getCountryDto().getNmr())
                    .dial(request.getCountryDto().getDial())
                    .description(request.getCountryDto().getDescription())
                    .useType(request.getCountryDto().getUseType())
                    .continentCode(request.getCountryDto().getContinentCode())
                    .continentEntity(continentEntity.get())
                    .build();

            CountryEntity saved = countryRepository.save(newCountry);

            //seq 번호 수동으로 업데이트 시킨다
            countryRepository.updateLastSeq("CNT", k);



//            Optional<CountryEntity> country = countryRepository.findById(saved.getCountrySeq());
//
//            Optional<CountryDto> dtoPage =
//
//                    country.map(p -> CountryDto.builder()
//                            .countrySeq(p.getCountrySeq())
//                            .country(p.getCountry())
//                            .countryCd(p.getCountryCd())
//                            .continentCode(p.getContinentCode())
//                            .continentCodeVal(p.getContinentEntity().getContinent())
//                            .iso2(p.getIso2())
//                            .iso3(p.getIso3())
//                            .description(p.getDescription())
//                            .dial(p.getDial())
//                            .nmr(p.getNmr())
//                            .useType(p.getUseType())
//                            .regUsr(p.getRegUsr())
//                            .regDt(p.getRegDt())
//                            .modUsr(p.getModUsr())
//                            .modDt(p.getModDt())
//                            .build()
//                    );
//
//            return dtoPage;
            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
//            res.setResult(dtoPage);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));

            String menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_COUNTRIES.getCode());
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_CREATE.getCode());

            //netive query
            String usrName = adminActiveLogService.findQryUsrName(saved.getRegUsr());

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(saved.getRegDt())
                    .adminId(saved.getRegUsr())
                    .menuName(menuName)
                    .activeType("C")
                    .contentsId(saved.getCountrySeq())
                    .activeMsg(usrName + " " + activeMsg + " '" + saved.getCountry() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e){
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }

        return res;
    }

    @Transactional
    public Response<Void> updateYn(Long id, HttpServletRequest req) {

        Response<Void> res = new Response<>();

        //id값이 없을 때
        if(id == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<CountryEntity> country = countryRepository.findById(id);


        //조회내용 없을 때
        if(!country.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        YnTypeEnum useType = country.get().getUseType();
        YnTypeEnum newUseType = (useType == YnTypeEnum.Y) ? YnTypeEnum.N : YnTypeEnum.Y;

        logger.info("newUseType - {} ", newUseType);

        try {
            country.get().updateYn(newUseType);
            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
//            res.setResult(dtoPage);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));

            String menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_COUNTRIES.getCode());
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_MODIFY.getCode());

            //netive query
            String usrName = adminActiveLogService.findQryUsrName(country.get().getModUsr());

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(country.get().getModDt())
                    .adminId(country.get().getModUsr())
                    .menuName(menuName)
                    .activeType("U")
                    .contentsId(country.get().getCountrySeq())
                    .activeMsg(usrName + " " + activeMsg + " '" + country.get().getCountry() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e) {
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }

        return res;
    }

    @Transactional
    public Response<Optional<CountryDto>> updateContent(Long id, CountryRequest request, HttpServletRequest req) {

        Response<Optional<CountryDto>> res = new Response<>();

        //id값이 없을 때
        if(id == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<ContinentEntity> continentEntity =
                Optional.ofNullable(continentRepository.findByCode(request.getCountryDto().getContinentCode()));

        //부모코드 조회내용 없을 때
        if(!continentEntity.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //USEYN 값 없거나 틀릴 때
        if(request.getCountryDto().getUseType() == null || (request.getCountryDto().getUseType() != YnTypeEnum.Y && request.getCountryDto().getUseType() != YnTypeEnum.N)){
            res.setResultCd(ResultCodeConst.NOT_VALID_USEYN.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if(request.getCountryDto().getCountry() == null){
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if(request.getCountryDto().getContinentCode() == null){
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if(request.getCountryDto().getIso2() == null){
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if(request.getCountryDto().getIso3() == null){
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if(request.getCountryDto().getDial() == null){
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<CountryEntity> countryUpdate = countryRepository.findById(id);

        //조회내용 없을 때
        if(!countryUpdate.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        try {

        countryUpdate.get()
                .update(request.getCountryDto().getCountry(),
                        request.getCountryDto().getCountryCd(),
                        request.getCountryDto().getIso2(),
                        request.getCountryDto().getIso3(),
                        request.getCountryDto().getContinentCode(),
                        request.getCountryDto().getNmr(),
                        request.getCountryDto().getDial(),
                        request.getCountryDto().getDescription(),
                        request.getCountryDto().getUseType(),
                        continentEntity.get()
                );

        Optional<CountryEntity> country = countryRepository.findById(countryUpdate.get().getCountrySeq());

        Optional<CountryDto> dtoPage =

                country.map(p -> CountryDto.builder()
                        .countrySeq(p.getCountrySeq())
                        .country(p.getCountry())
                        .countryCd(p.getCountryCd())
                        .continentCode(p.getContinentCode())
                        .continentCodeVal(p.getContinentEntity().getContinent())
                        .iso2(p.getIso2())
                        .iso3(p.getIso3())
                        .description(p.getDescription())
                        .dial(p.getDial())
                        .nmr(p.getNmr())
                        .useType(p.getUseType())
                        .regUsr(p.getRegUsr())
                        .regDt(p.getRegDt())
                        .modUsr(p.getModUsr())
                        .modDt(p.getModDt())
                        .build()
                );

            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            res.setResult(dtoPage);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));

            String menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_COUNTRIES.getCode());
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_MODIFY.getCode());

            //netive query
            String usrName = adminActiveLogService.findQryUsrName(country.get().getModUsr());

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(country.get().getModDt())
                    .adminId(country.get().getModUsr())
                    .menuName(menuName)
                    .activeType("U")
                    .contentsId(country.get().getCountrySeq())
                    .activeMsg(usrName + " " + activeMsg + " '" + country.get().getCountry() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e){
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }

        return res;
    }

    @Transactional
    public Response<Void> deleteContent(Long id, HttpServletRequest req) {

        Response<Void> res = new Response<>();

        //id값이 없을 때
        if(id == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<CountryEntity> country = countryRepository.findById(id);

        //조회내용 없을 때
        if(!country.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        try {
            countryRepository.deleteById(id);
            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
//            res.setResult(dtoPage);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));
            Long adminId = getAccountId.ofId(httpServletRequest);

            String menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_COUNTRIES.getCode());
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_DELETE.getCode());

            //netive query
            String usrName = adminActiveLogService.findQryUsrName(adminId);

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(LocalDateTime.now())
                    .adminId(adminId)
                    .menuName(menuName)
                    .activeType("D")
                    .contentsId(country.get().getCountrySeq())
                    .activeMsg(usrName + " " + activeMsg + " '" + country.get().getCountry() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e) {
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }
        return res;
    }

    public List<CountryExcelDto> getExcelList() {

        List<CountryExcelDto> getExcelList = countryRepository.findExcelList();

        logger.info("getExcelList - {} ", getExcelList);

        return getExcelList;
    }


    //엑셀 다운로드 요청
    public Map<String, Object> downExcelList() throws Exception {
        // 데이터 가져오기
        List<CountryExcelDto> excelList = this.getExcelList();

        logger.info("excelList - {} ", excelList);

        return ExcelWriter.createExcelData(excelList, CountryExcelDto.class);
    }

    //마지막 코드 번호 확인
    public Integer getLastSeq(String code) {

        return countryRepository.getLastSeq(code);
    }

    //코드 번호 업데이트
    @Transactional
    public void updateLastSeq(String code, Integer k) {

        countryRepository.updateLastSeq(code, k);
    }

    //이름으로 코드검색
    public Long getOneName(String val) {

        return countryRepository.countByCountry(val);
    }

    @Transactional
    public Response<Void> saveExcelContent(CountryDto countryDto, HttpServletRequest req) {

        Response<Void> res = new Response<>();

        Optional<ContinentEntity> continentCode =
                Optional.ofNullable(continentRepository.findByContinent(countryDto.getContinentCodeVal()));

        logger.info("continentCode - {}", continentCode);

        //부모코드 조회내용 없을 때
        if (!continentCode.isPresent()) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //USEYN 값 없거나 틀릴 때
        if (countryDto.getUseType() == null || (countryDto.getUseType() != YnTypeEnum.Y && countryDto.getUseType() != YnTypeEnum.N)) {
            res.setResultCd(ResultCodeConst.NOT_VALID_USEYN.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if(countryDto.getCountry() == null){
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if(countryDto.getIso2() == null){
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if(countryDto.getIso3() == null){
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if(countryDto.getDial() == null){
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        try {

            CountryEntity newCountry = CountryEntity.builder()
                    .country(countryDto.getCountry())
                    .iso2(countryDto.getIso2())
                    .iso3(countryDto.getIso3())
                    .countryCd(countryDto.getCountryCd())
                    .continentCode(continentCode.get().getCode())
                    .description(countryDto.getDescription())
                    .useType(countryDto.getUseType())
                    .nmr(countryDto.getNmr())
                    .dial(countryDto.getDial())
                    .countryCd(countryDto.getCountryCd())
                    .build();

            CountryEntity saved = countryRepository.save(newCountry);

            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
//            res.setResult(dtoPage);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));

            String menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_COUNTRIES.getCode());
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_CREATE.getCode());

            //netive query
            String usrName = adminActiveLogService.findQryUsrName(saved.getRegUsr());

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(saved.getRegDt())
                    .adminId(saved.getRegUsr())
                    .menuName(menuName)
                    .activeType("C")
                    .contentsId(saved.getCountrySeq())
                    .activeMsg(usrName + " " + activeMsg + " '" + saved.getCountry() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e) {
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }

        return res;
    }

}
