package com.sparwk.adminnode.admin.biz.v1.projects.service;

import com.sparwk.adminnode.admin.biz.v1.CommonPagingListDto;
import com.sparwk.adminnode.admin.biz.v1.admin.service.AdminActiveLogService;
import com.sparwk.adminnode.admin.biz.v1.projects.dto.*;
import com.sparwk.adminnode.admin.biz.v1.songCode.dto.SongDetailCodeDto;
import com.sparwk.adminnode.admin.biz.v1.songCode.dto.SongDetailCodeRequest;
import com.sparwk.adminnode.admin.config.common.ActiveCodeConst;
import com.sparwk.adminnode.admin.config.common.GetAccountId;
import com.sparwk.adminnode.admin.config.common.Response;
import com.sparwk.adminnode.admin.config.common.ResultCodeConst;
import com.sparwk.adminnode.admin.jpa.entity.FormatTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.PeriodTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.admin.AdminActiveLogEntity;
import com.sparwk.adminnode.admin.jpa.entity.profile.ProfileCompanyEntity;
import com.sparwk.adminnode.admin.jpa.entity.profile.ProfileEntity;
import com.sparwk.adminnode.admin.jpa.entity.projects.ProjcetCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.projects.ProjectEntity;
import com.sparwk.adminnode.admin.jpa.entity.projects.ProjectSorterEnum;
import com.sparwk.adminnode.admin.jpa.entity.songCode.SongCodeEntity;
import com.sparwk.adminnode.admin.jpa.entity.songCode.SongDetailCodeEntity;
import com.sparwk.adminnode.admin.jpa.repository.profile.ProfileCompanyRepository;
import com.sparwk.adminnode.admin.jpa.repository.profile.ProfileRepository;
import com.sparwk.adminnode.admin.jpa.repository.projects.ProjectCowritersViewRepository;
import com.sparwk.adminnode.admin.jpa.repository.projects.ProjectRepository;
import com.sparwk.adminnode.admin.jpa.repository.projects.ProjectViewRepository;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class
ProjectService {

    private final ProjectRepository projectRepository;
    private final ProjectViewRepository projectViewRepository;
    private final ProfileRepository profileRepository;
    private final ProfileCompanyRepository profileCompanyRepository;
    private final ProjectCowritersViewRepository projectCowritersViewRepository;
    private final AdminActiveLogService adminActiveLogService;

    private final HttpServletRequest httpServletRequest;
    private final MessageSourceAccessor messageSourceAccessor;
    
    private final Logger logger = LoggerFactory.getLogger(ProjectService.class);


    public Response<CommonPagingListDto> getList(ProjcetCateEnum cate, String val, YnTypeEnum completeVal, PeriodTypeEnum periodType, String sdate, String edate, ProjectSorterEnum sorter, PageRequest pageRequest) {

        Response<CommonPagingListDto> res = new Response<>();

        List<ProjectViewListDto> projectList = projectViewRepository.findQryAll(cate,
                                                                                val,
                                                                                completeVal,
                                                                                periodType,
                                                                                sdate,
                                                                                edate,
                                                                                sorter,
                                                                                pageRequest);

        logger.info("projectList - {}", projectList);

        //조회내용 없을 때
        if(projectList == null || projectList.size() < 1){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        CommonPagingListDto list = new CommonPagingListDto();

        //총 카운트
        long totalSize = projectViewRepository.countQryAll(cate,
                val,
                completeVal,
                periodType,
                sdate,
                edate);
        long totalPage = totalSize / pageRequest.getPageSize();
        long mod = totalSize % pageRequest.getPageSize();
        if(mod > 0) {
            totalPage += 1;
        }

        list.setList(projectList);
        list.setSize(pageRequest.getPageSize());
        list.setPage(pageRequest.getPageNumber());
        list.setTotalPage(totalPage);
        list.setTotalSize(totalSize);
        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(list);
        return res;
    }

    //상세페이지
    public Response<Optional<ProjectDetailDto>> getOne(Long id) {

        Response<Optional<ProjectDetailDto>> res = new Response<>();

        //id값이 없을 때
        if(id == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<ProjectEntity> project = projectRepository.findById(id);

        logger.info("project - {}", project);

        //조회내용 없을 때
        if(!project.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //프로젝트 오너 이름
        Optional<ProfileEntity> owner = profileRepository.findById(project.get().getProjOwner());

        logger.info("owner - {}", owner);

        //조회내용 없을 때
        if(!owner.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //프로젝트 회사
        String companyName;
        Optional<ProfileCompanyEntity> company = profileCompanyRepository.findById(project.get().getCompProfileId());
        //조회내용 없을 때
        if(!company.isPresent()){
            companyName = "";
        } else {
            companyName = company.get().getProfileCompanyName();
        }

        //프로젝트 타입 pitchProjTypeCd
        List<ProjectMetadataDto> pitchProjTypeCdName = projectRepository.findQryCommonMeta("PJT", project.get().getPitchProjTypeCd(), project.get().getProjId());
        //회사 리스트
        List<ProjectProfileDto> getProjInviteComList = projectRepository.findQryProjInviteCom(project.get().getProjId());
        //budgetName 리스트 String으로 추가
        List<ProjectMetadataDto> getProjCondDataList = projectRepository.findQryCommonMeta("PJC", project.get().getProjCondCd(), project.get().getProjId());
        //Create Roles String으로 추가
        List<ProjectMetadataDto> getCreateRolsDataList = projectRepository.findQryRolesMeta("DEX", project.get().getProjId());
        //장르 리스트 String으로 추가
        List<ProjectMetadataDto> getSongGenreDataList = projectRepository.findQrySongMeta("SGG", project.get().getProjId());
        //무드 리스트 String으로 추가
        List<ProjectMetadataDto> getSongMoodDataList = projectRepository.findQrySongMeta("SGM", project.get().getProjId());

        //Working Location 리스트 String으로 추가
        ProjectMetadataDto getProjWorkLocatData = projectRepository.findQryWorkLocatMeta("PWL", project.get().getProjWorkLocat(), project.get().getProjId());

        //Service Location 리스트 String으로 추가
        List<ProjectMetadataDto> getProjServiceLocaDataList = projectRepository.findQryCountryMeta("CNT", project.get().getProjId());

        //테마 리스트 String으로 추가
        List<ProjectMetadataDto> getSongThemeDataList = projectRepository.findQrySongMeta("SGT", project.get().getProjId());
        logger.info("getSongThemeDataList - {}", getSongThemeDataList);

        //첨부파일 리스트 List로 추가
        List<ProjectReferenceFileDto> getProjectReferenceFileList = projectRepository.findQryProjectReferenceFileList(project.get().getProjId());
        logger.info("getProjectReferenceFileList - {}", getProjectReferenceFileList);

        //Song 리스트 List로 추가
        List<ProjectSongDto> getProjectSongList = projectRepository.findQryProjectSongList(project.get().getProjId());
        logger.info("getProjectSongList - {}", getProjectSongList);

        //Song 리스트 List로 추가
        List<ProjectCowritersViewListDto> getProjectCowritersList = projectCowritersViewRepository.findQryAll(project.get().getProjId());
        logger.info("getProjectCowritersList - {}", getProjectCowritersList);

               Optional<ProjectDetailDto> dtoPage =

                project.map(p -> ProjectDetailDto.builder()
                        .projId(p.getProjId())
                        .projectTitle(p.getProjTitle())
                        .projDesc(p.getProjDesc())
                        .avatarFileUrl(p.getAvatarFileUrl())
                        .projStatus(String.valueOf(p.getCompleteYn()))
                        .pitchProjTypeCd(p.getPitchProjTypeCd())
                        .pitchProjTypeCdName(pitchProjTypeCdName.get(0).getAttrDtlCdVal())
                        .projOwner(p.getProjOwner())
                        .compProfileId(p.getCompProfileId())
                        .compProfileName(companyName)
                        .projOwnerName(owner.get().getFullName())
                        .projectInviteCompanyList(getProjInviteComList)
                        .regDt(p.getRegDt())
                        .projDdlDt(p.getProjDdlDt())
                        .budget(p.getProjCondCd())
                        .budgetName(getProjCondDataList.get(0).getAttrDtlCdVal())
                        .maxVal(p.getMaxVal())
                        .minVal(p.getMinVal())
                        .projPublYn(p.getProjPublYn())
                        .projectRolesList(getCreateRolsDataList)
                        .projectGenresList(getSongGenreDataList)
                        .projectMoodsList(getSongMoodDataList)
                        .projectThemesList(getSongThemeDataList)
                        .projWorkLocat(p.getProjWorkLocat())
                        .projWorkLocatName(getProjWorkLocatData.getAttrDtlCdVal())
                        .projectServiceCountriesList(getProjServiceLocaDataList)
                        .projectReferenceFileList(getProjectReferenceFileList)
                        .projectSongList(getProjectSongList)
                        .projectCowritersList(getProjectCowritersList)
                        .build()
                );

        logger.info("SongDetailCode - {}", dtoPage);

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(dtoPage);
        return res;
    }

    //수정처리
    @Transactional
    public Response<Optional<ProjectDetailDto>> updateContent(Long id, ProjectModifyRequest request, HttpServletRequest req) {

        Response<Optional<ProjectDetailDto>> res = new Response<>();

        //id값이 없을 때
        if(id == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<ProjectEntity> project = projectRepository.findById(id);

        logger.info("project - {} ", project);

        //조회내용 없을 때
        if(!project.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Format 값 없거나 틀릴 때
        if(request.getProjTitle() == null){
            res.setResultCd(ResultCodeConst.NOT_VALID_FORMAT.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        try {

            //수정
            project.get().update(request.getProjTitle());

            //수정 재조회
            Optional<ProjectEntity> projectUpdate =
                    projectRepository.findById(project.get().getProjId());

            //프로젝트 오너 이름
            Optional<ProfileEntity> owner = profileRepository.findById(project.get().getProjOwner());

            logger.info("owner - {}", owner);

            //조회내용 없을 때
            if(!owner.isPresent()){
                res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
                res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
                return res;
            }

            //프로젝트 회사
            String companyName;
            Optional<ProfileCompanyEntity> company = profileCompanyRepository.findById(project.get().getCompProfileId());
            //조회내용 없을 때
            if(!company.isPresent()){
                companyName = "";
            } else {
                companyName = company.get().getProfileCompanyName();
            }

            //프로젝트 타입 pitchProjTypeCd
            List<ProjectMetadataDto> pitchProjTypeCdName = projectRepository.findQryCommonMeta("PJT", project.get().getPitchProjTypeCd(), project.get().getProjId());
            //회사 리스트
            List<ProjectProfileDto> getProjInviteComList = projectRepository.findQryProjInviteCom(project.get().getProjId());
            //budgetName 리스트 String으로 추가
            List<ProjectMetadataDto> getProjCondDataList = projectRepository.findQryCommonMeta("PJC", project.get().getProjCondCd(), project.get().getProjId());
            //Create Roles String으로 추가
            List<ProjectMetadataDto> getCreateRolsDataList = projectRepository.findQryRolesMeta("DEX", project.get().getProjId());
            //장르 리스트 String으로 추가
            List<ProjectMetadataDto> getSongGenreDataList = projectRepository.findQrySongMeta("SGG", project.get().getProjId());
            //무드 리스트 String으로 추가
            List<ProjectMetadataDto> getSongMoodDataList = projectRepository.findQrySongMeta("SGM", project.get().getProjId());

            //Working Location 리스트 String으로 추가
            ProjectMetadataDto getProjWorkLocatData = projectRepository.findQryWorkLocatMeta("PWL", project.get().getProjWorkLocat(), project.get().getProjId());

            //Service Location 리스트 String으로 추가
            List<ProjectMetadataDto> getProjServiceLocaDataList = projectRepository.findQryCountryMeta("CNT", project.get().getProjId());

            //테마 리스트 String으로 추가
            List<ProjectMetadataDto> getSongThemeDataList = projectRepository.findQrySongMeta("SGT", project.get().getProjId());
            logger.info("getSongThemeDataList - {}", getSongThemeDataList);

            //첨부파일 리스트 List로 추가
            List<ProjectReferenceFileDto> getProjectReferenceFileList = projectRepository.findQryProjectReferenceFileList(project.get().getProjId());
            logger.info("getProjectReferenceFileList - {}", getProjectReferenceFileList);

            //Song 리스트 List로 추가
            List<ProjectSongDto> getProjectSongList = projectRepository.findQryProjectSongList(project.get().getProjId());
            logger.info("getProjectSongList - {}", getProjectSongList);

            //Song 리스트 List로 추가
            List<ProjectCowritersViewListDto> getProjectCowritersList = projectCowritersViewRepository.findQryAll(project.get().getProjId());
            logger.info("getProjectCowritersList - {}", getProjectCowritersList);

            Optional<ProjectDetailDto> dtoPage =

                    project.map(p -> ProjectDetailDto.builder()
                            .projId(p.getProjId())
                            .projectTitle(p.getProjTitle())
                            .projDesc(p.getProjDesc())
                            .avatarFileUrl(p.getAvatarFileUrl())
                            .projStatus(String.valueOf(p.getCompleteYn()))
                            .pitchProjTypeCd(p.getPitchProjTypeCd())
                            .pitchProjTypeCdName(pitchProjTypeCdName.get(0).getAttrDtlCdVal())
                            .projOwner(p.getProjOwner())
                            .compProfileId(p.getCompProfileId())
                            .compProfileName(companyName)
                            .projOwnerName(owner.get().getFullName())
                            .projectInviteCompanyList(getProjInviteComList)
                            .regDt(p.getRegDt())
                            .projDdlDt(p.getProjDdlDt())
                            .budget(p.getProjCondCd())
                            .budgetName(getProjCondDataList.get(0).getAttrDtlCdVal())
                            .maxVal(p.getMaxVal())
                            .minVal(p.getMinVal())
                            .projPublYn(p.getProjPublYn())
                            .projectRolesList(getCreateRolsDataList)
                            .projectGenresList(getSongGenreDataList)
                            .projectMoodsList(getSongMoodDataList)
                            .projectThemesList(getSongThemeDataList)
                            .projWorkLocat(p.getProjWorkLocat())
                            .projWorkLocatName(getProjWorkLocatData.getAttrDtlCdVal())
                            .projectServiceCountriesList(getProjServiceLocaDataList)
                            .projectReferenceFileList(getProjectReferenceFileList)
                            .projectSongList(getProjectSongList)
                            .projectCowritersList(getProjectCowritersList)
                            .build()
                    );
            logger.info("dtoPage - {} ", dtoPage);

            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            res.setResult(dtoPage);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));

            String menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_PROJECTS_LIST.getCode());
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_MODIFY.getCode());
            //netive query
            String usrName = adminActiveLogService.findQryUsrName(project.get().getModUsr());

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(project.get().getModDt())
                    .adminId(project.get().getModUsr())
                    .menuName(menuName)
                    .activeType("U")
                    .contentsId(project.get().getProjId())
                    .activeMsg(usrName + " " + activeMsg + " '" + project.get().getProjTitle() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e){
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }

        return res;
    }
}
