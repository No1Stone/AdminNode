package com.sparwk.adminnode.admin.biz.v1.anr.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sparwk.adminnode.admin.jpa.entity.ExcelColumnName;
import com.sparwk.adminnode.admin.jpa.entity.ExcelDto;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Arrays;
import java.util.List;

@Setter
@Getter
@NoArgsConstructor
public class AnrExcelDto implements ExcelDto {

    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    @ExcelColumnName(headerName = "A&R Seq")
    private Long anrSeq;
    @Schema(description = "A&R Service 이름", nullable = false, example = "Reject")
    @ExcelColumnName
    @JsonProperty("anrServiceName")
    private String anrServiceName;
    @Schema(description = "A&R CODE ", nullable = false, example = "ANR000216")
    @ExcelColumnName
    @JsonProperty("anrCd")
    private String anrCd;
    @Schema(description = "A&R GROUP CODE", nullable = false, example = "ARG000001")
    @ExcelColumnName
    @JsonProperty("anrGroupCd")
    private String anrGroupCd;
    @Schema(description = "대륙", nullable = false, example = "PRO")
    @ExcelColumnName
    @JsonProperty("anrGroupCdName")
    private String anrGroupCdName;
    @Schema(description = "코드 사용여부 Y/N", nullable = false, defaultValue = "Y", allowableValues = {"Y", "N"})
    @ExcelColumnName
    @JsonProperty("useType")
    private YnTypeEnum useType;
    @Schema(description = "description", nullable = true, example = "설명")
    @ExcelColumnName
    @JsonProperty("description")
    private String description;

    @Override
    public List<String> mapToList() {
        return Arrays.asList(
                String.valueOf(anrSeq),
                anrServiceName,
                anrCd,
                anrGroupCd,
                anrGroupCdName,
                String.valueOf(useType),
                description
        );
    }
}
