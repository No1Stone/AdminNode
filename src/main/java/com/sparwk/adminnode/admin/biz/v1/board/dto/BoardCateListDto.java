package com.sparwk.adminnode.admin.biz.v1.board.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class BoardCateListDto {

    @Schema(description = "카테고리 코드 9자리", nullable = false, example = "ENC000001")
    private String cateCd;
    @Schema(description = "카테고리 코드 이름", nullable = true, example = "Best Music")
    private String cateCdName;

    @Builder
    public BoardCateListDto(String cateCd,
                            String cateCdName
    ) {
        this.cateCd = cateCd;
        this.cateCdName = cateCdName;
    }
}