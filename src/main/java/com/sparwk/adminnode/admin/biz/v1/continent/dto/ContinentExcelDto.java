package com.sparwk.adminnode.admin.biz.v1.continent.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sparwk.adminnode.admin.jpa.entity.ExcelColumnName;
import com.sparwk.adminnode.admin.jpa.entity.ExcelDto;
import com.sparwk.adminnode.admin.jpa.entity.ExcelFileName;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Arrays;
import java.util.List;

@Data
@NoArgsConstructor
@ExcelFileName
public class ContinentExcelDto implements ExcelDto {
    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    @ExcelColumnName(headerName = "Continent Seq")
    private Long continentSeq;

    @Schema(description = "대륙명", nullable = false, example = "Africa")
    @ExcelColumnName
    @JsonProperty("continent")
    private String continent;

    @Schema(description = "ISO 대륙코드", nullable = false, example = "AF")
    @ExcelColumnName
    @JsonProperty("code")
    private String code;

    @Schema(description = "description", nullable = true, example = "설명")
    @ExcelColumnName
    @JsonProperty("description")
    private String description;

    @Schema(description = "코드 사용여부 Y/N", nullable = false, defaultValue = "Y", allowableValues = {"Y", "N"})
    @ExcelColumnName
    @JsonProperty("useType")
    private YnTypeEnum useType;

    @Override
    public List<String> mapToList() {
        return Arrays.asList(String.valueOf(continentSeq),
                                continent,
                                code,
                                description,
                                String.valueOf(useType)
        );
    }

}
