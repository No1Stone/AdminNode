package com.sparwk.adminnode.admin.biz.v1.account.dto;

import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Setter
@Getter
@NoArgsConstructor
public class IndividualMemberViewListDto {

    @Schema(description = "id값 Seq 사용", example = "1")
    private Long accntId;
    @Schema(description = "id값 Seq 사용", example = "1")
    private Long profileId;
    @Schema(description = "ACCNT EMAIL", example = "sparwk@sparwk.com")
    private String accntEmail;
    @Schema(description = "PASSPORT_FIRST_NAME", example = "Yoon")
    private String passportFirstName;
    @Schema(description = "PASSPORT_Middle_NAME", nullable = true, example = "Jung")
    private String passportMiddleName;
    @Schema(description = "PASSPORT_LAST_NAME", example = "Hun")
    private String passportLastName;
    @Schema(description = "ACCNT FULL NAME", nullable = true, example = "Mr.Yoon")
    private String profileFullName;
    @Schema(description = "Company PROFILE NAME", nullable = true, example = "JYP")
    private String companyName;
    @Schema(description = "Company PROFILE NAME", nullable = true, example = "JYP")
    private String rolesName;
    @Schema(description = "여권인증여부 Y/N", defaultValue = "N", allowableValues = {"Y", "N"})
    private YnTypeEnum passportVerifyYn;
    @Schema(description = "IPI 인증여부 Y/N", defaultValue = "N", allowableValues = {"Y", "N"})
    private YnTypeEnum ipiVerifyYn;
    @Schema(description = "등록일", pattern = "yyMMdd hh:mm:ss", example = "2021-12-08T05:44:26.627991")
    private LocalDateTime regDt;

    @Builder
    public IndividualMemberViewListDto(Long accntId,
                                       Long profileId,
                                       String accntEmail,
                                       String passportFirstName,
                                       String passportMiddleName,
                                       String passportLastName,
                                       String profileFullName,
                                       String rolesName,
                                       String companyName,
                                       YnTypeEnum passportVerifyYn,
                                       YnTypeEnum ipiVerifyYn,
                                       LocalDateTime regDt

    ) {
        this.accntId = accntId;
        this.profileId = profileId;
        this.accntEmail = accntEmail;
        this.passportFirstName = passportFirstName;
        this.passportMiddleName = passportMiddleName;
        this.passportLastName = passportLastName;
        this.profileFullName = profileFullName;
        this.companyName = companyName;
        this.rolesName = rolesName;
        this.passportVerifyYn = passportVerifyYn;
        this.ipiVerifyYn = ipiVerifyYn;
        this.regDt = regDt;
    }
}
