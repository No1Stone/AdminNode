package com.sparwk.adminnode.admin.biz.v1.admin.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Setter
@Getter
@NoArgsConstructor
public class AdminActiveLogListDto {

    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long adminId;
    @Schema(description = "Admin Email", nullable = false, example = "admin@sparwk.com")
    private String adminEmail;
    @Schema(description = "Full Name", nullable = false, example = "Yoon Jung Sub")
    private String fullName;
    @Schema(description = "Menu Name", nullable = false, example = "메뉴명")
    private String menuName;
    @Schema(description = "설명", nullable = false, example = "설명")
    private String activeMsg;
    @Schema(description = "등록일", pattern = "yyMMdd hh:mm:ss", example = "2021-12-08T05:44:26.627991")
    private LocalDateTime activeTime;

    @Builder
    public AdminActiveLogListDto(Long adminId,
                                 String adminEmail,
                                 String fullName,
                                 String menuName,
                                 String activeMsg,
                                 LocalDateTime activeTime
                    ) {
        this.adminId = adminId;
        this.adminEmail = adminEmail;
        this.fullName = fullName;
        this.menuName = menuName;
        this.activeMsg = activeMsg;
        this.activeTime = activeTime;
    }
}