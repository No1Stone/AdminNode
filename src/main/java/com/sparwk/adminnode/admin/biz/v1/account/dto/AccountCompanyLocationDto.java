package com.sparwk.adminnode.admin.biz.v1.account.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class AccountCompanyLocationDto {

    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long accntId;
    @Schema(description = "LOCATION_CD", nullable = false, example = "CNT000146")
    private String locationCd;
    @Schema(description = "LOCATION_CD NAME", nullable = true, example = "USA")
    private String locationCdName;

    @Builder
    public AccountCompanyLocationDto(Long accntId,
                                     String locationCd,
                                     String locationCdName
    ) {
        this.accntId = accntId;
        this.locationCd = locationCd;
        this.locationCdName = locationCdName;
    }
}
