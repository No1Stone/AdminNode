package com.sparwk.adminnode.admin.biz.v1.account.dto;

import com.sparwk.adminnode.admin.biz.v1.profile.dto.ProfileCurrentPositionDto;
import com.sparwk.adminnode.admin.biz.v1.profile.dto.ProfileMetaDataDto;
import com.sparwk.adminnode.admin.biz.v1.profile.dto.ProfileSnsDto;
import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

@Setter
@Getter
@NoArgsConstructor
public class AccountMemberDetailDto {

    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long accntId;
    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long profileId;
    @Schema(description = "ACCNT_TYPE_CD", nullable = false, example = "COMPANY / GROUP / PERSON")
    private String accntTypeCd;
    @Schema(description = "ACCNT First NAME", nullable = false, example = "Yoon")
    private String accntFirstName;
    @Schema(description = "ACCNT Middle NAME", nullable = true, example = "Jung")
    private String accntMiddleName;
    @Schema(description = "ACCNT Last NAME", nullable = false, example = "Hun")
    private String accntLastName;
    @Schema(description = "ACCNT FULL NAME", nullable = false, example = "Mr.Yoon")
    private String profileFullName;
    @Schema(description = "STAGE_NAME_YN", nullable = false, defaultValue = "N", allowableValues = {"Y", "N"})
    private YnTypeEnum stageNameYn;

    @Schema(description = "ACCNT EMAIL", nullable = false, example = "sparwk@sparwk.com")
    private String accntEmail;
    @Schema(description = "Date a Birth", nullable = false, example = "2001")
    private String bthYear;
    @Schema(description = "Date a Middle", nullable = false, example = "1")
    private String bthMonth;
    @Schema(description = "Date a Day", nullable = false, example = "1")
    private String bthDay;
    @Schema(description = "Gender", nullable = true)
    private List<ProfileMetaDataDto> genderList;
    @Schema(description = "Language", nullable = true)
    private List<ProfileMetaDataDto> languageList;
    @Schema(description = "Current country", nullable = false, example = "CNT000158")
    private String currentCityCountryCd;
    @Schema(description = "Current country Name", nullable = true, example = "USA")
    private String currentCityCountryCdName;
    @Schema(description = "Current city", nullable = true, example = "구로구")
    private String currentCityNm;
    @Schema(description = "Hometown Country", nullable = true, example = "CNT000158")
    private String hometownCountryCd;
    @Schema(description = "Hometown country Name", nullable = true, example = "USA")
    private String hometownCountryCdName;
    @Schema(description = "Hometown city", nullable = true, example = "구로구")
    private String hometownCityNm;

    @Schema(description = "Passport Country", nullable = false, example = "CNT000158")
    private String passportCountryCd;
    @Schema(description = "Passport country Name", nullable = true, example = "USA")
    private String passportCountryCdName;
    @Schema(description = "Passport photocopy", nullable = true, example = "http://...")
    private String passportPhotocopy;
    @Schema(description = "Passport file", nullable = true, example = "filename.pdf")
    private String passportfile;
    @Schema(description = "ACCOUNTVERIFYYN", nullable = false, defaultValue = "N", allowableValues = {"Y", "N"})
    private YnTypeEnum accountVerifyYn;
    @Schema(description = "COUNTRY_CD", nullable = false, example = "CNT000158")
    private String phoneCountryCd;
    @Schema(description = "COUNTRY_CD_NAME", nullable = true, example = "USA")
    private String phoneCountryCdName;
    @Schema(description = "PHONE_NUMBER", nullable = false, example = "0821012344321")
    private String phoneNumber;
    @Schema(description = "accountVerifyPhoneYn", nullable = false, defaultValue = "N", allowableValues = {"Y", "N"})
    private YnTypeEnum accountVerifyPhoneYn;

    @Schema(description = "profileCurrentPositionDtoList", nullable = true)
    private List<ProfileCurrentPositionDto> profileCurrentPositionDtoList;

    @Schema(description = "Headline", nullable = true, example = "Headline Text")
    private String headline;
    @Schema(description = "Roles", nullable = true)
    private List<ProfileMetaDataDto> rolesList;
    @Schema(description = "Territories", nullable = true)
    private List<ProfileMetaDataDto> territoriesList;
    @Schema(description = "Software", nullable = true)
    private List<ProfileMetaDataDto> softwareList;
    @Schema(description = "Genres", nullable = true)
    private List<ProfileMetaDataDto> genresList;
    @Schema(description = "Sub-Genres", nullable = true)
    private List<ProfileMetaDataDto> subGenresList;
    @Schema(description = "Moods", nullable = true)
    private List<ProfileMetaDataDto> moodsList;
    @Schema(description = "Themes", nullable = true)
    private List<ProfileMetaDataDto> themesList;
    @Schema(description = "Vocal", nullable = true)
    private List<ProfileMetaDataDto> vocalList;
    @Schema(description = "Era", nullable = true)
    private List<ProfileMetaDataDto> eraList;
    @Schema(description = "Instruments", nullable = true)
    private List<ProfileMetaDataDto> instrumentsList;
    @Schema(description = "Tempo", nullable = true)
    private List<ProfileMetaDataDto> tempoaList;

    @Schema(description = "Tempo", nullable = true)
    private List<ProfileSnsDto> snsList;

    @Schema(description = "IPI number", nullable = true, example = "00045620792")
    private String ipiNumber;
    @Schema(description = "CAE number", nullable = true, example = "207000043")
    private String caeNumber;
    @Schema(description = "ISNI number", nullable = true, example = "1234567890123456")
    private String isniNumber;
    @Schema(description = "IPN number", nullable = true, example = "W0512345")
    private String ipnNumber;
    @Schema(description = "NRO Society / number", nullable = true, example = "SAMI / 10846979")
    private String nroNumber;

    @Schema(description = "등록일", pattern = "yyMMdd hh:mm:ss", example = "2021-12-08T05:44:26.627991")
    private LocalDateTime regDt;

    @Builder
    public AccountMemberDetailDto(Long accntId,
                                  Long profileId,
                                  String accntTypeCd,
                                  String accntFirstName,
                                  String accntMiddleName,
                                  String accntLastName,
                                  String profileFullName,
                                  YnTypeEnum stageNameYn,
                                  String accntEmail,
                                  String bthYear,
                                  String bthMonth,
                                  String bthDay,
                                  List<ProfileMetaDataDto> genderList,
                                  List<ProfileMetaDataDto> languageList,
                                  String currentCityCountryCd,
                                  String currentCityCountryCdName,
                                  String currentCityNm,
                                  String hometownCountryCd,
                                  String hometownCountryCdName,
                                  String hometownCityNm,
                                  String passportCountryCd,
                                  String passportCountryCdName,
                                  String passportPhotocopy,
                                  String passportfile,
                                  YnTypeEnum accountVerifyYn,
                                  String phoneCountryCd,
                                  String phoneCountryCdName,
                                  String phoneNumber,
                                  YnTypeEnum accountVerifyPhoneYn,
                                  List<ProfileCurrentPositionDto> profileCurrentPositionDtoList,
                                  String headline,
                                  List<ProfileMetaDataDto> rolesList,
                                  List<ProfileMetaDataDto> territoriesList,
                                  List<ProfileMetaDataDto> softwareList,
                                  List<ProfileMetaDataDto> genresList,
                                  List<ProfileMetaDataDto> subGenresList,
                                  List<ProfileMetaDataDto> moodsList,
                                  List<ProfileMetaDataDto> themesList,
                                  List<ProfileMetaDataDto> vocalList,
                                  List<ProfileMetaDataDto> eraList,
                                  List<ProfileMetaDataDto> instrumentsList,
                                  List<ProfileMetaDataDto> tempoaList,
                                  List<ProfileSnsDto> snsList,
                                  String ipiNumber,
                                  String caeNumber,
                                  String isniNumber,
                                  String ipnNumber,
                                  String nroNumber,
                                  LocalDateTime regDt
    ) {
        this.accntId = accntId;
        this.profileId = profileId;
        this.accntTypeCd = accntTypeCd;
        this.accntFirstName = accntFirstName;
        this.accntMiddleName = accntMiddleName;
        this.accntLastName = accntLastName;
        this.profileFullName = profileFullName;
        this.stageNameYn = stageNameYn;
        this.accntEmail = accntEmail;
        this.bthYear = bthYear;
        this.bthMonth = bthMonth;
        this.bthDay = bthDay;
        this.genderList = genderList;
        this.languageList = languageList;
        this.currentCityCountryCd = currentCityCountryCd;
        this.currentCityCountryCdName = currentCityCountryCdName;
        this.currentCityNm = currentCityNm;
        this.hometownCountryCd = hometownCountryCd;
        this.hometownCountryCdName = hometownCountryCdName;
        this.hometownCityNm = hometownCityNm;
        this.passportCountryCd = passportCountryCd;
        this.passportCountryCdName = passportCountryCdName;
        this.passportPhotocopy = passportPhotocopy;
        this.passportfile = passportfile;
        this.accountVerifyYn = accountVerifyYn;
        this.phoneCountryCd = phoneCountryCd;
        this.phoneCountryCdName = phoneCountryCdName;
        this.phoneNumber = phoneNumber;
        this.accountVerifyPhoneYn = accountVerifyPhoneYn;
        this.profileCurrentPositionDtoList = profileCurrentPositionDtoList;
        this.headline = headline;
        this.rolesList = rolesList;
        this.territoriesList = territoriesList;
        this.softwareList = softwareList;
        this.genresList = genresList;
        this.subGenresList = subGenresList;
        this.moodsList = moodsList;
        this.themesList = themesList;
        this.vocalList = vocalList;
        this.eraList = eraList;
        this.instrumentsList = instrumentsList;
        this.tempoaList = tempoaList;
        this.snsList = snsList;
        this.ipiNumber = ipiNumber;
        this.caeNumber = caeNumber;
        this.isniNumber = isniNumber;
        this.ipnNumber = ipnNumber;
        this.nroNumber = nroNumber;
        this.regDt = regDt;

    }
}
