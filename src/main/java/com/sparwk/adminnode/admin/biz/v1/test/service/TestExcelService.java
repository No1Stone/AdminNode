package com.sparwk.adminnode.admin.biz.v1.test.service;

import com.sparwk.adminnode.admin.biz.v1.test.dto.TestDto;
import com.sparwk.adminnode.admin.biz.v1.test.dto.TestExcelDto;
import com.sparwk.adminnode.admin.jpa.entity.test.TestEntity;
import com.sparwk.adminnode.admin.jpa.entity.test.TestExcelEntity;
import com.sparwk.adminnode.admin.jpa.repository.test.TestExcelRepository;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class TestExcelService {

    private final TestExcelRepository testExcelRepository;

    private final Logger logger = LoggerFactory.getLogger(TestExcelService.class);


    @Transactional
    public Optional<TestExcelDto> saveContent(TestExcelDto request, HttpServletRequest req) {

        TestExcelEntity newTestExcel = TestExcelEntity.builder()
                .code(request.getCode())
                .ckey(request.getCkey())
                .cval(request.getCval())
                .cdesc(request.getCdesc())
                .build();

        TestExcelEntity saved = testExcelRepository.save(newTestExcel);

        Optional<TestExcelEntity> test = testExcelRepository.findById(saved.getTestExcelId());

        Optional<TestExcelDto> dtoPage =

                test
                        .map(p -> TestExcelDto.builder()
                                .code(p.getCode())
                                .ckey(p.getCkey())
                                .cval(p.getCval())
                                .cdesc(p.getCdesc())
                                .build()
                        );

        return dtoPage;
    }

}
