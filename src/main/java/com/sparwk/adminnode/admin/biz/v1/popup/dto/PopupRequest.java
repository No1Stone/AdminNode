package com.sparwk.adminnode.admin.biz.v1.popup.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class PopupRequest {

    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long popupSeq;
    @Schema(description = "코드 9자리", nullable = false, example = "PPS000001")
    private String popupStatusCd;
    @Schema(description = "POPUP 이름", nullable = false, example = "공지사항")
    private String popupTitle;
    @Schema(description = "POPUP 시작일시", nullable = false, example = "팝업오픈 시작일시")
    private String sdate;
    @Schema(description = "POPUP 종료일시", nullable = false, example = "팝업오픈 종료일시")
    private String edate;
    @Schema(description = "POPUP 가로 크기", nullable = false, example = "300")
    private int popupSizeWidth;
    @Schema(description = "POPUP 세로 크기", nullable = false, example = "500")
    private int popupSizeHeight;
    @Schema(description = "POPUP 가로 위치", nullable = false, example = "300")
    private int popupLocationWidth;
    @Schema(description = "POPUP 세로 위치", nullable = false, example = "500")
    private int popupLocationHeight;
    @Schema(description = "POPUP 내용", nullable = true, example = "팝업 내용")
    private String content;
    @Schema(description = "등록자", nullable = false, example = "10000001")
    private Long regUsr;
    @Schema(description = "수정자", nullable = false, example = "10000001")
    private Long modUsr;
}
