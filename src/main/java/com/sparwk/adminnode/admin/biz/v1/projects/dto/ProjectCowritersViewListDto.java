package com.sparwk.adminnode.admin.biz.v1.projects.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
@NoArgsConstructor
public class ProjectCowritersViewListDto {

    @Schema(description = "id값 Seq 사용", nullable = true, example = "1")
    private Long projId;
    @Schema(description = "id값 Seq 사용", nullable = true, example = "1")
    private Long profileId;
    @Schema(description = "FullName", nullable = true, example = "Mr.Yoon")
    private String fullName;
    @Schema(description = "Email", nullable = true, example = "yoon@sparwk.com")
    private String accntEmail;
    @Schema(description = "phoneNumber", nullable = true, example = "(KO) +821011112222")
    private String phoneNumber;
    @Schema(description = "companyName", nullable = true, example = "EK MUSIC")
    private String companyName;
    @Schema(description = "song count", nullable = true, example = "3")
    private int cnt;


    @Builder
    public ProjectCowritersViewListDto(Long projId,
                                       Long profileId,
                                       String fullName,
                                       String accntEmail,
                                       String phoneNumber,
                                       String companyName,
                                       int cnt
    ) {
        this.projId = projId;
        this.profileId = profileId;
        this.fullName = fullName;
        this.accntEmail = accntEmail;
        this.phoneNumber = phoneNumber;
        this.companyName = companyName;
        this.cnt = cnt;
    }
}
