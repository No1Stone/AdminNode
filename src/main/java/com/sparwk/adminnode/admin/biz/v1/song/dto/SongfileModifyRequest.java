package com.sparwk.adminnode.admin.biz.v1.song.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class SongfileModifyRequest {

    @Schema(description = "song id 번호", nullable = false, example = "1")
    private Long songId;
    @Schema(description = "song seq 번호", nullable = false, example = "1")
    private Long songFileSeq;
    @Schema(name = "song file comt", nullable = false, example = "good")
    private String songFileComt;


    @Builder
    public SongfileModifyRequest(Long songId,
                                 Long songFileSeq,
                                 String songFileComt
    ) {

        this.songId = songId;
        this.songFileSeq = songFileSeq;
        this.songFileComt = songFileComt;
    }
}
