package com.sparwk.adminnode.admin.biz.v1.account.dto;

import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Setter
@Getter
@NoArgsConstructor
public class AssoiateMemberViewListDto {

    @Schema(description = "id값 Seq 사용", example = "1")
    private Long accntId;
    @Schema(description = "id값 Seq 사용", example = "1")
    private Long profileId;
    @Schema(description = "ACCNT EMAIL", example = "sparwk@sparwk.com")
    private String accntEmail;
    @Schema(description = "PASSPORT_FIRST_NAME", example = "Yoon")
    private String passportFirstName;
    @Schema(description = "PASSPORT_Middle_NAME", nullable = true, example = "Jung")
    private String passportMiddleName;
    @Schema(description = "PASSPORT_LAST_NAME", example = "Hun")
    private String passportLastName;
    @Schema(description = "ACCNT FULL NAME", nullable = true, example = "Mr.Yoon")
    private String profileFullName;
    @Schema(description = "Stage name 여부 Y/N", defaultValue = "N", allowableValues = {"Y", "N"})
    private YnTypeEnum stageNameYn;
    @Schema(description = "bthYear", nullable = true, example = "2000")
    private String bthYear;
    @Schema(description = "bthMonth", nullable = true, example = "10")
    private String bthMonth;
    @Schema(description = "bthDay", nullable = true, example = "01")
    private String bthDay;
    @Schema(description = "gender list", nullable = true, example = "Female")
    private String gender;
    @Schema(description = "등록일", pattern = "yyMMdd hh:mm:ss", example = "2021-12-08T05:44:26.627991")
    private LocalDateTime regDt;


    @Builder
    public AssoiateMemberViewListDto(Long accntId,
                                     Long profileId,
                                     String accntEmail,
                                     String passportFirstName,
                                     String passportMiddleName,
                                     String passportLastName,
                                     String profileFullName,
                                     YnTypeEnum stageNameYn,
                                     String bthYear,
                                     String bthMonth,
                                     String bthDay,
                                     String gender,
                                     LocalDateTime regDt

    ) {
        this.accntId = accntId;
        this.profileId = profileId;
        this.accntEmail = accntEmail;
        this.passportFirstName = passportFirstName;
        this.passportMiddleName = passportMiddleName;
        this.passportLastName = passportLastName;
        this.profileFullName = profileFullName;
        this.stageNameYn = stageNameYn;
        this.bthYear = bthYear;
        this.bthMonth = bthMonth;
        this.bthDay = bthDay;
        this.gender = gender;
        this.regDt = regDt;
    }
}

