package com.sparwk.adminnode.admin.biz.v1.profile.dto;

import com.sparwk.adminnode.admin.jpa.entity.YnTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class ProfileCompanyRosteredDto {

    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long companyProfileId;
    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long accntId;
    @Schema(description = "id값 Seq 사용", nullable = false, example = "1")
    private Long profileId;
    @Schema(description = "Member Name", nullable = false, example = "Yoon Jung Sub")
    private String fullName;
    @Schema(description = "Primary Y/n", defaultValue = "N", allowableValues = {"Y", "N"})
    private YnTypeEnum primaryYn;
    @Schema(description = "a&r Y/n", defaultValue = "N", allowableValues = {"Y", "N"})
    private YnTypeEnum anrYn;
    @Schema(description = "artist Y/n", defaultValue = "N", allowableValues = {"Y", "N"})
    private YnTypeEnum artistYn;
    @Schema(description = "creator Y/n", defaultValue = "N", allowableValues = {"Y", "N"})
    private YnTypeEnum creatorYn;
    @Schema(description = "Email", nullable = false, example = "example@example.com")
    private String accntEmail;
    @Schema(description = "Date of joining StartDt", nullable = false, example = "example@example.com")
    private String joiningStartDt;
    @Schema(description = "Date of quitting day", nullable = false, example = "example@example.com")
    private String joiningEndDt;
    @Schema(description = "joining End Yn", defaultValue = "N", allowableValues = {"Y", "N"})
    private YnTypeEnum joiningEndYn;

    @Builder
    public ProfileCompanyRosteredDto(Long companyProfileId,
                                Long accntId,
                                Long profileId,
                                String fullName,
                                YnTypeEnum primaryYn,
                                YnTypeEnum anrYn,
                                YnTypeEnum artistYn,
                                YnTypeEnum creatorYn,
                                String accntEmail,
                                String joiningStartDt,
                                String joiningEndDt,
                                YnTypeEnum joiningEndYn
    ) {
        this.companyProfileId = companyProfileId;
        this.accntId = accntId;
        this.profileId = profileId;
        this.fullName = fullName;
        this.primaryYn = primaryYn;
        this.anrYn = anrYn;
        this.artistYn = artistYn;
        this.creatorYn = creatorYn;
        this.accntEmail = accntEmail;
        this.joiningStartDt = joiningStartDt;
        this.joiningEndDt = joiningEndDt;
        this.joiningEndYn = joiningEndYn;

    }
}
