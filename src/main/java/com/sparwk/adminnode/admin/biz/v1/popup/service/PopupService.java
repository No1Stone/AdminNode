package com.sparwk.adminnode.admin.biz.v1.popup.service;

import com.sparwk.adminnode.admin.biz.v1.CommonPagingListDto;
import com.sparwk.adminnode.admin.biz.v1.admin.service.AdminActiveLogService;
import com.sparwk.adminnode.admin.biz.v1.board.dto.BoardDelete;
import com.sparwk.adminnode.admin.biz.v1.popup.dto.PopupDto;
import com.sparwk.adminnode.admin.biz.v1.popup.dto.PopupRequest;
import com.sparwk.adminnode.admin.config.common.ActiveCodeConst;
import com.sparwk.adminnode.admin.config.common.GetAccountId;
import com.sparwk.adminnode.admin.config.common.Response;
import com.sparwk.adminnode.admin.config.common.ResultCodeConst;
import com.sparwk.adminnode.admin.jpa.entity.PeriodTypeEnum;
import com.sparwk.adminnode.admin.jpa.entity.admin.AdminActiveLogEntity;
import com.sparwk.adminnode.admin.jpa.entity.board.BoardFaqEntity;
import com.sparwk.adminnode.admin.jpa.entity.commonCode.CommonDetailCodeEntity;
import com.sparwk.adminnode.admin.jpa.entity.popup.PopupCateEnum;
import com.sparwk.adminnode.admin.jpa.entity.popup.PopupEntity;
import com.sparwk.adminnode.admin.jpa.entity.popup.PopupSorterEnum;
import com.sparwk.adminnode.admin.jpa.repository.commonCode.CommonDetailCodeRepository;
import com.sparwk.adminnode.admin.jpa.repository.popup.PopupRepository;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class PopupService {

    private final PopupRepository popupRepository;
    private final CommonDetailCodeRepository commonDetailCodeRepository;
    private final AdminActiveLogService adminActiveLogService;

    private final HttpServletRequest httpServletRequest;
    private final MessageSourceAccessor messageSourceAccessor;

    private final Logger logger = LoggerFactory.getLogger(PopupService.class);


    public Response<CommonPagingListDto> getList(    PopupCateEnum cate,
                                              String val,
                                              PeriodTypeEnum periodType,
                                              String sdate,
                                              String edate,
                                              PopupSorterEnum sorter,
                                              PageRequest pageRequest) {

        Response<CommonPagingListDto> res = new Response<>();

        List<PopupDto> popupList =
                popupRepository.findQryAll(cate, val, periodType, sdate, edate, sorter, pageRequest);

        logger.info("popupList - {}", popupList);

        //조회내용 없을 때
        if(popupList == null || popupList.size() < 1){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        CommonPagingListDto list = new CommonPagingListDto();

        //총 카운트
        long totalSize = popupRepository.countQryAll(cate, val, periodType, sdate, edate);
        long totalPage = totalSize / pageRequest.getPageSize();
        long mod = totalSize % pageRequest.getPageSize();
        if(mod > 0) {
            totalPage += 1;
        }

        list.setList(popupList);
        list.setSize(pageRequest.getPageSize());
        list.setPage(pageRequest.getPageNumber());
        list.setTotalPage(totalPage);
        list.setTotalSize(totalSize);

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(list);
        return res;
    }

    public Response<Optional<PopupDto>> getOne(Long id) {

        Response<Optional<PopupDto>> res = new Response<>();

        //id값이 없을 때
        if(id == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<PopupEntity> popup = popupRepository.findById(id);

        logger.info("popup - {}", popup);

        //조회내용 없을 때
        if(!popup.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //카테고리값 확인
        Optional<CommonDetailCodeEntity> commonDetailCode =
                Optional.of(commonDetailCodeRepository.findByDcode(popup.get().getPopupStatusCd()));

        //카테고리 코드내용 없을 때
        if(!commonDetailCode.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<PopupDto> dtoPage =

                popup.map(p -> PopupDto.builder()
                        .popupSeq(p.getPopupSeq())
                        .popupTitle(p.getPopupTitle())
                        .popupStatusCd(p.getPopupStatusCd())
                        .popupStatusCdName(commonDetailCode.get().getVal())
                        .sdate(p.getSdate())
                        .edate(p.getEdate())
                        .popupSizeWidth(p.getPopupSizeWidth())
                        .popupSizeHeight(p.getPopupSizeHeight())
                        .popupLocationWidth(p.getPopupLocationWidth())
                        .popupLocationHeight(p.getPopupLocationHeight())
                        .content(p.getContent())
                        .regUsr(p.getRegUsr())
                        .regDt(p.getRegDt())
                        .modUsr(p.getModUsr())
                        .modDt(p.getModDt())
                        .build()
                );

        logger.info("Popup - {}", dtoPage);

        //정상작동 할 때
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(dtoPage);
        return res;
    }

    @Transactional
    public Response<Void> saveContent(PopupRequest request, HttpServletRequest req) {

        Response<Void> res = new Response<>();

        //카테고리값 확인
        Optional<CommonDetailCodeEntity> commonDetailCode =
                Optional.of(commonDetailCodeRepository.findByDcode(request.getPopupStatusCd()));

        //카테고리 코드내용 없을 때
        if(!commonDetailCode.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if(
                request.getPopupTitle() == null
                        || request.getSdate() == null
                        || request.getEdate() == null
        ){
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if(
                request.getPopupSizeWidth() == 0
                        || request.getPopupSizeHeight() == 0
                        || request.getPopupLocationWidth() == 0
                        || request.getPopupLocationHeight() == 0
        ){
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        try {

            PopupEntity newPopup = PopupEntity.builder()
                    .popupTitle(request.getPopupTitle())
                    .popupStatusCd(request.getPopupStatusCd())
                    .sdate(request.getSdate())
                    .edate(request.getEdate())
                    .popupSizeWidth(request.getPopupSizeWidth())
                    .popupSizeHeight(request.getPopupSizeHeight())
                    .popupLocationWidth(request.getPopupLocationWidth())
                    .popupLocationHeight(request.getPopupLocationHeight())
                    .content(request.getContent())
                    .build();

            PopupEntity saved = popupRepository.save(newPopup);

            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
//            res.setResult(dtoPage);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));

            String menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_POPUP_SETTING.getCode());
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_CREATE.getCode());

            //netive query
            String usrName = adminActiveLogService.findQryUsrName(saved.getRegUsr());

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(saved.getRegDt())
                    .adminId(saved.getRegUsr())
                    .menuName(menuName)
                    .activeType("C")
                    .contentsId(saved.getPopupSeq())
                    .activeMsg(usrName + " " + activeMsg + " '" + saved.getPopupTitle() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e){
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }

        return res;
    }

    @Transactional
    public Response<Optional<PopupDto>> updateContent(Long id, PopupRequest request, HttpServletRequest req) {

        Response<Optional<PopupDto>> res = new Response<>();

        //id값이 없을 때
        if(id == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<PopupEntity> popupUpdate = popupRepository.findById(id);

        //조회내용 없을 때
        if(!popupUpdate.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //카테고리값 확인
        Optional<CommonDetailCodeEntity> commonDetailCode =
                Optional.of(commonDetailCodeRepository.findByDcode(request.getPopupStatusCd()));

        //카테고리 코드내용 없을 때
        if(!commonDetailCode.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_PCODE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if(
                request.getPopupTitle() == null
                        || request.getSdate() == null
                        || request.getEdate() == null
        ){
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //Value 값 없을 때
        if(
                request.getPopupSizeWidth() == 0
                        || request.getPopupSizeHeight() == 0
                        || request.getPopupLocationWidth() == 0
                        || request.getPopupLocationHeight() == 0
        ){
            res.setResultCd(ResultCodeConst.NOT_VALID_VALUE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        try {

            popupUpdate.get()
                    .update(request.getPopupStatusCd(),
                            request.getPopupTitle(),
                            request.getSdate(),
                            request.getEdate(),
                            request.getPopupSizeWidth(),
                            request.getPopupSizeHeight(),
                            request.getPopupLocationWidth(),
                            request.getPopupLocationHeight(),
                            request.getContent()
                    );

            Optional<PopupEntity> popup = popupRepository.findById(popupUpdate.get().getPopupSeq());

            Optional<PopupDto> dtoPage =

                    popup.map(p -> PopupDto.builder()
                            .popupSeq(p.getPopupSeq())
                            .popupTitle(p.getPopupTitle())
                            .popupStatusCd(p.getPopupStatusCd())
                            .popupStatusCdName(commonDetailCode.get().getVal())
                            .sdate(p.getSdate())
                            .edate(p.getEdate())
                            .popupSizeWidth(p.getPopupSizeWidth())
                            .popupSizeHeight(p.getPopupSizeHeight())
                            .popupLocationWidth(p.getPopupLocationWidth())
                            .popupLocationHeight(p.getPopupLocationHeight())
                            .content(p.getContent())
                            .regUsr(p.getRegUsr())
                            .regDt(p.getRegDt())
                            .modUsr(p.getModUsr())
                            .modDt(p.getModDt())
                            .build()
                    );

            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            res.setResult(dtoPage);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));

            String menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_POPUP_SETTING.getCode());
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_MODIFY.getCode());

            //netive query
            String usrName = adminActiveLogService.findQryUsrName(popup.get().getModUsr());

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(popup.get().getModDt())
                    .adminId(popup.get().getModUsr())
                    .menuName(menuName)
                    .activeType("U")
                    .contentsId(popup.get().getPopupSeq())
                    .activeMsg(usrName + " " + activeMsg + " '" + popup.get().getPopupTitle() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e){
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }

        return res;
    }

    @Transactional
    public Response<Void> deleteContent(Long id, HttpServletRequest req) {

        Response<Void> res = new Response<>();

        //id값이 없을 때
        if(id == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Optional<PopupEntity> popup = popupRepository.findById(id);

        //조회내용 없을 때
        if(!popup.isPresent()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        try {
            popupRepository.deleteById(id);
            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
//            res.setResult(dtoPage);

            ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
            GetAccountId getAccountId = new GetAccountId();
            logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));
            Long adminId = getAccountId.ofId(httpServletRequest);

            String menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_POPUP_SETTING.getCode());
            String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_DELETE.getCode());

            //netive query
            String usrName = adminActiveLogService.findQryUsrName(adminId);

            AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                    .activeTime(LocalDateTime.now())
                    .adminId(adminId)
                    .menuName(menuName)
                    .activeType("D")
                    .contentsId(popup.get().getPopupSeq())
                    .activeMsg(usrName + " " + activeMsg + " '" + popup.get().getPopupTitle() + "' in " + menuName)
                    .build();

            adminActiveLogService.saveContent(newLog);

        } catch (Exception e) {
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }
        return res;
    }

    @Transactional
    public Response<Void> deleteList(@NotNull BoardDelete request) {

        Response<Void> res = new Response<>();

        //id값이 없을 때
        if (request.getId() == null) {
            res.setResultCd(ResultCodeConst.NOT_EXIST_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //조회내용 없을 때
        try {

            Long[] kkk = request.getId();

            for(int i=0; i < kkk.length; i++) {

                Optional<PopupEntity> board = popupRepository.findById(request.getId()[i]);

                ////////// 로그 등록 /////////////////////////////////////////////////////////////////////////////////////
                GetAccountId getAccountId = new GetAccountId();
                logger.info("acc test - {}", getAccountId.ofId(httpServletRequest));
                Long adminId = getAccountId.ofId(httpServletRequest);


                String menuName = messageSourceAccessor.getMessage(ActiveCodeConst.MENU_POPUP_SETTING.getCode());
                String activeMsg = messageSourceAccessor.getMessage(ActiveCodeConst.ACTIVE_DELETE.getCode());

                //netive query
                String usrName = adminActiveLogService.findQryUsrName(adminId);

                AdminActiveLogEntity newLog = AdminActiveLogEntity.builder()
                        .activeTime(LocalDateTime.now())
                        .adminId(adminId)
                        .menuName(menuName)
                        .activeType("D")
                        .contentsId(board.get().getPopupSeq())
                        .activeMsg(usrName + " " + activeMsg + " '" + board.get().getPopupTitle() + "' in " + menuName)
                        .build();

                adminActiveLogService.saveContent(newLog);
            }

            popupRepository.deleteQryList(request.getId());
            //정상작동 할 때
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
//            res.setResult(dtoPage);

        } catch (Exception e) {
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }
        return res;

    }
}
